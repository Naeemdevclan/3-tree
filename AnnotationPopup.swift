//
//  AnnotationPopup.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 14/04/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class AnnotationPopup: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBAction func Cancel_Pressed(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBOutlet weak var annotationsTable: UITableView!
    
    var factory:String?
    var popupArray: AnyObject?
    
    var regionNames = [
        "A",
        "B",
        "C",
        "D",
        "E",
        "F",
        "G",
        "H",
        "I",
        "J",
        "K"
    ]
    struct items {
        var A : UIColor?
        var B : String
        var C : String
        var D : String
        var E : String
    }
    var tableCellData = [items]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(popupArray?.count)
        
//        self.navigationController?.title = factory
        // Do any additional setup after loading the view.
       
//        for var Audit=0; Audit<popupArray?.count; Audit+=1 {
        let popupArray2:[AnyObject] = popupArray as! [AnyObject]
        for Audit in (0 ..< popupArray2.count) where Audit<popupArray2.count{
            
            let AuditResultName = popupArray2[Audit]["AuditResult"]! as! String
            let AuditCodeNumber = popupArray2[Audit]["AuditCode"]! as! String
            let AuditorName = popupArray2[Audit]["Auditor"]! as! String
            let AuditorPo = popupArray2[Audit]["Po"]! as! String
            
            var color: UIColor?
            var AuditResult: String?
            
            if AuditResultName == "P" {
                AuditResult = " "
                color = UIColor(colorLiteralRed: 114.0/255, green: 161.0/255, blue: 14.0/255, alpha: 1.0)
                
            }else if AuditResultName == "F" {
                AuditResult = " "
                color = UIColor(colorLiteralRed: 220.0/255, green: 0.0/255, blue: 8.0/255, alpha: 1.0)
                
            }else if AuditResultName == "H" {
                AuditResult = " "
                color = UIColor.orange
            }else {
                color = UIColor(colorLiteralRed: 216.0/255, green: 215.0/255, blue: 217.0/255, alpha: 1.0)
                AuditResult = "X"
            }
            self.tableCellData.append(items(A: color, B: AuditCodeNumber, C: AuditorName, D: AuditorPo, E: AuditResult!))
            print(self.tableCellData)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Table View
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = self.annotationsTable.dequeueReusableCell(withIdentifier: "cell") as! AnnotationPopupCell
        
        cell.AuditResult.backgroundColor = tableCellData[indexPath.row].A
        cell.AuditCode.text = tableCellData[indexPath.row].B
        cell.Auditor.text = tableCellData[indexPath.row].C
        cell.Po.text = tableCellData[indexPath.row].D
        
        cell.AuditResultLabel.text = tableCellData[indexPath.row].E
        return cell
    }
    
    func tableView(_ tableView: UITableView,numberOfRowsInSection section: Int) -> Int{
        return tableCellData.count
    }
//
//    
//    //
//    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle{
//        return .FullScreen
//    }
//    func presentationController(controller: UIPresentationController, viewControllerForAdaptivePresentationStyle style: UIModalPresentationStyle) -> UIViewController?{
//        print(controller.presentedViewController)
//        let navController:UINavigationController = UINavigationController(rootViewController: controller.presentedViewController)
//        controller.presentedViewController.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action:"done")
//        areaListTable.reloadData()
//        //            selectedItemIndex = -1
//        return navController
//    }
//    
//    func done (){
//        presentedViewController?.dismissViewControllerAnimated(true, completion: nil)
//        //        mapSearch()
//    }
    
    //MARK: UITableView Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        //            let selectedCell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        //            selectedCell.accessoryType = UITableViewCellAccessoryType.Checkmark
        //            selectedItemIndex = indexPath.row
        if let KPIController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
            //            self.presentViewController(resultController, animated: true, completion: nil)
            print(self.presentedViewController)
            print(self.presentingViewController)
            let cell = tableView.cellForRow(at: indexPath) as! AnnotationPopupCell
            if cell.AuditResultLabel.text != "X"{
                AuditCodeOfDashboardCell = cell.AuditCode.text
                self.present(KPIController, animated: false, completion: nil)
            }
//            if cell.AuditResult!.backgroundColor!.isEqual(UIColor.lightGrayColor()){
//                AuditCodeOfDashboardCell = cell.AuditCode.text
//                self.presentViewController(KPIController, animated: false, completion: nil)
//            }else{
//                print("no match")
//            }
        }
        
    }
    fileprivate func compareColors (_ c1:UIColor, c2:UIColor) -> Bool{
        // some kind of weird rounding made the colors unequal so had to compare like this
        
        var red:CGFloat = 0
        var green:CGFloat  = 0
        var blue:CGFloat = 0
        var alpha:CGFloat  = 0
        c1.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        
        var red2:CGFloat = 0
        var green2:CGFloat  = 0
        var blue2:CGFloat = 0
        var alpha2:CGFloat  = 0
        c2.getRed(&red2, green: &green2, blue: &blue2, alpha: &alpha2)
        
        return (Int(green*255) == Int(green2*255))
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UIColor {
    func isEqualToColor(_ otherColor : UIColor) -> Bool {
        if self == otherColor {
            return true
        }
        
        let colorSpaceRGB = CGColorSpaceCreateDeviceRGB()
        let convertColorToRGBSpace : ((_ color : UIColor) -> UIColor?) = { (color) -> UIColor? in
            if color.cgColor.colorSpace?.model == CGColorSpaceModel.monochrome {
                let oldComponents = color.cgColor.components
                let components : [CGFloat] = [ oldComponents![0], oldComponents![0], oldComponents![0], oldComponents![1] ]
                let colorRef = CGColor(colorSpace: colorSpaceRGB, components: components)
                let colorOut = UIColor(cgColor: colorRef!)
                return colorOut
            }
            else {
                return color;
            }
        }
        
        let selfColor = convertColorToRGBSpace(self)
        let otherColor = convertColorToRGBSpace(otherColor)
        
        if let selfColor = selfColor, let otherColor = otherColor {
            return selfColor.isEqual(otherColor)
        }
        else {
            return false
        }
    }
}


