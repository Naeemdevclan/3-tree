//
//  DocPreview.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 26/08/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
//var viewer : UIDocumentInteractionController!

class DocPreview: UIViewController, URLSessionDownloadDelegate, UIDocumentInteractionControllerDelegate {

    
    var downloadFileURL: URL!

    
    
    var downloadTask: URLSessionDownloadTask!
    var backgroundSession: Foundation.URLSession!
    
    var viewer : UIDocumentInteractionController?
    
    
//    @IBAction func close(sender: AnyObject) {
//        
////        self.dismissViewControllerAnimated(true, completion: nil)
////        self.view.removeFromSuperview()
//    }
    
    var pdfViewed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        print("downloadFileURL = ",downloadFileURL)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if pdfViewed == false{
        
        print("downloadFileURL when appears = ",downloadFileURL)
        
        let backgroundSessionConfiguration = URLSessionConfiguration.background(withIdentifier: "backgroundSession")
       
        backgroundSession = Foundation.URLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)

        
        if !downloadFileURL.absoluteString.isEmpty {
            
            downloadTask = backgroundSession.downloadTask(with: downloadFileURL)
            
            //            self.progressView.hidden = false
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            downloadTask.resume()
            
        }
//        if !DocFilePath.isEmpty{
        
//            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
            
//            dispatch_after(delayTime, dispatch_get_main_queue()) {
                print("test")
            
                //showFileWithPath(DocFilePath)
//            }
//        }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func showFileWithPath(_ path: String){
        
        let topVC = self.topMostController()
        print("topMostController before Preview = ",topVC)
        print(self.view)
        
//        self.view.window!.setValue(topVC.view.window, forKey: "window")
        // = topVC.view.window
       
        let topVC2 = self.topMostController()
        print("topMostController before Preview2 = ",topVC2)
        
//        var viewer : UIDocumentInteractionController!
        
        let isFileFound:Bool? = FileManager.default.fileExists(atPath: path)
        if isFileFound == true{
            print("self.view.window= \(self.view.window)")
            // 2nd time self.view.window returns nil which means that Self class view is not in visible
            //            dispatch_async(dispatch_get_main_queue(), { () -> Void in
//            if (topVC.view.window != nil){
                print("yes kpi is loaded into memory & ")
                print("KPI view's window is visible")
                // viewController is visible
                
                viewer = UIDocumentInteractionController(url: URL(fileURLWithPath: path))
            
//                viewer!.delegate = self
            
//                self.definesPresentationContext = true
            
            
            let url = URL(string:"itms-books:");
            
            if UIApplication.shared.canOpenURL(url!) {
                viewer!.presentOpenInMenu(from: CGRect.zero, in: topVC.view, animated: true)
            }
              // viewer.presentPreviewAnimated(true)
//            }
            
        }
    }
    
   
    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didFinishDownloadingTo location: URL){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false

        let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        
        let documentDirectoryPath:String = path.first!//path[0]
        
        let fileManager = FileManager()
        
        var destinationURLForFile = URL(fileURLWithPath: documentDirectoryPath + "/QAReport.pdf")
        
        if fileManager.fileExists(atPath: destinationURLForFile.path){
            
            do{
                try fileManager.removeItem(atPath: destinationURLForFile.path)
                destinationURLForFile = URL(fileURLWithPath: documentDirectoryPath + "/QAReport.pdf")
            }catch{
                print(error)
            }
        }
        
        
        do {
            try fileManager.moveItem(at: location, to: destinationURLForFile)
            
            // show file
            
            DispatchQueue.main.async(execute: { () -> Void in
                
                self.showFileWithPath(destinationURLForFile.path)
            })
            
        }catch{
            print(error)
            print("An error occurred while moving file to destination url")
        }
        
    }
    func documentInteractionController(_ controller: UIDocumentInteractionController, willBeginSendingToApplication application: String?) {
        print("document start sending to application")
        pdfViewed = true
    }
    func documentInteractionController(_ controller: UIDocumentInteractionController, didEndSendingToApplication application: String?) {
        print("document end sending to application")
    }
   
    func documentInteractionControllerDidDismissOpenInMenu(_ controller: UIDocumentInteractionController) {
        print("document open in Menu dismissed.")
    }
    
//    func documentInteractionControllerViewControllerForPreview(controller: UIDocumentInteractionController) -> UIViewController{
//        return self
//        
////    }
//    func documentInteractionControllerWillBeginPreview(controller: UIDocumentInteractionController) {
//        print("document will start preview")
//         pdfViewed = true
//    }
//    func documentInteractionControllerDidEndPreview(controller: UIDocumentInteractionController) {
//        
//        print("document preview ends")
//        controller.dismissPreviewAnimated(true)
//        
//        //        viewer.dismissPreviewAnimated(true)
//        //        self.dismissViewControllerAnimated(false, completion: nil)  //- this will take us back to dashboard
//    }
    
    
    func topMostController() -> UIViewController {
        var topController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        while (topController.presentedViewController != nil) {
            topController = topController.presentedViewController!
        }
        return topController
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


