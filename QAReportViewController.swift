//
//  QAReportViewController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 15/03/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
var urlToDownload:URL!


class QAReportViewController: UIViewController,UITabBarDelegate {

    @IBOutlet weak var tabBar2: UITabBar!
    @IBOutlet weak var tabBar1: UITabBar!
    
    weak var tab1vc: UIViewController!
    weak var tab2vc: UIViewController!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!

    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    
    
    @IBOutlet weak var progressView: UIProgressView!
    
//    var downloadReport:Bool = true
    
//    var downloadTask: NSURLSessionDownloadTask!
//    var backgroundSession: NSURLSession!
    
    @IBAction func DownloadBTN_pressed(_ sender: AnyObject) {
        
////        if downloadReport == true{
////            downloadReport = false
//            downloadTask = backgroundSession.downloadTaskWithURL(urlToDownload)
//            self.progressView.hidden = false
//            downloadTask.resume()
////        }else{
//            print("Report downloaded sucessfully")
////        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.progressView.isHidden = true

        // Do any additional setup after loading the view.
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "Selected QAReport")!, UIImage(named: "Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)

    //- - - - - - - - - - - - - - - -
        
//        let backgroundSessionConfiguration = NSURLSessionConfiguration.backgroundSessionConfigurationWithIdentifier("backgroundSession")
//        backgroundSession = NSURLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: NSOperationQueue.mainQueue())
//        progressView.setProgress(0.0, animated: false)
    
    }
    override func viewDidAppear(_ animated: Bool) {
        
//        if downloadReport == true{
//            downloadReport = false
//            downloadTask = backgroundSession.downloadTaskWithURL(urlToDownload)
//            self.progressView.hidden = false
//            downloadTask.resume()
//        }else{
//            print("Report downloaded sucessfully")
//        }
    }
    
    
    
//    // - - Handling download file-  - - - - - -  - -
//    
//    func URLSession(session: NSURLSession,
//        downloadTask: NSURLSessionDownloadTask,
//        didFinishDownloadingToURL location: NSURL){
//            
//            //            backgroundSession.fi
//            let path = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
//            let documentDirectoryPath:String = path.first!//path[0]
//            let fileManager = NSFileManager()
//            var destinationURLForFile = NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingString("/QAReport.pdf"))
//            
//            if fileManager.fileExistsAtPath(destinationURLForFile.path!){
//                //                showFileWithPath(destinationURLForFile.path!)
//                do{
//                    try fileManager.removeItemAtPath(destinationURLForFile.path!)
//                    destinationURLForFile = NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingString("/QAReport.pdf"))
//                }catch{
//                    print(error)
//                }
//            }
//            //            else{
//            //                if location != nil{
//            do {
//                try fileManager.moveItemAtURL(location, toURL: destinationURLForFile)
//                // show file
//                dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                    
//                    self.showFileWithPath(destinationURLForFile.path!)
//                })
//            }catch{
//                print("An error occurred while moving file to destination url")
//            }
//            //    }
//            //            }
//    }
//    
//    func showFileWithPath(path: String){
//        let isFileFound:Bool? = NSFileManager.defaultManager().fileExistsAtPath(path)
//        if isFileFound == true{
//            dispatch_async(dispatch_get_main_queue(), { () -> Void in
//               
//                let viewer = UIDocumentInteractionController(URL: NSURL(fileURLWithPath: path))
//                viewer.delegate = self
//                viewer.presentPreviewAnimated(true)
//            })
//            
//        }
//    }
//    func URLSession(session: NSURLSession,
//        downloadTask: NSURLSessionDownloadTask,
//        didWriteData bytesWritten: Int64,
//        totalBytesWritten: Int64,
//        totalBytesExpectedToWrite: Int64){
//            progressView.setProgress(Float(totalBytesWritten)/Float(totalBytesExpectedToWrite), animated: true)
//    }
//    
//    func documentInteractionControllerViewControllerForPreview(controller: UIDocumentInteractionController) -> UIViewController{
//        return self
//        
//    }
//    func documentInteractionControllerDidEndPreview(controller: UIDocumentInteractionController) {
//        print("document preview ends")
////        downloadReport = false
////        controller.dismissPreviewAnimated(true)
////        self.removeFromParentViewController()
////        self.dismissViewControllerAnimated(false, completion: nil)
//    }
//    func documentInteractionControllerWillPresentOptionsMenu(controller: UIDocumentInteractionController) {
//        print("option menu is here")
//    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
//            defaults.removeObjectForKey("userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 4:
//            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
//                //                self.dismissViewControllerAnimated(true, completion: nil)
//                presentViewController(resultController, animated: true, completion: nil)
//            }
            
            let selectedImage:UIImage = UIImage(named: "Selected QAReport")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 5:
            if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }else{
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }

            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_selected_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        default:
            break
        }
    }
}
