//
//  AddDefect.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 19/04/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

var addednumberofdefects = 0
var DefectAdded = [String]()

protocol SaveDefectsLogedTemporarily {
        func saveToArrayOfDefectsLogged(_ Color: UIColor ,ID: String, Type : String, Code : String)
}


import UIKit
//import RebekkaTouch


//var AddDeffectOfflineImageData:[String] = []
//var AddDeffectOfflineTextData = [String:String]()




var timesDefectTypes = false

var DefectsData0 = [IndexPath:String]()
var DefectsRateData0 = [IndexPath:String]()
var DefectsAreaData0 = [IndexPath:String]()

class AddDefect: UIViewController, UITabBarDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate,UIGestureRecognizerDelegate {

    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    weak var tab1vc: UIViewController!
    weak var tab2vc: UIViewController!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!

    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    
    @IBOutlet weak var scrollView: UIScrollView!
    // IBOutlets
//    AuditCodeLabel
    @IBOutlet weak var AuditCodeLabel: UILabel!
//    SampleChecked
    @IBOutlet weak var SampleChecked: UILabel!
//    SampleSize
    @IBOutlet weak var SampleSize: UILabel!
//    AuditStageLabel
    @IBOutlet weak var AuditStageLabel: UILabel!
    
    @IBOutlet weak var sampleImage: UIImageView!
    
    @IBOutlet weak var BtnDefectType: UIButton!
    @IBOutlet weak var BtnDefectCode: UIButton!
    @IBOutlet weak var BtnDefectArea: UIButton!
   
    @IBOutlet weak var PICK_DEFECT_TYPE: UILabel!
    @IBOutlet weak var PICK_DEFECT_CODE: UILabel!
    @IBOutlet weak var PICK_DEFECT_AREA: UILabel!

    @IBOutlet weak var commentsField: UITextField!
    
    
    // delegate variable of protocol SaveDefectsLogedTemporarily
    
    
    var delegate: SaveDefectsLogedTemporarily?
    
    //- Variable for CAP
    
    var MajorORMinorColor:UIColor?
    // - - - - -
    
    var Previous_PICK_DEFECT_TYPE_TEXT = ""
    var Previous_PICK_DEFECT_TYPE = ""
    
    var capturePhoto:UIImage?
     var imagePicker: UIImagePickerController!
    
//    @IBOutlet weak var ADD: UIButton!
    
    var AuditCodein : String!
    var SampleCheckedin : String!
    var SampleSizein : String!
    var AuditStagein : String!
    var AuditStageColorin : UIColor!
    var ReportId: String!
    var MajorORMinor: String!
    var AuditScheduleDate: String!
    
    var DefectID = ""
    var PICK_DEFECT_TYPE_TEXT = ""
    
    var DefectCodeID = ""
    var PICK_DEFECT_CODE_TEXT = ""

    var PICK_DEFECT_AREA_TEXT = ""
    var DefectedAreaID = ""
    
    var endDate:Date!
    var convertedEndDate:String!

    @IBOutlet weak var saveTopConstraint: NSLayoutConstraint!
    var dictionary:NSDictionary!
    var bodyData:String!
    
    // Rebekka Touch FTP Session
//    var session: Session!
    
    let libraryDirectoryPathString = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true).first!
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    var jsonFilePath:URL!
    var created:Bool!
    
    var dirArray:[AnyObject]!
    var monArray:[AnyObject]!
    var dayArray:[AnyObject]!
    
//    var resource: ResourceItem!
    var monthResource: String!
    var dayResource: String!
    
    var paramPath = "/quonda"
    
    
    
    
    // variables for Offline mode
    
//    var created:Bool!
//    var isDirectory: ObjCBool = false
    
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    
//    let fileManager = NSFileManager.defaultManager()
    
    
    
    @IBAction func BTNDefectAreaPressed(_ sender: Any) {
        performSegue(withIdentifier: "defectArea", sender: ReportId)
    }
   // UIButtons on clicked Actions
    @IBAction func BtnDefectTypePressed(_ sender: AnyObject) {
        if timesDefectTypes == false{
            performSegue(withIdentifier: "DefectTypeSegue", sender: ReportId)
            
        }else if timesDefectTypes == true{
//            if PICK_DEFECT_TYPE.text != Previous_PICK_DEFECT_TYPE && self.PICK_DEFECT_TYPE_TEXT != Previous_PICK_DEFECT_TYPE {
                performSegue(withIdentifier: "DefectTypeSegue", sender: ReportId)
//                print("moving a head.")
//                DefectsRateData0.removeAll()
//                PICK_DEFECT_CODE.text = ""
//                self.PICK_DEFECT_CODE_TEXT = ""

//            }
        }
    }
    
    @IBAction func BtnDefectCodePressed(_ sender: AnyObject) {
        print(DefectID)
        print(PICK_DEFECT_TYPE.text)
        print(PICK_DEFECT_TYPE_TEXT)
        if DefectID != "" && PICK_DEFECT_TYPE.text != ""{ //PICK_DEFECT_TYPE_TEXT != "" {
           
            timesDefectTypes = true
            performSegue(withIdentifier: "DefectRateSegue", sender: ReportId)
        
        }else{
            let alertView = UIAlertController(title: "Message" , message: "First Select DefectType.", preferredStyle: .alert)
            let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            })
            alertView.addAction(ok_action)
            
            self.present(alertView, animated: true, completion: nil)
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        commentsField.resignFirstResponder()
        return true
    }
    
    
    var butotn:UIButton!
    var savedParamPath:String!
    var convertedEndDateTime = ""
    var destImageNametext = ""
    @IBAction func SAVE_pressed(_ sender: AnyObject) {
        
        print("sampleImage=")
        
        addednumberofdefects = addednumberofdefects + 1
        let counfd =  defaults.object(forKey:"addedDefectsCounts \(self.AuditCodein)") as! Int
      let defectadded =  counfd + 1
        defaults.set(defectadded, forKey:"addedDefectsCounts \(self.AuditCodein)")

       // if self.delegate != nil{
            
            self.endDate = Date()
           convertedEndDateTime = self.endDate.CustomDateTimeformat
        self.delegate?.saveToArrayOfDefectsLogged(MajorORMinorColor!, ID: convertedEndDateTime ,Type: self.PICK_DEFECT_TYPE.text!, Code: self.PICK_DEFECT_CODE.text!)
        print(self.sampleImage.image)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        if (self.sampleImage.image != UIImage(named: "default.png")){
            print("every thing is ok")
        }
        if self.sampleImage.image != UIImage(named: "default.png") && self.PICK_DEFECT_TYPE.text != "DEFECT TYPE" && self.PICK_DEFECT_CODE.text != "DEFECT CODE" && self.PICK_DEFECT_CODE.text != "" && self.PICK_DEFECT_AREA.text != "DEFECT AREA"{
            
            //Now save to arrayOfDeffectsLogged
            
//            if self.delegate != nil{
//                
//                self.endDate = Date()
//                let convertedEndDateTime = self.endDate.CustomDateTimeformat
////                MajorORMinorColor
//                
//                                                       // (Color,    ID  ,   Type   ,     Code )
//                self.delegate?.saveToArrayOfDefectsLogged(MajorORMinorColor!, ID: convertedEndDateTime ,Type: self.PICK_DEFECT_TYPE.text!, Code: self.PICK_DEFECT_CODE.text!)
//            }
            
            //  && !self.commentsField.text!.isEmpty
            
            // disable userInteraction of all the items displayed on the screen
            
            sampleImage.isUserInteractionEnabled = false
            BtnDefectType.isUserInteractionEnabled = false
            BtnDefectCode.isUserInteractionEnabled = false
            BtnDefectArea.isUserInteractionEnabled = false
            commentsField.isUserInteractionEnabled = false
            
            butotn = sender as! UIButton
            butotn.isEnabled = false
            butotn.backgroundColor = UIColor.gray
            
            //get image from Library Dir
            let imagePath = fileLibraryDirectory("myCapturedImage.jpg")  // this path is in string format
            print(imagePath)
            let CapImage = loadImageFromPath(imagePath)
            var CapUIImage:UIImage?
            
            //        if CapUIImage == nil{
            //            print("saved image is nill")
            //        }else{
            CapUIImage = CapImage
            print(CapUIImage)
            
            let defaultImage = UIImage(named: "default")
            let jpgImageData = UIImageJPEGRepresentation(defaultImage!, 0.5)
            let CapUIImageData = UIImageJPEGRepresentation(defaultImage!, 0.5)
            
            if jpgImageData == CapUIImageData{
                print("same image as sample image")
            }
            
            if savedParamPath != nil{
                
//                ImageUpload(NSURL(string: imagePath)!, destinationFileName: savedParamPath)
                
            }
            else{
                let randomString = randomStringWithLength(6)
                print("randomString=")
                print(randomString)
                let url: URL = URL(string: imagePath)!
                
                let value = PICK_DEFECT_CODE.text!
                let Valuecomponents = value.components(separatedBy: " - ")
                print("left=\(Valuecomponents.first)")
                print("right=\(Valuecomponents.last)")
                
                let Name1 = "\(self.AuditCodeLabel.text!)"+"_"+"\(Valuecomponents.first!)"+"_"
                let Name2 = "\(self.DefectedAreaID)"+"_"+"\(randomString).jpg"
                let destImageName = Name1+Name2
                destImageNametext = Name1+Name2
                //"AUDIT_CODE_DEFECT_CODE_AREA_CODE_RANDOM_NO.jpg"
                //            let path = "/quonda/\(NSUUID().UUIDString).jpg"
                
                //            paramPath = paramPath+"/"+"\(NSUUID().UUIDString).jpg"
                paramPath = paramPath+"/"+destImageName
                print("paramPath =")
                print(paramPath)
                
                savedParamPath = paramPath
                
                
                //Now Check if Internet connection is not available then save images and other data to Documents directory
                //?????
                //????
                //??
                //?
                
                //If Internet is available or Not just save the data offline.
                //First Stop sync Service Then save data
                
                syncObject.endBackgroundTask()
                updateTimer?.invalidate()
                syncObject = nil
                
                saveDataInDocumentsDir(imagePath, destinationFileName: destImageName)
                
                
                //NOW START Sync Service again
                
                syncObject = syncService()
                
                
                
                /*
                if !Reachability.isConnectedToNetwork(){
                    
                    print("Internet is not available in AddDefect screen.")
                    
                    // imagePath: is a path of saved image.
                    //paramPath: will be the path of server, where image has to save.
                   
                    saveDataInDocumentsDir(imagePath, destinationFileName: destImageName)
                    
                }else{
                    
                    ImageUpload(url, destinationFileName: paramPath)
                }
            */
                
            }
            
        }else{
            
            var SI = ""
            var DT = ""
            var DC = ""
            var DA = ""
            
            if self.sampleImage.image == UIImage(named: "default.png"){
                SI = "SamplePhoto "
            }
            if self.PICK_DEFECT_TYPE.text == "DEFECT TYPE"{
                DT = "DefectType,"
            }
            if self.PICK_DEFECT_CODE.text == "DEFECT CODE" || self.PICK_DEFECT_CODE.text == ""{
                DC = "DefectCode,"
            }
            if self.PICK_DEFECT_AREA.text == "DEFECT AREA" {
                DA = "DefectArea,"
            }
            
            let alertView = UIAlertController(title: "Note!" , message: "\(SI+DT+DC+DA) Cannot remain empty", preferredStyle: .alert)
            let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            })
                alertView.addAction(ok_action)
            
            self.present(alertView, animated: true, completion: nil)

        }
    }
    
    
    
    
//    var success = ""
//    //upload function for FTP image upload
//    func ImageUpload(url:NSURL!, destinationFileName:String!) {
//        
//        if Reachability.isConnectedToNetwork() == true {
//            print("Internet connection is OK")
//            
//            self.session.upload(url, path: destinationFileName) {
//                (result, error) -> Void in
//                print("Upload file with result:\n\(result), error: \(error)\n\n")
//                
//                print(result)
//                self.success = "\(result)"
//                if self.success == "true"{
//                    
//                    //CALL Function To Upload TEXT DATA
//                    
//                    self.uploadTextData()
//                    
//                    // deletes the saved image
//                    let libraryDirectoryPath = NSURL(string: self.libraryDirectoryPathString)!
//                    self.jsonFilePath = libraryDirectoryPath.URLByAppendingPathComponent("myCapturedImage.jpg")
//                    
//                    // creating a .jpg file in the Library Directory
//                    if self.fileManager.fileExistsAtPath(self.jsonFilePath.absoluteString, isDirectory: &self.isDirectory) {
//                        do{
//                            let defaultImage = UIImage(named: "default")
//                            let jpgImageData = UIImageJPEGRepresentation(defaultImage!, 1.0)
//                            try jpgImageData?.writeToFile(self.jsonFilePath.path!, options: .DataWritingFileProtectionNone)
//                            print("successfully written Default image in Library dir!")
//                        } catch let error as NSError {
//                            print("Default Image couldn't written to file ")
//                            print(error.description)
//                        }
//                        
//                    } else {
//                        print("File NOT exists")
//                    }
//                }else{
//                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
//                    self.butotn.enabled = true
//                    self.butotn.backgroundColor = UIColor.lightGrayColor()
//                    
//                    let alertView = UIAlertController(title: "Retry" , message: "\(error!.description)Audit Not Saved", preferredStyle: .Alert)
//                    let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .Default, handler: { (UIAlertAction) -> Void in
//                        
//                        self.sampleImage.userInteractionEnabled = true
//                        self.BtnDefectType.userInteractionEnabled = true
//                        self.BtnDefectCode.userInteractionEnabled = true
//                        self.BtnDefectArea.userInteractionEnabled = true
//                        self.commentsField.userInteractionEnabled = true
//                        self.savedParamPath = destinationFileName
//                    })
//
//                    alertView.addAction(ok_action)
//                    self.presentViewController(alertView, animated: true, completion: nil)
//                }
//            }
//        } //if internet connection is OK
//        else{
//            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
//            self.butotn.enabled = true
//            self.butotn.backgroundColor = UIColor.lightGrayColor()
//            
//            let alertView = UIAlertController(title: "Audit cannot save" , message: "You may not connected to Internet.\nTry again", preferredStyle: .Alert)
//          
//            let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .Default, handler: { (UIAlertAction) -> Void in
//                
//                self.sampleImage.userInteractionEnabled = true
//                self.BtnDefectType.userInteractionEnabled = true
//                self.BtnDefectCode.userInteractionEnabled = true
//                self.BtnDefectArea.userInteractionEnabled = true
//                self.commentsField.userInteractionEnabled = true
//                
//                })
//
//            alertView.addAction(ok_action)
//            self.presentViewController(alertView, animated: true, completion: nil)
//        } // Network failed else part
//    }
    
    


   
    func saveDataInDocumentsDir(_ imagePath:String!, destinationFileName: String!){
        
        print("You are in AddDeffect OFFLINE mode.")
        
        // Creating Json file
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("AddDefectImagesFile\(self.AuditCodeLabel.text!).json")
        
    
        // creating a AddDefectImagesFile.json file in the Documents folder
        
        if !fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("AddDefectImagesFile.json File created ")
                
                // Now save it to FilesToSync.json file
                
                saveFileNameTo_FilesToSync("AddDefectImagesFile\(self.AuditCodeLabel.text!).json")
                
                //(jsonFilePath.absoluteString)
                
                
            } else {
                print("Couldn't create file for some reason")
            }
        }
        else {
            
            print("AddDefectImagesFile.json File already exists")
        }
        
        //file creation ends
        
        
        
        let DisplayedImage = self.sampleImage.image!.resize(0.45)
        
        if let data = UIImageJPEGRepresentation(DisplayedImage, 0.5) {
            let Imagefilename = getDocumentsDirectory().appendingPathComponent(destinationFileName)
            let ImagePathsfile = getDocumentsDirectory().appendingPathComponent("AddDefectImagesFile\(self.AuditCodeLabel.text!).json")
           
            if !self.fileManager.fileExists(atPath: Imagefilename, isDirectory: &self.isDirectory) {
                
                try? data.write(to: URL(fileURLWithPath: Imagefilename), options: [.atomic])
                
                // first read out the file to add this image in AddDefectImagesFile\(AuditCode)
                
                do{
                    var readString: String
                    readString = try NSString(contentsOfFile: ImagePathsfile, encoding: String.Encoding.utf8.rawValue) as String
                    
                    print("readAuditProgress = ",readString)
                    
                    /// here send readString to function to update values stored in AddDeffectImagesFile(AuditCode) File
                    
                    self.AddDeffectImagesInAuditCodeFile(readString, imgFileName: destinationFileName, destinationFileName:destinationFileName)
                    
                }
                catch let error as NSError{
                    print("There is an error while reading AuditProgress(AuditCode) File.",error.description)
                }
                
                
//                AddDeffectOfflineImageData.append(Imagefilename) //updateValue(Imagefilename, forKey: "fds")
//                
//                print("AddDeffectOfflineImageData =",AddDeffectOfflineImageData)
//                
//                do{
//                    try "\(AddDeffectOfflineImageData)".writeToFile(ImagePathsfile, atomically: true, encoding: NSUTF8StringEncoding)
//                    
//                    //Now save text data into the document directory.
//                    saveOfflineTextData(destinationFileName)
//                    
//                }
//                catch{
//                    print("threre may be an error in writing AddDeffectOfflineImageData.")
//                }
//                
//                
//                //Reading from JsonDictionary File stored in documents directory
//                do{
//                    var readString: String
//                    readString = try NSString(contentsOfFile: ImagePathsfile, encoding: NSUTF8StringEncoding) as String
//                    //                    print("readString=\(readString)")
////                    let readStringParsed = parseJSON(readString)
//                    print("readAddDefect_ImageString = ",readString)
//                }
//                catch let error as NSError{
//                    print("There is an error while reading from JsonDictionary File.",error.description)
//                    
//                }

            }
        }
      
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
//        //Reading from JsonDictionary File stored in documents directory
//        do{
//            var readString: String
//            readString = try NSString(contentsOfFile: ImagePathsfile, encoding: NSUTF8StringEncoding) as String
//            //                    print("readString=\(readString)")
//            let readStringParsed = parseJSON(readString)
//            print("readAddDefect_StringParsed = ",readStringParsed)
//        }
//        catch let error as NSError{
//            print("There is an error while reading from JsonDictionary File.",error.description)
//            
//        }
        
    }
    
    
    
    func AddDeffectImagesInAuditCodeFile(_ savedData: String?, imgFileName:String, destinationFileName:String!){
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        let jsonFilePath = getDocumentsDirectory().appendingPathComponent("AddDefectImagesFile\(self.AuditCodeLabel.text!).json")
        
        if savedData != nil && savedData != "" {
            
            var AddDeffectOfflineImageData:[String:String] = parseAddDefectImagesFile(savedData!)
            
             AddDeffectOfflineImageData.updateValue(imgFileName, forKey: destinationFileName)
//            AddDeffectOfflineImageData.append(imgFileName)
            
            // Now write into the AuditProgress(AuditCode) file
            
            if self.fileManager.fileExists(atPath: jsonFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AddDeffectOfflineImageData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: jsonFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    //Now save text data into the document directory.
                    saveOfflineTextData(destinationFileName)
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
            
        }else{
            
            var AddDeffectOfflineImageData = [String:String]()
            
            AddDeffectOfflineImageData.updateValue(imgFileName, forKey: destinationFileName)
//            AddDeffectOfflineImageData.append(imgFileName)
            
            // Now write into the AuditProgress(AuditCode) file
            
            if self.fileManager.fileExists(atPath: jsonFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AddDeffectOfflineImageData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: jsonFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    //Now save text data into the document directory.
                    saveOfflineTextData(destinationFileName)
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        //Reading from JsonDictionary File stored in documents directory
        do{
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath, encoding: String.Encoding.utf8.rawValue) as String
            
            let readStringParsed = parseAddDefectImagesFile(readString)
            print("readAddDefectImagesStringParsed = ",readStringParsed)
        }
        catch let error as NSError{
            print("There is an error while reading from JsonDictionary File.",error.description)
            
        }
        
    }
    
    
    
    
    func showNilError(){
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        if self.butotn != nil{
            self.butotn.isEnabled = true
            self.butotn.backgroundColor = UIColor.lightGray
        }
        let alertView = UIAlertController(title: "Cannot determin path" , message: "Press OK", preferredStyle: .alert)
        let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertView.addAction(ok_action)
        self.present(alertView, animated: true, completion: nil)
    }
    func uploadTextData(){
        
            // saving audit
            if self.PICK_DEFECT_TYPE.text != "DEFECT TYPE" && self.PICK_DEFECT_CODE.text != "DEFECT CODE" && self.PICK_DEFECT_CODE.text != "" && self.PICK_DEFECT_AREA.text != "DEFECT AREA" {
                //????
                self.endDate = Date()
                self.convertedEndDate = self.endDate.CustomDateTimeformat
                print(self.convertedEndDate)
                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCodeLabel.text!)&ReportId=\(self.ReportId)&DefectCode=\(self.DefectCodeID)&DefectArea=\(self.DefectedAreaID)&Nature=\(self.MajorORMinor)&SampleNo=\(self.SampleChecked.text!)&DateTime=\(self.convertedEndDate)&Remarks=\(self.commentsField.text!)"
                print("AddDefect-bodydata-> \(self.bodyData)")
                //            if success == "true"{
                self.performLoadDataRequestWithURL(self.getUrl())
        }
    }
    

    func saveOfflineTextData(_ destinationFileName:String!){
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        let jsonFilePath2 = documentsDirectoryPath.appendingPathComponent("AddDefectTextAuditCodeFile\(self.AuditCodeLabel.text!).json")
        // creating a AddDeffectTextFile.json file in the Documents folder
        
        if !fileManager.fileExists(atPath: jsonFilePath2.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath2.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("AddDefectTextAuditCodeFile.json File created ")
                
                // Now save it to FilesToSync.json file
                
                saveFileNameTo_FilesToSync("AddDefectTextAuditCodeFile\(self.AuditCodeLabel.text!).json")
                //(jsonFilePath2.absoluteString)

                
            } else {
                print("Couldn't create file for some reason")
            }
        }
        else {
            
            print("AddDefectTextAuditCodeFile.json File already exists")
        }
      
        
        
        
        // First check before saving audit
        
        if self.PICK_DEFECT_TYPE.text != "DEFECT TYPE" && self.PICK_DEFECT_CODE.text != "DEFECT CODE" && self.PICK_DEFECT_CODE.text != "" && self.PICK_DEFECT_AREA.text != "DEFECT AREA" {
            
            
            //First Read out the AddDefectTextAuditCodeFile
            
            do{
                var readString: String
                readString = try NSString(contentsOfFile: jsonFilePath2.absoluteString, encoding: String.Encoding.utf8.rawValue) as String
                
                print("readAddDefect_TextDataString = ",readString)
                
                /// here send readString to function to update values stored in AddDeffectText(AuditCode) File
                
                self.AddDeffectTextDataAuditCodeFile(readString, TextDataFilePath: jsonFilePath2.absoluteString, destinationFileName: destinationFileName)
                
            }
            catch let error as NSError{
                print("There is an error while reading AddDeffectText(AuditCode) File.",error.description)
            }
            
        }
    }
    
    func AddDeffectTextDataAuditCodeFile(_ savedTextData:String?, TextDataFilePath:String, destinationFileName:String!) -> Void {
        
        if savedTextData != nil && savedTextData != "" {
            
//            self.endDate = Date()
//            self.convertedEndDateTime = self.endDate.CustomDateTimeformat
            print(self.convertedEndDateTime)
            
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCodeLabel.text!)&ReportId=\(self.ReportId!)&DefectCode=\(self.DefectCodeID)&DefectArea=\(self.DefectedAreaID)&Nature=\(self.MajorORMinor!)&SampleNo=\(self.SampleChecked.text!)&DateTime=\(self.convertedEndDateTime)&Remarks=\(self.commentsField.text!)&Picture=\(destImageNametext)"
            
            print("AddDefect-Offline_bodydata-> \(self.bodyData)")
            
            
            var AddDeffectOfflineTextData:[String:String] = parseAddDefectTextDataJSON(savedTextData!)
            
            AddDeffectOfflineTextData.updateValue(bodyData, forKey: "AddDeffect_bodyData\(destinationFileName!)")
            AddDeffectOfflineTextData.updateValue("http://app.3-tree.com/quonda/save-defect.php", forKey: "AddDeffect_url")
            
            
            if self.fileManager.fileExists(atPath: TextDataFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AddDeffectOfflineTextData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: TextDataFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to AddDefectTextAuditCodeFile // File not exist")
                    print(error.description)
                }
            }
            
           
            
            // Reset all and dismiss current view
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.butotn.isEnabled = true
            self.butotn.backgroundColor = UIColor.lightGray
            
            self.savedParamPath = nil
            
            DefectsData0.removeAll()
            DefectsRateData0.removeAll()
            DefectsAreaData0.removeAll()
            
            self.dismiss(animated: false, completion: nil)
            
        }else{
            
            
//            self.endDate = Date()
//            self.convertedEndDateTime = self.endDate.CustomDateTimeformat
            print(self.convertedEndDateTime)
            
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCodeLabel.text!)&ReportId=\(self.ReportId!)&DefectCode=\(self.DefectCodeID)&DefectArea=\(self.DefectedAreaID)&Nature=\(self.MajorORMinor!)&SampleNo=\(self.SampleChecked.text!)&DateTime=\(self.convertedEndDateTime)&Remarks=\(self.commentsField.text!)&Picture=\(destImageNametext)"
            
            print("AddDefect-Offline_bodydata-> \(self.bodyData)")
            
            
            var AddDeffectOfflineTextData = [String:String]()
            
            AddDeffectOfflineTextData.updateValue(bodyData, forKey: "AddDeffect_bodyData\(destinationFileName!)")
            AddDeffectOfflineTextData.updateValue("http://app.3-tree.com/quonda/save-defect.php", forKey: "AddDeffect_url")
            
            
            if self.fileManager.fileExists(atPath: TextDataFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AddDeffectOfflineTextData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: TextDataFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
           
            
            // Reset all and dismiss current view
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.butotn.isEnabled = true
            self.butotn.backgroundColor = UIColor.lightGray
            
            self.savedParamPath = nil
            
            DefectsData0.removeAll()
            DefectsRateData0.removeAll()
            DefectsAreaData0.removeAll()
            
            self.dismiss(animated: false, completion: nil)
            

        }
        
        //Reading from JsonDictionary File stored in documents directory
        do{
            var readString: String
            readString = try NSString(contentsOfFile: TextDataFilePath, encoding: String.Encoding.utf8.rawValue) as String
            //                    print("readString=\(readString)")
            let readStringParsed = parseAddDefectTextDataJSON(readString)
            print("AddDefectText_readStringParsed = ",readStringParsed)
        }
        catch let error as NSError{
            print("There is an error while reading from AddDefectTextAuditCodeFile.",error.description)
            
        }

    }
    
    
    
    
    
//    func ListDir() {
////        let today = NSDate()
////        var todayString = today.CustomDateformat
//        print("AuditScheduleDate =")
//        print(AuditScheduleDate)
//        let scheduleDateString = defaults.objectForKey(self.AuditCodeLabel.text!)as! String  //= AuditScheduleDate //"2016-04-20"
//        print(scheduleDateString)
//        var parts = scheduleDateString.componentsSeparatedByString("-")
//        print(parts[0]) // year
//        print(parts[1]) // month
//        print(parts[2]) // day
//        self.session.list("/quonda") {
//            (resources, error) -> Void in
//            if resources == nil{
//               print("resources = nil")
//                
//                self.ListDir()
////                self.showNilError()
//            }
//            else{
//                
//                self.dirArray = resources!
//                
//                for item in self.dirArray{
//                    self.resource = item as! ResourceItem
//                    
//                    if self.resource.type == .Directory && self.resource.name == parts[0]{
//                        self.paramPath = self.paramPath+"/"+"\(parts[0])"
//                        
//                        self.session.list("/quonda/"+"\(parts[0])", completionHandler: { (resources, error) -> Void in
//                            if resources == nil{
//                                self.showNilError()
//                            }
//                            else{
//                                
//                                self.monArray = resources!
//                                
//                                for item in self.monArray{
//                                    self.resource = item as! ResourceItem
//                                    
//                                    if self.resource.type == .Directory && self.resource.name == parts[1]{
//                                        self.monthResource = self.resource.name
//                                        self.paramPath = self.paramPath+"/"+"\(parts[1])"
//                                        break
//                                    }
//                                }
//                                if self.monthResource == nil{
//                                    self.session.createDirectory("/quonda/"+"\(parts[0])"+"/"+"\(parts[1])") {
//                                        (result, error) -> Void in
//                                        print("Create directory with result:\n\(result), error: \(error)")
//                                        if result == true{
//                                            self.monthResource = parts[1]
//                                            self.paramPath = self.paramPath+"/"+"\(parts[1])"
//                                        }//still resolve error handling in case of error
//                                    }
//                                }
//                                if self.monthResource == parts[1]{
//                                    
//                                    self.session.list("/quonda/"+"\(parts[0])"+"/"+"\(parts[1])", completionHandler: { (resources, error) -> Void in
//                                        if resources == nil{
//                                            self.showNilError()
//                                        }
//                                        else{
//                                            
//                                            self.dayArray = resources!   // Array of days in a month
//                                            
//                                            for item in self.dayArray{
//                                                self.resource = item as! ResourceItem
//                                                
//                                                if self.resource.type == .Directory && self.resource.name == parts[2]{
//                                                    print("day name is here..")
//                                                    print(self.resource.name)
//                                                    self.dayResource = parts[2]
//                                                    self.paramPath = self.paramPath+"/"+"\(parts[2])"
//                                                    break
//                                                }
//                                            }
//                                            if self.dayResource == nil{
//                                                self.session.createDirectory("/quonda/"+"\(parts[0])"+"/"+"\(parts[1])"+"/"+"\(parts[2])") {
//                                                    (result, error) -> Void in
//                                                    print("day result=\(result)")
//                                                    print("day error=\(error)")
//
//                                                    if result == true{
//                                                        
//                                                        self.dayResource = parts[2]
//                                                        self.paramPath = self.paramPath+"/"+"\(parts[2])"
//                                                        
//                                                    }
//                                                    else{ // case of error if day dir cannot create
//                                                        self.session.createDirectory("/quonda/"+"\(parts[0])"+"/"+"\(parts[1])"+"/"+"\(parts[2])") {
//                                                            (result, error) -> Void in
//                                                            print("day result=\(result)")
//                                                            print("day error=\(error)")
//                                                            
//                                                            if result == true{
//                                                                
//                                                                self.dayResource = parts[2]
//                                                                self.paramPath = self.paramPath+"/"+"\(parts[2])"
//                                                                
//                                                            }
//                                                        }
//                                                    }
//                                                }
//                                            }
//                                            else if self.dayResource == parts[2]{
//                                                print(self.dayResource)
//                                                // here we've got a full dayPath for saving our captured imageo onto the FTP Server.
//                                                // which is paramPath string.
//                                            }
//                                            
//                                            print("end")
//                                        } // (else) if quonda/parts[0]/parts[1] != nil
//                                }) // end od month dir listing
//                                }
//                            } // (else) if quonda/parts[0] != nil
//                        }) // end of year dir listing
//                        break
//                    }
//                    print("- - - -")
//                }
//            } //(else) if resources in quonda dir != nil
//        } // end of quonda dir
//    }

    
//    func ImageUpload2() {
//        if let URL = NSBundle.mainBundle().URLForResource("marker", withExtension: "png") {
//            let path = "/quonda\(NSUUID().UUIDString).png"
//            self.session.upload(URL, path: path) {
//                (result, error) -> Void in
//                print("Upload file with result:\n\(result), error: \(error)\n\n")
//            }
//        }
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        print(PICK_DEFECT_TYPE.text)
        print(PICK_DEFECT_TYPE_TEXT)
        print(DefectID)
        
        
        if !DefectsData.isEmpty && !DefectsDataID.isEmpty{
          
            PICK_DEFECT_TYPE.text = ""
            self.PICK_DEFECT_TYPE_TEXT = ""
            self.DefectID = ""
            
            for id in DefectsDataID{
                print(id.1)
                print("-------")
                //            list = list+d.1+","
                //                self.Country.tag = Int(id.1)!
                self.DefectID = id.1
            }
            for d in DefectsData{
                print(d.1)
                print("-------")
                //            list = list+d.1+","
                self.PICK_DEFECT_TYPE_TEXT = d.1
                PICK_DEFECT_TYPE.text = d.1
                
                
                if timesDefectTypes == true{
                    
                    if PICK_DEFECT_TYPE.text != Previous_PICK_DEFECT_TYPE && self.PICK_DEFECT_TYPE_TEXT != Previous_PICK_DEFECT_TYPE {
                        print("moving a head.")
                        PICK_DEFECT_CODE.text = ""
                        self.PICK_DEFECT_CODE_TEXT = ""
                        DefectsRateData0.removeAll()  // i think it has no effect
                    }
                }

                // saving Previous_PICK_DEFECT_TYPE_TEXT and Previous_PICK_DEFECT_TYPE for future use.
                
                Previous_PICK_DEFECT_TYPE_TEXT = self.PICK_DEFECT_TYPE_TEXT
                Previous_PICK_DEFECT_TYPE = PICK_DEFECT_TYPE.text!
            }
            DefectsDataID.removeAll()
            DefectsData.removeAll()
        }
    //- - - - - - - - - - - - - - - - - - - -
        print(PICK_DEFECT_CODE.text)
        print(PICK_DEFECT_CODE_TEXT)
        print(DefectCodeID)

        if !DefectsRateData.isEmpty && !DefectsRateDataID.isEmpty{
            PICK_DEFECT_CODE.text = ""
            self.PICK_DEFECT_CODE_TEXT = ""
            self.DefectCodeID = ""
            //            self.Country.tag = 0000
            for id in DefectsRateDataID{
                print(id.1)
                print("-------")
                //            list = list+d.1+","
                //                self.Country.tag = Int(id.1)!
                self.DefectCodeID = id.1
            }
            for d in DefectsRateData{
                print(d.1)
                print("-------")
                //            list = list+d.1+","
                self.PICK_DEFECT_CODE_TEXT = d.1
                PICK_DEFECT_CODE.text = d.1
            }
            DefectsRateDataID.removeAll()
            DefectsRateData.removeAll()
        }
        //- - - - - - - - - - - - - - - - - - - -
        print(PICK_DEFECT_AREA.text)
        print(PICK_DEFECT_AREA_TEXT)
        print(DefectedAreaID)
        if !AreaData.isEmpty && !AreaDataID.isEmpty{
            PICK_DEFECT_AREA.text = ""
            self.PICK_DEFECT_AREA_TEXT = ""
            self.DefectedAreaID = ""
            for id in AreaDataID{
                print(id.1)
                print("-------")
                //            list = list+d.1+","
                //                self.Country.tag = Int(id.1)!
                self.DefectedAreaID = id.1
            }
            for d in AreaData{
                print(d.1)
                print("-------")
                self.PICK_DEFECT_AREA_TEXT = d.1
                PICK_DEFECT_AREA.text = d.1
            }
            AreaDataID.removeAll()
            AreaData.removeAll()
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.endDate = Date()
        convertedEndDateTime = self.endDate.CustomDateTimeformat
        print(convertedEndDateTime)
        // - - - - add notifications to current view    keyboardWillShow:
        NotificationCenter.default.addObserver(self, selector: #selector(AddDefect.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.addObserver(self, selector: #selector(AddDefect.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
    
        
        // - - - -
        self.PICK_DEFECT_TYPE.text = "DEFECT TYPE"
        self.PICK_DEFECT_CODE.text = "DEFECT CODE"
        self.PICK_DEFECT_AREA.text = "DEFECT AREA"
       commentsField.delegate = self
        
        scrollView.contentSize.height = 600
        
        commentsField.layer.borderWidth = 1
        commentsField.layer.borderColor = UIColor.lightGray.cgColor
        
        // clear all Defect Types, Defect Rates and Defect Areas
        DefectsData0.removeAll()
        DefectsRateData0.removeAll()
        DefectsAreaData0.removeAll()
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        //- - - - - - - - -
        // create FTP Session configuration
//        var configuration = SessionConfiguration()
//        configuration.host = "192.163.245.220"
//        configuration.username = "ali"
//        configuration.password = "AAutpksa1546"
//        self.session = Session(configuration: configuration)
//        
//        if Reachability.isConnectedToNetwork(){
//            
//            ListDir()
//        }
    
        sleep(UInt32(1.0))
        // - - - - - - -
        let delay = 1.0 * Double(NSEC_PER_SEC)  // `NSEC_PER_SEC` means one billion(100000000) and It converts 0.5 nano seconds to seconds
        let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time) {
            self.sampleImageTapped()
        }
      PICK_DEFECT_TYPE.sizeToFit()
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
        
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        //        self.scrollView.contentSize.height = 10000
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Selected Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)


        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(AddDefect.sampleImageTapped))
        sampleImage.isUserInteractionEnabled = true
        sampleImage.addGestureRecognizer(tapGestureRecognizer)
        
        
        self.AuditCodeLabel.text = AuditCodein
        self.SampleChecked.text = SampleCheckedin
        self.SampleSize.text = SampleSizein
        self.AuditStageLabel.text = AuditStagein
        self.AuditStageLabel.backgroundColor = AuditStageColorin

        
        let libraryDirectoryPath = URL(string: libraryDirectoryPathString)!
        jsonFilePath = libraryDirectoryPath.appendingPathComponent("myCapturedImage.jpg")
       
        // creating a .jpg file in the Library Directory
        if !fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("File created ")
                
            } else {
                print("Couldn't create file for some reason")
            }
        } else {
            print("File already exists")
        }
        
        commentsField.isHidden = true
        if(self.ReportId == "28"){
            commentsField.isHidden = false
            saveTopConstraint.constant = 23
        }
        else{
            commentsField.isHidden = true
            saveTopConstraint.constant = -23

        }
    }
    
    
    // adjust the screen size when keyboard appears and hides
    
    func keyboardWillHide(_ sender: Notification) {
        let userInfo: [AnyHashable: Any] = sender.userInfo!
        let keyboardSize: CGSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue.size
        self.view.frame.origin.y += keyboardSize.height
    }
    
    func keyboardWillShow(_ sender: Notification) {
        let userInfo: [AnyHashable: Any] = sender.userInfo!
        let keyboardSize: CGSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue.size
        let offset: CGSize = (userInfo[UIKeyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size
        
        if keyboardSize.height == offset.height {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y -= keyboardSize.height
            })
        } else {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y += keyboardSize.height - offset.height
            })
        }
    }

    
    func sampleImageTapped(){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileLibraryDirectory(_ filename: String) -> String {
        
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL.path
        
    }
    func loadImageFromPath(_ path: String) -> UIImage? {
        
        let image = UIImage(contentsOfFile: path)
        
        if image == nil {
            
            print("missing image at: \(path)")
        }
        print("Loading image from path: \(path)") // this is just for you to see the path in case you want to go to the directory, using Finder.
        return image
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: false, completion: nil)
        self.dismiss(animated: false, completion: nil)

    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
       
        imagePicker.dismiss(animated: true, completion: nil)
       
        DispatchQueue.main.async { () -> Void in
            self.capturePhoto = info[UIImagePickerControllerOriginalImage] as? UIImage
//            print(self.capturePhoto)
            if let capturePhoto = self.capturePhoto{
                self.sampleImage.image = capturePhoto
                
                
                if self.fileManager.fileExists(atPath: self.jsonFilePath.absoluteString, isDirectory: &self.isDirectory) {
                    do{
                        let resizedImage = capturePhoto.resize(0.45)
                       let jpgImageData = UIImageJPEGRepresentation(resizedImage, 0.5)
                        try jpgImageData?.write(to: URL(fileURLWithPath: self.jsonFilePath.path), options: .noFileProtection)
                        print("data written successfully!")
                    } catch let error as NSError {
                        print("Image couldn't written to file ")
                        print(error.description)
                        // cannot save image in memory
                    }
                }

//                self.performSegueWithIdentifier("major", sender: capturePhoto)
            }
            //else NO image can be captured.
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        
//        sampleImage.image = recievedImg
        BtnDefectType.layer.borderWidth = 1
        BtnDefectType.layer.borderColor = UIColor.lightGray.cgColor
        BtnDefectCode.layer.borderWidth = 1
        BtnDefectCode.layer.borderColor = UIColor.lightGray.cgColor
        BtnDefectArea.layer.borderWidth = 1
        BtnDefectArea.layer.borderColor = UIColor.lightGray.cgColor
//        ADD.layer.cornerRadius = 10.0
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
            defaults.removeObject(forKey: "userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
//        case 4:
//            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
//                //                self.dismissViewControllerAnimated(true, completion: nil)
//                presentViewController(resultController, animated: true, completion: nil)
//            }
//            
//            let selectedImage:UIImage = UIImage(named: "Selected QAReport")!
//            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
//            
//            break
        case 5:
            DefectsData0.removeAll()
            DefectsRateData0.removeAll()
            DefectsAreaData0.removeAll()
            self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)

            //self.dismiss(animated: true, completion: nil)
//            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("5") as? AuditorModeViewController {
//                self.dismissViewControllerAnimated(true, completion: nil)
//                presentViewController(resultController, animated: true, completion: nil)
//            }
//            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
//            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break

        default:
            break
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "DefectTypeSegue"{
            let navigationVC = segue.destination as! UINavigationController
            let destinationVC = navigationVC.viewControllers.first as! DefectTypes
            
            destinationVC.ReportId = sender as! String
            print("defectType segue is called")
        }
        if DefectID != "" && PICK_DEFECT_TYPE.text != ""{ //PICK_DEFECT_TYPE_TEXT != ""{
            
            if segue.identifier == "DefectRateSegue"{
                let navigationVC = segue.destination as! UINavigationController
                let destinationVC = navigationVC.viewControllers.first as! DefectRate
                
                destinationVC.DefectID_Received = DefectID
                destinationVC.ReportId = sender as! String
                
            }
        }
        if DefectID != "" && PICK_DEFECT_TYPE.text != ""{ //PICK_DEFECT_TYPE_TEXT != ""{
            
            if segue.identifier == "defectArea"{
                let navigationVC = segue.destination as! UINavigationController
                let destinationVC = navigationVC.viewControllers.first as! PickArea
                
                destinationVC.DefectID_Received = DefectID
                destinationVC.ReportId = sender as! String
                
            }
        }

        if segue.identifier == "saved"{
            print("saved Button taped")
            self.dismiss(animated: false, completion: nil)
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

    func getUrl() -> URL {
        let toEscape = "http://app.3-tree.com/quonda/save-defect.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
    
    func performLoadDataRequestWithURL(_ url: URL) -> String? {
        
        //        self.bodyData = self.bodyData!+"&PageId=\(currentPage)"
        //        self.bodyData = "User=\(defaults.objectForKey("userid")as! String)&AuditCode=&Date=2016-04-20"
        
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = self.bodyData!.data(using: String.Encoding.utf8)
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main){
            response, data, error in
            print("\(response)")
            if (error != nil) {
                print(error)
                UIApplication.shared.isNetworkActivityIndicatorVisible = false

                self.butotn.isEnabled = true
                self.butotn.backgroundColor = UIColor.lightGray
                
                let alertView = UIAlertController(title: "unKnown Error Found" , message: "\(error?.localizedDescription)Press OK", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    self.sampleImage.isUserInteractionEnabled = true
                    self.BtnDefectType.isUserInteractionEnabled = true
                    self.BtnDefectCode.isUserInteractionEnabled = true
                    self.BtnDefectArea.isUserInteractionEnabled = true
                    self.commentsField.isUserInteractionEnabled = true
                   
                })

                alertView.addAction(ok_action)
                self.present(alertView, animated: true, completion: nil)
            }
            
            if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                // - - - - - - write this JSON data to file- - - - - - - -
                //??????
                
                print(json)
                print(self.bodyData)
                self.dictionary = self.parseJSON(json)
                
                print("\(self.dictionary)")
                if("\(self.dictionary["Status"]!)" == "OK"){
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.butotn.isEnabled = true
                    self.butotn.backgroundColor = UIColor.lightGray
                   
                    self.savedParamPath = nil
                    
                    DefectsData0.removeAll()
                    DefectsRateData0.removeAll()
                    DefectsAreaData0.removeAll()
                    DefectAdded.append(self.AuditCodein)
                    self.dismiss(animated: false, completion: nil)
                    
                }else{
                    let alertView = UIAlertController(title: "cannot save" , message: "Press OK", preferredStyle: .alert)
                    let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                        
                        self.sampleImage.isUserInteractionEnabled = true
                        self.BtnDefectType.isUserInteractionEnabled = true
                        self.BtnDefectCode.isUserInteractionEnabled = true
                        self.BtnDefectArea.isUserInteractionEnabled = true
                        self.commentsField.isUserInteractionEnabled = true
                        
                    })

                    alertView.addAction(ok_action)
                    self.present(alertView, animated: true, completion: nil)
                }
                
            }//if data != nil
            else{
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.butotn.isEnabled = true
                self.butotn.backgroundColor = UIColor.lightGray
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                let alertView = UIAlertController(title: "No Data Found" , message: "Press OK", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    self.sampleImage.isUserInteractionEnabled = true
                    self.BtnDefectType.isUserInteractionEnabled = true
                    self.BtnDefectCode.isUserInteractionEnabled = true
                    self.BtnDefectArea.isUserInteractionEnabled = true
                    self.commentsField.isUserInteractionEnabled = true
                    
                })

                alertView.addAction(ok_action)
                self.present(alertView, animated: true, completion: nil)
                print("return data is nil")
            }
        }
        return json
    }
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    func parseAddDefectImagesFile(_ jsonString: String) -> [String:String] {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:String] {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    func parseAddDefectTextDataJSON(_ jsonString: String) -> [String:String] {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:String] {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }

    
    func randomStringWithLength(_ len: Int) -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for i in 0 ..< len {
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString
    }
    
    

} // end of Add Defect class

extension UIImage {
    func resize(_ scale:CGFloat)-> UIImage {
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: size.width*scale, height: size.height*scale)))
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
    func resizeToWidth(_ width:CGFloat)-> UIImage {
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
}








