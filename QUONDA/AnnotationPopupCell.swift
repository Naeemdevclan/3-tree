//
//  AnnotationPopupCell.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 14/04/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class AnnotationPopupCell: UITableViewCell {

    @IBOutlet weak var AuditResult: UIView!
    @IBOutlet weak var AuditCode: UILabel!
    @IBOutlet weak var Auditor: UILabel!
    @IBOutlet weak var Po: UILabel!
    
    @IBOutlet weak var AuditResultLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
