//
//  AppDelegate.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 25/02/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

var syncObject:syncService!

import UIKit
import UserNotifications
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?
    
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    
//    let locationManager = CLLocationManager()
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Override point for customization after application launch.
      application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil))
        
        if Reachability.isConnectedToNetwork(){
        
            // send data to server in FIFO
           //application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil))
           // setupNotificationSettings()
            self.saveDeviceUniqueToken()
            print("Internet is OK \n sync service started")
            syncObject = syncService()
            
            if #available(iOS 10.0, *)
            {
                //Seeking permission of the user to display app notifications
                UNUserNotificationCenter.current().requestAuthorization(options:       [.alert,.sound,.badge], completionHandler: {didAllow,Error in })
                UNUserNotificationCenter.current().delegate = self
                
            }
            // send Auditor Location to the server
//            sendAuditorLocation()

            
            //NSTimer.scheduledTimerWithTimeInterval(1800.0, target: self, selector: #selector(self.sendAuditorLocation), userInfo: nil, repeats: true)
//            if let identifierForVendor = UIDevice.current.identifierForVendor {
//                print(identifierForVendor.uuidString)
//            }
//           print(UIDevice.current.identifierForVendor!.uuidString)

        
        }
        UIApplication.shared.applicationIconBadgeNumber = 0
        print("application has finished launching.")
        
        return true
    }
    func setupNotificationSettings() {
        let notificationSettings: UIUserNotificationSettings! = UIApplication.shared.currentUserNotificationSettings
        
        if (notificationSettings.types == .none || notificationSettings.types == .alert || notificationSettings.types == .sound){
            // Specify the notification types.
            let notificationTypes: UIUserNotificationType = .alert
            
            
            // Specify the notification actions.
            let justInformAction = UIMutableUserNotificationAction()
            justInformAction.identifier = "justInform"
            justInformAction.title = "OK, got it"
            justInformAction.activationMode = UIUserNotificationActivationMode.background
            justInformAction.isDestructive = false
            justInformAction.isAuthenticationRequired = false
            
            var modifyListAction = UIMutableUserNotificationAction()
            modifyListAction.identifier = "editList"
            modifyListAction.title = "Edit list"
            modifyListAction.activationMode = UIUserNotificationActivationMode.foreground
            modifyListAction.isDestructive = false
            modifyListAction.isAuthenticationRequired = true
            
            var trashAction = UIMutableUserNotificationAction()
            trashAction.identifier = "trashAction"
            trashAction.title = "Delete list"
            trashAction.activationMode = UIUserNotificationActivationMode.background
            trashAction.isDestructive = true
            trashAction.isAuthenticationRequired = true
            
            let actionsArray = NSArray(objects: justInformAction, modifyListAction, trashAction)
            let actionsArrayMinimal = NSArray(objects: trashAction, modifyListAction)
            
            // Specify the category related to the above actions.
            let shoppingListReminderCategory = UIMutableUserNotificationCategory()
            shoppingListReminderCategory.identifier = "shoppingListReminderCategory"
            shoppingListReminderCategory.setActions(actionsArray as? [UIUserNotificationAction], for: UIUserNotificationActionContext.default)
            shoppingListReminderCategory.setActions(actionsArrayMinimal as? [UIUserNotificationAction], for: UIUserNotificationActionContext.minimal)
            
            
            let categoriesForSettings = NSSet(objects: shoppingListReminderCategory)
            
            
            // Register the notification settings.
            let newNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: categoriesForSettings as? Set<UIUserNotificationCategory>)
            UIApplication.shared.registerUserNotificationSettings(newNotificationSettings)
        }
    }
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {

        let alert = UIAlertView(title: notification.alertTitle!, message: notification.alertBody!, delegate: nil, cancelButtonTitle: "OK")
                    alert.show()

        NotificationCenter.default.post(name: Notification.Name(rawValue: "TodoListShouldRefresh"), object: self)
    }
    
//    func sendAuditorLocation() {
//        
//        self.locationManager.delegate = self
//        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
//        self.locationManager.requestWhenInUseAuthorization()
//        self.locationManager.startUpdatingLocation()
//        
//        print("loaction config called")
//    }
//    
//    
//    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        
//        print("coordinate =",locations.first?.coordinate)
//        
//        var latitudeLabel = String(locations.first!.coordinate.latitude)
//        var longitudeLabel = String(locations.first!.coordinate.longitude)
//        
//        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error) -> Void in
//            if (error != nil) {
//                print("ERROR:" + error!.localizedDescription)
//                return
//            }
//            print("placemarks!.count = ",placemarks!.count)
//            
//            if placemarks!.count > 0 {
//                let pm = placemarks![0]
//                self.saveLatitudeLongitude(pm)
//                print("called after saveLatitude")
//            } else {
//                print("Problem with the data received from geocoder")
//            }
//            
//        })
//    }
//    
//    
//    
//    func saveLatitudeLongitude(placemark: CLPlacemark) {
//        
//        var latitudeLabel:String = String(placemark.location!.coordinate.latitude)
//        var longitudeLabel:String = String(placemark.location!.coordinate.longitude)
//        //        let lat = String(placemark.location!.coordinate.latitude)
//        print("lat =", latitudeLabel)
//        
//        var bodyData = "User=\(defaults.objectForKey("userid")as! String)&Latitude=\(latitudeLabel)&Longitude=\(longitudeLabel)"
//        print("bodydata for location =",bodyData)
//        
//        let request: NSMutableURLRequest = NSMutableURLRequest(URL: NSURL(string: "http://portal.3-tree.com/api/android/location.php")!)
//        var json = ""
//        request.HTTPMethod = "POST"
//        request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding)
//        
//        let session = NSURLSession.sharedSession()
//        
//        let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
//            
//            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
//                print("error")
//                return
//            }
//            if (error != nil) {
//                print(error)
//                
//            }
//            else if data != nil {
//                json = NSString(data: data!, encoding: NSUTF8StringEncoding) as! String
//                
//                print(json)
//                
//                
//                var dictionary:NSDictionary = self.parseJSON(json)
//                
//                print("information when Schedule btn clicked = \(dictionary)")
//                
//                if("\(dictionary["Status"]!)" == "OK"){
//                    
//                    print("Successfuly sent Coordinates \(dictionary["Message"]!)")
//                    
//                }else{
//                    print("There is an error while sending Coordinates to the server.")
//                }
//                
//            }//if data != nil
//            else{
//                print("return data is nil")
//            }
//            
//        }
//        
//        task.resume()
//        
//        
//        self.locationManager.stopUpdatingLocation()
//    }
//    
//    
//    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
//        print("Error:" + error.localizedDescription)
//    }
    

//    
//    func reachabilityChanged(notification: NSNotification) {
//        if reach!.isReachableViaWiFi() || reach!.isReachableViaWWAN() {
//            print("Service avalaible!!!")
//        } else {
//            print("No service avalaible!!!")
//        }
//    }
    
    func saveDeviceUniqueToken(){
        
        
        var DeviceId = ""
        
        if let uniqueId = KeychainService.loadPassword(){
           print(uniqueId as String)
        }else{
            if let identifierForVendor = UIDevice.current.identifierForVendor {
                DeviceId = identifierForVendor.uuidString
                print(DeviceId)
                KeychainService.savePassword(token: DeviceId as String)

            }

        }
 // password = "Pa55worD"
    }

    func applicationWillResignActive(_ application: UIApplication) {
        self.window?.endEditing(true)

        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        self.window?.endEditing(true)
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
       // NotificationCenter.default.post(name: Notification.Name(rawValue: "TodoListShouldRefresh"), object: self)

    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    //To display notifications when app is running  inforeground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
    }
    func scheduleNotifications(){
        if #available(iOS 10.0, *)
        {
            //Setting content of the notification
            let content = UNMutableNotificationContent()
            content.title = "hello"
            content.body = "notification pooped out"
            //Setting time for notification trigger
            let date = Date(timeIntervalSinceNow: 2)
            let dateCompenents = Calendar.current.dateComponents([.year,.month,.day                    ,.hour,.minute,.second], from: date)
            
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateCompenents, repeats: false)
            //Adding Request
            let request = UNNotificationRequest(identifier: "timerdone", content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        }
    }

}

