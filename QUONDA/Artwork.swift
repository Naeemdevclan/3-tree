//
//  Artwork.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 07/04/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import Foundation
import MapKit

class Artwork: NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let discipline: AnyObject
    let coordinate: CLLocationCoordinate2D
    var imageName: String!

    
    init(title: String, locationName: String, discipline: AnyObject, coordinate: CLLocationCoordinate2D, imageName: String!) {
        self.title = title
        let subtitle_charCount = locationName.characters.count
        self.locationName = locationName
        self.discipline = discipline
        self.coordinate = coordinate
        self.imageName = imageName
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
}
