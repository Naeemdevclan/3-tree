//
//  AuditCommentsViewController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 03/05/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//


//var AuditCommentsOfflineData = [String:String]()

import UIKit
import MapKit
class AuditCommentsVC: UIViewController, UITabBarDelegate, UITextViewDelegate, UITextFieldDelegate, SavingViewControllerDelegate,SavingHybridDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIScrollViewDelegate,CLLocationManagerDelegate {

    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!

    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    
    
    
    @IBOutlet weak var AuditCode: UILabel!
    @IBOutlet weak var AuditStage: UILabel!
    
    
//    @IBOutlet weak var ShipQuantity: UITextField!

//    @IBOutlet weak var ReScreenQuantity: UITextField!
    
    
//    @IBOutlet weak var Status: UISwitch!
//    var statusActive = false
    var AuditResult: String!
    var jsonFilePath:URL!
    // let libraryDirectoryPathString = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true).first!
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    

//    @IBOutlet weak var ProductionStatus: UITextField!
    
//    @IBOutlet weak var Comments: UITextView!
    
//    @IBOutlet weak var finishBtn: UIButton!
   
    var dictionary:NSDictionary!
    
    var MutableDictionary:NSMutableDictionary!
    
    var AuditCodein: String!
    var AuditStagein: String!
    var AuditStageColorin : UIColor!
    
    // use for show/Hide Comments views
    
    var ReportID_against_current_audit: String!
    
    // variable to recieve ArrayOfDefectsLogged assigned while adding defects
    
    var arrayOfDefectsLoggedRecieved:[((Color:UIColor, ID:String),(DefectType:String,DefectCode:String))]? //[(DefectType:String,DefectCode:String)]?
    
    // outmostScrollView
    @IBOutlet weak var outmostScrollView: UIScrollView!
    
    // outmostStackView
    @IBOutlet weak var outmostStackView: UIStackView!
    
    // 2nd view Outlets in case of ReportType Arcadia.
    
    var placeholderLabel:UILabel!
    
    var capturePhoto: UIImage? = nil
    var packageImage: UIImage? = nil
    var labImage: UIImage? = nil
    var imagePicker: UIImagePickerController!
    
    @IBOutlet weak var ApprovedSample: UISwitch!
    @IBOutlet weak var ApprovedTrim: UISwitch!
    
    var ApprovedSampleValue:String?
    var ApprovedTrimValue:String?
    
//    @IBOutlet weak var commentsView_Arcadia: UIStackView!
    
//    @IBOutlet weak var ScrollView_Arcadia: UIScrollView!
    
    @IBOutlet weak var Comments_Arcadia: UITextView!
    
    @IBOutlet weak var Qty_of_Lots_TextField: UITextField!
    
    @IBOutlet weak var Qty_per_Lot_TextField: UITextField!
    
    @IBOutlet weak var Knitted_TextField: UITextField!
    @IBOutlet weak var Dyed_TextField: UITextField!
    
    @IBOutlet weak var Cutting_TextField: UITextField!
    
    @IBOutlet weak var Sewing_TextField: UITextField!
 
    @IBOutlet weak var Finishing_TextField: UITextField!
    
    @IBOutlet weak var Packed_TextField: UITextField!
    
     // 2nd view Outlets in case of ReportType Hybrid
    @IBOutlet weak var ShipQuantity3: UITextField!
    @IBOutlet weak var ShipDate: UIButton!
    @IBOutlet weak var AssortmentCartonQty: UITextField!
    @IBOutlet weak var AssortmentCartonSize: UITextField!
    
    @IBOutlet weak var SolidSizeQty: UITextField!
    @IBOutlet weak var SolidSizeType: UIButton!
//    @IBOutlet weak var SolidSizeQty: UIButton!
    @IBOutlet weak var WorkmanshipResult: UIButton!
    
    // 2nd view Outlets in case of ReportType Controlist.
    @IBOutlet weak var ShipQuantity2: UITextField!
    @IBOutlet weak var ReScreenQuantity2: UITextField!
    @IBOutlet weak var TotalCartons: UITextField!
    @IBOutlet weak var NoOfShippedCartons: UITextField!
    @IBOutlet weak var NoOfCartonsInspected: UITextField!
//    @IBOutlet weak var PiecesAvailableForInspection: UITextField!
    
    // UIViews
    // 2nd view Outlets in case of Hybrid Report Type.

    @IBOutlet weak var HybridCommentView: UIView!

    // common view between controlist and arcadia
    
    @IBOutlet weak var commonView_Arcadia_Controlist: UIView!
    
    // Outlets in case of Managing Views between Arcadia/Controlist
    
    @IBOutlet weak var ArcadiaView1: UIView!
    
    @IBOutlet weak var ControlistView1: UIView!
    
    @IBOutlet weak var ArcadiaView2: UIView!
    
    // Array of all inputViews in view of respective ReportID
    
    var arrayOfInputViews:[UITextField]!
    
    
    // -x?x?x?
    @IBOutlet weak var InspectionStatus_TextField: UITextField!
    @IBOutlet weak var EstimatedFinalDate: UIButton!
    
    @IBOutlet weak var AuditResultBtn: UIButton!
    
    // Audit Result View
    
    @IBOutlet weak var AuditResultView: UIView!
    
    
    // WorkmanshipResult variable
    var workmanshipResultValue:String!
    
    
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    var created:Bool!
    var paramPath = "/quonda"
    var otherImagesType:String!
    var savedParamPath:String!

    var sampleImage = UIImageView()
    
   // let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    
    var sampleChecked : String!
    var defectRatevalue : String!
    
    @IBOutlet weak var viewImgV: UIImageView!
    @IBOutlet weak var ViewUploadImages: UIView!
    @IBOutlet weak var mgfHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mgfBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var packingBtn: UIButton!
    @IBOutlet weak var LabBtn: UIButton!
    @IBOutlet weak var MiscBtn: UIButton!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        outmostScrollView.contentSize.height = outmostStackView.frame.height
        
        // - - - - -
        
//        ScrollView_Arcadia
//        commentsView_Arcadia
        
//        OuterMostScroller.contentSize.height = OuterMostStacker.frame.height //+20
    }

    
    
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(AuditCommentsVC.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        // Now add toolbar to each of the keyboard in textfield
        self.Qty_of_Lots_TextField.inputAccessoryView = doneToolbar
        self.Qty_per_Lot_TextField.inputAccessoryView = doneToolbar
        self.Knitted_TextField.inputAccessoryView = doneToolbar
        self.Dyed_TextField.inputAccessoryView = doneToolbar
        self.Cutting_TextField.inputAccessoryView = doneToolbar
        self.Sewing_TextField.inputAccessoryView = doneToolbar
        self.Finishing_TextField.inputAccessoryView = doneToolbar
        self.Packed_TextField.inputAccessoryView = doneToolbar
        
    }
    
    func doneButtonAction()
    {
        self.Qty_of_Lots_TextField.resignFirstResponder()
        self.Qty_per_Lot_TextField.resignFirstResponder()
        self.Knitted_TextField.resignFirstResponder()
        self.Dyed_TextField.resignFirstResponder()
        self.Cutting_TextField.resignFirstResponder()
        self.Sewing_TextField.resignFirstResponder()
        self.Finishing_TextField.resignFirstResponder()
        self.Packed_TextField.resignFirstResponder()
        
        
        self.Qty_of_Lots_TextField.isUserInteractionEnabled = true
        self.Qty_per_Lot_TextField.isUserInteractionEnabled = true
        self.InspectionStatus_TextField.isUserInteractionEnabled = true
        self.Knitted_TextField.isUserInteractionEnabled = true
        self.Dyed_TextField.isUserInteractionEnabled = true
        self.Cutting_TextField.isUserInteractionEnabled = true
        self.Sewing_TextField.isUserInteractionEnabled = true
        self.Finishing_TextField.isUserInteractionEnabled = true
        self.Packed_TextField.isUserInteractionEnabled = true
        
        self.Comments_Arcadia.isUserInteractionEnabled = true
        
        self.EstimatedFinalDate.isUserInteractionEnabled = true
        
        self.AuditResultBtn.isUserInteractionEnabled = true
        
    }
    
  
    func sampleImageTapped(){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        
        present(imagePicker, animated: true, completion: nil)
    }
    func samplePhotoLibImageTapped(){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .savedPhotosAlbum
        
        present(imagePicker, animated: true, completion: nil)
    }
    @IBAction func cancelClicked(_ sender: UIButton) {
        mgfHeightConstraint.constant = 100
       // mgfBottomConstraint.constant = 565
        self.ViewUploadImages.isHidden = true
        
    }
    @IBAction func saveAttachmentsClicked(_ sender: UIButton) {
        mgfHeightConstraint.constant = 120
        //mgfBottomConstraint.constant = 565
        
        self.viewImgV.image = nil
        
        if self.fileManager.fileExists(atPath: self.jsonFilePath.absoluteString, isDirectory: &self.isDirectory) {
            do{
                let resizedImage = self.sampleImage.image?.resize(0.45)
                let jpgImageData = UIImageJPEGRepresentation(resizedImage!,0.80)
                try jpgImageData?.write(to: URL(fileURLWithPath: self.jsonFilePath.path), options: .noFileProtection)
                print("data written successfully!")
                self.SAVE_pressed()
                
            } catch let error as NSError {
                print("Image couldn't written to file ")
                print(error.description)
                // cannot save image in memory
            }
        }
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        imagePicker.dismiss(animated: true, completion: nil)
        
        DispatchQueue.main.async { () -> Void in
            self.capturePhoto = info[UIImagePickerControllerOriginalImage] as? UIImage
            //print(self.capturePhoto)
            self.sampleImage.image = nil
            if let capturePhoto = self.capturePhoto{
                self.sampleImage.image = capturePhoto
                //self.getImageView()
                self.viewImgV.image = capturePhoto
                self.mgfHeightConstraint.constant = 409
               // self.mgfBottomConstraint.constant = 565 - 359
                
                self.ViewUploadImages.isHidden = false
                
                
                
            }
            
        }
    }
    


//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        
//        imagePicker.dismiss(animated: true, completion: nil)
//        
//        DispatchQueue.main.async { () -> Void in
//            self.capturePhoto = info[UIImagePickerControllerOriginalImage] as? UIImage
//                        print(self.capturePhoto)
////            if let capturePhoto = self.capturePhoto{
////                self.sampleImage.image = capturePhoto
////                
////                
////                if self.fileManager.fileExistsAtPath(self.jsonFilePath.absoluteString, isDirectory: &self.isDirectory) {
////                    do{
////                        let resizedImage = capturePhoto.resize(0.45)
////                        let jpgImageData = UIImageJPEGRepresentation(resizedImage, 1.0)
////                        try jpgImageData?.writeToFile(self.jsonFilePath.path!, options: .DataWritingFileProtectionNone)
////                        print("data written successfully!")
////                    } catch let error as NSError {
////                        print("Image couldn't written to file ")
////                        print(error.description)
////                        // cannot save image in memory
////                    }
////                }
////                
////                //                self.performSegueWithIdentifier("major", sender: capturePhoto)
////            }
//            //else NO image can be captured.
//        }
//    }
//    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        mgfHeightConstraint.constant = 100
        //mgfBottomConstraint.constant = 565

        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Selected Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)

        
        
        
        // - - - - add notifications to current view    keyboardWillShow:
        
//        NotificationCenter.default.addObserver(self, selector: #selector(AddDefect.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
//        
//        NotificationCenter.default.addObserver(self, selector: #selector(AddDefect.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
        
        
        // - - - -
        

        self.ShipQuantity3.delegate = self
        self.AssortmentCartonQty.delegate = self
        self.AssortmentCartonSize.delegate = self
        
//        self.ShipQuantity.delegate = self
//        self.ReScreenQuantity.delegate = self
//        self.ProductionStatus.delegate = self
//        self.Comments.delegate = self
        
//        self.ProductionStatus.layer.borderWidth = 1
//        self.ProductionStatus.layer.borderColor = UIColor.lightGray.cgColor
//        
//        self.Comments.layer.borderWidth = 1
//        self.Comments.layer.borderColor = UIColor.lightGray.cgColor
//        
//        self.finishBtn.layer.borderWidth = 1
//        self.finishBtn.layer.borderColor = UIColor.gray.cgColor
//        self.finishBtn.layer.cornerRadius = 8
        
//        Status.addTarget(self, action: #selector(AuditCommentsVC.switchIsChanged(_:)), for: UIControlEvents.valueChanged)
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(AuditCommentsVC.tap(_:)))
//        commentsView.addGestureRecognizer(tapGesture)

        
        // 2nd Veiw when Report Type is Arcadia
        
        self.Comments_Arcadia.layer.borderWidth = 1
        self.Comments_Arcadia.layer.borderColor = UIColor.orange.cgColor
        
        self.Comments_Arcadia.delegate = self
        self.Qty_of_Lots_TextField.delegate = self
        self.Qty_per_Lot_TextField.delegate = self
        self.InspectionStatus_TextField.delegate = self
        self.Knitted_TextField.delegate = self
        self.Dyed_TextField.delegate = self
        self.Cutting_TextField.delegate = self
        self.Sewing_TextField.delegate = self
        self.Finishing_TextField.delegate = self
        self.Packed_TextField.delegate = self
        
        
        // Controlist delegates
        ShipQuantity2.delegate = self
        ReScreenQuantity2.delegate = self
        TotalCartons.delegate = self
        NoOfShippedCartons.delegate = self
        NoOfCartonsInspected.delegate = self
//        PiecesAvailableForInspection.delegate = self
        
        addDoneButtonOnKeyboard()
        self.sendAuditorLocation()

        ApprovedSample.addTarget(self, action: #selector(AuditCommentsVC.switchApprovSampleChanged(_:)), for: UIControlEvents.valueChanged)
        ApprovedTrim.addTarget(self, action: #selector(AuditCommentsVC.switchApprovTrimChanged(_:)), for: UIControlEvents.valueChanged)
        

        // add placeholder text in comments_Arcadia TextView
        placeholderLabel = UILabel()
        placeholderLabel.text = "QA Comments"
        placeholderLabel.font = UIFont.italicSystemFont(ofSize: Comments_Arcadia.font!.pointSize)
        placeholderLabel.sizeToFit()
        Comments_Arcadia.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: Comments_Arcadia.font!.pointSize / 2)
        placeholderLabel.textColor = UIColor(white: 0, alpha: 0.3)
        placeholderLabel.isHidden = !Comments_Arcadia.text.isEmpty

        
        // Hide views to be managed
        ArcadiaView1.isHidden = true
        ControlistView1.isHidden = true
        ArcadiaView2.isHidden = true
        
//        OuterMostScroller.delegate = self
//        ScrollView_Arcadia.delegate = self
        
        HybridCommentView.isHidden = true
//        commentsView.isHidden = true
        commonView_Arcadia_Controlist.isHidden = true
        AuditResultView.isHidden = true
        // manin Views
        outmostScrollView.delegate = self
//        outmostStackView
        
        
        let libraryDirectoryPath = URL(string: documentsDirectoryPathString)!
        jsonFilePath = libraryDirectoryPath.appendingPathComponent("myCapturedImage.jpg")
        
        // creating a .jpg file in the Library Directory
        if !fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("File created ")
                
            } else {
                print("Couldn't create file for some reason")
            }
        } else {
            print("File already exists")
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    // adjust the screen size when keyboard appears and hides
    
    func keyboardWillHide(_ sender: Notification) {
        let userInfo: [AnyHashable: Any] = sender.userInfo!
        let keyboardSize: CGSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue.size
        self.view.frame.origin.y += keyboardSize.height
    }
    
    func keyboardWillShow(_ sender: Notification) {
        let userInfo: [AnyHashable: Any] = sender.userInfo!
        let keyboardSize: CGSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue.size
        let offset: CGSize = (userInfo[UIKeyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size
        
        if keyboardSize.height == offset.height {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y -= keyboardSize.height
            })
        } else {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y += keyboardSize.height - offset.height
            })
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        // if reportId is Controlist then do the bellow.
        if ReportID_against_current_audit == "28"{

            
            
        }else if ReportID_against_current_audit == "36"{
            // Do nothing
            
        }else{
            
            
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
       
        if textView == Comments_Arcadia{
            
            placeholderLabel.isHidden = !Comments_Arcadia.text.isEmpty
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.AuditCode.text = AuditCodein
        self.AuditStage.text = AuditStagein
        self.AuditStage.backgroundColor = AuditStageColorin

print("ReportID_against_current_audit =",ReportID_against_current_audit)
        // show/Hide between 2 views
      
        //HybridCommentView
        if self.ReportID_against_current_audit == "36"{  // "ReportID OF Hybrid"
         
            // Then only show view against Hybrid
//            self.commentsView.isHidden = true
            
//            self.ScrollView_Arcadia.isHidden = false
//            self.commentsView_Arcadia.isHidden = false
            
            
            HybridCommentView.isHidden = false
            
            ArcadiaView1.isHidden = true
            ControlistView1.isHidden = true
            ArcadiaView2.isHidden = true
            commonView_Arcadia_Controlist.isHidden = true
            AuditResultView.isHidden = false
            
        }else if self.ReportID_against_current_audit == "32"{  // "ReportID OF Arcadia"
           
            // Then only show view against arcadia
//            self.commentsView.isHidden = true
//            self.ScrollView_Arcadia.isHidden = false
//            self.commentsView_Arcadia.isHidden = false
            
            ArcadiaView1.isHidden = false
            ControlistView1.isHidden = true
            ArcadiaView2.isHidden = false
            AuditResultView.isHidden = false
            commonView_Arcadia_Controlist.isHidden = false
            
        }else if self.ReportID_against_current_audit == "46"{  // "ReportID OF Arcadia"
            
            // Then only show view against arcadia
            //            self.commentsView.isHidden = true
            //            self.ScrollView_Arcadia.isHidden = false
            //            self.commentsView_Arcadia.isHidden = false
            
            ArcadiaView1.isHidden = false
            ControlistView1.isHidden = true
            ArcadiaView2.isHidden = true
            AuditResultView.isHidden = false
            commonView_Arcadia_Controlist.isHidden = false
            
        }
        else if self.ReportID_against_current_audit == "28"{  // "ReportID OF Controlist"
           
            // Then only show view against arcadia
//            self.commentsView.isHidden = true
//            self.ScrollView_Arcadia.isHidden = false
//            self.commentsView_Arcadia.isHidden = false
            
            ArcadiaView1.isHidden = true
            ControlistView1.isHidden = false
            ArcadiaView2.isHidden = true
            AuditResultView.isHidden = false
            commonView_Arcadia_Controlist.isHidden = false
            // Create array of inputviews to be used in respective report
            self.arrayOfInputViews = [ShipQuantity2,
                                      ReScreenQuantity2,
                                      TotalCartons,
                                      NoOfShippedCartons,
                                      NoOfCartonsInspected,
                                      InspectionStatus_TextField,
                                      Knitted_TextField,
                                      Dyed_TextField,
                                      Cutting_TextField,
                                      Sewing_TextField,
                                      Finishing_TextField,
                                      Packed_TextField]
            
            // PiecesAvailableForInspection
            
//            var count = 20
//            for view in arrayOfInputViews{
//                
//                view.tag = count
//                count += 1
//            }
            
            
        }else{
//            self.commentsView.isHidden = false
            
            ArcadiaView1.isHidden = true
            ControlistView1.isHidden = true
            ArcadiaView2.isHidden = true
            AuditResultView.isHidden = true
            commonView_Arcadia_Controlist.isHidden = true
//            self.ScrollView_Arcadia.isHidden = true
//            self.commentsView_Arcadia.isHidden = true
        }
        
        
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
    }
    
    
//    func tap(_ gestue: UITapGestureRecognizer){
//        self.ShipQuantity.resignFirstResponder()
//        self.ReScreenQuantity.resignFirstResponder()
//        self.ProductionStatus.resignFirstResponder()
//        self.Comments.resignFirstResponder()
//    }
    
//    @IBAction func selectAttachmentImage(_ sender: AnyObject) {
//        
//        let alertView = UIAlertController(title: nil , message: "Choose Image From", preferredStyle: .alert)
//        let camera_action: UIAlertAction = UIAlertAction(title: "Camera", style: .default) { (UIAlertAction) in
//            
//            self.sampleImageTapped()
//        }
//
//        let photos_action: UIAlertAction = UIAlertAction(title: "Photos", style: .default) { (UIAlertAction) in
//            
//            self.samplePhotoLibImageTapped()
//        }
//        
//        alertView.addAction(camera_action)
//        alertView.addAction(photos_action)
//        self.present(alertView, animated: true, completion:{
//            alertView.view.superview?.isUserInteractionEnabled = true
//            alertView.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
//        })
//    }
//    func alertControllerBackgroundTapped()
//    {
//        self.dismiss(animated: true, completion: nil)
//    }
//    
//    
//   
    
    @IBAction func selectAttachmentImage(_ sender: AnyObject) {
        
        otherImagesType = ""
        let pressedBtn = sender as! UIButton
        print(pressedBtn == packingBtn)
        print(pressedBtn == MiscBtn)
        print(pressedBtn == LabBtn)
        var message = ""
        if pressedBtn == packingBtn{
            otherImagesType = "PACK"
            message =  "Choose Images For Packing"
            
        }
        else if pressedBtn == MiscBtn{
            
            
            otherImagesType = "MISC"
            message =  "Choose Images For \(otherImagesType!)"
            
        }
        else if pressedBtn == LabBtn{
            
            
            paramPath =  "/specs-sheet"
            isLabImage = true
            otherImagesType = "LAB"
            message =  "Choose Image For LAB Report/Specs Sheet"
            
        }
        print(otherImagesType)
        
        if(otherImagesType == "LAB"){
            
        }
        let alertView = UIAlertController(title: nil , message: message, preferredStyle: .alert)
        let camera_action: UIAlertAction = UIAlertAction(title: "Camera", style: .default) { (UIAlertAction) in
            
            self.sampleImageTapped()
        }
        
        let photos_action: UIAlertAction = UIAlertAction(title: "Photos", style: .default) { (UIAlertAction) in
            
            self.samplePhotoLibImageTapped()
        }
        alertView.addAction(camera_action)
        alertView.addAction(photos_action)
        self.present(alertView, animated: true, completion:{
            alertView.view.superview?.isUserInteractionEnabled = true
            alertView.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
        
    }
    func alertControllerBackgroundTapped()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func switchIsChanged(_ mySwitch: UISwitch) {
        
        if mySwitch.isOn {
            print("Pass")
            AuditResult = "P"
        } else {
            print("Fail")
            AuditResult = "F"
        }
    }
    
    func switchApprovSampleChanged(_ SampleSwitch: UISwitch) -> Void {
        if SampleSwitch.isOn {
            print("approvedSample ON")
            
            ApprovedSampleValue = "Yes"
        } else {
            print("approvedSample OFF")
            ApprovedSampleValue = "No"
        }
        print(ApprovedSampleValue)
    }
    func switchApprovTrimChanged(_ TrimSwitch: UISwitch) -> Void {
       
        if TrimSwitch.isOn {
            print("approvedTrim ON")
            
            ApprovedTrimValue = "Y"
        } else {
            print("approvedTrim OFF")
            ApprovedTrimValue = "N"
        }
        print(ApprovedTrimValue)
    }
    
    
    
    func textFieldShouldReturn(_ userText: UITextField!) -> Bool {
     
        if ReportID_against_current_audit == "28"{
            
//            for view in arrayOfInputViews{
//                
//                if view == userText{
//                    userText.resignFirstResponder()
//                }
//                view.isUserInteractionEnabled = true
//            }
            userText.resignFirstResponder()
            self.AuditResultBtn.isUserInteractionEnabled = true
            self.Comments_Arcadia.isUserInteractionEnabled = true
            
            
        }else if ReportID_against_current_audit == "36"{
            
//            ShipQuantity3
//            AssortmentCartonQty
//            AssortmentCartonSize
            userText.resignFirstResponder()
            
        }else{
            
            if userText == self.InspectionStatus_TextField{
                
                self.InspectionStatus_TextField.resignFirstResponder()
                
                self.Qty_of_Lots_TextField.isUserInteractionEnabled = true
                self.Qty_per_Lot_TextField.isUserInteractionEnabled = true
                self.InspectionStatus_TextField.isUserInteractionEnabled = true
                self.Knitted_TextField.isUserInteractionEnabled = true
                self.Dyed_TextField.isUserInteractionEnabled = true
                self.Cutting_TextField.isUserInteractionEnabled = true
                self.Sewing_TextField.isUserInteractionEnabled = true
                self.Finishing_TextField.isUserInteractionEnabled = true
                self.Packed_TextField.isUserInteractionEnabled = true
                
                self.Comments_Arcadia.isUserInteractionEnabled = true
                
                self.EstimatedFinalDate.isUserInteractionEnabled = true
                
                self.AuditResultBtn.isUserInteractionEnabled = true
                
                
            }
//            else if userText == self.ProductionStatus{
//                self.ProductionStatus.resignFirstResponder()
//            }
        }
        return true;
    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        
//        if arrayOfInputViews != nil {
//            
//            for view in arrayOfInputViews{
//                
//                if view.isFirstResponder{
//                    view.resignFirstResponder()
//                }
//                view.isUserInteractionEnabled = true
//            }
//            if self.Comments_Arcadia.isFirstResponder{
//                
//                self.Comments_Arcadia.resignFirstResponder()
//            }
//            self.AuditResultBtn.isUserInteractionEnabled = true
//            self.Comments_Arcadia.isUserInteractionEnabled = true
//        }
    }
    
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
       
        if ReportID_against_current_audit == "36"{
      
        }else{
            if textView == self.Comments_Arcadia && self.Comments_Arcadia.isFirstResponder{
                
                self.Qty_of_Lots_TextField.isUserInteractionEnabled = false
                self.Qty_per_Lot_TextField.isUserInteractionEnabled = false
                self.InspectionStatus_TextField.isUserInteractionEnabled = false
                self.Knitted_TextField.isUserInteractionEnabled = false
                self.Dyed_TextField.isUserInteractionEnabled = false
                self.Cutting_TextField.isUserInteractionEnabled = false
                self.Sewing_TextField.isUserInteractionEnabled = false
                self.Finishing_TextField.isUserInteractionEnabled = false
                self.Packed_TextField.isUserInteractionEnabled = false
                
                self.Comments_Arcadia.isUserInteractionEnabled = true
                
                self.EstimatedFinalDate.isUserInteractionEnabled = false
                
                self.AuditResultBtn.isUserInteractionEnabled = false
                
//                for view in arrayOfInputViews{
//                    view.isUserInteractionEnabled = false
//                }
            }
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == Packed_TextField){
            var txtAfterUpdate:NSString = textField.text! as NSString
            txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
            if(txtAfterUpdate == "100" || txtAfterUpdate == "100%"){
             // Packed_TextField.text = "100"
                Sewing_TextField.text = "100"
                Cutting_TextField.text = "100"
                Finishing_TextField.text = "100"
            }
        }
        return true
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            
            if ReportID_against_current_audit == "36"{
                textView.resignFirstResponder()
            }
            else{
//                self.Comments.resignFirstResponder()
                // resignFirstResponder of Arcadia comments here
                
                if textView == self.Comments_Arcadia{
                    
                    self.Comments_Arcadia.resignFirstResponder()
                    
                    self.Qty_of_Lots_TextField.isUserInteractionEnabled = true
                    self.Qty_per_Lot_TextField.isUserInteractionEnabled = true
                    self.InspectionStatus_TextField.isUserInteractionEnabled = true
                    self.Knitted_TextField.isUserInteractionEnabled = true
                    self.Dyed_TextField.isUserInteractionEnabled = true
                    self.Cutting_TextField.isUserInteractionEnabled = true
                    self.Sewing_TextField.isUserInteractionEnabled = true
                    self.Finishing_TextField.isUserInteractionEnabled = true
                    self.Packed_TextField.isUserInteractionEnabled = true
                    
                    self.Comments_Arcadia.isUserInteractionEnabled = true
                    
                    self.EstimatedFinalDate.isUserInteractionEnabled = true
                    
                    self.AuditResultBtn.isUserInteractionEnabled = true
                    
//                    for view in arrayOfInputViews{
//                        view.isUserInteractionEnabled = true
//                    }
                    
                }
            }
//            self.Comments_Arcadia.resignFirstResponder()
            return false
        }  // end of back slash check
        return true
    }
    
    
    @IBAction func SelectEstimatedFinalDate(_ sender: AnyObject) {
        
            DatePickerDialog().show("SelectDate", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
                (date) -> Void in
                
                self.EstimatedFinalDate.setTitle("\(date.formatted)", for: UIControlState())
            }
        
    }
    
    
    @IBAction func SelectShipmentDate(_ sender: Any) {
        DatePickerDialog().show("SelectDate", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            
            self.ShipDate.setTitle("\(date.formatted)", for: UIControlState())
        }

    }
    
    
    @IBAction func SelectSolidSizeType(_ sender: Any) {
        
    }
    
    
    @IBAction func SelectWorkmanshipResult(_ sender: Any) {
        
        print("SelectWorkmanshipAuditResult button is cliked.")
        
        let popupVC = UIStoryboard(name: "Main2", bundle: nil).instantiateViewController(withIdentifier: "PopUpVCHybrid") as! HybridPopUpViewControler
        
        popupVC.delegate = self
        
        popupVC.pickerdata = ["PASS","FAIL"]
        popupVC.pickerdataIDs = ["P","F"]
        
        popupVC.WhichButton = "GarmentAuditResult"
        
        popupVC.AllreadySelectedText = self.WorkmanshipResult.titleLabel!.text!
        
        self.addChildViewController(popupVC)
        popupVC.view.frame = self.view.frame
        
        self.view.addSubview(popupVC.view)
        self.didMove(toParentViewController: self)
        
        
    }
    
    func saveGarmentConformityAuditResult(_ strText: String, strID: String) {
        
        self.WorkmanshipResult.setTitle(strText, for: .normal)
        workmanshipResultValue = strID
    }
    func savePackagingAuditResult(_ strText: String, strID: String) {
        // Do Nothing
    }
    
    
    @IBAction func SelectAuditResult(_ sender: AnyObject) {
        
        print("SelectAuditResult button is cliked.")
                
        let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as! PopUpViewController
        
        popupVC.delegate = self
        
        popupVC.pickerdata = ["PASS","FAIL","HOLD"]
        popupVC.pickerdataIDs = ["P","F","H"]
        
        popupVC.WhichButton = "AuditResult"
        
        popupVC.AllreadySelectedText = self.AuditResultBtn.titleLabel!.text!
        
        self.addChildViewController(popupVC)
        popupVC.view.frame = self.view.frame
        
        self.view.addSubview(popupVC.view)
        self.didMove(toParentViewController: self)

    }
    
    func saveAuditResultText(_ strText: String, strID: String) {
        print("Audit Result =",strText)
        print("Result ID =",strID)
        self.AuditResultBtn.setTitle(strText, for: UIControlState())
        self.AuditResult = strID
    }
    
    func saveAuditTypeText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveAuditorsText(_ strText: String, strID: String) {
        // DO Nothing....
    }
    func saveVendorsText(_ strText: String, strID: String) {
        // DO Nothing....
    }
    func saveText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveUnitText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveBrandText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveStyleText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveReportText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveVendorText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveSampleSizeText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveProductionLineText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveAQLText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveAQLData(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveSelectedColor(_ strText : String, strID : String)
    {
        
    }
    
    func showalert(){
        let alertView = UIAlertController(title: nil , message: "Do write some comments", preferredStyle: .alert)
        let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertView.addAction(ok_action)
        self.present(alertView, animated: true, completion: nil)
    }
    
//    @IBAction func Finish(_ sender: AnyObject) {
//     print("newAuditResult=\(AuditResult)")
//        if AuditResult == nil{
//            
//            let alertView = UIAlertController(title: nil , message: "Are you sure Audit Result is Fail?", preferredStyle: .alert)
//            let yes_action: UIAlertAction = UIAlertAction(title: "Yes", style: .default, handler: { (UIAlertAction) -> Void in
//                
//                self.AuditResult = "F"
//                
//                if self.Comments.text.isEmpty{
//                    
//                    self.showalert()
//                    
//                }else{
//                    
//                    //If Internet is available or Not just save the data offline.
//                    
//                    //First Stop sync Service Then save data
//                    
//                    syncObject.endBackgroundTask()
//                    updateTimer?.invalidate()
//                    syncObject = nil
//                    
//                    
//                    self.saveOfflineCommentsData()
//                    
//                    //NOW START Sync Service again
//                    
//                    syncObject = syncService()
//                    
//                /*
//                     if !Reachability.isConnectedToNetwork(){
//                        
//                        self.saveOfflineCommentsData()
//                    }else{
//                        self.performLoadDataRequestWithURL(self.getUrl())
//                    }   
//                */
//                }
//                
//            })
//            let no_action: UIAlertAction = UIAlertAction(title: "No", style: .default, handler: {(UIAlertAction)-> Void in
//
//            })
//            
//            alertView.addAction(yes_action)
//            alertView.addAction(no_action)
//
//            self.present(alertView, animated: true, completion: nil)
//
//            
//        }else if self.Comments.text.isEmpty{
//            
//            self.showalert()
//            
//        }else if AuditResult != nil && !(self.Comments.text.isEmpty){
//            print(AuditResult)
//            print(self.Comments.text)
//            
//            //If Internet is available or Not just save the data offline.
//            
//            self.saveOfflineCommentsData()
//            
//            
//       /*   if !Reachability.isConnectedToNetwork(){
//                
//                self.saveOfflineCommentsData()
//            }else{
//                self.performLoadDataRequestWithURL(self.getUrl())
//            }
    
//        */
//        }
//    }
    
    
    // Arcadia / Controlist save & Continue button action
//    var saveContinue_Pressed = false
    @IBAction func Save_and_Continue(_ sender: AnyObject) {
  
//        if self.Comments_Arcadia.text == nil || self.Comments_Arcadia.text.isEmpty {
//            self.showalert()
//
//        }else
            if self.AuditResult == nil || self.AuditResult.isEmpty {
            
            
            let alertView = UIAlertController(title: nil , message: "Are you sure Audit Result is Fail?", preferredStyle: .alert)
            let yes_action: UIAlertAction = UIAlertAction(title: "Yes", style: .default, handler: { (UIAlertAction) -> Void in
                
                self.AuditResult = "F"
//                self.saveContinue_Pressed = true
//                self.performSegue(withIdentifier: "goto Final comments", sender: nil)

                self.saveOfflineCommentsData()
            })
            let no_action: UIAlertAction = UIAlertAction(title: "No", style: .default, handler: {(UIAlertAction)-> Void in
                
            })
            
            alertView.addAction(yes_action)
            alertView.addAction(no_action)
            
            self.present(alertView, animated: true, completion: nil)
            
            
        }else{
           // performSegue(withIdentifier: "goto Final comments", sender: nil)

//            saveContinue_Pressed = true
            self.saveOfflineCommentsData()
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
//            defaults.removeObjectForKey("userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
            //        case 4:
            //            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
            //                //                self.dismissViewControllerAnimated(true, completion: nil)
            //                presentViewController(resultController, animated: true, completion: nil)
            //            }
            //
            //            let selectedImage:UIImage = UIImage(named: "Selected QAReport")!
            //            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            //
            //            break
        case 5:
            if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                present(resultController, animated: true, completion: nil)
            }
            }else{
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                    present(resultController, animated: true, completion: nil)
                }

            }
            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_selected_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break

        default:
            break
        }
    }

    func getUrl() -> URL {
        let toEscape = "http://app.3-tree.com/quonda/save-comments.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
    var bodyData:String!
//    func performLoadDataRequestWithURL(_ url: URL) -> String? {
//        
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
////        var CommentsIn_UTF8 = self.Comments.text!.cStringUsingEncoding(NSUTF8StringEncoding)!
//        //        self.bodyData = self.bodyData!+"&PageId=\(currentPage)"
//        self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodein!)&ShipQty=\(self.ShipQuantity.text!)&ReScreenQty=\(self.ReScreenQuantity.text!)&AuditResult=\(self.AuditResult!)&Comments=\(self.Comments.text!)&ProductionStatus=\(self.ProductionStatus.text!)"
//        
//        print("CommentViewController-bodydata -> \(self.bodyData)")
//        
//        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
//        var json = ""
//        request.httpMethod = "POST"
//        request.httpBody = self.bodyData!.data(using: String.Encoding.utf8)
//        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main){
//            response, data, error in
//            print("\(response)")
//            if (error != nil) {
//                print(error)
//                UIApplication.shared.isNetworkActivityIndicatorVisible = false
//                let alertView = UIAlertController(title: nil , message: "\(error?.localizedDescription) \n Error in saving data!", preferredStyle: .alert)
//                let ok_action: UIAlertAction = UIAlertAction(title: "Retry", style: .default, handler: { (UIAlertAction) -> Void in
//                    
//                    self.performLoadDataRequestWithURL(self.getUrl())
//                })
//                let cancel_action: UIAlertAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
//                alertView.addAction(ok_action)
//                alertView.addAction(cancel_action)
//                self.present(alertView, animated: true, completion: nil)
//                
//                //                self.activityIndicator.stopAnimating()
//                //                self.messageFrame.removeFromSuperview()
//            }
//            
//            if data != nil {
//                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
////                // - - - - - - write this JSON data to file- - - - - - - -
////                //                print("created : \(self.created)")
////                //                if self.created == true{
////                if self.fileManager.fileExistsAtPath(self.jsonFilePath2.absoluteString, isDirectory: &self.isDirectory) {
////                    do{
////                        try json.writeToFile(self.jsonFilePath2.path!, atomically: true, encoding: NSUTF8StringEncoding)
////                    } catch let error as NSError {
////                        print("JSON data couldn't written to file // File not exist")
////                        print(error.description)
////                    }
////                }
//                print("json=")
//                print(json)
//                self.dictionary = self.parseJSON(json)
//                print("self.dictionary=")
//                print("\(self.dictionary)")
//                print(self.dictionary["Status"])
//                if"\(self.dictionary["Status"]!)" == "OK" {
//                    
//                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
//                    print("status is OK")
//                    if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "5") as? AuditorModeViewController {
//                        
//                        self.present(resultController, animated: true, completion: nil)
//                    }
//                }else{
//                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
//                    
//                    let alertView = UIAlertController(title: "Error in saving data" , message: "Press OK", preferredStyle: .alert)
//                    let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//                    
//                    alertView.addAction(ok_action)
//                    self.present(alertView, animated: true, completion: nil)
//                }
//                
//            }//if data != nil
//            else{
//                UIApplication.shared.isNetworkActivityIndicatorVisible = false
//                return print("return data is nil")
//            }
//        }
//        return json
//    }
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    
    func JSON_To_FoundationObj(_ jsonString: String) -> NSMutableDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSMutableDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    
    func parseCommentsJSON(_ jsonString: String) -> [String:String] {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:String] {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    
    
    func saveOfflineCommentsData(){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        print("You are in AuditComments OFFLINE mode.")
        
        // Creating AuditComment Json file
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("AuditCommentFile\(self.AuditCode.text!).json")
        
        // creating a .json file in the Documents folder
        
        if !fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("File created ")
             
                // Now save it to FilesToSync.json file
                
                //(jsonFilePath.absoluteString)
                
            } else {
                print("Couldn't create file for some reason")
            }
        } else {
            print("File already exists")
        }
        //file creation ends
        
        
        
        //First Read out the AuditComment AuditCodeFile
        
        do{
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath.absoluteString, encoding: String.Encoding.utf8.rawValue) as String
            
            print("readAuditComment_String = ",readString)
            
            /// here send readString to function to update values stored in AuditCommentFile(AuditCode) File
            
            self.AuditCommentAuditCodeFile(readString, AuditCommentFilePath: jsonFilePath.absoluteString)
            
        }
        catch let error as NSError{
            print("There is an error while reading AddDeffectText(AuditCode) File.",error.description)
        }
    
    }// end of func
    
    
    func AuditCommentAuditCodeFile(_ savedCommentsData:String?, AuditCommentFilePath: String) -> Void {
       
        if ReportID_against_current_audit == "36"{
            // save data against Hybrid only
            
            AuditCommentAuditCodeFile_Hybrid(savedCommentsData, AuditCommentFilePath: AuditCommentFilePath)
        
        }else if ReportID_against_current_audit == "28"{
            // save data against Controlist only
            
            AuditCommentAuditCodeFile_Controlist(savedCommentsData, AuditCommentFilePath: AuditCommentFilePath)
        }
        else if ReportID_against_current_audit == "32"{
        //if saveContinue_Pressed == true{
            // save data against Arcadia only
            
            AuditCommentAuditCodeFile_Arcadia(savedCommentsData, AuditCommentFilePath: AuditCommentFilePath)
            
        }
        else if ReportID_against_current_audit == "46"{
            //if saveContinue_Pressed == true{
            // save data against Arcadia only
            //OPEN WHEN DONE
           // performSegue(withIdentifier: "goto Final comments", sender: nil)

            AuditCommentAuditCodeFile_Arcadia(savedCommentsData, AuditCommentFilePath: AuditCommentFilePath)
            
        }
//        else {
//            
//            if savedCommentsData != nil && savedCommentsData != "" {
//                
//                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodein!)&ShipQty=\(self.ShipQuantity.text!)&ReScreenQty=\(self.ReScreenQuantity.text!)&AuditResult=\(self.AuditResult!)&Comments=\(self.Comments.text!)&ProductionStatus=\(self.ProductionStatus.text!)"
//                
//                print("AuditComments-bodydata -> \(self.bodyData)")
//                
//                
//                
//                var AuditCommentsOfflineData:[String:String] = parseCommentsJSON(savedCommentsData!)
//                
//                
//                let randomString = randomStringWithLength(6)
//                
//                AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "AuditComments_bodyData\(self.AuditCode.text!)_\(randomString)")
//                AuditCommentsOfflineData.updateValue("http://portal.3-tree.com/api/android/quonda/save-comments.php", forKey: "AuditComment_url")
//                
//                
//                if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
//                    do{
//                        let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)
//                        
//                        let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
//                        
//                        try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
//                        
//                        //Note!!
//                        
//                        if !Reachability.isConnectedToNetwork(){
//                            
//                            //BEFORE MOVING ONTO THE NEXT SCREEN YOU HAVE TO UPDATE AUDITCOMPLETED VALUE IN AUDITMODE.JSON
//                            
//                            /*   let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
//                             
//                             let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("AuditMode.json")
//                             do {
//                             var readString: String
//                             readString = try NSString(contentsOfFile: jsonFilePath.path!, encoding: NSUTF8StringEncoding) as String
//                             print("readString_AuditorModeVC=\(readString)")
//                             self.updateAuditorModeVCDataWhenOffline(readString)
//                             } catch let error as NSError {
//                             print(error.description)
//                             }
//                             */
//                        }
//                        
//                        
//                        // now move onto the AuditorModeViewController screen
//                        
//                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
//                        
//                        if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "5") as? AuditorModeViewController {
//                            
//                            self.present(resultController, animated: true, completion: nil)
//                        }
//                        
//                    } catch let error as NSError {
//                        print("JSON data couldn't written to file // File not exist")
//                        print(error.description)
//                    }
//                }
//                
//            }else{
//                
//                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodein!)&ShipQty=\(self.ShipQuantity.text!)&ReScreenQty=\(self.ReScreenQuantity.text!)&AuditResult=\(self.AuditResult!)&Comments=\(self.Comments.text!)&ProductionStatus=\(self.ProductionStatus.text!)"
//                
//                print("AuditComments-bodydata -> \(self.bodyData)")
//                
//                
//                
//                var AuditCommentsOfflineData = [String:String]()
//                
//                
//                let randomString = randomStringWithLength(6)
//                
//                AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "AuditComments_bodyData\(self.AuditCode.text!)_\(randomString)")
//                AuditCommentsOfflineData.updateValue("http://portal.3-tree.com/api/android/quonda/save-comments.php", forKey: "AuditComment_url")
//                
//                
//                if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
//                    do{
//                        let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)
//                        
//                        let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
//                        
//                        try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
//                        
//                        // first read out the file
//                        //                    do{
//                        //                        let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
//                        //                        let jsonSyncFilePath = documentsDirectoryPath.URLByAppendingPathComponent("FilesToSync.json")
//                        //
//                        //                        var readString: String?
//                        //                        readString = try NSString(contentsOfFile: jsonSyncFilePath.path!, encoding: NSUTF8StringEncoding) as String
//                        //                        print("sync file after adding comments =",readString)
//                        //                    }catch{
//                        //                        print("cannot read")
//                        //                    }
//                        
//                        
//                        //Note!!
//                        
//                        //BEFORE MOVING ONTO THE NEXT SCREEN YOU HAVE TO UPDATE AUDITCOMPLETED VALUE IN AUDITMODE.JSON
//                        
//                        /*     let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
//                         
//                         let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("AuditMode.json")
//                         do {
//                         var readString: String
//                         readString = try NSString(contentsOfFile: jsonFilePath.path!, encoding: NSUTF8StringEncoding) as String
//                         print("readString_AuditorModeVC=\(readString)")
//                         self.updateAuditorModeVCDataWhenOffline(readString)
//                         } catch let error as NSError {
//                         print(error.description)
//                         }
//                         */
//                        
//                        
//                        // now move onto the AuditorModeViewController screen
//                        
//                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
//                        
//                        if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "5") as? AuditorModeViewController {
//                            
//                            self.present(resultController, animated: true, completion: nil)
//                        }
//                        
//                    } catch let error as NSError {
//                        print("JSON data couldn't written to file // File not exist")
//                        print(error.description)
//                    }
//                }
//            }
//        }// else part end of Arcadia
    
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        //Reading from JsonDictionary File stored in documents directory
        do{
            var readString: String
            readString = try NSString(contentsOfFile: AuditCommentFilePath, encoding: String.Encoding.utf8.rawValue) as String
            //                    print("readString=\(readString)")
            let readStringParsed = parseCommentsJSON(readString)
            print("AuditComments_readStringParsed = ",readStringParsed)
        }
        catch let error as NSError{
            print("There is an error while reading from JsonDictionary File.",error.description)
            
        }

    }
    
    func AuditCommentAuditCodeFile_Arcadia(_ savedCommentsData:String?, AuditCommentFilePath: String) -> Void{
        
        // - - - -
        
            if savedCommentsData != nil && savedCommentsData != "" {
                
                if self.ApprovedSampleValue == nil{
                   
                    self.ApprovedSampleValue = "No"
                }
                if self.ApprovedTrimValue == nil{
                    
                    self.ApprovedTrimValue = "N"
                }
                if self.AuditResult == nil{
                    
                    self.AuditResult = "F"
                }
                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodein!)&AuditResult=\(self.AuditResult!)&Comments=\(self.Comments_Arcadia.text!)&ShipQty=\(self.Qty_of_Lots_TextField.text!)&ApprovedSample=\(self.ApprovedSampleValue!)&ApprovedTrims=\(self.ApprovedTrimValue!)&QtyOfLots=\(self.Qty_of_Lots_TextField.text!)&QtyPerLot=\(self.Qty_per_Lot_TextField.text!)&InspectionStatus=\(self.InspectionStatus_TextField.text!)&Knitted=\(self.Knitted_TextField.text!)&Dyed=\(self.Dyed_TextField.text!)&Cutting=\(self.Cutting_TextField.text!)&Sewing=\(self.Sewing_TextField.text!)&Finishing=\(self.Finishing_TextField.text!)&Packing=\(Packed_TextField.text!)\(locationBodyData)"
                
//                self.bodyData = "User=\(defaults.objectForKey("userid")as! String)&AuditCode=\(AuditCodein!)&ShipQty=\(self.ShipQuantity.text!)&ReScreenQty=\(self.ReScreenQuantity.text!)&AuditResult=\(self.AuditResult)&Comments=\(self.Comments.text!)&ProductionStatus=\(self.ProductionStatus.text!)"
                
                print("AuditCommentsArcadia2-bodydata -> \(self.bodyData)")
                
                
                
                var AuditCommentsOfflineData:[String:String] = parseCommentsJSON(savedCommentsData!)
                
                
                let randomString = randomStringWithLength(6)
                
                AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "AuditComments_bodyData\(self.AuditCode.text!)_\(randomString)")
                AuditCommentsOfflineData.updateValue("http://app.3-tree.com/quonda/save-comments.php", forKey: "AuditComment_url")
                
                
                if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
                    do{
                        let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)
                        
                        let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                        
                        try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
                        
                        //Note!!
                        
                        if !Reachability.isConnectedToNetwork(){
                            
                            //BEFORE MOVING ONTO THE NEXT SCREEN YOU HAVE TO UPDATE AUDITCOMPLETED VALUE IN AUDITMODE.JSON
                            
                            /*   let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
                             
                             let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("AuditMode.json")
                             do {
                             var readString: String
                             readString = try NSString(contentsOfFile: jsonFilePath.path!, encoding: NSUTF8StringEncoding) as String
                             print("readString_AuditorModeVC=\(readString)")
                             self.updateAuditorModeVCDataWhenOffline(readString)
                             } catch let error as NSError {
                             print(error.description)
                             }
                             */
                        }
                        
                        
                        // now move onto the Next screen
                        //???????
                        //?????
                        //???
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        defaults.set("Completed", forKey: "AuditComments\(AuditCodein!)")

                        performSegue(withIdentifier: "goto Final comments", sender: nil)

                        
//                        if let resultController = self.storyboard!.instantiateViewControllerWithIdentifier("5") as? AuditorModeViewController {
//                            
//                            self.presentViewController(resultController, animated: true, completion: nil)
//                        }
                        
                    } catch let error as NSError {
                        print("JSON data couldn't written to file // File not exist")
                        print(error.description)
                    }
                }
                
            }else{
                
                if self.ApprovedSampleValue == nil{
                    
                    self.ApprovedSampleValue = "No"
                    
                }
                if self.ApprovedTrimValue == nil{
                    
                    self.ApprovedTrimValue = "N"
                }
                if self.AuditResult == nil{
                    
                    self.AuditResult = "F"
                }
                if self.EstimatedFinalDate.titleLabel?.text == nil{
                    
                    self.EstimatedFinalDate.titleLabel!.text = ""
                }
                print("self.Qty_per_Lot_TextField.text! =",self.Qty_per_Lot_TextField.text!)
                
                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodein!)&AuditResult=\(self.AuditResult!)&Comments=\(self.Comments_Arcadia.text!)&ApprovedSample=\(self.ApprovedSampleValue!)&ShipQty=\(self.Qty_of_Lots_TextField.text!)&ApprovedTrims=\(self.ApprovedTrimValue!)&QtyOfLots=\(self.Qty_of_Lots_TextField.text!)&QtyPerLot=\(self.Qty_per_Lot_TextField.text!)&InspectionStatus=\(self.InspectionStatus_TextField.text!)&Knitted=\(self.Knitted_TextField.text!)&Dyed=\(self.Dyed_TextField.text!)&Cutting=\(self.Cutting_TextField.text!)&Sewing=\(self.Sewing_TextField.text!)&Finishing=\(self.Finishing_TextField.text!)&Packing=\(Packed_TextField.text!)\(locationBodyData)"

                
                print("AuditCommentsArcadia-bodydata -> \(self.bodyData)")
                
                
                
                var AuditCommentsOfflineData = [String:String]()
                
                
                let randomString = randomStringWithLength(6)
                
                AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "AuditComments_bodyData\(self.AuditCode.text!)_\(randomString)")
                AuditCommentsOfflineData.updateValue("http://app.3-tree.com/quonda/save-comments.php", forKey: "AuditComment_url")
                
                
                if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
                    do{
                        let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)
                        
                        let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                        
                        try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
                        
                        // first read out the file
                        //                    do{
                        //                        let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
                        //                        let jsonSyncFilePath = documentsDirectoryPath.URLByAppendingPathComponent("FilesToSync.json")
                        //
                        //                        var readString: String?
                        //                        readString = try NSString(contentsOfFile: jsonSyncFilePath.path!, encoding: NSUTF8StringEncoding) as String
                        //                        print("sync file after adding comments =",readString)
                        //                    }catch{
                        //                        print("cannot read")
                        //                    }
                        
                        
                        //Note!!
                        
                        //BEFORE MOVING ONTO THE NEXT SCREEN YOU HAVE TO UPDATE AUDITCOMPLETED VALUE IN AUDITMODE.JSON
                        
                        /*     let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
                         
                         let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("AuditMode.json")
                         do {
                         var readString: String
                         readString = try NSString(contentsOfFile: jsonFilePath.path!, encoding: NSUTF8StringEncoding) as String
                         print("readString_AuditorModeVC=\(readString)")
                         self.updateAuditorModeVCDataWhenOffline(readString)
                         } catch let error as NSError {
                         print(error.description)
                         }
                         */
                        
                        
                        // now move onto the AuditorModeViewController screen
                        //?????
                        //???
                        //??
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        defaults.set("Completed", forKey: "AuditComments\(AuditCodein!)")

                        performSegue(withIdentifier: "goto Final comments", sender: nil)
                        
                        
//                        if let resultController = self.storyboard!.instantiateViewControllerWithIdentifier("5") as? AuditorModeViewController {
//                            
//                            self.presentViewController(resultController, animated: true, completion: nil)
//                        }
                        
                    } catch let error as NSError {
                        print("JSON data couldn't written to file // File not exist")
                        print(error.description)
                    }
                }
            }
        
        // - - -
        
    }
    
    
    func AuditCommentAuditCodeFile_Controlist(_ savedCommentsData:String?, AuditCommentFilePath: String) -> Void{
        
        // - - Controlist Report - -
        
        if savedCommentsData != nil && savedCommentsData != "" {
            
//            ShipQuantity2,
//            ReScreenQuantity2,
//            TotalCartons,
//            NoOfShippedCartons,
//            NoOfCartonsInspected,
//            InspectionStatus_TextField,
//            Knitted_TextField,
//            Dyed_TextField,
//            Cutting_TextField,
//            Sewing_TextField,
//            Finishing_TextField,
//            Packed_TextField
            if self.AuditResult == "Optional(\"P\")"{
                self.AuditResult = "P"
            }
            if self.AuditResult == "Optional(\"P\")"{
                self.AuditResult = "P"
            }
            print("AuditResult=\(self.AuditResult!)")
            if self.AuditResult == "Optional(\"P\")"{
                self.AuditResult = "P"
            }
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodein!)&ShipQty=\(self.ShipQuantity2.text!)&ReScreenQty=\(self.ReScreenQuantity2.text!)&TotalCartons=\(self.TotalCartons.text!)&CartonsShipped=\(self.NoOfShippedCartons.text!)&CartonsInspected=\(self.NoOfCartonsInspected.text!)&InspectionStatus=\(self.InspectionStatus_TextField.text!)&Knitted=\(self.Knitted_TextField.text!)&Dyed=\(self.Dyed_TextField.text!)&Cutting=\(self.Cutting_TextField.text!)&Sewing=\(self.Sewing_TextField.text!)&Finishing=\(self.Finishing_TextField.text!)&Packing=\(Packed_TextField.text!)&AuditResult=\(self.AuditResult!)&Comments=\(self.Comments_Arcadia.text!)"
            
            //                self.bodyData = "User=\(defaults.objectForKey("userid")as! String)&AuditCode=\(AuditCodein!)&ShipQty=\(self.ShipQuantity.text!)&ReScreenQty=\(self.ReScreenQuantity.text!)&AuditResult=\(self.AuditResult)&Comments=\(self.Comments.text!)&ProductionStatus=\(self.ProductionStatus.text!)"
            
            print("AuditCommentsControlist2-bodydata -> \(self.bodyData)")
            
            
            
            var AuditCommentsOfflineData:[String:String] = parseCommentsJSON(savedCommentsData!)
            
            
            let randomString = randomStringWithLength(6)
            
            AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "AuditComments_bodyData\(self.AuditCode.text!)_\(randomString)")
            AuditCommentsOfflineData.updateValue("http://app.3-tree.com/quonda/save-comments.php", forKey: "AuditComment_url")
            
            
            if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    //Note!!
                    
                    if !Reachability.isConnectedToNetwork(){
                        
                        //BEFORE MOVING ONTO THE NEXT SCREEN YOU HAVE TO UPDATE AUDITCOMPLETED VALUE IN AUDITMODE.JSON
                        
                        /*   let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
                         
                         let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("AuditMode.json")
                         do {
                         var readString: String
                         readString = try NSString(contentsOfFile: jsonFilePath.path!, encoding: NSUTF8StringEncoding) as String
                         print("readString_AuditorModeVC=\(readString)")
                         self.updateAuditorModeVCDataWhenOffline(readString)
                         } catch let error as NSError {
                         print(error.description)
                         }
                         */
                    }
                    
                    
                    // now move onto the Next screen
                    //???????
                    //?????
                    //???
                    
                    //var SavedDefects = defaults.object(forKey: "\(self.AuditCode.text!)") as? [((Color:UIColor, ID:String),(DefectType:String,DefectCode:String))]
            
                    
                  //  if self.arrayOfDefectsLoggedRecieved != nil{
                    //if SavedDefects != nil {
                        
                       // performSegue(withIdentifier: "CAP", sender: nil)
                        
                  //  }else{
                        if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                        if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                            present(resultController, animated: true, completion: nil)
                        }
                    }else{
                        if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                            present(resultController, animated: true, completion: nil)
                        }
                        
                    }
                   // }
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
            
        }else{
            if self.AuditResult == "Optional(\"P\")"{
                self.AuditResult = "P"
            }
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodein!)&ShipQty=\(self.ShipQuantity2.text!)&ReScreenQty=\(self.ReScreenQuantity2.text!)&TotalCartons=\(self.TotalCartons.text!)&CartonsShipped=\(self.NoOfShippedCartons.text!)&CartonsInspected=\(self.NoOfCartonsInspected.text!)&InspectionStatus=\(self.InspectionStatus_TextField.text!)&Knitted=\(self.Knitted_TextField.text!)&Dyed=\(self.Dyed_TextField.text!)&Cutting=\(self.Cutting_TextField.text!)&Sewing=\(self.Sewing_TextField.text!)&Finishing=\(self.Finishing_TextField.text!)&Packing=\(Packed_TextField.text!)&AuditResult=\(self.AuditResult!)&Comments=\(self.Comments_Arcadia.text!)"
            
            
            
            print("AuditCommentsControlist-bodydata -> \(self.bodyData)")
            
            
            
            var AuditCommentsOfflineData = [String:String]()
            
            
            let randomString = randomStringWithLength(6)
            
            AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "AuditComments_bodyData\(self.AuditCode.text!)_\(randomString)")
            AuditCommentsOfflineData.updateValue("http://app.3-tree.com/quonda/save-comments.php", forKey: "AuditComment_url")
            
            
            if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    // first read out the file
                    //                    do{
                    //                        let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
                    //                        let jsonSyncFilePath = documentsDirectoryPath.URLByAppendingPathComponent("FilesToSync.json")
                    //
                    //                        var readString: String?
                    //                        readString = try NSString(contentsOfFile: jsonSyncFilePath.path!, encoding: NSUTF8StringEncoding) as String
                    //                        print("sync file after adding comments =",readString)
                    //                    }catch{
                    //                        print("cannot read")
                    //                    }
                    
                    
                    //Note!!
                    
                    //BEFORE MOVING ONTO THE NEXT SCREEN YOU HAVE TO UPDATE AUDITCOMPLETED VALUE IN AUDITMODE.JSON
                    
                    /*     let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
                     
                     let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("AuditMode.json")
                     do {
                     var readString: String
                     readString = try NSString(contentsOfFile: jsonFilePath.path!, encoding: NSUTF8StringEncoding) as String
                     print("readString_AuditorModeVC=\(readString)")
                     self.updateAuditorModeVCDataWhenOffline(readString)
                     } catch let error as NSError {
                     print(error.description)
                     }
                     */
                    
                    
                    // now move onto the AuditorModeViewController screen
                    //?????
                    //???
                    //??
                    
                    //var SavedDefects = defaults.object(forKey: "\(self.AuditCode.text!)") as? [((Color:UIColor, ID:String),(DefectType:String,DefectCode:String))]
                    
                    
                    // if self.arrayOfDefectsLoggedRecieved != nil{
                    //if SavedDefects != nil {
                        
                        //performSegue(withIdentifier: "CAP", sender: nil)
                        
                    // }else {
                        if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                            if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                                present(resultController, animated: true, completion: nil)
                            }
                        }else{
                            if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                                present(resultController, animated: true, completion: nil)
                            }
                            
                        }
                    
                    ///}
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
        }
        
        // - - -
        
    }
    
    
    func AuditCommentAuditCodeFile_Hybrid(_ savedCommentsData:String?, AuditCommentFilePath: String) -> Void{
        
        // - - - -
        
        if savedCommentsData != nil && savedCommentsData != "" {
            
            if workmanshipResultValue == nil{
                
                workmanshipResultValue = "F"
            }
            if self.AuditResult == nil{
                
                self.AuditResult = "F"
            }
            if self.ShipDate.titleLabel?.text == nil{
                
                self.ShipDate.titleLabel?.text = ""
                //self.ShipDate.setTitle("", for: UIControlState())
            }
          
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodein!)&AuditResult=\(self.AuditResult!)&Comments=\(self.Comments_Arcadia.text!)&ShipQty=\(self.ShipQuantity3.text!)&ShipmentDate=\(self.ShipDate.titleLabel!.text!)&AssortmentCartonQty=\(self.AssortmentCartonQty.text!)&AssortmentSizeQty=\(self.AssortmentCartonSize.text!)&SolidSizeQty=\(self.SolidSizeQty.text!)&SolidSizeType=&WorkmanshipResult=\(self.workmanshipResultValue!)"
            
            print("AuditCommentsHybrid-bodydata -> \(self.bodyData)")
            
            
            
            var AuditCommentsOfflineData:[String:String] = parseCommentsJSON(savedCommentsData!)
            
            
            let randomString = randomStringWithLength(6)
            
            AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "AuditComments_bodyData\(self.AuditCode.text!)_\(randomString)")
            AuditCommentsOfflineData.updateValue("http://app.3-tree.com/quonda/save-comments.php", forKey: "AuditComment_url")
            
            
            if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    //Note!!
                    
                    if !Reachability.isConnectedToNetwork(){
                        
                        //BEFORE MOVING ONTO THE NEXT SCREEN YOU HAVE TO UPDATE AUDITCOMPLETED VALUE IN AUDITMODE.JSON
                        
                        /*   let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
                         
                         let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("AuditMode.json")
                         do {
                         var readString: String
                         readString = try NSString(contentsOfFile: jsonFilePath.path!, encoding: NSUTF8StringEncoding) as String
                         print("readString_AuditorModeVC=\(readString)")
                         self.updateAuditorModeVCDataWhenOffline(readString)
                         } catch let error as NSError {
                         print(error.description)
                         }
                         */
                    }
                    
                    
                    // now move onto the Next screen
                    //???????
                    //?????
                    //???
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    
                    if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                        if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                            present(resultController, animated: true, completion: nil)
                        }
                    }else{
                        if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                            present(resultController, animated: true, completion: nil)
                        }
                        
                    }
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
            
        }else{
            
            if workmanshipResultValue == nil{
                
                workmanshipResultValue = "F"
            }
            if self.AuditResult == nil{
                
                self.AuditResult = "F"
            }
            if self.ShipDate.titleLabel?.text == nil{
                self.ShipDate.titleLabel?.text = ""
               // self.ShipDate.setTitle("", for: UIControlState())
            }
            
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodein!)&AuditResult=\(self.AuditResult!)&Comments=\(self.Comments_Arcadia.text!)&ShipQty=\(self.ShipQuantity3.text!)&ShipmentDate=\(self.ShipDate.titleLabel!.text!)&AssortmentCartonQty=\(self.AssortmentCartonQty.text!)&AssortmentSizeQty=\(self.AssortmentCartonSize.text!)&SolidSizeQty=\(self.SolidSizeQty.text!)&SolidSizeType=&WorkmanshipResult=\(self.workmanshipResultValue!)"
            
            print("AuditCommentsHybrid-bodydata -> \(self.bodyData)")
            

            
            
            
            var AuditCommentsOfflineData = [String:String]()
            
            
            let randomString = randomStringWithLength(6)
            
            AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "AuditComments_bodyData\(self.AuditCode.text!)_\(randomString)")
            AuditCommentsOfflineData.updateValue("http://app.3-tree.com/quonda/save-comments.php", forKey: "AuditComment_url")
            
            
            if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    // first read out the file
                    //                    do{
                    //                        let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
                    //                        let jsonSyncFilePath = documentsDirectoryPath.URLByAppendingPathComponent("FilesToSync.json")
                    //
                    //                        var readString: String?
                    //                        readString = try NSString(contentsOfFile: jsonSyncFilePath.path!, encoding: NSUTF8StringEncoding) as String
                    //                        print("sync file after adding comments =",readString)
                    //                    }catch{
                    //                        print("cannot read")
                    //                    }
                    
                    
                    //Note!!
                    
                    //BEFORE MOVING ONTO THE NEXT SCREEN YOU HAVE TO UPDATE AUDITCOMPLETED VALUE IN AUDITMODE.JSON
                    
                    /*     let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
                     
                     let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("AuditMode.json")
                     do {
                     var readString: String
                     readString = try NSString(contentsOfFile: jsonFilePath.path!, encoding: NSUTF8StringEncoding) as String
                     print("readString_AuditorModeVC=\(readString)")
                     self.updateAuditorModeVCDataWhenOffline(readString)
                     } catch let error as NSError {
                     print(error.description)
                     }
                     */
                    
                    
                    // now move onto the AuditorModeViewController screen
                    //?????
                    //???
                    //??
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    
                    
                    if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                        if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                            present(resultController, animated: true, completion: nil)
                        }
                    }else{
                        if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                            present(resultController, animated: true, completion: nil)
                        }
                        
                    }
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
        }
        
        // - - -
        
    }
    
    //Random string function
    
    func randomStringWithLength(_ len: Int) -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for i in 0 ..< (len){
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString
    }

    
    
    func updateAuditorModeVCDataWhenOffline(_ savedData:String?) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        print("savedDataString=\(savedData)")
        
        if savedData != nil {
            
            self.MutableDictionary = self.JSON_To_FoundationObj(savedData!)
            
            var planned, completed, pending:Int!
            planned = (self.MutableDictionary["Today"]!as! [String: AnyObject])["Planned"] as! Int
            completed = (self.MutableDictionary["Today"]!as! [String: AnyObject])["Completed"] as! Int
            pending = (self.MutableDictionary["Today"]!as! [String: AnyObject])["Pending"] as! Int
            
            print("Total Audits = \(self.MutableDictionary["Audits"]!)")
            
//            print("OOOOOOOOoooooooo = \(self.MutableDictionary["Audits"]![0]["AuditCode"]!)")
//            print((self.MutableDictionary["Audits"]![0] as! [String: AnyObject])["Completed"]!)
            
            
            if var All_Audits = self.MutableDictionary["Audits"]! as? [[String : AnyObject]] {
                
                var i = 0
                for Audit in All_Audits{
                    
                    let AuditCodeName = Audit["AuditCode"]! as! String
                    print("AuditorModeVC_AuditCode= \(AuditCodeName)")
                    
                    let completedAuditt = Audit["Completed"]! as! String
                    print("Before =",completedAuditt)

                    if self.AuditCode.text! == AuditCodeName {
                        print("audit codes are same.")
                        All_Audits[i]["Completed"] = "Y" as AnyObject?
                        self.MutableDictionary["Audits"] = All_Audits
                        
                        //Now its Time to save this update in AuditMode.json file
                        saveOfflineAuditorModeVCData()
                    }
                    
                    i += 1
                }
            }
            print("After changing to Completed = ")
//            print(self.MutableDictionary["Audits"]![0]["Completed"]!)
            
//            AuditMode.json

        }
    }
    
    
    func saveOfflineAuditorModeVCData(){
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("AuditMode.json")
        
        
        if self.fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &self.isDirectory) {
            do{
                let jsonDictionary = try JSONSerialization.data(withJSONObject: self.MutableDictionary, options: JSONSerialization.WritingOptions.prettyPrinted)
                
                let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                
                try json.write(toFile: jsonFilePath.path, atomically: true, encoding: String.Encoding.utf8)
           
                
            } catch let error as NSError {
                print("AuditMode.json data couldn't be written after update // File not exist")
                print(error.description)
            }
        }
        
        
        //Reading from JsonDictionary File stored in documents directory
        do{
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath.path, encoding: String.Encoding.utf8.rawValue) as String
            //                    print("readString=\(readString)")
            let readStringParsed = parseJSON(readString)
            print("AuditorMode in AuditComments_readStringParsed = ",readStringParsed)
        }
        catch let error as NSError{
            print("There is an error while reading from JsonDictionary File.",error.description)
            
        }
        
        
        
        
        
    }// end of func

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // CAP
        if segue.identifier == "CAP"{
        
            let destinationVC = segue.destination as! CorrectiveActionPlanViewController
          
            //destinationVC.arrayOfDefectsLoggedRecieved_FromCommentsView = self.arrayOfDefectsLoggedRecieved!
            destinationVC.AuditCodeIn = self.AuditCode.text!
            destinationVC.AuditStageIn = self.AuditStage.text!
            destinationVC.AuditStageColor = self.AuditStage.backgroundColor
//            AuditCodeIn
//            AuditStageIn
//            AuditStageColor
//            arrayOfDefectsLoggedRecieved_FromCommentsView
            
        }
        
        if segue.identifier == "goto Final comments"{
            let destinationVC = segue.destination as! FinalRemarksViewController
            destinationVC.AuditCodein = AuditCodein
            destinationVC.AuditStagein = self.AuditStage.text!
            destinationVC.AuditStageColorin = self.AuditStage.backgroundColor!
            print(ReportID_against_current_audit)
            // here send ReportId to commentsVC for hide/show between two views in commentsView.
            destinationVC.ReportID_against_current_audit = self.ReportID_against_current_audit
            
            
           // present(destinationVC, animated: true, completion: nil)
            
        }

        //goToAuditorModeViewC
        
        //goto Final comments
        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    func saveSelectedSize(_ strText: String, strID: String) {
        print(strText,strID)
        
    }
    
    
    
    
    
    //MARK: Location update
    
    let locationManager = CLLocationManager()
    
    func sendAuditorLocation() {
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        print("loaction config called")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        print("coordinate =",locations.first?.coordinate)
        
        let latitudeLabel = String(locations.first!.coordinate.latitude)
        let longitudeLabel = String(locations.first!.coordinate.longitude)
        print("latitudeLabel =",latitudeLabel)
        print("longitudeLabel =",longitudeLabel)
        
        self.saveLatitudeLongitude(latitudeLabel, longitudeLabel: longitudeLabel)
        
        //        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error) -> Void in
        //            if (error != nil) {
        //                print("ERROR:" + error!.localizedDescription)
        //                return
        //            }
        //            print("placemarks!.count = ",placemarks!.count)
        //
        //            if placemarks!.count > 0 {
        //                let pm = placemarks![0]
        //                self.saveLatitudeLongitude(pm)
        //                print("called after saveLatitude")
        //            } else {
        //                print("Problem with the data received from geocoder")
        //            }
        //
        //        })
    }
    var locationBodyData = ""
    func saveLatitudeLongitude(_ latitudeLabel:String, longitudeLabel:String){ //(placemark: CLPlacemark) {
        print("lat =", latitudeLabel)
        print("long =", longitudeLabel)
        locationBodyData = "&Latitude=\(latitudeLabel)&Longitude=\(longitudeLabel)"
        
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error:" + error.localizedDescription)
    }
    
    //Locations

    
    
    
    ////Naeem Added Code
    
    
    
    
    
    
    
    ////  SAVE IMAGE START
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileLibraryDirectory(_ filename: String) -> String {
        
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL.path
        
    }
    func loadImageFromPath(_ path: String) -> UIImage? {
        
        let image = UIImage(contentsOfFile: path)
        
        if image == nil {
            
            print("missing image at: \(path)")
        }
        print("Loading image from path: \(path)") // this is just for you to see the path in case you want to go to the directory, using Finder.
        return image
        
    }
    
    
    func SAVE_pressed() {
        
        savedParamPath = nil
        print("sampleImage=")
        print(self.sampleImage.image)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        if (self.sampleImage.image != UIImage(named: "default.png")){
            print("every thing is ok")
        }
        if self.sampleImage.image != UIImage(named: "default.png"){
            
            //Now save to arrayOfDeffectsLogged
            
            self.sampleImage.isUserInteractionEnabled = false
            //get image from Library Dir
            let imagePath = fileLibraryDirectory("myCapturedImage.jpg")  // this path is in string format
            print(imagePath)
            let CapImage = loadImageFromPath(imagePath)
            var CapUIImage:UIImage?
            
            //        if CapUIImage == nil{
            //            print("saved image is nill")
            //        }else{
            CapUIImage = CapImage
            //print(CapUIImage)
            
            let defaultImage = UIImage(named: "default")
            let jpgImageData = UIImageJPEGRepresentation(defaultImage!, 0.8)
            let CapUIImageData = UIImageJPEGRepresentation(defaultImage!, 0.8)
            
            if jpgImageData == CapUIImageData{
                print("same image as sample image")
            }
            
            if savedParamPath != nil{
                
                //                ImageUpload(NSURL(string: imagePath)!, destinationFileName: savedParamPath)
                
            }
            else{
                let randomString = randomStringWithLength(6)
                print("randomString=")
                print(randomString)
                let url: URL = URL(string: imagePath)!
                
                let Name1 = "\(self.AuditCode.text!)"+"_"+otherImagesType!+"_"+"\(randomString).jpg"
                print(Name1)
                let destImageName = Name1
                //"AUDIT_CODE_DEFECT_CODE_AREA_CODE_RANDOM_NO.jpg"
                //            let path = "/quonda/\(NSUUID().UUIDString).jpg"
                
                //            paramPath = paramPath+"/"+"\(NSUUID().UUIDString).jpg"
                paramPath = paramPath+"/"+destImageName
                print("paramPath =")
                print(paramPath)
                
                savedParamPath = paramPath
                
                
                //Now Check if Internet connection is not available then save images and other data to Documents directory
                //?????
                //????
                //??
                //?
                
                //If Internet is available or Not just save the data offline.
                //First Stop sync Service Then save data
                
                syncObject.endBackgroundTask()
                updateTimer?.invalidate()
                syncObject = nil
                
                saveDataInDocumentsDir(imagePath, destinationFileName: destImageName)
                
                
                //NOW START Sync Service again
                
                syncObject = syncService()
                
                
                
                /*
                 if !Reachability.isConnectedToNetwork(){
                 
                 print("Internet is not available in AddDefect screen.")
                 
                 // imagePath: is a path of saved image.
                 //paramPath: will be the path of server, where image has to save.
                 
                 saveDataInDocumentsDir(imagePath, destinationFileName: destImageName)
                 
                 }else{
                 
                 ImageUpload(url, destinationFileName: paramPath)
                 }
                 */
                
            }
            
        }else{
            
            var SI = ""
            
            if self.sampleImage.image == UIImage(named: "default.png"){
                SI = "SamplePhoto "
            }
            let alertView = UIAlertController(title: "Note!" , message: "\(SI) Cannot remain empty", preferredStyle: .alert)
            let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            })
            alertView.addAction(ok_action)
            
            self.present(alertView, animated: true, completion: nil)
            
        }
    }
    
    func saveDataInDocumentsDir(_ imagePath:String!, destinationFileName: String!){
        
        print("You are in AddDeffect OFFLINE mode.")
        
        // Creating Json file
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("AddDefectImagesFile\(self.AuditCode.text!).json")
        
        
        // creating a AddDefectImagesFile.json file in the Documents folder
        
        if !fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("AddDefectImagesFile.json File created ")
                
                // Now save it to FilesToSync.json file
                
                saveFileNameTo_FilesToSync("AddDefectImagesFile\(self.AuditCode.text!).json")
                
                //(jsonFilePath.absoluteString)
                
                
            } else {
                print("Couldn't create file for some reason")
            }
        }
        else {
            
            print("AddDefectImagesFile.json File already exists")
        }
        
        //file creation ends
        
        
        
        let DisplayedImage = self.sampleImage.image!.resize(0.45)
        
        if let data = UIImageJPEGRepresentation(DisplayedImage, 0.8) {
            let Imagefilename = getDocumentsDirectory().appendingPathComponent(destinationFileName)
            let ImagePathsfile = getDocumentsDirectory().appendingPathComponent("AddDefectImagesFile\(self.AuditCode.text!).json")
            
            if !self.fileManager.fileExists(atPath: Imagefilename, isDirectory: &self.isDirectory) {
                
                try? data.write(to: URL(fileURLWithPath: Imagefilename), options: [.atomic])
                
                // first read out the file to add this image in AddDefectImagesFile\(AuditCode)
                
                do{
                    var readString: String
                    readString = try NSString(contentsOfFile: ImagePathsfile, encoding: String.Encoding.utf8.rawValue) as String
                    
                    print("readAuditProgress = ",readString)
                    
                    /// here send readString to function to update values stored in AddDeffectImagesFile(AuditCode) File
                    
                    self.AddDeffectImagesInAuditCodeFile(readString, imgFileName: destinationFileName, destinationFileName:destinationFileName)
                }
                catch let error as NSError{
                    print("There is an error while reading AuditProgress(AuditCode) File.",error.description)
                }
                
                
                //                AddDeffectOfflineImageData.append(Imagefilename) //updateValue(Imagefilename, forKey: "fds")
                //
                //                print("AddDeffectOfflineImageData =",AddDeffectOfflineImageData)
                //
                //                do{
                //                    try "\(AddDeffectOfflineImageData)".writeToFile(ImagePathsfile, atomically: true, encoding: NSUTF8StringEncoding)
                //
                //                    //Now save text data into the document directory.
                //                    saveOfflineTextData(destinationFileName)
                //
                //                }
                //                catch{
                //                    print("threre may be an error in writing AddDeffectOfflineImageData.")
                //                }
                //
                //
                //                //Reading from JsonDictionary File stored in documents directory
                //                do{
                //                    var readString: String
                //                    readString = try NSString(contentsOfFile: ImagePathsfile, encoding: NSUTF8StringEncoding) as String
                //                    //                    print("readString=\(readString)")
                ////                    let readStringParsed = parseJSON(readString)
                //                    print("readAddDefect_ImageString = ",readString)
                //                }
                //                catch let error as NSError{
                //                    print("There is an error while reading from JsonDictionary File.",error.description)
                //
                //                }
                
            }
        }
        
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        //        //Reading from JsonDictionary File stored in documents directory
        //        do{
        //            var readString: String
        //            readString = try NSString(contentsOfFile: ImagePathsfile, encoding: NSUTF8StringEncoding) as String
        //            //                    print("readString=\(readString)")
        //            let readStringParsed = parseJSON(readString)
        //            print("readAddDefect_StringParsed = ",readStringParsed)
        //        }
        //        catch let error as NSError{
        //            print("There is an error while reading from JsonDictionary File.",error.description)
        //
        //        }
        
    }
    func parseAddDefectImagesFile(_ jsonString: String) -> [String:String] {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:String] {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    
    
    func AddDeffectImagesInAuditCodeFile(_ savedData: String?, imgFileName:String, destinationFileName:String!){
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        let jsonFilePath = getDocumentsDirectory().appendingPathComponent("AddDefectImagesFile\(self.AuditCode.text!).json")
        
        if savedData != nil && savedData != "" {
            
            var AddDeffectOfflineImageData:[String:String] = parseAddDefectImagesFile(savedData!)
            
            AddDeffectOfflineImageData.updateValue(imgFileName, forKey: destinationFileName)
            //            AddDeffectOfflineImageData.append(imgFileName)
            
            // Now write into the AuditProgress(AuditCode) file
            
            if self.fileManager.fileExists(atPath: jsonFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AddDeffectOfflineImageData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: jsonFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    //Now save text data into the document directory.
                    if(destinationFileName.contains("LAB")){
                        saveOfflineTextDataForLab(destinationFileName)
                    }else{
                        saveOfflineTextData(destinationFileName)
                    }
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
            
        }else{
            
            var AddDeffectOfflineImageData = [String:String]()
            
            AddDeffectOfflineImageData.updateValue(imgFileName, forKey: destinationFileName)
            //            AddDeffectOfflineImageData.append(imgFileName)
            
            // Now write into the AuditProgress(AuditCode) file
            
            if self.fileManager.fileExists(atPath: jsonFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AddDeffectOfflineImageData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: jsonFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    //Now save text data into the document directory.
                    //saveOfflineTextData(destinationFileName)
                    if(destinationFileName.contains("LAB")){
                        saveOfflineTextDataForLab(destinationFileName)
                    }else{
                        saveOfflineTextData(destinationFileName)
                    }
                    
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        //Reading from JsonDictionary File stored in documents directory
        do{
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath, encoding: String.Encoding.utf8.rawValue) as String
            
            let readStringParsed = parseAddDefectImagesFile(readString)
            print("readAddDefectImagesStringParsed = ",readStringParsed)
        }
        catch let error as NSError{
            print("There is an error while reading from JsonDictionary File.",error.description)
            
        }
        
    }
    //LAB Report Json
    
    func saveOfflineTextDataForLab(_ destinationFileName:String!){
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        let jsonFilePath2 = documentsDirectoryPath.appendingPathComponent("AddTextAuditCodeFileForLab\(self.AuditCode.text!).json")
        // creating a AddDeffectTextFile.json file in the Documents folder
        
        if !fileManager.fileExists(atPath: jsonFilePath2.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath2.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("AddTextAuditCodeFile.json File created ")
                
                // Now save it to FilesToSync.json file
                
                saveFileNameTo_FilesToSync("AddTextAuditCodeFileForLab\(self.AuditCode.text!).json")
                //(jsonFilePath2.absoluteString)
                
                
            } else {
                print("Couldn't create file for some reason")
            }
        }
        else {
            
            print("AddTextAuditCodeFile.json File already exists")
        }
        
        
        
        
        // First check before saving audit
        
        //        if self.PICK_DEFECT_TYPE.text != "DEFECT TYPE" && self.PICK_DEFECT_CODE.text != "DEFECT CODE" && self.PICK_DEFECT_CODE.text != "" && self.PICK_DEFECT_AREA.text != "DEFECT AREA" {
        //
        
        //First Read out the AddTextAuditCodeFile
        
        do{
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath2.absoluteString, encoding: String.Encoding.utf8.rawValue) as String
            
            print("readAddDefect_TextDataString = ",readString)
            
            /// here send readString to function to update values stored in AddDeffectText(AuditCode) File
            
            self.AddDeffectTextDataAuditCodeFileForLab(readString, TextDataFilePath: jsonFilePath2.absoluteString, destinationFileName: destinationFileName)
            
        }
        catch let error as NSError{
            print("There is an error while reading AddDeffectText(AuditCode) File.",error.description)
        }
        
        // }
        
    }
    
    
    
    //End Lab Report Json
    func saveOfflineTextData(_ destinationFileName:String!){
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        let jsonFilePath2 = documentsDirectoryPath.appendingPathComponent("AddTextAuditCodeFile\(self.AuditCode.text!).json")
        // creating a AddDeffectTextFile.json file in the Documents folder
        
        if !fileManager.fileExists(atPath: jsonFilePath2.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath2.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("AddTextAuditCodeFile.json File created ")
                
                // Now save it to FilesToSync.json file
                
                saveFileNameTo_FilesToSync("AddTextAuditCodeFile\(self.AuditCode.text!).json")
                //(jsonFilePath2.absoluteString)
                
                
            } else {
                print("Couldn't create file for some reason")
            }
        }
        else {
            
            print("AddTextAuditCodeFile.json File already exists")
        }
        
        
        
        
        // First check before saving audit
        
        //        if self.PICK_DEFECT_TYPE.text != "DEFECT TYPE" && self.PICK_DEFECT_CODE.text != "DEFECT CODE" && self.PICK_DEFECT_CODE.text != "" && self.PICK_DEFECT_AREA.text != "DEFECT AREA" {
        //
        
        //First Read out the AddTextAuditCodeFile
        
        do{
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath2.absoluteString, encoding: String.Encoding.utf8.rawValue) as String
            
            print("readAddDefect_TextDataString = ",readString)
            
            /// here send readString to function to update values stored in AddDeffectText(AuditCode) File
            
            self.AddDeffectTextDataAuditCodeFile(readString, TextDataFilePath: jsonFilePath2.absoluteString, destinationFileName: destinationFileName)
            
        }
        catch let error as NSError{
            print("There is an error while reading AddDeffectText(AuditCode) File.",error.description)
        }
        
        // }
    }
    
    func parseAddDefectTextDataJSON(_ jsonString: String) -> [String:String] {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:String] {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    //SAVE LAB IMAGES
    
    func AddDeffectTextDataAuditCodeFileForLab(_ savedTextData:String?, TextDataFilePath:String, destinationFileName:String!) -> Void {
        print("savedData =")
        print(savedTextData!)
        print("TextDataFilePath =")
        print(TextDataFilePath)
        print("destinationFileName =")
        print(destinationFileName!)
        var imageName = ""
        var imageUrl = ""
        if savedTextData != nil && savedTextData != "" {
            
            //            self.endDate = Date()
            //            self.convertedEndDate = defaults.object(forKey: self.AuditCode.text!)as! String
            //            //= self.endDate.CustomDateTimeformat
            //            print(self.convertedEndDate)
            print(destinationFileName!)
            // self.showalertPath(messsage: destinationFileName!)
            //if(destinationFileName!.contains("LAB")){
            
            imageName = "&SpecsSheets=\(destinationFileName!)"
            imageUrl = "http://app.3-tree.com/quonda/save-specs-sheet.php"
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCode.text!)\(imageName)"
            
            print("AddDefect-Offline_bodydata-> \(self.bodyData)")
            
            
            var AddDeffectOfflineTextData:[String:String] = parseAddDefectTextDataJSON(savedTextData!)
            
            AddDeffectOfflineTextData.updateValue(bodyData, forKey: "AddDeffect_bodyData\(destinationFileName!)")
            AddDeffectOfflineTextData.updateValue(imageUrl, forKey: "AddDeffect_url_LAB")
            if self.fileManager.fileExists(atPath: TextDataFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AddDeffectOfflineTextData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: TextDataFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to AddTextAuditCodeFile // File not exist")
                    print(error.description)
                }
            }
            
            
            
            // }
            
            
            
            
            
            // Reset all and dismiss current view
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.savedParamPath = nil
            
            DefectsData0.removeAll()
            DefectsRateData0.removeAll()
            DefectsAreaData0.removeAll()
            
            // self.dismiss(animated: false, completion: nil)
            
        }else{
            
            
            //            self.endDate = Date()
            //            self.convertedEndDate = defaults.object(forKey: self.AuditCodeLabel.text!)as! String  //= self.endDate.CustomDateTimeformat
            //            print(self.convertedEndDate)
            print(destinationFileName!)
            // if(destinationFileName!.contains("LAB")){
            imageName = "&SpecsSheets=\(destinationFileName!)"
            imageUrl = "http://app.3-tree.com/quonda/save-specs-sheet.php"
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCode.text!)\(imageName)"
            
            //            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCodeLabel.text!)&ReportId=\(self.ReportId!)&DefectCode=\(self.DefectCodeID)&DefectArea=\(self.DefectedAreaID)&Nature=\(self.MajorORMinor!)&SampleNo=\(self.SampleChecked.text!)&DateTime=\(self.convertedEndDate!)&Remarks=\(self.commentsField.text!)"
            
            print("AddDefect-Offline_bodydata-> \(self.bodyData)")
            
            
            var AddDeffectOfflineTextData = [String:String]()
            
            AddDeffectOfflineTextData.updateValue(bodyData, forKey: "AddDeffect_bodyData\(destinationFileName!)")
            AddDeffectOfflineTextData.updateValue(imageUrl, forKey: "AddDeffect_url_LAB")
            
            if self.fileManager.fileExists(atPath: TextDataFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AddDeffectOfflineTextData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: TextDataFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
            
            
            //  }
            
            //            AddDeffectOfflineTextData.updateValue(bodyData, forKey: "AddDeffect_bodyData\(destinationFileName!)")
            //            AddDeffectOfflineTextData.updateValue(imageUrl, forKey: "AddDeffect_url")
            
            
            
            // Reset all and dismiss current view
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            //            self.butotn.isEnabled = true
            //            self.butotn.backgroundColor = UIColor.lightGray
            //
            self.savedParamPath = nil
            
            DefectsData0.removeAll()
            DefectsRateData0.removeAll()
            DefectsAreaData0.removeAll()
            
            // self.dismiss(animated: false, completion: nil)
            
            
        }
        
        //Reading from JsonDictionary File stored in documents directory
        do{
            var readString: String
            readString = try NSString(contentsOfFile: TextDataFilePath, encoding: String.Encoding.utf8.rawValue) as String
            //                    print("readString=\(readString)")
            let readStringParsed = parseAddDefectTextDataJSON(readString)
            print("AddDefectText_readStringParsed = ",readStringParsed)
        }
        catch let error as NSError{
            print("There is an error while reading from AddTextAuditCodeFile.",error.description)
            
        }
        
    }
    
    
    
    
    //END SAVING LAB IMAGES
    
    func AddDeffectTextDataAuditCodeFile(_ savedTextData:String?, TextDataFilePath:String, destinationFileName:String!) -> Void {
        print("savedData =")
        print(savedTextData!)
        print("TextDataFilePath =")
        print(TextDataFilePath)
        print("destinationFileName =")
        print(destinationFileName!)
        var imageName = ""
        var imageUrl = ""
        if savedTextData != nil && savedTextData != "" {
            
            //            self.endDate = Date()
            //            self.convertedEndDate = defaults.object(forKey: self.AuditCode.text!)as! String
            //            //= self.endDate.CustomDateTimeformat
            //            print(self.convertedEndDate)
            print(destinationFileName!)
            // self.showalertPath(messsage: destinationFileName!)
            if(destinationFileName!.contains("LAB")){
                
                imageName = "&SpecsSheets=\(destinationFileName!)"
                imageUrl = "http://app.3-tree.com/quonda/save-specs-sheet.php"
                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCode.text!)\(imageName)"
                
                print("AddDefect-Offline_bodydata-> \(self.bodyData)")
                
                
                var AddDeffectOfflineTextData:[String:String] = parseAddDefectTextDataJSON(savedTextData!)
                
                AddDeffectOfflineTextData.updateValue(bodyData, forKey: "AddDeffect_bodyData\(destinationFileName!)")
                AddDeffectOfflineTextData.updateValue(imageUrl, forKey: "AddDeffect_url_LAB")
                if self.fileManager.fileExists(atPath: TextDataFilePath, isDirectory: &self.isDirectory) {
                    do{
                        let jsonDictionary = try JSONSerialization.data(withJSONObject: AddDeffectOfflineTextData, options: JSONSerialization.WritingOptions.prettyPrinted)
                        
                        let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                        
                        try json.write(toFile: TextDataFilePath, atomically: true, encoding: String.Encoding.utf8)
                        
                    } catch let error as NSError {
                        print("JSON data couldn't written to AddTextAuditCodeFile // File not exist")
                        print(error.description)
                    }
                }
                
                
                
            }
            else{
                
                imageName = "&Image=\(destinationFileName!)"
                imageUrl = "http://app.3-tree.com/quonda/save-report-image.php"
                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCode.text!)\(imageName)"
                print("AddDefect-Offline_bodydata-> \(self.bodyData)")
                var AddDeffectOfflineTextData:[String:String] = parseAddDefectTextDataJSON(savedTextData!)
                
                AddDeffectOfflineTextData.updateValue(bodyData, forKey: "AddDeffect_bodyData\(destinationFileName!)")
                AddDeffectOfflineTextData.updateValue(imageUrl, forKey: "AddDeffect_url_PACK")
                if self.fileManager.fileExists(atPath: TextDataFilePath, isDirectory: &self.isDirectory) {
                    do{
                        let jsonDictionary = try JSONSerialization.data(withJSONObject: AddDeffectOfflineTextData, options: JSONSerialization.WritingOptions.prettyPrinted)
                        
                        let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                        
                        try json.write(toFile: TextDataFilePath, atomically: true, encoding: String.Encoding.utf8)
                        
                    } catch let error as NSError {
                        print("JSON data couldn't written to AddTextAuditCodeFile // File not exist")
                        print(error.description)
                    }
                }
                
                
                
            }
            
            
            
            
            // Reset all and dismiss current view
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.savedParamPath = nil
            
            DefectsData0.removeAll()
            DefectsRateData0.removeAll()
            DefectsAreaData0.removeAll()
            
            // self.dismiss(animated: false, completion: nil)
            
        }else{
            
            
            //            self.endDate = Date()
            //            self.convertedEndDate = defaults.object(forKey: self.AuditCodeLabel.text!)as! String  //= self.endDate.CustomDateTimeformat
            //            print(self.convertedEndDate)
            print(destinationFileName!)
            if(destinationFileName!.contains("LAB")){
                imageName = "&SpecsSheets=\(destinationFileName!)"
                imageUrl = "http://app.3-tree.com/quonda/save-specs-sheet.php"
                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCode.text!)\(imageName)"
                
                //            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCodeLabel.text!)&ReportId=\(self.ReportId!)&DefectCode=\(self.DefectCodeID)&DefectArea=\(self.DefectedAreaID)&Nature=\(self.MajorORMinor!)&SampleNo=\(self.SampleChecked.text!)&DateTime=\(self.convertedEndDate!)&Remarks=\(self.commentsField.text!)"
                
                print("AddDefect-Offline_bodydata-> \(self.bodyData)")
                
                
                var AddDeffectOfflineTextData = [String:String]()
                
                AddDeffectOfflineTextData.updateValue(bodyData, forKey: "AddDeffect_bodyData\(destinationFileName!)")
                AddDeffectOfflineTextData.updateValue(imageUrl, forKey: "AddDeffect_url_LAB")
                
                if self.fileManager.fileExists(atPath: TextDataFilePath, isDirectory: &self.isDirectory) {
                    do{
                        let jsonDictionary = try JSONSerialization.data(withJSONObject: AddDeffectOfflineTextData, options: JSONSerialization.WritingOptions.prettyPrinted)
                        
                        let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                        
                        try json.write(toFile: TextDataFilePath, atomically: true, encoding: String.Encoding.utf8)
                        
                    } catch let error as NSError {
                        print("JSON data couldn't written to file // File not exist")
                        print(error.description)
                    }
                }
                
                
            }
            else{
                
                imageName = "&Image=\(destinationFileName!)"
                imageUrl = "http://app.3-tree.com/quonda/save-report-image.php"
                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCode.text!)\(imageName)"
                
                //            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCodeLabel.text!)&ReportId=\(self.ReportId!)&DefectCode=\(self.DefectCodeID)&DefectArea=\(self.DefectedAreaID)&Nature=\(self.MajorORMinor!)&SampleNo=\(self.SampleChecked.text!)&DateTime=\(self.convertedEndDate!)&Remarks=\(self.commentsField.text!)"
                
                print("AddDefect-Offline_bodydata-> \(self.bodyData)")
                
                
                var AddDeffectOfflineTextData = [String:String]()
                
                AddDeffectOfflineTextData.updateValue(bodyData, forKey: "AddDeffect_bodyData\(destinationFileName!)")
                AddDeffectOfflineTextData.updateValue(imageUrl, forKey: "AddDeffect_url_PACK")
                if self.fileManager.fileExists(atPath: TextDataFilePath, isDirectory: &self.isDirectory) {
                    do{
                        let jsonDictionary = try JSONSerialization.data(withJSONObject: AddDeffectOfflineTextData, options: JSONSerialization.WritingOptions.prettyPrinted)
                        
                        let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                        
                        try json.write(toFile: TextDataFilePath, atomically: true, encoding: String.Encoding.utf8)
                        
                    } catch let error as NSError {
                        print("JSON data couldn't written to file // File not exist")
                        print(error.description)
                    }
                }
                
                
                
            }
            
            //            AddDeffectOfflineTextData.updateValue(bodyData, forKey: "AddDeffect_bodyData\(destinationFileName!)")
            //            AddDeffectOfflineTextData.updateValue(imageUrl, forKey: "AddDeffect_url")
            
            
            
            // Reset all and dismiss current view
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            //            self.butotn.isEnabled = true
            //            self.butotn.backgroundColor = UIColor.lightGray
            //
            self.savedParamPath = nil
            
            DefectsData0.removeAll()
            DefectsRateData0.removeAll()
            DefectsAreaData0.removeAll()
            
            // self.dismiss(animated: false, completion: nil)
            
            
        }
        
        //Reading from JsonDictionary File stored in documents directory
        do{
            var readString: String
            readString = try NSString(contentsOfFile: TextDataFilePath, encoding: String.Encoding.utf8.rawValue) as String
            //                    print("readString=\(readString)")
            let readStringParsed = parseAddDefectTextDataJSON(readString)
            print("AddDefectText_readStringParsed = ",readStringParsed)
        }
        catch let error as NSError{
            print("There is an error while reading from AddTextAuditCodeFile.",error.description)
            
        }
        
    }
    
    
    /// SAVE IMAGE END
    
    
}
