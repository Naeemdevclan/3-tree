//
//  AuditProgress.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 19/04/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

let AuditProgressQueue = OperationQueue()
let serialQueue = DispatchQueue(label: "auditProgressQueue1", attributes: [])
let serialQueue2 = DispatchQueue(label: "auditProgressQueue2", attributes: [])
let serialQueue3 = DispatchQueue(label: "auditProgressQueue3", attributes: [])


var updateTimer: Timer?
var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid


import UIKit

class AuditProgress: UIViewController,UITabBarDelegate, SaveDefectsLogedTemporarily{ //, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var btnApproved: UIButton!
    @IBOutlet weak var btnAddSpecs: UIButton!
    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    weak var tab1vc: UIViewController!
    weak var tab2vc: UIViewController!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!

    
    @IBOutlet weak var percentProgress: UILabel!
    @IBOutlet weak var defectRate: UILabel!
    @IBOutlet weak var lbldefectAllowed: UILabel!
    @IBOutlet weak var totalSample: UILabel!
    @IBOutlet weak var auditProgressView: UIProgressView!
    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    
    //IBOutlets
    @IBOutlet weak var AuditCodeLabel: UILabel!
    @IBOutlet weak var SampleChecked: UILabel!
    @IBOutlet weak var SampleSize: UILabel!
    @IBOutlet weak var AuditStageLabel: UILabel!
    
    var criticalBtnHidden:Bool!
    var minorBtnHidden:Bool!
    var aqlLevelHidden:Bool!
    var sampleSpecsHidden:Bool!
    var aqlLevelValue :String!
    var defectRatevalue :String!
    var defectAllwed :String!


    var SelecteColorsProgressAudit :[Any]!

    
    
    @IBOutlet weak var MAJOR_Btn: UIButton!
    
    @IBOutlet weak var MINOR_Btn: UIButton!
    
    @IBOutlet weak var CRITICAL_Btn: UIButton!
    
    @IBOutlet weak var lblAQLLevel: UILabel!
    var ApprovedButton = UIButton(type: .system) as UIButton
    
    
//    var updateTimer: NSTimer?
//    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    var arrayOfDefectsLogged:[((Color:UIColor, ID:String),(DefectType:String,DefectCode:String))] = []
    
    
    @IBAction func btnAddSpecsAction(_ sender: UIButton) {
        
        btnAddSpecs.isHidden = true
        //btnApproved.setTitle("NEXT >>", for: .normal)
        
       // btnApproved.backgroundColor = UIColor.lightGray
        if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "addSpecsViewController") as? AddSpecsViewController {
            resultController.AuditCodeSp = AuditCodein
            resultController.AuditStageSp = AuditStagein
            resultController.AuditStageColorSp = AuditStageColorin
            resultController.SampleCheckedSp = SampleChecked.text!
            resultController.SampleSizeSp = SampleSizein
            resultController.Report_Id = self.ReportId
            self.present(resultController, animated: true, completion: nil)
        }
        

    }
    @IBAction func ApprovedPressed(_ sender: UIButton) {
//        performSegueWithIdentifier("AuditComment", sender: nil)
        //check if button title equals to "Final & Finish" then do something

    //  var ApprovedButton:UIButton = UIButton() // it does not work so i declare and initialize the ApprovedButton above
        
        ApprovedButton = sender
        
//        if ApprovedButton.titleLabel?.text == NSLocalizedString("FinalandFinish_Text", comment: "Message"){  //"Final & Finish"{
//            
//           
//        }
//        else{
        print(SampleSize.text!, SampleChecked.text!)
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            ApprovedButton.isEnabled = false
           // ApprovedButton.setTitle("NEXT >>", for: .normal)
           // ApprovedButton.backgroundColor = UIColor.darkGray

            //ApprovedButton.backgroundColor = UIColor.lightGray
            
            // save then increment
            //????
            endDate = Date()
            convertedEndDate = endDate.CustomDateTimeformat
            print(convertedEndDate)
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodeLabel.text!)&ReportId=\(ReportId!)&Current=\(SampleChecked.text!)&SampleSize=\(SampleSize.text!)&StartDateTime=\(convertedStartDate!)&EndDateTime=\(convertedEndDate!)"
            print("AuditProgress bodydata=")
            print(self.bodyData)
            
    
            // If Internet is available or Not then...
            
            //First Stop sync Service Then save data
            if syncObject != nil{
            syncObject.endBackgroundTask()
            updateTimer?.invalidate()
            syncObject = nil
            }
            
            saveDataInDocumentsDir(self.bodyData)
            

            //NOW START Sync Service again
            
            syncObject = syncService()
            
            if Reachability.isConnectedToNetwork(){
                
                // send data to server in FIFO

                
//                updateTimer = NSTimer.scheduledTimerWithTimeInterval(30.0, target: self, selector: #selector(AuditProgress.callServer), userInfo: nil, repeats: true)
//                registerBackgroundTask()
                
            }
            
         /*   if !Reachability.isConnectedToNetwork(){
                
                saveDataInDocumentsDir(self.bodyData)
                
            }else{
            
                self.performLoadDataRequestWithURL(self.getUrl())
            }
         */
            
            //checking that if "SampleChecked" is equals to "SampleSize" then do something
        var IntVal = Int(SampleChecked.text!)!
        IntVal += 1
        self.SampleChecked.text = "\(IntVal)"
            let SampleChecked_IntVal = Int(self.SampleChecked.text!)!
            let SampleSize_IntVal = Int(self.SampleSize.text!)!
            auditProgressView.setProgress(Float(Double(SampleChecked_IntVal)/Double(SampleSize_IntVal)), animated: true)
            percentProgress.text = String(format: "%.2f",(Double(SampleChecked_IntVal)/Double(SampleSize_IntVal))*100) + "%"
            self.totalSample.text = "\(SampleSize_IntVal)"
            
            if let counfd =  defaults.object(forKey:"addedDefectsCounts \(self.AuditCodein)"){
            defectRate.text = "0.0"
            if(counfd as! Int>0){
            let dd = Double(counfd as! Int)/Double(SampleChecked_IntVal)
            defectRate.text = String(format: "%.2f", dd*100) + "%"
            }
            }
       
            if SampleChecked_IntVal == (SampleSize_IntVal){
                
                let ApprovedButton = sender 
                ApprovedButton.setTitle(NSLocalizedString("FinalandFinish_Text", comment: "Message"), for: UIControlState())
                
                //And Now Also Disable UserInteraction on Major Minor Critical Buttons
//                MAJOR_Btn.isUserInteractionEnabled = false
//                MINOR_Btn.isUserInteractionEnabled = false
//                CRITICAL_Btn.isUserInteractionEnabled = false
                btnAddSpecs.isHidden = false
                
            }
            else{
                ApprovedButton.setTitle("APPROVED", for: .normal)
                //ApprovedButton.backgroundColor = UIColor.darkGray
                self.ApprovedButton.backgroundColor = UIColor(colorLiteralRed: 96.0/255, green: 147.0/255, blue: 9.0/255, alpha: 1.0)


                btnAddSpecs.isHidden = false
               
            }
        //}
    }
    
    
    var SampleCheckedIntValue:Int!
    var imagePicker: UIImagePickerController!
    var capturePhoto:UIImage?
    
    
    var AuditCodein : String!
    var SampleCheckedin : String!
    var SampleSizein : String!
    var AuditStagein : String!
    var AuditStageColorin : UIColor!
    var ReportId: String!
    var AuditScheduleDate: String!
    
    
//    var numberOfTimesViewAppeared = 0
    var startDate:Date!
    var dateFormatter:DateFormatter!
    var convertedStartDate:String!
    
    var endDate:Date!
    var convertedEndDate:String!;
    
    
    var dictionary:NSDictionary!
    var bodyData:String!
    
    var created:Bool!
    var isDirectory: ObjCBool = false
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    let fileManager = FileManager.default
   

    func sendServerRequest(_ url: URL, bodyData:String) -> Bool! {
        
        var status:Bool = false
        
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = bodyData.data(using: String.Encoding.utf8)
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main){
            response, data, error in
            print("\(response)")
            
            if (error != nil) {
                
                status = false
                print(error)
            }
            
            if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
               
                print(json)
                self.dictionary = self.parseJSON(json)
                
                print("\(self.dictionary)")
                print("\(self.dictionary["Result"])")
                
                if ("\(self.dictionary["Status"]!)" == "OK") {
                    // remove the keyValue pair saved in AuditProgress(AuditCode)file
                    
                    status = true
                }
                
            }//if data != nil
            else{
                status = false
            }
        }
        return status
    }


    
    func saveDataInDocumentsDir(_ bodyData:String!){
        
        print("You are in AuditProgress OFFLINE mode.")
        
        // Creating Json file
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("AuditProgress\(self.AuditCodeLabel.text!).json")
        
        // creating a .json file in the Documents folder
        
        if !fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("File created ")
              
                // Now save it to FilesToSync.json file
                
                saveFileNameTo_FilesToSync("AuditProgress\(self.AuditCodeLabel.text!).json")
                //(jsonFilePath.absoluteString)
                
               
            } else {
                print("Couldn't create file for some reason")
            }
        } else {
            print("File already exists")
        }
        //file creation ends
        
        
        
        if fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory){
            
           
            var bodyKey: String
            var url: String
            
            bodyKey = "\(SampleChecked.text!)"

                // "AuditProgress_bodyData_\(self.AuditCodeLabel.text!)_\(SampleChecked.text!)_\(NSDate().formattedDateTime)"
            url = "http://app.3-tree.com/quonda/audit-progress.php"
            
            // first read out the file
            do{
                var readString: String
                readString = try NSString(contentsOfFile: jsonFilePath.path, encoding: String.Encoding.utf8.rawValue) as String
                
                print("readAuditProgress = ",readString)
                
                /// here send readString to function to update values stored in AuditProgress(AuditCode) File
                
                self.updateAuditProgressAuditCodeFile(readString, body: bodyData, bodyKey: bodyKey, url: url, urlKey: "AuditProgress_url")
                
            }
            catch let error as NSError{
                print("There is an error while reading AuditProgress(AuditCode) File.",error.description)
            }
        }
    }
    
    
    func updateAuditProgressAuditCodeFile(_ savedData: String?, body:String, bodyKey:String, url:String, urlKey:String){
      
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("AuditProgress\(self.AuditCodeLabel.text!).json")
        
        if savedData != nil && savedData != "" {
            var readAuditProgressParsed:[String:String] = parseAuditProgressJSON(savedData!)
            
            readAuditProgressParsed.updateValue(body, forKey: bodyKey)
            readAuditProgressParsed.updateValue(url, forKey: urlKey)
            
            // Now write into the AuditProgress(AuditCode) file
            
            if self.fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: readAuditProgressParsed, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: jsonFilePath.path, atomically: true, encoding: String.Encoding.utf8)
                    
                    
                    
                    //Also SAVE SAMPLE CHECKED in to the TempDefaults for use of start/Resume process
                    
                    
                    
                    
                    //set ApprovedButton color back to Green
                    ApprovedButton.setTitle("APPROVED", for: .normal)

                    self.ApprovedButton.backgroundColor = UIColor(colorLiteralRed: 96.0/255, green: 147.0/255, blue: 9.0/255, alpha: 1.0)
                    
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }


        }else{
            
            var readAuditProgressParsed = [String:String]()
            readAuditProgressParsed.updateValue(body, forKey: bodyKey)
            readAuditProgressParsed.updateValue(url, forKey: urlKey)
            
            if self.fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: readAuditProgressParsed, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: jsonFilePath.path, atomically: true, encoding: String.Encoding.utf8)
                    
                    //set ApprovedButton color back to Green
                    ApprovedButton.setTitle("APPROVED", for: .normal)

                    self.ApprovedButton.backgroundColor = UIColor(colorLiteralRed: 96.0/255, green: 147.0/255, blue: 9.0/255, alpha: 1.0)
                    
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
        }
        TempDefaults.set(self.SampleChecked.text!, forKey: "\(self.AuditCodeLabel.text!)Checked")

        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        ApprovedButton.isEnabled = true
        let SampleChecked_IntVal = Int(self.SampleChecked.text!)!
        let SampleSize_IntVal = Int(self.SampleSize.text!)!
        TempDefaults.set(self.SampleChecked.text!, forKey: "\(self.AuditCodeLabel.text!)Checked")

        if SampleChecked_IntVal == SampleSize_IntVal{
        if ReportId == "36"{ // Hybrid
            
            performSegue(withIdentifier: "gotoMeasurementStageHybrid", sender: nil)
        }else if ReportId == "38" {// TM Clothing
            performSegue(withIdentifier: "gotoMeasurementStageTMClothing", sender: nil)
            
        }else if ReportId == "14" || ReportId == "34" {// TM Clothing
            performSegue(withIdentifier: "gotoMGFCommentVC", sender: nil)
            
        }else if ReportId == "45" || ReportId == "44" {
            performSegue(withIdentifier: "gotoLevisCommentVC", sender: nil)
            
        }else if ReportId == "28" || ReportId == "32" || ReportId == "46"{ // Controlist or Arcadia
            
            // NOW GO TO COMMENTS VIEW
            performSegue(withIdentifier: "MeasurementResults", sender: nil)
            //. . . . . . . . . . . . .
        }else{
            
            // NOW GO TO Default COMMENTS VIEW
            performSegue(withIdentifier: "DefaultAuditComment", sender: nil)
            //. . . . . . . . . . . . .
        }

        }
        //Reading from JsonDictionary File stored in documents directory
        do{
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath.path, encoding: String.Encoding.utf8.rawValue) as String

            let readStringParsed = parseJSON(readString)
            print("AuditProgress_readStringParsed = ",readStringParsed)
        }
        catch let error as NSError{
            print("There is an error while reading from JsonDictionary File.",error.description)
            
        }

    }
    var reach: Reachability2?
    
    func reachabilityChanged(_ notification: Notification) {
        if reach!.isReachableViaWiFi() || reach!.isReachableViaWWAN() {
            print("Service avalaible!!!")
        } else {
            print("No service avalaible!!!")
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        
        print(ReportId)
        addednumberofdefects = 0
        auditProgressView.transform = auditProgressView.transform.scaledBy(x: 1, y: 20)
        auditProgressView.setProgress(0, animated: true)

//         NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("reinstateBackgroundTask"), name: UIApplicationDidBecomeActiveNotification, object: nil)
//        
        
        reach = Reachability2.forInternetConnection()
        
        // Tell the reachability that we DON'T want to be reachable on 3G/EDGE/CDMA
        //reach!.reachableOnWWAN = false
        
        // Here we set up a NSNotification observer. The Reachability2 that caused the notification
        // is passed in the object parameter
        
        // Register to receive Notification
        NotificationCenter.default.addObserver(self,
                                                         selector: #selector(AuditProgress.reachabilityChanged(_:)),
                                                         name: NSNotification.Name.reachabilityChanged,
                                                         object: nil)
        
        reach!.startNotifier()
        
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
        
      // Hide critical button if ReportID equals 11,14,20,23,32
      // Hide minor button if ReportID equals 25
        //btnAddSpecs
        btnAddSpecs.isHidden = true
        if criticalBtnHidden == true {
            
            CRITICAL_Btn.isHidden = true
        }else{
            
            CRITICAL_Btn.isHidden = false
        }
        
        if minorBtnHidden == true{
            
            MINOR_Btn.isHidden = true
        }else{
            MINOR_Btn.isHidden = false
        }
        if(sampleSpecsHidden == true){
            btnAddSpecs.isHidden = true
        }else{
             btnAddSpecs.isHidden = false
        }
        if(aqlLevelHidden == true){
            lblAQLLevel.isHidden = true
        }
        else{
            lblAQLLevel.isHidden = false
            lblAQLLevel.text = "AQL: \(aqlLevelValue!)"
        }
        
        if DefectsData0.count > 0 && DefectsRateData0.count > 0 && DefectsAreaData0.count > 0{
            timesDefectTypes = false
            DefectsData0.removeAll()
            DefectsRateData0.removeAll()
            DefectsAreaData0.removeAll()
        }
       
        dateFormatter = DateFormatter()
        print(ReportId)
        
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        //        self.scrollView.contentSize.height = 10000
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Selected Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)

        
        print("SampleCheckedin=\(SampleCheckedin)")
        
        var IntVal = Int(SampleCheckedin)!
         IntVal += 1
        if(IntVal<=1){
            defaults.set(0, forKey:"addedDefectsCounts \(self.AuditCodein)")
        }
        else{
            let counfd =  defaults.object(forKey:"addedDefectsCounts \(self.AuditCodein)")
        }
        print("sampleCheckedin_IntVal=\(IntVal)")
        self.AuditCodeLabel.text = AuditCodein
        self.SampleChecked.text = "\(IntVal)" // SampleCheckedin
        print("SampleChecked.text=\(SampleChecked.text)")
        self.SampleSize.text = SampleSizein
        let IntValSample = Int(SampleSizein)!
          print(Int(SampleSizein)!)
        self.totalSample.text = "\(IntValSample)"
        self.AuditStageLabel.text = AuditStagein
        self.AuditStageLabel.backgroundColor = AuditStageColorin
        print(Double(Double(IntVal)/Double(Int(SampleSizein)!)))
        let dd = Double(Double(IntVal)/Double(Int(SampleSizein)!))
        print(dd * 100.0)
        //auditProgressView.setProgress(Float(dd), animated: true)
        if(IntVal == 1){
        percentProgress.text = String(format: "%.2f",Double(0)/Double(Int(SampleSizein)!)*100) + "%"
            auditProgressView.setProgress(Float(0), animated: true)
        }
        else{
            auditProgressView.setProgress(Float(dd), animated: true)

           percentProgress.text = String(format: "%.2f",Double(IntVal)/Double(Int(SampleSizein)!)*100) + "%"
        }
        defectRate.text = "\(defectRatevalue!)%"
        lbldefectAllowed.text = defectAllwed!
       
     
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func reinstateBackgroundTask() {
        if updateTimer != nil && (backgroundTask == UIBackgroundTaskInvalid) {
            registerBackgroundTask()
        }
    }
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask (expirationHandler: {
            [unowned self] in
            self.endBackgroundTask()
        })
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTask() {
        NSLog("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
    }

    
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        
        let SampleChecked_IntVal = Int(self.SampleChecked.text!)!
        let SampleSize_IntVal = Int(self.SampleSize.text!)!
        print(SampleChecked_IntVal)
        print(SampleSize_IntVal)
        
        if let counfd =  defaults.object(forKey:"addedDefectsCounts \(self.AuditCodein)"){
        if(counfd  as! Int>0){
            let dd = Double(counfd as! Int)/Double(SampleChecked_IntVal)
            defectRate.text = String(format: "%.2f", dd*100) + "%"
        }
        }
        
        if SampleChecked_IntVal == SampleSize_IntVal{
            //self.ApprovedButton.setTitle(NSLocalizedString("FinalandFinish_Text", comment: "Message"), for: UIControlState())
            
            btnApproved.setTitle("Approve & Finish", for: .normal)
            self.btnApproved.backgroundColor = UIColor(colorLiteralRed: 96.0/255, green: 147.0/255, blue: 9.0/255, alpha: 1.0)

//btnAddSpecs.isHidden = false
//            self.SampleChecked.text = "Final & Finish"
            
        }
        
        startDate = Date()
//        dateFormatter.dateFormat = "EEEE, MMMM dd, yyyy"
//        convertedDate = dateFormatter.stringFromDate(currentDate)
        
        convertedStartDate = startDate.CustomDateTimeformat
        print(convertedStartDate)
        print(DefectAdded)
  /*
        numberOfTimesViewAppeared += 1
        print(numberOfTimesViewAppeared)
        print("viewDidAppear")
        if numberOfTimesViewAppeared > 1{
            var IntVal = Int(self.SampleChecked.text!)!
            IntVal++
            self.SampleChecked.text = "\(IntVal)" // SampleCheckedin
        }
  */
        
//        self.AuditCodeLabel.text = AuditCodein
//        self.SampleChecked.text = SampleCheckedin
//        self.SampleSize.text = SampleSizein
//        self.AuditStageLabel.text = AuditStagein
//        self.AuditStageLabel.backgroundColor = AuditStageColorin
        
        if let cnbtn = defaults.object(forKey: "SpecsBtn"){
            defaults.removeObject(forKey: "SpecsBtn")
            btnAddSpecs.isHidden = false
        }

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func criticalBtn_Pressed(_ sender: AnyObject) {
        btnApproved.setTitle("NEXT >>", for: .normal)
        
        btnApproved.backgroundColor = UIColor.lightGray
        

        performSegue(withIdentifier: "critical", sender: ReportId)
//        var IntVal = Int(SampleChecked.text!)!
//        IntVal += 1
      //  self.SampleChecked.text = "\(IntVal)"
    }
    
    @IBAction func majorBtn_Pressed(_ sender: AnyObject) {
        
        btnApproved.setTitle("NEXT >>", for: .normal)
        
        btnApproved.backgroundColor = UIColor.lightGray
        performSegue(withIdentifier: "major", sender: ReportId)
//        var IntVal = Int(SampleChecked.text!)!
//        IntVal += 1
       // self.SampleChecked.text = "\(IntVal)"
    }
    
    @IBAction func minorBtn_Pressed(_ sender: AnyObject) {
        btnApproved.setTitle("NEXT >>", for: .normal)
        
        btnApproved.backgroundColor = UIColor.lightGray
        performSegue(withIdentifier: "minor", sender: ReportId)
//        var IntVal = Int(SampleChecked.text!)!
//        IntVal += 1
//        self.SampleChecked.text = "\(IntVal)"
    }
//
//    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
//        imagePicker.dismissViewControllerAnimated(true, completion: nil)
//        dispatch_async(dispatch_get_main_queue()) { () -> Void in
//            self.capturePhoto = info[UIImagePickerControllerOriginalImage] as? UIImage
//            print(self.capturePhoto)
//            if let capturePhoto = self.capturePhoto{
//                self.performSegueWithIdentifier("major", sender: capturePhoto)
//            }
//        }
//    }
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
//            defaults.removeObjectForKey("userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
//        case 4:
//            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
//                //                self.dismissViewControllerAnimated(true, completion: nil)
//                presentViewController(resultController, animated: true, completion: nil)
//            }
//            
//            let selectedImage:UIImage = UIImage(named: "Selected QAReport")!
//            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
//            
//            break
        case 5:
            
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)

            //self.dismiss(animated: true, completion: nil)
//            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("5") as? ManagerModeViewController {
//                //                self.dismissViewControllerAnimated(true, completion: nil)
//                presentViewController(resultController, animated: true, completion: nil)
//            }
//            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
//            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_selected_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        default:
            break
        }
    }
    
    
    
    // SaveDefectsLogged Protocol Function defination
    
    func saveToArrayOfDefectsLogged(_ Color: UIColor, ID: String, Type: String, Code: String) {
        
       // self.arrayOfDefectsLogged.append(((Color:Color, ID:ID),(DefectType:Type,DefectCode:Code)))
        
        if let loadedCart = defaults.array(forKey: "defectsArr\(AuditCodein!)") as? [[String: Any]] {
            for item in loadedCart {
                
                
                let diyValues = (item["Color"] as! String).components(separatedBy: " ")
                print("diyValues \(diyValues)")
                let returnedColor = UIColor(colorLiteralRed: diyValues[1].FloatValue()!, green: diyValues[2].FloatValue()!, blue: diyValues[3].FloatValue()!, alpha: diyValues[4].FloatValue()!)
                print(returnedColor)
                print(item["Color"]  as! String)    // A, B
                print("ID" + (item["ID"] as! String))    // 19.99, 4.99
                print("DefectType" + (item["DefectType"]   as! String))
                print("DefectCode" + (item["DefectCode"]   as! String))
                
            }
        }

        
        var dict:[[String: Any]] = []
        if let defects =  defaults.array(forKey: "defectsArr\(AuditCodein!)") as? [[String: Any]] {
            dict = defects
            print(dict)
            dict.append(["Color":"\(Color)","ID":ID,"DefectType":Type,"DefectCode":Code] as [String:Any])
            print(dict)
        }else{
            dict.append(["Color":"\(Color)","ID":ID,"DefectType":Type,"DefectCode":Code] as [String:Any])
        }
        defaults.set(dict, forKey: "defectsArr\(AuditCodein!)")
                if let loadedCart = defaults.array(forKey: "defectsArr\(AuditCodein!)") as? [[String: Any]] {
                    print(loadedCart)  // [[price: 19.99, qty: 1, name: A], [price: 4.99, qty: 2, name: B]]"
                    for item in loadedCart {
                        
                        let diyValues = (item["Color"] as! String).components(separatedBy: " ")
                        print("diyValues \(diyValues)")
                        let returnedColor = UIColor(colorLiteralRed: diyValues[1].FloatValue()!, green: diyValues[2].FloatValue()!, blue: diyValues[3].FloatValue()!, alpha: diyValues[4].FloatValue()!)
                        print(returnedColor)
                        print(item["Color"]  as! String)    // A, B
                        print(item["ID"] as! String)    // 19.99, 4.99
                        print(item["DefectType"]   as! String)
                        print(item["DefectCode"]   as! String)       // 1, 2
                    self.arrayOfDefectsLogged.append(((Color:returnedColor, ID:item["ID"] as! String),(DefectType:item["DefectType"]   as! String,DefectCode:item["DefectCode"]   as! String)))
print(arrayOfDefectsLogged)
                    }
                }
        
        print("arrayOfDefectsLogged = ",arrayOfDefectsLogged)
    }
//    destinationVC.delegate = self
    //            destinationVC.arrayOfDefectsLogged = self.arrayOfDefectsLogged
    

    
    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "critical"{
            let destinationNG = segue.destination as! UINavigationController
            let destinationVC = destinationNG.viewControllers.first as! AddDefect
            
            //            let destinationVC = segue.destinationViewController as! AddDefect
            destinationVC.delegate = self
            destinationVC.AuditCodein = self.AuditCodeLabel.text!
            destinationVC.AuditStagein = self.AuditStageLabel.text!
            destinationVC.AuditStageColorin = self.AuditStageLabel.backgroundColor!
            destinationVC.SampleCheckedin = self.SampleChecked.text
            destinationVC.SampleSizein = self.SampleSize.text
            
            destinationVC.ReportId = sender as! String
            destinationVC.MajorORMinor = "2"
            destinationVC.AuditScheduleDate = self.AuditScheduleDate
            
            // data for CAP
            destinationVC.MajorORMinorColor = self.CRITICAL_Btn.backgroundColor!
        }
        else if segue.identifier == "major"{
            let destinationNG = segue.destination as! UINavigationController
            let destinationVC = destinationNG.viewControllers.first as! AddDefect

//            let destinationVC = segue.destinationViewController as! AddDefect
            destinationVC.delegate = self

            destinationVC.AuditCodein = self.AuditCodeLabel.text!
            destinationVC.AuditStagein = self.AuditStageLabel.text!
            destinationVC.AuditStageColorin = self.AuditStageLabel.backgroundColor!
            destinationVC.SampleCheckedin = self.SampleChecked.text
            destinationVC.SampleSizein = self.SampleSize.text
            
            destinationVC.ReportId = sender as! String
            destinationVC.MajorORMinor = "1"
            destinationVC.AuditScheduleDate = self.AuditScheduleDate
            
            // Set the delegate variable of AddDefect to self class, for getting arrayOfDeffectsLogged
           // destinationVC.delegate = self
            
            // data for CAP
            destinationVC.MajorORMinorColor = self.MAJOR_Btn.backgroundColor!
            
        }
        else if segue.identifier == "minor"{
            let destinationNG = segue.destination as! UINavigationController
            let destinationVC = destinationNG.viewControllers.first as! AddDefect
//            let destinationVC = segue.destinationViewController as! AddDefect
            
            destinationVC.AuditCodein = self.AuditCodeLabel.text!
            destinationVC.AuditStagein = self.AuditStageLabel.text!
            destinationVC.AuditStageColorin = self.AuditStageLabel.backgroundColor!
            destinationVC.SampleCheckedin = self.SampleChecked.text
            destinationVC.SampleSizein = self.SampleSize.text
            
            destinationVC.ReportId = sender as! String
            destinationVC.MajorORMinor = "0"
            destinationVC.AuditScheduleDate = self.AuditScheduleDate
            
            // Set the delegate variable of AddDefect to self class, for getting arrayOfDeffectsLogged
            destinationVC.delegate = self
            
            // data for CAP
            destinationVC.MajorORMinorColor = self.MINOR_Btn.backgroundColor!

        }else if segue.identifier == "MeasurementResults"{
            let destinationVC = segue.destination as! MeasurementResults
            destinationVC.AuditCodein = self.AuditCodeLabel.text
            destinationVC.AuditStagein = self.AuditStageLabel.text!
            destinationVC.AuditStageColorin = self.AuditStageLabel.backgroundColor!
            
            // send ReportId to commentsView to show/Hide Two views
            
            destinationVC.ReportID_against_current_audit = self.ReportId
            
            // send arrayOfDefectsLogged to show the CAP (corrective action plan) userInterface
            
            if !self.arrayOfDefectsLogged.isEmpty{
                destinationVC.arrayOfDefectsLoggedRecieved = self.arrayOfDefectsLogged
            }
            
            
            present(destinationVC, animated: true, completion: nil)
    
        }else if segue.identifier == "DefaultAuditComment"{
            let destinationVC = segue.destination as! DefaultCommentVC
            destinationVC.AuditCodein = self.AuditCodeLabel.text
            destinationVC.AuditStagein = self.AuditStageLabel.text!
            destinationVC.AuditStageColorin = self.AuditStageLabel.backgroundColor!
            
           
        }else if segue.identifier == "gotoMeasurementStageHybrid"{
           
            let destinationVC = segue.destination as! Measurement_Stage_Hybrid
            destinationVC.AuditCodeIn = self.AuditCodeLabel.text
            destinationVC.AuditStageIn = self.AuditStageLabel.text!
            destinationVC.AuditStageColor = self.AuditStageLabel.backgroundColor!
//            destinationVC.ReportID_against_current_audit = ReportId!
        }else if segue.identifier == "gotoMGFCommentVC"{
            
            let destinationVC = segue.destination as! MGFCommentVC
            destinationVC.AuditCodein = self.AuditCodeLabel.text
            destinationVC.AuditStagein = self.AuditStageLabel.text!
            destinationVC.AuditStageColorin = self.AuditStageLabel.backgroundColor!
            destinationVC.defectRatevalue = self.defectRatevalue
            destinationVC.sampleChecked = SampleChecked.text!
            destinationVC.ReportID_against_current_audit = ReportId!
            if !self.arrayOfDefectsLogged.isEmpty{
                destinationVC.arrayOfDefectsLoggedRecieved = self.arrayOfDefectsLogged
            }
        }else if segue.identifier == "gotoLevisCommentVC"{
            
            let destinationVC = segue.destination as! LevisCommentVC
            destinationVC.AuditCodein = self.AuditCodeLabel.text
            destinationVC.AuditStagein = self.AuditStageLabel.text!
            destinationVC.AuditStageColorin = self.AuditStageLabel.backgroundColor!
            destinationVC.defectRatevalue = self.defectRatevalue
            destinationVC.sampleChecked = SampleChecked.text!
            destinationVC.ReportID_against_current_audit = ReportId!
            if !self.arrayOfDefectsLogged.isEmpty{
                destinationVC.arrayOfDefectsLoggedRecieved = self.arrayOfDefectsLogged
            }
        }else if segue.identifier == "gotoMeasurementStageTMClothing"{
            
            let destinationVC = segue.destination as! Measurement_Stage_TMClothing
            destinationVC.AuditCodeIn = self.AuditCodeLabel.text
            destinationVC.AuditStageIn = self.AuditStageLabel.text!
            destinationVC.AuditStageColor = self.AuditStageLabel.backgroundColor!
            destinationVC.SelecteColors =   self.SelecteColorsProgressAudit

            //            destinationVC.ReportID_against_current_audit = ReportId!
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    func getUrl() -> URL {
        let toEscape = "http://app.3-tree.com/quonda/audit-progress.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }

    func performLoadDataRequestWithURL(_ url: URL) -> String? {
        
        //        self.bodyData = self.bodyData!+"&PageId=\(currentPage)"
//        self.bodyData = "User=\(defaults.objectForKey("userid")as! String)&AuditCode=&Date=2016-04-20"
        
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = self.bodyData!.data(using: String.Encoding.utf8)
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main){
            response, data, error in
            print("\(response)")
            if (error != nil) {
                print(error)
                //                self.activityIndicator.stopAnimating()
                //                self.messageFrame.removeFromSuperview()
            }
            
            if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                // - - - - - - write this JSON data to file- - - - - - - -
                //                print("created : \(self.created)")
                //                if self.created == true{
//                if self.fileManager.fileExistsAtPath(self.jsonFilePath2.absoluteString, isDirectory: &self.isDirectory) {
//                    do{
//                        try json.writeToFile(self.jsonFilePath2.path!, atomically: true, encoding: NSUTF8StringEncoding)
//                    } catch let error as NSError {
//                        print("JSON data couldn't written to file // File not exist")
//                        print(error.description)
//                    }
//                }
                print(json)
                self.dictionary = self.parseJSON(json)
                
                print("\(self.dictionary)")
                print("\(self.dictionary["Result"])")
                
                  if ("\(self.dictionary["Status"]!)" == "OK") {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        self.ApprovedButton.isEnabled = true
                    self.ApprovedButton.setTitle("APPROVED", for: .normal)

                        self.ApprovedButton.backgroundColor = UIColor(colorLiteralRed: 96.0/255, green: 147.0/255, blue: 9.0/255, alpha: 1.0)
                }

            }//if data != nil
            else{
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                let alertView = UIAlertController(title: "Cannot Approved" , message: "Press OK to Approve again", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                    self.performLoadDataRequestWithURL(self.getUrl())
                })
//                let NO_action: UIAlertAction = UIAlertAction(title: "NO", style: .Default, handler: nil)
                
                alertView.addAction(ok_action)
//                alertView.addAction(NO_action)
                
                self.present(alertView, animated: true, completion: nil)
                print("return data is nil")
            }
        }
        return json
    }
    func parseToRemove(_ jsonString: String) -> Dictionary<String,String> {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String,String> {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    
    func parseAuditProgressJSON(_ jsonString: String) -> [String:String] {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:String] {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }

}
extension Date {
    var CustomDateTimeformat: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"// "dd MMM yyyy"  // HH:mm:ss Z"
        return  formatter.string(from: self)
    }
    var CustomDateformat: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"// "dd MMM yyyy"  // HH:mm:ss Z"
        return  formatter.string(from: self)
    }

}





// Global Methods and atributes

var isDirectory: ObjCBool = false
let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
let fileManager = FileManager.default


func saveFileNameTo_FilesToSync(_ FileName:String) -> Void {
   
    
    // first read out the file
    do{
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        let jsonSyncFilePath = documentsDirectoryPath.appendingPathComponent("FilesToSync.json")
        
        var readString: String
        readString = try NSString(contentsOfFile: jsonSyncFilePath.path, encoding: String.Encoding.utf8.rawValue) as String
        
//        print("FilesToSync readString = ",readString)
        
        /// here send readString to function to update FileNames stored in FilesToSync.json File
        
        saveToSyncFile(readString, FileName: FileName, jsonSyncFilePath: jsonSyncFilePath.absoluteString)
        
    }
    catch let error as NSError{
        print("There is an error while reading AuditProgress(AuditCode) File.",error.description)
    }
}

func saveToSyncFile(_ savedData: String?, FileName:String, jsonSyncFilePath:String){
    
    
    if savedData != nil && savedData != "" {
        
        var FilesIn_SyncFile:[String] = parseSyncFile(savedData!)
        
        // Append File Name if it already doesn't exist
        
        FilesIn_SyncFile.append(FileName)
        
        
        // Now write into the FilesToSync.json file
        
        if fileManager.fileExists(atPath: jsonSyncFilePath, isDirectory: &isDirectory) {
            do{
                let jsonDictionary = try JSONSerialization.data(withJSONObject: FilesIn_SyncFile, options: JSONSerialization.WritingOptions.prettyPrinted)
                
                let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                
                try json.write(toFile: jsonSyncFilePath, atomically: true, encoding: String.Encoding.utf8)
                
                
            } catch let error as NSError {
                print("JSON data couldn't written to file // File not exist")
                print(error.description)
            }
        }
        
        
    }else{
        
        var FilesIn_SyncFile:[String] = []
        
        FilesIn_SyncFile.append(FileName)
        
        // Now write into the FilesToSync.json file
        
        if fileManager.fileExists(atPath: jsonSyncFilePath, isDirectory: &isDirectory) {
            do{
                let jsonDictionary = try JSONSerialization.data(withJSONObject: FilesIn_SyncFile, options: JSONSerialization.WritingOptions.prettyPrinted)
                
                let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                
                try json.write(toFile: jsonSyncFilePath, atomically: true, encoding: String.Encoding.utf8)
                
            } catch let error as NSError {
                print("JSON data couldn't written to file // File not exist")
                print(error.description)
            }
        }
    }
    
    //Reading from JsonDictionary File stored in documents directory
    do{
        var readString: String
        readString = try NSString(contentsOfFile: jsonSyncFilePath, encoding: String.Encoding.utf8.rawValue) as String
        
        let readStringParsed = parseSyncFile(readString)
        print("Recent Files added in SyncFile = ",readStringParsed)
    }
    catch let error as NSError{
        print("There is an error while reading from JsonDictionary File.",error.description)
        
    }
    
}
func parseSyncFile(_ jsonString: String) -> [String] {
    
    print(jsonString)
    if let data = jsonString.data(using: String.Encoding.utf8) {
        let error: NSError? = nil
        if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String] {
            print(json)
            return json!
        } else if let error = error {
            //Here's where the error comes back.
            print("JSON Error: \(error)")
        } else {
            print("Unknown JSON Error")
        }
    }
    return [""]
}




