//
//  AuditQuantity.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 20/10/2017.
//  Copyright © 2017 Muhammad Naeem. All rights reserved.
//

import UIKit

class AuditQuantity: UIViewController , UITabBarDelegate, UITextViewDelegate, UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIScrollViewDelegate,SavingScannedCartonsNumbersDelegate {
    
    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!
    
    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    
    
    
    @IBOutlet weak var AuditCode: UILabel!
    @IBOutlet weak var AuditStage: UILabel!
    
    
    //    @IBOutlet weak var ShipQuantity: UITextField!
    
    //    @IBOutlet weak var ReScreenQuantity: UITextField!
    
    
    //    @IBOutlet weak var Status: UISwitch!
    //    var statusActive = false
    var AuditResult: String!
    var jsonFilePath:URL!
    // let libraryDirectoryPathString = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true).first!
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    
    
    //    @IBOutlet weak var ProductionStatus: UITextField!
    
    //    @IBOutlet weak var Comments: UITextView!
    
    //    @IBOutlet weak var finishBtn: UIButton!
    
    var dictionary:NSDictionary!
    
    var MutableDictionary:NSMutableDictionary!
    
    var AuditCodein: String!
    var AuditStagein: String!
    var AuditStageColorin : UIColor!
    
    // use for show/Hide Comments views
    
    var ReportID_against_current_audit: String!
    
    // variable to recieve ArrayOfDefectsLogged assigned while adding defects
    
    var arrayOfDefectsLoggedRecieved:[((Color:UIColor, ID:String),(DefectType:String,DefectCode:String))]? //[(DefectType:String,DefectCode:String)]?
    
    // outmostScrollView
    @IBOutlet weak var outmostScrollView: UIScrollView!
    
    // outmostStackView
    @IBOutlet weak var outmostStackView: UIStackView!
    

    
    
    // WorkmanshipResult variable
    var workmanshipResultValue:String!
    
    
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    var created:Bool!
    var paramPath = "/quonda"
    var otherImagesType:String!
    var savedParamPath:String!
    
    var sampleImage = UIImageView()

    @IBOutlet weak var txtShipmentQtyUnits: UITextField!
    @IBOutlet weak var txtShipmentQtyCartons: UITextField!
    @IBOutlet weak var txtPresentedQtyForInspection: UITextField!
    @IBOutlet weak var txtUnitsPacked: UITextField!
    
    @IBOutlet weak var txtUnitsFinished: UITextField!
    @IBOutlet weak var txtNotFinished: UITextField!
    @IBOutlet weak var cartonsView: UIView!
    
    
    
    // Carton Nos
    @IBOutlet var CartonNos: [UITextField]!
    
    @IBOutlet weak var addCartonBtn: UIButton!
    
    @IBOutlet weak var addCartonsHieghtContraint: NSLayoutConstraint!
    
    @IBOutlet weak var txtSizeCol1: UITextField!
    
    @IBOutlet weak var txtSizeCol2: UITextField!
    
    @IBOutlet weak var txtSizeCol3: UITextField!
    
    @IBOutlet weak var txtSizeCol4: UITextField!
    
    @IBOutlet weak var txtSizeCol5: UITextField!
    @IBOutlet weak var txtSizeCol6: UITextField!
    
    @IBOutlet weak var txtSizeCol7: UITextField!
    
    @IBOutlet weak var txtSizeCol8: UITextField!
    @IBOutlet weak var innerStackViewConstraintHieght: NSLayoutConstraint!
    
    @IBOutlet weak var resultViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var txtSizeCol9: UITextField!
    
    @IBOutlet weak var txtSizeCol10: UITextField!
    
    @IBOutlet weak var txtSizeQty1: UITextField!
    
    @IBOutlet weak var txtSizeQty2: UITextField!
    
    @IBOutlet weak var txtSizeQty3: UITextField!
    
    @IBOutlet weak var txtSizeQty4: UITextField!
    
    @IBOutlet weak var txtSizeQty5: UITextField!
    
    @IBOutlet weak var txtSizeQty6: UITextField!
    
    @IBOutlet weak var txtSizeQty7: UITextField!
    
    @IBOutlet weak var txtSizeQty8: UITextField!
    
    @IBOutlet weak var txtSizeQty9: UITextField!
    
    @IBOutlet weak var txtSizeQty10: UITextField!
    
    @IBOutlet weak var txtSampleQty1: UITextField!
    @IBOutlet weak var txtSampleQty2: UITextField!
    @IBOutlet weak var txtSampleQty3: UITextField!
    @IBOutlet weak var txtSampleQty4: UITextField!
    @IBOutlet weak var txtSampleQty5: UITextField!
    @IBOutlet weak var txtSampleQty6: UITextField!
    @IBOutlet weak var txtSampleQty7: UITextField!
    @IBOutlet weak var txtSampleQty8: UITextField!
    @IBOutlet weak var txtSampleQty9: UITextField!
    @IBOutlet weak var txtSampleQty10: UITextField!
    
    
    
    
    
    
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        outmostScrollView.contentSize.height = outmostStackView.frame.height + 100

    }
    
    
    
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(AuditQuantity.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        // Now add toolbar to each of the keyboard in textfield
        //self.Qty_of_Lots_TextField.inputAccessoryView = doneToolbar
       
        
    }
    
    func doneButtonAction()
    {
//        self.Qty_of_Lots_TextField.resignFirstResponder()
//        self.AuditResultBtn.isUserInteractionEnabled = true
        
    }
    

    @IBOutlet weak var noOfCartonsView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //mgfBottomConstraint.constant = 565
        noOfCartonsView.bringSubview(toFront: cartonsView)
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Selected Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)
        var j = 0
        for carton in CartonNos{
            carton.delegate = self
//            carton.text = "\(j)"
//            j = j + 1
        }
      self.txtFieldStyle()
       txtNotFinished.delegate = self
    }
    func txtFieldStyle(){
        txtShipmentQtyUnits.layer.borderWidth = 2.0
        txtShipmentQtyUnits.layer.borderColor = UIColor.lightGray.cgColor
        txtShipmentQtyUnits.layer.cornerRadius = 5.0
        
        txtShipmentQtyCartons.layer.borderWidth = 2.0
        txtShipmentQtyCartons.layer.borderColor = UIColor.lightGray.cgColor
        txtShipmentQtyCartons.layer.cornerRadius = 5.0
        
        
        txtPresentedQtyForInspection.layer.borderWidth = 2.0
        txtPresentedQtyForInspection.layer.borderColor = UIColor.lightGray.cgColor
        txtPresentedQtyForInspection.layer.cornerRadius = 5.0
        
        txtUnitsPacked.layer.borderWidth = 2.0
        txtUnitsPacked.layer.borderColor = UIColor.lightGray.cgColor
        txtUnitsPacked.layer.cornerRadius = 5.0
        
        txtUnitsFinished.layer.borderWidth = 2.0
        txtUnitsFinished.layer.borderColor = UIColor.lightGray.cgColor
        txtUnitsFinished.layer.cornerRadius = 5.0
        
        txtNotFinished.layer.borderWidth = 2.0
        txtNotFinished.layer.borderColor = UIColor.lightGray.cgColor
        txtNotFinished.layer.cornerRadius = 5.0
        
        
        txtSizeCol1.layer.borderWidth = 2.0
        txtSizeCol1.layer.borderColor = UIColor.lightGray.cgColor
        txtSizeCol1.layer.cornerRadius = 5.0
        txtSizeCol2.layer.borderWidth = 2.0
        txtSizeCol2.layer.borderColor = UIColor.lightGray.cgColor
        txtSizeCol2.layer.cornerRadius = 5.0
        txtSizeCol3.layer.borderWidth = 2.0
        txtSizeCol3.layer.borderColor = UIColor.lightGray.cgColor
        txtSizeCol3.layer.cornerRadius = 5.0
        txtSizeCol4.layer.borderWidth = 2.0
        txtSizeCol4.layer.borderColor = UIColor.lightGray.cgColor
        txtSizeCol4.layer.cornerRadius = 5.0
        txtSizeCol5.layer.borderWidth = 2.0
        txtSizeCol5.layer.borderColor = UIColor.lightGray.cgColor
        txtSizeCol5.layer.cornerRadius = 5.0
        txtSizeCol6.layer.borderWidth = 2.0
        txtSizeCol6.layer.borderColor = UIColor.lightGray.cgColor
        txtSizeCol6.layer.cornerRadius = 5.0
        txtSizeCol7.layer.borderWidth = 2.0
        txtSizeCol7.layer.borderColor = UIColor.lightGray.cgColor
        txtSizeCol7.layer.cornerRadius = 5.0
        txtSizeCol8.layer.borderWidth = 2.0
        txtSizeCol8.layer.borderColor = UIColor.lightGray.cgColor
        txtSizeCol8.layer.cornerRadius = 5.0
        txtSizeCol9.layer.borderWidth = 2.0
        txtSizeCol9.layer.borderColor = UIColor.lightGray.cgColor
        txtSizeCol9.layer.cornerRadius = 5.0
        txtSizeCol10.layer.borderWidth = 2.0
        txtSizeCol10.layer.borderColor = UIColor.lightGray.cgColor
        txtSizeCol10.layer.cornerRadius = 5.0
        
        txtSizeQty1.layer.borderWidth = 2.0
        txtSizeQty1.layer.borderColor = UIColor.lightGray.cgColor
        txtSizeQty1.layer.cornerRadius = 5.0
        txtSizeQty2.layer.borderWidth = 2.0
        txtSizeQty2.layer.borderColor = UIColor.lightGray.cgColor
        txtSizeQty2.layer.cornerRadius = 5.0
        txtSizeQty3.layer.borderWidth = 2.0
        txtSizeQty3.layer.borderColor = UIColor.lightGray.cgColor
        txtSizeQty3.layer.cornerRadius = 5.0
        txtSizeQty4.layer.borderWidth = 2.0
        txtSizeQty4.layer.borderColor = UIColor.lightGray.cgColor
        txtSizeQty4.layer.cornerRadius = 5.0
        txtSizeQty5.layer.borderWidth = 2.0
        txtSizeQty5.layer.borderColor = UIColor.lightGray.cgColor
        txtSizeQty5.layer.cornerRadius = 5.0
        txtSizeQty6.layer.borderWidth = 2.0
        txtSizeQty6.layer.borderColor = UIColor.lightGray.cgColor
        txtSizeQty6.layer.cornerRadius = 5.0
        txtSizeQty7.layer.borderWidth = 2.0
        txtSizeQty7.layer.borderColor = UIColor.lightGray.cgColor
        txtSizeQty7.layer.cornerRadius = 5.0
        txtSizeQty8.layer.borderWidth = 2.0
        txtSizeQty8.layer.borderColor = UIColor.lightGray.cgColor
        txtSizeQty8.layer.cornerRadius = 5.0
        txtSizeQty9.layer.borderWidth = 2.0
        txtSizeQty9.layer.borderColor = UIColor.lightGray.cgColor
        txtSizeQty9.layer.cornerRadius = 5.0
        txtSizeQty10.layer.borderWidth = 2.0
        txtSizeQty10.layer.borderColor = UIColor.lightGray.cgColor
        txtSizeQty10.layer.cornerRadius = 5.0
        
        
        
        txtSampleQty1.layer.borderWidth = 2.0
        txtSampleQty1.layer.borderColor = UIColor.lightGray.cgColor
        txtSampleQty1.layer.cornerRadius = 5.0
        txtSampleQty2.layer.borderWidth = 2.0
        txtSampleQty2.layer.borderColor = UIColor.lightGray.cgColor
        txtSampleQty2.layer.cornerRadius = 5.0
        txtSampleQty3.layer.borderWidth = 2.0
        txtSampleQty3.layer.borderColor = UIColor.lightGray.cgColor
        txtSampleQty3.layer.cornerRadius = 5.0
        txtSampleQty4.layer.borderWidth = 2.0
        txtSampleQty4.layer.borderColor = UIColor.lightGray.cgColor
        txtSampleQty4.layer.cornerRadius = 5.0
        txtSampleQty5.layer.borderWidth = 2.0
        txtSampleQty5.layer.borderColor = UIColor.lightGray.cgColor
        txtSampleQty5.layer.cornerRadius = 5.0
        txtSampleQty6.layer.borderWidth = 2.0
        txtSampleQty6.layer.borderColor = UIColor.lightGray.cgColor
        txtSampleQty6.layer.cornerRadius = 5.0
        txtSampleQty7.layer.borderWidth = 2.0
        txtSampleQty7.layer.borderColor = UIColor.lightGray.cgColor
        txtSampleQty7.layer.cornerRadius = 5.0
        txtSampleQty8.layer.borderWidth = 2.0
        txtSampleQty8.layer.borderColor = UIColor.lightGray.cgColor
        txtSampleQty8.layer.cornerRadius = 5.0
        txtSampleQty9.layer.borderWidth = 2.0
        txtSampleQty9.layer.borderColor = UIColor.lightGray.cgColor
        txtSampleQty9.layer.cornerRadius = 5.0
        
        
        txtSampleQty10.layer.borderWidth = 2.0
        txtSampleQty10.layer.borderColor = UIColor.lightGray.cgColor
        txtSampleQty10.layer.cornerRadius = 5.0

    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField.text!.characters.count == 0){
        textField.text = ""
            if(textField == txtShipmentQtyUnits||textField == txtUnitsFinished||textField == txtShipmentQtyCartons||textField == txtPresentedQtyForInspection||textField == txtUnitsPacked||textField == txtNotFinished){
               textField.text = " "
            }
//        for carton in CartonNos{
//                carton.text = ""
//                //            carton.text = "\(j)"
//                //            j = j + 1
//            }
        }
        
        textField.layer.borderWidth = 2.0
        textField.layer.borderColor = UIColor.orange.cgColor
        textField.layer.cornerRadius = 5.0
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer.borderWidth = 2.0
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.cornerRadius = 5.0

    }
    func textViewDidChange(_ textView: UITextView) {
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.AuditCode.text = AuditCodein
        self.AuditStage.text = AuditStagein
        self.AuditStage.backgroundColor = AuditStageColorin
        }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
    }
   
    @IBAction func addCartonAction(_ sender: UIButton) {
        if(addCartonsHieghtContraint.constant < 454.0)
        {
            addCartonBtn.isHidden = false
            addCartonsHieghtContraint.constant = addCartonsHieghtContraint.constant + 38
            resultViewHeightConstraint.constant = resultViewHeightConstraint.constant + 38
            innerStackViewConstraintHieght.constant = innerStackViewConstraintHieght.constant + 38
            
        }
        else{
            addCartonBtn.isHidden = true
        }
        print(addCartonsHieghtContraint.constant)
    }
    var bodyData = ""
func popupMsg(msg:String){
    
    let alertView = UIAlertController(title: nil , message: msg, preferredStyle: .alert)
    let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    
    alertView.addAction(ok_action)
    self.present(alertView, animated: true, completion: nil)
}
 func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
            //            defaults.removeObjectForKey("userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
            //        case 4:
            //            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
            //                //                self.dismissViewControllerAnimated(true, completion: nil)
            //                presentViewController(resultController, animated: true, completion: nil)
            //            }
            //
            //            let selectedImage:UIImage = UIImage(named: "Selected QAReport")!
            //            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            //
        //            break
        case 5:
            if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                    present(resultController, animated: true, completion: nil)
                }
            }else{
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                    present(resultController, animated: true, completion: nil)
                }
                
            }
            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_selected_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
            
        default:
            break
        }
    }

  @IBAction func Save_and_Continue(_ sender: AnyObject) {
    
        for carton in CartonNos{
            if carton.text != nil{
                if(carton.text?.characters.count != 0 ){
                ArrayOfCartonNumbers.append(carton.text!)
                }
            }
        }
        print(ArrayOfCartonNumbers)
        if(ArrayOfCartonNumbers.count != 0){
            saveOfflineCommentsData()
        }else{
            popupMsg(msg: "There must be one cartonNo atleast.")
            
        }
        //self.performSegue(withIdentifier: "gotoAuditQuantity", sender: nil)
        

    }
@IBAction func saveAndContinue(_ sender: UIButton) {
        
    
    }

    //Saving data offline
    func parseCommentsJSON(_ jsonString: String) -> [String:String] {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:String] {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    var ArrayOfCartonNumbers:[String]! = []

    func saveOfflineCommentsData(){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        print("You are in AuditComments OFFLINE mode.")
        
        // Creating AuditComment Json file
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("AuditQtyFile\(self.AuditCode.text!).json")
        
        // creating a .json file in the Documents folder
        
        if !fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("File created ")
                
                // Now save it to FilesToSync.json file
                
                saveFileNameTo_FilesToSync("AuditQtyFile\(self.AuditCode.text!).json")
                
                saveFileNameTo_FilesToSync("AuditCommentFile\(self.AuditCode.text!).json")

                //(jsonFilePath.absoluteString)
                
            } else {
                print("Couldn't create file for some reason")
            }
        } else {
            print("File already exists")
        }
        //file creation ends
        
        
        
        //First Read out the AuditComment AuditCodeFile
        
        do{
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath.absoluteString, encoding: String.Encoding.utf8.rawValue) as String
            
            print("readAuditComment_String = ",readString)
            
            /// here send readString to function to update values stored in AuditCommentFile(AuditCode) File
            
            self.AuditCommentAuditCodeFile(readString, AuditCommentFilePath: jsonFilePath.absoluteString)
            
        }
        catch let error as NSError{
            print("There is an error while reading AddDeffectText(AuditCode) File.",error.description)
        }
        
    }// end of func
    
    
    func AuditCommentAuditCodeFile(_ savedCommentsData:String?, AuditCommentFilePath: String) -> Void {
        
        if ReportID_against_current_audit == "32"{
            //if saveContinue_Pressed == true{
            // save data against Arcadia only
            
            AuditCommentAuditCodeFile_Arcadia(savedCommentsData, AuditCommentFilePath: AuditCommentFilePath)
            
        }
        else if ReportID_against_current_audit == "46"{
            //if saveContinue_Pressed == true{
            // save data against Arcadia only
            
            AuditCommentAuditCodeFile_Arcadia(savedCommentsData, AuditCommentFilePath: AuditCommentFilePath)
            
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        //Reading from JsonDictionary File stored in documents directory
        do{
            var readString: String
            readString = try NSString(contentsOfFile: AuditCommentFilePath, encoding: String.Encoding.utf8.rawValue) as String
            //                    print("readString=\(readString)")
            let readStringParsed = parseCommentsJSON(readString)
            print("AuditComments_readStringParsed = ",readStringParsed)
        }
        catch let error as NSError{
            print("There is an error while reading from JsonDictionary File.",error.description)
            
        }
        
    }
    
    func AuditCommentAuditCodeFile_Arcadia(_ savedCommentsData:String?, AuditCommentFilePath: String) -> Void{
        
        // - - - -
        
        if savedCommentsData != nil && savedCommentsData != "" {

            let crtnNumbers = ArrayOfCartonNumbers.joined(separator: ",")
            
            bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCode.text!)&CartonNos=\(crtnNumbers)&ShipmentQtyUnits=\(txtShipmentQtyUnits.text!)&ShipmentQtyCtns=\(txtShipmentQtyCartons.text!)&PresentedQty=\(txtPresentedQtyForInspection.text!)&UnitsPackedQty=\(txtUnitsPacked.text!)&UnitsFinishedQty=\(txtUnitsFinished.text!)&UnitsNotFinishedQty=\(txtNotFinished.text)&SizeColor1=\(txtSizeCol1.text!)&SizeQty1=\(txtSizeQty1.text!)&SampleQty1=\(txtSampleQty1.text!)&SizeColor2=\(txtSizeCol2.text!)&SizeQty2=\(txtSizeQty2.text!)&SampleQty2=\(txtSampleQty2.text!)&SizeColor3=\(txtSizeCol3.text!)&SizeQty3=\(txtSizeQty3.text!)&SampleQty3=\(txtSampleQty3.text!)&SizeColor4=\(txtSizeCol4.text!)&SizeQty4=\(txtSizeQty4.text!)&SampleQty4=\(txtSampleQty4.text!)&SizeColor5=\(txtSizeCol5.text!)&SizeQty5=\(txtSizeQty5.text!)&SampleQty5=\(txtSampleQty5.text!)&SizeColor6=\(txtSizeCol6.text!)&SizeQty6=\(txtSizeQty6.text!)&SampleQty6=\(txtSampleQty6.text!)&SizeColor7=\(txtSizeCol7.text!)&SizeQty7=\(txtSizeQty7.text!)&SampleQty7=\(txtSampleQty7.text!)&SizeColor8=\(txtSizeCol8.text!)&SizeQty8=\(txtSizeQty8.text!)&SampleQty8=\(txtSampleQty8.text!)&SizeColor9=\(txtSizeCol9.text!)&SizeQty9=\(txtSizeQty9.text!)&SampleQty9=\(txtSampleQty9.text!)&SizeColor10=\(txtSizeCol10.text!)&SizeQty10=\(txtSizeQty10.text!)&SampleQty10=\(txtSampleQty10.text!)"
            //
            print("Packaging_bodyData = ",bodyData)
            
            
            var AuditCommentsOfflineData:[String:String] = parseCommentsJSON(savedCommentsData!)
            
            
            let randomString = randomStringWithLength(6)
            
            AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "AuditQty_bodyData\(self.AuditCode.text!)_\(randomString)")
            AuditCommentsOfflineData.updateValue("http://app.3-tree.com/quonda/save-audit-quantity.php", forKey: "AuditQty_url")
            ArrayOfCartonNumbers.removeAll()

            
            if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    //Note!!
                    
                    if !Reachability.isConnectedToNetwork(){
                        
                    }
                    
                    
                    // now move onto the Next screen
                    //???????
                    //?????
                    //???
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                        
                        present(resultController, animated: true, completion: nil)
                    }

                    //performSegue(withIdentifier: "goto Final comments", sender: nil)
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
            
        }else{
            let crtnNumbers = ArrayOfCartonNumbers.joined(separator: ",")

             bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCode.text!)&CartonNos=\(crtnNumbers)&ShipmentQtyUnits=\(txtShipmentQtyUnits.text!)&ShipmentQtyCtns=\(txtShipmentQtyCartons.text!)&PresentedQty=\(txtPresentedQtyForInspection.text!)&UnitsPackedQty=\(txtUnitsPacked.text!)&UnitsFinishedQty=\(txtUnitsFinished.text!)&UnitsNotFinishedQty=\(txtNotFinished.text)&SizeColor1=\(txtSizeCol1.text!)&SizeQty1=\(txtSizeQty1.text!)&SampleQty1=\(txtSampleQty1.text!)&SizeColor2=\(txtSizeCol2.text!)&SizeQty2=\(txtSizeQty2.text!)&SampleQty2=\(txtSampleQty2.text!)&SizeColor3=\(txtSizeCol3.text!)&SizeQty3=\(txtSizeQty3.text!)&SampleQty3=\(txtSampleQty3.text!)&SizeColor4=\(txtSizeCol4.text!)&SizeQty4=\(txtSizeQty4.text!)&SampleQty4=\(txtSampleQty4.text!)&SizeColor5=\(txtSizeCol5.text!)&SizeQty5=\(txtSizeQty5.text!)&SampleQty5=\(txtSampleQty5.text!)&SizeColor6=\(txtSizeCol6.text!)&SizeQty6=\(txtSizeQty6.text!)&SampleQty6=\(txtSampleQty6.text!)&SizeColor7=\(txtSizeCol7.text!)&SizeQty7=\(txtSizeQty7.text!)&SampleQty7=\(txtSampleQty7.text!)&SizeColor8=\(txtSizeCol8.text!)&SizeQty8=\(txtSizeQty8.text!)&SampleQty8=\(txtSampleQty8.text!)&SizeColor9=\(txtSizeCol9.text!)&SizeQty9=\(txtSizeQty9.text!)&SampleQty9=\(txtSampleQty9.text!)&SizeColor10=\(txtSizeCol10.text!)&SizeQty10=\(txtSizeQty10.text!)&SampleQty10=\(txtSampleQty10.text!)"
            print("Packaging_bodyData = ",bodyData)
            print("AuditCommentsArcadia-bodydata -> \(self.bodyData)")
            
            ArrayOfCartonNumbers.removeAll()
            
            var AuditCommentsOfflineData = [String:String]()
            
            
            
            let randomString = randomStringWithLength(6)
            
            AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "AuditQty_bodyData\(self.AuditCode.text!)_\(randomString)")
            AuditCommentsOfflineData.updateValue("http://app.3-tree.com/quonda/save-audit-quantity.php", forKey: "AuditQty_url")
            
            
            if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    // now move onto the AuditorModeViewController screen
                    //?????
                    //???
                    //??
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    //performSegue(withIdentifier: "goto Final comments", sender: nil)
                    defaults.set("Completed", forKey: "OfflineAudit\(AuditCodein!)")

                    if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                        
                        present(resultController, animated: true, completion: nil)
                    }

                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
        }
        
        // - - -
        
    }
    //Random string function
    
    func randomStringWithLength(_ len: Int) -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for i in 0 ..< (len){
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString
    }
    @IBAction func gotoScanCartons(_ sender: UIButton) {
        if let resultController = storyboard!.instantiateViewController(withIdentifier: "qrScannerController") as? QRScannerController {
            resultController.delegate = self
            present(resultController, animated: true, completion: nil)
        }
    }
    
    func saveScannedCartons(cartons: [String]) {
        print(cartons)
        var i = 0;
        for carton in CartonNos{
            if carton.text != nil{
                if(carton.text?.characters.count == 0 || carton.text == ""||carton.text == " "){
                    if(i < cartons.count){
                    //if(cartons[i].isAlphanumeric() == true){
                    carton.text! = cartons[i]
                    i = i + 1
                    //}
                    }
                   // ArrayOfCartonNumbers.append(carton.text!)
                }
            }
        }
        addCartonsHieghtContraint.constant = addCartonsHieghtContraint.constant + CGFloat(((cartons.count/4)*38))
        resultViewHeightConstraint.constant = resultViewHeightConstraint.constant + CGFloat(((cartons.count/4)*38))
        innerStackViewConstraintHieght.constant = innerStackViewConstraintHieght.constant + CGFloat(((cartons.count/4)*38))

        if(addCartonsHieghtContraint.constant < 454.0)
        {
            addCartonBtn.isHidden = false
        }
        else{
            addCartonBtn.isHidden = true
        }


        
    }
}
extension String {
    
    func isAlphanumeric() -> Bool {
        return self.rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) == nil && self != ""
    }
    
    func isAlphanumeric(ignoreDiacritics: Bool = false) -> Bool {
        if ignoreDiacritics {
            return self.range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil && self != ""
        }
        else {
            return self.isAlphanumeric()
        }
    }
    
}
