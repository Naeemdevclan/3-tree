 //
//  AuditView.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 19/04/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
import Darwin
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


var imageCache = [String:UIImage]()
var isMreasurementTmClothing = false
var isMreasurementTmClothingCode = ""

class AuditView: UIViewController, UITextViewDelegate, UITabBarDelegate, UIWebViewDelegate {

    @IBOutlet weak var lblStyleNo: UILabel!
    
    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    weak var tab1vc: UIViewController!
    weak var tab2vc: UIViewController!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!

    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
//    @IBOutlet weak var ImgRightLabel: UILabel!
    @IBOutlet weak var commentBox: UITextView!
    
    @IBOutlet weak var AuditCodeLabel: UILabel!
    @IBOutlet weak var AuditStageLabel: UILabel!
    @IBOutlet weak var Picture: UIImageView!
    
    @IBOutlet weak var orderQtyLabel: UILabel!
     
    @IBOutlet weak var styleNo: UILabel!
    @IBOutlet weak var poNo: UILabel!
    @IBOutlet weak var brandName: UILabel!
    @IBOutlet weak var season: UILabel!
    @IBOutlet weak var etdRequired: UILabel!
    @IBOutlet weak var sampleSize: UILabel!
    @IBOutlet weak var VendorName: UILabel!
    @IBOutlet weak var Sizes: UILabel!

    @IBOutlet weak var commentsWeb: UIWebView!
    
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    let fileManager = FileManager.default
    var jsonFilePath2:URL!
//    var created:Bool!
    var dictionary:NSDictionary!
    
    var recievedAuditFromAuditModeViewController: String!
    var AuditScheduleDate: String!
    
    var sampleCheckedForAuditProgress: String!
    var sampleSizeForAuditProgress = ""
    var ReportId:String!
    var colors:[Any]!
    var aql:String = ""
    var defectRatein = ""
    var defectAllwedin = ""
    
    var strLabel = UILabel()
    var messageFrame = UIView()
    var activityIndicator = UIActivityIndicatorView()

    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnEditLabel: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnCancelLabel: UILabel!
    
    
    @IBOutlet weak var startButtonLabel: UILabel!

    @IBAction func BtnCancelPressed(_ sender: Any) {
        let alertView = UIAlertController(title: "", message: "Are you sure, You want to delete this auidt code?(\(AuditCodeLabel.text!))", preferredStyle: .alert)
        let Reload_action: UIAlertAction = UIAlertAction(title: "Yes", style: .default, handler: { (UIAlertAction) -> Void in
                  self.progressBarDisplayer("Loading. . .", true)
            _ = self.performLoadDataRequestWithURL(self.getUrl())
        })
        let no_action: UIAlertAction = UIAlertAction(title: "No", style: .default, handler: { (UIAlertAction) -> Void in
            self.activityIndicator.stopAnimating()
            self.messageFrame.removeFromSuperview()
        })
        
        alertView.addAction(Reload_action)
        alertView.addAction(no_action)
        self.present(alertView, animated: true, completion: nil)
    }
    @IBAction func BtnEditPressed(_ sender: UIButton) {
        if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
            //                self.dismissViewControllerAnimated(true, completion: nil)
            resultController.EditAudit = "Edit"
            resultController.auditCode = AuditCodeLabel.text!
            present(resultController, animated: true, completion: nil)
        }
        

    }
    @IBAction func BtnStartPressed(_ sender: AnyObject) {
       
        print(sampleCheckedForAuditProgress,sampleSizeForAuditProgress)
        if sampleCheckedForAuditProgress != sampleSizeForAuditProgress || Int(sampleCheckedForAuditProgress) == 0 {
            
            
            
//            if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "specsMeasurementsViewController") as? SpecsMeasurementsViewController {
//                resultController.AuditCodeSp = self.AuditCodeLabel.text!
//                resultController.AuditStageSp = self.AuditStageLabel.text!
//
//                resultController.AuditStageColorSp = self.AuditStageLabel.backgroundColor!
//
//                //resultController.SampleCheckedSp = SampleChecked.text!
//                resultController.SampleSizeSp = self.sampleSizeForAuditProgress
//                resultController.Report_Id = self.ReportId
//                self.present(resultController, animated: true, completion: nil)
//            }

            performSegue(withIdentifier: "sendDataToAuditProgress", sender: ReportId)
        }
            
        else if sampleCheckedForAuditProgress == sampleSizeForAuditProgress{
            
            print("sampleChecked and sampleSize both are equal")
           
            if ReportId == "36"{  // Hybrid
              
                performSegue(withIdentifier: "Goto_Main2_HybridCommentVC", sender: self.AuditCodeLabel.text!)
                
            }else if ReportId == "28" || ReportId == "32" || ReportId == "46"{  // Controlist/Arcadia
                if let completeMeasurement = defaults.object(forKey: "MeasurementsResults\(self.AuditCodeLabel.text!)"){
                    
                    if let completeComment = defaults.object(forKey: "AuditComments\(self.AuditCodeLabel.text!)"){
                        
                        if let completeAuditSummary = defaults.object(forKey: "AuditSummary\(self.AuditCodeLabel.text!)"){
                            //GO tO AUDIT QTY
                            performSegue(withIdentifier: "gotAuditQty", sender: self.AuditCodeLabel.text!)

                        }else{
                            //GO tO AUDIT SUMMARY
                            performSegue(withIdentifier: "gotoRemarks", sender: self.AuditCodeLabel.text!)

                        }

                    }else{
                        //GO tO AUDIT COMMENTS

                        performSegue(withIdentifier: "gotoAuditComment", sender: self.AuditCodeLabel.text!)

                    }
                    
                }else{
                performSegue(withIdentifier: "GoToMeasurementResults", sender: self.AuditCodeLabel.text!)
                }
           // gotoMGFCommentVC
                //gotoLevisCommentVC
            }else if ReportId == "14" || ReportId == "34"{  // Controlist/Arcadia
                
                performSegue(withIdentifier: "gotoMGFCommentVC", sender: self.AuditCodeLabel.text!)
                // gotoMGFCommentVC
            }else if ReportId == "45" || ReportId == "44"{  // Controlist/Arcadia
                if let saveLevis = defaults.object(forKey: "saveLevis\(AuditCodeLabel.text!)"){
                    performSegue(withIdentifier: "gotoLevisFinal", sender: self.AuditCodeLabel.text!)

//                    resultController.AuditCodein = AuditCodein
//                    resultController.AuditStagein = AuditStagein
//                    resultController.AuditStageColorin = AuditStageColorin
//                    resultController.defectRatevalue = self.defectRatevalue
//                    resultController.sampleChecked = sampleChecked
//                    resultController.ReportID_against_current_audit = ReportID_against_current_audit!
//                    if !self.arrayOfDefectsLoggedRecieved.isEmpty{
//                        resultController.arrayOfDefectsLoggedRecieved = self.arrayOfDefectsLoggedRecieved
//                    }
                    
                    

                }else{
                performSegue(withIdentifier: "gotoLevisCommentVC", sender: self.AuditCodeLabel.text!)
                }
                // gotoMGFCommentVC
            }else if ReportId == "38" {  // Controlist/Arcadia
                
                print(self.AuditCodeLabel.text!)
                print("isMreasurementTmClothingCode \(self.AuditCodeLabel.text!)")
                if let resume = defaults.object(forKey: "isMreasurementTmClothingCode \(self.AuditCodeLabel.text!)"){
                    if(resume as! String == self.AuditCodeLabel.text!){
                        self.performSegue(withIdentifier: "gotoTMClothingCommentVC", sender: self.AuditCodeLabel.text!)
                    }
                    else{
                        performSegue(withIdentifier: "gotoMeasurementStageTMClothing", sender: self.AuditCodeLabel.text!)
                    }
                }else{
                    performSegue(withIdentifier: "gotoMeasurementStageTMClothing", sender: self.AuditCodeLabel.text!)
                }
//
            }else{  //Default Report
                
                performSegue(withIdentifier: "DefaultAuditComment", sender: self.AuditCodeLabel.text!)
            }
        }
            
//        else if sampleCheckedForAuditProgress < sampleSizeForAuditProgress && Int(sampleCheckedForAuditProgress) > 0{
//            
//            print("sampleChecked is less than sampleSize")
//            performSegueWithIdentifier("resumeAudit", sender: self.AuditCodeLabel.text)
//        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = true
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
//        commentBox.delegate = self
//        // Do any additional setup after loading the view.
////        ImgRightLabel.transform = CGAffineTransvformMakeRotation(CGFloat(-M_PI_2))
//        commentBox.layer.borderWidth = 1
//        commentBox.layer.borderColor = UIColor.grayColor().CGColor
        
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        //        self.scrollView.contentSize.height = 10000
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Selected Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)

       
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("AuditMode.json")
        jsonFilePath2 = jsonFilePath
        
        
        
        
        self.AuditCodeLabel.text = recievedAuditFromAuditModeViewController
        do {
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath2.path, encoding: String.Encoding.utf8.rawValue) as String
            print("AuditMode.json = \(readString)")
            
            self.showDataWhenOffline(readString)
            
        } catch let error as NSError {
            print(error.description)
        }
        btnEdit.isHidden = true
        btnEditLabel.isHidden = true
        btnCancel.isHidden = true
        btnCancelLabel.isHidden = true
//        commentsWeb.layer.borderWidth = 1.0
//        commentsWeb.layer.borderColor = UIColor.gray.cgColor
        if let userType = defaults.object(forKey: "UserType"){
            if("\(userType as! String)" == "LEVIS"){
                // audit_Item = "BOOKING"
                //AuditTypeView.isHidden = false
               lblStyleNo.text = "Product Code"
                lblStyleNo.frame.size.width = lblStyleNo.frame.size.width + 100
                self.styleNo.frame.origin.x = self.styleNo.frame.origin.x + 100

            }else{
                
            }}

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        let d = defaults.object(forKey: self.AuditCodeLabel.text!)as! String
        print("defaults.auditcode date =",d)
       
   print(sampleCheckedForAuditProgress)
        //changing start button label if below condition stands true
        if Int(sampleCheckedForAuditProgress) == 0 {
            self.startButtonLabel.text = "Start"
            btnEdit.isHidden = false
            btnEditLabel.isHidden = false
            btnCancel.isHidden = false
            btnCancelLabel.isHidden = false

        }
        else if sampleCheckedForAuditProgress == sampleSizeForAuditProgress{
            
            self.startButtonLabel.text = "Resume"
            btnEdit.isHidden = true
            btnEditLabel.isHidden = true
            btnCancel.isHidden = true
            btnCancelLabel.isHidden = true

            
        }
        else if Int(sampleCheckedForAuditProgress) < Int(sampleSizeForAuditProgress) && Int(sampleCheckedForAuditProgress) > 0{
            
            self.startButtonLabel.text = "Resume"
            btnEdit.isHidden = true
            btnEditLabel.isHidden = true
            btnCancel.isHidden = true
            btnCancelLabel.isHidden = true

        }

    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func webViewDidStartLoad(webView: UIWebView) {
//        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
//    }
//    
//    func webViewDidFinishLoad(webView: UIWebView) {
//        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
//    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
//            defaults.removeObjectForKey("userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
//        case 4:
//            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
//                //                self.dismissViewControllerAnimated(true, completion: nil)
//                presentViewController(resultController, animated: true, completion: nil)
//            }
//            
//            let selectedImage:UIImage = UIImage(named: "Selected QAReport")!
//            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
//            
//            break
        case 5:
            if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                                if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                                    self.dismiss(animated: true, completion: nil)

                                    present(resultController, animated: true, completion: nil)
                                }
                            }else{
                                if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                                    self.dismiss(animated: true, completion: nil)

                                    present(resultController, animated: true, completion: nil)
                                }
                    }

            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_selected_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break

        default:
            break
        }
    }

    
    func showDataWhenOffline(_ savedData:String?){
        
        if savedData != nil {
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            self.dictionary = self.parseJSON(savedData!)
            print("\(self.dictionary)")
            let All_Audits = self.dictionary["Audits"]! as! [[String : AnyObject]]
            
            print("Total Audits = \((self.dictionary["Audits"]! as AnyObject).count)")
            
//            self.totalRowsInTable = self.dictionary["Audits"]!.count
            print("All_Audits count = \(All_Audits.count)")
            if All_Audits.count > 0{
            for Audit in All_Audits{
                
                print(Audit)
                let AuditCodeName = Audit["AuditCode"]! as! String
                
                if AuditCodeName == self.AuditCodeLabel.text! {
                      print(Audit)
//                    print(Audit["AuditView"]!["BrandId"]!)
//                    print("Vendor =",Audit["AuditView"]!["Vendor"]!)
//                    print("Sizes =",Audit["AuditView"]!["Sizes"])
                    let orderQty = Audit["AuditView"]!["OrderQty"]
                    print(orderQty)
                    if(orderQty is String){
                    orderQtyLabel.text = "\(orderQty as! String)"
                    }else if(orderQty is NSNumber){
                        orderQtyLabel.text = "\(orderQty as! NSNumber)"
                    }
                    if let aqlVlaue = Audit["AuditView"]!["Aql"]{
                       // aql = aqlVlaue as! String
                        print(aqlVlaue!)
                        
                        let AQLData:[String] = ["1.5","2.5","4","6.5"]
                        if(AQLData.contains(aqlVlaue as! String)){
                            aql = aqlVlaue as! String
                        }
                          //  else{
//                           aql = AQLData[Int(aqlVlaue as! String)! - 1]
//                        }
                    }
                    if let DefectsAllowed = Audit["AuditView"]!["DefectsAllowed"]{
                        defectAllwedin = "\(DefectsAllowed as AnyObject)"
                    }
                    if let aqlVlaue = Audit["AuditView"]!["Dr"]{
                        defectRatein = aqlVlaue as! String
                    }
                    var sizesText:String = ""
                    if(Audit["AuditView"]!["Sizes"] is [String:AnyObject]){
                    let sizeArray = Audit["AuditView"]!["Sizes"] as! [String:AnyObject]
                    
                   
                    var count = 1
                    for s in sizeArray{
                        print("s.1 =",s.1)
                        sizesText.append("\(s.1)")
                        
                        if count != sizeArray.count{
                            count += 1
                            sizesText.append(" ")
                        }
                    }
                    sizesText = sizesText.replacingOccurrences(of: " ", with: ",")
                    print("sizesText =",sizesText)
                    }
                    
                    self.AuditStageLabel.text = Audit["AuditView"]!["AuditStage"]! as? String
                    let stageHEXcolor = Audit["AuditView"]!["Color"]! as! String
                    print(stageHEXcolor)
                    self.colors = Audit["AuditView"]!["Colors"]! as! [Any]
                    print("Colors")
                    print(self.colors)
                    self.AuditStageLabel.backgroundColor = hexStringToUIColor(stageHEXcolor)
                    // - - -
                    print("self.AuditStageLabel.backgroundColor\(self.AuditStageLabel.backgroundColor)")
                    let imageUrl = Audit["AuditView"]!["Picture"]! as! String
                    self.displayImage(imageUrl)
                    // - - -
                    self.poNo.text = Audit["AuditView"]!["Po"] as? String
                    print(self.poNo.text)
                   //
                    if let userType = defaults.object(forKey: "UserType"){
                        if("\(userType as! String)" == "LEVIS"){
                            // audit_Item = "BOOKING"
                            //AuditTypeView.isHidden = false
                            lblStyleNo.text = "Product Code"
                            lblStyleNo.frame.size.width = lblStyleNo.frame.size.width + 100
                            self.styleNo.frame.origin.x = self.styleNo.frame.origin.x + 100
                            self.styleNo.text = Audit["AuditView"]!["ProductCode"] as? String

                        }else{
                            self.styleNo.text = Audit["AuditView"]!["Style"] as? String

                        }}

                    self.brandName.text = Audit["AuditView"]!["Brand"] as? String
                    self.VendorName.text = Audit["AuditView"]!["Vendor"] as? String
                    self.Sizes.text = sizesText
                    self.season.text = Audit["AuditView"]!["Season"] as? String
                    self.etdRequired.text = Audit["AuditView"]!["EtdRequired"] as? String
                    self.sampleSize.text = Audit["AuditView"]!["SampleSize"] as? String
                    self.AuditScheduleDate = Audit["AuditView"]!["AuditDate"] as! String
                    
                    ReportId = Audit["AuditView"]!["ReportId"] as! String
                    
                    //Going to show comments
                    let style_id = Audit["StyleId"]! as! String
                    print(style_id)
                    var requestURL: URL!
                    var request: URLRequest!
                    // loading data from web
                    requestURL = URL(string:"http://app.3-tree.com/protoware/style-comments.php?User=\(defaults.object(forKey: "userid")as! String)&StyleId=\(style_id)")
                    print(requestURL)
                    request = URLRequest(url: requestURL!)
                    
                    commentsWeb.loadRequest(request)
            
                    // StyleId
                    sampleCheckedForAuditProgress = Audit["AuditView"]!["SampleChecked"]! as! String
                    sampleSizeForAuditProgress = Audit["AuditView"]!["SampleSize"]! as! String
                    print("sampleCheckedForAuditProgress =",sampleCheckedForAuditProgress)
                    print("sampleSizeForAuditProgress =",sampleSizeForAuditProgress)
                    
                    //COMPARE that if there any sampleChecked exists in TempDefaults
                    print("TempDefault in AuditView = ",TempDefaults.object(forKey: "\(self.AuditCodeLabel.text!)Checked"))
                    if var TempDefaultSampleChecked = TempDefaults.object(forKey: "\(self.AuditCodeLabel.text!)Checked"){
                        
                        sampleCheckedForAuditProgress = TempDefaultSampleChecked as! String
                    }
                    
                    break
                }
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            }else{
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                print("data count is less than 1")
            }
        
        }else{
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            print("data is nil")
        }
    }
    func displayImage(_ imgUrl: String){
        // - - - - Loading images- - - - - - -
        
        // If this image is already cached, don't re-download
        if let img = imageCache[imgUrl] {
            self.Picture.image = img
        }
        else {
            // The image isn't cached, download the img data
            // We should perform this in a background thread
            print("url string:")
            print(imgUrl)
            let request: URLRequest = URLRequest(url: URL(string: imgUrl)!)
            let mainQueue = OperationQueue.main
            NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
                if error == nil {
                    // Convert the downloaded data in to a UIImage object
                    let image = UIImage(data: data!)
                    // Store the image in to our cache
                    imageCache[imgUrl] = image
                    // Update the cell
                    //                    dispatch_async(dispatch_get_main_queue(), {
                    if let imageToUpdate = image {
                        self.Picture.image = imageToUpdate
                    }
                    //                    })
                }
                else {
                    print("Error: \(error!.localizedDescription)")
                }
            })
        }
        // - - - -END - -Loading images- - - - - - -
    }
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    func hexStringToUIColor (_ hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
//        (NSCharacterSet.whitespaceAndNewlineCharacterSet() as NSCharacterSet).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = cString.substring(from: cString.characters.index(cString.startIndex, offsetBy: 1))
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
  
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      
        if segue.identifier == "sendDataToAuditProgress"{
            
            let destinationVC = segue.destination as! AuditProgress
            let Report_id_toSend = sender as! String
            
            //Set default values to critical and minor buttons
            destinationVC.criticalBtnHidden = false
            destinationVC.minorBtnHidden = false
            destinationVC.sampleSpecsHidden = false
            destinationVC.SelecteColorsProgressAudit = self.colors
            destinationVC.aqlLevelValue = aql
            destinationVC.defectRatevalue = defectRatein
            destinationVC.defectAllwed = defectAllwedin
            
            
            if Report_id_toSend == "11"||Report_id_toSend == "14"||Report_id_toSend == "20"||Report_id_toSend == "23"||Report_id_toSend == "32"||Report_id_toSend == "36"||Report_id_toSend == "46" {
                
                destinationVC.criticalBtnHidden = false
                destinationVC.minorBtnHidden = false
                destinationVC.aqlLevelHidden = true

                
            }else if Report_id_toSend == "25"{
                
                destinationVC.minorBtnHidden = true
                destinationVC.criticalBtnHidden = false
                destinationVC.aqlLevelHidden = true

                
            }else if(Report_id_toSend == "34" || Report_id_toSend == "14"){
                
                destinationVC.criticalBtnHidden = false
                destinationVC.aqlLevelHidden = false
            }
            else if(Report_id_toSend == "45" || Report_id_toSend == "44"){
                destinationVC.criticalBtnHidden = false
                destinationVC.aqlLevelHidden = false
                destinationVC.minorBtnHidden = true
                destinationVC.sampleSpecsHidden = false
            }
            else{
                destinationVC.criticalBtnHidden = true
                destinationVC.aqlLevelHidden = true

            }
            
//            destinationVC.criticalBtnHidden = self.brandName.text == "Basler"
            
            print("destinationVC.criticalBtnHidden=\(destinationVC.criticalBtnHidden!)")
            destinationVC.AuditCodein = self.AuditCodeLabel.text!
            destinationVC.AuditStagein = self.AuditStageLabel.text!
            destinationVC.AuditStageColorin = self.AuditStageLabel.backgroundColor!
            
            print("sampleCheckedForAuditProgress=\(sampleCheckedForAuditProgress)")
            destinationVC.SampleCheckedin = self.sampleCheckedForAuditProgress
            destinationVC.SampleSizein = self.sampleSizeForAuditProgress
            destinationVC.AuditScheduleDate = self.AuditScheduleDate
            
            destinationVC.ReportId = sender as! String
        }
        else if segue.identifier == "GoToMeasurementResults"{
            
            let destinationVC = segue.destination as! MeasurementResults
            destinationVC.AuditCodein = sender as! String
            destinationVC.AuditStagein = self.AuditStageLabel.text!
            destinationVC.AuditStageColorin = self.AuditStageLabel.backgroundColor!
            
            // here send ReportId to commentsVC for hide/show between two views in commentsView.
            destinationVC.ReportID_against_current_audit = self.ReportId
            
            
            present(destinationVC, animated: true, completion: nil)
        
        }else if segue.identifier == "gotoMeasurementStageTMClothing"{
            
            let destinationVC = segue.destination as! Measurement_Stage_TMClothing
            destinationVC.AuditCodeIn = sender as! String
            destinationVC.AuditStageIn = self.AuditStageLabel.text!
            destinationVC.AuditStageColor = self.AuditStageLabel.backgroundColor!
            
            // here send ReportId to commentsVC for hide/show between two views in commentsView.
            destinationVC.ReportID_against_current_audit = self.ReportId
            destinationVC.SelecteColors = self.colors
            
            
           // present(destinationVC, animated: true, completion: nil)
          //gotoLevisCommentVC
        }else if segue.identifier == "gotoMGFCommentVC"{
            
            let destinationVC = segue.destination as! MGFCommentVC
            destinationVC.AuditCodein = sender as! String
            destinationVC.AuditStagein = self.AuditStageLabel.text!
            destinationVC.AuditStageColorin = self.AuditStageLabel.backgroundColor!
            destinationVC.defectRatevalue = defectRatein
            destinationVC.sampleChecked = sampleCheckedForAuditProgress
            //destinationVC.defectRatevalue = self.defectRatevalue
            //resultController.sampleChecked = sampleChecked
                //destinationVC.arrayOfDefectsLoggedRecieved = self.arrayOfDefectsLogged
            // here send ReportId to commentsVC for hide/show between two views in commentsView.
            destinationVC.ReportID_against_current_audit = self.ReportId
           // destinationVC.SelecteColors = self.colors
            
            
            // present(destinationVC, animated: true, completion: nil)
            
        }else if segue.identifier == "gotoLevisCommentVC"{
            
            let destinationVC = segue.destination as! LevisCommentVC
            destinationVC.AuditCodein = sender as! String
            destinationVC.AuditStagein = self.AuditStageLabel.text!
            destinationVC.AuditStageColorin = self.AuditStageLabel.backgroundColor!
            destinationVC.defectRatevalue = defectRatein
            destinationVC.sampleChecked = sampleCheckedForAuditProgress
            //destinationVC.arrayOfDefectsLoggedRecieved = self.arrayOfDefectsLogged
            // here send ReportId to commentsVC for hide/show between two views in commentsView.
            destinationVC.ReportID_against_current_audit = self.ReportId
            // destinationVC.SelecteColors = self.colors
            
            
            // present(destinationVC, animated: true, completion: nil)
            
        }else if segue.identifier == "gotoLevisFinal"{
            
            let destinationVC = segue.destination as! SaveLevisCommentVC
            destinationVC.AuditCodein = sender as! String
            destinationVC.AuditStagein = self.AuditStageLabel.text!
            destinationVC.AuditStageColorin = self.AuditStageLabel.backgroundColor!
            destinationVC.defectRatevalue = defectRatein
            destinationVC.sampleChecked = sampleCheckedForAuditProgress
            //destinationVC.arrayOfDefectsLoggedRecieved = self.arrayOfDefectsLogged
            // here send ReportId to commentsVC for hide/show between two views in commentsView.
            destinationVC.ReportID_against_current_audit = self.ReportId
            // destinationVC.SelecteColors = self.colors
            
            
            // present(destinationVC, animated: true, completion: nil)
            
        }else if segue.identifier == "gotoTMClothingCommentVC"{
            
            //            let destinationNVC = segue.destination as! UINavigationController
            //            let destinationVC = destinationNVC.viewControllers.first as! TMClothingCommentVC
            let destinationVC = segue.destination as! TMClothingCommentVC
            
            //            let destinationVC = segue.destination as! HybridCommentViewController
            destinationVC.AuditCodein = self.AuditCodeLabel.text!
            destinationVC.AuditStagein = self.AuditStageLabel.text!
            destinationVC.AuditStageColorin = self.AuditStageLabel.backgroundColor!
            destinationVC.ReportID_against_current_audit = "38"
        }else if segue.identifier == "DefaultAuditComment"{
            let destinationVC = segue.destination as! DefaultCommentVC
            destinationVC.AuditCodein = self.AuditCodeLabel.text
            destinationVC.AuditStagein = self.AuditStageLabel.text!
            destinationVC.AuditStageColorin = self.AuditStageLabel.backgroundColor!
            
            
        }else if segue.identifier == "Goto_Main2_HybridCommentVC"{
            
            //HybridCommentVC
            let destinationVC = segue.destination as! HybridCommentViewController
            destinationVC.AuditCodein = sender as! String
            destinationVC.AuditStagein = self.AuditStageLabel.text!
            destinationVC.AuditStageColorin = self.AuditStageLabel.backgroundColor!
            destinationVC.ReportID_against_current_audit = self.ReportId
                    }
            //AUDIT COMMENTS
        else if segue.identifier == "gotoAuditComment"{
            let destinationVC = segue.destination as! AuditCommentsVC
            destinationVC.AuditCodein = sender as! String
            destinationVC.AuditStagein = self.AuditStageLabel.text!
            destinationVC.AuditStageColorin = self.AuditStageLabel.backgroundColor!
            
            // here send ReportId to commentsVC for hide/show between two views in commentsView.
            destinationVC.ReportID_against_current_audit = self.ReportId
            
            
            present(destinationVC, animated: true, completion: nil)
            
        }
            // FINAL REMARKS
        else if segue.identifier == "gotoRemarks"{
            let destinationVC = segue.destination as! FinalRemarksViewController
            destinationVC.AuditCodein = sender as! String
            destinationVC.AuditStagein = self.AuditStageLabel.text!
            destinationVC.AuditStageColorin = self.AuditStageLabel.backgroundColor!
            print(ReportId)
            // here send ReportId to commentsVC for hide/show between two views in commentsView.
            destinationVC.ReportID_against_current_audit = self.ReportId
            
            
            // present(destinationVC, animated: true, completion: nil)
            
        }
            // FINAL AUDIT QTY
        else if segue.identifier == "gotAuditQty"{
            let destinationVC = segue.destination as! AuditQuantity
            destinationVC.AuditCodein = sender as! String
            destinationVC.AuditStagein = self.AuditStageLabel.text!
            destinationVC.AuditStageColorin = self.AuditStageLabel.backgroundColor!
            
            // here send ReportId to commentsVC for hide/show between two views in commentsView.
            destinationVC.ReportID_against_current_audit = self.ReportId
            
            
            present(destinationVC, animated: true, completion: nil)
            
        }


        

        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
//Canlcel Audit
    var bodyData:String!
    func getUrl() -> URL {
        let toEscape = "http://app.3-tree.com/quonda/delete-audit.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
    
    func performLoadDataRequestWithURL(_ url: URL) -> String? {
        
                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodeLabel.text!)"
        
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = self.bodyData!.data(using: String.Encoding.utf8)
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main){
            response, data, error in
            print("\(response)")
            if (error != nil) {
                print(error!)
                                self.activityIndicator.stopAnimating()
                                self.messageFrame.removeFromSuperview()
            }
            
            if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                // - - - - - - write this JSON data to file- - - - - - - -
                //                print("created : \(self.created)")
                //                if self.created == true{
                //                if self.fileManager.fileExistsAtPath(self.jsonFilePath2.absoluteString, isDirectory: &self.isDirectory) {
                //                    do{
                //                        try json.writeToFile(self.jsonFilePath2.path!, atomically: true, encoding: NSUTF8StringEncoding)
                //                    } catch let error as NSError {
                //                        print("JSON data couldn't written to file // File not exist")
                //                        print(error.description)
                //                    }
                //                }
                print(json)
                self.dictionary = self.parseJSON(json)
                
                print("\(self.dictionary)")
                print("\(self.dictionary["Result"])")
                
                if ("\(self.dictionary["Status"]!)" == "OK") {
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                        if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                            self.dismiss(animated: true, completion: nil)
                            
                            self.present(resultController, animated: true, completion: nil)
                        }
                    }else{
                        if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                            self.dismiss(animated: true, completion: nil)
                            
                            self.present(resultController, animated: true, completion: nil)
                        }
                    }
                    

                }
                
            }//if data != nil
            else{
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
                let alertView = UIAlertController(title: "Cannot Delete" , message: "", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                    
                })
                
                alertView.addAction(ok_action)
                //                alertView.addAction(NO_action)
                
                self.present(alertView, animated: true, completion: nil)
                print("return data is nil")
            }
        }
        return json
    }
    func progressBarDisplayer(_ msg:String, _ indicator:Bool ) {
        print(msg)
        self.strLabel = UILabel(frame: CGRect(x: view.frame.midX - 40, y: view.frame.midY - 50, width: 200, height: 50))  // x: 50, y: 0, width: 200, height: 50
        self.strLabel.text = msg
        self.strLabel.textColor = UIColor.white
        self.messageFrame = UIView(frame: CGRect(x: 0, y: 69 , width: view.frame.width, height: view.frame.height - 118))  //x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50
        //        messageFrame.layer.cornerRadius = 15
        self.messageFrame.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        if indicator {
            self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
            self.activityIndicator.frame = CGRect(x: view.frame.midX - 80, y: view.frame.midY - 50, width: 50, height: 50)  // x: 0, y: 0, width: 50, height: 50
            self.activityIndicator.startAnimating()
            self.messageFrame.addSubview(activityIndicator)
        }
        self.messageFrame.addSubview(strLabel)
        self.view.addSubview(self.messageFrame)
    }


}
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}


