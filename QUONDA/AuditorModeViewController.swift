//
//  AuditorModeViewController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 15/03/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
import AVFoundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class AuditorModeViewController: UIViewController,UITabBarDelegate {

    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    weak var tab1vc: UIViewController!
    weak var tab2vc: UIViewController!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!
    
    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    
//    @IBOutlet weak var AuditCodesTable: UITableView!
    
    // IBOutlets
    
    
//    @IBOutlet weak var currentDateLabel: UILabel!
    
    @IBOutlet weak var currentDateLabel: UIButton!
    @IBOutlet weak var plannedAudits: UILabel!
    @IBOutlet weak var completedAudits: UILabel!
    @IBOutlet weak var pendingReports: UILabel!
    
    
//    @IBOutlet weak var scrollView: UIScrollView!
//    var AuditCodes = [String]()
    var AuditCodes: [String] = []
    var AuditCompleted: [String] = []
    var AuditColors: [String] = []
    var AuditBlocks:[Int] = []
    
    
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    var jsonFilePath2:URL!
    var created:Bool!
    var dictionary:NSDictionary!

    
    
    var strLabel = UILabel()
    var messageFrame = UIView()
    var activityIndicator = UIActivityIndicatorView()
 
    
    
    
    
    // New design screen outlets
    
    
    var firstTimeLoading :Bool = true
    
    @IBOutlet weak var OutMostScrollView: UIScrollView!
    @IBOutlet weak var OutMostStackView: UIStackView!
    
    @IBOutlet weak var stackVeiw1: UIStackView!
    @IBOutlet weak var stackVeiw2: UIStackView!
    @IBOutlet weak var stackVeiw3: UIStackView!
    @IBOutlet weak var stackVeiw4: UIStackView!
    @IBOutlet weak var stackVeiw5: UIStackView!
    @IBOutlet weak var stackVeiw6: UIStackView!
    @IBOutlet weak var stackVeiw7: UIStackView!
    @IBOutlet weak var stackVeiw8: UIStackView!
    @IBOutlet weak var stackVeiw9: UIStackView!
    @IBOutlet weak var stackVeiw10: UIStackView!
    @IBOutlet weak var stackVeiw11: UIStackView!
    @IBOutlet weak var stackVeiw12: UIStackView!
    
    
    
    
    @IBOutlet weak var Day1_Btn: UIButton!
    @IBOutlet weak var Day2_Btn: UIButton!
    @IBOutlet weak var Day3_Btn: UIButton!
    @IBOutlet weak var Day4_Btn: UIButton!
    @IBOutlet weak var Day5_Btn: UIButton!
    @IBOutlet weak var PreviousForwordDay_Btn: UIButton!
    
    
    @IBOutlet var Audits_and_StageDay1: [UILabel]!
    @IBOutlet var Audits_and_StageDay2: [UILabel]!
    @IBOutlet var Audits_and_StageDay3: [UILabel]!
    @IBOutlet var Audits_and_StageDay4: [UILabel]!
    @IBOutlet var Audits_and_StageDay5: [UILabel]!
    
    
    
    var sortedPreviousDays_withDate:[(String,(pDay:String,Stats:[(Audits:String,Stage:String)]))]? = []
    
    var sortedForwordDays_withDate:[(String,(pDay:String,Stats:[(Audits:String,Stage:String)]))]? = []
    
    
    
    var BlocksDataDictionary:[Int:[String:(completedState:String, color:String,onGoingStatus :String,deviceID:String)]]? = [:]


    var BlockDictionary1:[String:(Status:String,Color:UIColor,onGoingStatus :String,deviceID:String)]? = [:]
    var BlockDictionary2:[String:(Status:String,Color:UIColor,onGoingStatus :String,deviceID:String)]? = [:]
    var BlockDictionary3:[String:(Status:String,Color:UIColor,onGoingStatus :String,deviceID:String)]? = [:]
    var BlockDictionary4:[String:(Status:String,Color:UIColor,onGoingStatus :String,deviceID:String)]? = [:]
    var BlockDictionary5:[String:(Status:String,Color:UIColor,onGoingStatus :String,deviceID:String)]? = [:]
    
    var BlockDictionary6:[String:(Status:String,Color:UIColor,onGoingStatus :String,deviceID:String)]? = [:]
    var BlockDictionary7:[String:(Status:String,Color:UIColor,onGoingStatus :String,deviceID:String)]? = [:]
    
    var BlockDictionary8:[String:(Status:String,Color:UIColor,onGoingStatus :String,deviceID:String)]? = [:]
    var BlockDictionary9:[String:(Status:String,Color:UIColor,onGoingStatus :String,deviceID:String)]? = [:]
    var BlockDictionary10:[String:(Status:String,Color:UIColor,onGoingStatus :String,deviceID:String)]? = [:]
    var BlockDictionary11:[String:(Status:String,Color:UIColor,onGoingStatus :String,deviceID:String)]? = [:]
    var BlockDictionary12:[String:(Status:String,Color:UIColor,onGoingStatus :String,deviceID:String)]? = [:]
    
    let DefaultBlockDictionary = [
        "A":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "B":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "C":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "D":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "E":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "F":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        ]
    
    
    //MARK: Instance methods
    func colorButton(withColor color:UIColor, title:String) -> UIButton{
        let newButton = UIButton(type: .system)
        newButton.backgroundColor = color
        newButton.setTitle(title, for: UIControlState())
        newButton.setTitleColor(UIColor.white, for: UIControlState())
        
        
        return newButton
    }
    func colorButton2(withColor color:UIColor, title:String, Subtitle:String,onGoingStatus :String,deviceID:String) -> UIButton{
        
        
        print(deviceID)
        let newButton = UIButton(type: .system)
        
        newButton.backgroundColor = color
        newButton.setTitle(title, for: UIControlState())
        newButton.setTitleColor(UIColor.white, for: UIControlState())
        newButton.layer.borderColor = UIColor.darkGray.cgColor
        newButton.layer.borderWidth = 1.0
        let subTitle = UILabel(frame: CGRect(x: 1, y: 1, width: 6, height: 10))
        subTitle.textAlignment = NSTextAlignment.center
        subTitle.font = subTitle.font.withSize(6.0)
        subTitle.text = Subtitle
        subTitle.isHidden = true
        subTitle.tag = 1
        let deviceId = UILabel(frame: CGRect(x: 1, y: 1, width: 6, height: 10))
        deviceId.textAlignment = NSTextAlignment.center
        deviceId.font = deviceId.font.withSize(6.0)
        deviceId.text = deviceID
        deviceId.isHidden = true
        deviceId.tag = 2
        

//        var subTitle2 = UILabel(frame: CGRectMake(1, 6, 6, 10))
//        subTitle2.textAlignment = NSTextAlignment.Center
//        subTitle2.font = subTitle.font.fontWithSize(6.0)
//        subTitle2.text = Subtitle+"1234"
//        subTitle2.hidden = false
//        subTitle2.tag = 2
        
        newButton.addTarget(self, action: #selector(self.AuditCode_BtnPressed), for: .touchUpInside)
        
        newButton.addSubview(subTitle)
        newButton.addSubview(deviceId)
//        newButton.addSubview(subTitle2)
        
        if onGoingStatus == "Y"{
            
            let orignalColor = newButton.backgroundColor
            newButton.layer.borderColor = UIColor.lightGray.cgColor
            newButton.layer.borderWidth = 1.0
            UIView.animate(withDuration: 0.7, delay: 0.1, options: [UIViewAnimationOptions.repeat, UIViewAnimationOptions.autoreverse, UIViewAnimationOptions.allowUserInteraction], animations: {
                
                newButton.backgroundColor = UIColor.clear
                //                newButton.alpha = 0.0
            }, completion: { (Status) in
                
                newButton.backgroundColor = orignalColor
                //                    newButton.alpha = 1.0
            })
        }

        return newButton
    }
    
    
    func AuditCode_BtnPressed(_ senderBtn: UIButton) -> Void {
        print("ButtonCode =",senderBtn.titleLabel!.text!)
        print("ButtonCompletedStatus =",(senderBtn.subviews.first as! UILabel).text!)
        print("auditCode Btn Pressed.")
        print("Device ID =",(senderBtn.viewWithTag(2) as! UILabel).text!)

       // if(1 == 1){
        
//        for view in senderBtn.subviews as [UIView] {
//            if let btn = view as? UILabel {
//                print("btn.text =",btn.text)
//                if btn.tag == 2{
//                    print("subview2 is printing.",btn.text)
//                }else if btn.tag == 1{
//                    print("subview1 is printing.",btn.text)
//                }
//            }
//        }
        
        // if AuditCommentFile against this selected Po exists in doc dir then do nothing.

        let SelectedAuditCode = senderBtn.titleLabel!.text!
        let CompletedStatus = (senderBtn.subviews.first as! UILabel).text!
        print("SelectedAuditCode =", SelectedAuditCode)
        print("CompletedStatus =",CompletedStatus)
    
        var DeviceId = ""
//        if let identifierForVendor = UIDevice.current.identifierForVendor {
//            DeviceId = identifierForVendor.uuidString
//            print(DeviceId)
//        }
        
        if let uniqueId = KeychainService.loadPassword(){
            print(uniqueId as String)
            DeviceId = uniqueId as String
        }
       
            
            
            if let CommentStatus = TempDefaults.object(forKey: "commentStatus\(SelectedAuditCode)") as? Bool{
                print("Yes there is commentStatus Which is True")
                if CommentStatus == true{
                    
                    TempDefaults.removeObject(forKey: "commentStatus\(SelectedAuditCode)")
                    defaults.removeObject(forKey: "OfflineAudit\(SelectedAuditCode)")

//                    let myCell = tableView.cellForRowAtIndexPath(indexPath)
                    AuditCodeFromAuditorMode =  SelectedAuditCode
                    
                    performSegue(withIdentifier: "goto-KPI-MODE", sender: nil)
                }
                
            }else{
                
                if "\(CompletedStatus)" == "N" {
                    
                    if(DeviceId == (senderBtn.viewWithTag(2) as! UILabel).text! || (senderBtn.viewWithTag(2) as! UILabel).text! == "" )
                    {
                        
                   // defaults.set("Completed", forKey: "OfflineAudit\(AuditCodein!)")

                if let completedOffline = defaults.object(forKey:"OfflineAudit\(SelectedAuditCode)") {
                        
                        //do nothing
                        print(completedOffline as! String)
                        showMessageView(message:"Audit completed. Wait for the report to Sync.")
                        
                    }else{

                    performSegue(withIdentifier: "sendDataToAuditView", sender: SelectedAuditCode)
                   //
                    
                    }
                    }else{
                        showMessageView(message:"Sorry, you cannot conduct selected audit on this phone. Please use same phone which is used for scheduling")
                        
                    }
                }
                else if "\(CompletedStatus)" == "Y" {
                    
                    AuditCodeFromAuditorMode = SelectedAuditCode
                   // showMessageView(message:"Audit completed. Wait for the report to Sync.")

                    performSegue(withIdentifier: "goto-KPI-MODE", sender: nil)

//                    let alertView:UIAlertController = UIAlertController(title: nil, message: "Wait to Audit Sync", preferredStyle: UIAlertControllerStyle.alert)
//                    let alertAction:UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel ,handler: nil )
//                    
//                    alertView.addAction(alertAction)
//                    
//                    self.present(alertView, animated: true, completion: { () -> Void in
                    
                   // })

                    print("AuditCodeOfDashboardCell in auditorMode = \(AuditCodeFromAuditorMode!)")
                   // performSegue(withIdentifier: "goto-KPI-MODE", sender: nil)
                }
                
        }
        
    
    }
    
    
    func showMessageView(message:String){
        
       // self.view.makeToast(message: "\(message)")
       // view.makeToast(message: message, duration: 2.0, position: HRToastPositionDefault as AnyObject, title: "")
     //view.makeToastActivity(message: message)
       // self.view.makeToast(message: message, duration:  2.0, position: HRToastPositionDefault as AnyObject)
        showToast(message: message)

        // basic usage
       // self.view.makeToast("This is a piece of toast")
        
        // toast with a specific duration and position
     //   self.view.makeToast("This is a piece of toast", duration: 3.0, position: .top)
        
//        // toast presented with multiple options and with a completion closure
//        self.view.makeToast("This is a piece of toast", duration: 2.0, point: CGPoint(x: 110.0, y: 110.0), title: "Toast Title", image: UIImage(named: "toast.png")) { didTap in
//            if didTap {
//                print("completion from tap")
//            } else {
//                print("completion without tap")
//            }
      //  }
      //  self.navigationController?.view.makeToast("This is a piece of toast with a custom style", duration: 3.0, position: .bottom)
        // display toast with an activity spinner
       // self.view.makeToastActivity(.center)
        
        // display any view as toast
       // self.view.showToast(myView)
    }
    
    func DisplayBlockData1(){
        //generate an array
        
        if BlockDictionary1 != nil && !(BlockDictionary1?.isEmpty)! {
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw1.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw1.removeArrangedSubview(view)
                }
            }
            
            
            var buttonArray = [UIButton]()
           
            for Data in BlockDictionary1!{
                
                buttonArray += [colorButton2(withColor: Data.1.Color, title: Data.0, Subtitle: Data.1.Status,onGoingStatus :Data.1.onGoingStatus,deviceID:Data.1.deviceID)]
                
            }
//            for (myKey,myValue) in BlockDictionary1!{
//                buttonArray += [colorButton(withColor: myValue, title: myKey)]
//            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 10
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw1.spacing = 10
                    stackVeiw1.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
            
            if count == 1 && !BArray.isEmpty{
                
                let emptyButton = UIButton(type: .system)
                emptyButton.isUserInteractionEnabled = false
                BArray.append(emptyButton)
                
                let emptyButton2 = UIButton(type: .system)
                emptyButton2.isUserInteractionEnabled = false
                BArray.append(emptyButton2)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                stackVeiw1.spacing = 10
                stackVeiw1.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }else if count == 2 && !BArray.isEmpty{
                
                let emptyButton1 = UIButton(type: .system)
                emptyButton1.isUserInteractionEnabled = false
                BArray.append(emptyButton1)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw1.spacing = 10
                stackVeiw1.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }
        }// if BlockDictionary is not nil
        else{
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw1.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw1.removeArrangedSubview(view)
                }
            }
            
            var buttonArray = [UIButton]()
            for (myKey,myValue) in DefaultBlockDictionary{
                buttonArray += [colorButton(withColor: myValue, title: "")]
            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 0
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw1.spacing = 0
                    stackVeiw1.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
        }// end of else part
    }
    
    
    func DisplayBlockData2(){
        //generate an array
        
        if BlockDictionary2 != nil && !(BlockDictionary2?.isEmpty)! {
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw2.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw2.removeArrangedSubview(view)
                }
            }
            
            
            var buttonArray = [UIButton]()
            
            for Data in BlockDictionary2!{
                
                buttonArray += [colorButton2(withColor: Data.1.Color, title: Data.0, Subtitle: Data.1.Status,onGoingStatus :Data.1.onGoingStatus,deviceID:Data.1.deviceID)]
                
            }
            
//            for (myKey,myValue) in BlockDictionary2!{
//                buttonArray += [colorButton(withColor: myValue, title: myKey)]
//            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 10
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw2.spacing = 10
                    stackVeiw2.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
            
            if count == 1 && !BArray.isEmpty{
                
                let emptyButton = UIButton(type: .system)
                emptyButton.isUserInteractionEnabled = false
                BArray.append(emptyButton)
                
                let emptyButton2 = UIButton(type: .system)
                emptyButton2.isUserInteractionEnabled = false
                BArray.append(emptyButton2)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw2.spacing = 10
                stackVeiw2.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }else if count == 2 && !BArray.isEmpty{
                
                let emptyButton1 = UIButton(type: .system)
                emptyButton1.isUserInteractionEnabled = false
                BArray.append(emptyButton1)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw2.spacing = 10
                stackVeiw2.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }
        }// if BlockDictionary is not nil
        else{
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw2.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw2.removeArrangedSubview(view)
                }
            }
            
            var buttonArray = [UIButton]()
            for (myKey,myValue) in DefaultBlockDictionary{
                buttonArray += [colorButton(withColor: myValue, title: "")]
            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 0
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw2.spacing = 0
                    stackVeiw2.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
        }// end of else part
    
    }
    
    
    
    func DisplayBlockData3(){
        //generate an array
        
        if BlockDictionary3 != nil && !(BlockDictionary3?.isEmpty)! {
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw3.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw3.removeArrangedSubview(view)
                }
            }
            
            
            var buttonArray = [UIButton]()
            
            for Data in BlockDictionary3!{
                
                buttonArray += [colorButton2(withColor: Data.1.Color, title: Data.0, Subtitle: Data.1.Status,onGoingStatus :Data.1.onGoingStatus,deviceID:Data.1.deviceID)]
                
            }
            
//            for (myKey,myValue) in BlockDictionary3!{
//                buttonArray += [colorButton(withColor: myValue, title: myKey)]
//            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 10
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw3.spacing = 10
                    stackVeiw3.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
            
            if count == 1 && !BArray.isEmpty{
                
                let emptyButton = UIButton(type: .system)
                emptyButton.isUserInteractionEnabled = false
                BArray.append(emptyButton)
                
                let emptyButton2 = UIButton(type: .system)
                emptyButton2.isUserInteractionEnabled = false
                BArray.append(emptyButton2)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw3.spacing = 10
                stackVeiw3.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }else if count == 2 && !BArray.isEmpty{
                
                let emptyButton1 = UIButton(type: .system)
                emptyButton1.isUserInteractionEnabled = false
                BArray.append(emptyButton1)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw3.spacing = 10
                stackVeiw3.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }
        }// if BlockDictionary is not nil
        else{
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw3.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw3.removeArrangedSubview(view)
                }
            }
            
            var buttonArray = [UIButton]()
            for (myKey,myValue) in DefaultBlockDictionary{
                buttonArray += [colorButton(withColor: myValue, title: "")]
            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 0
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                   
                    stackVeiw3.spacing = 0
                    stackVeiw3.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
        }
    
    }  // End DisplayBlockData3
    
    
    
    
    func DisplayBlockData4(){
        //generate an array
        
        if BlockDictionary4 != nil && !(BlockDictionary4?.isEmpty)! {
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw4.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw4.removeArrangedSubview(view)
                }
            }
            
            
            var buttonArray = [UIButton]()
         
            for Data in BlockDictionary4!{
                
                buttonArray += [colorButton2(withColor: Data.1.Color, title: Data.0, Subtitle: Data.1.Status,onGoingStatus :Data.1.onGoingStatus,deviceID:Data.1.deviceID)]
                
            }
            
//            for (myKey,myValue) in BlockDictionary4!{
//                buttonArray += [colorButton(withColor: myValue, title: myKey)]
//            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 10
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw4.spacing = 10
                    stackVeiw4.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
            
            if count == 1 && !BArray.isEmpty{
                
                let emptyButton = UIButton(type: .system)
                emptyButton.isUserInteractionEnabled = false
                BArray.append(emptyButton)
                
                let emptyButton2 = UIButton(type: .system)
                emptyButton2.isUserInteractionEnabled = false
                BArray.append(emptyButton2)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw4.spacing = 10
                stackVeiw4.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }else if count == 2 && !BArray.isEmpty{
                
                let emptyButton1 = UIButton(type: .system)
                emptyButton1.isUserInteractionEnabled = false
                BArray.append(emptyButton1)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw4.spacing = 10
                stackVeiw4.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }
        }// if BlockDictionary is not nil
        else{
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw4.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw4.removeArrangedSubview(view)
                }
            }
            
            var buttonArray = [UIButton]()
            for (myKey,myValue) in DefaultBlockDictionary{
                buttonArray += [colorButton(withColor: myValue, title: "")]
            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 0
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw4.spacing = 0
                    stackVeiw4.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
        }
        
    } // End DisplayBlockData4
    
    
    func DisplayBlockData5(){
        //generate an array
        
        if BlockDictionary5 != nil && !(BlockDictionary5?.isEmpty)! {
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw5.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw5.removeArrangedSubview(view)
                }
            }
            
            
            var buttonArray = [UIButton]()
           
            for Data in BlockDictionary5!{
                
                buttonArray += [colorButton2(withColor: Data.1.Color, title: Data.0, Subtitle: Data.1.Status,onGoingStatus :Data.1.onGoingStatus,deviceID:Data.1.deviceID)]
                
            }
            
//            for (myKey,myValue) in BlockDictionary5!{
//                buttonArray += [colorButton(withColor: myValue, title: myKey)]
//            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 10
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw5.spacing = 10
                    stackVeiw5.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
            
            if count == 1 && !BArray.isEmpty{
                
                let emptyButton = UIButton(type: .system)
                emptyButton.isUserInteractionEnabled = false
                BArray.append(emptyButton)
                
                let emptyButton2 = UIButton(type: .system)
                emptyButton2.isUserInteractionEnabled = false
                BArray.append(emptyButton2)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw5.spacing = 10
                stackVeiw5.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }else if count == 2 && !BArray.isEmpty{
                
                let emptyButton1 = UIButton(type: .system)
                emptyButton1.isUserInteractionEnabled = false
                BArray.append(emptyButton1)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw5.spacing = 10
                stackVeiw5.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }
        }// if BlockDictionary is not nil
        else{
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw5.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw5.removeArrangedSubview(view)
                }
            }
            
            var buttonArray = [UIButton]()
            for (myKey,myValue) in DefaultBlockDictionary{
                buttonArray += [colorButton(withColor: myValue, title: "")]
            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 0
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw5.spacing = 0
                    stackVeiw5.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
        }
        
    }  // End DisplayBlockData5
    
    
    func DisplayBlockData6(){
        //generate an array
        
        if BlockDictionary6 != nil && !(BlockDictionary6?.isEmpty)! {
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw6.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw6.removeArrangedSubview(view)
                }
            }
            
            
            var buttonArray = [UIButton]()
            
            for Data in BlockDictionary6!{
                
                buttonArray += [colorButton2(withColor: Data.1.Color, title: Data.0, Subtitle: Data.1.Status,onGoingStatus :Data.1.onGoingStatus,deviceID:Data.1.deviceID)]
                
            }
            
//            for (myKey,myValue) in BlockDictionary6!{
//                buttonArray += [colorButton(withColor: myValue, title: myKey)]
//            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                let buttonSubTitle = b.subviews.first as! UILabel
                print("buttonSubTitle.text =",buttonSubTitle.text!)
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 10
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw6.spacing = 10
                    stackVeiw6.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
            
            if count == 1 && !BArray.isEmpty{
                
                let emptyButton = UIButton(type: .system)
                emptyButton.isUserInteractionEnabled = false
                BArray.append(emptyButton)
                
                let emptyButton2 = UIButton(type: .system)
                emptyButton2.isUserInteractionEnabled = false
                BArray.append(emptyButton2)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw6.spacing = 10
                stackVeiw6.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }else if count == 2 && !BArray.isEmpty{
                
                let emptyButton1 = UIButton(type: .system)
                emptyButton1.isUserInteractionEnabled = false
                BArray.append(emptyButton1)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw6.spacing = 10
                stackVeiw6.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }
        }// if BlockDictionary is not nil
        else{
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw6.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw6.removeArrangedSubview(view)
                }
            }
            
            var buttonArray = [UIButton]()
            for (myKey,myValue) in DefaultBlockDictionary{
                buttonArray += [colorButton(withColor: myValue, title: "")]
            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 0
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw6.spacing = 0
                    stackVeiw6.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
        }
        
    }  // End display keyboard 6
    
    
    
    func DisplayBlockData7(){
        //generate an array
        
        if BlockDictionary7 != nil && !(BlockDictionary7?.isEmpty)! {
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw7.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw7.removeArrangedSubview(view)
                }
            }
            
            
            var buttonArray = [UIButton]()
            
            for Data in BlockDictionary7!{
                
                buttonArray += [colorButton2(withColor: Data.1.Color, title: Data.0, Subtitle: Data.1.Status,onGoingStatus :Data.1.onGoingStatus,deviceID:Data.1.deviceID)]
                
            }
            
//            for (myKey,myValue) in BlockDictionary7!{
//            
//                buttonArray += [colorButton(withColor: myValue, title: myKey)]
//            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 10
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw7.spacing = 10
                    stackVeiw7.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
            
            if count == 1 && !BArray.isEmpty{
                
                let emptyButton = UIButton(type: .system)
                emptyButton.isUserInteractionEnabled = false
                BArray.append(emptyButton)
                
                let emptyButton2 = UIButton(type: .system)
                emptyButton2.isUserInteractionEnabled = false
                BArray.append(emptyButton2)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw7.spacing = 10
                stackVeiw7.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }else if count == 2 && !BArray.isEmpty{
                
                let emptyButton1 = UIButton(type: .system)
                emptyButton1.isUserInteractionEnabled = false
                BArray.append(emptyButton1)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw7.spacing = 10
                stackVeiw7.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }
        }// if BlockDictionary is not nil
        else{
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw7.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw7.removeArrangedSubview(view)
                }
            }
            
            var buttonArray = [UIButton]()
            for (myKey,myValue) in DefaultBlockDictionary{
                buttonArray += [colorButton(withColor: myValue, title: "")]
            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 0
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw7.spacing = 0
                    stackVeiw7.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
        } // else part ends
        
    }
    
    func DisplayBlockData8(){
        //generate an array
        
        if BlockDictionary8 != nil && !(BlockDictionary8?.isEmpty)! {
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw8.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw8.removeArrangedSubview(view)
                }
            }
            
            var buttonArray = [UIButton]()
            
            for Data in BlockDictionary8!{
                
                buttonArray += [colorButton2(withColor: Data.1.Color, title: Data.0, Subtitle: Data.1.Status,onGoingStatus :Data.1.onGoingStatus,deviceID:Data.1.deviceID)]
                
            }
            
//            for (myKey,myValue) in BlockDictionary8!{
//                buttonArray += [colorButton(withColor: myValue, title: myKey)]
//            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 10
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw8.spacing = 10
                    stackVeiw8.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
            
            if count == 1 && !BArray.isEmpty{
                
                let emptyButton = UIButton(type: .system)
                emptyButton.isUserInteractionEnabled = false
                BArray.append(emptyButton)
                
                let emptyButton2 = UIButton(type: .system)
                emptyButton2.isUserInteractionEnabled = false
                BArray.append(emptyButton2)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw8.spacing = 10
                stackVeiw8.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }else if count == 2 && !BArray.isEmpty{
                
                let emptyButton1 = UIButton(type: .system)
                emptyButton1.isUserInteractionEnabled = false
                BArray.append(emptyButton1)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw8.spacing = 10
                stackVeiw8.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }
        }// if BlockDictionary is not nil
        else{
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw8.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw8.removeArrangedSubview(view)
                }
            }
            
            var buttonArray = [UIButton]()
            for (myKey,myValue) in DefaultBlockDictionary{
                buttonArray += [colorButton(withColor: myValue, title: "")]
            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 0
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw8.spacing = 0
                    stackVeiw8.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
        } // else part ends
        
    }
    
    func DisplayBlockData9(){
        //generate an array
        
        if BlockDictionary9 != nil && !(BlockDictionary9?.isEmpty)! {
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw9.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw9.removeArrangedSubview(view)
                }
            }
            
            
            var buttonArray = [UIButton]()
            
            for Data in BlockDictionary9!{
                
                buttonArray += [colorButton2(withColor: Data.1.Color, title: Data.0, Subtitle: Data.1.Status,onGoingStatus :Data.1.onGoingStatus,deviceID:Data.1.deviceID)]
                
            }
            
//            for (myKey,myValue) in BlockDictionary9!{
//                buttonArray += [colorButton(withColor: myValue, title: myKey)]
//            }
            
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 10
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw9.spacing = 10
                    stackVeiw9.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
            
            if count == 1 && !BArray.isEmpty{
                
                let emptyButton = UIButton(type: .system)
                emptyButton.isUserInteractionEnabled = false
                BArray.append(emptyButton)
                
                let emptyButton2 = UIButton(type: .system)
                emptyButton2.isUserInteractionEnabled = false
                BArray.append(emptyButton2)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw9.spacing = 10
                stackVeiw9.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }else if count == 2 && !BArray.isEmpty{
                
                let emptyButton1 = UIButton(type: .system)
                emptyButton1.isUserInteractionEnabled = false
                BArray.append(emptyButton1)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw9.spacing = 10
                stackVeiw9.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }
        }// if BlockDictionary is not nil
        else{
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw9.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw9.removeArrangedSubview(view)
                }
            }
            
            
            var buttonArray = [UIButton]()
            for (myKey,myValue) in DefaultBlockDictionary{
                buttonArray += [colorButton(withColor: myValue, title: "")]
            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 0
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw9.spacing = 0
                    stackVeiw9.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
        } // else part ends
    }

    
    
    func DisplayBlockData10(){
        //generate an array
        
        if BlockDictionary10 != nil && !(BlockDictionary10?.isEmpty)! {
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw10.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw10.removeArrangedSubview(view)
                }
            }
            
            
            var buttonArray = [UIButton]()
            
            for Data in BlockDictionary10!{
                
                buttonArray += [colorButton2(withColor: Data.1.Color, title: Data.0, Subtitle: Data.1.Status,onGoingStatus :Data.1.onGoingStatus,deviceID:Data.1.deviceID)]
                
            }
            
//            for (myKey,myValue) in BlockDictionary10!{
//                buttonArray += [colorButton(withColor: myValue, title: myKey)]
//            }
            
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 10
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw10.spacing = 10
                    stackVeiw10.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
            
            if count == 1 && !BArray.isEmpty{
                
                let emptyButton = UIButton(type: .system)
                emptyButton.isUserInteractionEnabled = false
                BArray.append(emptyButton)
                
                let emptyButton2 = UIButton(type: .system)
                emptyButton2.isUserInteractionEnabled = false
                BArray.append(emptyButton2)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw10.spacing = 10
                stackVeiw10.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }else if count == 2 && !BArray.isEmpty{
                
                let emptyButton1 = UIButton(type: .system)
                emptyButton1.isUserInteractionEnabled = false
                BArray.append(emptyButton1)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw10.spacing = 10
                stackVeiw10.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }
        }// if BlockDictionary is not nil
        else{
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw10.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw10.removeArrangedSubview(view)
                }
            }
            
            
            var buttonArray = [UIButton]()
            for (myKey,myValue) in DefaultBlockDictionary{
                buttonArray += [colorButton(withColor: myValue, title: "")]
            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 0
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw10.spacing = 0
                    stackVeiw10.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
        } // else part ends
    }
    
    
    
    func DisplayBlockData11(){
        //generate an array
        
        if BlockDictionary11 != nil && !(BlockDictionary11?.isEmpty)! {
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw11.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw11.removeArrangedSubview(view)
                }
            }
            
            
            var buttonArray = [UIButton]()
            
            for Data in BlockDictionary11!{
                
                buttonArray += [colorButton2(withColor: Data.1.Color, title: Data.0, Subtitle: Data.1.Status,onGoingStatus :Data.1.onGoingStatus,deviceID:Data.1.deviceID)]
                
            }
            
//            for (myKey,myValue) in BlockDictionary11!{
//                buttonArray += [colorButton(withColor: myValue, title: myKey)]
//            }
            
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 10
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw11.spacing = 10
                    stackVeiw11.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
            
            if count == 1 && !BArray.isEmpty{
                
                let emptyButton = UIButton(type: .system)
                emptyButton.isUserInteractionEnabled = false
                BArray.append(emptyButton)
                
                let emptyButton2 = UIButton(type: .system)
                emptyButton2.isUserInteractionEnabled = false
                BArray.append(emptyButton2)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw11.spacing = 10
                stackVeiw11.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }else if count == 2 && !BArray.isEmpty{
                
                let emptyButton1 = UIButton(type: .system)
                emptyButton1.isUserInteractionEnabled = false
                BArray.append(emptyButton1)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw11.spacing = 10
                stackVeiw11.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }
        }// if BlockDictionary is not nil
        else{
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw11.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw11.removeArrangedSubview(view)
                }
            }
            
            
            var buttonArray = [UIButton]()
            for (myKey,myValue) in DefaultBlockDictionary{
                buttonArray += [colorButton(withColor: myValue, title: "")]
            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 0
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw11.spacing = 0
                    stackVeiw11.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
        } // else part ends
    }
    
    
    

    func DisplayBlockData12(){
        //generate an array
        
        if BlockDictionary12 != nil && !(BlockDictionary12?.isEmpty)! {
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw12.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw12.removeArrangedSubview(view)
                }
            }
            
            
            var buttonArray = [UIButton]()
            
            for Data in BlockDictionary12!{
                
                buttonArray += [colorButton2(withColor: Data.1.Color, title: Data.0, Subtitle: Data.1.Status,onGoingStatus :Data.1.onGoingStatus,deviceID:Data.1.deviceID)]
                
            }
            
//            for (myKey,myValue) in BlockDictionary12!{
//                buttonArray += [colorButton(withColor: myValue, title: myKey)]
//            }
            
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 10
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw12.spacing = 10
                    stackVeiw12.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
            
            if count == 1 && !BArray.isEmpty{
                
                let emptyButton = UIButton(type: .system)
                emptyButton.isUserInteractionEnabled = false
                BArray.append(emptyButton)
                
                let emptyButton2 = UIButton(type: .system)
                emptyButton2.isUserInteractionEnabled = false
                BArray.append(emptyButton2)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw12.spacing = 10
                stackVeiw12.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }else if count == 2 && !BArray.isEmpty{
                
                let emptyButton1 = UIButton(type: .system)
                emptyButton1.isUserInteractionEnabled = false
                BArray.append(emptyButton1)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 10
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackVeiw12.spacing = 10
                stackVeiw12.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }
        }// if BlockDictionary is not nil
        else{
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackVeiw12.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackVeiw12.removeArrangedSubview(view)
                }
            }
            
            
            var buttonArray = [UIButton]()
            for (myKey,myValue) in DefaultBlockDictionary{
                buttonArray += [colorButton(withColor: myValue, title: "")]
            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 0
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackVeiw12.spacing = 0
                    stackVeiw12.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
        } // else part ends
        
    }

    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        OutMostScrollView.contentSize.height = OutMostStackView.frame.height+20//250
        //+totalHeight
        // OutMostStackView.frame.height+1000
        //CGSize(width: OutMostStackView.frame.width, height: OutMostStackView.frame.height)
       
            }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let buttonNamesArray = [self.Day1_Btn, self.Day2_Btn, self.Day3_Btn, self.Day4_Btn, self.Day5_Btn]
       
        for button in buttonNamesArray{
            
        let subTitle = UILabel(frame: CGRect(x: 5, y: 5, width: 46, height: 11))
        subTitle.textAlignment = NSTextAlignment.center
        subTitle.font = subTitle.font.withSize(7.0)
        subTitle.text = nil
        subTitle.isHidden = true
        button!.addSubview(subTitle)
        }
        
        
//        DisplayBlockData1()
//        DisplayBlockData2()
//        DisplayBlockData3()
//        DisplayBlockData4()
//        DisplayBlockData5()
        
//        DisplayBlockData6()
//        DisplayBlockData7()
      
//        DisplayBlockData8()
//        DisplayBlockData9()
//        DisplayBlockData10()
//        DisplayBlockData11()
//        DisplayBlockData12()
        
        // Do any additional setup after loading the view.
//        print(self.presentingViewController)
//        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
      
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
//        self.scrollView.contentSize.height = 10000
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Selected Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)

        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)

        
        
        // sets color of separtor line in uitableview 
 //       AuditCodesTable.separatorColor = UIColor.blueColor()
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("AuditMode.json")
        jsonFilePath2 = jsonFilePath
        // creating a .json file in the Documents folder
        if !fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("File created ")
                
            } else {
                print("Couldn't create file for some reason")
            }
        } else {
            print("File already exists")
        }
        //file creation ends
        
    }
    
     func isConnectedToNk() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    override func viewDidAppear(_ animated: Bool) {
        
        self.progressBarDisplayer("Loading. . .", true)
        print(isConnectedToNk(),Reachability.isConnectedToNetwork())
        if !Reachability.isConnectedToNetwork(){
            
            print("Internet connection FAILED")
            do {
                var readString: String
                readString = try NSString(contentsOfFile: jsonFilePath2.path, encoding: String.Encoding.utf8.rawValue) as String
                print("readString=\(readString)")
                self.LoadDataWhenOffline(readString)
            } catch let error as NSError {
                print(error.description)
            }
            
            
        }else{
            self.BlocksDataDictionary = [:]
            BlockDictionary1 = [:]
            BlockDictionary2 = [:]
            BlockDictionary3 = [:]
            BlockDictionary4 = [:]
            BlockDictionary5 = [:]
            BlockDictionary6 = [:]
            BlockDictionary7 = [:]
            BlockDictionary8 = [:]
            BlockDictionary9 = [:]
            BlockDictionary10 = [:]
            BlockDictionary11 = [:]
            BlockDictionary12 = [:]
            
            let url = getUrl()
            let today = Date()
            
      //
            let urlData = self.performLoadDataRequestWithURL(url, Date: today.CustomDateformat)
        }
        
    }
    
    
 
    func LoadDataWhenOffline(_ savedData:String?) {
        
        
        print("savedDataString=\(savedData)")
        
        if savedData != nil && savedData != "" {
    
                self.dictionary = self.parseJSON(savedData!)
                
                var planned, completed, pending:Int!
                planned = (self.dictionary["Today"]! as! [String: AnyObject])["Planned"] as! Int
                completed = (self.dictionary["Today"]! as! [String: AnyObject])["Completed"] as! Int
                pending = (self.dictionary["Today"]! as! [String: AnyObject])["Pending"] as! Int
//                self.currentDateLabel.text = self.dictionary["Today"]!["Day"] as! String
            self.currentDateLabel.setTitle((self.dictionary["Today"]! as! [String: AnyObject])["Day"] as! String, for: UIControlState())
                self.plannedAudits.text = String(planned)
                self.completedAudits.text = String(completed)
                self.pendingReports.text = String(pending)
            
            
            let PreviousFiveDayObjects = ["Day-1","Day-2","Day-3","Day-4","Day-5"]
            let AdvancedFiveDayObjects = ["Day+1","Day+2","Day+3","Day+4","Day+5"]
            
            //   [date: (day,statsArray)]
            var pObjDays:[String:(pDay:String,Stats:[(Audits:String,Stage:String)])]? = [:]
            var AdvanceObjDays:[String:(pDay:String,Stats:[(Audits:String,Stage:String)])]? = [:]
            
            
            for p in PreviousFiveDayObjects {
                
                if (self.dictionary["\(p)"]) != nil{
                    
                    let pday = (self.dictionary["\(p)"]! as! [String: AnyObject])["Day"] as? String
                    let pdate = (self.dictionary["\(p)"]! as! [String: AnyObject])["Date"] as? String
                    let myStats = (self.dictionary["\(p)"]! as? [String: AnyObject])?["Stats"] as? [AnyObject]//as? NSArray
                    
                    var StatsArray:[(Audits:String,Stage:String)] = []
                    
                    if myStats != nil{
                        for a in myStats!{
                            print("Audits =",a["Audits"]!!)
                            print("Stage =",a["Stage"]!!)
                            StatsArray.append((Audits:a["Audits"]!! as! String,Stage:a["Stage"]!! as! String))
                        }
                    }
                    print("StatsArray =",StatsArray)
                    print("stats =",myStats)
                    
                    if pday != nil{
                        
                        print("previousDay =",pday)
                        pObjDays?.updateValue((pday!,StatsArray), forKey: pdate!)
                        
                        print("PObjDays =",pObjDays)
                    }
                }
            }
            
            for ad in AdvancedFiveDayObjects {
                
                if (self.dictionary["\(ad)"]) != nil{
                    
                    let aday = (self.dictionary["\(ad)"]! as! [String: AnyObject])["Day"] as? String
                    let adate = (self.dictionary["\(ad)"]! as! [String: AnyObject])["Date"] as? String
                    let myStats = (self.dictionary["\(ad)"]! as! [String: AnyObject])["Stats"] as? [AnyObject]//as? NSArray
                    
                    var StatsArray:[(Audits:String,Stage:String)] = []
                    
                    if myStats != nil{
                        for a in myStats!{
                            print("Audits =",a["Audits"]!!)
                            print("Stage =",a["Stage"]!!)
                            StatsArray.append((Audits:a["Audits"]!! as! String,Stage:a["Stage"]!! as! String))
                        }
                    }
                    print("StatsArray =",StatsArray)
                    print("stats =",myStats)
                    
                    if aday != nil{
                        print("AdvanceDay =",aday)
                        AdvanceObjDays?.updateValue((aday!,StatsArray), forKey: adate!)
                        
                        print("AdvanceObjDays =",AdvanceObjDays)
                    }
                }
            }

            
            let buttonNamesArray = [self.Day1_Btn, self.Day2_Btn, self.Day3_Btn, self.Day4_Btn, self.Day5_Btn]
            let buttonLabelsArray = [self.Audits_and_StageDay1,self.Audits_and_StageDay2,self.Audits_and_StageDay3,self.Audits_and_StageDay4,self.Audits_and_StageDay5]
            
            
            // for previous days
            
            if pObjDays!.keys.count > 0{
                // sorting pObjDays with respect to date
                
                let df = DateFormatter()
                df.dateFormat = "yyyy-MM-dd"
                
                self.sortedPreviousDays_withDate = []
                
                self.sortedPreviousDays_withDate = pObjDays!.sorted{ (a,b) -> Bool in
                    
                    let comparisonResult = df.date(from: a.0)!.compare(df.date(from: b.0)!)
                    
                    if comparisonResult == ComparisonResult.orderedDescending{
                        
                        return true
                        
                    }else{
                        
                        return false
                    }
                }
                
                print("sortedPreviousDays_withDate =", self.sortedPreviousDays_withDate!)
                
            }
            
            // for the forword days
            
            if AdvanceObjDays?.count > 0{
                
                // sorting AdvanceObjDays with respect to date
                
                let df = DateFormatter()
                df.dateFormat = "yyyy-MM-dd"
                
                
                self.sortedForwordDays_withDate = []
                
                self.sortedForwordDays_withDate = AdvanceObjDays!.sorted{ (a,b) -> Bool in
                    
                    let comparisonResult = df.date(from: a.0)!.compare(df.date(from: b.0)!)
                    
                    if comparisonResult == ComparisonResult.orderedDescending{
                        
                        return true
                        
                    }else{
                        
                        return false
                    }
                }
                
                print("sortedForwordDays_withDate =", self.sortedForwordDays_withDate!)
                
            }
            
            
            
            // if it is first time loading then
            
            if self.firstTimeLoading == true{
                
                
                print("its first time loading.")
                // if AdvanceObjDays.count > zero Then show More days button (>)
                
                if AdvanceObjDays?.count > 0{
                    self.PreviousForwordDay_Btn.setTitle(" >", for: UIControlState())
                }
                
                
                var buttonCount = 0
                for pd in self.sortedPreviousDays_withDate!{
                    // pd.1.pDay  is Day
                    print("pd =",pd)
                    buttonNamesArray[buttonCount]?.setTitle(pd.1.pDay, for: UIControlState())
                    
                    var subTitle = buttonNamesArray[buttonCount]?.subviews.first as! UILabel
                    subTitle.text = pd.0
                    
                    
                    // show Audits and Stages along side of day Buttons using
                    //Now set AuditStage labels for Audits_and_StageDays
                    
                    let stats = pd.1.Stats
                    
                    for (stat,label) in zip(stats, buttonLabelsArray[buttonCount]!) {
                        let auditsCount = stat.Audits
                        let stage = stat.Stage
                        
                        label.text = "\(auditsCount) \(stage)"
                    }
                    
                    // setting of label ends
                    
                    
                    buttonCount+=1
                }
                
                
                self.firstTimeLoading = false
            }
            

            
            
            let All_Audits = self.dictionary["Audits"]! as! [[String : AnyObject]]
            print("Total Audits = \((self.dictionary["Audits"]! as AnyObject).count)")
            
                if All_Audits.count < 1 {
                    
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                    self.showDataInBlocks(self.BlocksDataDictionary)
                    
                                            DispatchQueue.main.async(execute: {
                    
                                                self.viewDidLayoutSubviews()
                                            })

//                    let alertView = UIAlertController(title: "No Data Found" , message: "make sure your Internet connection is Ok.", preferredStyle: .alert)
//                    let ok_action: UIAlertAction = UIAlertAction(title: "Retry", style: .default, handler: { (UIAlertAction) -> Void in
//                        
//                        self.progressBarDisplayer("Loading. . .", true)
//                        self.performLoadDataRequestWithURL(self.getUrl(),Date: Date().CustomDateformat)
//                    })
//                    let Cancel_action: UIAlertAction = UIAlertAction(title: "Dismiss", style: .default, handler: { (UIAlertAction) -> Void in
//                        
//                        self.activityIndicator.stopAnimating()
//                        self.messageFrame.removeFromSuperview()
//                        
//                        // Total audits count is zero then show empty blocks.
//                        
//                        self.showDataInBlocks(self.BlocksDataDictionary)
//                       
//                        DispatchQueue.main.async(execute: {
//                           
//                            self.viewDidLayoutSubviews()
//                        })
//                        
//
//                    })
//                    
//                    alertView.addAction(Cancel_action)
//                    alertView.addAction(ok_action)
//                    self.present(alertView, animated: true, completion: nil)
                    
                }else{
                    // Reset the bellow data structures
                    
                    self.AuditCodes.removeAll()
                    self.AuditCompleted.removeAll()
                    self.AuditColors.removeAll()
                    
                    self.BlocksDataDictionary = [:]
                    //                    var blockValue:[String:(completedState:String, color:String)] = [:]
                    
                    for Audit in All_Audits{
                        
                        let AuditCodeName = Audit["AuditCode"]! as! String
                        let deviceId = Audit["DeviceId"]! as! String

                        let completedAudit = Audit["Completed"]! as! String
                        let AuditColor = Audit["Color"]! as! String
                        let block = Audit["Block"]! as! Int
                        let OnGoing = Audit["OnGoing"]! as! String
                        let AuditResult = Audit["AuditResult"]! as! String

                        print("AuditCodeName=\(AuditCodeName)")
                         print("OnGoing=\(OnGoing)")
                         print("AuditResult=\(AuditResult)")
                        print("completedAudit=\(completedAudit)")
                        print("AuditorColor=\(AuditColor)")
                        
                        self.AuditCodes.append(AuditCodeName)
                        self.AuditCompleted.append(completedAudit)
                        self.AuditColors.append(AuditColor)
                        //                        self.AuditBlocks.append(Audit["Block"]! as! Int)
                        
                        var blockValue:[String:(completedState:String, color:String,onGoingStatus :String,deviceID :String)] = [:]
                        
                        if self.BlocksDataDictionary?.keys.count > 0 {
                            
                            for blockNum in (self.BlocksDataDictionary?.keys)! {
                                
                                if blockNum == block{
                                    let oldBlockValue = self.BlocksDataDictionary![blockNum]
                                    
                                    blockValue = oldBlockValue!
                                    blockValue.updateValue((completedState: completedAudit, color: AuditColor,onGoingStatus:OnGoing,deviceID:deviceId), forKey: AuditCodeName)
                                    print("block is equal to blockNum =",blockNum,block)
                                    break
                                }else{
                                    print("block not equal")
                                    blockValue.updateValue((completedState: completedAudit, color: AuditColor,onGoingStatus:OnGoing,deviceID:deviceId), forKey: AuditCodeName)
                                }
                            }
                            self.BlocksDataDictionary?.updateValue(blockValue, forKey: block)
                            
                        }else{
                            
                            blockValue.updateValue((completedState: completedAudit, color: AuditColor,onGoingStatus:OnGoing,deviceID:deviceId), forKey: AuditCodeName)
                            self.BlocksDataDictionary?.updateValue(blockValue, forKey: block)
                        }
                        
//                        
//                        // Save AuditCode and AuditDate in userDefaults so that it can be used when auditing offline or Online
//                        if completedAudit == "N"{
//                            defaults.setObject(dateSentInRequest, forKey: AuditCodeName)
//                        }else{
//                            defaults.removeObjectForKey(AuditCodeName)
//                        }
                    }
                    
                    if("\(self.dictionary["Status"]!)" == "OK" && self.AuditCodes.count > 0){
                        print("AuditCodes-count = \(self.AuditCodes.count)")
                       
                        // Now show data in Blocks
                        print("BlocksDataDictionary =",self.BlocksDataDictionary)
                        
                        self.showDataInBlocks(self.BlocksDataDictionary)
                        
                        self.activityIndicator.stopAnimating()
                        self.messageFrame.removeFromSuperview()
                        
                    }
            } //else part of count<1
            
            }//if savedData != nil
            else{
                print("return data is nil")
//                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            self.activityIndicator.stopAnimating()
            self.messageFrame.removeFromSuperview()

//            let alertView = UIAlertController(title: "No Data Found" , message: "Make sure you are connected to the Internet.", preferredStyle: .alert)
//                let ok_action: UIAlertAction = UIAlertAction(title: "Retry", style: .default, handler: { (UIAlertAction) -> Void in
//                    
//                    self.progressBarDisplayer("Loading. . .", true)
//                    self.performLoadDataRequestWithURL(self.getUrl(),Date: Date().CustomDateformat)
//                })
//            let Cancel_action: UIAlertAction = UIAlertAction(title: "No", style: .default, handler: { (UIAlertAction) -> Void in
//                
//                self.activityIndicator.stopAnimating()
//                self.messageFrame.removeFromSuperview()
//            })
//
//           
//                alertView.addAction(Cancel_action)
//                alertView.addAction(ok_action)
//                self.present(alertView, animated: true, completion: nil)
            }
        
        
    }
    
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
//            defaults.removeObjectForKey("userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            print("self.presentingViewController = \(self.presentingViewController)")
            
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
//        case 4:
//            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
//                //                self.dismissViewControllerAnimated(true, completion: nil)
//                presentViewController(resultController, animated: true, completion: nil)
//            }
//            
//            let selectedImage:UIImage = UIImage(named: "Selected QAReport")!
//            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
//            
//            break
        case 5:
            if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                    present(resultController, animated: true, completion: nil)
                }
            }else{
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }

//            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
//            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_selected_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break

        default:
            break
        }
    }
    
 
    
    func progressBarDisplayer(_ msg:String, _ indicator:Bool ) {
        print(msg)
        self.strLabel = UILabel(frame: CGRect(x: view.frame.midX - 40, y: view.frame.midY - 50, width: 200, height: 50))  // x: 50, y: 0, width: 200, height: 50
        self.strLabel.text = msg
        self.strLabel.textColor = UIColor.white
        self.messageFrame = UIView(frame: CGRect(x: 0, y: 69 , width: view.frame.width, height: view.frame.height - 118))  //x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50
        //        messageFrame.layer.cornerRadius = 15
        self.messageFrame.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        if indicator {
            self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
            self.activityIndicator.frame = CGRect(x: view.frame.midX - 80, y: view.frame.midY - 50, width: 50, height: 50)  // x: 0, y: 0, width: 50, height: 50
            self.activityIndicator.startAnimating()
            self.messageFrame.addSubview(activityIndicator)
        }
        self.messageFrame.addSubview(strLabel)
        self.view.addSubview(self.messageFrame)
    }

    
    
    
    func getUrl() -> URL {
        let toEscape = "http://app.3-tree.com/quonda/auditor-schedule.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
    var bodyData:String!
    func performLoadDataRequestWithURL(_ url: URL, Date: String) -> String? {
        
//        let today = NSDate()
        self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&Date=\(Date)" //2016-08-07"
        
        var dateSentInRequest = Date  // today.CustomDateformat
        
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = self.bodyData!.data(using: String.Encoding.utf8)
//        let session = NSURLSession.sharedSession()
//        let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
//            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
//                print("error")
//                return
//            }
//            
//            let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
//            print(dataString)
//        }
//        task.resume()
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main){
            response, data, error in
            print("\(response)")
            if (error != nil) {
                print(error)
//                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
//                self.showLoadingMessage("Loading. . .", dismiss: true)
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
                
                let alertView = UIAlertController(title: "Data Cannot Fetch" , message: "\(error!.localizedDescription)", preferredStyle: .alert)
                let Reload_action: UIAlertAction = UIAlertAction(title: "Retry", style: .default, handler: { (UIAlertAction) -> Void in
                    
//                    self.showLoadingMessage("Loading. . .", dismiss: false)
                    self.progressBarDisplayer("Loading. . .", true)
                    self.performLoadDataRequestWithURL(self.getUrl(), Date: Date)
                })
                let no_action: UIAlertAction = UIAlertAction(title: "NO", style: .default, handler: { (UIAlertAction) -> Void in
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                })
               
                alertView.addAction(Reload_action)
                alertView.addAction(no_action)
                self.present(alertView, animated: true, completion: nil)
                }
            
            if data != nil {
                
                //Reset all blocksdata
                self.ResetAllBlockData()
                
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
              print("json of selected Date =",json)
                self.dictionary = self.parseJSON(json)

                print("\(self.dictionary)")
                
                var planned, completed, pending:Int!
                planned = (self.dictionary["Today"]! as! [String: AnyObject])["Planned"] as! Int
                completed = (self.dictionary["Today"]! as! [String: AnyObject])["Completed"] as! Int
                pending = (self.dictionary["Today"]! as! [String: AnyObject])["Pending"] as! Int
               
                print("planned =",planned)
                print("completed =",completed)
                print("self.dictionary[Today]![Day] = ",(self.dictionary["Today"]! as! [String: AnyObject])["Day"])
                
//                self.currentDateLabel.text = self.dictionary["Today"]!["Day"] as! String
                self.currentDateLabel.setTitle((self.dictionary["Today"]! as! [String: AnyObject])["Day"] as! String, for: UIControlState())
                self.plannedAudits.text = String(planned)
                self.completedAudits.text = String(completed)
                self.pendingReports.text = String(pending)
                
                let PreviousFiveDayObjects = ["Day-1","Day-2","Day-3","Day-4","Day-5"]
                let AdvancedFiveDayObjects = ["Day+1","Day+2","Day+3","Day+4","Day+5"]
                
                //   [date: (day,statsArray)]
                var pObjDays:[String:(pDay:String,Stats:[(Audits:String,Stage:String)])]? = [:]
//                var AdvanceObjDays:[String:String]? = [:]
                
                var AdvanceObjDays:[String:(pDay:String,Stats:[(Audits:String,Stage:String)])]? = [:]

                for p in PreviousFiveDayObjects {
                    
                    if (self.dictionary["\(p)"]) != nil{
                        
                        let pday = (self.dictionary["\(p)"]! as! [String: AnyObject])["Day"] as? String
                        let pdate = (self.dictionary["\(p)"]! as! [String: AnyObject])["Date"] as? String
                        let myStats = (self.dictionary["\(p)"]! as! [String: AnyObject])["Stats"] as? [AnyObject]
                        
                        var StatsArray:[(Audits:String,Stage:String)] = []
                        
                        if myStats != nil{
                            for a in myStats!{
                                print("Audits =",a["Audits"]!!)
                                print("Stage =",a["Stage"]!!)
                                StatsArray.append((Audits:a["Audits"]!! as! String,Stage:a["Stage"]!! as! String))
                            }
                        }
                        print("StatsArray =",StatsArray)
                        print("stats =",myStats)
                        
                        if pday != nil{
                            print("previousDay =",pday)
//                            PreviousObjDays?.updateValue(pday!, forKey: pdate!)
//                            print("PreviousObjDays =",PreviousObjDays)
                            
                            pObjDays?.updateValue((pday!,StatsArray), forKey: pdate!)
                            
                            print("PObjDays =",pObjDays)
                        }
                    }
                }
                
                for ad in AdvancedFiveDayObjects {
                  
                    if (self.dictionary["\(ad)"]) != nil{
                        
                        let aday = (self.dictionary["\(ad)"]! as! [String: AnyObject])["Day"] as? String
                        let adate = (self.dictionary["\(ad)"]! as! [String: AnyObject])["Date"] as? String
                        let myStats = (self.dictionary["\(ad)"]! as! [String: AnyObject])["Stats"] as? [AnyObject]
                        
                        var StatsArray:[(Audits:String,Stage:String)] = []
                        
                        if myStats != nil{
                            for a in myStats!{
                                print("Audits =",a["Audits"]!!)
                                print("Stage =",a["Stage"]!!)
                                StatsArray.append((Audits:a["Audits"]!! as! String,Stage:a["Stage"]!! as! String))
                            }
                        }
                        print("StatsArray =",StatsArray)
                        print("stats =",myStats)
                        
                        if aday != nil{
                            print("AdvanceDay =",aday)
                            AdvanceObjDays?.updateValue((aday!,StatsArray), forKey: adate!)
                            
                            print("AdvanceObjDays =",AdvanceObjDays)
                        }
                    }
                }
                
                
                let buttonNamesArray = [self.Day1_Btn, self.Day2_Btn, self.Day3_Btn, self.Day4_Btn, self.Day5_Btn]
                let buttonLabelsArray = [self.Audits_and_StageDay1,self.Audits_and_StageDay2,self.Audits_and_StageDay3,self.Audits_and_StageDay4,self.Audits_and_StageDay5]
                
                
                
                // for previous days
                
                if pObjDays!.keys.count > 0{
                    
                    // sorting pObjDays with respect to date
                    
                    let df = DateFormatter()
                    df.dateFormat = "yyyy-MM-dd"
                    
                    self.sortedPreviousDays_withDate = []
                    
                    self.sortedPreviousDays_withDate = pObjDays!.sorted{ (a,b) -> Bool in
                        
                        let comparisonResult = df.date(from: a.0)!.compare(df.date(from: b.0)!)
                        
                        if comparisonResult == ComparisonResult.orderedDescending{
                            
                            return true
                            
                        }else{
                            
                            return false
                        }
                    }
                    
                    print("sortedPreviousDays_withDate =", self.sortedPreviousDays_withDate!)
                    
                }
                
                
                
            // for the forword days
                
                if AdvanceObjDays?.count > 0{
                    
                    // sorting AdvanceObjDays with respect to date
                    
                    let df = DateFormatter()
                    df.dateFormat = "yyyy-MM-dd"
                    
                    
                    self.sortedForwordDays_withDate = []
                    
                    self.sortedForwordDays_withDate = AdvanceObjDays!.sorted{ (a,b) -> Bool in
                        
                        let comparisonResult = df.date(from: a.0)!.compare(df.date(from: b.0)!)
                        
                        if comparisonResult == ComparisonResult.orderedDescending{
                            
                            return true
                            
                        }else{
                            
                            return false
                        }
                    }
                    
                    print("sortedForwordDays_withDate =", self.sortedForwordDays_withDate!)
                    
                }
                
                
                
                // if it is first time loading then
                
                if self.firstTimeLoading == true{
                    
                    print("its first time loading.")
                    // if AdvanceObjDays.count > zero Then show More days button (>)
                    
                    if AdvanceObjDays?.count > 0{
                        self.PreviousForwordDay_Btn.setTitle(" >", for: UIControlState())
                    }
                    
                    
                    var buttonCount = 0
                    for pd in self.sortedPreviousDays_withDate!{
                        // pd.1.pDay  is Day
                        print("pd =",pd)
                        buttonNamesArray[buttonCount]?.setTitle(pd.1.pDay, for: UIControlState())
                        
                        var subTitle = buttonNamesArray[buttonCount]?.subviews.first as! UILabel
                        subTitle.text = pd.0
                        print("pd.0 button date =",pd.0)
                        print("buttonDate =",(buttonNamesArray[buttonCount]?.subviews.first as! UILabel).text)
                        
                        // show Audits and Stages along side of day Buttons using
                        
                        //Fist Reset all lables
                        
//                        for labelSet in buttonLabelsArray{
//                            
//                            for label in labelSet{
//                                label.text = ""
//                            }
//                        }
                        //Now set AuditStage labels for Audits_and_StageDays
                        
                        let stats = pd.1.Stats
                        
                        for (stat,label) in zip(stats, buttonLabelsArray[buttonCount]!) {
                            let auditsCount = stat.Audits
                            let stage = stat.Stage
                            
                            label.text = "\(auditsCount) \(stage)"
                        }
                        
                        // setting of label ends
                        
                        
                        buttonCount+=1
                    }
                    
                    
                    self.firstTimeLoading = false
                }
                
                
                
                let All_Audits = self.dictionary["Audits"]! as! [[String : AnyObject]]
            
                print("Total Audits = \((self.dictionary["Audits"]! as AnyObject).count)")
                
                
                if (self.dictionary["Audits"] as AnyObject).count < 1 {
//                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                  
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                    DispatchQueue.main.async(execute: {
                                    self.viewDidLayoutSubviews()
                            })
                    
//                    let alertView = UIAlertController(title: nil , message: "No Data Found", preferredStyle: .alert)
//                    let ok_action: UIAlertAction = UIAlertAction(title: "Retry", style: .default, handler: { (UIAlertAction) -> Void in
//                        
////                        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
//                        self.progressBarDisplayer("Loading. . .", true)
//                        self.performLoadDataRequestWithURL(self.getUrl(), Date: Date)
//                    })
//                    let Cancel_action: UIAlertAction = UIAlertAction(title: "Dismiss", style: .default, handler: { (UIAlertAction) -> Void in
//                        
//                        self.activityIndicator.stopAnimating()
//                        self.messageFrame.removeFromSuperview()
//
//                        
//                        // Total audits count is zero then show empty blocks.
//                        
//                        self.showDataInBlocks(self.BlocksDataDictionary)
//                        DispatchQueue.main.async(execute: {
//                            self.viewDidLayoutSubviews()
//                        })
//                        
//                    })
//
//                    alertView.addAction(Cancel_action)
//                    alertView.addAction(ok_action)
//                    self.present(alertView, animated: true, completion: nil)
       
                }else{
                    // save data to Documents Dir if date is Today
                    
                    //if Date == NSDate().CustomDateformat{
                        
                        if self.fileManager.fileExists(atPath: self.jsonFilePath2.absoluteString, isDirectory: &self.isDirectory) {
                            do{
                                print("old json data in AuditorMode =",json)
                                try json.write(toFile: self.jsonFilePath2.path, atomically: true, encoding: String.Encoding.utf8)
                            } catch let error as NSError {
                                print("JSON data couldn't written to file // File not exist")
                                print(error.description)
                            }
                        }
                    //}
                    
                    // Reset the bellow data structures
                    
                    self.AuditCodes.removeAll()
                    self.AuditCompleted.removeAll()
                    self.AuditColors.removeAll()
                    
                    self.BlocksDataDictionary = [:]

                    for Audit in All_Audits{
                        print("Audit for checking =",Audit)
                        let AuditCodeName = Audit["AuditCode"]! as! String
                        let deviceId = Audit["DeviceId"]! as! String

                        let completedAudit = Audit["Completed"]! as! String
                        let AuditColor = Audit["Color"]! as! String
                        let block = Audit["Block"]! as! Int
                        let OnGoing = Audit["OnGoing"]! as! String
                        let AuditResult = Audit["AuditResult"]! as! String
                        let AuditStage = Audit["AuditStage"]! as! String
                        
                        print("AuditCodeName=\(AuditCodeName)")
                        print("OnGoing=\(OnGoing)")
                        print("AuditResult=\(AuditResult)")
                        print("completedAudit=\(completedAudit)")
                        print("AuditorColor=\(AuditColor)")
                        print("AuditStage=\(AuditStage)")

//                        print("\(AuditCodeName)")
//                        print(completedAudit)
//                        print("AuditorColor=\(AuditColor)")
                        
                        self.AuditCodes.append(AuditCodeName)
                        self.AuditCompleted.append(completedAudit)
                        self.AuditColors.append(AuditColor)
//                        self.AuditBlocks.append(Audit["Block"]! as! Int)
                      
                        var blockValue:[String:(completedState:String, color:String,onGoingStatus :String,deviceID:String)] = [:]
                       
                        if self.BlocksDataDictionary?.keys.count > 0 {
                           
                            for blockNum in (self.BlocksDataDictionary?.keys)! {
                              
                                if blockNum == block{
                                    let oldBlockValue = self.BlocksDataDictionary![blockNum]
                                    
                                    blockValue = oldBlockValue!
                                    blockValue.updateValue((completedState: completedAudit, color: AuditColor,onGoingStatus :OnGoing,deviceID:deviceId), forKey: AuditCodeName)
                                    print("block is equal to blockNum =",blockNum,block)
                                    break
                                }else{
                                    print("block not equal")
                                    blockValue.updateValue((completedState: completedAudit, color: AuditColor,onGoingStatus :OnGoing,deviceID:deviceId), forKey: AuditCodeName)
                                }
                            }
                            self.BlocksDataDictionary?.updateValue(blockValue, forKey: block)
                       
                        }else{
                            
                            blockValue.updateValue((completedState: completedAudit, color: AuditColor,onGoingStatus :OnGoing,deviceID:deviceId), forKey: AuditCodeName)
                            self.BlocksDataDictionary?.updateValue(blockValue, forKey: block)
                        }
                        
                        
                        // Save AuditCode and AuditDate in userDefaults so that it can be used when auditing offline or Online
                        if completedAudit == "N"{
                            defaults.set(dateSentInRequest, forKey: AuditCodeName)
                        }else{
                            defaults.removeObject(forKey: AuditCodeName)
                        }
                    }

                    if("\(self.dictionary["Status"]!)" == "OK" && self.AuditCodes.count > 0){
                        print("AuditCodes-count = \(self.AuditCodes.count)")
//                        self.AuditCodesTable.dataSource = self
//                        self.AuditCodesTable.delegate = self
//                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                        
                        // Now data in Blocks
                       print("BlocksDataDictionary =",self.BlocksDataDictionary)
                       
                        self.showDataInBlocks(self.BlocksDataDictionary)
                        
                            self.activityIndicator.stopAnimating()
                            self.messageFrame.removeFromSuperview()
//
//                            self.AuditCodesTable.reloadData()
//                        })
                        
                    }
                } //else part of count<1
            }//if data != nil
            else{
                print("return data is nil")
//                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
                
                let alertView = UIAlertController(title: "No Data Found" , message: "Press OK to Reload Data", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    self.progressBarDisplayer("Loading. . .", true)
                    self.performLoadDataRequestWithURL(self.getUrl(), Date: Date)
                })
                let Cancel_action: UIAlertAction = UIAlertAction(title: "No", style: .default, handler: {(UIAlertAction) -> Void in
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                })
                
                alertView.addAction(Cancel_action)
                alertView.addAction(ok_action)
                self.present(alertView, animated: true, completion: nil)
            }
        }
        return json
    }
    
    
    
     //  [BlockID:[AuditCode:(completedState, color)]]
    func showDataInBlocks(_ BlocksDictionary:[Int:[String:(completedState:String, color:String,onGoingStatus :String,deviceID:String)]]?) -> Void {
        
//        AuditCodes
//        AuditCompleted
//        AuditColors
        
        // if there is no BlockData found from server then show empty Blocks.
        
        let AllBlockKeys = [0,1,2,3,4,5,6,7,8,9,10,11]

        for key in AllBlockKeys {
            
            if !((BlocksDictionary?.keys.contains(key))!){

                switch key {
                case 0:
                    DisplayBlockData1()
                case 1:
                    DisplayBlockData2()
                case 2:
                    DisplayBlockData3()
                case 3:
                    DisplayBlockData4()
                case 4:
                    DisplayBlockData5()
                case 5:
                    DisplayBlockData6()
                case 6:
                    DisplayBlockData7()
                case 7:
                    DisplayBlockData8()
                case 8:
                    DisplayBlockData9()
                case 9:
                    DisplayBlockData10()
                case 10:
                    DisplayBlockData11()
                case 11:
                    DisplayBlockData12()
                default:
                    Void()
                }
            }
        }
        
        for blockID in (BlocksDictionary?.keys)! {
            
            switch blockID {
            case 0:
                print("You are going to show BlockDictionary1 12:00 AM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    print(Audit)
                    let auditCode = Audit.0
                    let completedStatus = Audit.1.completedState
                    let OnGoing = Audit.1.onGoingStatus

                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    let deviceId = Audit.1.deviceID
                    BlockDictionary1?.updateValue((Status:completedStatus,Color:auditColor,onGoingStatus :OnGoing,deviceID:deviceId), forKey: auditCode)
                    
                }
                DisplayBlockData1()
                
                

            case 1:
                print("You are going to show BlockDictionary2 2:00 AM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let completedStatus = Audit.1.completedState
                    let OnGoing = Audit.1.onGoingStatus

                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    let deviceId = Audit.1.deviceID
                    BlockDictionary2?.updateValue((Status:completedStatus,Color:auditColor,onGoingStatus :OnGoing,deviceID:deviceId), forKey: auditCode)
                }
                DisplayBlockData2()
                
                

            case 2:
                print("You are going to show BlockDictionary3 4:00 AM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let completedStatus = Audit.1.completedState
                    let OnGoing = Audit.1.onGoingStatus

                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    let deviceId = Audit.1.deviceID
                    BlockDictionary3?.updateValue((Status:completedStatus,Color:auditColor,onGoingStatus :OnGoing,deviceID:deviceId), forKey: auditCode)
                }
                DisplayBlockData3()
                
                
            case 3:
                print("You are going to show BlockDictionary4 6:00 AM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let completedStatus = Audit.1.completedState
                    let OnGoing = Audit.1.onGoingStatus

                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    let deviceId = Audit.1.deviceID
                    BlockDictionary4?.updateValue((Status:completedStatus,Color:auditColor,onGoingStatus :OnGoing,deviceID:deviceId), forKey: auditCode)
                }
                DisplayBlockData4()
                
                

            
            case 4:
                print("You are going to show BlockDictionary5 8:00 AM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let completedStatus = Audit.1.completedState
                    let OnGoing = Audit.1.onGoingStatus

                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    let deviceId = Audit.1.deviceID
                    BlockDictionary5?.updateValue((Status:completedStatus,Color:auditColor,onGoingStatus :OnGoing,deviceID:deviceId), forKey: auditCode)
                }
                DisplayBlockData5()
                
                

            case 5:
                print("You are going to show BlockDictionary6 10:00 AM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let completedStatus = Audit.1.completedState
                    let OnGoing = Audit.1.onGoingStatus

                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    let deviceId = Audit.1.deviceID
                    BlockDictionary6?.updateValue((Status:completedStatus,Color:auditColor,onGoingStatus :OnGoing,deviceID:deviceId), forKey: auditCode)
                }
                DisplayBlockData6()
                

                
            case 6:
                print("You are going to show BlockDictionary7 12:00 PM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
               
                for Audit in blockDataAgainstCurrentKey! {
                  
                    let auditCode = Audit.0
                    let completedStatus = Audit.1.completedState
                    let OnGoing = Audit.1.onGoingStatus

                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    let deviceId = Audit.1.deviceID
                    BlockDictionary7?.updateValue((Status:completedStatus,Color:auditColor,onGoingStatus :OnGoing,deviceID:deviceId), forKey: auditCode)
                    //updateValue(auditColor, forKey: auditCode)
                }
                DisplayBlockData7()
                
            case 7:
                print("You are going to show BlockDictionary8 2:00 PM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let completedStatus = Audit.1.completedState
                    let OnGoing = Audit.1.onGoingStatus

                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    let deviceId = Audit.1.deviceID
                    BlockDictionary8?.updateValue((Status:completedStatus,Color:auditColor,onGoingStatus :OnGoing,deviceID:deviceId), forKey: auditCode)
                }
                DisplayBlockData8()
                
                

            case 8:
                print("You are going to show BlockDictionary9 4:00 PM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let completedStatus = Audit.1.completedState
                    let OnGoing = Audit.1.onGoingStatus

                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    let deviceId = Audit.1.deviceID
                    BlockDictionary9?.updateValue((Status:completedStatus,Color:auditColor,onGoingStatus :OnGoing,deviceID:deviceId), forKey: auditCode)
                }
                DisplayBlockData9()
                
                

            case 9:
                print("You are going to show BlockDictionary10 6:00 PM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let completedStatus = Audit.1.completedState
                    let OnGoing = Audit.1.onGoingStatus

                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    let deviceId = Audit.1.deviceID
                    BlockDictionary10?.updateValue((Status:completedStatus,Color:auditColor,onGoingStatus :OnGoing,deviceID:deviceId), forKey: auditCode)
                }
                DisplayBlockData10()
                
                

            case 10:
                print("You are going to show BlockDictionary11 8:00 PM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let completedStatus = Audit.1.completedState
                    let OnGoing = Audit.1.onGoingStatus

                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    let deviceId = Audit.1.deviceID
                    BlockDictionary11?.updateValue((Status:completedStatus,Color:auditColor,onGoingStatus :OnGoing,deviceID:deviceId), forKey: auditCode)
                }
                DisplayBlockData11()
                
                

            case 11:
                print("You are going to show BlockDictionary12 10:00 PM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let completedStatus = Audit.1.completedState
                    let OnGoing = Audit.1.onGoingStatus

                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    let deviceId = Audit.1.deviceID
                    BlockDictionary12?.updateValue((Status:completedStatus,Color:auditColor,onGoingStatus :OnGoing,deviceID:deviceId), forKey: auditCode)
                }
                DisplayBlockData12()
                
                

            default:
                print("default case while showing blocks")
            }
            
        }
        
        
        // Now update stackView scroll Size
        DispatchQueue.main.async(execute: {
            self.viewDidLayoutSubviews()
        })
        
        
        
//        for BlockNum in Blocks! {
//
//            switch BlockNum {
//            
//            case 5:
//                print("You are going to show BlockDictionary6 10:00 AM")
//            
//            default:
//                print("default case while showing blocks")
//            }
//        }
    }
    
    func ResetAllBlockData() -> Void {
        // Reset all blocks data
        
        self.BlocksDataDictionary = [:]
        BlockDictionary1 = [:]
        BlockDictionary2 = [:]
        BlockDictionary3 = [:]
        BlockDictionary4 = [:]
        BlockDictionary5 = [:]
        BlockDictionary6 = [:]
        BlockDictionary7 = [:]
        BlockDictionary8 = [:]
        BlockDictionary9 = [:]
        BlockDictionary10 = [:]
        BlockDictionary11 = [:]
        BlockDictionary12 = [:]
        // if there is no BlockData found from server then show empty Blocks.
        
        let AllBlockKeys = [0,1,2,3,4,5,6,7,8,9,10,11]
        
        for key in AllBlockKeys {
            
            if !((BlocksDataDictionary?.keys.contains(key))!){
                
                switch key {
                case 0:
                    DisplayBlockData1()
                case 1:
                    DisplayBlockData2()
                case 2:
                    DisplayBlockData3()
                case 3:
                    DisplayBlockData4()
                case 4:
                    DisplayBlockData5()
                case 5:
                    DisplayBlockData6()
                case 6:
                    DisplayBlockData7()
                case 7:
                    DisplayBlockData8()
                case 8:
                    DisplayBlockData9()
                case 9:
                    DisplayBlockData10()
                case 10:
                    DisplayBlockData11()
                case 11:
                    DisplayBlockData12()
                default:
                    Void()
                }
            }
        }

    }
    
    @IBAction func Day_BtnPressed(_ sender: AnyObject) {
     
        let buttonPressed = sender as! UIButton
        let Day:String? = buttonPressed.titleLabel?.text
        
        if let Day = Day{
            print("Day pressed =",Day)
            print("button subviews count =",buttonPressed.subviews.count)
//            print("last subview =",(buttonPressed.subviews.last as! UILabel).text!)
            let date = (buttonPressed.subviews.first as! UILabel).text!
            print("Date pressed =",date)
            self.progressBarDisplayer("Loading. . .", true)
            self.performLoadDataRequestWithURL(self.getUrl(), Date: date)
        }
        
        
        // show Audits and Stages along side of day Button using
        // sortedPreviousDays_withDate
        
//        //Fist Reset all lables
//        for label in Audits_and_Stage{
//            label.text = ""
//            
//        }
        
//        
//        for pDayObj in sortedPreviousDays_withDate!{
//            
//            let SelectedDate = (buttonPressed.subviews.first as! UILabel).text!
//            
//            if SelectedDate == pDayObj.0{
//               
//                let stats = pDayObj.1.Stats
//                
//                for (stat,label) in zip(stats, Audits_and_Stage) {
//                    let auditsCount = stat.Audits
//                    let stage = stat.Stage
//                    
//                    label.text = "\(auditsCount) \(stage)"
//                }
//            }
//            //date.0
//        }
//        
        
    }
    
    
    @IBAction func PreviousForwordDay_BtnPressed(_ sender: AnyObject) {
        
        let buttonPressed = sender as! UIButton
        print("forword button label =",  buttonPressed.titleLabel?.text)
        
        if buttonPressed.titleLabel?.text != nil{
            
            if buttonPressed.titleLabel?.text! == " >"{
               
                showForwordDays()
                PreviousForwordDay_Btn.setTitle(" <", for: UIControlState())
                
                
            }else if buttonPressed.titleLabel?.text! == " <"{
                
                showPreviousDays()
                PreviousForwordDay_Btn.setTitle(" >", for: UIControlState())
            }
            
        }// if title no nil
    }
    
    
    func showPreviousDays() -> Void {
        
        let buttonNamesArray = [self.Day1_Btn, self.Day2_Btn, self.Day3_Btn, self.Day4_Btn, self.Day5_Btn]
        let buttonLabelsArray = [self.Audits_and_StageDay1,self.Audits_and_StageDay2,self.Audits_and_StageDay3,self.Audits_and_StageDay4,self.Audits_and_StageDay5]
        
        //Fist Reset all forwordDayButtons and labels
        
        for pbutton in buttonNamesArray{
            pbutton!.setTitle(nil, for: UIControlState())
            let subTitle = pbutton!.subviews.first as! UILabel
            subTitle.text = nil
        }
            
        for labelSet in buttonLabelsArray{
            
            for label in labelSet!{
                label.text = ""
            }
        }

        var buttonCount = 0
        for pd in self.sortedPreviousDays_withDate!{
            // pd.1.pDay  is Day
            print("pd =",pd)
            buttonNamesArray[buttonCount]?.setTitle(pd.1.pDay, for: UIControlState())
            
            let subTitle = buttonNamesArray[buttonCount]?.subviews.first as! UILabel
            subTitle.text = pd.0
            
            // show Audits and Stages along side of day Buttons using
            
         //Now set AuditStage labels for Audits_and_StageDays
            
            let stats = pd.1.Stats
            
            for (stat,label) in zip(stats, buttonLabelsArray[buttonCount]!) {
                let auditsCount = stat.Audits
                let stage = stat.Stage
                
                label.text = "\(auditsCount) \(stage)"
            }
            
            // setting of label ends
            
            
            buttonCount+=1
        }
    }
    
    
    func showForwordDays() -> Void {
        
        let buttonNamesArray = [self.Day1_Btn, self.Day2_Btn, self.Day3_Btn, self.Day4_Btn, self.Day5_Btn]
        let buttonLabelsArray = [self.Audits_and_StageDay1,self.Audits_and_StageDay2,self.Audits_and_StageDay3,self.Audits_and_StageDay4,self.Audits_and_StageDay5]
        
        //Fist Reset all forwordDayButtons and labels
        
        for fbutton in buttonNamesArray{
            fbutton!.setTitle(nil, for: UIControlState())
            let subTitle = fbutton!.subviews.first as! UILabel
            subTitle.text = nil
            
        }
        for labelSet in buttonLabelsArray{
            
            for label in labelSet!{
                label.text = ""
            }
            
        }
        
        var buttonCount = 0
        for pd in self.sortedForwordDays_withDate!{
            // pd.1.pDay  is Day
            print("pd =",pd)
            buttonNamesArray[buttonCount]?.setTitle(pd.1.pDay, for: UIControlState())
            
            var subTitle = buttonNamesArray[buttonCount]?.subviews.first as! UILabel
            subTitle.text = pd.0
            
            // show Audits and Stages along side of day Buttons using
     
            //Now set AuditStage labels for Audits_and_StageDays
            
            let stats = pd.1.Stats
            
            for (stat,label) in zip(stats, buttonLabelsArray[buttonCount]!) {
                let auditsCount = stat.Audits
                let stage = stat.Stage
                
                label.text = "\(auditsCount) \(stage)"
            }
            
            // setting of label ends
            
            
            buttonCount+=1
        }
    }
    
    
    
    
    
    
    
    
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
 /*
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet() as NSCharacterSet).uppercaseString
        
        if (cString.hasPrefix("#")) {
            cString = cString.substringFromIndex(cString.startIndex.advancedBy(1))
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.grayColor()
        }
        
        var rgbValue:UInt32 = 0
        NSScanner(string: cString).scanHexInt(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
 */
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if segue.identifier == "sendDataToAuditView"{
            let destinationVC_NgC = segue.destination as! UINavigationController
            let destinationVC = destinationVC_NgC.viewControllers.first as! AuditView
            destinationVC.recievedAuditFromAuditModeViewController = sender as? String
            
        }else if segue.identifier == "goto-KPI-MODE"{
            let destinationVC = segue.destination as! KPIViewController
            AuditCodeOfDashboardCell = sender as? String
            present(destinationVC, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func changeAuditModeToMnagerBtnPressed(_ sender: Any) {
        if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                present(resultController, animated: true, completion: nil)
            }
        }else{
            
            
//            if let resultController = storyboard!.instantiateViewController(withIdentifier: "qrScannerController") as? QRScannerController {
//                present(resultController, animated: true, completion: nil)
//            }

            
        }
    }
    
}

func hexStringToUIColor (_ hex:String) -> UIColor {
    
//    trimmingCharacters(in: NSCharacterSet.whitespaceAndNewlineCharacterSet() as NSCharacterSet).uppercased()
    var cString:String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString = cString.substring(from: cString.characters.index(cString.startIndex, offsetBy: 1))
    }
    
    if ((cString.characters.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

extension UIViewController {
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x:5 , y: self.view.frame.size.height-150, width: self.view.frame.size.width - 10, height: 100))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        toastLabel.numberOfLines = 0
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 2, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    } }





