//
//  CAP_cellDefect.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 15/11/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class CAP_cellDefect: UITableViewCell {

    
    @IBOutlet weak var DefectType: UILabel!
    @IBOutlet weak var DefectCode: UILabel!
    @IBOutlet weak var DefectColor: UILabel!
    
    @IBOutlet weak var DefectID: UILabel!
    
    @IBOutlet weak var CAP_textField: UITextField!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        DefectColor.layer.cornerRadius = 8.0
        DefectColor.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
