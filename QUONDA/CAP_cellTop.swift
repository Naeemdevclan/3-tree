//
//  CAP_cellTop.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 16/11/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class CAP_cellTop: UITableViewCell {

    @IBOutlet weak var AuditCodeLabel: UILabel!
    @IBOutlet weak var AuditStageLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
