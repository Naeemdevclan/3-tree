//
//  CAPtextField.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 13/11/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class CAPtextField: UITextField {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupTextField()
    }
    
    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        setupTextField()
    }
    
    
    func setupTextField() -> Void {
        self.layer.borderColor = UIColor.red.cgColor
    }
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
