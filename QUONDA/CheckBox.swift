//
//  CheckBox.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 13/04/2017.
//  Copyright © 2017 Muhammad Naeem. All rights reserved.
//

import UIKit

class CheckBox: UIButton {
    // Images
    let checkedImage = UIImage(named: "checked-checkbox-512")! as UIImage
    let uncheckedImage = UIImage(named: "Unchecked-Checkbox")! as UIImage
    
    // Bool property
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(checkedImage, for: .normal)
            } else {
                self.setImage(uncheckedImage, for: .normal)
            }
        }
    }
    
    override func awakeFromNib() {
        self.addTarget(self, action: #selector(CheckBox.actionSample(sender:)), for: UIControlEvents.touchUpInside)
        self.isChecked = false
    }
    
    func actionSample(sender: UIButton) {
        if sender == self {
            isChecked = !isChecked
        }
    }
}
