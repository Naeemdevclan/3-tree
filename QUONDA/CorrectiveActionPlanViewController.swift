//
//  CorrectiveActionPlanViewController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 11/11/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class CorrectiveActionPlanViewController: UIViewController, UITableViewDataSource,UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate, UITabBarDelegate {
    
    
    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    
    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!

    var arrayOfDefectsLoggedRecieved_FromCommentsView:[((Color:UIColor, ID:String),(DefectType:String,DefectCode:String))] = []
    
    
    
    
    //[(DefectType:String,DefectCode:String)]!
    var AuditCodeIn = ""
    var AuditStageIn = ""
    var AuditStageColor:UIColor!
    var ReportID = ""
    
    
    
    var jsonFilePath:URL!
    // let libraryDirectoryPathString = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true).first!
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    var created:Bool!


//    @IBOutlet weak var AuditCodeLabel: UILabel!
//    @IBOutlet weak var AuditStageLabel: UILabel!
//    
//    @IBOutlet weak var DefectsStack: UIStackView!
//    @IBOutlet weak var ScrollView: UIScrollView!
    
    @IBOutlet weak var DefectsTable: UITableView!
    
//    @IBOutlet weak var superViewOfTable: UIView!
    
    var Darray:[((Color:UIColor, ID:String),(DefectType:String,DefectCode:String))]! // = [()]
    
    //[(DefectType:String,DefectCode:String)] = []
    
//    var Darray = ["A","B","C","D","E","F","G"] //,"H","I","J"]
    
    var TagLength = 0
    var arrayOfTags:[Int] = []
    
    var TAG_CAPsArray:[String:String] = [:]
   var TAG_CAPArray = [AnyObject]()
    var othersCAP = ""
    
    
    var keyboardShow_userInfo:[AnyHashable: Any]?
    
    var placeholderLabel:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Selected Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)
        
        

        
        
        
        
//        self.AuditCodeLabel.text = AuditCodeIn
//        self.AuditStageLabel.text = AuditStageIn
//        self.AuditStageLabel.backgroundColor = AuditStageColor
//        
        DefectsTable.dataSource = self
        DefectsTable.delegate = self
     
        
//        var SUBDarray = []
//        
//        if Darray.count > 7 {
//            
//            for 0 in
//        }
        
//        TagLength = Darray.count
       
//        for i in 0...TagLength-1 {
//            print(i)
//            arrayOfTags.append(i)
//        }
        
//        for _ in stride(from: from, to: to, by: by) { }
        
       
        //var SavedDefects = defaults.object(forKey: "\(self.AuditCodeIn)") as? [((Color:UIColor, ID:String),(DefectType:String,DefectCode:String))]
        
        
        
        
        if(arrayOfDefectsLoggedRecieved_FromCommentsView.count>0){
        Darray = arrayOfDefectsLoggedRecieved_FromCommentsView
        }else{
            if let loadedCart = defaults.array(forKey: "defectsArr\(AuditCodeIn)") as? [[String: Any]] {
                for item in loadedCart {
                    
                    let diyValues = (item["Color"] as! String).components(separatedBy: " ")
                    print("diyValues \(diyValues)")
                    let returnedColor = UIColor(colorLiteralRed: diyValues[1].FloatValue()!, green: diyValues[2].FloatValue()!, blue: diyValues[3].FloatValue()!, alpha: diyValues[4].FloatValue()!)
                    print(returnedColor)
                    print(item["ID"] as! String)    // 19.99, 4.99
                    print(item["DefectType"]   as! String)
                    print(item["DefectCode"]   as! String)
                    
                    self.arrayOfDefectsLoggedRecieved_FromCommentsView.append(((Color:returnedColor, ID:item["ID"] as! String),(DefectType:item["DefectType"] as! String,DefectCode:item["DefectCode"] as! String)))

                }
            }
            Darray = arrayOfDefectsLoggedRecieved_FromCommentsView
            print(Darray)
        }
       // Darray = SavedDefects!
        //[((Color:UIColor.redColor(), ID:"16-Nov-2016"),(DefectType:"DTDTDT",DefectCode:"DCDCDC")),((Color:UIColor.redColor(), ID:"17-Nov-2016"),(DefectType:"DTDTDT",DefectCode:"DCDCDC")),((Color:UIColor.redColor(), ID:"18-Nov-2016"),(DefectType:"DTDTDT",DefectCode:"DCDCDC")),((Color:UIColor.redColor(), ID:"19-Nov-2016"),(DefectType:"DTDTDT",DefectCode:"DCDCDC")),((Color:UIColor.redColor(), ID:"20-Nov-2016"),(DefectType:"DTDTDT",DefectCode:"DCDCDC")),((Color:UIColor.redColor(), ID:"21-Nov-2016"),(DefectType:"DTDTDT",DefectCode:"DCDCDC")),((Color:UIColor.redColor(), ID:"22-Nov-2016"),(DefectType:"DTDTDT",DefectCode:"DCDCDC")),((Color:UIColor.redColor(), ID:"23-Nov-2016"),(DefectType:"DTDTDT",DefectCode:"DCDCDC"))]
       
        
//        ScrollView.delegate = self
//        ScrollView.contentSize.height = 900
//        ScrollView.translatesAutoresizingMaskIntoConstraints = false
        
        // - - - - add notifications to current view    keyboardWillShow:
        
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name:UIKeyboardWillShowNotification, object: self.view.window)
//        
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name:UIKeyboardWillHideNotification, object: self.view.window)
        
        
        // - - - -
        
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        
//        if textView == Comments_Arcadia{
        
            placeholderLabel.isHidden = !textView.text.isEmpty
//        }
    }

    
//    let items = [[], ["A","B","C","D","E"], ["sausage", "chicken pesto", "prawns", "mushrooms"]]
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cellToReturn: UITableViewCell?
        
        if indexPath.section == 0{
            let cell = self.DefectsTable.dequeueReusableCell(withIdentifier: "cell_minus1") as! CAP_cellTop
            
            cell.AuditCodeLabel.text = AuditCodeIn
            cell.AuditStageLabel.text = AuditStageIn
            cell.AuditStageLabel.backgroundColor = AuditStageColor
            
            cellToReturn = cell
            
        }else if indexPath.section == 1{
            let cell = self.DefectsTable.dequeueReusableCell(withIdentifier: "cell0") as! CAP_cell0
            //            return cell
            cellToReturn = cell
            
        }else if indexPath.section == 2{
            
            let cell = self.DefectsTable.dequeueReusableCell(withIdentifier: "cell") as! CAP_cellDefect
            
            cell.DefectColor.backgroundColor = (Darray[indexPath.row].0).Color
            cell.DefectType.text = (Darray[indexPath.row].1).DefectType//.DefectType
            cell.DefectCode.text = (Darray[indexPath.row].1).DefectCode//DefectCode
            cell.CAP_textField.delegate = self
            print("indexPath.row =",indexPath.row)
            
            cell.CAP_textField.tag = indexPath.row  //(Darray[indexPath.row].0).ID //indexPath.section*10+indexPath.row
            
            
            print("CAP_textField.subviews = ",cell.CAP_textField.subviews)
           
            var subViewLabel:UILabel?
            
            let subViews = cell.CAP_textField.subviews
            for subView in subViews{
                
                if subView.isKind(of: UILabel.self){
                    subViewLabel = subView as! UILabel
//                    subViewID = (subView as! UILabel).text!
                }
            }
            
            if subViewLabel == nil{
                
                // add ID as a SubView in CAP_textField
                placeholderLabel = UILabel()
                placeholderLabel.text = (Darray[indexPath.row].0).ID
                placeholderLabel.sizeToFit()
                
                cell.CAP_textField.addSubview(placeholderLabel)
                
                placeholderLabel.frame.origin = CGPoint(x: 5, y: cell.CAP_textField.font!.pointSize / 2)
                placeholderLabel.textColor = UIColor(white: 0, alpha: 0.3)
                placeholderLabel.isHidden = true//false
                
                print("subview added")
            }
            //   return cell!
            cellToReturn = cell
            
        }else if indexPath.section == 3{

            let cell = self.DefectsTable.dequeueReusableCell(withIdentifier: "cellLast") as! CAP_cellLast
            
            cell.CAP_others_TextView.delegate = self
            cell.CAP_others_TextView.layer.borderWidth = 1
            cell.CAP_others_TextView.layer.borderColor = UIColor.gray.cgColor

            // add placeholder text in comments_Arcadia TextView
            placeholderLabel = UILabel()
            placeholderLabel.text = "CAP - Others"
            placeholderLabel.font = UIFont.italicSystemFont(ofSize: cell.CAP_others_TextView.font!.pointSize)
            placeholderLabel.sizeToFit()
            cell.CAP_others_TextView.addSubview(placeholderLabel)
            placeholderLabel.frame.origin = CGPoint(x: 5, y: cell.CAP_others_TextView.font!.pointSize / 2)
            placeholderLabel.textColor = UIColor(white: 0, alpha: 0.3)
            placeholderLabel.isHidden = !cell.CAP_others_TextView.text!.isEmpty
            

//            return cell
            cellToReturn = cell

        }
        return cellToReturn!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4 //3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 1
            
        }else if section == 1{
            return 1
            
        }else if section == 2{
            return Darray.count
            
        }else if section == 3{
            return 1
            
        }else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0
        {
            return 35.0
            
        }else if indexPath.section == 1{
            return 35.0
            
        }else if indexPath.section == 2{
            return 80.0
            
        }else if indexPath.section == 3{
        
            return 305.0 // 275.0
            
        }else{
            
            return 0.0
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
//        cell!.userInteractionEnabled = true
        print("row pressed.")
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
//        DefectsTable.frame = CGRectMake(DefectsTable.frame.origin.x, DefectsTable.frame.origin.y, DefectsTable.frame.size.width, DefectsTable.contentSize.height)
//        DefectsTable.reloadData()
    }
    
//    var count = 0
//    override func viewDidAppear(animated: Bool) {
//
//        if count == 0{
//          
//            DefectsTable.frame = CGRectMake(DefectsTable.frame.origin.x, DefectsTable.frame.origin.y, DefectsTable.frame.size.width, DefectsTable.contentSize.height)
//
//            DefectsTable.reloadData()
//            count = 1
//        }
//        
//    }
 
    
    override func viewDidLayoutSubviews(){
        
//        super.viewDidLayoutSubviews()
        
//        DefectsTable.frame = CGRectMake(DefectsTable.frame.origin.x, DefectsTable.frame.origin.y, DefectsTable.frame.size.width, DefectsTable.contentSize.height)
//        DefectsTable.reloadData()
//        
//        ScrollView.contentSize.height = 1000//LayoutView.frame.height
    }
    
    
//    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
    
//        DefectsTable.frame = CGRectMake(DefectsTable.frame.origin.x, DefectsTable.frame.origin.y, DefectsTable.frame.size.width, DefectsTable.contentSize.height)
//        DefectsTable.reloadData()
        
//    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        print("--Text field Begin editing")
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("Text field ends editing")
       
        var subViewID = ""
        
        print("textField.subviews = ",textField.subviews)
        let subViews = textField.subviews
        for subView in subViews{
            
            if subView.isKind(of: UILabel.self){
                subViewID = (subView as! UILabel).text!
                print("subViewID__ =",subViewID)
                break
            }
        }
        
        let caps = ["Id":subViewID,"Cap":textField.text!]
        let jsonData = try! JSONSerialization.data(withJSONObject: caps, options: JSONSerialization.WritingOptions())
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as! String
        print("jsonString =",jsonString)

        TAG_CAPsArray.updateValue(textField.text!, forKey: subViewID)
        TAG_CAPArray.append(jsonString as AnyObject)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
//        if arrayOfTags.contains(textField.tag) {
        
//        var subViewID = ""
//        
//        print("textField.subviews = ",textField.subviews)
//        let subViews = textField.subviews
//        for subView in subViews{
//            
//            if subView.isKindOfClass(UILabel){
//                subViewID = (subView as! UILabel).text!
//            }
//        }
//       
//        
//            TAG_CAPsArray.updateValue(textField.text!, forKey: subViewID)
//        }
        
        textField.resignFirstResponder()
        
        return true
    }
    
//    func textViewDidBeginEditing(textView: UITextView) {
//        print("textView has start editing.")
//        
//        let keyboardSize: CGSize = keyboardShow_userInfo![UIKeyboardFrameBeginUserInfoKey]!.CGRectValue.size
//        let offset: CGSize = keyboardShow_userInfo![UIKeyboardFrameEndUserInfoKey]!.CGRectValue.size
//        
//        if keyboardSize.height == offset.height {
//            UIView.animateWithDuration(0.1, animations: { () -> Void in
//                self.view.frame.origin.y -= keyboardSize.height
//            })
//        }else {
//            UIView.animateWithDuration(0.1, animations: { () -> Void in
//                self.view.frame.origin.y += keyboardSize.height - offset.height
//            })
//        }
////        self.view.frame.origin.y - keyboardHeight
//    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n" {
            othersCAP = textView.text
            textView.resignFirstResponder()
//            self.view.frame.origin.y += (keyboardShow_userInfo![UIKeyboardFrameBeginUserInfoKey]!.CGRectValue.size).height
        }
        return true
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
            //            defaults.removeObjectForKey("userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
            //        case 4:
            //            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
            //                //                self.dismissViewControllerAnimated(true, completion: nil)
            //                presentViewController(resultController, animated: true, completion: nil)
            //            }
            //
            //            let selectedImage:UIImage = UIImage(named: "Selected QAReport")!
            //            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            //
        //            break
        case 5:
            if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }else{
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        default:
            break
        }
    }
    
    
//    var Cap_json:AnyObject?
    
    @IBAction func SaveBtn_Pressed(_ sender: AnyObject) {
        
        print("save and continue pressed.")
        print("othersCAP =",othersCAP)
        print("TAG_CAPsArray =",TAG_CAPArray)
        print("\(TAG_CAPArray)")
//        var Cap_json:AnyObject?
        
//        Cap_json = try? NSJSONSerialization.dataWithJSONObject(TAG_CAPsArray, options: NSJSONWritingOptions)
        
//        let jsonData = try! NSJSONSerialization.dataWithJSONObject(TAG_CAPsArray, options: NSJSONWritingOptions.allZeros)
//        let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
//        print(jsonString)
        
        // Here send Data to the server.
        //if !Reachability.isConnectedToNetwork(){
            saveOfflineCommentsData()
        //}
        

       // performLoadDataRequestWithURL()
    }
    var bodyData = ""
    var task:URLSessionDataTask?
    func delete(element: String) {
        DefectAdded = DefectAdded.filter() { $0 != element }
    }
    func performLoadDataRequestWithURL() -> String? {
        
        let jsonData = try! JSONSerialization.data(withJSONObject: TAG_CAPArray, options: JSONSerialization.WritingOptions())
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as! String
        print("jsonString =",jsonString)

        
        self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCodeIn)&Others=\(othersCAP)&Cap=\(TAG_CAPArray)"
        
        print("bodyData =",bodyData)
//        http://portal.3-tree.com/api/android/quonda/save-cap.php
        
        let request: NSMutableURLRequest = NSMutableURLRequest(url: self.getUrl())
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = bodyData.data(using: String.Encoding.utf8)
        let session = URLSession.shared
        
        task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            print("response =",response)
            
            if (error != nil) {
                print(error)
                
                
                let alertView = UIAlertController(title: nil , message: "Cannot save CAP \n try saving again.", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                })
                alertView.addAction(ok_action)
                
                self.present(alertView, animated: true, completion: nil)
                
                
            }
            else if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                
                print(json)
                print("status is OK")
                self.delete(element: self.AuditCodeIn)
                if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                    if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                        
                        self.present(resultController, animated: true, completion: nil)
                    }
                }else{
                    if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                        
                        self.present(resultController, animated: true, completion: nil)
                    }
                }
                
//                var dictionary:NSDictionary = self.parseJSON(json)
                
//                print("information from location Dictionary =\(dictionary)")
//                
//                if("\(dictionary["Status"]!)" == "OK"){
//                    
//                    print("Successfuly sent Coordinates)")
//                    
//                }else{
//                    print("There is an error while sending Coordinates to the server.")
//                }
                
            }//if data != nil
            else{
                print("return data is nil")
            }
            
        }) 

        task?.resume()
        return ""
    }
    
    
    func getUrl() -> URL {
        let toEscape = "http://app.3-tree.com/quonda/save-cap.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
    func saveOfflineCommentsData(){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        print("You are in AuditComments OFFLINE mode.")
        
        // Creating AuditComment Json file
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("AuditCapFile\(self.AuditCodeIn).json")
        
        // creating a .json file in the Documents folder
        
        if !fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("File created ")
                
                // Now save it to FilesToSync.json file
                
                saveFileNameTo_FilesToSync("AuditCapFile\(self.AuditCodeIn).json")
                //(jsonFilePath.absoluteString)
                
            } else {
                print("Couldn't create file for some reason")
            }
        } else {
            print("File already exists")
        }
        //file creation ends
        
        
        
        //First Read out the AuditComment AuditCodeFile
        
        do{
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath.absoluteString, encoding: String.Encoding.utf8.rawValue) as String
            
            print("readAuditComment_String = ",readString)
            
            /// here send readString to function to update values stored in AuditCapFile(AuditCode) File
            
            self.AuditCommentAuditCodeFile(readString, AuditCommentFilePath: jsonFilePath.absoluteString)
            
        }
        catch let error as NSError{
            print("There is an error while reading AddDeffectText(AuditCode) File.",error.description)
        }
        
    }
    
    func AuditCommentAuditCodeFile(_ savedCommentsData:String?, AuditCommentFilePath: String) -> Void {
        
        if ReportID == "14" || ReportID == "34"{
            // save data against Tm Clothing only
            
            AuditCommentAuditCodeFile_Hybrid(savedCommentsData, AuditCommentFilePath: AuditCommentFilePath)
            
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        //Reading from JsonDictionary File stored in documents directory
        do{
            var readString: String
            readString = try NSString(contentsOfFile: AuditCommentFilePath, encoding: String.Encoding.utf8.rawValue) as String
            //                    print("readString=\(readString)")
            let readStringParsed = parseCommentsJSON(readString)
            print("AuditComments_readStringParsed = ",readStringParsed)
        }
        catch let error as NSError{
            print("There is an error while reading from JsonDictionary File.",error.description)
            
        }
        
    }
   
    func AuditCommentAuditCodeFile_Hybrid(_ savedCommentsData:String?, AuditCommentFilePath: String) -> Void{
        
        // - - - -
        
      //  if savedCommentsData != nil && savedCommentsData != "" {
        self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCodeIn)&Others=\(othersCAP)&Cap=\(TAG_CAPArray)"
    
            print("AuditCapsHybrid-bodydata -> \(self.bodyData)")
            var AuditCommentsOfflineData:[String:String] = parseCommentsJSON(savedCommentsData!)
            let randomString = randomStringWithLength(6)
            AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "AuditCaps_bodyData\(self.AuditCodeIn)_\(randomString)")
            AuditCommentsOfflineData.updateValue("http://app.3-tree.com/quonda/save-cap.php", forKey: "AuditCap_url")
            
            
            if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)  

                    ////sdsd
                    var json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    print(json)
                   // json = json.replacingOccurrences(of: "\\", with: "", options: NSString.CompareOptions.literal, range: nil)
                    print(json)

                    try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    //Note!!
                    
                    if !Reachability.isConnectedToNetwork(){
                                         }
                    
                    
                    // now move onto the Next screen
                    //???????
                    //?????
                    //???
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    //if(DefectAdded.contains(AuditCodein)){
                    if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                        if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                            
                            present(resultController, animated: true, completion: nil)
                        }
                    }else{
                        if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                            
                            present(resultController, animated: true, completion: nil)
                        }
                    }
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
            
        
        //}
    }
        
        // - - -
    //Random string function
    
    func randomStringWithLength(_ len: Int) -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for i in 0 ..< (len){
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString
    }

   func parseCommentsJSON(_ jsonString: String) -> [String:String] {
    if let data = jsonString.data(using: String.Encoding.utf8) {
        let error: NSError? = nil
        if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:String] {
            return json!
        } else if let error = error {
            //Here's where the error comes back.
            print("JSON Error: \(error)")
        } else {
            print("Unknown JSON Error")
        }
    }
    return ["":""]
}

    // end of func


    // adjust the screen size when keyboard appears and hides
    
//    func keyboardWillHide(sender: NSNotification) {
//        let userInfo: [NSObject : AnyObject] = sender.userInfo!
//        let keyboardSize: CGSize = userInfo[UIKeyboardFrameBeginUserInfoKey]!.CGRectValue.size
//        self.view.frame.origin.y += keyboardSize.height
//    }
    
//    func keyboardWillShow(sender: NSNotification) {
//        let userInfo: [NSObject : AnyObject] = sender.userInfo!
//        let keyboardSize: CGSize = userInfo[UIKeyboardFrameBeginUserInfoKey]!.CGRectValue.size
//        let offset: CGSize = userInfo[UIKeyboardFrameEndUserInfoKey]!.CGRectValue.size
//        
//        keyboardShow_userInfo = sender.userInfo!
//        
////        keyboardHeight = keyboardSize.height
//        
////        if keyboardSize.height == offset.height {
////            UIView.animateWithDuration(0.1, animations: { () -> Void in
////                self.view.frame.origin.y -= keyboardSize.height
////            })
////        } else {
////            UIView.animateWithDuration(0.1, animations: { () -> Void in
////                self.view.frame.origin.y += keyboardSize.height - offset.height
////            })
////        }
//    }

    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
