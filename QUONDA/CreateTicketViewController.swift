//
//  CreateTicketViewController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 20/04/2017.
//  Copyright © 2017 Muhammad Naeem. All rights reserved.
//

import UIKit

import FileBrowser

class CreateTicketViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,UITextViewDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var contentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var subCategoryView: UIView!
    
    @IBOutlet weak var priorityView: UIView!
    @IBOutlet weak var txtMessageView: UITextView!
    
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnGallery: UIButton!
    @IBOutlet weak var btnFile: UIButton!
    @IBOutlet weak var btnCreat: UIButton!
    
    @IBOutlet weak var lblCategry: UILabel!
    @IBOutlet weak var lblSubcategory: UILabel!
    @IBOutlet weak var lblPriority: UILabel!
    
    @IBOutlet weak var btnSelectCategory: UIButton!
    
    @IBOutlet weak var btnSelectSubCategory: UIButton!
    @IBOutlet weak var btnPriority: UIButton!
    var list : UITableView = UITableView.init()
    var SelecteList = [String]()
    var selectCategory = "Category"
    var messageFrame = UIView()
    var activityIndicator = UIActivityIndicatorView()
    var strLabel = UILabel()
    var category = [String:String]()
    var subcategory = [String:String]()
    var priority = [String:String]()
    var files = [Any]()
    var capturePhoto: UIImage? = nil
    var imagePicker: UIImagePickerController!
    var departKey = ""
    var showImageView = UIImageView.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SelecteList.append("One")
        SelecteList.append("Two")
        SelecteList.append("Three")
        SelecteList.append("Four")
        SelecteList.append("Five")
        SelecteList.append("Six")
        SelecteList.append("Seven")
        SelecteList.append("Eight")
        SelecteList.append("Nine")
        SelecteList.append("Ten")
       
        txtMessageView.text = "Enter your issue details here..."
        txtMessageView.textColor = .lightGray
        btnCreat.layer.cornerRadius = 15
        self.setListView()
        showImageView.frame = self.view.frame
        showImageView.backgroundColor = UIColor.white
        showImageView.isHidden = true
        self.view.addSubview(showImageView)
        let tapgestures = UITapGestureRecognizer.init(target: self, action: #selector(hideImage))
        tapgestures.delegate = self
        showImageView.isUserInteractionEnabled = true
        showImageView.addGestureRecognizer(tapgestures)
        let backGroundTap = UITapGestureRecognizer.init(target: self, action: #selector(hideTableView))
        backGroundTap.delegate = self
        backGroundTap.numberOfTapsRequired = 1
        self.contentView.addGestureRecognizer(backGroundTap)
        let tap = UITapGestureRecognizer(target: self, action: #selector(tableTapped))
        self.list.addGestureRecognizer(tap)
    }
    func tableTapped(tap:UITapGestureRecognizer) {
        let location = tap.location(in: self.list)
        let path = self.list.indexPathForRow(at: location)
        if let indexPathForRow = path {
            tableView(list, didSelectRowAt: path!)
        } else {
            // handle tap on empty space below existing rows however you want
        }
    }
    func hideImage(){
        showImageView.isHidden = true
    }
    func hideTableView(){
        self.view.endEditing(true)
        list.isHidden = true
        
    }
    func setListView(){
        list.frame = CGRect(x:0.0,y:0.0,width:categoryView.frame.width,height:340.0)
        list.delegate = self
        list.dataSource = self
        list.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        list.isHidden = true
        list.layer.borderWidth = 5.0
        list.layer.cornerRadius = 10.0
        list.layer.borderColor = UIColor.darkGray.cgColor
        self.contentView.addSubview(list)
    }
    @IBAction func selectAction(_ sender: UIButton) {
        if(sender == btnSelectCategory){
             SelecteList.removeAll()
            list.isHidden = false
            selectCategory = "Category"
            for (key, value) in category {
                print("\(key) -> \(value)")
                SelecteList.append(value)
            }
            SelecteList = SelecteList.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
            
            list.frame = CGRect(x:categoryView.frame.origin.x,y:categoryView.frame.origin.y + categoryView.frame.height,width:categoryView.frame.width,height:CGFloat(SelecteList.count * 44))
            // SelecteList = ["1","2","3","4","5","6","7","8","9","10"]
            list.reloadData()
        }
        else if(sender == btnSelectSubCategory){
            SelecteList.removeAll()
            list.isHidden = false
            selectCategory = "SubCategory"
            SelecteList.removeAll()
            for (key, value) in subcategory {
                print("\(key) -> \(value)")
                if(key.contains(departKey)){
                    SelecteList.append(value)
                }
                
            }
            SelecteList = SelecteList.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
            
            //            for (key, value) in subcategory {
            //                print("\(key) -> \(value)")
            //                SelecteList.append(value)
            //            }
            list.frame = CGRect(x:subCategoryView.frame.origin.x,y:subCategoryView.frame.origin.y + subCategoryView.frame.height,width:subCategoryView.frame.width,height:CGFloat(SelecteList.count * 44))
            
            //SelecteList = ["1st","2nd","3rd","4th","5th","6th","7th","8th","9th","10th"]
            list.reloadData()
        }
        else{
            SelecteList.removeAll()
            list.isHidden = false
            selectCategory = "Priority"
            for (key, value) in priority {
                print("\(key) -> \(value)")
                SelecteList.append(value)
            }
            SelecteList = SelecteList.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
            list.frame = CGRect(x:priorityView.frame.origin.x,y:priorityView.frame.origin.y + priorityView.frame.height,width:priorityView.frame.width,height:CGFloat(SelecteList.count * 44))
            //list.frame.height = SelecteList.count * 44
            //SelecteList = ["High","Medium","Low"]
            list.reloadData()
            
        }
    }
    func sampleImageTapped(){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        
        present(imagePicker, animated: true, completion: nil)
    }
    func samplePhotoLibImageTapped(){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        imagePicker.dismiss(animated: true, completion: nil)
        
        DispatchQueue.main.async { () -> Void in
            self.capturePhoto = info[UIImagePickerControllerOriginalImage] as? UIImage
            let imageData:NSData = UIImageJPEGRepresentation(self.capturePhoto!,0.4)! as NSData
            let ImageString = imageData.base64EncodedString(options: .endLineWithLineFeed)
            let fileItem = ["jpg":ImageString]
            self.files.append(fileItem)
            self.attachements()
            
        }
    }
    
    @IBAction func btnbackAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func cameraAction(_ sender: Any) {
        self.samplePhotoLibImageTapped()
        
        //        let alertView = UIAlertController(title: "Choose Image From" , message: "", preferredStyle: .alert)
        //        let camera_action: UIAlertAction = UIAlertAction(title: "Camera", style: .default, handler: {(UIAlertAction)-> Void in
        //            self.sampleImageTapped()
        //        })
        //        let gallery_action: UIAlertAction = UIAlertAction(title: "Gallery", style: .default, handler: {(UIAlertAction)-> Void in
        //            self.samplePhotoLibImageTapped()
        //        })
        //
        //        alertView.addAction(camera_action)
        //        alertView.addAction(gallery_action)
        //        self.present(alertView, animated: true, completion: nil)
    }
    @IBAction func galleryAction(_ sender: Any) {
        //samplePhotoLibImageTapped()
    }
    @IBAction func fileAction(_ sender: Any) {
        let fileBrowser = FileBrowser()
        present(fileBrowser, animated: true, completion: nil)
        fileBrowser.didSelectFile = { (file: FBFile) -> Void in
            let urlString: String = file.filePath.path
            print(urlString)
            // etc.
            do {
                let data = try NSData(contentsOfFile: urlString, options: NSData.ReadingOptions.mappedIfSafe)
                let fileString = data.base64EncodedString(options: .endLineWithLineFeed)
                let fileItem = [file.fileExtension! :fileString]
                print(fileItem)
                self.files.append(fileItem)
                self.attachements()
            }
            catch let error {
                print(error.localizedDescription)
            }
            
            
            
        }
        // print(listFilesFromDocumentsFolder())
    }
    
    //    func listFilesFromDocumentsFolder() -> [String]
    //    {
    //        var theError = NSErrorPointer<NSError?>
    //        let dirs = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true) as? [String]
    //
    //        if dirs != nil {
    //            let dir = dirs![0]//this path upto document directory
    //
    //            //this will give you the path to MyFiles
    //            let MyFilesPath = dir.stringByAppendingPathComponent("/BioData")
    //            if !FileManager.defaultManager().fileExistsAtPath(MyFilesPath) {
    //                FileManager.defaultManager().createDirectoryAtPath(MyFilesPath, withIntermediateDirectories: false, attributes: nil, error: theError)
    //            } else {
    //                print("not creted or exist")
    //            }
    //
    //            let fileList = FileManager.defaultManager().contentsOfDirectoryAtPath(MyFilesPath, error: theError) as! [String]
    //
    //            var count = fileList.count
    //            for var i = 0; i < count; i += 1
    //            {
    //                var filePath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first as! String
    //                filePath = filePath.stringByAppendingPathComponent(fileList[i])
    //                let properties = [NSURLLocalizedNameKey, NSURLCreationDateKey, NSURLContentModificationDateKey, NSURLLocalizedTypeDescriptionKey]
    //                var attr = NSFileManager.defaultManager().attributesOfItemAtPath(filePath, error: NSErrorPointer())
    //            }
    //            println("fileList: \(fileList)")
    //            return fileList.filter{ $0.pathExtension == "pdf" }.map{ $0.lastPathComponent } as [String]
    //        }else{
    //            let fileList = [""]
    //            return fileList
    //        }
    // }
    @IBAction func createAction(_ sender: Any) {
        progressBarDisplayer("Creating a ticket", true)
        
        _ =  performLoginRequestWithURL(getUrl())
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SelecteList.count;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.backgroundColor = UIColor.lightGray
        cell.textLabel?.text = SelecteList[indexPath.row]
        cell.textLabel?.textColor = UIColor.white
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(selectCategory == "Category"){
            lblCategry.text = SelecteList[indexPath.row]
            lblSubcategory.text = "Select Sub Category"
            for (key, value) in category {
                print("\(key) -> \(value)")
                if(value == lblCategry.text!){
                    departKey = "\(key)"
                }
                //SelecteList.append(value)
            }
        }
        else if(selectCategory == "SubCategory"){
            lblSubcategory.text = SelecteList[indexPath.row]
        }
        else{
            lblPriority.text = SelecteList[indexPath.row]
        }
        list.isHidden = true
    }
    //MARK:- TOUCHES BEGAN
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        //self.list.isHidden = true
    }
    
    //MARK:- TEXTVIEW DELEGATES
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Enter your issue details here..." || textView.text.replacingOccurrences(of: " ", with: "") == "" {
            txtMessageView.text = ""
            txtMessageView.textColor = .black
        }
//        txtMessageView.text = ""
//        txtMessageView.textColor = .black
        list.isHidden = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "Enter your issue details here..." || textView.text.replacingOccurrences(of: " ", with: "") == "" {
            txtMessageView.text = "Enter your issue details here..."
            txtMessageView.textColor = .lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        print(textView.text)
        if text == "\n"{
            textView.resignFirstResponder()
        }
        return true
    }
    
    // API call
    func getUrl() -> URL {
        let toEscape = "http://support.3-tree.com/app/create-ticket.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
    var bodyData:String?
    func performLoginRequestWithURL(_ url: URL) -> String? {
        if(lblCategry.text! == ""||lblCategry.text! == "Select Category"){
            let alertView = UIAlertController(title: "Create Ticket" , message: "Select Category first", preferredStyle: .alert)
            let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction)-> Void in
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
            })
            alertView.addAction(ok_action)
            self.present(alertView, animated: true, completion: nil)
            
            return ""
        }else if(lblSubcategory.text! == ""||lblSubcategory.text! == "Select Sub Category"){
            let alertView = UIAlertController(title: "Create Ticket" , message: "Select Sub Category first", preferredStyle: .alert)
            let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction)-> Void in
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
            })
            alertView.addAction(ok_action)
            self.present(alertView, animated: true, completion: nil)
            
            return ""
            
        }else if(txtMessageView.text == "" || txtMessageView.text! == "Enter your issue details here..."){
            
            let alertView = UIAlertController(title: "Create Ticket" , message: "Write message first", preferredStyle: .alert)
            let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction)-> Void in
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
            })
            alertView.addAction(ok_action)
            self.present(alertView, animated: true, completion: nil)
            
            return ""
            
        }else if(lblPriority.text! == ""||lblPriority.text! == "Select Priority" ){
            let alertView = UIAlertController(title: "Create Ticket" , message: "Select Priority first", preferredStyle: .alert)
            let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction)-> Void in
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
            })
            alertView.addAction(ok_action)
            self.present(alertView, animated: true, completion: nil)
            
            return ""
            
        }else{
            var str = "Files=\(self.files.count)"
            for item  in 0..<self.files.count{
                let it =  self.files[item] as! [String:String]
                for (key, value) in it {
                    str = str + "&" + "File" + "\(item)" + "Ext" + "=" + key
                    str =  str + "&" + "File" + "\(item)" + "Data" + "=" + value
                }
            }
            bodyData = "UserEmail=\(defaults.object(forKey: "userEmail") as! String)&Department=\(departKey)&Subject=\(lblSubcategory.text!)&Message=\(txtMessageView.text!)&Priority=\(lblPriority.text!)&\(str)"
            print(url,bodyData!)
            
            let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
            var json = ""
            request.httpMethod = "POST"
            request.httpBody = bodyData!.data(using: String.Encoding.utf8)
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main){
                response, data, error in
                print("\(response)")
                if data != nil {
                    json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                    // print(json)
                    let dictionary = self.parseJSON(json)
                    print(dictionary)
                    //                            print("\(dictionary["countries"]!)")
                    //                            print("\(dictionary["Status"]!)")
                    if("\(dictionary["Status"]!)" == "ERROR")
                    {
                        self.activityIndicator.stopAnimating()
                        self.messageFrame.removeFromSuperview()
                        
                        //show popup on LOGIN FAIL
                        let alert = UIAlertController(title: "Alert", message: "\(dictionary["Message"]!)", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(UIAlertAction) -> Void in
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                    }else if dictionary.count < 1{
                        let alertView = UIAlertController(title: "\(dictionary["Message"]!)" , message: "Press OK", preferredStyle: .alert)
                        let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction)-> Void in
                        })
                        alertView.addAction(ok_action)
                        self.present(alertView, animated: true, completion: nil)
                    }
                    else
                    {
                        for (key, value) in dictionary {
                            print("\(key) -> \(value)")
                        }
                        self.dismiss(animated: true, completion: nil)
                        
                        //                    self.priorities = dictionary["Priorities"] as! [String:String]
                        //                    print(self.priorities)
                        //                    self.subjects = dictionary["Subjects"] as! [String:String]
                        //                    print(self.subjects)
                        //                    self.departments = dictionary["Departments"] as! [String:String]
                        //                    print(self.departments)
                        
                        
                        // print("My_UserType = \(dictionary["UserType"]!)")
                        
                        self.activityIndicator.stopAnimating()
                        self.messageFrame.removeFromSuperview()
                        
                    }
                }// data != nil
                else{
                    let alertView = UIAlertController(title: "No Data Found" , message: "OK", preferredStyle: .alert)
                    let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction)-> Void in
                    })
                    alertView.addAction(ok_action)
                    self.present(alertView, animated: true, completion: nil)
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                }
            }
            return json
        }
    }
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    func progressBarDisplayer(_ msg:String, _ indicator:Bool ) {
        print(msg)
        strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 200, height: 50))
        strLabel.text = msg
        strLabel.textColor = UIColor.white
        messageFrame = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25 , width: 180, height: 50))
        messageFrame.layer.cornerRadius = 15
        messageFrame.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        if indicator {
            activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
            activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
            activityIndicator.startAnimating()
            messageFrame.addSubview(activityIndicator)
        }
        messageFrame.addSubview(strLabel)
        view.addSubview(messageFrame)
    }
    func attachements(){
        let yAxis = 40
        var  i = 0
        for _ in self.files {
            let dict = self.files[i] as! [String:Any]
            for (key, value) in dict {
                print("\(key) -> \(value)")
                
                
                let imgView = UIView()
                imgView.frame = CGRect(x:20,y:btnCamera.frame.origin.y + btnCamera.frame.height + 10 + CGFloat(yAxis*i),width:contentView.frame.width - 40,height:30)
                imgView.tag = 301 + i
                //            let imageView = UIImageView()
                //            imageView.frame = CGRect(x:0,y:3,width:24, height:24)
                //            imageView.image = UIImage.init(named: "crossMark")
                let btn = UIButton.init(type: .custom)
                btn.setTitleColor(UIColor.gray, for: UIControlState.normal)
                btn.frame = CGRect(x:35,y:0,width:contentView.frame.width - 70,height:30)
                
                btn.setTitle("Attachement #\(i + 1)", for: .normal)
                btn.accessibilityHint = "\(value)"
                btn.tag = 101 + i
                btn.addTarget(self, action: #selector(tapped(sender:)), for: .touchUpInside)
                //
                let crossBtn = UIButton.init(type: .custom)
                crossBtn.setTitleColor(UIColor.gray, for: UIControlState.normal)
                crossBtn.frame = CGRect(x:0,y:3,width:24, height:24)
                crossBtn.setImage(UIImage.init(named: "Red-cross"), for: .normal)
                crossBtn.setTitle("Attachement #\(i + 1)", for: .normal)
                crossBtn.accessibilityHint = "\(value)"
                crossBtn.tag = 201 + i
                crossBtn.addTarget(self, action: #selector(reloadAttchmentsView(sender:)), for: .touchUpInside)
                imgView.addSubview(crossBtn)
                // imgView.addSubview(imageView)
                imgView.addSubview(btn)
                contentView.addSubview(imgView)
                contentViewHeightConstraint.constant = contentViewHeightConstraint.constant + 30
                i = i + 1
            }
        }
    }
    @objc func reloadAttchmentsView(sender: UIButton)
    {
        var i = 0
        print(sender.tag)
        for _ in files{
            let viewd = self.view.viewWithTag(301 + i)
            i = i + 1
            viewd?.removeFromSuperview()
            viewDidLayoutSubviews()
        }
        files.remove(at: sender.tag - 201)
        contentViewHeightConstraint.constant = contentViewHeightConstraint.constant - 30
        attachements()
    }
    @objc func tapped(sender: UIButton)
    {
        print(sender.accessibilityHint!)
        
        
        let dict = files[sender.tag - 101] as! [String:Any]
        for (key, value) in dict {
            print("\(key) -> \(value)")
            
            self.showImageView.contentMode = .scaleAspectFit
            self.showImageView.isHidden = false
            
            let dataDecoded : Data = Data(base64Encoded: "\(value)", options: .ignoreUnknownCharacters)!
            let decodedimage = UIImage(data: dataDecoded)
            self.showImageView.image = decodedimage
            
            //self.showImageView.image = UIImage(data: value as! Data)
        }
    }
    
}
