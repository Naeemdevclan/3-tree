//
//  DashboardCell.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 02/03/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class DashboardCell: UITableViewCell {

    @IBOutlet weak var color_code: UILabel!
    @IBOutlet weak var first_column: UILabel!
    @IBOutlet weak var second_column: UILabel!
    @IBOutlet weak var audit_Stage_Code: UILabel!
    @IBOutlet weak var third_column: UILabel!
    // expanded cell LEFT
    @IBOutlet weak var expandedCell_LeftView: UIView!
    @IBOutlet weak var defects: UILabel!
    @IBOutlet weak var sampleSize: UILabel!
    @IBOutlet weak var DHU: UILabel!
    @IBOutlet weak var auditResult_Label: UILabel!
    
    //expanded cell PICTURE
    @IBOutlet weak var expandedCell_Photo: UIImageView!
    
    //expanded cell RIGHT
    @IBOutlet weak var expandedCell_RightView: UIView!
    @IBOutlet weak var styleNo: UILabel!
    @IBOutlet weak var brand: UILabel!
    @IBOutlet weak var season: UILabel!
    @IBOutlet weak var POno: UILabel!
    @IBOutlet weak var etd: UILabel!
    @IBOutlet weak var auditStage: UILabel!
    
    
    
    var arrowButtonPressed:Bool = false
    var arrowButtonPressed_:Bool = false

   
    @IBOutlet weak var extendedCell: UIView!
//    @IBOutlet weak var extended_cell_content_view: UIView!
  
    @IBOutlet weak var arrowButton: UIImageView!
    
    @IBOutlet weak var acessoryButton: UIButton!
    
//    @IBAction func acessoryButtonClicked(sender: AnyObject) {
//        
//        print("Arrow Button pressed")
//        if arrowButtonPressed == false{
//            arrowButtonPressed = true
//            arrowButton.image = UIImage(named: "down_arrow.png")
//        }else{
//            arrowButtonPressed = false
//            arrowButton.image = UIImage(named: "right_arrow")
//        }
//        
//    }
//    
    
//    var arrowButtonPressed: Bool = false
//    @IBAction func acessoryButtonClicked(sender: AnyObject) {
//        
//        print("Arrow Button pressed")
////        acessoryButton.setImage(UIImage(named: "down_arrow.png"), forState: [.Selected, .Highlighted])
//
//            if arrowButtonPressed == false{
//                arrowButtonPressed = true
//                acessoryButton.imageView?.image = UIImage(named: "down_arrow")
//            }else{
//                arrowButtonPressed = false
//                acessoryButton.imageView?.image = UIImage(named: "right_arrow")
//        }
////        sender.setBackgroundImage(UIImage(named: "right_arrow"), forState: .Normal)
////        sender.setBackgroundImage(UIImage(named: "down_arrow"), forState: .Highlighted)
//
//    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        acessoryButton.setImage(UIImage(named: "right_arrow.png"), forState: .Normal)
//        acessoryButton.setImage(UIImage(named: "down_arrow.png"), forState: UIControlState.Selected.union(UIControlState.Highlighted))
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
//        expandedCell_LeftView.layer.masksToBounds = true
//        expandedCell_LeftView.layer.cornerRadius = 8.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
