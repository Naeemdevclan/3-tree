//
//  DashboardViewController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 02/03/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}






//corelocation attributes

var updateLocationTimer: Timer?
var backgroundLocationTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid


var selectedArrowButton:NSMutableArray! = NSMutableArray()
var previousButtonTag:Int!



//protocol DashboardViewControllerProtocol {
//    func dismissViewController1AndPresentViewController2()
//}


class DashboardViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITabBarDelegate,UNUserNotificationCenterDelegate, CLLocationManagerDelegate {//,DashboardViewControllerProtocol {
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!

    var selectedIndexPath:IndexPath?
    var cellTappedForRow = -1

    var cellTapped:Bool = false
    var currentRow = -1;

    var currentSelection:Bool = false
    
    var rowpath = -1
    var previousTappedButtonIndexPath: IndexPath?

    var cell:DashboardCell!
    
    var activityIndicator = UIActivityIndicatorView()
    var messageFrame = UIView()
    var strLabel = UILabel()
    
    weak var tab1vc: UIViewController!
    weak var tab2vc: UIViewController!
    weak var tab3vc: UIViewController!
    weak var tab4vc: UIViewController!
    weak var tab5vc: UIViewController!
    weak var tab6vc: UIViewController!
    weak var tab7vc: UIViewController!
    weak var tab8vc: UIViewController!

    var currentViewController: UIViewController!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!

    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    
//    var items: [String] = ["We", "Heart", "Swift"]
    struct items {
        var A : UIColor
        var B : String
        var C : String
        var D : String
        var E : String
        var F : String
        var G : String
        var H : String
        var I : String
        var J : String
        var K : String
        var L : String
        var M : String
        var N : String
        var O : String
        var P : String
        var Q : String
    }
    
    var imageCache = [String:UIImage]()
    
    var tableCellData = [items]()
    var totalRowsInTable:Int!
    var bodyData:String?
    var dictionary:NSDictionary!
    var Page:Int = 1
    var indicator:UIActivityIndicatorView!
    var IndexpathToDeleteRow:IndexPath!
    
    var jsonFilePath2:URL!
    var created:Bool!
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default

    var searchParameters: String?
    var isSearchRequest:Bool = false

    var AuditCodeToSend:String!
    /*
// PATH TO Documents directory
    
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first!
    var documentsDirectoryPath:NSURL!
 */

    var task:URLSessionDataTask?
    
     required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        print("hello......hello.....")
     }
    
    
    
    // Location manager parameter.
    
    let locationManager = CLLocationManager()
    //let locator = CLLocationManager()

    func refreshList(){
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var testString = "%123213"
        do {
            
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            let range = NSRange(location: 0, length: testString.characters.count)
            let block = { (result: NSTextCheckingResult?, flags: NSRegularExpression.MatchingFlags, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
                if let result = result, result.resultType == NSTextCheckingResult.CheckingType.link {
                    print(result.url)
                    testString = ""
                }
            }
            detector.enumerateMatches(in: testString, options: [], range: range, using: block)
        } catch {
            print(error)
        }
        print(testString)
       
//        if isUpdateAvaialble == true{
//            popupMessage("Update is Availale")
//        }
        
        //self.view.makeToast(message: "Audit completed. Wait for the report to Sync.", duration:  5.0, position: HRToastPositionDefault as AnyObject)

//        // Saving JSON DATA
//        let documentsDirectoryPath2 = URL(string: documentsDirectoryPathString)!
//        
//        let jsonFilePath22 = documentsDirectoryPath2.appendingPathComponent("AuditCommentFileS418337.json")
//        do {
//            var readString: String
//            readString = try NSString(contentsOfFile: jsonFilePath22.path, encoding: String.Encoding.utf8.rawValue) as String
//            print("readString=\(readString)")
//            self.showDataWhenOffline(readString)
//        } catch let error as NSError {
//            print(error.description)
//        }

//
//        Locator.shared.locate { result in
//            switch result {
//            case .Success(self.locator):
//                if let location = self.locator.location {
//                    print(location )                  /* ... */ }
//                
//            default:
//            break;
//            }
//        }
//        
         // self.scheduleLocalNotification(title:"Audit",message:"Audit Report Synced")
//        let notification = UILocalNotification()
//        notification.fireDate = Date.init()
//        notification.alertBody = "Alert!"
//        notification.alertAction = "open"
//        notification.hasAction = true
//        notification.userInfo = ["UUID": "reminderID" ]
//        UIApplication.shared.scheduleLocalNotification(notification)
        
        
        
//        NotificationCenter.default.addObserver(self, selector: #selector(DashboardViewController.refreshList), name: NSNotification.Name(rawValue: "TodoListShouldRefresh"), object: nil)

        // send Auditor Location to the server
        
        sendAuditorLocation()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reinstateBackgroundTask), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        updateLocationTimer = Timer.scheduledTimer(timeInterval: 900.0, target: self, selector: #selector(self.sendAuditorLocation), userInfo: nil, repeats: true)
        
        registerBackgroundTask()
        
        
//        sendAuditorLocation()
        


        // Do any additional setup after loading the view.
        
        if self.searchParameters != nil{
            print(searchParameters!)
            self.bodyData = searchParameters
        }else{
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&Status=Any"
        }
        // Saving JSON DATA
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("dashboard.json")
        jsonFilePath2 = jsonFilePath
        print(jsonFilePath2)
        print("- - - - - -")
        print(jsonFilePath2.path)
        print("- - - - - - -")
        print(jsonFilePath2.relativeString)
        print("- - - - - -")
        print(jsonFilePath2.relativePath)
        print("- - - - - - -")

        
        // creating a .json file in the Documents folder
        if !fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("File created ")
                
            } else {
                print("Couldn't create file for some reason")
            }
        } else {
            print("File already exists")
        }
    //file creation ends
        
        
//     self.showLoadingMessage("Loading. . .", dismiss: false)
        progressBarDisplayer("Loading. . .", true)
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
            let url = getUrl()
            _ = self.performLoadDataRequestWithURL(url, currentPage: Page)
        }else {
            print("Internet connection FAILED")
            do {
                var readString: String
                readString = try NSString(contentsOfFile: jsonFilePath2.path, encoding: String.Encoding.utf8.rawValue) as String
                print("readString=\(readString)")
                self.showDataWhenOffline(readString)
            } catch let error as NSError {
                print(error.description)
            }
//            let alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
//            alert.show()
        }
// Get Data from the server to display it on table
        
//        let url = getUrl()
//        let urlData = self.performLoadDataRequestWithURL(url, currentPage: Page)


//// Saving JSON DATA
////        let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first!
//        let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
//        
//        let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("dashboard.json")
//        let fileManager = NSFileManager.defaultManager()
//        var isDirectory: ObjCBool = false
//
//        // creating a .json file in the Documents folder
//        if !fileManager.fileExistsAtPath(jsonFilePath.absoluteString, isDirectory: &isDirectory) {
//            let created = fileManager.createFileAtPath(jsonFilePath.absoluteString, contents: nil, attributes: nil)
//            if created {
//                print("File created ")
//                
//                // Get Data from server
////                let url = getUrl()
////                self.performLoadDataRequestWithURL(url, currentPage: Page)
//                
//            } else {
//                print("Couldn't create file for some reason")
//            }
//        } else {
//            print("File already exists")
//        }

        
        //Append data to string array to display on tableview
        
//        tableCellData.append(items(A: UIColor.greenColor(), B: "S184453", C: "Nishat Apareal", D: "woowen button"))
//        tableCellData.append(items(A: UIColor.redColor(), B: "S13458", C: "Style Textile", D: "knits"))
//        tableCellData.append(items(A: UIColor.greenColor(), B: "S184453", C: "Nishat Apareal", D: "woowen button"))
//        tableCellData.append(items(A: UIColor.redColor(), B: "S13458", C: "Style Textile", D: "knits"))

        
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Selected Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)

    }
    func popupMessage(_ message:String){
        let alertView = UIAlertController(title: nil , message: message, preferredStyle: .alert)
        let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertView.addAction(ok_action)
        self.present(alertView, animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("defect tab is appeared")
        
    }
//    func dismissViewController1AndPresentViewController2() {
//        self.dismissViewControllerAnimated(false, completion: { () -> Void in
//            let vc2: KPIViewController = self.storyboard?.instantiateViewControllerWithIdentifier("6") as! KPIViewController
//            self.presentViewController(vc2, animated: true, completion: nil)
//        })
//    }
    
    
    
    func showDataWhenOffline(_ savedData:String?){
        
//        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        print("Hiiiiiii")
        print("savedDataString=\(savedData)")
        if savedData != nil && savedData != "" {
            
            self.dictionary = self.parseJSON(savedData!)
            print("\(self.dictionary)")
            if self.dictionary.count > 0{
                let All_Audits = self.dictionary["Audits"]! as! [[String : AnyObject]]
                if All_Audits.count > 0{
                    print("Total Audits = \((self.dictionary["Audits"]! as AnyObject).count)")
                    
                    //initially set NO to all elements in selectedArrowButton i.e means there is no cell that is currently selected
                    
                    for _ in 0 ..< (self.dictionary["Audits"]! as AnyObject).count //yourTableSize = how many rows u got
                    {
                        selectedArrowButton.add("NO")
                    }
                    print("whenOffline =",selectedArrowButton.count)
                    print("whenOFFLINE =",selectedArrowButton)

                    
                    self.totalRowsInTable = (self.dictionary["Audits"]! as AnyObject).count
                    print("All_Audits count = \(All_Audits.count)")
                    for Audit in All_Audits{
                        print(Audit)
                        let AuditCodeName = Audit["AuditCode"]! as! String
                        var AuditStageColor = ""
                        if Audit["Color"] is String {
                            AuditStageColor = Audit["Color"]! as! String
                            
                        }
                       // let AuditStageColor = Audit["Color"]! as! String

                        let vendorName = Audit["Vendor"]! as! String
                        let AuditStageCodeName = Audit["AuditStageCode"]! as! String
                        let ReportTypeName = Audit["ReportType"]! as! String
                        let AuditResultName = Audit["AuditResult"]! as! String
                        
                        var color: UIColor!
                        if AuditResultName == "P" || AuditResultName == "A" || AuditResultName == "B"{
                            
                            color = UIColor(colorLiteralRed: 114.0/255, green: 161.0/255, blue: 14.0/255, alpha: 1.0)
                            
                        }else if AuditResultName == "F" || AuditResultName == "C"{
                            
                            color = UIColor(colorLiteralRed: 220.0/255, green: 0.0/255, blue: 8.0/255, alpha: 1.0)
                            
                        }else if AuditResultName == "H" || AuditResultName == "R"{
                            
                            color = UIColor.orange
                        }
                        //- - - - Expanded cell data management (LEFT)- - - - - - - - -
                        
                        let noOfDefects:String = "DEFECTS: "+(Audit["Defects"]! as! NSNumber).stringValue
                        print(noOfDefects)
                        let sampleSize = "SAMPLE SIZE: "+(Audit["SampleSize"]! as! NSNumber).stringValue
                        let dhu = "DHU: \((Audit["Dhu"]! as! NSNumber).stringValue)%"
                        print(dhu)
                        let auditResult_label = Audit["AuditResult"]! as! String
                        
                        // - - - - -Expanded cell PICTURE
                        let pictureURL = Audit["Picture"]! as! String
                        print("pictureURL",pictureURL)
                        
                        //- - - - Expanded cell data management (RIGHT)- - - - - - - - -
                        let styleNo = "Style No:  "+(Audit["Style"]! as! String)
                        print(styleNo)
                        let brandName = "BRAND:  "+(Audit["Brand"]! as! String)
                        print(brandName)
                        let seasonName:String!
                        if (Audit["Season"]!.boolValue != false){
                            seasonName = "SEASON:  "+(Audit["Season"]! as! String)
                            //                    print(season)
                        }else{
                            seasonName = "SEASON:  "
                        }
                        //                let seasonName = "SEASON:  "+(Audit["Season"]! as! String)
                        print(seasonName)
                        let poNumber = "PO NO:  "+(Audit["Po"]! as! String)
                        print(poNumber)
                        let etd = "ETD REQUIRED:  "+(Audit["EtdRequired"]! as! String)
                        print(etd)
                        var auditStageName = ""
                        if(Audit["AuditStage"] is String){
                        auditStageName = "AUDIT STAGE:  "+(Audit["AuditStage"]! as! String)
                        }
                        print(auditStageName)
                        
                        self.tableCellData.append(items(A: color, B: AuditCodeName, C: vendorName, D: AuditStageCodeName, E: ReportTypeName, F: noOfDefects, G: sampleSize, H: dhu, I: auditResult_label,J: styleNo,K: brandName,L: seasonName,M: poNumber,N: etd,O: auditStageName,P: pictureURL,Q:AuditStageColor))
                    }
                    
                    if("\(self.dictionary["Status"]!)" == "OK" && self.tableCellData.count > 0){
                        print("TableCellData-count = \(self.tableCellData.count)")
                        self.tableView.dataSource = self
                        self.tableView.delegate = self
                        DispatchQueue.main.async(execute: { () -> Void in
                            
                            self.activityIndicator.stopAnimating()
                            self.messageFrame.removeFromSuperview()
                            self.tableView.reloadData()
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        })
                    }
                } // if All Audits count < 0
                else{
//                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                    print("All Audits Count is less than zero")
                }
            }else{
//                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
                print("Data count is zero")
            }
        }else{
//            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            self.activityIndicator.stopAnimating()
            self.messageFrame.removeFromSuperview()
            showLoadingMessage("NO data found!! \n Make sure the Internet is connected.")
            print("NO data found!!")
        }
    }
    func getUrl() -> URL {
        let toEscape = "http://app.3-tree.com/quonda/audits.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
//    var bodyData:String?
//     var task:NSURLSessionDataTask!
    
    func showLoadingMessage(_ message:String){
        
       let strLabel = UILabel(frame: CGRect(x: view.frame.midX - 50, y: view.frame.midY - 50, width: 200, height: 50))  // x: 50, y: 0, width: 200, height: 50
        strLabel.text = message
        strLabel.textColor = UIColor.white
       let messageFrame = UIView(frame: CGRect(x: 0, y: 69 , width: view.frame.width, height: view.frame.height - 118))  //x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50
        //        messageFrame.layer.cornerRadius = 15
        messageFrame.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        messageFrame.addSubview(strLabel)
        view.addSubview(messageFrame)

        let duration:UInt64 = 3; // duration in seconds
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(duration*NSEC_PER_SEC)) / Double(NSEC_PER_SEC)) { () -> Void in
            messageFrame.removeFromSuperview()
        }
    }
    
    
    
//    func showLoadingMessage(message:String, dismiss:Bool){
//        
//        let alert:UIAlertController = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.Alert)
//        if dismiss == false{
//            self.presentViewController(alert, animated: true, completion: nil)
//           
//        }else{
//             alert.dismissViewControllerAnimated(true, completion: nil)
//        }
//    }

 
    func performLoadDataRequestWithURL(_ url: URL, currentPage: Int) -> String? {
        
//        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
//        progressBarDisplayer("Loading. . .", true)
        
        self.bodyData = self.bodyData!+"&PageId=\(currentPage)"
//        self.bodyData = "User=\(defaults.objectForKey("userid")as! String)&Status=Any&PageId=\(currentPage)"

        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = self.bodyData!.data(using: String.Encoding.utf8)
//        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()){
//            response, data, error in
        let session = URLSession.shared
       
        task = session.dataTask(with: request as URLRequest) { (data, response, error) in
           
            print("\(response)")
            if (error != nil) {
                print(error!)
//                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
                
                if self.isSearchRequest{
                    let alertView = UIAlertController(title: "No Data Found" , message: "\(error!.localizedDescription) \n To Dismiss Press OK", preferredStyle: .alert)
                    let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction)-> Void in
                        
                        if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                            
//                            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                            self.dismiss(animated: true, completion: nil)
                            self.present(resultController, animated: true, completion: nil)
                        }})
                    alertView.addAction(ok_action)
                    self.present(alertView, animated: true, completion: nil)
                    self.isSearchRequest = false
                }
                else{
                    
//                    self.showLoadingMessage("Loading. . .", dismiss: true)
                   
                    DispatchQueue.main.async(execute: {
                        self.activityIndicator.stopAnimating()
                        self.messageFrame.removeFromSuperview()
                    })
                    
                    let alertView = UIAlertController(title: "No Data Found" , message: "\(error?.localizedDescription)", preferredStyle: .alert)
                    let Retry_action: UIAlertAction = UIAlertAction(title: "Retry", style: .default, handler: { (UIAlertAction) -> Void in
                        
//                        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                        if self.Page <= 1{
                            self.progressBarDisplayer("Loading. . .", true)
                        }else{
                            self.NewCellsProgressBar(true)
                        }
                        self.performLoadDataRequestWithURL(self.getUrl(), currentPage: self.Page)
                    })
                   let cancelAction:UIAlertAction = UIAlertAction(title: "cancel", style: .default, handler: nil)
                    alertView.addAction(cancelAction)
                    alertView.addAction(Retry_action)
//                    self.presentViewController(alertView, animated: true, completion: nil)
                }
            }
            
            if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                // - - - - - - write this JSON data to file- - - - - - - -
                print("created : \(self.created)")
//                if self.created == true{
                if self.fileManager.fileExists(atPath: self.jsonFilePath2.absoluteString, isDirectory: &self.isDirectory) {
                    do{
                        try json.write(toFile: self.jsonFilePath2.path, atomically: true, encoding: String.Encoding.utf8)
                       print("reading from file..")

                    } catch let error as NSError {
                        print("JSON data couldn't written to file // File not exist")
                        print(error.description)
                    }
                }
                print(json)
                self.dictionary = self.parseJSON(json)
//            }
            print(json)
//            self.dictionary = self.parseJSON(json)
            print("\(self.dictionary)")
            let All_Audits = self.dictionary["Audits"]! as! [[String : AnyObject]]
//            print("\((dictionary["Audits"]![0]!["AuditCode"]!))")
            
//            print("\((dictionary["Audits"]!.valueForKey("0")!.valueForKey("AuditCode")!))")  // dictionary["0"]?.valueForKey("name")!
            
                print("Total Audits = \((self.dictionary["Audits"]! as AnyObject).count)")
                
                if (self.dictionary["Audits"] as AnyObject).count < 1 {
                    
//                    self.activityIndicator.stopAnimating()
//                    self.messageFrame.removeFromSuperview()
                    
                    if self.isSearchRequest{
                        let alertView = UIAlertController(title: "No Data Found" , message: "To Dismiss Press OK", preferredStyle: .alert)
                        let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction)-> Void in
                            
                            if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
//                                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                                self.dismiss(animated: true, completion: nil)
                                self.present(resultController, animated: true, completion: nil)
                            }})
                        alertView.addAction(ok_action)
                        self.present(alertView, animated: true, completion: nil)
                        self.isSearchRequest = false
                    }
                    else{
                        
//                        self.showLoadingMessage("Loading. . .", dismiss: true)
                        
                        DispatchQueue.main.async(execute: {
                            self.activityIndicator.stopAnimating()
                            self.messageFrame.removeFromSuperview()
                        })
                        
                        let alertView = UIAlertController(title: nil , message: "No Data Found", preferredStyle: .alert)
                        let Retry_action: UIAlertAction = UIAlertAction(title: "Retry", style: .default, handler: { (UIAlertAction) -> Void in
                            
//                            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                           
                            if self.Page <= 1{
                                self.progressBarDisplayer("Loading. . .", true)
                            }else{
                                self.NewCellsProgressBar(true)
                            }
                            self.performLoadDataRequestWithURL(self.getUrl(), currentPage: self.Page)
                        })
                        let cancelAction:UIAlertAction = UIAlertAction(title: "cancel", style: .default, handler: nil)
                        
                        alertView.addAction(cancelAction)
                        alertView.addAction(Retry_action)
                        self.present(alertView, animated: true, completion: nil)
                    }
                }else{
                    
                    self.totalRowsInTable = (self.dictionary["Audits"]! as AnyObject).count
                    print("All_Audits count = \(All_Audits.count)")

                    for i in 0 ..< ((self.dictionary["Audits"]! as AnyObject).count) //yourTableSize = how many rows u got
                    {
                        selectedArrowButton.add("NO")
                    }
                    print(selectedArrowButton.count)
                    print(selectedArrowButton)
                    print("All_Audits count = \(All_Audits.count)")
                    for Audit in All_Audits{
                        print(Audit)
                        let AuditCodeName = Audit["AuditCode"]! as! String
                        var AuditStageColor = ""
                        if  Audit["Color"] is String {
                            AuditStageColor = Audit["Color"]! as! String

                        }
                        let vendorName = Audit["Vendor"]! as! String
                        let AuditStageCodeName = Audit["AuditStageCode"]! as! String
                        let ReportTypeName = Audit["ReportType"]! as! String
                        let AuditResultName = Audit["AuditResult"]! as! String
                        
                       // var color: UIColor!
                        var color: UIColor = UIColor.lightGray

                        if AuditResultName == "P" || AuditResultName == "A" || AuditResultName == "B"{
                            
                            //                    var greencolor = UIColor.greenColor()
                            //                    color = UIColor.greenColor()
                            //                     color = greencolor.adjust(0.9, green: 1.0, blue: 0.0, alpha: 1.0)
                            color = UIColor(colorLiteralRed: 114.0/255, green: 161.0/255, blue: 14.0/255, alpha: 1.0)
                            
                        }else if AuditResultName == "F" || AuditResultName == "C"{
                            
                            //                    color = UIColor.redColor()
                            color = UIColor(colorLiteralRed: 220.0/255, green: 0.0/255, blue: 8.0/255, alpha: 1.0)
                            
                        }else if AuditResultName == "H" || AuditResultName == "R"{
                            
                            color = UIColor.orange
                        }
                        //- - - - Expanded cell data management (LEFT)- - - - - - - - -
                        
                        let noOfDefects:String = "DEFECTS: "+(Audit["Defects"]! as! NSNumber).stringValue
                        print(noOfDefects)
                        let sampleSize = "SAMPLE SIZE: "+(Audit["SampleSize"]! as! NSNumber).stringValue
                        let dhu = "DHU: \((Audit["Dhu"]! as! NSNumber).stringValue)%"
                        print(dhu)
                        let auditResult_label = Audit["AuditResult"]! as! String
                        
                        // - - - - -Expanded cell PICTURE
                        let pictureURL = Audit["Picture"]! as! String
                        print("pictureURL",pictureURL)
                        
                        //- - - - Expanded cell data management (RIGHT)- - - - - - - - -
                        let styleNo = "Style No:  "+(Audit["Style"]! as! String)
                        print(styleNo)
                        let brandName = "BRAND:  "+(Audit["Brand"]! as! String)
                        print(brandName)
                        let seasonName:String!
                        if (Audit["Season"]!.boolValue != false){
                            seasonName = "SEASON:  "+(Audit["Season"]! as! String)
                            //                    print(season)
                        }else{
                            seasonName = "SEASON:  "
                        }
                        //                let seasonName = "SEASON:  "+(Audit["Season"]! as! String)
                        print(seasonName)
                        let poNumber = "PO NO:  "+(Audit["Po"]! as! String)
                        print(poNumber)
                        let etd = "ETD REQUIRED:  "+(Audit["EtdRequired"]! as! String)
                        print(etd)
                        var auditStageName = ""
                        if(Audit["AuditStage"] is String){
                            auditStageName = "AUDIT STAGE:  "+(Audit["AuditStage"]! as! String)
                        }
//                        let auditStageName = "AUDIT STAGE:  "+(Audit["AuditStage"]! as! String)
//                        print(auditStageName)
                        
                        //                print("\(AuditCodeName)")
                        //                print("\(vendorName)")
                        //                print("\(ReportTypeName)")
                        
                        self.tableCellData.append(items(A: color, B: AuditCodeName, C: vendorName, D: AuditStageCodeName, E: ReportTypeName, F: noOfDefects, G: sampleSize, H: dhu, I: auditResult_label,J: styleNo,K: brandName,L: seasonName,M: poNumber,N: etd,O: auditStageName,P: pictureURL,Q:AuditStageColor))
                        
                    }
                    
                    if("\(self.dictionary["Status"]!)" == "OK" && self.tableCellData.count > 0){
                        print("TableCellData-count = \(self.tableCellData.count)")
                        self.tableView.dataSource = self
                        self.tableView.delegate = self
                        DispatchQueue.main.async(execute: { () -> Void in
                           
//                            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
//                            self.showLoadingMessage("Loading. . .", dismiss: true)
                            
                            self.activityIndicator.stopAnimating()
                            self.messageFrame.removeFromSuperview()
                            
                            self.tableView.reloadData()
                        })
                        //                if currentPage == 1{
//                        self.activityIndicator.stopAnimating()
//                        self.messageFrame.removeFromSuperview()
                        //                }
                    }
                } //else part of count<1
            }//if data != nil
            else{
                
//                self.showLoadingMessage("Loading. . .", dismiss: true)
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
                
                
                let alertView = UIAlertController(title: "No Data Found" , message: "Press OK to retry", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    if self.Page <= 1{
                        self.progressBarDisplayer("Loading. . .", true)
                    }else{
                        self.NewCellsProgressBar(true)
                    }
//                    self.progressBarDisplayer("Reloading. . .", true)
                    self.performLoadDataRequestWithURL(self.getUrl(), currentPage: self.Page)
                })
                let NO_action: UIAlertAction = UIAlertAction(title: "NO", style: .default, handler:{ (UIAlertAction) -> Void in
                    DispatchQueue.main.async(execute: {
                        self.activityIndicator.stopAnimating()
                        self.messageFrame.removeFromSuperview()
                    })
                    
                })
                
                alertView.addAction(ok_action)
                alertView.addAction(NO_action)
                
                self.present(alertView, animated: true, completion: nil)
                print("return data is nil")

            }
        }
        task!.resume()
        print("task started")
        return json
    }
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
    
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
//            defaults.removeObjectForKey("userid")

            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)

            break
        case 2:
            print("self.parentViewController = \(self.presentingViewController)")
            
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            //cancel the request of Dashboard Data
            if task != nil {
                print("task request cancelled")
            task!.cancel()
            }
            print("task is not nil")
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
//
            break
//        case 4:
//            print("dashboard case 4:")
////            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
////
////                presentViewController(resultController, animated: true, completion: nil)
////            }
//      
////            let selectedImage:UIImage = UIImage(named: "Selected QAReport")!
////            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
//            
//            break
        case 5:
            if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }else{
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }
            //cancel the request of Dashboard Data
            if task != nil {
                print("task request cancelled")
                task!.cancel()
            }

            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }

            //cancel the request of Dashboard Data
            if task != nil {
                print("task request cancelled")
                task!.cancel()
            }

            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            //cancel the request of Dashboard Data
            if task != nil {
                print("task request cancelled")
                task!.cancel()
            }

            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            //cancel the request of Dashboard Data
            if task != nil {
                print("task request cancelled")
                task!.cancel()
            }

            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_selected_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break

        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        return (self.dictionary.valueForKey("Audits")!.count)
        return self.tableCellData.count
    }
    
//    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
//
////        if self.Page   {
//            print("willDisplayCell called")
//            print(self.dictionary.valueForKey("Audits")!.count-1)
//            print(indexPath.row)
//            print("//////////////")
//            if indexPath.row+1 == self.dictionary.valueForKey("Audits")!.count {
//                self.loadNewPageRows(++self.Page)
//            }
////        }
//        
//    }
    
    func loadNewPageRows(_ page: Int){
        print("pageNo:")
        print(page)
        print("hello......hello.....")
        let url = getUrl()
        NewCellsProgressBar(true)
        self.performLoadDataRequestWithURL(url, currentPage: page)

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(indexPath.row)
        let cell:DashboardCell = self.tableView.dequeueReusableCell(withIdentifier: "cell") as! DashboardCell
        
        
        if (selectedArrowButton.object(at: indexPath.row) as AnyObject).isEqual(to: "NO"){
            cell.acessoryButton.isSelected = false
            
        }else if (selectedArrowButton.object(at: indexPath.row) as AnyObject).isEqual(to: "YES"){
            cell.acessoryButton.isSelected = true
            
        }
        
        if cell.acessoryButton.isSelected == true{
            cell.arrowButton.image = UIImage(named: "down_arrow")
        }else{
            cell.arrowButton.image = UIImage(named: "right_arrow")
        }
       // if(self.tableCellData.count > 25){
        if indexPath.row == self.tableCellData.count - 1 && !(self.tableCellData.count < 25) {
            if !(self.tableCellData.count < 25){
            
                self.loadNewPageRows(1+self.Page)
            }
        }
            
        else{
    
            // set Row color
            if indexPath.row % 2 == 0{
                cell.backgroundColor = UIColor(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1.0)//lightGrayColor()
            }
            else{
                cell.backgroundColor = UIColor(red: 240/255.0, green: 240/255.0, blue: 240/255.0, alpha: 1.0)
            }
            
            // set cell DATA
            print(tableCellData)
            cell.color_code.backgroundColor = tableCellData[indexPath.row].A
            cell.first_column.text = tableCellData[indexPath.row].B
            cell.second_column.text = tableCellData[indexPath.row].C
            cell.audit_Stage_Code.text = "{" + tableCellData[indexPath.row].D + "}"
            cell.third_column.text =  tableCellData[indexPath.row].E
            if(tableCellData[indexPath.row].D == "F"){
            cell.audit_Stage_Code.textColor = hexStringToUIColor(tableCellData[indexPath.row].Q)
            }
            
            //- - - -Expanded cell data (LEFT)- - - -
            
            cell.expandedCell_LeftView.backgroundColor = tableCellData[indexPath.row].A
            cell.defects.text = tableCellData[indexPath.row].F
            cell.sampleSize.text = tableCellData[indexPath.row].G
            cell.DHU.text = tableCellData[indexPath.row].H
            cell.auditResult_Label.text = tableCellData[indexPath.row].I
            
            //        // - - - - - Expanded cell PICTURE- - - - -
            // Start by setting the cell's image to a static file
            // Without this, we will end up without an image view!
            cell.expandedCell_Photo.image = UIImage(named: "default.jpg")
            
            //        if let url = NSURL(string: tableCellData[indexPath.row].P){
            //            if let data = NSData(contentsOfURL: url) {
            //                cell.expandedCell_Photo.image = UIImage(data: data)
            //            }
            //        }
            //- - - -Expanded cell data (RIGHT)- - - -
            
            cell.styleNo.text = tableCellData[indexPath.row].J
            cell.brand.text = tableCellData[indexPath.row].K
            cell.season.text = tableCellData[indexPath.row].L
            cell.POno.text = tableCellData[indexPath.row].M
            cell.etd.text = tableCellData[indexPath.row].N
            cell.auditStage.text = tableCellData[indexPath.row].O
            if cell.auditStage.text == "Final"{
                cell.auditStage.textColor = UIColor.red
            }
            // - - - - Loading images- - - - - - -
            
            // - - - - Loading images- - - - - - -
            //        if !tableView.decelerating{
            //
            ////        // Start by setting the cell's image to a static file
            ////        // Without this, we will end up without an image view!
            ////        cell.expandedCell_Photo.image = UIImage(named: "Blank52")
            ///*
            //        // If this image is already cached, don't re-download
            //        if let img = imageCache[tableCellData[indexPath.row].P] {
            //            cell.expandedCell_Photo.image = img
            //        }
            //        else {
            //            // The image isn't cached, download the img data
            //            // We should perform this in a background thread
            //            print("url string:")
            //            print(self.tableCellData[indexPath.row].P)
            //            let request: NSURLRequest = NSURLRequest(URL: NSURL(string: self.tableCellData[indexPath.row].P)!)
            //            let mainQueue = NSOperationQueue.mainQueue()
            //            NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
            //                if error == nil {
            //                    // Convert the downloaded data in to a UIImage object
            //                    let image = UIImage(data: data!)
            //                    // Store the image in to our cache
            //                    self.imageCache[self.tableCellData[indexPath.row].P] = image
            //                    // Update the cell
            ////                    dispatch_async(dispatch_get_main_queue(), {
            //                        if let cellToUpdate: DashboardCell = self.tableView.cellForRowAtIndexPath(indexPath) as! DashboardCell {
            //                            cellToUpdate.expandedCell_Photo.image = image
            //                        }
            ////                    })
            //                }
            //                else {
            //                    print("Error: \(error!.localizedDescription)")
            //                }
            //            })
            //        }  */
            //    }
            
        }// if check of loadingCell
        return cell
    }
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let destinationVC = storyboard!.instantiateViewControllerWithIdentifier("6") as? KPIViewController
        let myCell = tableView.cellForRow(at: indexPath) as! DashboardCell
        AuditCodeOfDashboardCell =  myCell.first_column.text
        print(myCell.first_column.text)
        print(AuditCodeOfDashboardCell)
        print(self.presentingViewController)
        print(self.presentedViewController)
        print(UIApplication.shared.keyWindow?.rootViewController)
        let rootVC = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController
        print(rootVC)
//        if rootVC?.isKindOfClass(<#T##aClass: AnyClass##AnyClass#>)
//        if currentPressentingViewController != nil{
//            currentPressentingViewController.presentViewController(destinationVC!, animated: true, completion: nil)
//        }else{
//         self.presentViewController(destinationVC!, animated: false, completion: nil)
//        }
        
//        if let delegate = UIApplication.sharedApplication().delegate {
//            delegate.window!!.rootViewController?.presentViewController(viewController, animated: true, completion: { () -> Void in
//                // optional completion code
//            })
//        }
//        print("You selected cell #\(indexPath.row)")
////        self.dismissViewControllerAnimated(false, completion: nil)
////        var cellTaped = tableView.cellForRowAtIndexPath(indexPath) as! DashboardCell
        self.performSegue(withIdentifier: "kpi", sender: self)
//
    }
  

    @IBAction func acessoryButtonTaped(_ sender: AnyObject) {
        
        let button = sender as! UIButton
        let view = button.superview!
        self.cell = view.superview as! DashboardCell
        
   // change arrow button to down
        
        let indexPath = tableView.indexPath(for: cell)
        
        if indexPath != nil && previousTappedButtonIndexPath == indexPath{
            selectedArrowButton.replaceObject(at: indexPath!.row, with: "NO")

            cell.arrowButton.image = UIImage(named: "right_arrow")
            previousTappedButtonIndexPath = nil
            previousButtonTag = nil
        }
        else if indexPath != nil && previousTappedButtonIndexPath != indexPath{
            if (previousTappedButtonIndexPath != nil && previousButtonTag != nil){
                selectedArrowButton.replaceObject(at: previousButtonTag, with: "NO")

                if let Pcell = self.tableView.cellForRow(at: previousTappedButtonIndexPath!) as? DashboardCell{
                    Pcell.arrowButton.image = UIImage(named: "right_arrow")
            }
            }
            selectedArrowButton.replaceObject(at: indexPath!.row, with: "YES")
            previousTappedButtonIndexPath = indexPath
            previousButtonTag = indexPath?.row
            // - - - - - - - - - - - - -
            // - - - - Loading images- - - - - - - 
            
            // If this image is already cached, don't re-download
            if let img = imageCache[tableCellData[indexPath!.row].P] {
                cell.expandedCell_Photo.image = img
            }
            else {
                // The image isn't cached, download the img data
                // We should perform this in a background thread
                print("url string:\(self.tableCellData[indexPath!.row].P)")
                print(self.tableCellData[indexPath!.row].P)
                if("\(self.tableCellData[indexPath!.row].P)" != "" ){
                let request: URLRequest = URLRequest(url: URL(string: self.tableCellData[indexPath!.row].P)!)
                let mainQueue = OperationQueue.main
                NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
                    if error == nil {
                        // Convert the downloaded data in to a UIImage object
                        let image = UIImage(data: data!)
                        // Store the image in to our cache
                        self.imageCache[self.tableCellData[indexPath!.row].P] = image
                        // Update the cell
                        //                    dispatch_async(dispatch_get_main_queue(), {
                        if let cellToUpdate: DashboardCell = self.tableView.cellForRow(at: indexPath!) as? DashboardCell {
                            cellToUpdate.expandedCell_Photo.image = image
                        }
                        //                    })
                    }
                    else {
                        print("Error: \(error!.localizedDescription)")
                    }
                })
                }
            }
            // - - - -END - -Loading images- - - - - - - 
            // - - - - - - - - - - - - --
                cell.arrowButton.image = UIImage(named: "down_arrow")
        }
        
        
        self.selectedIndexPath = indexPath
        rowpath = (indexPath?.row)!
        print("Row Button sellected: \(rowpath)")
        self.tableView.beginUpdates()
//        self.tableView.cellForRowAtIndexPath(indexPath!)
//        self.tableView.reloadRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.None)
        self.tableView.endUpdates()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        print(indexPath.row)
        if indexPath == selectedIndexPath{
            if(cell.arrowButton.image == UIImage(named: "down_arrow")){
                return 150
            }else{
                let height = heightForView(text:self.tableCellData[indexPath.row].C, font: UIFont.systemFont(ofSize: 11), width: ScreenSize.SCREEN_WIDTH-16)
                return 35 + height
                //return 35
            }
        }else{
            let height = heightForView(text:self.tableCellData[indexPath.row].C, font: UIFont.systemFont(ofSize: 11), width: ScreenSize.SCREEN_WIDTH-16)
            return 35 + height
        }
    }

    func progressBarDisplayer(_ msg:String, _ indicator:Bool ) {
        print(msg)
        strLabel = UILabel(frame: CGRect(x: view.frame.midX - 40, y: view.frame.midY - 50, width: 200, height: 50))  // x: 50, y: 0, width: 200, height: 50
        strLabel.text = msg
        strLabel.textColor = UIColor.white
        messageFrame = UIView(frame: CGRect(x: 0, y: 69 , width: view.frame.width, height: view.frame.height - 118))  //x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50
//        messageFrame.layer.cornerRadius = 15
        messageFrame.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        if indicator {
            activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
            activityIndicator.frame = CGRect(x: view.frame.midX - 80, y: view.frame.midY - 50, width: 50, height: 50)  // x: 0, y: 0, width: 50, height: 50
            activityIndicator.startAnimating()
            messageFrame.addSubview(activityIndicator)
        }
        messageFrame.addSubview(strLabel)
        view.addSubview(messageFrame)
    }
    
    func NewCellsProgressBar(_ indicator:Bool ) {
 
        strLabel = UILabel(frame: CGRect(x: 170, y: 0, width: 200, height: 50))
        strLabel.text = "loading. . ."
        strLabel.textColor = UIColor.white
        messageFrame = UIView(frame: CGRect(x: 0, y: view.frame.height - 97, width: view.frame.width, height: 50))
        messageFrame.layer.cornerRadius = 5
        messageFrame.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        if indicator {
            activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
            activityIndicator.frame = CGRect(x: 120, y: 0, width: 50, height: 50)
            activityIndicator.startAnimating()
            messageFrame.addSubview(activityIndicator)
        }
        messageFrame.addSubview(strLabel)
        view.addSubview(messageFrame)
    }
    
    
    
    
    
    // NSLocation related functions
    
    func sendAuditorLocation() {
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        print("loaction config called")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        print("coordinate =",locations.first?.coordinate)
        
        let latitudeLabel = String(locations.first!.coordinate.latitude)
        let longitudeLabel = String(locations.first!.coordinate.longitude)
        print("latitudeLabel =",latitudeLabel)
        print("longitudeLabel =",longitudeLabel)
        
        self.saveLatitudeLongitude(latitudeLabel, longitudeLabel: longitudeLabel)
        
//        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error) -> Void in
//            if (error != nil) {
//                print("ERROR:" + error!.localizedDescription)
//                return
//            }
//            print("placemarks!.count = ",placemarks!.count)
//            
//            if placemarks!.count > 0 {
//                let pm = placemarks![0]
//                self.saveLatitudeLongitude(pm)
//                print("called after saveLatitude")
//            } else {
//                print("Problem with the data received from geocoder")
//            }
//            
//        })
    }
    
    
    
    func saveLatitudeLongitude(_ latitudeLabel:String, longitudeLabel:String){ //(placemark: CLPlacemark) {
        
       // var latitudeLabel:String = String(placemark.location!.coordinate.latitude)
       // var longitudeLabel:String = String(placemark.location!.coordinate.longitude)
        //        let lat = String(placemark.location!.coordinate.latitude)
        print("lat =", latitudeLabel)
        print("long =", longitudeLabel)
        
        var bodyData = "User=\(defaults.object(forKey: "userid")as! String)&Latitude=\(latitudeLabel)&Longitude=\(longitudeLabel)"
        print("bodydata for location =",bodyData)
        
        let request: NSMutableURLRequest = NSMutableURLRequest(url: URL(string: "http://app.3-tree.com/location.php")!)
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = bodyData.data(using: String.Encoding.utf8)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
//            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
//                print("error")
//                return
//            }
            if (error != nil) {
                print(error)
                
            }
            else if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                
                print(json)
                
                
                var dictionary:NSDictionary = self.parseJSON(json)
                
                print("information from location Dictionary =\(dictionary)")
                
                if("\(dictionary["Status"]!)" == "OK"){
                    
                    print("Successfuly sent Coordinates)")
                    
                }else{
                    print("There is an error while sending Coordinates to the server.")
                }
                
            }//if data != nil
            else{
                print("return data is nil")
            }
            
        }) 
        
        task.resume()
        
        
        self.locationManager.stopUpdatingLocation()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error:" + error.localizedDescription)
    }
    
    
    // background location functions
    
    @objc func reinstateBackgroundTask() {
        if updateLocationTimer != nil && (backgroundLocationTask == UIBackgroundTaskInvalid) {
            registerBackgroundTask()
        }
    }
    func registerBackgroundTask() {
        backgroundLocationTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            self.endBackgroundTask()
        })
            //UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler {
//            [unowned self] in
//            self.endBackgroundTask()
//        }
//        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTask() {
        NSLog("backgroundLocationTask task ended.")
        UIApplication.shared.endBackgroundTask(backgroundLocationTask)
        backgroundLocationTask = UIBackgroundTaskInvalid
    }
    //To display notifications when app is running  inforeground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
    }
    func scheduleNotifications(){
        if #available(iOS 10.0, *)
        {
            //Setting content of the notification
            let content = UNMutableNotificationContent()
            content.title = "hello"
            content.body = "notification pooped out"
            //Setting time for notification trigger
            let date = Date(timeIntervalSinceNow: 2)
            let dateCompenents = Calendar.current.dateComponents([.year,.month,.day                    ,.hour,.minute,.second], from: date)
            
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateCompenents, repeats: false)
            //Adding Request
            let request = UNNotificationRequest(identifier: "timerdone", content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        }
    }

} // end of class





