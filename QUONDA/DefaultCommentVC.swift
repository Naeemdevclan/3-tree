//
//  DefaultCommentVC.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 01/01/2017.
//  Copyright © 2017 Muhammad Naeem. All rights reserved.
//

import UIKit

class DefaultCommentVC: UIViewController, UITabBarDelegate, UITextViewDelegate, UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!

    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    
    
    
    @IBOutlet weak var AuditCode: UILabel!
    @IBOutlet weak var AuditStage: UILabel!
    
    
    @IBOutlet weak var ShipQuantity: UITextField!
    
    @IBOutlet weak var ReScreenQuantity: UITextField!
    
    @IBOutlet weak var Status: UISwitch!
    
    var statusActive = false
    var AuditResult: String!
    
    @IBOutlet weak var ProductionStatus: UITextField!
    
    @IBOutlet weak var Comments: UITextView!
    
    @IBOutlet weak var finishBtn: UIButton!
    
    var AuditCodein: String!
    var AuditStagein: String!
    var AuditStageColorin : UIColor!
    
    var placeholderLabel:UILabel!
    
    var created:Bool!
    var isDirectory: ObjCBool = false
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    let fileManager = FileManager.default
    
    var bodyData:String!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Selected Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)

        
        
        
        // add placeholder text in comments_Arcadia TextView
        
        placeholderLabel = UILabel()
        placeholderLabel.text = "QA Comments"
        placeholderLabel.font = UIFont.italicSystemFont(ofSize: Comments.font!.pointSize)
        placeholderLabel.sizeToFit()
        Comments.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: Comments.font!.pointSize / 2)
        placeholderLabel.textColor = UIColor(white: 0, alpha: 0.3)
        placeholderLabel.isHidden = !Comments.text.isEmpty
        

        self.ShipQuantity.delegate = self
        self.ReScreenQuantity.delegate = self
        self.ProductionStatus.delegate = self
        self.Comments.delegate = self
        
        self.ProductionStatus.layer.borderWidth = 1
        self.ProductionStatus.layer.borderColor = UIColor.lightGray.cgColor
        
        self.Comments.layer.borderWidth = 1
        self.Comments.layer.borderColor = UIColor.lightGray.cgColor
        
        self.finishBtn.layer.borderWidth = 1
        self.finishBtn.layer.borderColor = UIColor.gray.cgColor
        self.finishBtn.layer.cornerRadius = 8
        
        Status.addTarget(self, action: #selector(DefaultCommentVC.switchIsChanged(_:)), for: UIControlEvents.valueChanged)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(DefaultCommentVC.tap(_:)))
        self.view.addGestureRecognizer(tapGesture)

        
        
    }

    
   
    func textViewDidChange(_ textView: UITextView) {
        
        if textView == Comments{
            
            placeholderLabel.isHidden = !Comments.text.isEmpty
        }
    }
    
    
    func tap(_ gestue: UITapGestureRecognizer){
        self.ShipQuantity.resignFirstResponder()
        self.ReScreenQuantity.resignFirstResponder()
        self.ProductionStatus.resignFirstResponder()
        self.Comments.resignFirstResponder()
    }
    
    
    func switchIsChanged(_ mySwitch: UISwitch) {
        
        if mySwitch.isOn {
            print("Pass")
            AuditResult = "P"
        } else {
            print("Fail")
            AuditResult = "F"
        }
    }
    
    func textFieldShouldReturn(_ userText: UITextField!) -> Bool {
      
        userText.resignFirstResponder()
        return true;
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
       
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
    
    func showalert(){
        let alertView = UIAlertController(title: nil , message: "Do write some comments", preferredStyle: .alert)
        let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertView.addAction(ok_action)
        self.present(alertView, animated: true, completion: nil)
    }

    
    
    @IBAction func Finish(_ sender: AnyObject) {
        print("newAuditResult=\(AuditResult)")
        if AuditResult == nil{
            
            let alertView = UIAlertController(title: nil , message: "Are you sure Audit Result is Fail?", preferredStyle: .alert)
            let yes_action: UIAlertAction = UIAlertAction(title: "Yes", style: .default, handler: { (UIAlertAction) -> Void in
                
                self.AuditResult = "F"
                
                if self.Comments.text.isEmpty{
                    
                    self.showalert()
                    
                }else{
                    
                    //If Internet is available or Not just save the data offline.
                    
                    //First Stop sync Service Then save data
                    
                    syncObject.endBackgroundTask()
                    updateTimer?.invalidate()
                    syncObject = nil
                    
                    
                    self.saveOfflineCommentsData()
                    
                    //NOW START Sync Service again
                    
                    syncObject = syncService()
                    
                    /*
                     if !Reachability.isConnectedToNetwork(){
                     
                     self.saveOfflineCommentsData()
                     }else{
                     self.performLoadDataRequestWithURL(self.getUrl())
                     }
                     */
                }
                
            })
            let no_action: UIAlertAction = UIAlertAction(title: "No", style: .default, handler: {(UIAlertAction)-> Void in
                
            })
            
            alertView.addAction(yes_action)
            alertView.addAction(no_action)
            
            self.present(alertView, animated: true, completion: nil)
            
            
        }else if self.Comments.text.isEmpty{
            
            self.showalert()
            
        }else if AuditResult != nil && !(self.Comments.text.isEmpty){
            print(AuditResult)
            print(self.Comments.text)
            
            //If Internet is available or Not just save the data offline.
            
            self.saveOfflineCommentsData()
            
            
            /*   if !Reachability.isConnectedToNetwork(){
             
             self.saveOfflineCommentsData()
             }else{
             self.performLoadDataRequestWithURL(self.getUrl())
             }
             */
        }
    }
    
    
    func saveOfflineCommentsData(){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        print("You are in AuditComments OFFLINE mode.")
        
        // Creating AuditComment Json file
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("AuditCommentFile\(self.AuditCode.text!).json")
        
        // creating a .json file in the Documents folder
        
        if !fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("File created ")
                
                // Now save it to FilesToSync.json file
                
                saveFileNameTo_FilesToSync("AuditCommentFile\(self.AuditCode.text!).json")
                //(jsonFilePath.absoluteString)
                
            } else {
                print("Couldn't create file for some reason")
            }
        } else {
            print("File already exists")
        }
        //file creation ends
        
        
        
        //First Read out the AuditComment AuditCodeFile
        
        do{
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath.absoluteString, encoding: String.Encoding.utf8.rawValue) as String
            
            print("readAuditComment_String = ",readString)
            
            /// here send readString to function to update values stored in AuditCommentFile(AuditCode) File
            
            self.AuditCommentAuditCodeFile(readString, AuditCommentFilePath: jsonFilePath.absoluteString)
            
        }
        catch let error as NSError{
            print("There is an error while reading AddDeffectText(AuditCode) File.",error.description)
        }
        
    }// end of func
    
    
    func AuditCommentAuditCodeFile(_ savedCommentsData:String?, AuditCommentFilePath: String) -> Void {
       
        if savedCommentsData != nil && savedCommentsData != "" {
//            if self.AuditResult == "Optional(\"P\")"{
//               self.AuditResult = "P"
//            }
                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodein!)&ShipQty=\(self.ShipQuantity.text!)&ReScreenQty=\(self.ReScreenQuantity.text!)&AuditResult=\(self.AuditResult!)&Comments=\(self.Comments.text!)&ProductionStatus=\(self.ProductionStatus.text!)"
            
            print("AuditComments-bodydata -> \(self.bodyData)")
            
            
            
            var AuditCommentsOfflineData:[String:String] = parseCommentsJSON(savedCommentsData!)
            
            
            let randomString = randomStringWithLength(6)
            
            AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "AuditComments_bodyData\(self.AuditCode.text!)_\(randomString)")
            AuditCommentsOfflineData.updateValue("http://app.3-tree.com/quonda/save-comments.php", forKey: "AuditComment_url")
            
            
            if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    //Note!!
                    
                    if !Reachability.isConnectedToNetwork(){
                        
                        //BEFORE MOVING ONTO THE NEXT SCREEN YOU HAVE TO UPDATE AUDITCOMPLETED VALUE IN AUDITMODE.JSON
                        
                        /*   let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
                         
                         let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("AuditMode.json")
                         do {
                         var readString: String
                         readString = try NSString(contentsOfFile: jsonFilePath.path!, encoding: NSUTF8StringEncoding) as String
                         print("readString_AuditorModeVC=\(readString)")
                         self.updateAuditorModeVCDataWhenOffline(readString)
                         } catch let error as NSError {
                         print(error.description)
                         }
                         */
                    }
                    
                    
                    // now move onto the AuditorModeViewController screen
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    
                    if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                        if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                            
                            present(resultController, animated: true, completion: nil)
                        }
                    }else{
                        if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                            
                            present(resultController, animated: true, completion: nil)
                        }
                    }

                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
            
        }else{
//            if self.AuditResult == "Optional(\"P\")"{
//                self.AuditResult = "P"
//            }
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodein!)&ShipQty=\(self.ShipQuantity.text!)&ReScreenQty=\(self.ReScreenQuantity.text!)&AuditResult=\(self.AuditResult!)&Comments=\(self.Comments.text!)&ProductionStatus=\(self.ProductionStatus.text!)"
            
            print("AuditComments-bodydata -> \(self.bodyData)")
            
            
            
            var AuditCommentsOfflineData = [String:String]()
            
            
            let randomString = randomStringWithLength(6)
            
            AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "AuditComments_bodyData\(self.AuditCode.text!)_\(randomString)")
            AuditCommentsOfflineData.updateValue("http://app.3-tree.com/quonda/save-comments.php", forKey: "AuditComment_url")
            
            
            if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    // first read out the file
                    //                    do{
                    //                        let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
                    //                        let jsonSyncFilePath = documentsDirectoryPath.URLByAppendingPathComponent("FilesToSync.json")
                    //
                    //                        var readString: String?
                    //                        readString = try NSString(contentsOfFile: jsonSyncFilePath.path!, encoding: NSUTF8StringEncoding) as String
                    //                        print("sync file after adding comments =",readString)
                    //                    }catch{
                    //                        print("cannot read")
                    //                    }
                    
                    
                    //Note!!
                    
                    //BEFORE MOVING ONTO THE NEXT SCREEN YOU HAVE TO UPDATE AUDITCOMPLETED VALUE IN AUDITMODE.JSON
                    
                    /*     let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
                     
                     let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("AuditMode.json")
                     do {
                     var readString: String
                     readString = try NSString(contentsOfFile: jsonFilePath.path!, encoding: NSUTF8StringEncoding) as String
                     print("readString_AuditorModeVC=\(readString)")
                     self.updateAuditorModeVCDataWhenOffline(readString)
                     } catch let error as NSError {
                     print(error.description)
                     }
                     */
                    
                    
                    // now move onto the AuditorModeViewController screen
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    
                    if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                        if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                            
                            present(resultController, animated: true, completion: nil)
                        }
                    }else{
                        if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                            
                            present(resultController, animated: true, completion: nil)
                        }
                    }
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
        }
        //        }// else part end of Arcadia
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        //Reading from JsonDictionary File stored in documents directory
        do{
            var readString: String
            readString = try NSString(contentsOfFile: AuditCommentFilePath, encoding: String.Encoding.utf8.rawValue) as String
            //                    print("readString=\(readString)")
            let readStringParsed = parseCommentsJSON(readString)
            print("AuditComments_readStringParsed = ",readStringParsed)
        }
        catch let error as NSError{
            print("There is an error while reading from JsonDictionary File.",error.description)
            
        }
        
    }
    
    
    //Random string function
    
    func randomStringWithLength(_ len: Int) -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for i in 0 ..< (len){
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString
    }
    
    
    
    func parseCommentsJSON(_ jsonString: String) -> [String:String] {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:String] {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
