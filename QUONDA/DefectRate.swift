//
//  DefectRate.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 20/04/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
var DefectsRateData = [IndexPath:String]()
var DefectsRateDataID = [IndexPath:String]()

var pTappedDefectsRateButtonIndexPath:IndexPath?
var pDefectsRateButtonTag:Int!


class DefectRate: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBAction func backButtonPressed(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var DefectsRateTable: UITableView!
    //    var data = [Int:String]()
    var dictionary:NSDictionary!
    var jsonFilePath2:URL!
    var created:Bool!
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    
    
    var cell:UITableViewCell!
    //    var pressed:Bool = false
    var selectedButton:NSMutableArray! = NSMutableArray()
    //    var pTappedDefectsRateButtonIndexPath:NSIndexPath?
    //    var pDefectsRateButtonTag:Int!
    var items: [String] = []//["We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift"]
    var itemsID: [String] = []
    
    var DefectID_Received: String!
    var ReportId: String!
    
    var All_DefectCodes = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = "Select Defect Code"
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("login.json")
        jsonLoginFilePath = jsonFilePath
        print(jsonLoginFilePath.path)
        
        do {
            var readString: String
            readString = try NSString(contentsOfFile: jsonLoginFilePath.path, encoding: String.Encoding.utf8.rawValue) as String
            print(readString)
            self.showDataWhenOffline(readString)
        } catch let error as NSError {
            print(error.description)
        }
        //        jsonLoginFilePath
        
        for i in 0 ..< (items.count) //yourTableSize = how many rows u got
        {
            selectedButton.add("NO")
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        //        for (var i = 0; i<items.count; i++) //yourTableSize = how many rows u got
        //        {
        //            selectedButton.addObject("NO")
        //        }
        if !DefectsRateData0.isEmpty {
            for d2 in DefectsRateData0{
                selectedButton.replaceObject(at: d2.0.row, with: "YES")
                
                let alreadyCheckedCell:UITableViewCell = self.DefectsRateTable.cellForRow(at: d2.0 as IndexPath)!
                //                alreadyCheckedCell.checkBox.selected = true
                
                DefectsRateData[d2.0 as IndexPath] = alreadyCheckedCell.textLabel!.text
                DefectsRateDataID[d2.0 as IndexPath] = alreadyCheckedCell.detailTextLabel?.text
                //                alreadyCheckedCell.checkBox.setBackgroundImage(UIImage(named: "check.png"), forState: .Selected)
            }
            DefectsRateData0.removeAll()
            self.DefectsRateTable.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return All_DefectCodes.count
        
        //        return self.dictionary["DefectAreas"]!.count
        //        return items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let Ccell = self.DefectsRateTable.dequeueReusableCell(withIdentifier: "ccell")!
        //        var checkBox:UIButton = UIButton(frame: CGRect(x: Ccell.frame.width, y: Ccell.frame.height/4, width: 30, height: 30))
        Ccell.textLabel!.text = items[indexPath.row]
        Ccell.detailTextLabel!.text = itemsID[indexPath.row]
        Ccell.detailTextLabel?.isHidden = true
        
        Ccell.textLabel!.font = UIFont(name: Ccell.textLabel!.font.fontName, size: 13)
        
        //   Ccell.checkBox.layer.cornerRadius = Ccell.checkBox.frame.height/2
        //        checkBox.setBackgroundImage(UIImage(named: "Radio-OFF"), forState: .Normal)
        //        checkBox.setBackgroundImage(UIImage(named: "Radio-ON"), forState: .Selected)
        
        //        checkBox.tag = indexPath.row+100
        //        checkBox.addTarget(self, action: "checkBoxTapped:", forControlEvents: UIControlEvents.TouchUpInside)
        
        //        if selectedButton.objectAtIndex(indexPath.row).isEqualToString("NO"){
        //            checkBox.selected = false
        //        }else{
        //            checkBox.selected = true
        //        }
        if (selectedButton.object(at: indexPath.row) as AnyObject).isEqual(to: "NO"){
            Ccell.accessoryType = .none
        }else {
            Ccell.accessoryType = .checkmark
            Ccell.backgroundColor = UIColor.lightGray
        }
        //        Ccell.contentView.addSubview(checkBox)
        
        return Ccell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("selected Index row path\(indexPath.row)")
        
        //        let button = sender as! UIButton
        //        let view = button.superview!
        //        self.cell = view.superview as! SearchStageCell
        //        let indexPath = searchTable.indexPathForCell(cell)
        
        let x = indexPath.row//button.tag - 100
        let cellPressed = tableView.cellForRow(at: indexPath)!
        
        // - - - - - - - - -
        //
        //        if cellPressed.accessoryType == .None{
        //            cellPressed.accessoryType = .Checkmark
        //            selectedButton.replaceObjectAtIndex(x, withObject: "YES")
        //            data[indexPath] = cellPressed.textLabel!.text
        //
        //        }else if cellPressed.accessoryType == .Checkmark{
        //            cellPressed.accessoryType = .None
        //            selectedButton.replaceObjectAtIndex(x, withObject: "NO")
        //            data.removeValueForKey(indexPath)
        //        }
        
        
        
        
        print(pTappedDefectsRateButtonIndexPath?.row)
//        if pTappedDefectsRateButtonIndexPath == indexPath{
//            //            selectedButton.replaceObjectAtIndex(x, withObject: "NO")
//            ////            button.selected = false
//            //            cellPressed.accessoryType = .None
//            //            DefectsRateData.removeValueForKey(indexPath)
//            //
//            //            pTappedDefectsRateButtonIndexPath = nil
//            //            pDefectsRateButtonTag = nil
//        }
       // else
        if pTappedDefectsRateButtonIndexPath != indexPath || pTappedDefectsRateButtonIndexPath == indexPath{
            if (pTappedDefectsRateButtonIndexPath != nil && pDefectsRateButtonTag != nil){
                if let Pcell = self.DefectsRateTable.cellForRow(at: pTappedDefectsRateButtonIndexPath!){
                    selectedButton.replaceObject(at: pDefectsRateButtonTag, with: "NO")
                    
                    Pcell.accessoryType = .none
                    DefectsRateData.removeValue(forKey: pTappedDefectsRateButtonIndexPath!)
                    DefectsRateDataID.removeValue(forKey: pTappedDefectsRateButtonIndexPath!)
                }
            }
            pTappedDefectsRateButtonIndexPath = indexPath
            selectedButton.replaceObject(at: x, with: "YES")
            //            button.selected = true
            cellPressed.accessoryType = .checkmark
            DefectsRateData[indexPath] = cellPressed.textLabel!.text
            DefectsRateDataID[indexPath] = cellPressed.detailTextLabel?.text
            
            pDefectsRateButtonTag = x
            
            //            DefectsRateData0 = DefectsRateData
            //            self.dismissViewControllerAnimated(true, completion: nil)
        }
        
        // - - Dismiss the current view - - - - - - -
        DefectsRateData0 = DefectsRateData as [IndexPath : String]
        self.dismiss(animated: false, completion: nil)
        self.DefectsRateTable.beginUpdates()
        self.DefectsRateTable.endUpdates()
    }
    
    //    func sortCountries(obj1:[String:AnyObject], obj2:[String:AnyObject]){
    //
    //    var sorted_DATA = unsortedCountries.sortedArrayUsingComparator({obj1, obj2 -> NSComparisonResult in
    //
    //    let rstrnt1 = obj1
    //    let rstrnt2 = obj2
    //    if (rstrnt1 as! String) < (rstrnt2 as! String) {
    //    return NSComparisonResult.OrderedAscending
    //    }
    //    if (rstrnt1 as! String) > (rstrnt2 as! String) {
    //    return NSComparisonResult.OrderedDescending
    //    }
    //    return NSComparisonResult.OrderedSame
    //    })
    //    }
    func before(_ value1: String, value2: String) -> Bool {
        // One string is alphabetically first.
        // ... True means value1 precedes value2.
        return value1 < value2;
    }
    
    func showDataWhenOffline(_ savedData:String?){
        
        if savedData != nil {
            
            //            var All_DefectCodes = NSMutableDictionary() //[String:AnyObject]!
            
            self.dictionary = self.parseJSON(savedData!)
            print("\(self.dictionary)")
            let DefectCodes = self.dictionary["DefectCodes"]!  as! [AnyObject]

            //let DefectCodes = self.dictionary["DefectCodes"]!  as! [String:AnyObject]
            print(DefectCodes)
            for def in DefectCodes{
                let ss = def["codes"] as! [AnyObject]
                
                
print("\(def["report"] as! NSNumber)")
                if "\(def["report"] as! NSNumber)" == ReportId!{
                    for de in ss{
                        
                        let DC = de as! [String:AnyObject]
                        print(DC["type"]!)  // reportId
                        print("\(DC["type"]!)")
                        print(ReportId!)
                    print(DC["defect"]!)  // do check if DC.1 is not nil
                    
                    
//                    let Keycomponents = (DC["defect"] as! String).components(separatedBy: "-")
//                    print(Keycomponents.first ?? "")
//                    print(Keycomponents.last!)
                    print(self.DefectID_Received)
                    if "\(DC["type"]!)" == self.DefectID_Received {
                        //let aString = "This is my string"
    //let newString = (DC["defect"] as! String).replacingOccurrences(of: Keycomponents.first ?? "", with: "")
                        // All_DefectCodes.setValue(i.value, forKey: Valuecomponents.first! )
                        All_DefectCodes.setValue(DC["defect"] as! String, forKey: DC["id"] as! String )
                    }

                    
                    
//                    var myDict:NSDictionary?
//                    do{
//                        myDict = try! DC.1 as? NSDictionary
//                    }catch{
//                        print(error)
//                    }
//                    print(myDict)
//                    for i in myDict!{
//                        print(i.key)
//                        let key = i.key as! String
////                        var value = i.value as! String
////                        var Valuecomponents = value.componentsSeparatedByString("-")
////                        print("left=\(Valuecomponents.first)")
////                        print("right=\(Valuecomponents.last)")
//                        
//                        let Keycomponents = key.components(separatedBy: "-")
//                        print(Keycomponents.first)
//                        print(Keycomponents.last)
//                        if Keycomponents.first! == self.DefectID_Received {
////                            All_DefectCodes.setValue(i.value, forKey: Valuecomponents.first! )
//                            All_DefectCodes.setValue(i.value, forKey: Keycomponents.last! )
//                        }
//                        for c in Keycomponents{
//                            print(c)
//                        }
//                        print(i.value)
//                    }
                    print("All_DefectCodes =\(All_DefectCodes)")
                    //                var components = DC.1 as
                    //                var fst = components
                    //                print(fst.first)
                }
                }
            }
            //            first[""]
            
            var myArray = [String : AnyObject]()
            //
            for dc in All_DefectCodes{
                
                myArray[dc.value as! String] = dc.key as AnyObject?
                
            }
            print(myArray)
            //            var mynewDICT:[Int:[String:AnyObject]]! = [:]
            //            var mynewDICT: [String:AnyObject]! = [:]
            // Create a dictionary.
            
            // Get the array from the keys property.
            let copyOfmyKeysArray = myArray.keys
            
            // Sort from low to high (alphabetical order).
            let sortedCopyOfmyKeysArray = copyOfmyKeysArray.sorted(by: <)
            print(sortedCopyOfmyKeysArray)
            
            //            for pi in sortedCopyOfmyArray{
            //                print(pi)
            //            }
            //            var s = myArray.keys.sort()
            //            print(s)
            // Loop over sorted keys.
            var i = 0
            for sortedKey in sortedCopyOfmyKeysArray{
                for key in copyOfmyKeysArray {
                    if key == sortedKey{
                        // Get value for this key.
                        if let value = myArray[key] {
                            //                            mynewDICT[value as! String] = key
                            //                            mynewDICT[i] = [value as! String : key]
                            self.items.append(key)
                            self.itemsID.append(value as! String)
                            
                            print("Key = \(key), Value = \(value)")
                            i+=1
                        }
                    }
                }}
            //            print(mynewDICT)
            //            var ix = 0
            //            for var ix = 0; ix < mynewDICT.count; ix++ {
            //                print(mynewDICT[ix])
            //            }
            //            while(ix < mynewDICT.count){
            //            print(mynewDICT[ix])
            //                ix+=1
            //            }
            
            // Get the array from the keys property.
            //            for countryObj in All_Countries{
            //
            //                var sorted_DATA = unsortedCountries.sortedArrayUsingComparator({countryObj, countryObj -> NSComparisonResult in
            //
            //                    let rstrnt1 = countryObj
            //                    let rstrnt2 = countryObj
            //                    if (rstrnt1 as! String) < (rstrnt2 as! String) {
            //                        return NSComparisonResult.OrderedAscending
            //                    }
            //                    if (rstrnt1 as! String) > (rstrnt2 as! String) {
            //                        return NSComparisonResult.OrderedDescending
            //                    }
            //                    return NSComparisonResult.OrderedSame
            //                })
            //            }
            //            print(unsortedCountries)
            
            //            print(sorted_DATA)
            //            if let unsortedCountries = self.dictionary["Countries"] as? NSArray{
            //                let descriptor = NSSortDescriptor(key: "", ascending: true)//(key: "name", ascending: true, selector: "caseInsensitiveCompare:")
            //                sortedCountries = unsortedCountries.sortedArrayUsingDescriptors([descriptor])
            ////            }
            //
            //            var sortedCountries =  All_Countries.sort({ (b:(String, AnyObject), a: (String, AnyObject)) -> Bool in
            //                print(a)
            //                print(b)
            //
            //
            //            })//sort { (left, right) -> Bool in
            //                let first = left.0//left["rstrnt_name"] as? String
            //                let second = right.0//right["rstrnt_name"] as? String
            
            //                return first < second
            //                if first<second{
            //                    return true
            //                }else{
            //                    return false
            //                }
            //                switch (first, second) {
            //                case  x : return x < y
            //                default: return false
            //                }
            //            }
            //            print(sortedCountries)
            //            print(All_Countries)
            //            var arr = Array(All_Countries)
            //            for (k,v) in (Array(All_Countries).sort {$0.1 as! String < $1.1 }) {
            //                print("\(k):\(v)")
            //            }
            print("Total Countries = \(All_DefectCodes.count)")
            
            //            for country in All_Countries{
            //                //
            //                //                print(country)
            //                //                self.items.append(country.1 as! String)
            //                //                self.itemsID.append(country.0)
            //            }
            //            var sorted = self.items.sort() // it sort by id's
            //            print(sorted)
            //           var ii = self.items.sort({ (a, b) -> Bool in
            //            print(a)
            //            print(b)
            //            if a < b {
            //                return true
            //            }
            //            return false
            //
            //            })
            //            print(ii)
            //            country.sort(){$0 < $1}
        }
    }
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
                
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
}


    
