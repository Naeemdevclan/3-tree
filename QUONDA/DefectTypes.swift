//
//  DefectCategories.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 20/04/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
var DefectsData = [IndexPath:String]()
var DefectsDataID = [IndexPath:String]()

var pTappedDefectsButtonIndexPath:IndexPath?
var pDefectsButtonTag:Int!


class DefectTypes: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBAction func backButtonPressed(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var DefectsTable: UITableView!
    //    var data = [Int:String]()
    var dictionary:NSDictionary!
    var jsonFilePath2:URL!
    var created:Bool!
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    
    
    var cell:UITableViewCell!
    //    var pressed:Bool = false
    var selectedButton:NSMutableArray! = NSMutableArray()
    //    var pTappedDefectsButtonIndexPath:NSIndexPath?
    //    var pDefectsButtonTag:Int!
    var items: [String] = []//["We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift"]
    var itemsID: [String] = []
    
    var ReportId: String!
    
    var All_DefectCodes = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("defectTypes is going to load")
        // Do any additional setup after loading the view.
        self.navigationItem.title = "Select Defect Type"
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("login.json")
        jsonLoginFilePath = jsonFilePath
        print(jsonLoginFilePath.path)
        
        do {
            var readString: String
            readString = try NSString(contentsOfFile: jsonLoginFilePath.path, encoding: String.Encoding.utf8.rawValue) as String
            print(readString)
            self.showDataWhenOffline(readString)
        } catch let error as NSError {
            print(error.description)
        }
        //        jsonLoginFilePath
        
        for i in 0 ..< (items.count) //yourTableSize = how many rows u got
        {
            selectedButton.add("NO")
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        //        for (var i = 0; i<items.count; i++) //yourTableSize = how many rows u got
        //        {
        //            selectedButton.addObject("NO")
        //        }
        if !DefectsData0.isEmpty {
            for d2 in DefectsData0{
                selectedButton.replaceObject(at: d2.0.row, with: "YES")
                
                let alreadyCheckedCell:UITableViewCell = self.DefectsTable.cellForRow(at: d2.0 as IndexPath)!
                //                alreadyCheckedCell.checkBox.selected = true
                
                DefectsData[d2.0 as IndexPath] = alreadyCheckedCell.textLabel!.text
                DefectsDataID[d2.0 as IndexPath] = alreadyCheckedCell.detailTextLabel?.text
                //                alreadyCheckedCell.checkBox.setBackgroundImage(UIImage(named: "check.png"), forState: .Selected)
            }
//            DefectsData0.removeAll()
            self.DefectsTable.reloadData()
        }
    }
    //    func checkBoxTapped(sender: AnyObject) {
    //
    //        let button = sender as! UIButton
    //        let view = button.superview!
    //        self.cell = view.superview as! UITableViewCell
    //        let indexPath = DefectsTable.indexPathForCell(cell)
    //
    //        var x = button.tag - 100
    //
    ////        if button.selected == true{
    ////            selectedButton.replaceObjectAtIndex(x, withObject: "NO")
    ////            button.selected = false
    ////            DefectsData.removeValueForKey(indexPath!)
    ////
    ////        }
    ////        else if button.selected == false{
    ////            selectedButton.replaceObjectAtIndex(x, withObject: "YES")
    ////            button.selected = true
    ////            DefectsData[indexPath!] = cell.checkLabel.text
    ////        }
    //
    //        if pTappedDefectsButtonIndexPath == indexPath{
    //            selectedButton.replaceObjectAtIndex(x, withObject: "NO")
    //            button.selected = false
    //            DefectsData.removeValueForKey(indexPath!)
    //            pTappedDefectsButtonIndexPath = nil
    //            pDefectsButtonTag = nil
    //        }
    //        else if pTappedDefectsButtonIndexPath != indexPath{
    //            if (pTappedDefectsButtonIndexPath != nil && pDefectsButtonTag != nil){
    //                let Pcell:UITableViewCell = self.DefectsTable.cellForRowAtIndexPath(pTappedDefectsButtonIndexPath!)! as! UITableViewCell
    //                selectedButton.replaceObjectAtIndex(pDefectsButtonTag, withObject: "NO")
    //               let previousButtonState =  Pcell.contentView.subviews.last//button.selected = false
    //                print((previousButtonState?.tag)!-100)
    //                previousButtonState?.setValue(false, forKey: "selected")
    //                print(previousButtonState?.valueForKey("selected"))
    //                DefectsData.removeValueForKey(pTappedDefectsButtonIndexPath!)
    //            }
    //            pTappedDefectsButtonIndexPath = indexPath
    //            selectedButton.replaceObjectAtIndex(x, withObject: "YES")
    //            button.selected = true
    //            DefectsData[indexPath!] = cell.textLabel!.text
    //            pDefectsButtonTag = x
    //        }
    //    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        return All_DefectCodes.count
        
//        return self.dictionary["DefectAreas"]!.count
        return items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let Ccell = self.DefectsTable.dequeueReusableCell(withIdentifier: "ccell")!
        //        var checkBox:UIButton = UIButton(frame: CGRect(x: Ccell.frame.width, y: Ccell.frame.height/4, width: 30, height: 30))
        Ccell.textLabel!.text = items[indexPath.row]
        Ccell.detailTextLabel!.text = itemsID[indexPath.row]
        Ccell.detailTextLabel?.isHidden = true
        
        Ccell.textLabel!.font = UIFont(name: Ccell.textLabel!.font.fontName, size: 13)
        
        //   Ccell.checkBox.layer.cornerRadius = Ccell.checkBox.frame.height/2
        //        checkBox.setBackgroundImage(UIImage(named: "Radio-OFF"), forState: .Normal)
        //        checkBox.setBackgroundImage(UIImage(named: "Radio-ON"), forState: .Selected)
        
        //        checkBox.tag = indexPath.row+100
        //        checkBox.addTarget(self, action: "checkBoxTapped:", forControlEvents: UIControlEvents.TouchUpInside)
        
        //        if selectedButton.objectAtIndex(indexPath.row).isEqualToString("NO"){
        //            checkBox.selected = false
        //        }else{
        //            checkBox.selected = true
        //        }
        if (selectedButton.object(at: indexPath.row) as AnyObject).isEqual(to: "NO"){
            Ccell.accessoryType = .none
        }else {
            Ccell.accessoryType = .checkmark
            Ccell.backgroundColor = UIColor.lightGray
        }
        //        Ccell.contentView.addSubview(checkBox)
        
        return Ccell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("selected Index row path\(indexPath.row)")
        
        //        let button = sender as! UIButton
        //        let view = button.superview!
        //        self.cell = view.superview as! SearchStageCell
        //        let indexPath = searchTable.indexPathForCell(cell)
        
        let x = indexPath.row//button.tag - 100
        let cellPressed = tableView.cellForRow(at: indexPath)!
        
        // - - - - - - - - -
        //
        //        if cellPressed.accessoryType == .None{
        //            cellPressed.accessoryType = .Checkmark
        //            selectedButton.replaceObjectAtIndex(x, withObject: "YES")
        //            data[indexPath] = cellPressed.textLabel!.text
        //
        //        }else if cellPressed.accessoryType == .Checkmark{
        //            cellPressed.accessoryType = .None
        //            selectedButton.replaceObjectAtIndex(x, withObject: "NO")
        //            data.removeValueForKey(indexPath)
        //        }
        
        
        
        
        print(pTappedDefectsButtonIndexPath?.row)
         if pTappedDefectsButtonIndexPath != indexPath || pTappedDefectsButtonIndexPath == indexPath{
            if (pTappedDefectsButtonIndexPath != nil && pDefectsButtonTag != nil){
                
                if let Pcell = self.DefectsTable.cellForRow(at: pTappedDefectsButtonIndexPath!) {
                
//                if let Pcell:UITableViewCell? = self.DefectsTable.cellForRowAtIndexPath(pTappedDefectsButtonIndexPath!)! {
                    selectedButton.replaceObject(at: pDefectsButtonTag, with: "NO")
                    //                let previousButtonState =  Pcell.contentView.subviews.last//button.selected = false
                    //                print((previousButtonState?.tag)!-100)
                    //                previousButtonState?.setValue(false, forKey: "selected")
                    //                print(previousButtonState?.valueForKey("selected"))
                    Pcell.accessoryType = .none
                    DefectsData.removeValue(forKey: pTappedDefectsButtonIndexPath!)
                    DefectsDataID.removeValue(forKey: pTappedDefectsButtonIndexPath!)
                }
            }
            pTappedDefectsButtonIndexPath = indexPath
            selectedButton.replaceObject(at: x, with: "YES")
            //            button.selected = true
            cellPressed.accessoryType = .checkmark
            DefectsData[indexPath] = cellPressed.textLabel!.text
            DefectsDataID[indexPath] = cellPressed.detailTextLabel?.text
            
            pDefectsButtonTag = x
            
            //            DefectsData0 = DefectsData
            //            self.dismissViewControllerAnimated(true, completion: nil)
        }
        
        // - - Dismiss the current view - - - - - - -
        DefectsData0 = DefectsData as [IndexPath : String]
        self.dismiss(animated: false, completion: nil)
        self.DefectsTable.beginUpdates()
        self.DefectsTable.endUpdates()
    }
    
    //    func sortCountries(obj1:[String:AnyObject], obj2:[String:AnyObject]){
    //
    //    var sorted_DATA = unsortedCountries.sortedArrayUsingComparator({obj1, obj2 -> NSComparisonResult in
    //
    //    let rstrnt1 = obj1
    //    let rstrnt2 = obj2
    //    if (rstrnt1 as! String) < (rstrnt2 as! String) {
    //    return NSComparisonResult.OrderedAscending
    //    }
    //    if (rstrnt1 as! String) > (rstrnt2 as! String) {
    //    return NSComparisonResult.OrderedDescending
    //    }
    //    return NSComparisonResult.OrderedSame
    //    })
    //    }
    func before(_ value1: String, value2: String) -> Bool {
        // One string is alphabetically first.
        // ... True means value1 precedes value2.
        return value1 < value2;
    }
    
    func showDataWhenOffline(_ savedData:String?){
        
        if savedData != nil {
            
//            var All_DefectCodes = NSMutableDictionary() //[String:AnyObject]!

            self.dictionary = self.parseJSON(savedData!)
            print("\(self.dictionary)")
            let DefectCodes = self.dictionary["DefectTypes"]!  as! [AnyObject]

           // let DefectCodes = self.dictionary["DefectTypes"]!  as! [String:AnyObject]
            print(DefectCodes)
            for de in DefectCodes{
                let DC =  de as! [String:AnyObject]
                print(DC)  // reportId
                print("////")
                let reports = DC["reports"] as!String
                if(reports != ""){
                let reportsArr = reports.components(separatedBy: ",")
                
                if reportsArr.contains(ReportId){
                    
                    
        All_DefectCodes.setValue(DC["name"], forKey: DC["id"] as! String )

//                    print(DC.1)  // do check if DC.1 is not nil
//                    var myDict:NSDictionary?
//                    do{
//                        myDict = try! DC.1 as? NSDictionary
//                    }catch{
//                        print(error)
//                    }
//                    if myDict != nil{
//                        print(myDict)
//                        for i in myDict!{
//                            print(i.key)
//                            let key = i.key as! String
//                            let Keycomponents = key.components(separatedBy: "-")
//                            print(Keycomponents.first)
//                            print(Keycomponents.last)
//                            if Keycomponents.first! == "0"{
//                                All_DefectCodes.setValue(i.value, forKey: Keycomponents.last! )
//                            }
//                            for c in Keycomponents{
//                                print(c)
//                            }
//                            print(i.value)
//                        }
//                    } // if myDict (means DC.1) != nil
                    print("All_DefectCodes= \(All_DefectCodes)")
                    
            // dismiss the view controller if there is no defect code
            
                    if All_DefectCodes.count < 1{
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                }
            }
//            first[""]
            
            var myArray = [String : AnyObject]()
            //
            for dc in All_DefectCodes{
                
                if let NotNulldcValue = dc.value as? String{
                    print("NotNulldcValue =",NotNulldcValue)
                    myArray[dc.value as! String] = dc.key as AnyObject?
                }
//                myArray[dc.value as! String] = dc.key
                
            }
            print(myArray)
//            var mynewDICT:[Int:[String:AnyObject]]! = [:]
            //            var mynewDICT: [String:AnyObject]! = [:]
            // Create a dictionary.
            
            // Get the array from the keys property.
            let copyOfmyKeysArray = myArray.keys
            
            // Sort from low to high (alphabetical order).
            let sortedCopyOfmyKeysArray = copyOfmyKeysArray.sorted(by: <)
            print(sortedCopyOfmyKeysArray)
            
//            for pi in sortedCopyOfmyArray{
//                print(pi)
//            }
//            var s = myArray.keys.sort()
//            print(s)
            // Loop over sorted keys.
            var i = 0
            for sortedKey in sortedCopyOfmyKeysArray{
                for key in copyOfmyKeysArray {
                    if key == sortedKey{
                        // Get value for this key.
                        if let value = myArray[key] {
                            //                            mynewDICT[value as! String] = key
//                            mynewDICT[i] = [value as! String : key]
                            self.items.append(key)  // here `key` is actually a value
                            self.itemsID.append(value as! String)  // and here `value` is actually a key
                            
                            print("Key = \(key), Value = \(value)")
                            i+=1
                        }
                    }
                }}
//            print(mynewDICT)
            //            var ix = 0
            //            for var ix = 0; ix < mynewDICT.count; ix++ {
            //                print(mynewDICT[ix])
            //            }
            //            while(ix < mynewDICT.count){
            //            print(mynewDICT[ix])
            //                ix+=1
            //            }
            
            // Get the array from the keys property.
            //            for countryObj in All_Countries{
            //
            //                var sorted_DATA = unsortedCountries.sortedArrayUsingComparator({countryObj, countryObj -> NSComparisonResult in
            //
            //                    let rstrnt1 = countryObj
            //                    let rstrnt2 = countryObj
            //                    if (rstrnt1 as! String) < (rstrnt2 as! String) {
            //                        return NSComparisonResult.OrderedAscending
            //                    }
            //                    if (rstrnt1 as! String) > (rstrnt2 as! String) {
            //                        return NSComparisonResult.OrderedDescending
            //                    }
            //                    return NSComparisonResult.OrderedSame
            //                })
            //            }
            //            print(unsortedCountries)
            
            //            print(sorted_DATA)
            //            if let unsortedCountries = self.dictionary["Countries"] as? NSArray{
            //                let descriptor = NSSortDescriptor(key: "", ascending: true)//(key: "name", ascending: true, selector: "caseInsensitiveCompare:")
            //                sortedCountries = unsortedCountries.sortedArrayUsingDescriptors([descriptor])
            ////            }
            //
            //            var sortedCountries =  All_Countries.sort({ (b:(String, AnyObject), a: (String, AnyObject)) -> Bool in
            //                print(a)
            //                print(b)
            //
            //
            //            })//sort { (left, right) -> Bool in
            //                let first = left.0//left["rstrnt_name"] as? String
            //                let second = right.0//right["rstrnt_name"] as? String
            
            //                return first < second
            //                if first<second{
            //                    return true
            //                }else{
            //                    return false
            //                }
            //                switch (first, second) {
            //                case  x : return x < y
            //                default: return false
            //                }
            //            }
            //            print(sortedCountries)
            //            print(All_Countries)
            //            var arr = Array(All_Countries)
            //            for (k,v) in (Array(All_Countries).sort {$0.1 as! String < $1.1 }) {
            //                print("\(k):\(v)")
            //            }
            print("Total Countries = \(All_DefectCodes.count)")
            
//            for country in All_Countries{
//                //
//                //                print(country)
//                //                self.items.append(country.1 as! String)
//                //                self.itemsID.append(country.0)
//            }
            //            var sorted = self.items.sort() // it sort by id's
            //            print(sorted)
            //           var ii = self.items.sort({ (a, b) -> Bool in
            //            print(a)
            //            print(b)
            //            if a < b {
            //                return true
            //            }
            //            return false
            //
            //            })
            //            print(ii)
            //            country.sort(){$0 < $1}
        }
    }
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
                
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
}


