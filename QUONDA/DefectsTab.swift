//
//  DefectsTab.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 13/05/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class DefectsTab: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var DefectsTabTable: UITableView!

    var imageCache = [String:UIImage]()
    
    var sketchs:[String] = []
    
    var dictionary:NSDictionary!
    
    
    var jsonFilePath2:URL!
    var created:Bool!
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    
    

    
    var items: [String] = ["We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift"]
    
    var noOfSections:Int!
    
    var DefectTypeNames: [String:AnyObject]! = [:]
    
//    var DT_Array: NSArray = []
    var myArray:AnyObject?//: [String:AnyObject?]!
    var myCount:AnyObject?
    var myDefectNamesArray:AnyObject?
    var DefectsArray: AnyObject!
    
    var myHeaderTitlesArray: AnyObject?
    
    var DefectTypesArray:[String] = []
    var FilteredDefectsArrayWithSections: [Int:AnyObject]! = [:]
    var FilteredDefectsSections: [Any]! = []

    
    
    
    var arrayOfkeys:[Int] = [Int]()
    
    var FilteredDefectTypesArray:Array<AnyObject>?
    
    var FilteredDefectsArray:NSArray?
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.DefectsTabTable.dataSource = self
        self.DefectsTabTable.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(DefectsTab.receiveReloadTableViewNotification(_:)), name: NSNotification.Name(rawValue: "reloadTableViewNotification"), object: nil)
        
//        self.DefectsTabTable.dataSource = self
//        self.DefectsTabTable.delegate = self
        
        print("DefectsTabLoaded")
        // register TableHeader NIB with this tableView
        
        let nib = UINib(nibName: "DefectTabHeaderView", bundle: nil)
        self.DefectsTabTable.register(nib, forHeaderFooterViewReuseIdentifier: "DefectTabHeaderViewid")
        
        
        // reading keyStats.json file stored in documents directory
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("keyStats.json")
        jsonFilePath2 = jsonFilePath
//        do {
//            var readString: String
//            readString = try NSString(contentsOfFile: jsonFilePath.path!, encoding: NSUTF8StringEncoding) as String
//            print(readString)
//            self.showDataWhenOffline(readString)
//        } catch let error as NSError {
//            print(error.description)
//        }

    }
    func receiveReloadTableViewNotification(_ notification: Notification){
//        self.DefectsTabTable.dataSource = self
//        self.DefectsTabTable.delegate = self
//print("reload method called")
       // print("myArray.count = \(myArray?.count)")
//        self.DefectsTabTable.reloadData()
//        self.DefectsTabTable.beginUpdates()
        updateData()
//        self.DefectsTabTable.endUpdates()
//        self.DefectsTabTable.reloadData()
    }
    
    var secondAppear = false
    override func viewDidAppear(_ animated: Bool) {
       print("DefectsTab is going to appear")
        if secondAppear == true{
            self.DefectsTabTable.reloadData()
        }
        secondAppear = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateData(){
        print("DefectsTab is now updated.")
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("keyStats.json")
        jsonFilePath2 = jsonFilePath
       // print("jsonFilePath2=\(jsonFilePath2)")
        DispatchQueue.main.async { () -> Void in
            
            do {
                var readString: String
                readString = try NSString(contentsOfFile: self.jsonFilePath2.path, encoding: String.Encoding.utf8.rawValue) as String
               // print(readString)
                self.showDataWhenOffline(readString)
            } catch let error as NSError {
                //print(error.description)
            }
        }
    }
    
    //- Delegate methods
//    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
//        var headerView: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
//        headerView.backgroundColor = UIColor.darkGrayColor()
////        view.tintColor = UIColor.grayColor()
//    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       
        var title = ""
        var titleINT = "0"
        
        print("c = \(FilteredDefectTypesArray)")
        
        if section < FilteredDefectTypesArray!.count{
           // print("FilteredDefectTypesArray[section]= \(FilteredDefectTypesArray![section])")
            print(FilteredDefectsArrayWithSections[section]!.count!)

            if FilteredDefectTypesArray!.count > 0{
                
                let currentDefectType = FilteredDefectsArray!.object(at: section)
                
                print("currentDefect=\(currentDefectType)")
                let DefectType = FilteredDefectTypesArray![section]   //currentDefectType.valueForKey("DefectType")
                let DefectsCount = (currentDefectType as AnyObject).value(forKey: "DefectsCount")
                
                title = DefectType as! String
                titleINT = DefectsCount as! String
            }
            let dd =  FilteredDefectsSections[section] as! [Any]
            print("defects:")
            print(dd)
            titleINT = "0"
            for dict in dd{
                let dddd = dict as! [String: AnyObject]
                //print(dict)
                let DefectsCount = dddd["DefectsCount"] as! String
                print("defects:")
                print(DefectsCount)
                titleINT = "\(Int(titleINT)! + Int(DefectsCount )!)"
                print("Totals:")
                print(titleINT)
 
            }
            //DefectsCount
            //- setting headerTitleINT.text
            
            // - -
//            print("headerSection= \(section)")
//            print("title= \(title)")
            
            // Dequeue with the reuse identifier
            let cell = self.DefectsTabTable.dequeueReusableHeaderFooterView(withIdentifier: "DefectTabHeaderViewid")
            let header = cell as! DefectTabHeaderView
           // print("header= \(header)")
            header.HeaderTitle.text = title
header.HeaderTitleINT.text = "\(titleINT)"
            //titleINT //"\(filteredDefects.count)"  //"2"
            //header.titleLabel.text = title
            
            return header
        }else{
            return nil
        }
    }
    

    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return FilteredDefectsArrayWithSections.keys.count
    }

   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let keysArray = FilteredDefectsArrayWithSections.keys
        var valuesAtkey:AnyObject?
        
        for i in keysArray {
            if i == section{
                print("key = \(i)")
                valuesAtkey = FilteredDefectsArrayWithSections[i]
//                print("valuesAtkey =\(valuesAtkey)")
//                print("valuesAtkey.count= \(valuesAtkey?.count)")
//                return valuesAtkey!.count
            }
        }
       // print(" valuesAtkey!.count in no of rows in section = \(valuesAtkey!.count)")
        return valuesAtkey!.count
    }
   
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.DefectsTabTable.dequeueReusableCell(withIdentifier: "DefectCell")! as! DefectsTabCell
        
        
//        let currentDefect = filteredDefectsArray.objectAtIndex(indexPath.row)
//        print("currentDefect=\(currentDefect)")
//        
        let section = indexPath.section
        let row = indexPath.row
        //print("section= \(section) and row = \(row)")
        
        let Key:Int = arrayOfkeys[section]
        let ArrayOfvalues = FilteredDefectsArrayWithSections[Key]
        let singleValueForRowInSection = ArrayOfvalues!.object(at: row)
        
//        print("ArrayOfvalues = \(ArrayOfvalues)")
//        print("singleValueForRowInSection = \(singleValueForRowInSection)")
        
        let DefectName = (singleValueForRowInSection as AnyObject).value(forKey: "DefectName")
        let DefectArea = (singleValueForRowInSection as AnyObject).value(forKey: "DefectArea")
        let DefectNature = (singleValueForRowInSection as AnyObject).value(forKey: "DefectNature")
        let Picture = (singleValueForRowInSection as AnyObject).value(forKey: "Picture")
        
//        print("DefectName for predicate= \(DefectName)")
//        print("DefectArea for predicate= \(DefectArea)")
//        print("DefectNature for predicate= \(DefectNature)")
//        print("Picture for predicate= \(Picture!)")
        cell.DefectLabel.text = DefectName as? String //textLabel?.text = DefectName as? String
        cell.AreaLabel.text = DefectArea as? String
        cell.NatureLabel.text = DefectNature as? String
        
        let aString: String = "\(Picture!)" //"This is picture string without /thumbs/"
        let LPictureString = aString.replacingOccurrences(of: "/thumbs/", with: "/")
       // print("newString = \(LPictureString)")
        
        cell.LargePictureString.text = LPictureString
       // print("LPictureString = \(cell.LargePictureString.text!)")
        
        let pictureUrl = Picture as? String
      

        // here is how you can do work in background priority
        
 /*       let qualityOfServiceClass = QOS_CLASS_BACKGROUND
        let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
        dispatch_async(backgroundQueue, {
            print("This is run on the background queue")
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print("This is run on the main queue, after the previous code in outer block")
            })
        })
*/
        
        
        
        // If this image is already cached, don't re-download
        if let img = self.imageCache[pictureUrl!] {
            cell.picture.image = img
        }
        else{
            if let url = URL(string: pictureUrl!){
                
                let qualityOfServiceClass = DispatchQoS.QoSClass.background
                let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
                
                backgroundQueue.async(execute: {
                    //print("This is run on the background queue")
                    if let data = try? Data(contentsOf: url) {
                        cell.picture.image = UIImage(data: data)
                        
                        // Convert the downloaded data in to a UIImage object
                        let image = UIImage(data: data)
                        // Store the image in to our cache
                        self.imageCache[pictureUrl!] = image
                    }
                    
                }) // end of background priority queue

            }
            
        }
       // - ! -! - !- !- !- !- !- - - - -
        
//        dispatch_async(dispatch_get_main_queue()) { () -> Void in
//            
//            if let url = NSURL(string: pictureUrl!){
//                if let data = NSData(contentsOfURL: url) {
//                    cell.picture.image = UIImage(data: data)
//                }
//            }
//        }

//        var arrayOfkeys = FilteredDefectsArrayWithSections.keys
//        for i in arrayOfkeys{
//            if indexPath.section == i{
//                var currentInddexPath = NSIndexPath(forRow: <#T##Int#>, inSection: i)
//                currentInddexPath
//            }
//        }


        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! DefectsTabCell
        
        let vc = storyboard!.instantiateViewController(withIdentifier: "showZoomedPicture") as! PicturePreview
        vc.imageURL = cell.LargePictureString.text
//        vc.photo = cell.picture.image
//        print("vc.photo = \(cell.picture.image)")
        
        self.present(vc, animated: false, completion: nil)
        
//        performSegueWithIdentifier("showZoomedPicture", sender: cell.picture.image)
        
    }
    
    
    
//    func openPicturePreview(){
//        performSegueWithIdentifier("showZoomedPicture", sender: nil)
//    }
    
    
    
    func showDataWhenOffline(_ savedData:String?){
        
        if savedData != nil {
            
            self.dictionary = self.parseJSON(savedData!)
//            print("\(self.dictionary)")
//            
            DefectsArray = self.dictionary["Defects"]! as AnyObject!
//            print("DefectsArray=")
//            print(DefectsArray)
//            print(DefectsArray.count)
            
//            var DefectTypeNamesValues:[AnyObject]! = []
            
            // calculating number of section in tableview
            
            let predicate:NSPredicate = NSPredicate(format: "DefectName != %@", "")
            FilteredDefectsArray = DefectsArray.filtered(using: predicate) as NSArray
            
            for i in 0 ..< FilteredDefectsArray!.count {
                
                DefectTypesArray.append((FilteredDefectsArray!.object(at: i) as AnyObject).value(forKey: "DefectType") as! String)
                
            }
            //DefectTypesArray after removing duplicates
            FilteredDefectTypesArray = Array(Set(DefectTypesArray)) as Array<AnyObject>?
            
//            FilteredDefectTypesArray = FilteredDefectTypesArray
            
            print("FilteredDefectTypesArray= \(FilteredDefectTypesArray)")
            print("FilteredDefectTypesArray= \(FilteredDefectTypesArray)")
            FilteredDefectsSections.removeAll()
            for j in 0 ..< FilteredDefectTypesArray!.count {
                
                let DType:NSString = FilteredDefectTypesArray![j] as! NSString // defectType
                let predicate:NSPredicate = NSPredicate(format: "DefectType == %@", DType)
                let array = FilteredDefectsArray!.filtered(using: predicate)  // this is a group array of specific defectType
                print("array= \(array)")
                FilteredDefectsArrayWithSections[j] = array as AnyObject?   // this is dictionary with section no as key and group array as value
                FilteredDefectsSections.append(array as [Any])
                //FilteredDefectsSections[j] = array as [Any]
                print(FilteredDefectsArrayWithSections[j]!)
                print(FilteredDefectsArrayWithSections[j]!.count!)
//                DefectTypesArray.append(FilteredDefectsArray.objectAtIndex(j) as! String)
                
            }
            
            getLists()
            
            print("arrayOfkeys in showDataWhenOffline = \(arrayOfkeys)")
            print(FilteredDefectsArrayWithSections.count)
            print("FilteredDefectsArrayWithSections= \(FilteredDefectsArrayWithSections)")
           
            print(" - - - - - - - ")
            self.DefectsTabTable.reloadData()
            
        }
        else{
            // if data found is nil
            noDataFound()
        }

//        dispatch_async(dispatch_get_main_queue()) { () -> Void in
//            
//        }
        
    }
    
    
    func noDataFound(){
        
        let alert:UIAlertController = UIAlertController(title: nil, message: "No Defect Found!", preferredStyle: UIAlertControllerStyle.alert)
        self.present(alert, animated: true, completion: nil)
        
        let duration:UInt64 = 2; // duration in seconds
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(duration*NSEC_PER_SEC)) / Double(NSEC_PER_SEC)) { () -> Void in
            alert.dismiss(animated: true, completion: nil)
        }
    }

    
    
    // Sort Array
    func getLists() {
        arrayOfkeys = Array(FilteredDefectsArrayWithSections.keys)
        print("arrayOfkeys before sorting = \(arrayOfkeys)")
        arrayOfkeys.sort { (value1, value2) -> Bool in
            return value1 < value2
        }
        print("arrayOfkeys After sorting = \(arrayOfkeys)")
    }

    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
                
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }

    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showZoomedPicture"{
            
            let picture = sender as! UIImage
            
            if picture != UIImage(named: "default"){
                
                let destinationVC = segue.destination as! PicturePreview
                    destinationVC.photo = picture
                    
                    if destinationVC.photo != nil{
                        let vc = storyboard!.instantiateViewController(withIdentifier: "showZoomedPicture") as! PicturePreview
                        
                        self.present(vc, animated: false, completion: nil)
                    }
                
            }
        }
    }
    

}



