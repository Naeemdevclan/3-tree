//
//  DefectsTabCell.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 18/05/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class DefectsTabCell: UITableViewCell {

    @IBOutlet weak var DefectLabel: UILabel!
    @IBOutlet weak var AreaLabel: UILabel!
    @IBOutlet weak var NatureLabel: UILabel!
    
    @IBOutlet weak var picture: UIImageView!
    
    @IBOutlet weak var LargePictureString: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
