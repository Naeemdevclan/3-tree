//
//  Downloader.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 04/04/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import Foundation

class Downloader {
    class func load(_ URL: Foundation.URL) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        let request = NSMutableURLRequest(url: URL)
        request.httpMethod = "GET"
//        let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
//            <#code#>
//        }
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error == nil) {
                // Success
                let statusCode = (response as! HTTPURLResponse).statusCode
                print("Success: \(statusCode)")
                
                // This is your file-variable:
                // data
            }
            else {
                // Failure
                print("Faulure: %@", error!.localizedDescription);
            }
        })
        task.resume()
    }
}
