//
//  EmailViewController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 05/09/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class EmailViewController: UIViewController, UITabBarDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!

    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    
    
    
    
    @IBOutlet weak var auditCodeView: UIView!
    @IBOutlet weak var AuditCodeLabel: UILabel!
    
    @IBOutlet weak var BrandLabel: UILabel!
    @IBOutlet weak var VendorLabel: UILabel!
    @IBOutlet weak var PoLabel: UILabel!
    @IBOutlet weak var StyleLabel: UILabel!
    
    
    @IBOutlet weak var EmailPicture: UIImageView!
    @IBOutlet weak var EmailsTable: UITableView!
    
    
    @IBOutlet weak var Email_Report_Btn: UIButton!
    
    @IBOutlet weak var Other_emails: UITextField!
    
    
    
    // Received data(User and AuditCode) from KPIViewController
    
    var user = defaults.object(forKey: "userid")as! String
    var receivedAuditCode = String()
    

    var dictionary:NSDictionary!
    
    var EmailsList = [Int:(ID:String, EMAILID:String)]()
    
    
    
    var selectedEmailsArray : NSMutableArray = []
//    var username1Array : NSMutableArray = ["hel","dsf","fsdg"]//just a sample
    
    @IBOutlet weak var SelectAll_Btn: UIButton!
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.BrandLabel.text = ""
        self.VendorLabel.text = ""
        self.PoLabel.text = ""
        self.StyleLabel.text = ""
        self.auditCodeView.backgroundColor = UIColor.white

        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "Selected QAReport")!, UIImage(named: "Auditor Mode")!, UIImage(named: "Selected Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)

        

        // setting all other ui elements
        
        // - - - - add notifications to current view    keyboardWillShow:
        
        NotificationCenter.default.addObserver(self, selector: #selector(EmailViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.addObserver(self, selector: #selector(EmailViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
        

        
        Email_Report_Btn.layer.cornerRadius = 10
        Email_Report_Btn.clipsToBounds = true
        
        Other_emails.delegate = self
        
        
        print("receivedAuditCode",receivedAuditCode)
        print("user",user)
        
        self.AuditCodeLabel.text = receivedAuditCode
        
//        EmailsTable.dataSource = self
//        EmailsTable.delegate = self
        
        _ = self.performLoadDataRequestWithURL(self.getUrl("http://app.3-tree.com/quonda/audit-emails.php"))
        
    }

    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        Other_emails.resignFirstResponder()
        return true
    }
    
    
    
    @IBAction func SelectAll_Btn_Pressed(_ sender: AnyObject) {
        
        let button = sender as! UIButton
        
        if(button.titleLabel?.text == "Select All")
        {
            //First removeAll the objects because may be user has already selected some of the rows 
            // and the below loop will append all rows to the previous selected rows.
            
            selectedEmailsArray.removeAllObjects()
            
            
            for email in EmailsList{
                print("email.0 = ",email.1)
                let emailID = email.1
                emailID.ID
                selectedEmailsArray.add(emailID.ID as AnyObject)
            }
            
            button.setTitle("Deselect All", for: UIControlState())
            print("All rows selected")
        }
        else
        {
            selectedEmailsArray.removeAllObjects();
            
            button.setTitle("Select All", for: UIControlState())
            print("No row selected")
        }
        
        self.EmailsTable.reloadData()
        
    }
    
    
    
    @IBAction func Email_Report_Btn_Pressed(_ sender: AnyObject) {
        
        // send request to server to send Email.
        
      
        
        // First prepare comma separated IDs from selectedEmailsArray
        
        var CommaSeparated_selectedEmailsArray = String()
        var EmailsArray: NSMutableArray = []
        
        if selectedEmailsArray.count > 0{
            
            EmailsArray.addObjects(from: selectedEmailsArray as [AnyObject])  // = selectedEmailsArray
            
            if EmailsArray.count >= 1{
                
                for ID in EmailsArray{
                    
                    CommaSeparated_selectedEmailsArray.append(ID as! String)
                    EmailsArray.remove(ID)
                    
                    if EmailsArray.count > 0{
                        CommaSeparated_selectedEmailsArray.append(",")
                    }
                    
                }
            }
        }
        
        // End of the preparation of Comma separated IDs from selectedEmailsArray
        
        
        print("CommaSeparated_selectedEmailsArray = ",CommaSeparated_selectedEmailsArray)
        print("selectedEmailsArray when Email Button pressed = ",selectedEmailsArray)
        
        var otherEmails = String()
        
        if !(self.Other_emails.text!.isEmpty) && self.Other_emails.text!.contains("@"){
            
            otherEmails = self.Other_emails.text!
        }
        
        
        
        // Now send Email Request to server
        
        if !CommaSeparated_selectedEmailsArray.isEmpty || !otherEmails.isEmpty{
            
            let emailbody = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(receivedAuditCode)&Users=\(CommaSeparated_selectedEmailsArray)&Others=\(otherEmails)"
            
            sendEmailToServer(emailbody, url: self.getUrl("http://app.3-tree.com/quonda/email-qa-report.php"))
            
        }
        
        
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
            //            defaults.removeObjectForKey("userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
                      
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 4:
            /*
             
             if downloadTask != nil{
             UIApplication.sharedApplication().networkActivityIndicatorVisible = false
             progressView.setProgress(0.0, animated: false)
             downloadTask.cancel()
             //                downloadTask = nil
             }
             
             performDocumentDownload()
             
             */
            
            break
        case 5:
                    
            if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }else{
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }

            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 6:
            
            //            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("6") as? KPIViewController {
            //                //                self.dismissViewControllerAnimated(true, completion: nil)
            //                presentViewController(resultController, animated: true, completion: nil)
            //            }
            
            //            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            //            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            
            break
        case 7:
            
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_selected_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break

        default:
            
            break
        }
        
    }
    
    
    // adjust the screen size when keyboard appears and hides
    
    func keyboardWillHide(_ sender: Notification) {
        let userInfo: [AnyHashable: Any] = sender.userInfo!
        let keyboardSize: CGSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue.size
        self.view.frame.origin.y += keyboardSize.height
    }
    
    func keyboardWillShow(_ sender: Notification) {
        let userInfo: [AnyHashable: Any] = sender.userInfo!
        let keyboardSize: CGSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue.size
        let offset: CGSize = (userInfo[UIKeyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size
        
        if keyboardSize.height == offset.height {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y -= keyboardSize.height
            })
        } else {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y += keyboardSize.height - offset.height
            })
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        let count = self.EmailsList.count
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.EmailsTable.dequeueReusableCell(withIdentifier: "cell")!
        print("you are in tableviewcellforRow")
        if EmailsList.count > 0{

        
            let EmailInfo = EmailsList[indexPath.row]//EmailInfo["Id"] as! String
            let Id = EmailInfo?.ID
            let EmailId = EmailInfo?.EMAILID

            cell.textLabel?.text = EmailId
            cell.detailTextLabel?.text = Id
          
            
            print("selectedEmailsArray = ",selectedEmailsArray)
           
            
            if(selectedEmailsArray.contains(Id as AnyObject))
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                print("No object present in emailArray.")
                cell.accessoryType = .none
            }
    
        
        }
    
        
//        cell.textLabel?.text = "nothing.."
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let CurrentCell = tableView.cellForRow(at: indexPath)
        
        let Id = CurrentCell?.detailTextLabel?.text
        
        
        
        if SelectAll_Btn.titleLabel?.text == "Deselect All" {
            
            SelectAll_Btn.setTitle("Select All", for: UIControlState())
        }
        
        print("selectedEmailsArray before tap = ", selectedEmailsArray)
        
        if CurrentCell?.accessoryType == UITableViewCellAccessoryType.none {
            
            selectedEmailsArray.add(Id as AnyObject)
            
            CurrentCell?.accessoryType = .checkmark
            print("selectedEmailsArray when tap =",selectedEmailsArray)
        
        }else{
            
            selectedEmailsArray.remove(Id as AnyObject)
            CurrentCell?.accessoryType = .none
            
            print("selectedEmailsArray when tapCancel =",selectedEmailsArray)
        }
        
    }
    
    
    
    
    
    func getUrl(_ url:String) -> URL {
        let toEscape = url
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
    
    
    var bodyData:String!
    
    func performLoadDataRequestWithURL(_ url: URL) -> String? {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(receivedAuditCode)"
        
        print("EmailViewController-bodydata -> \(self.bodyData)")
        
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = self.bodyData!.data(using: String.Encoding.utf8)
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main){
            response, data, error in
            print("\(response)")
            if (error != nil) {
                print(error)
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                let alertView = UIAlertController(title: nil , message: "\(error?._code) \n Error in loading page!", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "Retry", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    self.performLoadDataRequestWithURL(url)
                })
                let cancel_action: UIAlertAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                alertView.addAction(ok_action)
                alertView.addAction(cancel_action)
                self.present(alertView, animated: true, completion: nil)
                
                //                self.activityIndicator.stopAnimating()
                //                self.messageFrame.removeFromSuperview()
            }
            
            if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String

                print("json=")
                print(json)
                self.dictionary = self.parseJSON(json)
                print("self.dictionary=")
                print("\(self.dictionary)")
                print(self.dictionary["Status"])
                if"\(self.dictionary["Status"]!)" == "OK" {
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    print("status is OK")
                    
                   let AuditArray = self.dictionary["Audit"]! as! [String:AnyObject]
                   
                    print("AuditArray = ", AuditArray)
                    
                    let brand =  AuditArray["Brand"]!
                    let Vendor =  AuditArray["Vendor"]!
                    let Po =  AuditArray["Po"]!
                    let Style =  AuditArray["Style"]!
                    let PictureURL = AuditArray["Picture"]!
                    if AuditArray["AuditResult"] is NSNull{
                        self.auditCodeView.backgroundColor = UIColor.darkGray
                    }else{
                    if let auditResult = AuditArray["AuditResult"]{                    if(auditResult as! String == "P"){
                    self.auditCodeView.backgroundColor = UIColor.init(red: 51, green: 169, blue: 36)
                    }
                    else if(auditResult as! String == "F"){
                    self.auditCodeView.backgroundColor = UIColor.init(red: 238, green: 0, blue: 11)

                    }
                    }
                    else{
                       self.auditCodeView.backgroundColor = UIColor.darkGray
                    }
                    }

                    print("Brand = ", brand)
                    print("Vendor = ", Vendor)
                    print("Po = ", Po)
                    print("Style = ", Style)
                    print("picture = ",PictureURL)
                    
                    self.BrandLabel.text = "\(brand as! String)"
                    self.VendorLabel.text = "\(Vendor as! String)"
                    self.PoLabel.text = "\(Po as! String)"
                    self.StyleLabel.text = "\(Style as! String)"
                    
                    if let url = URL(string: PictureURL as! String){
                        if let data = try? Data(contentsOf: url) {
                            self.EmailPicture.image = UIImage(data: data)
                        }
                    }

                    
                    
                    var EmailArray:[[String:Any]] = []//Array<Any>()
                    
                    EmailArray = self.dictionary["Emails"]! as! [[String:Any]]
                    print("EmailsArray = ",EmailArray)
                    
//                    var EmailsList = [Int:[String:String]]()
                    
                    var i = 0
                    for EmailInfo in EmailArray{
                        let Id = EmailInfo["Id"] as! String
                        let Name = EmailInfo["Name"] as! String
                        
                        print("Id =",Id)
                        print("Name =",Name)
                        
                        self.EmailsList.updateValue((Id,Name), forKey: i)
                        
                        print("self.EmailsList[i]?.EMAILID ->", self.EmailsList[i]?.EMAILID)
                        
                        i += 1
                    }
                    
                    print("EmailsList = ",self.EmailsList)
                    
                    let firstElement = self.EmailsList[0]
                    print("firstElement ID = ",firstElement?.ID)
                    print("firstElement EMAILID = ",firstElement?.EMAILID)
                
                    DispatchQueue.main.async(execute: {
                       
                        self.EmailsTable.dataSource = self
                        self.EmailsTable.delegate = self
                    })

                    self.EmailsTable.beginUpdates()
                    self.EmailsTable.endUpdates()

                    
                    
                }else{
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    
                    let alertView = UIAlertController(title: "Error in loading page" , message: "Press OK", preferredStyle: .alert)
                    let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                    
                    alertView.addAction(ok_action)
                    self.present(alertView, animated: true, completion: nil)
                }
                
            }//if data != nil
            else{
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                return print("return data is nil")
            }
        }
        return json
    }
    
    func sendEmailToServer(_ EmailBodyData:String, url: URL) -> String? {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        self.bodyData = EmailBodyData
        
        print("EmailBodyData -> \(self.bodyData)")
        
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = self.bodyData!.data(using: String.Encoding.utf8)
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main){
            response, data, error in
            print("\(response)")
            if (error != nil) {
                print(error)
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                let alertView = UIAlertController(title: nil , message: "\(error?._code) \n Error in Sending Email!", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "Retry", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    self.sendEmailToServer(EmailBodyData, url: url)
                })
                let cancel_action: UIAlertAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                alertView.addAction(ok_action)
                alertView.addAction(cancel_action)
                self.present(alertView, animated: true, completion: nil)
                
                //                self.activityIndicator.stopAnimating()
                //                self.messageFrame.removeFromSuperview()
            }
            
            if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                
                print("json=")
                print(json)
                self.dictionary = self.parseJSON(json)
                print("self.dictionary=")
                print("\(self.dictionary)")
                print(self.dictionary["Status"])
                if"\(self.dictionary["Status"]!)" == "OK" {
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    print("status is OK")
                    
                    self.EmailSentMessage("Email Sent!")
                    
//                    self.performSegueWithIdentifier("goBackToKPI", sender: nil)

                    
                }else{
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    
                    let alertView = UIAlertController(title: nil, message: "\(self.dictionary["Message"]!)", preferredStyle: .alert)
                    let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                    
                    alertView.addAction(ok_action)
                    self.present(alertView, animated: true, completion: nil)
                }
                
            }//if data != nil
            else{
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                return print("return data is nil")
            }
        }
        return json
    }
    
    
    func EmailSentMessage(_ message:String){
        
        let alert:UIAlertController = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.alert)
        self.present(alert, animated: true, completion: nil)
        
        let duration:UInt64 = 3; // duration in seconds
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(duration*NSEC_PER_SEC)) / Double(NSEC_PER_SEC)) { () -> Void in
            alert.dismiss(animated: true, completion: {
                
                self.performSegue(withIdentifier: "goBackToKPI", sender: self.AuditCodeLabel.text!)
                
            })
        }
    }
    

    
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    
    //goBackToKPI
    
     // MARK: - Navigation
    
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        
        if segue.identifier == "goBackToKPI"{
            print("you are moving back to KPI VC")
            
            let destinationVC = segue.destination as! KPIViewController
            destinationVC.AuditCodeFromEmailVC = sender as! String
        }
     }
    

    
}
