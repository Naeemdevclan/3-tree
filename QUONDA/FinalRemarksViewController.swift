//
//  FinalRemarksViewController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 02/11/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class FinalRemarksViewController: UIViewController, UITabBarDelegate, UITextFieldDelegate,UIScrollViewDelegate {

    
    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!

    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    

    @IBOutlet weak var AuditCode: UILabel!
    @IBOutlet weak var AuditStage: UILabel!
    
    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var LayoutView: UIView!
    
    
    
    //IBOutlet Overall Result Summary 
    
    @IBOutlet weak var ShippingStatus: UISegmentedControl!
    @IBOutlet weak var ShippingRemarks: UITextField!
    
    @IBOutlet weak var MaterialConfirmityStatus: UISegmentedControl!
    @IBOutlet weak var MaterialConfirmityRemarks: UITextField!
    
    
    @IBOutlet weak var ProductConfirmityStyleStatus: UISegmentedControl!
    @IBOutlet weak var ProductConfirmityStyleRemarks: UITextField!
    
    
    @IBOutlet weak var ProductConfirmityColorStatus: UISegmentedControl!
    @IBOutlet weak var ProductConfirmityColorRemarks: UITextField!
    
    
    
    @IBOutlet weak var ExportCartonPackingStatus: UISegmentedControl!
    @IBOutlet weak var ExportCartonPackingRemarks: UITextField!
    
    
    
    @IBOutlet weak var InnerCartonPackingStatus: UISegmentedControl!
    @IBOutlet weak var InnerCartonPackingRemarks: UITextField!
    
    
    
    @IBOutlet weak var ProductPackagingStatus: UISegmentedControl!
    @IBOutlet weak var ProductPackagingRemarks: UITextField!
    
    
    
    @IBOutlet weak var AssortmentStatus: UISegmentedControl!
    @IBOutlet weak var AssortmentRemarks: UITextField!
    
    
    @IBOutlet weak var LabelingStatus: UISegmentedControl!
    @IBOutlet weak var LabelingRemarks: UITextField!
    
    
    
    @IBOutlet weak var MarkingStatus: UISegmentedControl!
    @IBOutlet weak var MarkingRemarks: UITextField!
    
    
    
    @IBOutlet weak var WorkmanshipStatus: UISegmentedControl!
    @IBOutlet weak var WorkmanshipRemarks: UITextField!
    
    
    
    @IBOutlet weak var WorkmanshipAppearanceStatus: UISegmentedControl!
    @IBOutlet weak var WorkmanshipAppearanceRemarks: UITextField!
    
    
    @IBOutlet weak var WorkmanshipFunctionStatus: UISegmentedControl!
    @IBOutlet weak var WorkmanshipFunctionRemarks: UITextField!
    
    
    @IBOutlet weak var WorkmanshipPrintedMaterialStatus: UISegmentedControl!
    @IBOutlet weak var WorkmanshipPrintedMaterialRemarks: UITextField!
    
    
    @IBOutlet weak var WorkmanshipFinishingStatus: UISegmentedControl!
    @IBOutlet weak var WorkmanshipFinishingRemarks: UITextField!
    
    
    
    @IBOutlet weak var MeasurementStatus: UISegmentedControl!
    @IBOutlet weak var MeasurementRemarks: UITextField!
    
    
    
    @IBOutlet weak var FabricWeightStatus: UISegmentedControl!
    @IBOutlet weak var FabricWeightRemarks: UITextField!
    
    
    
    @IBOutlet weak var CalibratedScaleStatus: UISegmentedControl!
    
    @IBOutlet weak var CalibratedScaleRemarks: UITextField!
    
    
    @IBOutlet weak var Cords_norm_otherStatus: UISegmentedControl!
    @IBOutlet weak var Cords_norm_otherRemarks: UITextField!
    
    
    
    @IBOutlet weak var InspectionConditionStatus: UISegmentedControl!
    @IBOutlet weak var InspectionConditionRemarks: UITextField!
    
    @IBOutlet weak var CartonSticker: UISegmentedControl!
    
    @IBOutlet weak var CartonStickerRemarks: UITextField!
    
    @IBOutlet weak var WorkmanshipFitting: UISegmentedControl!
    @IBOutlet weak var WorkmanshipFittingRemarks: UITextField!
    
    
    
    
    
    @IBOutlet var Remarks_Array: [UITextField]!
    
    var currentView_y:CGFloat?
    
    
    var arrayOfRemarks:[UITextField]!
    
    
    
    var jsonFilePath:URL!
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    var dictionary:NSDictionary!
    var MutableDictionary:NSMutableDictionary!
    var AuditCodein: String!
    var AuditStagein: String!
    var AuditStageColorin : UIColor!
    
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    var created:Bool!
    var paramPath = "/quonda"
    var otherImagesType:String!
    var savedParamPath:String!
    var ReportID_against_current_audit: String!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        LayoutView.frame = CGRect.init(x: LayoutView.frame.origin.x, y: LayoutView.frame.origin.y, width: LayoutView.frame.size.width, height: 2497)
//        print(LayoutView.frame)
//        ScrollView.contentSize.height = LayoutView.frame.height + 2497

        
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Selected Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)

        

        
        // - - - - add notifications to current view    keyboardWillShow:
        
      //  NotificationCenter.default.addObserver(self, selector: #selector(AddDefect.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        
      //  NotificationCenter.default.addObserver(self, selector: #selector(AddDefect.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
        
        
        // - - - -
        
        
        ShippingRemarks.delegate = self
        MaterialConfirmityRemarks.delegate = self
        ProductConfirmityStyleRemarks.delegate = self
        ProductConfirmityColorRemarks.delegate = self
        ExportCartonPackingRemarks.delegate = self
        InnerCartonPackingRemarks.delegate = self
        ProductPackagingRemarks.delegate = self
        AssortmentRemarks.delegate = self
        LabelingRemarks.delegate = self
        MarkingRemarks.delegate = self
        WorkmanshipRemarks.delegate = self
        WorkmanshipAppearanceRemarks.delegate = self
        WorkmanshipFunctionRemarks.delegate = self
        WorkmanshipPrintedMaterialRemarks.delegate = self
        WorkmanshipFinishingRemarks.delegate = self
        MeasurementRemarks.delegate = self
        FabricWeightRemarks.delegate = self
        Cords_norm_otherRemarks.delegate = self
        InspectionConditionRemarks.delegate = self
        
        for T in Remarks_Array{
            T.delegate = self
        }
        
        // Enables the default Nothing selected on the start
        
        ShippingStatus.selectedSegmentIndex = UISegmentedControlNoSegment
        MaterialConfirmityStatus.selectedSegmentIndex = UISegmentedControlNoSegment
        ProductConfirmityStyleStatus.selectedSegmentIndex = UISegmentedControlNoSegment
        ProductConfirmityColorStatus.selectedSegmentIndex = UISegmentedControlNoSegment
        ExportCartonPackingStatus.selectedSegmentIndex = UISegmentedControlNoSegment
        InnerCartonPackingStatus.selectedSegmentIndex = UISegmentedControlNoSegment
        ProductPackagingStatus.selectedSegmentIndex = UISegmentedControlNoSegment
        AssortmentStatus.selectedSegmentIndex = UISegmentedControlNoSegment
        LabelingStatus.selectedSegmentIndex = UISegmentedControlNoSegment
        MarkingStatus.selectedSegmentIndex = UISegmentedControlNoSegment
        WorkmanshipStatus.selectedSegmentIndex = UISegmentedControlNoSegment
        WorkmanshipAppearanceStatus.selectedSegmentIndex = UISegmentedControlNoSegment
        WorkmanshipFunctionStatus.selectedSegmentIndex = UISegmentedControlNoSegment
        WorkmanshipPrintedMaterialStatus.selectedSegmentIndex = UISegmentedControlNoSegment
        WorkmanshipFinishingStatus.selectedSegmentIndex = UISegmentedControlNoSegment
        MeasurementStatus.selectedSegmentIndex = UISegmentedControlNoSegment
        FabricWeightStatus.selectedSegmentIndex = UISegmentedControlNoSegment
        CalibratedScaleStatus.selectedSegmentIndex = UISegmentedControlNoSegment
        Cords_norm_otherStatus.selectedSegmentIndex = UISegmentedControlNoSegment
        InspectionConditionStatus.selectedSegmentIndex = UISegmentedControlNoSegment
        CartonSticker.selectedSegmentIndex = UISegmentedControlNoSegment
        WorkmanshipFitting.selectedSegmentIndex = UISegmentedControlNoSegment

        
        // original y point of current view
        self.currentView_y = self.view.frame.origin.y
        
        arrayOfRemarks = [self.ShippingRemarks,
                              self.MaterialConfirmityRemarks,
                              self.ProductConfirmityStyleRemarks,
                              self.ProductConfirmityColorRemarks,
                              self.ExportCartonPackingRemarks,
                              self.InnerCartonPackingRemarks,
                              self.ProductPackagingRemarks,
                              self.AssortmentRemarks,
                              self.LabelingRemarks,
                              self.MarkingRemarks,
                              self.WorkmanshipRemarks,
                              self.WorkmanshipAppearanceRemarks,
                              self.WorkmanshipFunctionRemarks,
                              self.WorkmanshipPrintedMaterialRemarks,
                              self.WorkmanshipFinishingRemarks,
                              self.MeasurementRemarks,
                              self.FabricWeightRemarks,
                              self.Cords_norm_otherRemarks,
                              self.InspectionConditionRemarks]
        
        arrayOfRemarks.append(contentsOf: Remarks_Array)
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tap(_:)))
//        self.LayoutView.addGestureRecognizer(tapGesture)
        self.AuditCode.text = AuditCodein
        self.AuditStage.text = AuditStagein
        self.AuditStage.backgroundColor = AuditStageColorin
        
        print(AuditCodein,AuditStagein,AuditStageColorin,ReportID_against_current_audit)
        self.ScrollView.delegate = self
        segmentedDefaultValues()
        print(segmentsValues)
    }

    
    
    // adjust the screen size when keyboard appears and hides
    
    func keyboardWillHide(_ sender: Notification) {
        let userInfo: [AnyHashable: Any] = sender.userInfo!
        let keyboardSize: CGSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue.size
      
        self.view.frame.origin.y += keyboardSize.height
    }
    
    func keyboardWillShow(_ sender: Notification) {
        let userInfo: [AnyHashable: Any] = sender.userInfo!
        let keyboardSize: CGSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue.size
        let offset: CGSize = (userInfo[UIKeyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size
        
        if keyboardSize.height == offset.height {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y -= keyboardSize.height
            })
        } else {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y += keyboardSize.height - offset.height
            })
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
    }
    
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        ScrollView.contentSize.height = LayoutView.frame.height + 2697
    }
    

    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
//        
//        for Remark:UITextField in arrayOfRemarks{
//            
//            if Remark.isFirstResponder{
//                
//                Remark.isUserInteractionEnabled = true
//                
//            }else{
//                
//                Remark.isUserInteractionEnabled = false
//            }
//        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
//        dispatch_async(dispatch_get_main_queue()) {
//            
//            textField.resignFirstResponder()
//            self.view.frame.origin.y = self.currentView_y!
//            self.ScrollView.scrollRectToVisible(textField.frame, animated: true)
//        }
    }
    
    

   
   // func jumpToNextTextField(TextField text_field:UITextField, withTag Tag:NSInteger) -> Void {
        
        // Gets the next responder from the view. Here we use self.view because we are searching for controls with
        // a specific tag, which are not subviews of a specific views, because each textfield belongs to the
        // content view of a static table cell.
        //
        // In other cases may be more convenient to use textField.superView, if all textField belong to the same view.
        
//        var nextResponder:UITextField!
//        
//        let subviews = self.LayoutView.subviews
//        for R in arrayOfRemarks{
//            if R.tag == Tag{
//                nextResponder = R
//            }
//        }
//        nextResponder = //.viewWithTag(Tag)!
//        if nextResponder != nil{
//            
//            if nextResponder.isKind(of: UITextField.self){
//                // If there is a next responder and it is a textfield, then it becomes first responder.
//                
//                nextResponder.becomeFirstResponder()
//                
//            }
//        }else{
//            // If there is not then removes the keyboard.
//            text_field.resignFirstResponder()
//        }
//
//    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()

//        var nextTag:NSInteger = textField.tag + 1
//        jumpToNextTextField(TextField: textField, withTag: nextTag)
//        return true
      
    
 
//        for Remark:UITextField in arrayOfRemarks{
//            
//            if Remark == textField{
//                    textField.resignFirstResponder()
//            }
//            
//            Remark.isUserInteractionEnabled = true
//        }
//        
//        switch textField {
//            
//        case ShippingRemarks:
//            
////            textField.resignFirstResponder()
//            MaterialConfirmityRemarks.becomeFirstResponder()
//            return true
//            
//        case MaterialConfirmityRemarks:
//            
////            textField.resignFirstResponder()
//            ProductConfirmityStyleRemarks.becomeFirstResponder()
//            return true
//            
//        case ProductConfirmityStyleRemarks:
//            
////            textField.resignFirstResponder()
//            ProductConfirmityColorRemarks.becomeFirstResponder()
//            return true
//            
//        case ProductConfirmityColorRemarks:
//            
////            textField.resignFirstResponder()
//            ExportCartonPackingRemarks.becomeFirstResponder()
//            return true
//            
//        case ExportCartonPackingRemarks:
//            
////            textField.resignFirstResponder()
//            InnerCartonPackingRemarks.becomeFirstResponder()
//            return true
//            
//        case InnerCartonPackingRemarks:
//            
////            textField.resignFirstResponder()
//            ProductPackagingRemarks.becomeFirstResponder()
//            return true
//            
//        case ProductPackagingRemarks:
//            
////            textField.resignFirstResponder()
//            AssortmentRemarks.becomeFirstResponder()
//            return true
//            
//        case AssortmentRemarks:
//            
////            textField.resignFirstResponder()
//            LabelingRemarks.becomeFirstResponder()
//            return true
//            
//        case LabelingRemarks:
//            
////            textField.resignFirstResponder()
//            MarkingRemarks.becomeFirstResponder()
//            return true
//            
//        case MarkingRemarks:
//            
////            textField.resignFirstResponder()
//            WorkmanshipRemarks.becomeFirstResponder()
//            return true
//            
//        case WorkmanshipRemarks:
//            
////            textField.resignFirstResponder()
//            WorkmanshipAppearanceRemarks.becomeFirstResponder()
//            return true
//            
//        case WorkmanshipAppearanceRemarks:
//            
////            textField.resignFirstResponder()
//            WorkmanshipFunctionRemarks.becomeFirstResponder()
//            return true
//            
//        case WorkmanshipFunctionRemarks:
//            
////            textField.resignFirstResponder()
//            WorkmanshipPrintedMaterialRemarks.becomeFirstResponder()
//            return true
//            
//        case WorkmanshipPrintedMaterialRemarks:
//            
////            textField.resignFirstResponder()
//            WorkmanshipFinishingRemarks.becomeFirstResponder()
//            return true
//            
//        case WorkmanshipFinishingRemarks:
//            
////            textField.resignFirstResponder()
//            MeasurementRemarks.becomeFirstResponder()
//            return true
//            
//        case MeasurementRemarks:
//            
////            textField.resignFirstResponder()
//            FabricWeightRemarks.becomeFirstResponder()
//            return true
//            
//        case FabricWeightRemarks:
//            
////            textField.resignFirstResponder()
//            Cords_norm_otherRemarks.becomeFirstResponder()  // as an example i put it here
//            return true
//
//        case Cords_norm_otherRemarks:
//            
////            textField.resignFirstResponder()
//            InspectionConditionRemarks.becomeFirstResponder()
//            return true
//            
//        case InspectionConditionRemarks:
//            
//            textField.resignFirstResponder()
//            
////            for Remark:UITextField in arrayOfRemarks{
////            
////                Remark.userInteractionEnabled = true
////            }
//            return true
//            
//        default:
//            
//            return true
//        }
////        return false
     return true
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(AddDefect.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        
        
        for Remark:UITextField in arrayOfRemarks{
            
            if Remark.isFirstResponder{
                Remark.resignFirstResponder()
            }
            Remark.isUserInteractionEnabled = true
        }
    }
    
    func tap(_ gestue: UITapGestureRecognizer){
       
        for Remark:UITextField in arrayOfRemarks{
            
            if Remark.isFirstResponder{
                Remark.resignFirstResponder()
            }
            Remark.isUserInteractionEnabled = true
        }
    }
    var segmentsValues = [String:String]()
    @IBAction func ChangeStatus(_ sender: UISegmentedControl) {
        
        if sender == ShippingStatus{
            
            switch ShippingStatus.selectedSegmentIndex {
            case 0:
                segmentsValues["ShippingStatus"] = "P"
            case 1:
                segmentsValues["ShippingStatus"] = "F"
            default:
                segmentsValues["ShippingStatus"] = ""
                
            }
            
        }else if sender == MaterialConfirmityStatus{
            
            switch MaterialConfirmityStatus.selectedSegmentIndex {
            case 0:
               segmentsValues["MaterialConfirmityStatus"] = "P"
            case 1:
                segmentsValues["MaterialConfirmityStatus"] = "F"
            default:
                segmentsValues["MaterialConfirmityStatus"] = ""
                
            }
            
        }
        else if sender == ProductConfirmityStyleStatus{
            
            switch ProductConfirmityStyleStatus.selectedSegmentIndex {
            case 0:
                segmentsValues["ProductConfirmityStyleStatus"] = "P"
            case 1:
                segmentsValues["ProductConfirmityStyleStatus"] = "F"
            default:
                segmentsValues["ProductConfirmityStyleStatus"] = ""
                
            }
            
        }
        else if sender == ProductConfirmityColorStatus{
            
            switch ProductConfirmityColorStatus.selectedSegmentIndex {
            case 0:
                segmentsValues["ProductConfirmityColorStatus"] = "P"
            case 1:
                segmentsValues["ProductConfirmityColorStatus"] = "F"
            default:
                segmentsValues["ProductConfirmityColorStatus"] = ""
                
            }
            
        }
        else if sender == ExportCartonPackingStatus{
            
            switch ExportCartonPackingStatus.selectedSegmentIndex {
            case 0:
                segmentsValues["ExportCartonPackingStatus"] = "P"
            case 1:
                segmentsValues["ExportCartonPackingStatus"] = "F"
            default:
                segmentsValues["ExportCartonPackingStatus"] = ""
                
            }
            
        }
        else if sender == InnerCartonPackingStatus{
            
            switch InnerCartonPackingStatus.selectedSegmentIndex {
            case 0:
                segmentsValues["InnerCartonPackingStatus"] = "P"
            case 1:
                segmentsValues["InnerCartonPackingStatus"] = "F"
            default:
                segmentsValues["InnerCartonPackingStatus"] = ""
                
            }
            
        }
        else if sender == ProductPackagingStatus{
            
            switch ProductPackagingStatus.selectedSegmentIndex {
            case 0:
                segmentsValues["ProductPackagingStatus"] = "P"
            case 1:
                segmentsValues["ProductPackagingStatus"] = "F"
            default:
                segmentsValues["ProductPackagingStatus"] = ""
                
            }
            
        }
        else if sender == AssortmentStatus{
            
            switch AssortmentStatus.selectedSegmentIndex {
            case 0:
                segmentsValues["AssortmentStatus"] = "P"
            case 1:
                segmentsValues["AssortmentStatus"] = "F"            default:
                segmentsValues["AssortmentStatus"] = ""
            }
            
        }
        else if sender == LabelingStatus{
            
            switch LabelingStatus.selectedSegmentIndex {
            case 0:
                segmentsValues["LabelingStatus"] = "P"
            case 1:
                segmentsValues["LabelingStatus"] = "F"
            default:
                segmentsValues["LabelingStatus"] = ""
                
            }
            
        }
        else if sender == MarkingStatus{
            
            switch MarkingStatus.selectedSegmentIndex {
            case 0:
                segmentsValues["MarkingStatus"] = "P"
            case 1:
                segmentsValues["MarkingStatus"] = "F"
            default:
                segmentsValues["MarkingStatus"] = ""
                
            }
            
        }
        else if sender == WorkmanshipStatus{
            
            switch WorkmanshipStatus.selectedSegmentIndex {
            case 0:
                segmentsValues["WorkmanshipStatus"] = "P"
            case 1:
                segmentsValues["WorkmanshipStatus"] = "F"
            default:
                segmentsValues["WorkmanshipStatus"] = ""
            }
            
        }
        else if sender == WorkmanshipAppearanceStatus{
            
            switch WorkmanshipAppearanceStatus.selectedSegmentIndex {
            case 0:
                segmentsValues["WorkmanshipAppearanceStatus"] = "P"
            case 1:
                segmentsValues["WorkmanshipAppearanceStatus"] = "F"
            default:
                segmentsValues["WorkmanshipAppearanceStatus"] = ""
                
            }
            
        }
        else if sender == WorkmanshipFunctionStatus{
            
            switch WorkmanshipFunctionStatus.selectedSegmentIndex {
            case 0:
                segmentsValues["WorkmanshipFunctionStatus"] = "P"
            case 1:
                segmentsValues["WorkmanshipFunctionStatus"] = "F"
            default:
                segmentsValues["WorkmanshipFunctionStatus"] = ""
                
            }
            
        }
        else if sender == WorkmanshipPrintedMaterialStatus{
            
            switch WorkmanshipPrintedMaterialStatus.selectedSegmentIndex {
            case 0:
                segmentsValues["WorkmanshipPrintedMaterialStatus"] = "P"
            case 1:
                segmentsValues["WorkmanshipPrintedMaterialStatus"] = "F"
            default:
                segmentsValues["WorkmanshipPrintedMaterialStatus"] = ""
                
            }
            
        }
        else if sender == WorkmanshipFinishingStatus{
            
            switch WorkmanshipFinishingStatus.selectedSegmentIndex {
            case 0:
                segmentsValues["WorkmanshipFinishingStatus"] = "P"
            case 1:
                segmentsValues["WorkmanshipFinishingStatus"] = "F"
            default:
                segmentsValues["WorkmanshipFinishingStatus"] = ""
                
            }
            
        }
        else if sender == MeasurementStatus{
            
            switch MeasurementStatus.selectedSegmentIndex {
            case 0:
                segmentsValues["MeasurementStatus"] = "P"
            case 1:
                segmentsValues["MeasurementStatus"] = "F"            default:
                segmentsValues["MeasurementStatus"] = ""
            }
            
        }
        else if sender == FabricWeightStatus{
            
            switch FabricWeightStatus.selectedSegmentIndex {
            case 0:
                segmentsValues["FabricWeightStatus"] = "P"
            case 1:
                segmentsValues["FabricWeightStatus"] = "F"            default:
                segmentsValues["FabricWeightStatus"] = ""
            }
            
        }
        
        else if sender == Cords_norm_otherStatus{
            
            switch Cords_norm_otherStatus.selectedSegmentIndex {
            case 0:
                segmentsValues["Cords_norm_otherStatus"] = "P"
            case 1:
                segmentsValues["Cords_norm_otherStatus"] = "F"
            default:
                segmentsValues["Cords_norm_otherStatus"] = ""
                
            }
            
        }
        
        else if sender == InspectionConditionStatus{
            
            switch InspectionConditionStatus.selectedSegmentIndex {
            case 0:
                segmentsValues["InspectionConditionStatus"] = "P"
            case 1:
                segmentsValues["InspectionConditionStatus"] = "F"
            default:
                segmentsValues["InspectionConditionStatus"] = ""
                
            }
            
        }
        
        else if sender == CalibratedScaleStatus{
            
            switch CalibratedScaleStatus.selectedSegmentIndex {
            case 0:
                segmentsValues["CalibratedScaleStatus"] = "P"
            case 1:
                segmentsValues["CalibratedScaleStatus"] = "F"
            default:
                segmentsValues["CalibratedScaleStatus"] = ""
            }
            
        }
        else if sender == CartonSticker{
            
            switch CartonSticker.selectedSegmentIndex {
            case 0:
                segmentsValues["CartonSticker"] = "P"
            case 1:
                segmentsValues["CartonSticker"] = "F"
            default:
                segmentsValues["CartonSticker"] = ""
            }
            
        }
        else if sender == WorkmanshipFitting{
            
            switch WorkmanshipFitting.selectedSegmentIndex {
            case 0:
                segmentsValues["WorkmanshipFitting"] = "P"
            case 1:
                segmentsValues["WorkmanshipFitting"] = "F"
            default:
                segmentsValues["WorkmanshipFitting"] = ""
            }
            
        }
        
    }
        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
            //            defaults.removeObjectForKey("userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
            //        case 4:
            //            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
            //                //                self.dismissViewControllerAnimated(true, completion: nil)
            //                presentViewController(resultController, animated: true, completion: nil)
            //            }
            //
            //            let selectedImage:UIImage = UIImage(named: "Selected QAReport")!
            //            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            //
        //            break
        case 5:
            if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }else{
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }
            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_selected_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break

        default:
            break
        }
    }
    
    
    var bodyData = ""
    
    @IBAction func saveAndContinue(_ sender: UIButton) {
        
        
        //self.performSegue(withIdentifier: "gotoAuditQuantity", sender: nil)
      saveOfflineCommentsData()
        }
    func segmentedDefaultValues(){
        
        segmentsValues["ShippingStatus"] = ""
        segmentsValues["MaterialConfirmityStatus"] = ""
        segmentsValues["ProductConfirmityStyleStatus"] = ""
        segmentsValues["ProductConfirmityColorStatus"] = ""
        segmentsValues["ExportCartonPackingStatus"] = ""
        segmentsValues["InnerCartonPackingStatus"] = ""
        segmentsValues["ProductPackagingStatus"] = ""
        segmentsValues["AssortmentStatus"] = ""
        segmentsValues["LabelingStatus"] = ""
        segmentsValues["MarkingStatus"] = ""
        segmentsValues["WorkmanshipStatus"] = ""
        segmentsValues["WorkmanshipAppearanceStatus"] = ""
        segmentsValues["WorkmanshipFunctionStatus"] = ""
        segmentsValues["WorkmanshipPrintedMaterialStatus"] = ""
        segmentsValues["WorkmanshipFinishingStatus"] = ""
        segmentsValues["MeasurementStatus"] = ""
        segmentsValues["FabricWeightStatus"] = ""
        segmentsValues["Cords_norm_otherStatus"] = ""
        segmentsValues["InspectionConditionStatus"] = ""
        segmentsValues["CalibratedScaleStatus"] = ""
        segmentsValues["CartonSticker"] = ""

        segmentsValues["WorkmanshipFitting"] = ""

        
    }
 //Saving data offline
    func parseCommentsJSON(_ jsonString: String) -> [String:String] {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:String] {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }

    
    func saveOfflineCommentsData(){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        print("You are in AuditComments OFFLINE mode.")
        
        // Creating AuditComment Json file
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("AuditSummaryFile\(self.AuditCode.text!).json")
        
        // creating a .json file in the Documents folder
        
        if !fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("File created ")
                
                // Now save it to FilesToSync.json file
                
                saveFileNameTo_FilesToSync("AuditSummaryFile\(self.AuditCode.text!).json")
                //(jsonFilePath.absoluteString)
                
            } else {
                print("Couldn't create file for some reason")
            }
        } else {
            print("File already exists")
        }
        //file creation ends
        
        
        
        //First Read out the AuditComment AuditCodeFile
        
        do{
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath.absoluteString, encoding: String.Encoding.utf8.rawValue) as String
            
            print("readAuditComment_String = ",readString)
            
            /// here send readString to function to update values stored in AuditCommentFile(AuditCode) File
            
            self.AuditCommentAuditCodeFile(readString, AuditCommentFilePath: jsonFilePath.absoluteString)
            
        }
        catch let error as NSError{
            print("There is an error while reading AddDeffectText(AuditCode) File.",error.description)
        }
        
    }// end of func
    
    
    func AuditCommentAuditCodeFile(_ savedCommentsData:String?, AuditCommentFilePath: String) -> Void {
        
        
        print(savedCommentsData!,AuditCommentFilePath)
        if ReportID_against_current_audit == "32"{
            //if saveContinue_Pressed == true{
            // save data against Arcadia only
            
            AuditCommentAuditCodeFile_Arcadia(savedCommentsData, AuditCommentFilePath: AuditCommentFilePath)
            
        }
        else if ReportID_against_current_audit == "46"{
            //if saveContinue_Pressed == true{
            // save data against Arcadia only
            
            AuditCommentAuditCodeFile_Arcadia(savedCommentsData, AuditCommentFilePath: AuditCommentFilePath)
            
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        //Reading from JsonDictionary File stored in documents directory
        do{
            var readString: String
            readString = try NSString(contentsOfFile: AuditCommentFilePath, encoding: String.Encoding.utf8.rawValue) as String
            //                    print("readString=\(readString)")
            let readStringParsed = parseCommentsJSON(readString)
            print("AuditComments_readStringParsed = ",readStringParsed)
        }
        catch let error as NSError{
            print("There is an error while reading from JsonDictionary File.",error.description)
            
        }
        
    }
    
    func AuditCommentAuditCodeFile_Arcadia(_ savedCommentsData:String?, AuditCommentFilePath: String) -> Void{
        
        // - - - -
        
        if savedCommentsData != nil && savedCommentsData != "" {
            
//            if self.AuditResult == nil{
//                
//                self.AuditResult = "F"
//            }
//            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodein!)&AuditResult=\(self.AuditResult!)&Remarks=\(self.Comments_Arcadia.text!)"
//            print("AuditCommentsArcadia2-bodydata -> \(self.bodyData)")
            //            //&CartonSticker=\(self.!)&CartonStickerRemarks=\(self.PolybagReason.text!)
            //&CalibratedScalesRemarks=\(self.cali.text!)
            //&WorkmanshipFitting=\(self.!)&WorkmanshipFittingRemarks=\(self.fitt.text!)&CartonSticker=\(self.!)&CartonStickerRemarks=\(self.PolybagReason.text!)
            bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCode.text!)&ShippingMarks=\(segmentsValues["ShippingStatus"]!)&ShippingMarksRemarks=\(self.ShippingRemarks.text!)&MaterialConformity=\(segmentsValues["MaterialConfirmityStatus"]!)&MaterialConformityRemarks=\(self.MaterialConfirmityRemarks.text!)&ProductStyle=\(segmentsValues["ProductConfirmityStyleStatus"]!)&ProductStyleRemarks=\(self.ProductConfirmityStyleRemarks.text!)&ProductColour=\(segmentsValues["ProductConfirmityColorStatus"]!)&ProductColourRemarks=\(self.ProductConfirmityColorRemarks.text!)&ExportCartonPacking=\(segmentsValues["ExportCartonPackingStatus"]!)&ExportCartonPackingRemarks=\(self.ExportCartonPackingRemarks.text!)&InnerCartonPacking=\(segmentsValues["InnerCartonPackingStatus"]!)&InnerCartonPackingRemarks=\(self.InnerCartonPackingRemarks.text!)&ProductPackaging=\(segmentsValues["ProductPackagingStatus"]!)&ProductPackagingRemarks=\(self.ProductPackagingRemarks.text!)&Assortment=\(segmentsValues["AssortmentStatus"]!)&AssortmentRemarks=\(self.AssortmentRemarks.text!)&Labeling=\(segmentsValues["LabelingStatus"]!)&LabelingMarksRemarks=\(self.LabelingRemarks.text!)&Markings=\(segmentsValues["MarkingStatus"]!)&MarkingsRemarks=\(self.MarkingRemarks.text!)&Workmanship=\(segmentsValues["WorkmanshipStatus"]!)&WorkmanshipRemarks=\(self.WorkmanshipRemarks.text!)&Function=\(segmentsValues["WorkmanshipFunctionStatus"]!)&FunctionRemarks=\(self.WorkmanshipFunctionRemarks.text!)&Appearance=\(segmentsValues["WorkmanshipAppearanceStatus"]!)&AppearanceRemarks=\(self.WorkmanshipAppearanceRemarks.text!)&PrintedMaterials=\(segmentsValues["WorkmanshipPrintedMaterialStatus"]!)&PrintedMaterialsRemarks=\(self.WorkmanshipPrintedMaterialRemarks.text!)&WorkmanshipFinishing=\(segmentsValues["WorkmanshipFinishingStatus"]!)&WorkmanshipFinishingRemarks=\(self.WorkmanshipFinishingRemarks.text!)&Measurement=\(segmentsValues["MeasurementStatus"]!)&MeasurementRemarks=\(self.MeasurementRemarks.text!)&FabricWeight=\(segmentsValues["FabricWeightStatus"]!)&FabricWeightRemarks=\(self.FabricWeightRemarks.text!)&WovenWeight=\(segmentsValues["CalibratedScaleStatus"]!)&WovenWeightRemarks=\(self.CalibratedScaleRemarks.text!)&CordNorm=\(segmentsValues["Cords_norm_otherStatus"]!)&CordNormRemarks=\(self.Cords_norm_otherRemarks.text!)&InspectionConditions=\(segmentsValues["InspectionConditionStatus"]!)&InspectionConditionsRemarks=\(self.InspectionConditionRemarks.text!)&Remarks1=\(self.Remarks_Array[0].text!)&Remarks2=\(self.Remarks_Array[1].text!)&Remarks3=\(self.Remarks_Array[2].text!)&Remarks4=\(self.Remarks_Array[3].text!)&WorkmanshipFitting=\(segmentsValues["WorkmanshipFitting"]!)&WorkmanshipFittingRemarks=\(self.WorkmanshipFittingRemarks.text!)&CartonSticker=\(segmentsValues["CartonSticker"]!)&CartonStickerRemarks=\(self.CartonStickerRemarks.text!)"
            //
            print("Packaging_bodyData = ",bodyData)
            
            
            var AuditCommentsOfflineData:[String:String] = parseCommentsJSON(savedCommentsData!)
            
            
            let randomString = randomStringWithLength(6)
            
            AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "AuditSummary_bodyData\(self.AuditCode.text!)_\(randomString)")
            AuditCommentsOfflineData.updateValue("http://app.3-tree.com/quonda/save-audit-summary.php", forKey: "AuditSummary_url")
            
            
            if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    //Note!!
                    
                    if !Reachability.isConnectedToNetwork(){
                        
                    }
                    
                    
                    // now move onto the Next screen
                    //???????
                    //?????
                    //???
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    defaults.set("Completed", forKey: "AuditSummary\(AuditCodein!)")

                    //performSegue(withIdentifier: "goto Final comments", sender: nil)
                    self.performSegue(withIdentifier: "gotoAuditQuantity", sender: nil)

                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
            
        }else{
            
//            if self.AuditResult == nil{
//                
//                self.AuditResult = "F"
//            }
//            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodein!)&AuditResult=\(self.AuditResult!)&Remarks=\(self.Comments_Arcadia.text!)"
//            
            //&CartonSticker=\(self.!)&CartonStickerRemarks=\(self.PolybagReason.text!)
            //&CalibratedScalesRemarks=\(self.cali.text!)
            //&WorkmanshipFitting=\(self.!)&WorkmanshipFittingRemarks=\(self.fitt.text!)
            
            bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCode.text!)&ShippingMarks=\(segmentsValues["ShippingStatus"]!)&ShippingMarksRemarks=\(self.ShippingRemarks.text!)&MaterialConformity=\(segmentsValues["MaterialConfirmityStatus"]!)&MaterialConformityRemarks=\(self.MaterialConfirmityRemarks.text!)&ProductStyle=\(segmentsValues["ProductConfirmityStyleStatus"]!)&ProductStyleRemarks=\(self.ProductConfirmityStyleRemarks.text!)&ProductColour=\(segmentsValues["ProductConfirmityColorStatus"]!)&ProductColourRemarks=\(self.ProductConfirmityColorRemarks.text!)&ExportCartonPacking=\(segmentsValues["ExportCartonPackingStatus"]!)&ExportCartonPackingRemarks=\(self.ExportCartonPackingRemarks.text!)&InnerCartonPacking=\(segmentsValues["InnerCartonPackingStatus"]!)&InnerCartonPackingRemarks=\(self.InnerCartonPackingRemarks.text!)&ProductPackaging=\(segmentsValues["ProductPackagingStatus"]!)&ProductPackagingRemarks=\(self.ProductPackagingRemarks.text!)&Assortment=\(segmentsValues["AssortmentStatus"]!)&AssortmentRemarks=\(self.AssortmentRemarks.text!)&Labeling=\(segmentsValues["LabelingStatus"]!)&LabelingMarksRemarks=\(self.LabelingRemarks.text!)&Markings=\(segmentsValues["MarkingStatus"]!)&MarkingsRemarks=\(self.MarkingRemarks.text!)&Workmanship=\(segmentsValues["WorkmanshipStatus"]!)&WorkmanshipRemarks=\(self.WorkmanshipRemarks.text!)&Function=\(segmentsValues["WorkmanshipFunctionStatus"]!)&FunctionRemarks=\(self.WorkmanshipFunctionRemarks.text!)&Appearance=\(segmentsValues["WorkmanshipAppearanceStatus"]!)&AppearanceRemarks=\(self.WorkmanshipAppearanceRemarks.text!)&PrintedMaterials=\(segmentsValues["WorkmanshipPrintedMaterialStatus"]!)&PrintedMaterialsRemarks=\(self.WorkmanshipPrintedMaterialRemarks.text!)&WorkmanshipFinishing=\(segmentsValues["WorkmanshipFinishingStatus"]!)&WorkmanshipFinishingRemarks=\(self.WorkmanshipFinishingRemarks.text!)&Measurement=\(segmentsValues["MeasurementStatus"]!)&MeasurementRemarks=\(self.MeasurementRemarks.text!)&FabricWeight=\(segmentsValues["FabricWeightStatus"]!)&FabricWeightRemarks=\(self.FabricWeightRemarks.text!)&WovenWeight=\(segmentsValues["CalibratedScaleStatus"]!)&WovenWeightRemarks=\(self.CalibratedScaleRemarks.text!)&CordNorm=\(segmentsValues["Cords_norm_otherStatus"]!)&CordNormRemarks=\(self.Cords_norm_otherRemarks.text!)&InspectionConditions=\(segmentsValues["InspectionConditionStatus"]!)&InspectionConditionsRemarks=\(self.InspectionConditionRemarks.text!)&Remarks1=\(self.Remarks_Array[0].text!)&Remarks2=\(self.Remarks_Array[1].text!)&Remarks3=\(self.Remarks_Array[2].text!)&Remarks4=\(self.Remarks_Array[3].text!)&WorkmanshipFitting=\(segmentsValues["WorkmanshipFitting"]!)&WorkmanshipFittingRemarks=\(self.WorkmanshipFittingRemarks.text!)&CartonSticker=\(segmentsValues["CartonSticker"]!)&CartonStickerRemarks=\(self.CartonStickerRemarks.text!)"
            //
            print("Packaging_bodyData = ",bodyData)
            print("AuditCommentsArcadia-bodydata -> \(self.bodyData)")
            
            
            
            var AuditCommentsOfflineData = [String:String]()
            
            
            let randomString = randomStringWithLength(6)
            
            AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "AuditSummary_bodyData\(self.AuditCode.text!)_\(randomString)")
            AuditCommentsOfflineData.updateValue("http://app.3-tree.com/quonda/save-audit-summary.php", forKey: "AuditSummary_url")
            
            
            if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    // now move onto the AuditorModeViewController screen
                    //?????
                    //???
                    //??
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    //performSegue(withIdentifier: "goto Final comments", sender: nil)
                    defaults.set("Completed", forKey: "AuditSummary\(AuditCodein!)")

                    self.performSegue(withIdentifier: "gotoAuditQuantity", sender: nil)

                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
        }
        
        // - - -
        
    }
    //Random string function
    
    func randomStringWithLength(_ len: Int) -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for i in 0 ..< (len){
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "gotoAuditQuantity"{
            let destinationVC = segue.destination as! AuditQuantity
            destinationVC.AuditCodein = AuditCodein
            destinationVC.AuditStagein = self.AuditStage.text!
            destinationVC.AuditStageColorin = self.AuditStage.backgroundColor!
            
            // here send ReportId to commentsVC for hide/show between two views in commentsView.
            destinationVC.ReportID_against_current_audit = self.ReportID_against_current_audit
            
            
            present(destinationVC, animated: true, completion: nil)
            
        }
        
    }
    

    
    
}
