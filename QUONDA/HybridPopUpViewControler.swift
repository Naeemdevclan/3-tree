//
//  HybridPopUpViewControler.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 20/12/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit


protocol SavingHybridDelegate
{
    func savePackagingAuditResult(_ strText : String, strID : String)
    func saveGarmentConformityAuditResult(_ strText : String, strID : String)
}

class HybridPopUpViewControler: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var pickerdata = [String]()
    var pickerdataIDs = [String]()
    
    var tableCellData = [String]()
    var tableCellDataIDs = [String]()
    
    var WhichButton = String()
    
    
    var delegate : SavingHybridDelegate?
    
    
    var PopUpselectedText = ""
    var PopUpselectedID = ""
    
    var AllreadySelectedText = String()
    var selectedRowText = String()
    
    var selectedButton:NSMutableArray! = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if !AllreadySelectedText.isEmpty{
            
            selectedRowText = AllreadySelectedText
        }
        
        print("selectedRowText =",selectedRowText)
        showAnimate()
        
        //        self.view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        
        if !pickerdata.isEmpty {
            tableCellData = pickerdata
            
        }
        
        if !pickerdataIDs.isEmpty{
            tableCellDataIDs = pickerdataIDs
            
        }
        else{
            tableCellDataIDs = tableCellData
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func CancelPressed(_ sender: AnyObject) {
        
        UIView.animate(withDuration: 0.17, animations: {
            self.view.transform = CGAffineTransform(scaleX: 0.2, y: 0.2) // onstart it will make self.view from Normal to Smaller
            self.view.alpha = 0.0 // This alpha value which is zero, means white color
            
        }, completion: { (finished) in
            if finished {
                self.view.removeFromSuperview()
            }
        })
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableCellData.count  //10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        
        cell.textLabel!.text = tableCellData[indexPath.row]
        cell.detailTextLabel!.text = tableCellDataIDs[indexPath.row]
        
        cell.detailTextLabel?.isHidden = true
        
        // Adding checkmarks to the rows
        
        if selectedRowText == cell.textLabel!.text! {
            cell.accessoryType = .checkmark
            
        }else {
            cell.accessoryType = .none
            
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("cell Tapped.")
        
        PopUpselectedText = tableView.cellForRow(at: indexPath)!.textLabel!.text!
        PopUpselectedID = tableView.cellForRow(at: indexPath)!.detailTextLabel!.text!
        
        
        
        if WhichButton == "PackagingAuditResult" {
            
            if((self.delegate) != nil)
            {
                delegate?.savePackagingAuditResult(PopUpselectedText, strID: PopUpselectedID) //("Report Here")
            }
            
        }
        else if WhichButton == "GarmentAuditResult" {
            
            if((self.delegate) != nil)
            {
                delegate?.saveGarmentConformityAuditResult(PopUpselectedText, strID: PopUpselectedID)  //("stageHere")
            }
            
        }
       
        
        
        // adding Checkmark to the selected row
        
        let cellPressed = tableView.cellForRow(at: indexPath)!
        cellPressed.accessoryType = .checkmark
        
        
        //      // - - Dismiss the current view - - - - - - -
        
        removeAnimate()
        
        
        //        self.view.removeFromSuperview()
    }
    
    func showAnimate() -> Void {
        self.view.transform = CGAffineTransform(scaleX: 0.5, y: 0.5) // onstart it will make self.view  from smaller to a little bigger
        self.view.alpha = 0.0 // This alpha value which is zero, means white color
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0) // Now when animation Ended it will make self.view to normal
        })
    }
    
    func removeAnimate() -> Void {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 0.5, y: 0.5) // onstart it will make self.view a little big than normal
            self.view.alpha = 0.0 // This alpha value which is zero, means white color
            
        }, completion: { (finished) in
            if finished {
                //                    self.dismissViewControllerAnimated(true, completion: nil)
                self.view.removeFromSuperview()
            }
        })
    }
    
    //
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
