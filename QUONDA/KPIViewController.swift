//
//  KPIViewController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 15/03/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
import WebKit

var currentPressentingViewController:UIViewController!
var AuditCodeOfDashboardCell:String?
var AuditCodeFromAuditorMode:String!
var isKPIpresented:Bool!

class KPIViewController: UIViewController, WKScriptMessageHandler, WKNavigationDelegate, UITabBarDelegate,URLSessionDownloadDelegate, UIDocumentInteractionControllerDelegate{
    
    // WKScriptMessageHandler protocol provides a method for receiving messages from JavaScript running in a webpage.
    
    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    weak var tab1vc: UIViewController!
    weak var tab2vc: UIViewController!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!

    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    
//    @IBOutlet weak var KpiWebView: UIWebView!
//    @IBOutlet weak var viewForWeb: UIView!
    
    var KpiWebView:WKWebView?
    
    @IBOutlet weak var progressView: UIProgressView!

    
    var DownloadProgressView = UIView()

    
    
    // attributes for download and save the Report.
  
    var downloadTask: URLSessionDownloadTask!
   
    var backgroundSession: Foundation.URLSession!
    
    var viewer : UIDocumentInteractionController?
    
    var downloadReport:Bool!
    
    //- - End of Report Attributes - - 
    
    // Email Report Attributes
    
    var AuditCodeFromEmailVC:String!
    
    //- - End of Email Report Attributes
    
    
    var AuditCodeForPDF:String?
    var AuditCodeForKeyStatsImages:String!
    
    var AuditCodeForEmailQAReport:String!
    
    
    let isDirectory: ObjCBool = false
    
    
    var buttonClicked:Int = 0
    
    var webConfig:WKWebViewConfiguration {
        get {
            
            // Create WKWebViewConfiguration instance
            let webCfg:WKWebViewConfiguration = WKWebViewConfiguration()
            
            // Setup WKUserContentController instance for injecting user script
            let userController:WKUserContentController = WKUserContentController()
            
            // Create/Add a script message handler named "buttonClicked" for receiving event notifications posted from the JS document using window.webkit.messageHandlers.buttonClicked.postMessage script message
            userController.add(self, name: "buttonClicked")
            
            // Get script that's to be injected into the document
            let js:String = buttonClickEventTriggeredScriptToAddToDocument()
            print(js)
            // Specify when and where and what user script needs to be injected into the web document
            let userScript:WKUserScript =  WKUserScript(source: js, injectionTime: WKUserScriptInjectionTime.atDocumentEnd, forMainFrameOnly: false)
            
            // Add the user script to the WKUserContentController instance
            userController.addUserScript(userScript)
            
            // Configure the WKWebViewConfiguration instance with the WKUserContentController
            webCfg.userContentController = userController;
            
            return webCfg;
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        
        selectedArrowButton.removeAllObjects()
        previousButtonTag = nil
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        DownloadProgressView.hidden = true
        
//        if self.presentingViewController == nil{
//            currentPressentingViewController = self.presentingViewController
//        }
        
//        self.progressView.hidden = false
        downloadReport = false
//        progressView.setProgress(0.0, animated: false)
        // try using self.view.frame.size.width instead of 375 height
        
       KpiWebView = WKWebView(frame: CGRect(x: 0, y: 69, width: self.view.frame.size.width, height: self.view.frame.size.height - 120), configuration: webConfig)  // previous height was 549
        
        // Delegate to handle navigation of web content
        KpiWebView!.navigationDelegate = self
        
        self.view.addSubview(KpiWebView!)

        // Do any additional setup after loading the view.
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "Selected QAReport")!, UIImage(named: "Auditor Mode")!, UIImage(named: "Selected Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)

        
        print(AuditCodeOfDashboardCell)
        
    }
    

//    func noDataFound(showMessage:Bool){
//        
//        let alert:UIAlertController = UIAlertController(title: nil, message: "Downloading QAReport..", preferredStyle: UIAlertControllerStyle.Alert)
//
//        if showMessage == true{
////           let topVC = self.topMostController()
//            self.presentViewController(alert, animated: true, completion: nil)
//       
//        }else{
////            alert.removeFromParentViewController()
//            alert.dismissViewControllerAnimated(true, completion: nil)
//        }
//    }
    
    override func viewDidAppear(_ animated: Bool) {
//       self.view.window?.makeKeyAndVisible()
        print("KPI view Appeared.")
        
        // Add Downloading Progress of QAReport
        
        DownloadProgressView = UIView(frame: CGRect(x: 0, y: 0 , width: view.frame.width, height: view.frame.height))
        DownloadProgressView.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        DownloadProgressView.tag = 1
        
        let strLabel = UILabel(frame: CGRect(x: view.frame.midX - 100, y: view.frame.midY - 50, width: 250, height: 60))
            strLabel.text = "Please Wait Downloading.."
            strLabel.textColor = UIColor.white
        
        DownloadProgressView.addSubview(strLabel)
        
        // End of Adding Downloading Progress
        
        
        
        print(AuditCodeOfDashboardCell)
       
        isKPIpresented = true
        
//         print(self.view.window!.rootViewController)
//        self.progressView.hidden = false
        
        downloadReport = false
        progressView.setProgress(0.0, animated: false)

        
        let backgroundSessionConfiguration = URLSessionConfiguration.background(withIdentifier: "backgroundSession")
       
        backgroundSession = Foundation.URLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
        
//        progressView.setProgress(0.0, animated: false)
        
        
        var requestURL: URL!
        var request: URLRequest!
        // loading data from web
        print("AuditCodeOfDashboardCell = \(AuditCodeOfDashboardCell)")
       
        
        if AuditCodeOfDashboardCell != nil{
           
            print("AuditCodeOfDashboardCell not nil = \(AuditCodeOfDashboardCell)")
            //setting the AuditCode for keySatsAndImages
            
            AuditCodeForKeyStatsImages = AuditCodeOfDashboardCell!
            
            AuditCodeForEmailQAReport = AuditCodeOfDashboardCell!
            
            print("1-AuditCodeForKeyStatsImages=\(AuditCodeForKeyStatsImages)")
            print(AuditCodeOfDashboardCell)
            requestURL = URL(string:"http://app.3-tree.com/quonda/kpis.php?User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodeOfDashboardCell!)&App=IOS")
            request = URLRequest(url: requestURL!)
            AuditCodeForPDF = AuditCodeOfDashboardCell
            AuditCodeOfDashboardCell = nil
            
        }else if AuditCodeFromAuditorMode != nil{
           
            print("AuditCodeOfDashboardCell not nil = \(AuditCodeFromAuditorMode)")
            //setting the AuditCode for keySatsAndImages
            
            AuditCodeForKeyStatsImages = AuditCodeFromAuditorMode!
            print("1-AuditCodeForKeyStatsImages=\(AuditCodeForKeyStatsImages)")
            
            print(AuditCodeFromAuditorMode)
            AuditCodeForEmailQAReport = AuditCodeFromAuditorMode
            
            requestURL = URL(string:"http://app.3-tree.com/quonda/kpis.php?User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodeFromAuditorMode!)&App=IOS")
            request = URLRequest(url: requestURL!)
            AuditCodeForPDF = AuditCodeFromAuditorMode
            AuditCodeFromAuditorMode = nil
        
        }
        else if AuditCodeFromEmailVC != nil{
         
            requestURL = URL(string:"http://app.3-tree.com/quonda/kpis.php?User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodeFromEmailVC!)&App=IOS")
            request = URLRequest(url: requestURL!)
            AuditCodeForPDF = AuditCodeFromEmailVC
            
            AuditCodeFromEmailVC = nil
        }
        else{
            
            requestURL = URL(string:"http://app.3-tree.com/quonda/kpis.php?User=\(defaults.object(forKey: "userid")as! String)&App=IOS")
            request = URLRequest(url: requestURL!)
        }
        
        
        KpiWebView!.load(request)
        print(self.presentingViewController)
        print(self.presentedViewController)
        print(AuditCodeForPDF)
    }
 
    
    // WKNavigationDelegate
    
    // called when a main frame Navigation starts
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    // called when a main frame Navigation ends
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        NSLog("%s", #function)
    }

    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        NSLog("%s. With Error %@", #function,error as NSError)
        showAlertWithMessage("Failed to load file with error \(error.localizedDescription)!")
    }
    
    // Button Click Script to Add to Document
    
    func buttonClickEventTriggeredScriptToAddToDocument() ->String{
        
        // Script: When window is loaded, execute an anonymous function that adds a "click" event handler function to the "ClickMeButton" button element. The "click" event handler calls back into our native code via the window.webkit.messageHandlers.buttonClicked.postMessage call
       print("called1")
        var script:String?
        
        if let filePath:String = Bundle(for: KPIViewController.self).path(forResource: "userScriptClickEvents", ofType:"js") {
            
            script = try? String (contentsOfFile: filePath, encoding: String.Encoding.utf8)
        }
        return script!;
        
    }
    
    
    // invoked when a script message is recieved from the web page
    // WKScriptMessageHandler Delegate Method
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        print("buttonclicked..")
        if let messageBody:NSDictionary = message.body as? NSDictionary {
           
            print("MessageBody=\(messageBody)")
            
            //BrandBtnId
            //VendorBtnId 
            
            if let idOfTappedButton:String = messageBody["BrandBtnId"] as? String { // this check is just for debug case
                print("BrandValue= \(idOfTappedButton)")
                print("key2 = \(messageBody["key2"] as? String)")
                print("key3 = \(messageBody["key3"] as? String)")
                print("you have tapped Brand")
            }
            else if let idOfTappedButton:String = messageBody["VendorBtnId"] as? String { // this check is just for debug case
                print("VendorValue= \(idOfTappedButton)")
                print("you have tapped Vendor")
            }
            
           else if let idOfTappedButton:String = messageBody["KeyStatsBtnId"] as? String{   // it is getting dateRangeValue that is passing from js-script
               
                print("dateRangeValue= \(idOfTappedButton)")
                print("selected brand = \(messageBody["brand"])")
                print("selected vendor = \(messageBody["vendor"])")
                print("you have tapped..")
                
                if storyboard!.instantiateViewController(withIdentifier: "KeyStatsImages") is KeyStatsImagesViewController {
                    
//                    presentViewController(resultController, animated: true, completion: nil)
                    print("AuditCodeForKeyStatsImages=\(AuditCodeForKeyStatsImages)")
                    
                    let dataToSend: [String:String] = [
                        "brand" : "\(messageBody["brand"]!)",
                        "vendor" : "\(messageBody["vendor"]!)",
                        "dateRange" : "\(idOfTappedButton)",
                        "InspectionType" : "\(messageBody["InspectionType"])"
                    ]
                    
//                    if AuditCodeForKeyStatsImages != nil{
                    
                        performSegue(withIdentifier: "goToKeyStatsAndImages", sender: dataToSend )
//                    }
                }
            }
            //- - - - - - - - - -
            else if let idOfTappedButton:String = messageBody["DateRangeBtnId"] as? String { // this check is just for debug case
                print("dateRangeValue= \(idOfTappedButton)")
                print("you have tapped date btn")
            }
                
            else if let idOfTappedButton:String = messageBody["AuditProgression"] as? String{
               
                print("AuditProgression= \(idOfTappedButton)")
                print("selected brand = \(messageBody["brand"] as? String)")
                print("selected vendor = \(messageBody["vendor"] as? String)")
                
                
                var dataToSend: [String:String] = [
                    "brand" : "\(messageBody["brand"]!)",
                    "vendor" : "\(messageBody["vendor"]!)",
                    "dateRange" : "\(idOfTappedButton)",
                    "InspectionType" : "\(messageBody["InspectionType"])"
                ]

                var auditCodeurl = ""
                if let auditcode = messageBody["InspectionType"]{
                    auditCodeurl = "&Audits=\(auditcode)"
                }
                else{
                    auditCodeurl = ""
                }
                var AuditCodeForKeyStatsImagesURL = ""
                if let auditKeyStatsImagesURL = AuditCodeForKeyStatsImages{
                    AuditCodeForKeyStatsImagesURL = "&AuditCode=\(auditKeyStatsImagesURL)"
                }
                else{
                    AuditCodeForKeyStatsImagesURL = ""
                }


             //   if AuditCodeForKeyStatsImages != nil{
                    let requestURL = URL(string:"http://app.3-tree.com/quonda/audit-progression.php?User=\(defaults.object(forKey: "userid")as! String)\(AuditCodeForKeyStatsImagesURL)&DateRange=\(dataToSend["dateRange"]!)&Brand=\(dataToSend["brand"]!)&Vendor=\(dataToSend["vendor"]!)\(auditCodeurl)")
             //   print("http://portal.3-tree.com/api/android/quonda/audit-progression.php?User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodeForKeyStatsImages!)&DateRange=\(dataToSend["dateRange"]!)&Brand=\(dataToSend["brand"]!)&Vendor=\(dataToSend["vendor"]!)&Audits=\(dataToSend["InspectionType"]!)")
                    
                    print("request for AuditProgression =",requestURL!)
                    
                    let request = URLRequest(url: requestURL!)
                    KpiWebView!.load(request)
               // }
            }
                
            else if let idOfTappedButton:String = messageBody["BrandProgression"] as? String{
                
                print("message Body = \(messageBody)")
                print("BrandProgression= \(idOfTappedButton)")
                print("selected brand = \(messageBody["brand"] as? String)")
                print("selected vendor = \(messageBody["vendor"] as? String)")
                
                var AuditCodeForKeyStatsImagesURL = ""
                if let auditKeyStatsImagesURL = AuditCodeForKeyStatsImages{
                    AuditCodeForKeyStatsImagesURL = "&AuditCode=\(auditKeyStatsImagesURL)"
                }
                else{
                    AuditCodeForKeyStatsImagesURL = ""
                }

                var dataToSend: [String:String] = [
                    "brand" : "\(messageBody["brand"]!)",
                    "vendor" : "\(messageBody["vendor"]!)",
                    "dateRange" : "\(idOfTappedButton)",
                ]

//                if AuditCodeForKeyStatsImages != nil{
                    let requestURL = URL(string:"http://app.3-tree.com/quonda/brand-progression.php?User=\(defaults.object(forKey: "userid")as! String)\(AuditCodeForKeyStatsImagesURL)&DateRange=\(dataToSend["dateRange"]!)&Brand=\(dataToSend["brand"]!)&Vendor=\(dataToSend["vendor"]!)")
                    
                    print("request for BrandProgression =",requestURL!)
                    
                    let request = URLRequest(url: requestURL!)
                    KpiWebView!.load(request)
//                }
            }

        }
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            
            // First check If filedownloading is in process Then do CANCEL the download.
            if downloadTask != nil{
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                progressView.setProgress(0.0, animated: false)
                downloadTask.cancel()
                downloadTask = nil
            }
            
            
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
//            defaults.removeObjectForKey("userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            
            // First check If filedownloading is in process Then do CANCEL the download.
            if downloadTask != nil{
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                progressView.setProgress(0.0, animated: false)
                downloadTask.cancel()
                downloadTask = nil
            }
            
            
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            
            // First check If filedownloading is in process Then do CANCEL the download.
            if downloadTask != nil{
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                progressView.setProgress(0.0, animated: false)
                downloadTask.cancel()
                downloadTask = nil
            }
            
  
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 4:
            /*
             
             if downloadTask != nil{
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                progressView.setProgress(0.0, animated: false)
                downloadTask.cancel()
//                downloadTask = nil
            }

            performDocumentDownload()
          
             */
            
            showReportOptions()
            
            break
        case 5:
            
            // First check If filedownloading is in process Then do CANCEL the download.
            if downloadTask != nil{
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                progressView.setProgress(0.0, animated: false)
                downloadTask.cancel()
                downloadTask = nil
            }
            
            
            
            if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }else{
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }
            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 6:
            
            // First check If filedownloading is in process Then do CANCEL the download.
            if downloadTask != nil{
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                progressView.setProgress(0.0, animated: false)
                downloadTask.cancel()
                downloadTask = nil
            }
            
            
//            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("6") as? KPIViewController {
//                //                self.dismissViewControllerAnimated(true, completion: nil)
//                presentViewController(resultController, animated: true, completion: nil)
//            }
            
//            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
//            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            
            break
        case 7:
            
            // First check If filedownloading is in process Then do CANCEL the download.
            if downloadTask != nil{
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                progressView.setProgress(0.0, animated: false)
                downloadTask.cancel()
                downloadTask = nil
            }
            
            
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            
            // First check If filedownloading is in process Then do CANCEL the download.
            if downloadTask != nil{
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                progressView.setProgress(0.0, animated: false)
                downloadTask.cancel()
                downloadTask = nil
            }
            
            
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_selected_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        default:
            break
        }
        
    }
    
    
// show popup to select either to send Email or Download.
    func showReportOptions(){
        let optionVC = UIAlertController(title: "What do you want?", message: "Download Report or Send Email", preferredStyle: .alert)
       
        let DownloadAction:UIAlertAction = UIAlertAction(title: "Download", style: .default ,handler: { (UIAlertAction) -> Void in
            
            if self.downloadTask != nil{
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.progressView.setProgress(0.0, animated: false)
                self.downloadTask.cancel()
            }

            self.performDocumentDownload()
        })


        let EmailAction:UIAlertAction = UIAlertAction(title: "Email", style: .default ,handler: { (UIAlertAction) -> Void in
            
            if self.AuditCodeForEmailQAReport != nil{
                
                self.performSegue(withIdentifier: "gotoEmailVC", sender: self.AuditCodeForEmailQAReport)
                
//                let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("9") as! EmailViewController
//                
////                popupVC.delegate = self
//                
//                popupVC.receivedAuditCode = self.AuditCodeForEmailQAReport
//                
//                
//                self.addChildViewController(popupVC)
//                popupVC.view.frame = self.view.frame
//                
//                self.view.addSubview(popupVC.view)
//                self.didMoveToParentViewController(self)
//                
                
            }
            
        })
        optionVC.addAction(DownloadAction)
        optionVC.addAction(EmailAction)

        
        self.present(optionVC, animated: true, completion:{
            optionVC.view.superview?.isUserInteractionEnabled = true
            optionVC.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
    }
    func alertControllerBackgroundTapped()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // - - Handling download file-  - - - - - -  - -
    
    func performDocumentDownload(){
        
        if AuditCodeForPDF != nil{
            
            downloadReport = true
            
            let url = URL(string: "http://app.3-tree.com/quonda/qa-report.php?User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodeForPDF!)")!
            //"http://portal.3-tree.com/api/android/quonda/qa-report.php?User=59c33016884a62116be975a9bb8257e3&AuditCode=S269983")!
            urlToDownload = url
            
        }
        
        if downloadReport == true{
            
            let url = URL(string: "http://app.3-tree.com/quonda/qa-report.php?User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodeForPDF!)")!
            
            downloadTask = backgroundSession.downloadTask(with: url)
            
           let topvc = topMostController()
            topvc.view.addSubview(DownloadProgressView)
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            downloadTask.resume()
            downloadReport = false
            
        }

    }
    
    
    
    func topMostController() -> UIViewController {
        var topController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        while (topController.presentedViewController != nil) {
            topController = topController.presentedViewController!
        }
        return topController
    }
    
    
    func urlSession(_ session: URLSession,
        downloadTask: URLSessionDownloadTask,
        didFinishDownloadingTo location: URL){
        
        
        do{
//            dispatch_async(dispatch_get_main_queue()) {
//                self.activityIndicator.stopAnimating()
//                self.messageFrame.removeFromSuperview()
//                self.messageFrame.hidden = true
                
//                self.DownloadProgressView.removeFromSuperview()
//                self.DownloadProgressView.hidden = true
                
                print("messageFrame is removed.")
//            }
        }catch{
            print("messageFrame cannot remove.")
        }
        
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
//            dispatch_async(dispatch_get_main_queue()) {
////                self.noDataFound(false)
//            }
//        sleep(1000)
        
            //            backgroundSession.fi
            let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
            let documentDirectoryPath:String = path.first!//path[0]
            let fileManager = FileManager()
            var destinationURLForFile = URL(fileURLWithPath: documentDirectoryPath + "/QAReport.pdf")
            
           if fileManager.fileExists(atPath: destinationURLForFile.path){

                do{
                    try fileManager.removeItem(atPath: destinationURLForFile.path)
                    destinationURLForFile = URL(fileURLWithPath: documentDirectoryPath + "/QAReport.pdf")
                }catch{
                    print(error)
                }
            }
       
        
            do {
                try fileManager.moveItem(at: location, to: destinationURLForFile)
                
                // show file
                // Before showing downloaded file 
                // First get the top most view controller's Download Progress View.
                // Then Remove it from its supper view.
                
                let topvc = topMostController()
                
                let DownloadProgressViewFrom_TopVC = topvc.view.viewWithTag(1)
                print("view with Tag = ",DownloadProgressViewFrom_TopVC)
                
                DownloadProgressViewFrom_TopVC?.removeFromSuperview()
                
//                self.DownloadProgressView.removeFromSuperview()   // This do not work.
                
                DispatchQueue.main.async(execute: { () -> Void in
                
                    self.showFileWithPath(destinationURLForFile.path)
                })
                
            }catch{
                print(error)
                print("An error occurred while moving file to destination url")
            }
        
 }
    
    
    func showFileWithPath(_ path: String){
        
        let topVC = self.topMostController()
        print("topMostController before Preview = ",topVC)
        print(self.view)
        
        //        self.view.window!.setValue(topVC.view.window, forKey: "window")
        // = topVC.view.window
        
        let topVC2 = self.topMostController()
        print("topMostController before Preview2 = ",topVC2)
        
        //        var viewer : UIDocumentInteractionController!
        
        let isFileFound:Bool? = FileManager.default.fileExists(atPath: path)
        if isFileFound == true{
            print("self.view.window= \(self.view.window)")
            // 2nd time self.view.window returns nil which means that Self class view is not in visible
            //            dispatch_async(dispatch_get_main_queue(), { () -> Void in
            //            if (topVC.view.window != nil){
            print("yes kpi is loaded into memory & ")
            print("KPI view's window is visible")
            // viewController is visible
            
            viewer = UIDocumentInteractionController(url: URL(fileURLWithPath: path))
            
            viewer!.delegate = self
            
            //                self.definesPresentationContext = true
            
            
            let url = URL(string:"itms-books:");
            
            if UIApplication.shared.canOpenURL(url!) {
                viewer!.presentOpenInMenu(from: CGRect.zero, in: topVC.view, animated: true)
            }
            // viewer.presentPreviewAnimated(true)
            //            }
            
        }
    }
    
    // it is for download progress
    func urlSession(_ session: URLSession,
        downloadTask: URLSessionDownloadTask,
        didWriteData bytesWritten: Int64,
        totalBytesWritten: Int64,
        totalBytesExpectedToWrite: Int64){
        
//        print("progressView.progress = ", progressView.progress)
//        if String(progressView.progress).containsString("0.9"){
//            print("yes it is completed.")
//            dispatch_async(dispatch_get_main_queue(), {
//                self.DownloadProgressView.removeFromSuperview()
//            })
//            
//        }
        DispatchQueue.main.async {
            self.progressView.setProgress(Float(totalBytesWritten)/Float(totalBytesExpectedToWrite), animated: true)
        }
//            progressView.setProgress(Float(totalBytesWritten)/Float(totalBytesExpectedToWrite), animated: true)
    }
    
    func documentInteractionController(_ controller: UIDocumentInteractionController, willBeginSendingToApplication application: String?) {
        print("document start sending to application")
        do{
            DispatchQueue.main.async {
//                self.activityIndicator.stopAnimating()
//                self.messageFrame.removeFromSuperview()
                print("messageFrame removed.")
            }
        }catch{
            print("messageFrame cannot remove.")
        }

    }
    
    func documentInteractionController(_ controller: UIDocumentInteractionController, didEndSendingToApplication application: String?) {
        print("document end sending to application")
    }
    
    func documentInteractionControllerDidDismissOpenInMenu(_ controller: UIDocumentInteractionController) {
        print("document open in Menu dismissed.")

    }
    
    
    
    // Helper
    func showAlertWithMessage(_ message:String) {
    
        let alertView:UIAlertController = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertAction:UIAlertAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel ,handler: nil )
    
        alertView.addAction(alertAction)
        
        self.present(alertView, animated: true, completion: { () -> Void in
            
        })
    }
    
    
    
      
    
//     MARK: - Navigation
//     In a storyboard-based application, you will often want to do a little preparation before navigation
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
        if segue.identifier == "goToKeyStatsAndImages"{
            print(sender!);
            let destinationVC = segue.destination as! KeyStatsImagesViewController
            destinationVC.AuditCodeIn = self.AuditCodeForKeyStatsImages
            destinationVC.DateSelected = (sender as! [String:String])["dateRange"] //sender as! String
            destinationVC.BrandSelected = (sender as! [String:String])["brand"]
            destinationVC.VendorSelected = (sender as! [String:String])["vendor"]
            destinationVC.InspectionTypeSelected = (sender as! [String:String])["InspectionType"]
        }
        else if segue.identifier == "gotoEmailVC"{
            let destinationVC = segue.destination as! EmailViewController
            destinationVC.receivedAuditCode = sender as! String
        }
    }

}




