//
//  KeyStatsImagesViewController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 12/05/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class KeyStatsImagesViewController: UIViewController, UITabBarDelegate {
    
    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    weak var tab1vc: UIViewController!
    weak var tab2vc: UIViewController!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!
    
    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!

    @IBOutlet weak var brand: UILabel!
    
    @IBOutlet weak var vendor: UILabel!
    
    @IBOutlet weak var PO: UILabel!
    
    @IBOutlet weak var style: UILabel!
    
    @IBOutlet weak var defectivePieces: UILabel!
    
    @IBOutlet weak var sampleSize: UILabel!
    
    @IBOutlet weak var sketch: UIImageView!
    
    @IBOutlet weak var containerView: UIView!
   
    
    var AuditCodeIn:String!
    var DateSelected:String!
    var BrandSelected:String!
    var VendorSelected:String!
    var InspectionTypeSelected:String!
    
    var dictionary:NSDictionary!
   
    var created:Bool!
//    let tempDirURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).URLByAppendingPathComponent("keyStats.json")
  
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    //NSURL(fileURLWithPath: NSTemporaryDirectory())
    
    //NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first!
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    var jsonFilePath2:URL!
    
    var activityIndicator = UIActivityIndicatorView()
    var messageFrame = UIView()
    var strLabel = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        print("DateSelected=\(DateSelected)")
        print("BrandSelected=\(BrandSelected)")
        print("VendorSelected=\(VendorSelected)")
        print("InspectionTypeSelected=\(InspectionTypeSelected)")
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        
        // Saving JSON DATA
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("keyStats.json")
        jsonFilePath2 = jsonFilePath
        
        // creating a .json file in the Temporary Directory
        
        if !fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("File created ")
                
            } else {
                print("Couldn't create file for some reason")
            }
        } else {
            print("File already exists")
        }
        
        
        // call request to get keyStats&Images data from the server
        performLoadDataRequestWithURL(getUrl())
        
        
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Selected Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getUrl() -> URL {
        let toEscape = "http://app.3-tree.com/quonda/key-stats.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
    
//    @IBOutlet weak var noDataFound_Label: UILabel!
    var bodyData:String!
    func performLoadDataRequestWithURL(_ url: URL) {
        
         progressBarDisplayer("Loading. . .", true)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        print("USER=")
        print(defaults.object(forKey: "userid")as! String)
        var auditCodeurl = ""
        if let auditcode = AuditCodeIn{
            auditCodeurl = "&AuditCode=\(auditcode)"
        }
        else{
          auditCodeurl = ""
        }
        print("AuditCode=")
        print(auditCodeurl)
        print("DateRange=")
        print(DateSelected!)
        print("BrandSelected!=")
        print(BrandSelected!)
        print("VendorSelected!=")
        print(VendorSelected!)
        print("InspectionTypeSelected!=")
        print(InspectionTypeSelected!)

     //   let today = NSDate()   // DateRange=2016-05-16:2016-05-16
        self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)\(auditCodeurl)&DateRange=\(DateSelected!)&Brand=\(BrandSelected!)&Vendor=\(VendorSelected!)&Audits=\(InspectionTypeSelected!)"
        //2016-05-16:2016-05-16"
       // DateRange=\(today.CustomDateformat):\(today.CustomDateformat)"
        print("KeyStatsAndImages_bodyData= \(self.bodyData!)")
        
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = self.bodyData!.data(using: String.Encoding.utf8)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let _:Data = data, let _:URLResponse = response, error == nil else {
                print("error")
                return
            }
            if (error != nil) {
                print(error!)
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                let alertView = UIAlertController(title: "Error in loading data" , message: "\(error!.localizedDescription)", preferredStyle: .alert)
                let Reload_action: UIAlertAction = UIAlertAction(title: "Reload", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    self.performLoadDataRequestWithURL(self.getUrl())
                })
                let no_action: UIAlertAction = UIAlertAction(title: "NO", style: .default, handler: nil)
                
                alertView.addAction(Reload_action)
                alertView.addAction(no_action)
                self.present(alertView, animated: true, completion: nil)
                
                                self.activityIndicator.stopAnimating()
                                self.messageFrame.removeFromSuperview()
            }
            
            else if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                // - - - - - - write this JSON data to file- - - - - - - -
               
               // print("created : \(self.created)")
                //                if self.created == true{
                if self.fileManager.fileExists(atPath: self.jsonFilePath2.absoluteString, isDirectory: &self.isDirectory) {
                    do{
                        try json.write(toFile: self.jsonFilePath2.path, atomically: true, encoding: String.Encoding.utf8)
                        print("wrting to file..")
                        
                        // call the update function of DefectsTab here
//                        var DT_obj = DefectsTab()
//                        DT_obj.updateData()
                        
                    } catch let error as NSError {
                        print("JSON data couldn't written to file // File not exist")
                        print(error.description)
                    }
                }
                
                /// - - - - - - - -
                
                // reading from the file stored in Temp Dir
               
                do {
                    var readString: String
                    readString = try NSString(contentsOfFile: self.jsonFilePath2.path, encoding: String.Encoding.utf8.rawValue) as String
                    print("readString=\(readString)")
                    
                } catch let error as NSError {
                    print("couldn't read file.")
                    print(error.description)
                }

                print(json)
                
                
                
                
                self.dictionary = self.parseJSON(json)
                
                print("KeyStatsReturnData = \(self.dictionary)")
                
               // print("\(self.dictionary["Po"] as? String)")
                
                if self.dictionary["Po"] as? String == nil{
                    print("No Data Found!")
                    self.noDataFound()
//                    self.noDataFound_Label.hidden = false
                }
                
                
                var Style, Defective, SampleSize:String!
                var Sketch,Brand, Vendor, Po:String!
                
                Style = self.dictionary["Style"]! as? String
                Defective = self.dictionary["Defective"]! as? String
                SampleSize = self.dictionary["SampleSize"]! as? String

                Sketch = self.dictionary["Sketch"] as? String
                Brand = self.dictionary["Brand"] as? String
                Vendor = self.dictionary["Vendor"] as? String
                Po = self.dictionary["Po"] as? String
                //print(Style,Defective,SampleSize,Sketch,Vendor,Po);
            //Must use dispatch_async() otherwise it will give an error message "this-application-is-modifying-the-autolayout-engine-from-a-background-thread"
                
               DispatchQueue.main.async(execute: { () -> Void in
               
                NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadTableViewNotification"), object: self)
               
                self.style.text = Style
                self.defectivePieces.text = Defective
                self.sampleSize.text = SampleSize
                
                self.brand.text = Brand
                self.vendor.text = Vendor
                self.PO.text = Po
                
//                if Sketch == "http://portal.3-tree.com/files/sketches/default.jpg"
               
                if let url = URL(string: Sketch){
                    if let data = try? Data(contentsOf: url) {
                        self.sketch.image = UIImage(data: data)
                    }
                }
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
//
//                print(self.style.text)
//                print(self.defectivePieces.text)
//                print(self.sampleSize.text)
//                print(self.brand.text)
//                print(self.vendor.text)
//                print(self.PO.text)

               })
                
//                var All_Audits = self.dictionary["Audits"]! as! [[String : AnyObject]]
                
                //                print("\((self.dictionary["Audits"]![0]!["AuditCode"]!))")
                //                print("\((self.dictionary["Audits"]![1]!["AuditCode"]!))")
                
                //            print("\((dictionary["Audits"]!.valueForKey("0")!.valueForKey("AuditCode")!))")  // dictionary["0"]?.valueForKey("name")!
                
//                print("Total Audits = \(self.dictionary["Audits"]!.count)")
                
//                if self.dictionary["Audits"]?.count < 1 {
//                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
//                    
//                    let alertView = UIAlertController(title: "No Data Found" , message: "Press OK to Reload Data", preferredStyle: .Alert)
//                    let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .Default, handler: { (UIAlertAction) -> Void in
//                        
//                        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
//                        self.performLoadDataRequestWithURL(self.getUrl())
//                    })
//                    let Cancel_action: UIAlertAction = UIAlertAction(title: "NO", style: .Default, handler: { (UIAlertAction) -> Void in
//                        
//                        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
//                    })
//                    
//                    alertView.addAction(Cancel_action)
//                    alertView.addAction(ok_action)
//                    self.presentViewController(alertView, animated: true, completion: nil)
//                    
//                }else{
                    // save data to Documents Dir
//                    if self.fileManager.fileExistsAtPath(self.jsonFilePath2.absoluteString, isDirectory: &self.isDirectory) {
//                        do{
//                            try json.writeToFile(self.jsonFilePath2.path!, atomically: true, encoding: NSUTF8StringEncoding)
//                        } catch let error as NSError {
//                            print("JSON data couldn't written to file // File not exist")
//                            print(error.description)
//                        }
//                    }
//                    for Audit in All_Audits{
//                        
//                        let AuditCodeName = Audit["AuditCode"]! as! String
//                        print("\(AuditCodeName)")
//                        let completedAudit = Audit["Completed"]! as! String
//                        print(completedAudit)
//                        let AuditColor = Audit["Color"]! as! String
//                        print("AuditorColor=\(AuditColor)")
//                    }
                
                    if("\(self.dictionary["Status"]!)" == "OK"){
                        
//                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                            
//                            self.AuditCodesTable.reloadData()
//                        })
                        self.activityIndicator.stopAnimating()
                        self.messageFrame.removeFromSuperview()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
 //               } //else part of count<1
            }//if data != nil
            else{
                print("return data is nil")
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
                let alertView = UIAlertController(title: "No Data Found" , message: "Press OK to Reload Data", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    self.performLoadDataRequestWithURL(self.getUrl())
                })
                let Cancel_action: UIAlertAction = UIAlertAction(title: "No", style: .default, handler: nil)
                
                alertView.addAction(Cancel_action)
                alertView.addAction(ok_action)
                self.present(alertView, animated: true, completion: nil)
            }
            
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
           // print("returned KeyStatsANDImages data =\(dataString)")
        }) 
        
        task.resume()
    }
    
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    func noDataFound(){
        
        let alert:UIAlertController = UIAlertController(title: nil, message: "No Data Found!", preferredStyle: UIAlertControllerStyle.alert)
        self.present(alert, animated: true, completion: nil)
        
        let duration:UInt64 = 3; // duration in seconds
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(duration*NSEC_PER_SEC)) / Double(NSEC_PER_SEC)) { () -> Void in
            alert.dismiss(animated: true, completion: nil)
        }
    }
// - - - - - - - - - -
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
//            defaults.removeObjectForKey("userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
            //        case 4:
            //            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
            //                //                self.dismissViewControllerAnimated(true, completion: nil)
            //                presentViewController(resultController, animated: true, completion: nil)
            //            }
            //
            //            let selectedImage:UIImage = UIImage(named: "Selected QAReport")!
            //            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            //
            //            break
        case 5:
            if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }else{
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }
            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_selected_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        default:
            break
        }
    }
    func progressBarDisplayer(_ msg:String, _ indicator:Bool ) {
        print(msg)
        strLabel = UILabel(frame: CGRect(x: view.frame.midX - 40, y: view.frame.midY - 50, width: 200, height: 50))  // x: 50, y: 0, width: 200, height: 50
        strLabel.text = msg
        strLabel.textColor = UIColor.white
        messageFrame = UIView(frame: CGRect(x: 0, y: 69 , width: view.frame.width, height: view.frame.height - 118))  //x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50
        //        messageFrame.layer.cornerRadius = 15
        messageFrame.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        if indicator {
            activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
            activityIndicator.frame = CGRect(x: view.frame.midX - 80, y: view.frame.midY - 50, width: 50, height: 50)  // x: 0, y: 0, width: 50, height: 50
            activityIndicator.startAnimating()
            messageFrame.addSubview(activityIndicator)
        }
        messageFrame.addSubview(strLabel)
        view.addSubview(messageFrame)
    }
    
    func NewCellsProgressBar(_ indicator:Bool ) {
        
        strLabel = UILabel(frame: CGRect(x: 170, y: 0, width: 200, height: 50))
        strLabel.text = "loading. . ."
        strLabel.textColor = UIColor.white
        messageFrame = UIView(frame: CGRect(x: 0, y: view.frame.height - 97, width: view.frame.width, height: 50))
        messageFrame.layer.cornerRadius = 5
        messageFrame.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        if indicator {
            activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
            activityIndicator.frame = CGRect(x: 120, y: 0, width: 50, height: 50)
            activityIndicator.startAnimating()
            messageFrame.addSubview(activityIndicator)
        }
        messageFrame.addSubview(strLabel)
        view.addSubview(messageFrame)
    }

}

