//
//  LoginView.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 07/03/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
var jsonLoginFilePath:URL!

let defaults = UserDefaults.standard
let TempDefaults = UserDefaults.standard


class LoginView: UIViewController,UITextFieldDelegate , UIGestureRecognizerDelegate {

    @IBOutlet weak var user_name: UITextField!
    
    @IBOutlet weak var userPassword: UITextField!
    
    @IBOutlet weak var LoginButton: UIButton!
    var messageFrame = UIView()
    var activityIndicator = UIActivityIndicatorView()
    var strLabel = UILabel()
    
//    var jsonLoginFilePath:NSURL!
    var created:Bool!
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    
    
    @IBOutlet weak var AppVersion: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // print(appUpdateAvailable())
        // Do any additional setup after loading the view, typically from a nib.
       // self.CheckForAppVersion()
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(LoginView.updateApp))
        tapGesture.delegate = self
        AppVersion.isUserInteractionEnabled = true
        AppVersion.addGestureRecognizer(tapGesture)
        
        // Saving JSON DATA
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("login.json")
        jsonLoginFilePath = jsonFilePath
        print(jsonLoginFilePath)

        // creating a .json file in the Documents folder
        if !fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("File created ")
                
            } else {
                print("Couldn't create file for some reason")
            }
        } else {
            print("File already exists")
        }
        
        if let lastUser = defaults.object(forKey: "lastLoged_username"){
            self.user_name.text = lastUser as! String
        }
        
//        if ((defaults.objectForKey("username") != nil && defaults.objectForKey("password") != nil)){
//            
//            self.performSelector("performAutomaticLoginRequest")
//        }
            user_name.delegate = self
            userPassword.delegate = self
//        }
    }
    override func viewDidAppear(_ animated: Bool) {
        _ = self.appUpdateAvailable()
    }
    func updateApp(gesture:UITapGestureRecognizer){
        let url = URL(string: "itms-apps://itunes.apple.com/ug/app/quonda-manager/id1093725497?mt=8")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    func popupMessage(_ message:String){
        let alertView = UIAlertController(title: "Update Available" , message: message, preferredStyle: .alert)
        let ok_action: UIAlertAction = UIAlertAction(title: "Update", style: .default, handler: { (UIAlertAction) -> Void in
            let url = URL(string: "itms-apps://itunes.apple.com/ug/app/quonda-manager/id1093725497?mt=8")!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            
        })
        let cancel_action: UIAlertAction = UIAlertAction(title: "Cancel", style: .default, handler: { (UIAlertAction) -> Void in
            
        })
        
        alertView.addAction(ok_action)
        alertView.addAction(cancel_action)

        self.present(alertView, animated: true, completion: nil)
    }
    func appUpdateAvailable() -> Bool
    {
        let storeInfoURL: String = "http://itunes.apple.com/lookup?bundleId=com.3-tree.QUONDA"
        var upgradeAvailable = false
        // Get the main bundle of the app so that we can determine the app's version number
        let bundle = Bundle.main
        if let infoDictionary = bundle.infoDictionary {
            // The URL for this app on the iTunes store uses the Apple ID for the  This never changes, so it is a constant
            let urlOnAppStore = NSURL(string: storeInfoURL)
            if let dataInJSON = NSData(contentsOf: urlOnAppStore! as URL) {
                // Try to deserialize the JSON that we got
                if let dict: NSDictionary = try! JSONSerialization.jsonObject(with: dataInJSON as Data, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject] as NSDictionary? {
                    if let results:NSArray = dict["results"] as? NSArray {
                        print(results[0] as! [String : Any])
                        if let version = ((results[0]) as AnyObject)["version"] as? String {
                            // Get the version number of the current version installed on device
                            if let currentVersion = infoDictionary["CFBundleShortVersionString"] as? String {
                                // Check if they are the same. If not, an upgrade is available.
                                print("\(version)")
                                
                                  print("\(currentVersion)")
                                if version != currentVersion {
                                    AppVersion.text = "Application Version:" + currentVersion
                                    AppVersion.textColor = UIColor.red
                                    upgradeAvailable = true
                                   // popupMessage("A new version of Quonda Manager is avaialble. Please update to \(version)")
                                }else{
                                    AppVersion.text = "Application Version:" + version
                                    AppVersion.textColor = UIColor.green
                                }
                            }
                        }
                    }
                }
            }
        }
        return upgradeAvailable
    }
    func CheckForAppVersion(){
        
        let standardUserDefaults = UserDefaults.standard
        let shortVersionKey = "CFBundleShortVersionString"
        let currentVersion = Bundle.main.infoDictionary![shortVersionKey] as! String
        let previousVersion = standardUserDefaults.object(forKey: shortVersionKey) as? String
        if previousVersion == currentVersion {
            // same version
            AppVersion.text = "Application Version:" + currentVersion
            AppVersion.textColor = UIColor.green
        } else {
            // replace with `if let previousVersion = previousVersion {` if you need the exact value
            if previousVersion != nil {
                // new version
                AppVersion.text = "Application Version:" + previousVersion!
                AppVersion.textColor = UIColor.red
            } else {
                AppVersion.text = "Application Version:" + currentVersion
                AppVersion.textColor = UIColor.green
                // first launch
            }
            standardUserDefaults.set(currentVersion, forKey: shortVersionKey)
            standardUserDefaults.synchronize()
        }
//        let defaults = UserDefaults.standard
//        
//        let currentAppVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
//        let previousVersion = defaults.string(forKey: "appVersion")
//
//        if previousVersion == nil {
//            // first launch
//            defaults.set(currentAppVersion, forKey: "appVersion")
//            defaults.synchronize()
//            AppVersion.textColor = UIColor.green
//        } else if previousVersion == currentAppVersion {
//            AppVersion.text = "Application Version:" + currentAppVersion
//            AppVersion.textColor = UIColor.green
//
//            // same version
//        } else {
//            AppVersion.text = "Application Version:" + previousVersion!
//            AppVersion.textColor = UIColor.red
//
//
//            // other version
//            defaults.set(currentAppVersion, forKey: "appVersion")
//            defaults.synchronize()
//        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == user_name{
            user_name.resignFirstResponder()
            userPassword.becomeFirstResponder()
//            return true
        }else if textField == userPassword{
            userPassword.resignFirstResponder()
            
            self.performAutomaticLoginRequest()
//            return true
        }
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func performAutomaticLoginRequest(){
        
        progressBarDisplayer("Signing in", true)
        
        self.LoginButton.isEnabled = false
        self.LoginButton.backgroundColor = UIColor.lightGray
        
        //In case when Internet is not available check if currently entered username/password matches the last username and password; then directly go to the dashboard.
        
        if !Reachability.isConnectedToNetwork(){
            if defaults.object(forKey: "lastLoged_username") != nil && defaults.object(forKey: "lastLoged_password") != nil {
                
                if self.user_name.text! == defaults.object(forKey: "lastLoged_username") as! String  && self.userPassword.text! == defaults.object(forKey: "lastLoged_password") as! String{
                    
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                    
                    self.presentDashboardViewController()
                    
                }else{
                    wrongUserInfoPopup()
                }
            }else{
                wrongUserInfoPopup()
            }
            
        }else{  // In case when Internet is available.
            
            //            let url = getLogin()
            performLoginRequestWithURL(getLogin())
        }
    }
    
    
    // popup error if user is first time loging in while offline OR if user is entering wrong information
    func wrongUserInfoPopup(){
        
        self.activityIndicator.stopAnimating()
        self.messageFrame.removeFromSuperview()
        
        self.LoginButton.isEnabled = true
        //self.LoginButton.backgroundColor = UIColor.blue
        
        let alertcontroller = UIAlertController(title: nil, message: "Make sure that your UserName and Password is correct.\n Also make sure you are connected to the Internet.", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertcontroller.addAction(ok)
        self.present(alertcontroller, animated: true, completion: nil)
    }
    
    
    
    @IBAction func signInFunc(_ sender: UIButton) {
    
        progressBarDisplayer("Signing in", true)
        self.LoginButton.isEnabled = false
        self.LoginButton.backgroundColor = UIColor.lightGray
        
//        Gets url string
        let url = getLogin()
        
//        Posts url with UITextField data.
        let jsonString = performLoginRequestWithURL(url)
    }
    func getLogin() -> URL {
        //let toEscape = "http://portal.3-tree.com/api/android/login.php"
        //let toEscape = "http://app.3-tree.com/login.php"
 let toEscape = "http://app.3-tree.com/login.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
    var bodyData:String?
    func performLoginRequestWithURL(_ url: URL) -> String? {
        
        if defaults.object(forKey: "username") != nil{
            
            let usrName:String = defaults.object(forKey: "username")as! String
            let pas:String = defaults.object(forKey: "password")as! String
            
            bodyData = "Username=\(usrName)&Password=\(pas)&AppVersion=186"
            
        }else{
            
            bodyData = "Username=\(self.user_name.text!)&Password=\(self.userPassword.text!)&AppVersion=186"
        }
        print(bodyData!, url)
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = bodyData!.data(using: String.Encoding.utf8)
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main){
            response, data, error in
            print("\(response)")
            if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                
                // - - - - - - write this JSON data to file- - - - - - - -
                
               // print("created : \(self.created)")
                //                if self.created == true{
                if self.fileManager.fileExists(atPath: jsonLoginFilePath.absoluteString, isDirectory: &self.isDirectory) {
                    do{
                        try json.write(toFile: jsonLoginFilePath.path, atomically: true, encoding: String.Encoding.utf8)
                        var readString: String
                        readString = try NSString(contentsOfFile: jsonLoginFilePath.path, encoding: String.Encoding.utf8.rawValue) as String
                        print(readString)
                    } catch let error as NSError {
                        print("JSON data couldn't written to file // file NOT exist")
                        print(error.description)
                    }
                    //                }
                }
                
               // print(json)
                let dictionary = self.parseJSON(json)
                            print("\(dictionary)")
                if("\(dictionary["Status"]!)" == "ERROR")
                {
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                    
                    //show popup on LOGIN FAIL
                    let alert = UIAlertController(title: "Alert", message: "Please provide the correct userName and password", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(UIAlertAction) -> Void in
                        
                        self.LoginButton.isEnabled = true
                        //self.LoginButton.backgroundColor = UIColor.blue
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    defaults.removeObject(forKey: "username")
                    defaults.removeObject(forKey: "password")
                    
                    //self.user_name.text = ""
                    self.userPassword.text = ""
                    
                }else if dictionary.count < 1{
                    let alertView = UIAlertController(title: "cannot Login" , message: "Press OK", preferredStyle: .alert)
                    let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction)-> Void in
                        
                        self.LoginButton.isEnabled = true
                        //self.LoginButton.backgroundColor = UIColor.blue
                    })
                    alertView.addAction(ok_action)
                    self.present(alertView, animated: true, completion: nil)
                    defaults.removeObject(forKey: "username")
                    defaults.removeObject(forKey: "password")
                    
                    //self.user_name.text = ""
                    self.userPassword.text = ""
                    
                }
                else
                {
                    for (key, value) in dictionary {
                        print("\(key) -> \(value)")
                    }
                    //                print("Successfully loged in name = \(dictionary["0"]?.valueForKey("name")!)")
                    //                self.log_in_status.text = "Successfuly loged in"
                    //                let alert = UIAlertController(title: "Alert", message: "Successfully logedIn", preferredStyle: UIAlertControllerStyle.Alert)
                    //                alert.addAction(UIAlertAction(title: "Proceed", style: UIAlertActionStyle.Default, handler: nil))
                    //                self.presentViewController(alert, animated: true, completion: nil)
                    print("AuditsManager = \(dictionary["AuditsManager"]!)")
                    
                    defaults.set(self.user_name.text, forKey: "lastLoged_username")
                    defaults.set(self.user_name.text, forKey: "username")
                    defaults.set(self.userPassword.text, forKey: "password")
                    defaults.set(self.userPassword.text, forKey: "lastLoged_password")
                    defaults.set("\(dictionary["User"]!)", forKey: "userid")
                    defaults.set("\(dictionary["Email"]!)", forKey: "userEmail")

                    defaults.set("\(dictionary["UserType"]!)", forKey: "UserType")
                    defaults.set("\(dictionary["AuditsManager"]!)", forKey: "AuditsManager")


                    
                    
                    //self.user_name.text = ""
                    self.userPassword.text = ""
                    
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                    
                    self.presentDashboardViewController()
                }
            }// data != nil
            else{
                let alertView = UIAlertController(title: "Login Failed" , message: "Press OK", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction)-> Void in
                    
                    self.LoginButton.isEnabled = true
                    //self.LoginButton.backgroundColor = UIColor.blue
                })
                alertView.addAction(ok_action)
                self.present(alertView, animated: true, completion: nil)
                defaults.removeObject(forKey: "username")
                defaults.removeObject(forKey: "password")
                
                //self.user_name.text = ""
                self.userPassword.text = ""
                
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
            }
        }
        return json
    }
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    func presentDashboardViewController(){
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "3")
        self.present(vc, animated: true, completion: nil)
    }
    func progressBarDisplayer(_ msg:String, _ indicator:Bool ) {
        print(msg)
        strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 200, height: 50))
        strLabel.text = msg
        strLabel.textColor = UIColor.white
        messageFrame = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25 , width: 180, height: 50))
        messageFrame.layer.cornerRadius = 15
        messageFrame.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        if indicator {
            activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
            activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
            activityIndicator.startAnimating()
            messageFrame.addSubview(activityIndicator)
        }
        messageFrame.addSubview(strLabel)
        view.addSubview(messageFrame)
    }

   /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let main_tabedViewController = segue.destinationViewController as! tabedController
        if(segue.identifier == "goto_TabedController")
        {
            main_tabedViewController.sampleData = "hurray you've logedIn successfully"
        }
    }
    */

}
