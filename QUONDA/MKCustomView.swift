//
//  MKCustomView.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 11/04/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class MKCustomView: UIView {

    @IBOutlet weak var AuditorName: UILabel!
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        let subView: UIView = loadViewFromNib()
        subView.frame = self.bounds
        //label1.text = "123" //would cause error
        addSubview(subView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func loadViewFromNib() -> UIView {
        let view: UIView = UINib(nibName: "MKCustomView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
        return view
    }
    
    
    // Our custom view from the XIB file
//    @IBOutlet var view: UIView!
    
//    var viewm: UIView!
    
//    @IBOutlet weak var colorCode: UILabel!
//    @IBOutlet weak var factoryName: UILabel!
    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
////        self.view.addSubview(factoryName)
////        NSBundle.mainBundle().loadNibNamed("MKCustomView", owner: self, options: nil)
//        self.addSubview(self.view);    // adding the top level view to the view hierarchy
//    }
    
    
    
//    required init?(coder: NSCoder) {
//        super.init(coder: coder)
//        NSBundle.mainBundle().loadNibNamed("MKCustomView", owner: self, options: nil)
//        self.addSubview(colorCode)
//        self.addSubview(factoryName)
////        assert(test != nil, "the button is conected just like it's supposed to be")
//    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
