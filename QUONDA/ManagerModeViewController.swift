//
//  ManagerModeViewController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 19/10/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ManagerModeViewController: UIViewController ,UITabBarDelegate ,SavingViewControllerDelegate {
    
    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!

    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    
    
    
    @IBOutlet weak var todayBtnLabel: UIButton!
    
    @IBOutlet weak var selectAuditorBtnText: UIButton!
    
    @IBOutlet weak var selectVendorBtnText: UIButton!
    
    
    @IBOutlet weak var plannedLabel: UILabel!
    @IBOutlet weak var completedLabel: UILabel!
    @IBOutlet weak var pendingLabel: UILabel!
    
    
    @IBOutlet weak var OutMostScrollView: UIScrollView!
    @IBOutlet weak var OutMostStackView: UIStackView!
    
    @IBOutlet weak var stackVeiw1: UIStackView!
    @IBOutlet weak var stackVeiw2: UIStackView!
    @IBOutlet weak var stackVeiw3: UIStackView!
    @IBOutlet weak var stackVeiw4: UIStackView!
    @IBOutlet weak var stackVeiw5: UIStackView!
    @IBOutlet weak var stackVeiw6: UIStackView!
    @IBOutlet weak var stackVeiw7: UIStackView!
    @IBOutlet weak var stackVeiw8: UIStackView!
    @IBOutlet weak var stackVeiw9: UIStackView!
    @IBOutlet weak var stackVeiw10: UIStackView!
    @IBOutlet weak var stackVeiw11: UIStackView!
    @IBOutlet weak var stackVeiw12: UIStackView!
    

    
    // activity progress variables
    
    var strLabel = UILabel()
    var messageFrame = UIView()
    var activityIndicator = UIActivityIndicatorView()
    
    

    
    // popUp data structure
    
    struct items {
        var AuditCode : String
        var Auditor : String
        var Vendor : String
        var Brand : String
        var Po : String
        var Style : String
        var Season : String
        var ETDrequired : String
        var ReportType : String
        var AuditStage : String
        var AuditTime : String
        var SampleSize : String
        var Line : String
        var Defects : String
        var AuditResult : String
        var Picture : String
    }
    var PopUpData = [items]()
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        OutMostScrollView.contentSize.height = OutMostStackView.frame.height+20//250
        
    }
    
    
    func sortDictionary(_ DataArray:[String],IDsArray:[String]) -> ([String],[String]) {
        
        var sortedDataArray:[String] = []
        var sortedIDsArray:[String] = []
        
        var myArray = [String : AnyObject]()
        
        for (auditor,auditorID) in zip(DataArray,IDsArray){
    
            myArray.updateValue("\(auditorID)" as AnyObject, forKey: "\(auditor)")
        }
        
        
        // Get the array from the keys property.
        let copy = myArray.keys
        // Sort from low to high (alphabetical order).
        let p = copy.sorted(by: <)
        
        for pi in p{
            print(pi)
        }
        
        let s = myArray.keys.sorted()
        
        // Loop over sorted keys.
        var i = 0
        for pin in p{
            for key in copy {
                if key == pin{
                    // Get value for this key.
                    if let value = myArray[key] {
                        
                        sortedDataArray.append(key)
                        sortedIDsArray.append(value as! String)
                        
                        print("Key = \(key), Value = \(value)")
                        i+=1
                    }
                }
            }}
        
        return (sortedDataArray,sortedIDsArray)
    }
    
    
    
    
    
    @IBAction func SelectAuditor(_ sender: AnyObject) {
        
        if self.Auditors != nil{
            
            var AuditorsArray1:[String] = []
            var AuditorsIDArray1:[String] = []
            
            let AuditorsArray2 = self.Auditors?.values
            let AuditorsIDArray2 = self.Auditors?.keys
            
            for (auditor,auditorID) in zip(AuditorsArray2!,AuditorsIDArray2!){
                
                AuditorsArray1.append("\(auditor)")
                AuditorsIDArray1.append(auditorID)
            }
            
            // - - - - Applying Sorting- - -
            
            var (SortedAuditorsArray,SortedAuditorsIDArray) = sortDictionary(AuditorsArray1, IDsArray: AuditorsIDArray1)
           
            SortedAuditorsArray.insert("All Auditors", at: SortedAuditorsArray.startIndex)
            SortedAuditorsIDArray.insert("010101", at: SortedAuditorsIDArray.startIndex)
           
            // - - - - -Sorting Ends - - - - -
            
            
            let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as! PopUpViewController
            
            popupVC.delegate = self
            
            //        if self.Auditors != nil{
            
            popupVC.pickerdata = SortedAuditorsArray
            popupVC.pickerdataIDs = SortedAuditorsIDArray
            
            
            popupVC.WhichButton = "Select Auditor"
            popupVC.AllreadySelectedText = ""
            popupVC.AllreadySelectedText = self.selectAuditorBtnText.titleLabel!.text!
            
            self.addChildViewController(popupVC)
            popupVC.view.frame = self.view.frame
            
            self.view.addSubview(popupVC.view)
            self.didMove(toParentViewController: self)
            
        }else{
            //Do nothing.
//            popupVC.pickerdata = AuditorsArray
//            popupVC.pickerdataIDs = AuditorsIDsArray
        }
        
        
    }
    
    
    @IBAction func SelectVendor(_ sender: AnyObject) {
        
        if self.Vendors != nil{
            
            var VendorsArray1:[String] = []
            var VendorsIDArray1:[String] = []
            
            let VendorsArray2 = self.Vendors?.values
            let VendorsIDArray2 = self.Vendors?.keys
            
            
            for (Vendor,VendorID) in zip(VendorsArray2!,VendorsIDArray2!){
                
                VendorsArray1.append("\(Vendor)")
                VendorsIDArray1.append("\(VendorID)")
            }
            
            // - - - - Applying Sorting- - -
            
            var (SortedVendorsArray,SortedVendorsIDArray) = sortDictionary(VendorsArray1, IDsArray: VendorsIDArray1)
            
            SortedVendorsArray.insert("All Vendors", at: SortedVendorsArray.startIndex)
            SortedVendorsIDArray.insert("010101", at: SortedVendorsIDArray.startIndex)
            
            // - - - - -Sorting Ends - - - - -
            
            
            let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as! PopUpViewController
            
            popupVC.delegate = self
            
            
            //        if self.Vendors != nil{
            
            popupVC.pickerdata = SortedVendorsArray
            popupVC.pickerdataIDs = SortedVendorsIDArray
            
            
            popupVC.WhichButton = "Select Vendor"
            popupVC.AllreadySelectedText = ""
            popupVC.AllreadySelectedText = self.selectVendorBtnText.titleLabel!.text!
            
            self.addChildViewController(popupVC)
            popupVC.view.frame = self.view.frame
            
            self.view.addSubview(popupVC.view)
            self.didMove(toParentViewController: self)
            
            
        }else{
            //Do nothing.
//            popupVC.pickerdata = AuditorsArray
//            popupVC.pickerdataIDs = AuditorsIDsArray
        }
    }
    
    
    func saveSelectedColor(_ strText : String, strID : String)
    {
        
    }
    func saveText(_ strText: String, strID: String) {
        
    }
    func saveUnitText(_ strText: String, strID: String) {
        
    }
    func saveBrandText(_ strText: String, strID: String) {
        
    }
    func saveStyleText(_ strText: String, strID: String) {
        
    }
    func saveReportText(_ strText: String, strID: String) {
        
    }
    func saveVendorText(_ strText: String, strID: String) {
        
    }
    func saveAuditTypeText(_ strText: String, strID: String) {
        
    }
    func saveSampleSizeText(_ strText: String, strID: String) {
        
    }
    func saveProductionLineText(_ strText: String, strID: String) {
        
    }
    func saveAuditResultText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveAQLText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveAQLData(_ strText: String, strID: String) {
        // Do Nothing
    }

    
    func saveAuditorsText(_ strText: String, strID: String) {
        print("Auditor Name =",strText)
        print("Auditor Id =",strID)
        
        selectAuditorBtnText.setTitle(strText, for: UIControlState())
//        selectVendorBtnText.setTitle("All Vendors", forState: .Normal)
        
        progressBarDisplayer("loading...", true)
        
        if strText == "All Auditors"{
            selectVendorBtnText.setTitle("All Vendors", for: UIControlState())
            performLoadDataRequestWithURL(self.getUrl(), Auditor: "", Vendor: "")
            
        }else{
            selectVendorBtnText.setTitle("Select Vendor", for: UIControlState())
            performLoadDataRequestWithURL(self.getUrl(), Auditor: strID, Vendor: "")
        }
    }
    
    func saveVendorsText(_ strText: String, strID: String) {
        print("Vendor Name =",strText)
        print("Vendor Id =",strID)
        
//        selectAuditorBtnText.setTitle("All Auditors", forState: .Normal)
        selectVendorBtnText.setTitle(strText, for: UIControlState())
        
        progressBarDisplayer("loading...", true)
        
        if strText == "All Vendors"{
            selectAuditorBtnText.setTitle("All Auditors", for: UIControlState())
            performLoadDataRequestWithURL(self.getUrl(), Auditor: "", Vendor: "")
        }else{
            selectAuditorBtnText.setTitle("Select Auditor", for: UIControlState())
            performLoadDataRequestWithURL(self.getUrl(), Auditor: "", Vendor: strID)
        }
    }
    
    
    
    
    
    
    
    
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    var jsonFilePath2:URL!
    var created:Bool!

    var dictionary:[String: AnyObject]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        //        self.scrollView.contentSize.height = 10000
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Selected Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)

        
        
        
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("ManagerMode.json")
        jsonFilePath2 = jsonFilePath
        // creating a .json file in the Documents folder
        if !fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("ManagerMode File created ")
                
            } else {
                print("Couldn't create ManagerMode file for some reason")
            }
        } else {
            print("ManagerMode File already exists")
        }
        //file creation ends

        
        if(!Reachability.isConnectedToNetwork()){
            
            let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
            
            let jsonFilePath = documentsDirectoryPath.appendingPathComponent("ManagerMode.json")
            jsonLoginFilePath = jsonFilePath
            print(jsonLoginFilePath.path)
            
            do {
                var readString: String
                readString = try NSString(contentsOfFile: jsonLoginFilePath.path , encoding: String.Encoding.utf8.rawValue) as String
                print(readString)
                self.showDataWhenOffline(readString)
            } catch let error as NSError {
                print(error.description)
            }
        }else{
            progressBarDisplayer("Loading. .", true)
            self.performLoadDataRequestWithURL(self.getUrl(), Auditor: "", Vendor: "")

        }
        
    }
    func showDataWhenOffline(_ savedData:String?){
        
               if savedData != nil {
            
            //Reset all blocksdata
            self.ResetAllBlockData()
            
            //json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
            
            self.dictionary = self.parseJSON(savedData!)
            
            print("Json of ManagerMode =\(self.dictionary)")
            
            
            // save list of Auditors and Vendors
            if("\(self.dictionary["Status"]!)" == "OK") {
                
                self.Auditors = self.dictionary["Auditors"] as! [String : AnyObject]
                print("Auditors = ",self.Auditors)
                
                self.Vendors = self.dictionary["Vendors"] as! [String : AnyObject]
                print("Vendors = ",self.Vendors)
                
            }
            
            for Auditor in self.Auditors!{
                print("Auditor Name =",Auditor.1)
                print("Auditor ID =",Auditor.0)
            }
            
            var planned, completed, pending:Int!
            planned = (self.dictionary["Today"]!as! [String: AnyObject])["Planned"] as! Int
            completed = (self.dictionary["Today"]!as! [String: AnyObject])["Completed"] as! Int
            pending = (self.dictionary["Today"]!as! [String: AnyObject])["Pending"] as! Int
            let day = (self.dictionary["Today"]!as! [String: AnyObject])["Day"] as! String
            
            print("planned =",planned)
            print("completed =",completed)
            print("pending =",pending)
            print("Today[Day] = ",self.dictionary["Today"]!["Day"])
            
            //                self.currentDateLabel.setTitle(self.dictionary["Today"]!["Day"] as! String, forState: UIControlState.Normal)
            //                self.plannedAudits.text = String(planned)
            //                self.completedAudits.text = String(completed)
            //                self.pendingReports.text = String(pending)
            
            self.todayBtnLabel.setTitle(day, for: UIControlState())
            self.plannedLabel.text = "\(planned!)"
            self.completedLabel.text = "\(completed!)"
            self.pendingLabel.text = "\(pending!)"
            
            print("planned =",self.plannedLabel.text!)
            print("completed =",self.completedLabel.text!)
            print("pending =", self.pendingLabel.text!)
            
            if (self.dictionary["Audits"] as AnyObject).count < 1 {
                //                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
                
            self.showDataInBlocks(self.BlocksDataDictionary)
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.viewDidLayoutSubviews()
                    })
            }else{
                
                // save Manager Mode data to Documents Dir
                
                if self.fileManager.fileExists(atPath: self.jsonFilePath2.absoluteString, isDirectory: &self.isDirectory) {
                    do{
                        print("old json data in ManagerMode =",savedData!)
                        try savedData!.write(toFile: self.jsonFilePath2.path, atomically: true, encoding: String.Encoding.utf8)
                    } catch let error as NSError {
                        print("JSON data couldn't written to file // File not exist")
                        print(error.description)
                    }
                }
                
                
                
                // Reset the bellow data structures
                
                //                    self.AuditCodes.removeAll()
                //                    self.AuditCompleted.removeAll()
                //                    self.AuditColors.removeAll()
                //
                self.BlocksDataDictionary = [:]
                
                var All_Audits = self.dictionary["Audits"]! as! [[String : AnyObject]]
                print("Total Audits = \((self.dictionary["Audits"]! as AnyObject).count)")
                All_Audits = All_Audits.sorted{$1["AuditTime"] as? Date > $0["AuditTime"] as? Date}
                
                //All_Audits.sorted(by: { $0.0["AuditTime"]! < $0.1["AuditTime"]! })
                print("All_Audits =",All_Audits)
                
                for item  in 0..<All_Audits.count{
                    let Audit = All_Audits[item]
                    let AuditCodeName = Audit["AuditCode"]! as! String
                    let OnGoing = Audit["OnGoing"]! as! String
                    let AuditColor = Audit["Color"]! as! String
                    let AuditStage = Audit["AuditStage"]! as! String
                    let AuditResult = Audit["AuditResult"]! as! String
                    
                    
                    let block = Audit["Block"]! as! Int    // this will be used for BlocksDataDictionary
                    
                    print("AuditCode =",AuditCodeName)
                    print("OnGoing =",OnGoing)
                    print("AuditorColor=\(AuditColor)")
                    print("AuditStage =",AuditStage)
                    print("AuditResult =",AuditResult)
                    
                    print("block =",block)
                    print("__________________")
                    //                        self.AuditCodes.append(AuditCodeName)
                    //                        self.AuditCompleted.append(completedAudit)
                    //                        self.AuditColors.append(AuditColor)
                    
                    // [auditcode:(Auditstage,OnGoing,Color)]
                    var blockValue:[String:(Auditstage:String, OnGoing:String, color:String, auditResult:String)] = [:]
                    
                    if self.BlocksDataDictionary?.keys.count > 0 {
                        
                        for blockNum in (self.BlocksDataDictionary?.keys)! {
                            
                            if blockNum == block{
                                let oldBlockValue = self.BlocksDataDictionary![blockNum]
                                
                                blockValue = oldBlockValue!
                                blockValue.updateValue((Auditstage:AuditStage, OnGoing:OnGoing, color:AuditColor,auditResult:AuditResult), forKey: AuditCodeName)
                                print("block is equal to blockNum =",blockNum,block)
                                break
                            }else{
                                print("block not equal")
                                blockValue.updateValue((Auditstage:AuditStage, OnGoing:OnGoing, color:AuditColor,auditResult:AuditResult), forKey: AuditCodeName)
                            }
                        }
                        self.BlocksDataDictionary?.updateValue(blockValue, forKey: block)
                        
                    }else{
                        
                        blockValue.updateValue((Auditstage:AuditStage, OnGoing:OnGoing, color:AuditColor,auditResult:AuditResult), forKey: AuditCodeName)
                        
                        self.BlocksDataDictionary?.updateValue(blockValue, forKey: block)
                    }
                }
                
                // Save AuditCode and AuditDate in userDefaults so that it can be used when auditing offline or Online
                //                        if completedAudit == "N"{
                //                            defaults.setObject(dateSentInRequest, forKey: AuditCodeName)
                //                        }else{
                //                            defaults.removeObjectForKey(AuditCodeName)
                //                        }
                
                //                    }
                
                if("\(self.dictionary["Status"]!)" == "OK") { // && self.AuditCodes.count > 0){
                    
                    //                        print("AuditCodes-count = \(self.AuditCodes.count)")
                    
                    // Now data in Blocks
                    print("BlocksDataDictionary =",self.BlocksDataDictionary)
                    
                    self.showDataInBlocks(self.BlocksDataDictionary)
                    
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                    
                    
                }
            } //else part of count<1
        }
    }
    // Dictionary of all the Blocks
    
    var BlocksDataDictionary:[Int:[String:(Auditstage:String, OnGoing:String, color:String,auditResult:String)]]? = [:]
   
    
    // individual dictionaries for each block
    
    var BlockDictionary1:[String:(Auditstage:String, OnGoing:String, color:UIColor,auditResult:String)]? = [:]
    var BlockDictionary2:[String:(Auditstage:String, OnGoing:String, color:UIColor,auditResult:String)]? = [:]
    var BlockDictionary3:[String:(Auditstage:String, OnGoing:String, color:UIColor,auditResult:String)]? = [:]
    var BlockDictionary4:[String:(Auditstage:String, OnGoing:String, color:UIColor,auditResult:String)]? = [:]
    var BlockDictionary5:[String:(Auditstage:String, OnGoing:String, color:UIColor,auditResult:String)]? = [:]
    
    var BlockDictionary6:[String:(Auditstage:String, OnGoing:String, color:UIColor,auditResult:String)]? = [:]
    var BlockDictionary7:[String:(Auditstage:String, OnGoing:String, color:UIColor,auditResult:String)]? = [:]
    
    var BlockDictionary8:[String:(Auditstage:String, OnGoing:String, color:UIColor,auditResult:String)]? = [:]
    var BlockDictionary9:[String:(Auditstage:String, OnGoing:String, color:UIColor,auditResult:String)]? = [:]
    var BlockDictionary10:[String:(Auditstage:String, OnGoing:String, color:UIColor,auditResult:String)]? = [:]
    var BlockDictionary11:[String:(Auditstage:String, OnGoing:String, color:UIColor,auditResult:String)]? = [:]
    var BlockDictionary12:[String:(Auditstage:String, OnGoing:String, color:UIColor,auditResult:String)]? = [:]
    
    let DefaultBlockDictionary = [
        "A":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "B":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "C":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "D":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "E":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "F":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "G":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "H":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "I":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "J":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "K":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "L":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "M":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "N":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "O":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "P":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "Q":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "R":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "S":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "T":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "U":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "V":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "W":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0),
        "X":UIColor(colorLiteralRed: 245.0/255, green: 245.0/255, blue: 245.0/255, alpha: 1.0)
        ]

    
    
    var Auditors:[String : AnyObject]?
    var Vendors:[String : AnyObject]?
    
    
    
    
    func getUrl() -> URL {
        let toEscape = "http://app.3-tree.com/quonda/auditors-schedule.php"
        //let urlString = toEscape.addingPercentEscapes(withalloedCharacters: String.Encoding.utf8)!
        let urlString = toEscape.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)

        let url = URL(string: urlString!)
        return url!
    }
   
    var bodyData:String!
    
    func performLoadDataRequestWithURL(_ url: URL, Auditor: String, Vendor: String) -> String?
    {
        
        self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&Auditor=\(Auditor)&Vendor=\(Vendor)"
        print("self.bodyData =",self.bodyData)
        
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = self.bodyData!.data(using: String.Encoding.utf8)
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main){
            response, data, error in
          
            print("\(response)")
           
            if (error != nil) {
                print(error!)
                
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
                
                let alertView = UIAlertController(title: "Data Cannot Fetch" , message: "\(error!.localizedDescription)", preferredStyle: .alert)
                let Reload_action: UIAlertAction = UIAlertAction(title: "Retry", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    self.progressBarDisplayer("Loading. . .", true)
                    self.performLoadDataRequestWithURL(self.getUrl(), Auditor: Auditor, Vendor: Vendor)
                })
                let no_action: UIAlertAction = UIAlertAction(title: "NO", style: .default, handler: { (UIAlertAction) -> Void in
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                })
                
                alertView.addAction(Reload_action)
                alertView.addAction(no_action)
                self.present(alertView, animated: true, completion: nil)
            }
            
            if data != nil {
                
                //Reset all blocksdata
                self.ResetAllBlockData()
                
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                
                self.dictionary = self.parseJSON(json)
                
                print("Json of ManagerMode =\(self.dictionary["Auditors"])")
                
                
                // save list of Auditors and Vendors
                if("\(self.dictionary["Status"]!)" == "OK") {
                    if(self.dictionary["Auditors"] is [String : AnyObject]){
                    self.Auditors = self.dictionary["Auditors"] as! [String : AnyObject]
                    print("Auditors = ",self.Auditors)
                    }
                    if(self.dictionary["Vendors"] is [String : AnyObject]){

                    self.Vendors = self.dictionary["Vendors"] as? [String : AnyObject]
                    
                    print("Vendors = ",self.Vendors)
                    }

                }
                
                
                var planned, completed, pending:Int!
                planned = (self.dictionary["Today"]!as! [String: AnyObject])["Planned"] as! Int
                completed = (self.dictionary["Today"]!as! [String: AnyObject])["Completed"] as! Int
                pending = (self.dictionary["Today"]!as! [String: AnyObject])["Pending"] as! Int
                let day = (self.dictionary["Today"]!as! [String: AnyObject])["Day"] as! String
                
                print("planned =",planned)
                print("completed =",completed)
                print("pending =",pending)
                print("Today[Day] = ",self.dictionary["Today"]!["Day"])
                
//                self.currentDateLabel.setTitle(self.dictionary["Today"]!["Day"] as! String, forState: UIControlState.Normal)
//                self.plannedAudits.text = String(planned)
//                self.completedAudits.text = String(completed)
//                self.pendingReports.text = String(pending)
               
                self.todayBtnLabel.setTitle(day, for: UIControlState())
                self.plannedLabel.text = "\(planned!)"
                self.completedLabel.text = "\(completed!)"
                self.pendingLabel.text = "\(pending!)"
                
                print("planned =",self.plannedLabel.text!)
                print("completed =",self.completedLabel.text!)
                print("pending =", self.pendingLabel.text!)
                
                if (self.dictionary["Audits"] as AnyObject).count < 1 {
                    //                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                    
                    let alertView = UIAlertController(title: nil , message: "No Data Found", preferredStyle: .alert)
                    let ok_action: UIAlertAction = UIAlertAction(title: "Retry", style: .default, handler: { (UIAlertAction) -> Void in
                        
                        self.progressBarDisplayer("Loading. . .", true)
                        self.performLoadDataRequestWithURL(self.getUrl(), Auditor: Auditor, Vendor: Vendor)
                    })
                    let Cancel_action: UIAlertAction = UIAlertAction(title: "Dismiss", style: .default, handler: { (UIAlertAction) -> Void in
                        
                        self.activityIndicator.stopAnimating()
                        self.messageFrame.removeFromSuperview()
                        
                        
                        // Total audits count is zero then show empty blocks.
                        
                        self.showDataInBlocks(self.BlocksDataDictionary)
                       
                        DispatchQueue.main.async(execute: {
                            
                            self.viewDidLayoutSubviews()
                        })
                        
                    })
                    
                    alertView.addAction(Cancel_action)
                    alertView.addAction(ok_action)
                    self.present(alertView, animated: true, completion: nil)
                    
                }else{
                    
                    // save Manager Mode data to Documents Dir
                    
                    if self.fileManager.fileExists(atPath: self.jsonFilePath2.absoluteString, isDirectory: &self.isDirectory) {
                        do{
                            print("old json data in ManagerMode =",json)
                            try json.write(toFile: self.jsonFilePath2.path, atomically: true, encoding: String.Encoding.utf8)
                        } catch let error as NSError {
                            print("JSON data couldn't written to file // File not exist")
                            print(error.description)
                        }
                    }
                    
                   
                    
                    // Reset the bellow data structures
                    
//                    self.AuditCodes.removeAll()
//                    self.AuditCompleted.removeAll()
//                    self.AuditColors.removeAll()
//                    
                    self.BlocksDataDictionary = [:]
                    
                    var All_Audits = self.dictionary["Audits"]! as! [[String : AnyObject]]
                    print("Total Audits = \((self.dictionary["Audits"]! as AnyObject).count)")
                All_Audits = All_Audits.sorted{$1["AuditTime"] as? Date > $0["AuditTime"] as? Date}

            //All_Audits.sorted(by: { $0.0["AuditTime"]! < $0.1["AuditTime"]! })
                    print("All_Audits =",All_Audits)

                    for item  in 0..<All_Audits.count{
                        let Audit = All_Audits[item] 
                        let AuditCodeName = Audit["AuditCode"]! as! String
                        let OnGoing = Audit["OnGoing"]! as! String
                        let AuditColor = Audit["Color"]! as! String
                        let AuditStage = Audit["AuditStage"]! as! String
                        let AuditResult = Audit["AuditResult"]! as! String

                        
                        let block = Audit["Block"]! as! Int    // this will be used for BlocksDataDictionary
                        
                        print("AuditCode =",AuditCodeName)
                        print("OnGoing =",OnGoing)
                        print("AuditorColor=\(AuditColor)")
                        print("AuditStage =",AuditStage)
                        print("AuditResult =",AuditResult)

                        print("block =",block)
                        print("__________________")
//                        self.AuditCodes.append(AuditCodeName)
//                        self.AuditCompleted.append(completedAudit)
//                        self.AuditColors.append(AuditColor)
                        
                        // [auditcode:(Auditstage,OnGoing,Color)]
                        var blockValue:[String:(Auditstage:String, OnGoing:String, color:String, auditResult:String)] = [:]
                        
                        if self.BlocksDataDictionary?.keys.count > 0 {
                            
                            for blockNum in (self.BlocksDataDictionary?.keys)! {
                                
                                if blockNum == block{
                                    let oldBlockValue = self.BlocksDataDictionary![blockNum]
                                    
                                    blockValue = oldBlockValue!
                                    blockValue.updateValue((Auditstage:AuditStage, OnGoing:OnGoing, color:AuditColor,auditResult:AuditResult), forKey: AuditCodeName)
                                    print("block is equal to blockNum =",blockNum,block)
                                    break
                                }else{
                                    print("block not equal")
                                    blockValue.updateValue((Auditstage:AuditStage, OnGoing:OnGoing, color:AuditColor,auditResult:AuditResult), forKey: AuditCodeName)
                                }
                            }
                            self.BlocksDataDictionary?.updateValue(blockValue, forKey: block)
                            
                        }else{
                            
                            blockValue.updateValue((Auditstage:AuditStage, OnGoing:OnGoing, color:AuditColor,auditResult:AuditResult), forKey: AuditCodeName)
                            
                            self.BlocksDataDictionary?.updateValue(blockValue, forKey: block)
                        }
                        }
                    
                        // Save AuditCode and AuditDate in userDefaults so that it can be used when auditing offline or Online
//                        if completedAudit == "N"{
//                            defaults.setObject(dateSentInRequest, forKey: AuditCodeName)
//                        }else{
//                            defaults.removeObjectForKey(AuditCodeName)
//                        }
                    
//                    }
                    
                    if("\(self.dictionary["Status"]!)" == "OK") { // && self.AuditCodes.count > 0){
                       
//                        print("AuditCodes-count = \(self.AuditCodes.count)")
                
                        // Now data in Blocks
                        print("BlocksDataDictionary =",self.BlocksDataDictionary)
                
                        self.showDataInBlocks(self.BlocksDataDictionary)
                
                        self.activityIndicator.stopAnimating()
                        self.messageFrame.removeFromSuperview()
                        
                        
                    }
                } //else part of count<1
            }//if data != nil
            else{
                print("return data is nil")
                //                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
                
                let alertView = UIAlertController(title: "No Data Found" , message: "Press OK to Reload Data", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    self.progressBarDisplayer("Loading. . .", true)
                    self.performLoadDataRequestWithURL(self.getUrl(), Auditor: Auditor, Vendor: Vendor)
                })
                let Cancel_action: UIAlertAction = UIAlertAction(title: "No", style: .default, handler: {(UIAlertAction) -> Void in
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                })
                
                alertView.addAction(Cancel_action)
                alertView.addAction(ok_action)
                self.present(alertView, animated: true, completion: nil)
            }
        }
        return json
    }
    
    
    
    
    func parseJSON(_ jsonString: String) -> [String: AnyObject] {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: AnyObject] {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":"" as AnyObject]
    }

    
    
    
    
    
    func showDataInBlocks(_ BlocksDictionary:[Int:[String:(Auditstage:String, OnGoing:String, color:String, auditResult:String)]]?) -> Void {
        
        //        Block
        //        AuditCodes
        //        Auditstages
        //        OnGoing
        //        AuditColors
        
        // if there is no BlockData found from server then show empty Blocks.
        
        let AllBlockKeys = [0,1,2,3,4,5,6,7,8,9,10,11]
        
        for key in AllBlockKeys {
            
            if !((BlocksDictionary?.keys.contains(key))!){
                
                switch key {
                case 0:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary1, stackViewX: stackVeiw1)
//                    DisplayBlockData1()
                case 1:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary2, stackViewX: stackVeiw2)
//                    DisplayBlockData2()
                case 2:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary3, stackViewX: stackVeiw3)
//                    DisplayBlockData3()
                case 3:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary4, stackViewX: stackVeiw4)
//                    DisplayBlockData4()
                case 4:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary5, stackViewX: stackVeiw5)
//                    DisplayBlockData5()
                case 5:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary6, stackViewX: stackVeiw6)
//                    DisplayBlockData6()
                case 6:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary7, stackViewX: stackVeiw7)
//                    DisplayBlockData7()
                case 7:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary8, stackViewX: stackVeiw8)
//                    DisplayBlockData8()
                case 8:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary9, stackViewX: stackVeiw9)
//                    DisplayBlockData9()
                case 9:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary10, stackViewX: stackVeiw10)
//                    DisplayBlockData10()
                case 10:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary11, stackViewX: stackVeiw11)
//                    DisplayBlockData11()
                case 11:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary12, stackViewX: stackVeiw12)
//                    DisplayBlockData12()
                default:
                    Void()
                }
            }
        }
        
        for blockID in (BlocksDictionary?.keys)! {
            
            let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
            print(blockDataAgainstCurrentKey!)
            switch blockID {
            case 0:
                print("You are going to show BlockDictionary1 12:00 AM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                print(blockDataAgainstCurrentKey!)
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let auditStage = Audit.1.Auditstage
                    let OnGoingStatus = Audit.1.OnGoing
                    let AuditResult = Audit.1.auditResult
                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)

                    BlockDictionary1?.updateValue((Auditstage:auditStage, OnGoing:OnGoingStatus, color:auditColor,auditResult:AuditResult), forKey: auditCode)
                    
                    
                }
                
                //DisplayBlockData1()
                DisplayBlockDataX(BlockDictionaryX: BlockDictionary1, stackViewX: stackVeiw1)
                
                
            case 1:
                print("You are going to show BlockDictionary2 2:00 AM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let auditStage = Audit.1.Auditstage
                    let OnGoingStatus = Audit.1.OnGoing
                    let AuditResult = Audit.1.auditResult
                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    BlockDictionary2?.updateValue((Auditstage:auditStage, OnGoing:OnGoingStatus, color:auditColor,auditResult:AuditResult), forKey: auditCode)
                    
                }
                //DisplayBlockData2()
                DisplayBlockDataX(BlockDictionaryX: BlockDictionary2, stackViewX: stackVeiw2)
                
                
            case 2:
                print("You are going to show BlockDictionary3 4:00 AM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let auditStage = Audit.1.Auditstage
                    let OnGoingStatus = Audit.1.OnGoing
                    let AuditResult = Audit.1.auditResult
                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    BlockDictionary3?.updateValue((Auditstage:auditStage, OnGoing:OnGoingStatus, color:auditColor,auditResult:AuditResult), forKey: auditCode)
                    
                }
                //DisplayBlockData3()
                DisplayBlockDataX(BlockDictionaryX: BlockDictionary3, stackViewX: stackVeiw3)
                
            case 3:
                print("You are going to show BlockDictionary4 6:00 AM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let auditStage = Audit.1.Auditstage
                    let OnGoingStatus = Audit.1.OnGoing
                    let AuditResult = Audit.1.auditResult
                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    BlockDictionary4?.updateValue((Auditstage:auditStage, OnGoing:OnGoingStatus, color:auditColor,auditResult:AuditResult), forKey: auditCode)
                    
                }
                //DisplayBlockData4()
                DisplayBlockDataX(BlockDictionaryX: BlockDictionary4, stackViewX: stackVeiw4)
                
                
                
            case 4:
                print("You are going to show BlockDictionary5 8:00 AM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                print(blockDataAgainstCurrentKey!)
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let auditStage = Audit.1.Auditstage
                    let OnGoingStatus = Audit.1.OnGoing
                    let AuditResult = Audit.1.auditResult

                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    BlockDictionary5?.updateValue((Auditstage:auditStage, OnGoing:OnGoingStatus, color:auditColor,auditResult:AuditResult), forKey: auditCode)
                
                }
                //DisplayBlockData5()
                
                DisplayBlockDataX(BlockDictionaryX: BlockDictionary5, stackViewX: stackVeiw5)
                
                
                
            case 5:
                print("You are going to show BlockDictionary6 10:00 AM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let auditStage = Audit.1.Auditstage
                    let OnGoingStatus = Audit.1.OnGoing
                    let AuditResult = Audit.1.auditResult

                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    BlockDictionary6?.updateValue((Auditstage:auditStage, OnGoing:OnGoingStatus, color:auditColor,auditResult:AuditResult), forKey: auditCode)
                    
                }
                //DisplayBlockData6()
                DisplayBlockDataX(BlockDictionaryX: BlockDictionary6, stackViewX: stackVeiw6)
                
                
            case 6:
                print("You are going to show BlockDictionary7 12:00 PM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let auditStage = Audit.1.Auditstage
                    let OnGoingStatus = Audit.1.OnGoing
                    let AuditResult = Audit.1.auditResult

                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    BlockDictionary7?.updateValue((Auditstage:auditStage, OnGoing:OnGoingStatus, color:auditColor,auditResult:AuditResult), forKey: auditCode)
                    
                    //updateValue(auditColor, forKey: auditCode)
                }
                //DisplayBlockData7()
                DisplayBlockDataX(BlockDictionaryX: BlockDictionary7, stackViewX: stackVeiw7)
                
            case 7:
                print("You are going to show BlockDictionary8 2:00 PM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let auditStage = Audit.1.Auditstage
                    let OnGoingStatus = Audit.1.OnGoing
                    let AuditResult = Audit.1.auditResult

                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    BlockDictionary8?.updateValue((Auditstage:auditStage, OnGoing:OnGoingStatus, color:auditColor, auditResult:AuditResult), forKey: auditCode)
                    
                }
                //DisplayBlockData8()
                DisplayBlockDataX(BlockDictionaryX: BlockDictionary8, stackViewX: stackVeiw8)
                
                
            case 8:
                print("You are going to show BlockDictionary9 4:00 PM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let auditStage = Audit.1.Auditstage
                    let OnGoingStatus = Audit.1.OnGoing
                    let AuditResult = Audit.1.auditResult

                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    BlockDictionary9?.updateValue((Auditstage:auditStage, OnGoing:OnGoingStatus, color:auditColor, auditResult:AuditResult), forKey: auditCode)
                    
                }
                //DisplayBlockData9()
                DisplayBlockDataX(BlockDictionaryX: BlockDictionary9, stackViewX: stackVeiw9)
                
                
            case 9:
                print("You are going to show BlockDictionary10 6:00 PM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let auditStage = Audit.1.Auditstage
                    let OnGoingStatus = Audit.1.OnGoing
                    let AuditResult = Audit.1.auditResult

                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    BlockDictionary10?.updateValue((Auditstage:auditStage, OnGoing:OnGoingStatus, color:auditColor, auditResult:AuditResult), forKey: auditCode)
                    
                }
                //DisplayBlockData10()
                DisplayBlockDataX(BlockDictionaryX: BlockDictionary10, stackViewX: stackVeiw10)
                
                
            case 10:
                print("You are going to show BlockDictionary11 8:00 PM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let auditStage = Audit.1.Auditstage
                    let OnGoingStatus = Audit.1.OnGoing
                    let AuditResult = Audit.1.auditResult

                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    BlockDictionary11?.updateValue((Auditstage:auditStage, OnGoing:OnGoingStatus, color:auditColor, auditResult:AuditResult), forKey: auditCode)
                    
                }
                //DisplayBlockData11()
                DisplayBlockDataX(BlockDictionaryX: BlockDictionary11, stackViewX: stackVeiw11)
                
                
            case 11:
                print("You are going to show BlockDictionary12 10:00 PM")
                let blockDataAgainstCurrentKey = BlocksDictionary![blockID]
                
                for Audit in blockDataAgainstCurrentKey! {
                    
                    let auditCode = Audit.0
                    let auditStage = Audit.1.Auditstage
                    let OnGoingStatus = Audit.1.OnGoing
                    let AuditResult = Audit.1.auditResult

                    let auditColor:UIColor = hexStringToUIColor(Audit.1.color)
                    
                    BlockDictionary12?.updateValue((Auditstage:auditStage, OnGoing:OnGoingStatus, color:auditColor, auditResult:AuditResult), forKey: auditCode)
                    
                }
                //DisplayBlockData12()
                DisplayBlockDataX(BlockDictionaryX: BlockDictionary12, stackViewX: stackVeiw12)
                
                
            default:
                print("default case while showing blocks")
            }
            
        }
        
        
        // Now update stackView scroll Size
        DispatchQueue.main.async(execute: {
            self.viewDidLayoutSubviews()
        })
        
        
        
        //        for BlockNum in Blocks! {
        //
        //            switch BlockNum {
        //            
        //            case 5:
        //                print("You are going to show BlockDictionary6 10:00 AM")
        //            
        //            default:
        //                print("default case while showing blocks")
        //            }
        //        }
    }
    
    
    
    
    
    
    func ResetAllBlockData() -> Void {
        // Reset all blocks data
        
   
         self.BlocksDataDictionary = [:]
        BlockDictionary1 = [:]
        BlockDictionary2 = [:]
        BlockDictionary3 = [:]
        BlockDictionary4 = [:]
        BlockDictionary5 = [:]
        BlockDictionary6 = [:]
        BlockDictionary7 = [:]
        BlockDictionary8 = [:]
        BlockDictionary9 = [:]
        BlockDictionary10 = [:]
        BlockDictionary11 = [:]
        BlockDictionary12 = [:]
        // if there is no BlockData found from server then show empty Blocks.
        
        let AllBlockKeys = [0,1,2,3,4,5,6,7,8,9,10,11]
        
        for key in AllBlockKeys {
            
            if !((BlocksDataDictionary?.keys.contains(key))!){
                
                switch key {
                case 0:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary1, stackViewX: stackVeiw1)
                case 1:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary2, stackViewX: stackVeiw2)
//                    DisplayBlockData2()
                case 2:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary3, stackViewX: stackVeiw3)
//                    DisplayBlockData3()
                case 3:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary4, stackViewX: stackVeiw4)
//                    DisplayBlockData4()
                case 4:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary5, stackViewX: stackVeiw5)
//                    DisplayBlockData5()
                case 5:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary6, stackViewX: stackVeiw6)
//                    DisplayBlockData6()
                case 6:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary7, stackViewX: stackVeiw7)
//                    DisplayBlockData7()
                case 7:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary8, stackViewX: stackVeiw8)
//                    DisplayBlockData8()
                case 8:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary9, stackViewX: stackVeiw9)
//                    DisplayBlockData9()
                case 9:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary10, stackViewX: stackVeiw10)
//                    DisplayBlockData10()
                case 10:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary11, stackViewX: stackVeiw11)
//                    DisplayBlockData11()
                case 11:
                    DisplayBlockDataX(BlockDictionaryX: BlockDictionary12, stackViewX: stackVeiw12)
//                    DisplayBlockData12()
                default:
                    Void()
                }
            }
        }
        
    }
    
    
    
    
    func DisplayBlockDataX(BlockDictionaryX BlockDictionary:[String:(Auditstage:String, OnGoing:String, color:UIColor,auditResult:String)]?  , stackViewX:UIStackView) {
        
        //generate an array
        
        if BlockDictionary != nil && !(BlockDictionary?.isEmpty)! {
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackViewX.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackViewX.removeArrangedSubview(view)
                }
            }
            
            
            var buttonArray = [UIButton]()
            print(BlockDictionary!)
           // item  in 0..<All_Audits.count
            for Data in BlockDictionary!{
               // let Data = BlockDictionary[]
                buttonArray += [colorButton2(withColor: Data.1.color, title: Data.1.Auditstage, OnGoingStatus: Data.1.OnGoing, AuditCode: Data.0,AuditResult: Data.1.auditResult)]
                //(withColor: Data.1.Color, title: Data.0, Subtitle: Data.1.Status)]
                
            }
            
            //            for (myKey,myValue) in BlockDictionary!{
            //                buttonArray += [colorButton(withColor: myValue, title: myKey)]
            //            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    BArray.append(b)
                    count = 2
                    
                }else if count == 2{
                    BArray.append(b)
                    count = 3
                    
                }else if count == 3{
                    BArray.append(b)
                    count = 4
                    
                }else if count == 4{
                    BArray.append(b)
                    count = 5
                }else if count == 5{
                    BArray.append(b)
                    count = 6
                }else if count == 6{
                    BArray.append(b)
                    count = 7
                    
                }else if count == 7{
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 16
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackViewX.spacing = 20
                    stackViewX.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
            
            if count == 1 && !BArray.isEmpty{
                
                let emptyButton = UIButton(type: .system)
                emptyButton.isUserInteractionEnabled = false
                BArray.append(emptyButton)
                
                let emptyButton2 = UIButton(type: .system)
                emptyButton2.isUserInteractionEnabled = false
                BArray.append(emptyButton2)
                
                let emptyButton3 = UIButton(type: .system)
                emptyButton3.isUserInteractionEnabled = false
                BArray.append(emptyButton3)
                
                let emptyButton4 = UIButton(type: .system)
                emptyButton4.isUserInteractionEnabled = false
                BArray.append(emptyButton4)
                
                let emptyButton5 = UIButton(type: .system)
                emptyButton5.isUserInteractionEnabled = false
                BArray.append(emptyButton5)
                
                let emptyButton6 = UIButton(type: .system)
                emptyButton6.isUserInteractionEnabled = false
                BArray.append(emptyButton6)
                
                let emptyButton7 = UIButton(type: .system)
                emptyButton7.isUserInteractionEnabled = false
                BArray.append(emptyButton7)
                
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 16
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackViewX.spacing = 20
                stackViewX.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }else if count == 2 && !BArray.isEmpty{
                
                let emptyButton1 = UIButton(type: .system)
                emptyButton1.isUserInteractionEnabled = false
                BArray.append(emptyButton1)
                
                let emptyButton2 = UIButton(type: .system)
                emptyButton2.isUserInteractionEnabled = false
                BArray.append(emptyButton2)
                
                let emptyButton3 = UIButton(type: .system)
                emptyButton3.isUserInteractionEnabled = false
                BArray.append(emptyButton3)
                
                let emptyButton4 = UIButton(type: .system)
                emptyButton4.isUserInteractionEnabled = false
                BArray.append(emptyButton4)
                
                let emptyButton5 = UIButton(type: .system)
                emptyButton5.isUserInteractionEnabled = false
                BArray.append(emptyButton5)
                
                let emptyButton6 = UIButton(type: .system)
                emptyButton6.isUserInteractionEnabled = false
                BArray.append(emptyButton6)
                
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 16
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackViewX.spacing = 20
                stackViewX.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }else if count == 3 && !BArray.isEmpty{
                
                let emptyButton1 = UIButton(type: .system)
                emptyButton1.isUserInteractionEnabled = false
                BArray.append(emptyButton1)
                
                let emptyButton2 = UIButton(type: .system)
                emptyButton2.isUserInteractionEnabled = false
                BArray.append(emptyButton2)
                
                let emptyButton3 = UIButton(type: .system)
                emptyButton3.isUserInteractionEnabled = false
                BArray.append(emptyButton3)
                
                let emptyButton4 = UIButton(type: .system)
                emptyButton4.isUserInteractionEnabled = false
                BArray.append(emptyButton4)
                
                let emptyButton5 = UIButton(type: .system)
                emptyButton5.isUserInteractionEnabled = false
                BArray.append(emptyButton5)
                
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 16
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackViewX.spacing = 20
                stackViewX.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }else if count == 4 && !BArray.isEmpty{
                
                let emptyButton1 = UIButton(type: .system)
                emptyButton1.isUserInteractionEnabled = false
                BArray.append(emptyButton1)
                
                let emptyButton2 = UIButton(type: .system)
                emptyButton2.isUserInteractionEnabled = false
                BArray.append(emptyButton2)
                
                let emptyButton3 = UIButton(type: .system)
                emptyButton3.isUserInteractionEnabled = false
                BArray.append(emptyButton3)
                
                let emptyButton4 = UIButton(type: .system)
                emptyButton4.isUserInteractionEnabled = false
                BArray.append(emptyButton4)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 16
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackViewX.spacing = 20
                stackViewX.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }else if count == 5 && !BArray.isEmpty{
                
                let emptyButton1 = UIButton(type: .system)
                emptyButton1.isUserInteractionEnabled = false
                BArray.append(emptyButton1)
                
                let emptyButton2 = UIButton(type: .system)
                emptyButton2.isUserInteractionEnabled = false
                BArray.append(emptyButton2)
                
                let emptyButton3 = UIButton(type: .system)
                emptyButton3.isUserInteractionEnabled = false
                BArray.append(emptyButton3)
                
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 16
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackViewX.spacing = 20
                stackViewX.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }else if count == 6 && !BArray.isEmpty{
                
                let emptyButton1 = UIButton(type: .system)
                emptyButton1.isUserInteractionEnabled = false
                BArray.append(emptyButton1)
                
                let emptyButton2 = UIButton(type: .system)
                emptyButton2.isUserInteractionEnabled = false
                BArray.append(emptyButton2)
                
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 16
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackViewX.spacing = 20
                stackViewX.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }else if count == 7 && !BArray.isEmpty{
                
                let emptyButton1 = UIButton(type: .system)
                emptyButton1.isUserInteractionEnabled = false
                BArray.append(emptyButton1)
                
                let H_StackView = UIStackView(arrangedSubviews: BArray)
                H_StackView.axis = .horizontal
                H_StackView.distribution = .fillEqually
                H_StackView.alignment = .fill
                H_StackView.spacing = 16
                H_StackView.translatesAutoresizingMaskIntoConstraints = false
                
                stackViewX.spacing = 20
                stackViewX.addArrangedSubview(H_StackView)
                
                BArray.removeAll()
                count = 0
                
            }
            
        }// if BlockDictionary is not nil
        else{
            
            //First Reset/Remove all the data created in stackView
            
            for view in stackViewX.arrangedSubviews{
                if !view.isKind(of: UILabel.self){
                    stackViewX.removeArrangedSubview(view)
                }
            }
            
            var buttonArray = [UIButton]()
            for (myKey,myValue) in DefaultBlockDictionary{
                buttonArray += [EmptyButton(withColor: myValue, title: "")]
            }
            
            var count = 0
            var BArray = [UIButton]()
            
            for b in buttonArray{
                
                if count == 0{
                    BArray.append(b)
                    count = 1
                    
                }else if count == 1{
                    
                    BArray.append(b)
                    count = 2
                    
                }else if count == 2{
                    
                    BArray.append(b)
                    count = 3
                    
                }else if count == 3{
                    
                    BArray.append(b)
                    count = 4
                    
                }else if count == 4{
                    
                    BArray.append(b)
                    count = 5
                    
                }else if count == 5{
                    
                    BArray.append(b)
                    count = 6
                    
                }else if count == 6{
                    
                    BArray.append(b)
                    count = 7
                    
                }else if count == 7{
                    
                    BArray.append(b)
                    
                    let H_StackView = UIStackView(arrangedSubviews: BArray)
                    H_StackView.axis = .horizontal
                    H_StackView.distribution = .fillEqually
                    H_StackView.alignment = .fill
                    H_StackView.spacing = 0
                    H_StackView.translatesAutoresizingMaskIntoConstraints = false
                    
                    stackViewX.spacing = 0
                    stackViewX.addArrangedSubview(H_StackView)
                    
                    BArray.removeAll()
                    count = 0
                }
                
            }
            
        }
    }
    
    
    
    
    
//                                (color,         auditStage,    OnGoingStatus,         AuditCode)

    func colorButton2(withColor color:UIColor, title:String, OnGoingStatus:String, AuditCode:String, AuditResult:String) -> UIButton{
        
        let newButton = UIButton(type: .system)
        
        newButton.backgroundColor = color
        if(AuditResult == "Pass"){
            newButton.layer.borderColor = UIColor.green.cgColor
            newButton.layer.borderWidth = 1.0
        }
        else if(AuditResult == "Fail"){
            newButton.layer.borderColor = UIColor.red.cgColor
            newButton.layer.borderWidth = 1.0
        }
        newButton.setTitle(title, for: UIControlState())
        newButton.setTitleColor(UIColor.white, for: UIControlState())
        
        let subTitle1 = UILabel(frame: CGRect(x: 1, y: 1, width: 6, height: 10))
        subTitle1.textAlignment = NSTextAlignment.center
        subTitle1.font = subTitle1.font.withSize(6.0)  //11.0
        subTitle1.text = OnGoingStatus
        subTitle1.isHidden = true
        subTitle1.tag = 1
        
        let subTitle2 = UILabel(frame: CGRect(x: 1, y: 1, width: 6, height: 10))//(1, 10, 70, 10))
        subTitle2.textAlignment = NSTextAlignment.center
        subTitle2.font = subTitle2.font.withSize(6.0)  //11.0
        subTitle2.text = AuditCode
        subTitle2.isHidden = true
        subTitle2.tag = 2
        
        newButton.addTarget(self, action: #selector(self.AuditStage_BtnPressed), for: .touchUpInside)
        
        newButton.addSubview(subTitle1)
        newButton.addSubview(subTitle2)
        
        
        // Add blink annimation on button
        
        if OnGoingStatus == "Y"{
            
            let orignalColor = newButton.backgroundColor
            newButton.layer.borderColor = UIColor.lightGray.cgColor
            newButton.layer.borderWidth = 1.0
            UIView.animate(withDuration: 0.7, delay: 0.1, options: [UIViewAnimationOptions.repeat, UIViewAnimationOptions.autoreverse, UIViewAnimationOptions.allowUserInteraction], animations: {
                
                newButton.backgroundColor = UIColor.clear
//                newButton.alpha = 0.0
                }, completion: { (Status) in
                    
                    newButton.backgroundColor = orignalColor
//                    newButton.alpha = 1.0
            })
        }
        return newButton
    }
    
    
    
    
    func EmptyButton(withColor color:UIColor, title:String) -> UIButton{
        let newButton = UIButton(type: .system)
        newButton.backgroundColor = color
        newButton.setTitle(title, for: UIControlState())
        newButton.setTitleColor(UIColor.white, for: UIControlState())
        
        
        return newButton
    }
    
    
    
    
    func AuditStage_BtnPressed(_ senderBtn: UIButton) -> Void {
        var auditCode = ""
        var onGoingStatus = ""
        
        let AuditStage_Btn = senderBtn
        for view in AuditStage_Btn.subviews{
            
            if view.tag == 1{
                onGoingStatus = (view as! UILabel).text!
            }else if view.tag == 2{
                auditCode = (view as! UILabel).text!
            }
        }
        
        print("auditCode = ",auditCode)
        print("onGoingStatus =",onGoingStatus)
        
        
        // show PopUp
        
        let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "managerPopUp") as! managerMode_PopUp
        
        // bellow the data is sending to PopUp 
        popupVC.jsonFilePath = jsonFilePath2
        popupVC.auditCode_Tapped = auditCode
        popupVC.demoText = "Hello I am Manager Mode."
        
        
        self.addChildViewController(popupVC)
        popupVC.view.frame = self.view.frame
        
        self.view.addSubview(popupVC.view)
        self.didMove(toParentViewController: self)
        

//        self.performSegueWithIdentifier("Manager_PoUp", sender: "")
        
    }
    
    
    
    
    
    
    
    
    func progressBarDisplayer(_ msg:String, _ indicator:Bool ) {
        print(msg)
        self.strLabel = UILabel(frame: CGRect(x: view.frame.midX - 40, y: view.frame.midY - 50, width: 200, height: 50))  // x: 50, y: 0, width: 200, height: 50
        self.strLabel.text = msg
        self.strLabel.textColor = UIColor.white
        self.messageFrame = UIView(frame: CGRect(x: 0, y: 69 , width: view.frame.width, height: view.frame.height - 118))  //x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50
        //        messageFrame.layer.cornerRadius = 15
        self.messageFrame.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        if indicator {
            self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
            self.activityIndicator.frame = CGRect(x: view.frame.midX - 80, y: view.frame.midY - 50, width: 50, height: 50)  // x: 0, y: 0, width: 50, height: 50
            self.activityIndicator.startAnimating()
            self.messageFrame.addSubview(activityIndicator)
        }
        self.messageFrame.addSubview(strLabel)
        self.view.addSubview(self.messageFrame)
    }
    
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
            //            defaults.removeObjectForKey("userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            print("self.presentingViewController = \(self.presentingViewController)")
            
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 5:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                present(resultController, animated: true, completion: nil)
            }
            //            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            //            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_selected_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break

        default:
            break
        }
    }
    
    

  /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
       
        if segue.identifier == "Manager_PoUp"{
            
            let destinationVC = segue.destinationViewController as! managerMode_PopUp
            destinationVC.demoText = sender as! String
        }
    }
    */
    func saveSelectedSize(_ strText: String, strID: String) {
        print(strText,strID)
        
    }
}







