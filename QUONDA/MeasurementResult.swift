//
//  MeasurementResult.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 31/10/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class MeasurementResult: UIViewController,UITextViewDelegate {

    @IBOutlet weak var MeasurementRemarks_Arcadia: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.MeasurementRemarks_Arcadia.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            // resignFirstResponder of Arcadia comments here
            self.MeasurementRemarks_Arcadia.resignFirstResponder()
            return false
        }
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
