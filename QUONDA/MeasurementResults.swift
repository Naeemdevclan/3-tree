//
//  MeasurementResults.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 17/10/2017.
//  Copyright © 2017 Muhammad Naeem. All rights reserved.
//

import UIKit

class MeasurementResults: UIViewController , UITabBarDelegate, UITextViewDelegate, SavingViewControllerDelegate {
    
    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!
    
    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    
    @IBOutlet weak var Comments_Arcadia: UITextView!
    @IBOutlet weak var AuditResultBtn: UIButton!

    
    @IBOutlet weak var AuditCode: UILabel!
    @IBOutlet weak var AuditStage: UILabel!
    
    
    var AuditResult: String!
    var jsonFilePath:URL!
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    var dictionary:NSDictionary!
    var MutableDictionary:NSMutableDictionary!
    var AuditCodein: String!
    var AuditStagein: String!
    var AuditStageColorin : UIColor!
    
    // use for show/Hide Comments views
    
    var ReportID_against_current_audit: String!
    
    // variable to recieve ArrayOfDefectsLogged assigned while adding defects
    
    var arrayOfDefectsLoggedRecieved:[((Color:UIColor, ID:String),(DefectType:String,DefectCode:String))]? //[(DefectType:String,DefectCode:String)]?
    
    // WorkmanshipResult variable
    var workmanshipResultValue:String!
    
    
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    var created:Bool!
    var paramPath = "/quonda"
    var otherImagesType:String!
    var savedParamPath:String!
    
    // let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
 
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(AuditCommentsVC.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        // Now add toolbar to each of the keyboard in textfield
      //  self.Comments_Arcadia.inputAccessoryView = doneToolbar
        self.Comments_Arcadia.inputAccessoryView = doneToolbar
        
    }
    
    func doneButtonAction()
    {
        self.Comments_Arcadia.resignFirstResponder()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        Comments_Arcadia.layer.borderWidth = 2.0
        Comments_Arcadia.layer.borderColor = UIColor.gray.cgColor
        Comments_Arcadia.layer.cornerRadius = 5.0
        Comments_Arcadia.text = ""
        Comments_Arcadia.textColor = UIColor.lightGray
        
        
        
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Selected Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)
        
        
        addDoneButtonOnKeyboard()
    }
    
    // adjust the screen size when keyboard appears and hides
    
    func keyboardWillHide(_ sender: Notification) {
        let userInfo: [AnyHashable: Any] = sender.userInfo!
        let keyboardSize: CGSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue.size
        self.view.frame.origin.y += keyboardSize.height
    }
    
    func keyboardWillShow(_ sender: Notification) {
        let userInfo: [AnyHashable: Any] = sender.userInfo!
        let keyboardSize: CGSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue.size
        let offset: CGSize = (userInfo[UIKeyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size
        
        if keyboardSize.height == offset.height {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y -= keyboardSize.height
            })
        } else {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y += keyboardSize.height - offset.height
            })
        }
    }

    
    override func viewWillAppear(_ animated: Bool) {
        self.AuditCode.text = AuditCodein
        self.AuditStage.text = AuditStagein
        self.AuditStage.backgroundColor = AuditStageColorin
        print("report id = ",ReportID_against_current_audit)
        
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
            textView.layer.borderWidth = 2.0
            textView.layer.borderColor = UIColor.orange.cgColor
            textView.layer.cornerRadius = 5.0
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = ""
            textView.textColor = UIColor.lightGray
            textView.layer.borderColor = UIColor.gray.cgColor

        }
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            
            //if ReportID_against_current_audit == "36"{
                textView.resignFirstResponder()
            }
            return true
    }
    
    @IBOutlet weak var lblMeasurementResult: UILabel!
    
    @IBAction func SelectAuditResult(_ sender: AnyObject) {
        
        print("SelectAuditResult button is cliked.")
        
        let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as! PopUpViewController
        
        popupVC.delegate = self
        
        popupVC.pickerdata = ["PASS","FAIL","HOLD"]
        popupVC.pickerdataIDs = ["P","F","H"]
        
        popupVC.WhichButton = "AuditResult"
        
        popupVC.AllreadySelectedText = self.lblMeasurementResult.text!
        self.addChildViewController(popupVC)
        popupVC.view.frame = self.view.frame
        
        self.view.addSubview(popupVC.view)
        self.didMove(toParentViewController: self)
        
    }
    
    func saveAuditResultText(_ strText: String, strID: String) {
        print("Audit Result =",strText)
        print("Result ID =",strID)
       // self.AuditResultBtn.setTitle(strText, for: UIControlState())
        
        self.lblMeasurementResult.text = strText
        self.AuditResult = strID
    }
    
    func saveAuditTypeText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveAuditorsText(_ strText: String, strID: String) {
        // DO Nothing....
    }
    func saveVendorsText(_ strText: String, strID: String) {
        // DO Nothing....
    }
    func saveText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveUnitText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveBrandText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveStyleText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveReportText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveVendorText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveSampleSizeText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveProductionLineText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveAQLText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveAQLData(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveSelectedColor(_ strText : String, strID : String)
    {
        
    }
    
    func showalert(){
        let alertView = UIAlertController(title: nil , message: "Do write some comments", preferredStyle: .alert)
        let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertView.addAction(ok_action)
        self.present(alertView, animated: true, completion: nil)
    }
    
    @IBAction func Save_and_Continue(_ sender: AnyObject) {
        
//        if self.Comments_Arcadia.text == nil || self.Comments_Arcadia.text.isEmpty {
//            self.showalert()
        
       // }else
    if self.AuditResult == nil || self.AuditResult.isEmpty {
            
            
            let alertView = UIAlertController(title: nil , message: "Are you sure Audit Result is Fail?", preferredStyle: .alert)
            let yes_action: UIAlertAction = UIAlertAction(title: "Yes", style: .default, handler: { (UIAlertAction) -> Void in
                
                self.AuditResult = "F"
                //                self.saveContinue_Pressed = true
                //WHEN COMPLETE OPEN OFFLINE SAVE
                self.saveOfflineCommentsData()
                
              //  if ReportId == "36"{  // Hybrid
                    
                    //self.performSegue(withIdentifier: "gotoAuditDetail", sender: nil)
                    
               // }
                //gotoAuditDetail
            })
            let no_action: UIAlertAction = UIAlertAction(title: "No", style: .default, handler: {(UIAlertAction)-> Void in
                
            })
            
            alertView.addAction(yes_action)
            alertView.addAction(no_action)
            
            self.present(alertView, animated: true, completion: nil)
            
            
        }else{
            //            saveContinue_Pressed = true
            //WHEN COMPLETE OPEN OFFLINE SAVE

            self.saveOfflineCommentsData()
           // self.performSegue(withIdentifier: "gotoAuditDetail", sender: nil)
            

        }
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
            //            defaults.removeObjectForKey("userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
            //        case 4:
            //            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
            //                //                self.dismissViewControllerAnimated(true, completion: nil)
            //                presentViewController(resultController, animated: true, completion: nil)
            //            }
            //
            //            let selectedImage:UIImage = UIImage(named: "Selected QAReport")!
            //            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            //
        //            break
        case 5:
            if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                    present(resultController, animated: true, completion: nil)
                }
            }else{
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                    present(resultController, animated: true, completion: nil)
                }
                
            }
            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_selected_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
            
        default:
            break
        }
    }
    
    func getUrl() -> URL {
        let toEscape = "http://app.3-tree.com/quonda/save-comments.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
    var bodyData:String!
   
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    
    func JSON_To_FoundationObj(_ jsonString: String) -> NSMutableDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSMutableDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    
    func parseCommentsJSON(_ jsonString: String) -> [String:String] {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:String] {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    
    
    func saveOfflineCommentsData(){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        print("You are in AuditComments OFFLINE mode.")
        
        // Creating AuditComment Json file
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("MeasurementResultFile\(self.AuditCode.text!).json")
        
        // creating a .json file in the Documents folder
        
        if !fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("File created ")
                
                // Now save it to FilesToSync.json file
                
                saveFileNameTo_FilesToSync("MeasurementResultFile\(self.AuditCode.text!).json")
                //(jsonFilePath.absoluteString)
                
            } else {
                print("Couldn't create file for some reason")
            }
        } else {
            print("File already exists")
        }
        //file creation ends
        
        
        
        //First Read out the AuditComment AuditCodeFile
        
        do{
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath.absoluteString, encoding: String.Encoding.utf8.rawValue) as String
            
            print("readAuditComment_String = ",readString)
            
            /// here send readString to function to update values stored in AuditCommentFile(AuditCode) File
            
            self.AuditCommentAuditCodeFile(readString, AuditCommentFilePath: jsonFilePath.absoluteString)
            
        }
        catch let error as NSError{
            print("There is an error while reading AddDeffectText(AuditCode) File.",error.description)
        }
        
    }// end of func
    
    
    func AuditCommentAuditCodeFile(_ savedCommentsData:String?, AuditCommentFilePath: String) -> Void {
        
        if ReportID_against_current_audit == "32"{
            //if saveContinue_Pressed == true{
            // save data against Arcadia only
            
            AuditCommentAuditCodeFile_Arcadia(savedCommentsData, AuditCommentFilePath: AuditCommentFilePath)
            
        }
        else if ReportID_against_current_audit == "46"{
            //if saveContinue_Pressed == true{
            // save data against Arcadia only
            
            AuditCommentAuditCodeFile_Arcadia(savedCommentsData, AuditCommentFilePath: AuditCommentFilePath)
            
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        //Reading from JsonDictionary File stored in documents directory
        do{
            var readString: String
            readString = try NSString(contentsOfFile: AuditCommentFilePath, encoding: String.Encoding.utf8.rawValue) as String
            //                    print("readString=\(readString)")
            let readStringParsed = parseCommentsJSON(readString)
            print("AuditComments_readStringParsed = ",readStringParsed)
        }
        catch let error as NSError{
            print("There is an error while reading from JsonDictionary File.",error.description)
            
        }
        
    }
    
    func AuditCommentAuditCodeFile_Arcadia(_ savedCommentsData:String?, AuditCommentFilePath: String) -> Void{
        
        // - - - -
        
        if savedCommentsData != nil && savedCommentsData != "" {
            
//            if self.AuditResult == nil{
//                
//                self.AuditResult = "F"
//            }
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodein!)&Result=\(self.AuditResult!)&Remarks=\(self.Comments_Arcadia.text!)"
                print("AuditCommentsArcadia2-bodydata -> \(self.bodyData)")
            
            
            
            var AuditCommentsOfflineData:[String:String] = parseCommentsJSON(savedCommentsData!)
            
            
            let randomString = randomStringWithLength(6)
            
            AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "MeasurementResult_bodyData\(self.AuditCode.text!)_\(randomString)")
            AuditCommentsOfflineData.updateValue("http://app.3-tree.com/quonda/save-measurement-result.php", forKey: "MeasurementResult_url")
            
            
            if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    //Note!!
                    
                    if !Reachability.isConnectedToNetwork(){
                        
                        }
                    
                    
                    // now move onto the Next screen
                    //???????
                    //?????
                    //???
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    defaults.set("Completed", forKey: "MeasurementsResults\(AuditCodein!)")
                    self.performSegue(withIdentifier: "gotoAuditDetail", sender: nil)
                    
                    } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
            
        }else{
            
//            if self.AuditResult == nil{
//                
//                self.AuditResult = "F"
//            }
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodein!)&Result=\(self.AuditResult!)&Remarks=\(self.Comments_Arcadia.text!)"
            
            
            print("AuditCommentsArcadia-bodydata -> \(self.bodyData)")
            
            
            
            var AuditCommentsOfflineData = [String:String]()
            
            
            let randomString = randomStringWithLength(6)
            
            AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "MeasurementResult_bodyData\(self.AuditCode.text!)_\(randomString)")
            AuditCommentsOfflineData.updateValue("http://app.3-tree.com/quonda/save-measurement-result.php", forKey: "MeasurementResult_url")
            
            
            if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    // now move onto the AuditorModeViewController screen
                    //?????
                    //???
                    //??
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    defaults.set("Completed", forKey: "MeasurementsResults\(AuditCodein!)")

                    self.performSegue(withIdentifier: "gotoAuditDetail", sender: nil)
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
        }
        
        // - - -
        
    }
    //Random string function
    
    func randomStringWithLength(_ len: Int) -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for i in 0 ..< (len){
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString
    }

    func updateAuditorModeVCDataWhenOffline(_ savedData:String?) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        print("savedDataString=\(savedData)")
        
        if savedData != nil {
            
            self.MutableDictionary = self.JSON_To_FoundationObj(savedData!)
            
            var planned, completed, pending:Int!
            planned = (self.MutableDictionary["Today"]!as! [String: AnyObject])["Planned"] as! Int
            completed = (self.MutableDictionary["Today"]!as! [String: AnyObject])["Completed"] as! Int
            pending = (self.MutableDictionary["Today"]!as! [String: AnyObject])["Pending"] as! Int
            
            print("Total Audits = \(self.MutableDictionary["Audits"]!)")
            
            //            print("OOOOOOOOoooooooo = \(self.MutableDictionary["Audits"]![0]["AuditCode"]!)")
            //            print((self.MutableDictionary["Audits"]![0] as! [String: AnyObject])["Completed"]!)
            
            
            if var All_Audits = self.MutableDictionary["Audits"]! as? [[String : AnyObject]] {
                
                var i = 0
                for Audit in All_Audits{
                    
                    let AuditCodeName = Audit["AuditCode"]! as! String
                    print("AuditorModeVC_AuditCode= \(AuditCodeName)")
                    
                    let completedAuditt = Audit["Completed"]! as! String
                    print("Before =",completedAuditt)
                    
                    if self.AuditCode.text! == AuditCodeName {
                        print("audit codes are same.")
                        All_Audits[i]["Completed"] = "Y" as AnyObject?
                        self.MutableDictionary["Audits"] = All_Audits
                        
                        //Now its Time to save this update in AuditMode.json file
                        saveOfflineAuditorModeVCData()
                    }
                    
                    i += 1
                }
            }
            print("After changing to Completed = ")
            //            print(self.MutableDictionary["Audits"]![0]["Completed"]!)
            
            //            AuditMode.json
            
        }
    }
    
    
    func saveOfflineAuditorModeVCData(){
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("AuditMode.json")
        
        
        if self.fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &self.isDirectory) {
            do{
                let jsonDictionary = try JSONSerialization.data(withJSONObject: self.MutableDictionary, options: JSONSerialization.WritingOptions.prettyPrinted)
                
                let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                
                try json.write(toFile: jsonFilePath.path, atomically: true, encoding: String.Encoding.utf8)
                
                
            } catch let error as NSError {
                print("AuditMode.json data couldn't be written after update // File not exist")
                print(error.description)
            }
        }
        
        
        //Reading from JsonDictionary File stored in documents directory
        do{
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath.path, encoding: String.Encoding.utf8.rawValue) as String
            //                    print("readString=\(readString)")
            let readStringParsed = parseJSON(readString)
            print("AuditorMode in AuditComments_readStringParsed = ",readStringParsed)
        }
        catch let error as NSError{
            print("There is an error while reading from JsonDictionary File.",error.description)
            
        }
        
        
        
        
        
    }// end of func
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // CAP
        if segue.identifier == "CAP"{
            
            let destinationVC = segue.destination as! CorrectiveActionPlanViewController
            
            //destinationVC.arrayOfDefectsLoggedRecieved_FromCommentsView = self.arrayOfDefectsLoggedRecieved!
            destinationVC.AuditCodeIn = self.AuditCode.text!
            destinationVC.AuditStageIn = self.AuditStage.text!
            destinationVC.AuditStageColor = self.AuditStage.backgroundColor
            //            AuditCodeIn
            //            AuditStageIn
            //            AuditStageColor
            //            arrayOfDefectsLoggedRecieved_FromCommentsView
            
        }else if segue.identifier == "gotoAuditDetail"{
            let destinationVC = segue.destination as! AuditCommentsVC
            destinationVC.AuditCodein = AuditCodein
            destinationVC.AuditStagein = self.AuditStage.text!
            destinationVC.AuditStageColorin = self.AuditStage.backgroundColor!
            
            // here send ReportId to commentsVC for hide/show between two views in commentsView.
            destinationVC.ReportID_against_current_audit = self.ReportID_against_current_audit
            
            
            present(destinationVC, animated: true, completion: nil)
            
        }
        
    }
    
    func saveSelectedSize(_ strText: String, strID: String) {
        print(strText,strID)
        
    }
    ////Naeem Added Code
    /// SAVE IMAGE START
    
    /// SAVE IMAGE END
}
