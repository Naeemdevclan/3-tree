//
//  MeasurementStageTMClothingCell.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 10/04/2017.
//  Copyright © 2017 Muhammad Naeem. All rights reserved.
//

import UIKit

class MeasurementStageTMClothingCell: UITableViewCell {

    @IBOutlet weak var colorName: UILabel!
    
    @IBOutlet weak var lblShipQty: UILabel!
    
    @IBOutlet weak var txtShipQty: UITextField!
    @IBOutlet var txtCartonNos: [UITextField]!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
