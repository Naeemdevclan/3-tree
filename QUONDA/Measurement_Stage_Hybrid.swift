//
//  Measurement Stage.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 15/12/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class Measurement_Stage_Hybrid: UIViewController, UITabBarDelegate, UITextFieldDelegate, SavingViewControllerDelegate,UIScrollViewDelegate {

    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    
    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!


    @IBOutlet weak var AuditcodeLabel: UILabel!
    @IBOutlet weak var AuditStageLabel: UILabel!
    
    // Carton Nos
    @IBOutlet var CartonNos: [UITextField]!
    
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var addCartonBtn: UIButton!
    
    @IBOutlet weak var addCartonsHieghtContraint: NSLayoutConstraint!
    @IBOutlet weak var MSampleSize: UITextField!
    @IBOutlet weak var TPointsOfMeasurement: UITextField!
    @IBOutlet weak var TOutofTolerance: UITextField!
    @IBOutlet weak var MaxPOM_OOTaccepted: UITextField!
    @IBOutlet weak var AuditResultBtn: UIButton!
    
    var AuditResult:String!
    
    var AuditCodeIn:String!
    var AuditStageIn:String!
    var AuditStageColor:UIColor!
    
    @IBAction func addCartonAction(_ sender: UIButton) {
        if(addCartonsHieghtContraint.constant < 494.0)
        {
            addCartonBtn.isHidden = false
            addCartonsHieghtContraint.constant = addCartonsHieghtContraint.constant + 38
        }
        else{
            addCartonBtn.isHidden = true
        }
        print(addCartonsHieghtContraint.constant)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
   
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        self.scrollView.delegate = self
        
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Selected Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)

        

        // set values of current view
        AuditcodeLabel.text = self.AuditCodeIn
        AuditStageLabel.text = self.AuditStageIn
        AuditStageLabel.backgroundColor = self.AuditStageColor
        
        for carton in CartonNos{
            carton.delegate = self
        }
        scrollView.contentSize.height = contentView.frame.height
        MSampleSize.delegate = self
        TPointsOfMeasurement.delegate = self
        TOutofTolerance.delegate = self
        MaxPOM_OOTaccepted.delegate = self
         self.addDoneButtonOnKeyboard()
    }
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        scrollView.contentSize.height = contentView.frame.height //+20
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(Measurement_Stage_Hybrid.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        // Now add toolbar to each of the keyboard in textfield
        self.MSampleSize.inputAccessoryView = doneToolbar
        self.TOutofTolerance.inputAccessoryView = doneToolbar
        self.MaxPOM_OOTaccepted.inputAccessoryView = doneToolbar
        self.TPointsOfMeasurement.inputAccessoryView = doneToolbar
        
        
    }
    
    func doneButtonAction()
    {
        self.MSampleSize.resignFirstResponder()
        self.TOutofTolerance.resignFirstResponder()
        self.MaxPOM_OOTaccepted.resignFirstResponder()
        self.TPointsOfMeasurement.resignFirstResponder()
        
        
    }
    

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == MaxPOM_OOTaccepted {
            if MaxPOM_OOTaccepted.text != nil && TOutofTolerance.text != nil{
                
                if Int(TOutofTolerance.text!)! < Int(MaxPOM_OOTaccepted.text!)! {
                    self.AuditResult = "P"
                    self.AuditResultBtn.setTitle("PASS", for: UIControlState())
               
                }else if Int(TOutofTolerance.text!)! > Int(MaxPOM_OOTaccepted.text!)! {
                    self.AuditResult = "F"
                    self.AuditResultBtn.setTitle("FAIL", for: UIControlState())
                }
            }else{
                print("Total Out of Tolerance and Max POM OOT \n cannot remain empty")
               
                let alertView = UIAlertController(title: nil, message: "Total Out of Tolerance and Max POM OOT \n cannot remain empty", preferredStyle: .alert)
                
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                
                alertView.addAction(ok_action)
                self.present(alertView, animated: true, completion: nil)
            }
            
        }
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func SelectAuditResult(_ sender: Any) {
        
        print("SelectAuditResult button is cliked.")
        
        let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as! PopUpViewController
        
        popupVC.delegate = self
        
        popupVC.pickerdata = ["PASS","FAIL"]
        popupVC.pickerdataIDs = ["P","F"]
        
        popupVC.WhichButton = "AuditResult"
        
        popupVC.AllreadySelectedText = self.AuditResultBtn.titleLabel!.text!
        
        self.addChildViewController(popupVC)
        popupVC.view.frame = self.view.frame
        
        self.view.addSubview(popupVC.view)
        self.didMove(toParentViewController: self)
        
    }
    
    func saveAuditResultText(_ strText: String, strID: String) {
        print("Audit Result =",strText)
        print("Result ID =",strID)
        self.AuditResultBtn.setTitle(strText, for: UIControlState())
        self.AuditResult = strID
    }
    
    func saveAuditTypeText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveAuditorsText(_ strText: String, strID: String) {
        // DO Nothing....
    }
    func saveVendorsText(_ strText: String, strID: String) {
        // DO Nothing....
    }
    func saveText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveUnitText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveBrandText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveStyleText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveReportText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveVendorText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveSampleSizeText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveProductionLineText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveAQLText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveAQLData(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveSelectedSize(_ strText: String, strID: String) {
        print(strText,strID)
        
    }
    func saveSelectedColor(_ strText : String, strID : String)
    {
        
    }
    // array of carton numbers
    
    var ArrayOfCartonNumbers:[String]! = []
    
    @IBAction func saveAndContinue(_ sender: Any) {
        
        print("saveButton Pressed.")
        
        
        for carton in CartonNos{
            if carton.text != nil{
                ArrayOfCartonNumbers.append(carton.text!)
            }
        }
print(ArrayOfCartonNumbers)
        if ArrayOfCartonNumbers.count != 0{
        
        if self.MSampleSize.text != nil && self.TPointsOfMeasurement.text != nil && self.TOutofTolerance.text != nil && self.MaxPOM_OOTaccepted.text != nil && self.AuditResult != nil{
            
            
            performSegue(withIdentifier: "gotoPackagingStageHybrid", sender: nil)
        }else{
            popupMsg(msg: "Please enter all the values.")
            }
        }else{
            popupMsg(msg: "There must be one cartonNo atleast.")
        }
    }
    

    func popupMsg(msg:String){
        
        let alertView = UIAlertController(title: nil , message: msg, preferredStyle: .alert)
        let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertView.addAction(ok_action)
        self.present(alertView, animated: true, completion: nil)
    }
    
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
            defaults.removeObject(forKey: "userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
            //        case 4:
            //            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
            //                //                self.dismissViewControllerAnimated(true, completion: nil)
            //                presentViewController(resultController, animated: true, completion: nil)
            //            }
            //
            //            let selectedImage:UIImage = UIImage(named: "Selected QAReport")!
            //            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            //
        //            break
        case 5:
            DefectsData0.removeAll()
            DefectsRateData0.removeAll()
            DefectsAreaData0.removeAll()
            
            self.dismiss(animated: true, completion: nil)
            //            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("5") as? ManagerModeViewController {
            //                self.dismissViewControllerAnimated(true, completion: nil)
            //                presentViewController(resultController, animated: true, completion: nil)
            //            }
            //            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            //            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_selected_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        default:
            break
        }
    }

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "gotoPackagingStageHybrid"{
            
            let destinationNVC = segue.destination as! UINavigationController
            
            let destinationVC = destinationNVC.viewControllers.first as! Packaging_Stage_Hybrid

//            let destinationVC = segue.destination as! Packaging_Stage_Hybrid
            
            destinationVC.AuditCodeIn = self.AuditcodeLabel.text!
            destinationVC.AuditStageIn = self.AuditStageLabel.text!
            destinationVC.AuditStageColor = self.AuditStageLabel.backgroundColor
            
            
            destinationVC.CartonNumbers = ArrayOfCartonNumbers
            destinationVC.MeasurementSampleSizeIn = self.MSampleSize.text!
            destinationVC.OutOfToleranceIn = self.TOutofTolerance.text!
            destinationVC.PomIn = self.TPointsOfMeasurement.text!
            destinationVC.MeasurementResultIn = self.AuditResult!
            
        }
        
    }
}


