//
//  Measurement_Stage_TMClothing.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 07/04/2017.
//  Copyright © 2017 Muhammad. All rights reserved.
//

import UIKit

class Measurement_Stage_TMClothing: UIViewController,UITabBarDelegate, UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    //Tab bar 1 items
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    //Tab bar 2 items
    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!

    @IBOutlet weak var AuditstageLabel: UILabel!
    @IBOutlet weak var auditcodeLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    //MARK: Properties
    var AuditCodeIn:String!
    var AuditStageIn:String!
    var AuditStageColor:UIColor!
    var ReportID_against_current_audit: String!
    var SelecteColors :[Any]!
    var cartonForColors = [String:Any]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTabBars()
        print(SelecteColors)
        // set values of current view
        auditcodeLabel.text = self.AuditCodeIn
        AuditstageLabel.text = self.AuditStageIn
        AuditstageLabel.backgroundColor = self.AuditStageColor
        
         tableView.reloadData()
        // Do any additional setup after loading the view.
    }
    func setupTabBars(){
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Selected Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
          Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)

    }
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
            defaults.removeObject(forKey: "userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
            //        case 4:
            //            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
            //                //                self.dismissViewControllerAnimated(true, completion: nil)
            //                presentViewController(resultController, animated: true, completion: nil)
            //            }
            //
            //            let selectedImage:UIImage = UIImage(named: "Selected QAReport")!
            //            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            //
        //            break
        case 5:
            DefectsData0.removeAll()
            DefectsRateData0.removeAll()
            DefectsAreaData0.removeAll()
            
            self.dismiss(animated: true, completion: nil)
            //            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("5") as? ManagerModeViewController {
            //                self.dismissViewControllerAnimated(true, completion: nil)
            //                presentViewController(resultController, animated: true, completion: nil)
            //            }
            //            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            //            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_selected_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break

        default:
            break
        }
    }
    

    func popupMsg(msg:String){
        
        let alertView = UIAlertController(title: nil , message: msg, preferredStyle: .alert)
        let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertView.addAction(ok_action)
        self.present(alertView, animated: true, completion: nil)
    }
    //MARK: TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2;
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section{
        case 0:
            return SelecteColors.count + 1;
        case 1:
             return SelecteColors.count;
        default:
            return SelecteColors.count;
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell :MeasurementStageTMClothingCell!
        switch indexPath.section{
        case 0:
            cell = tableView.dequeueReusableCell(withIdentifier: "shipmentQuantityCell", for: indexPath) as! MeasurementStageTMClothingCell
            cell.txtShipQty.layer.borderWidth = 2.0
            cell.txtShipQty.layer.cornerRadius = 2.0
            cell.txtShipQty.layer.borderColor = UIColor.lightGray.cgColor
            if(indexPath.row == 0){
                cell.colorName.font = UIFont.boldSystemFont(ofSize: 17.0)
                cell.lblShipQty.isHidden = false
                cell.txtShipQty.isHidden = true
            }else{
                cell.colorName.font = UIFont.systemFont(ofSize: 17.0)
            cell.lblShipQty.isHidden = true
            cell.txtShipQty.isHidden = false
            cell.colorName.text = "\(SelecteColors[indexPath.row - 1])"
            cell.txtShipQty.tag = 200 + indexPath.row
            cell.txtShipQty.delegate = self
            print(cell.txtShipQty.tag)
            //cell.txtShipQty.text = "20"
            }
             break;

        case 1:
            cell = tableView.dequeueReusableCell(withIdentifier: "cartonNumbersCell", for: indexPath) as! MeasurementStageTMClothingCell
            cell.colorName.text = "\(SelecteColors[indexPath.row])"
            var txtCartons  = [UITextField]()
            for carton in cell.txtCartonNos{
                carton.delegate = self
                txtCartons.append(carton)
            }
//             var  i = 0
//            var index = 0
//            if(indexPath.row == 0){
//                 i = 300
//            }
//            else{
//                i = 300 + (indexPath.row * 100)
//            }
//            for item in 0..<cell.txtCartonNos.count{
//                index = index + 1
//                cell.txtCartonNos[item].delegate = self
//                cell.txtCartonNos[item].tag = i + index
//                txtCartons.append(cell.txtCartonNos[item])
//                print(cell.txtCartonNos[item].tag)
//            }
            cartonForColors["\(SelecteColors[indexPath.row])"] = txtCartons
            
            print(cartonForColors)

             break;
        default:
            break;
        }
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let footerView = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:40))
        footerView.backgroundColor = UIColor.lightGray
        let titleLabel = UILabel.init(frame: CGRect(x:20, y:0, width:tableView.frame.size.width - 40, height:40))
        titleLabel.textAlignment = .left
        titleLabel.textColor = UIColor.darkGray
        titleLabel.font = UIFont.boldSystemFont(ofSize: 22.0)
        footerView.addSubview(titleLabel)
        switch section{
        case 0:
            titleLabel.text = "Shipment Quantity"
            break;
        case 1:
            titleLabel.text = "Carton Numbers"
            break;
        default:
            break;
        }
        return footerView

    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        
        switch section{
        case 0:
       return nil
        case 1:
            let footerView = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:280))
            footerView.backgroundColor = UIColor.clear
            let saveBtn = UIButton.init()
            saveBtn.backgroundColor = UIColor.orange
            saveBtn.layer.cornerRadius = 5.0
            saveBtn.setTitle("Save & Contineou", for: .normal)
            saveBtn.addTarget(self, action: #selector(Measurement_Stage_TMClothing.saveBtnPressed), for: .touchUpInside)
            saveBtn.frame.size = CGSize(width:170,height:30)
            saveBtn.frame = CGRect(x:20, y:10, width:tableView.frame.size.width/2, height:30)
           // saveBtn.center = footerView.center
            footerView.addSubview(saveBtn)
            return footerView
        default:
            return nil
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section{
        case 0:
            return 40.0;
        case 1:
            return 40.0;
        default:
            return 0.0;
        }

    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section{
        case 0:
            return 0.0;
        case 1:
            return 280.0;
        default:
            return 0.0;
        }

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section{
        case 0:
            return 57.0;
        case 1:
            return 172.0;
        default:
            return 0.0;
        }

    }
    func saveBtnPressed(sender : UIButton){
        
        btnPressedAction()
        
       // var cell :MeasurementStageTMClothingCell!
       // var cell2 :MeasurementStageTMClothingCell!
        
    }
    func getValues(){
        // var cell :MeasurementStageTMClothingCell!

 
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      //  let indexPath2 = NSIndexPath.init(item: 0, section: 1)
        //cell = tableView.dequeueReusableCell(withIdentifier: "cartonNumbersCell", for:indexPath2 as IndexPath) as! MeasurementStageTMClothingCell
//        let cell2 = tableView.dequeueReusableCell(withIdentifier: "cartonNumbersCell") as! MeasurementStageTMClothingCell
//        for carton in cell2.txtCartonNos{
//            print(carton.tag)
//            print(carton.text ?? "")
//        }
        textField.resignFirstResponder()
        return true
    }
    
    
    var task:URLSessionDataTask?
    
    func performLoadDataRequestWithURL(bodyDataIn:String) -> String? {
        
        //        let jsonData = try! JSONSerialization.data(withJSONObject: TAG_CAPsArray, options: JSONSerialization.WritingOptions())
        //        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as! String
        //        print("jsonString =",jsonString)
        
        
        //        let bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCodeIn)"
        //
        //        print("bodyData =",bodyData)
        
        let request: NSMutableURLRequest = NSMutableURLRequest(url: self.getUrl())
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = bodyDataIn.data(using: String.Encoding.utf8)
        let session = URLSession.shared
        
        task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            print("response =",response)
            
            if (error != nil) {
                print(error)
                
                
                let alertView = UIAlertController(title: nil , message: "Cannot save this Stage \n try saving again.", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                })
                alertView.addAction(ok_action)
                
                self.present(alertView, animated: true, completion: nil)
                
                
            }
            else if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                
                print(json)
                print("Packaging status is OK")
                
                self.performSegue(withIdentifier: "gotoTMClothingCommentVC", sender: nil)
                isMreasurementTmClothing = true
                print(self.AuditCodeIn)
                print("isMreasurementTmClothingCode \(self.AuditCodeIn!)")
                defaults.set(self.AuditCodeIn, forKey:"isMreasurementTmClothingCode \(self.AuditCodeIn!)")
               print("\(defaults.object(forKey: "isMreasurementTmClothingCode \(self.AuditCodeIn!)"))")
                //                if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                //                    self.present(resultController, animated: true, completion: nil)
                //                }
                
            }//if data != nil
            else{
                print("return data is nil")
            }
            
        })
        
        task?.resume()
        return ""
    }
    
    
    func getUrl() -> URL {
        let toEscape = "http://app.3-tree.com/quonda/save-armedangles.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
    var bodyData = ""
    func btnPressedAction(){
        do{
//            let jsonCartonNumbers = try JSONSerialization.data(withJSONObject: self.CartonNumbers!, options: JSONSerialization.WritingOptions.prettyPrinted)
//            
//            //        let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
//            print(jsonCartonNumbers)
//            let cartonString = self.CartonNumbers.joined(separator: ",")
            var QuantitiesCarton = [String:Any]()
            for i in 0..<SelecteColors.count{
                print(i)
                let items = cartonForColors["\(SelecteColors[i])"] as! [UITextField]
                var cartons = [String]()
                for carton in items{
                    print(carton.tag)
                    print(carton.text ?? "")
                    if(carton.text != ""){
                        cartons.append(carton.text!)
                    }
                }
                let stringRepresentation = cartons.joined(separator: ",") // "1-2-3"
                QuantitiesCarton["\(SelecteColors[i])"] = stringRepresentation
                
            }
            print(QuantitiesCarton)
            
            var Quantities = [String:Any]()
            for i in 0..<SelecteColors.count{
                print(i)
                let indexPath = NSIndexPath.init(item: i, section: 0)
                let cell = tableView.dequeueReusableCell(withIdentifier: "shipmentQuantityCell", for:indexPath as IndexPath) as! MeasurementStageTMClothingCell
                print(cell.txtShipQty.text!)
                Quantities["\(SelecteColors[i])"] = cell.txtShipQty.text
            }
            print(Quantities)
            

            //self.getValues()

            
            bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCodeIn!)&Quantities=\(QuantitiesCarton)&CartonNos=\(Quantities)"
            
            print("Packaging_bodyData = ",bodyData)
            
            performLoadDataRequestWithURL(bodyDataIn: bodyData)
            
        }catch{
            print("cannot prepare JsonCarton Numbers")
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "gotoTMClothingCommentVC"{
            
//            let destinationNVC = segue.destination as! UINavigationController
//            let destinationVC = destinationNVC.viewControllers.first as! TMClothingCommentVC
            let destinationVC = segue.destination as! TMClothingCommentVC
        
            //            let destinationVC = segue.destination as! HybridCommentViewController
            destinationVC.AuditCodein = self.auditcodeLabel.text!
            destinationVC.AuditStagein = self.AuditstageLabel.text!
            destinationVC.AuditStageColorin = self.AuditstageLabel.backgroundColor!
            destinationVC.ReportID_against_current_audit = "38"
        }
    }

    
}

