//
//  MiscTabCell.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 17/05/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class MiscTabCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var LargePictureString: UILabel!
}
