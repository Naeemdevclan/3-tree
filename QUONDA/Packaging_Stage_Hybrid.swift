//
//  Packaging_Stage_Hybrid.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 15/12/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class Packaging_Stage_Hybrid: UIViewController, UITabBarDelegate, UIScrollViewDelegate, UITextFieldDelegate, SavingHybridDelegate {

    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    
    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    

    @IBOutlet weak var AuditCodeLabel: UILabel!
    @IBOutlet weak var AuditStageLabel: UILabel!
   
    var AuditCodeIn:String!
    var AuditStageIn:String!
    var AuditStageColor:UIColor!
    
    var CartonNumbers:[String]!
    var MeasurementSampleSizeIn,OutOfToleranceIn,PomIn,MeasurementResultIn:String!
    
    @IBOutlet weak var outMostScrollView: UIScrollView!
    @IBOutlet weak var outMostStackView: UIStackView!
    
    
    // views in sub Stack Views
    
    // list of Reason views
         // Packaging
    @IBOutlet weak var AssortmentReasonView: UIView!
    @IBOutlet weak var MainLabelReasonView: UIView!
    @IBOutlet weak var CareLabelReasonView: UIView!
    @IBOutlet weak var HangtagReasonView: UIView!
    @IBOutlet weak var PolybagReasonView: UIView!
    @IBOutlet weak var StickerReasonView: UIView!
    @IBOutlet weak var PriceReasonView: UIView!
    @IBOutlet weak var HangerReasonView: UIView!
    @IBOutlet weak var CartonDimensionReasonView: UIView!
    @IBOutlet weak var ShippingMarksReasonView: UIView!
    @IBOutlet weak var PakagingOthersReasonView: UIView!
        // Garment Conformity
    @IBOutlet weak var ConstructionReasonView: UIView!
    @IBOutlet weak var AppearanceReasonView: UIView!
    @IBOutlet weak var HandfeelReasonView: UIView!
    @IBOutlet weak var ThreadReasonView: UIView!
    @IBOutlet weak var StitchesReasonView: UIView!
    @IBOutlet weak var PrintReasonView: UIView!
    @IBOutlet weak var ColorComboReasonView: UIView!
    @IBOutlet weak var ComponentsReasonView: UIView!
    @IBOutlet weak var TrimsReasonView: UIView!
    @IBOutlet weak var AccessoriesReasonView: UIView!
    @IBOutlet weak var GarmentConformityOthersReasonView: UIView!
    

    
    // list of Reason text boxes
        // Packaging
    @IBOutlet weak var AssortmentReason: UITextField!
    @IBOutlet weak var MainLabelReason: UITextField!
    @IBOutlet weak var CareLabelReason: UITextField!
    @IBOutlet weak var HangtagReason: UITextField!
    @IBOutlet weak var PolybagReason: UITextField!
    @IBOutlet weak var StickerReason: UITextField!
    @IBOutlet weak var PriceReason: UITextField!
    @IBOutlet weak var HangerReason: UITextField!
    @IBOutlet weak var CartonDimensionReason: UITextField!
    @IBOutlet weak var ShippingMarksReason: UITextField!
    @IBOutlet weak var PakagingOthersReason: UITextField!
        // Garment Conformity
    @IBOutlet weak var ConstructionReason: UITextField!
    @IBOutlet weak var AppearanceReason: UITextField!
    @IBOutlet weak var HandfeelReason: UITextField!
    @IBOutlet weak var ThreadReason: UITextField!
    @IBOutlet weak var StitchesReason: UITextField!
    @IBOutlet weak var PrintReason: UITextField!
    @IBOutlet weak var ColorComboReason: UITextField!
    @IBOutlet weak var ComponentsReason: UITextField!
    @IBOutlet weak var TrimsReason: UITextField!
    @IBOutlet weak var AccessoriesReason: UITextField!
    @IBOutlet weak var GarmentConformityOthersReason: UITextField!
    
    
    
    
    
    
    // list of buttons
        // Packaging
    @IBOutlet weak var AssortmentBtn: UISegmentedControl!
    @IBOutlet weak var MainLabelBtn: UISegmentedControl!
    @IBOutlet weak var CareLabelBtn: UISegmentedControl!
    @IBOutlet weak var HangtagBtn: UISegmentedControl!
    @IBOutlet weak var PolybagBtn: UISegmentedControl!
    @IBOutlet weak var StickerBtn: UISegmentedControl!
    @IBOutlet weak var PriceBtn: UISegmentedControl!
    @IBOutlet weak var HangerBtn: UISegmentedControl!
    @IBOutlet weak var CartonDimensionBtn: UISegmentedControl!
    @IBOutlet weak var ShippingMarksBtn: UISegmentedControl!
    @IBOutlet weak var PakagingOthersBtn: UISegmentedControl!
         // Garment Conformity
    @IBOutlet weak var ConstructionBtn: UISegmentedControl!
    @IBOutlet weak var AppearanceBtn: UISegmentedControl!
    @IBOutlet weak var HandfeelBtn: UISegmentedControl!
    @IBOutlet weak var ThreadBtn: UISegmentedControl!
    @IBOutlet weak var StitchesBtn: UISegmentedControl!
    @IBOutlet weak var PrintBtn: UISegmentedControl!
    @IBOutlet weak var ColorComboBtn: UISegmentedControl!
    @IBOutlet weak var ComponentsBtn: UISegmentedControl!
    @IBOutlet weak var TrimsBtn: UISegmentedControl!
    @IBOutlet weak var AccessoriesBtn: UISegmentedControl!
    @IBOutlet weak var GarmentConformityOthersBtn: UISegmentedControl!
    
    
    
    @IBOutlet weak var PackagingAuditResultBtn: UIButton!
    @IBOutlet weak var GarmentConformityAuditResultBtn: UIButton!
    
    // Audit results
    var PackagingAuditResult:String!
    var GarmentConformityAuditResult:String!
    
    
// List of buttons IDs
    var AssortmentID:String! = ""
    var MainLabelID:String! = ""
    var CareLabelID:String! = ""
    var HangtagID:String! = ""
    var PolybagID:String! = ""
    var StickerID:String! = ""
    var PriceID:String! = ""
    var HangerID:String! = ""
    var CartonDimensionID:String! = ""
    var ShippingMarksID:String! = ""
    var PakagingOthersID:String! = ""
    var ConstructionID:String! = ""
    var AppearanceID:String! = ""
    var HandfeelID:String! = ""
    var ThreadID:String! = ""
    var StitchesID:String! = ""
    var PrintID:String! = ""
    var ColorComboID:String! = ""
    var ComponentsID:String! = ""
    var TrimsID:String! = ""
    var AccessoriesID:String! = ""
    var GarmentConformityOthersID:String! = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Selected Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        
        
        self.outMostScrollView.delegate = self
        
        AuditCodeLabel.text = self.AuditCodeIn
        AuditStageLabel.text = self.AuditStageIn
        AuditStageLabel.backgroundColor = AuditStageColor
        
        // IN Start just Hide all the Reason subviews
            // Packaging
//        AssortmentReasonView.isHidden = true
//        MainLabelReasonView.isHidden = true
//        CareLabelReasonView.isHidden = true
//        HangtagReasonView.isHidden = true
//        PolybagReasonView.isHidden = true
//        StickerReasonView.isHidden = true
//        PriceReasonView.isHidden = true
//        HangerReasonView.isHidden = true
//        CartonDimensionReasonView.isHidden = true
//        ShippingMarksReasonView.isHidden = true
//        PakagingOthersReasonView.isHidden = true
//        
//            // Garment Conformity
//        ConstructionReasonView.isHidden = true
//        AppearanceReasonView.isHidden = true
//        HandfeelReasonView.isHidden = true
//        ThreadReasonView.isHidden = true
//        StitchesReasonView.isHidden = true
//        PrintReasonView.isHidden = true
//        ColorComboReasonView.isHidden = true
//        ComponentsReasonView.isHidden = true
//        TrimsReasonView.isHidden = true
//        AccessoriesReasonView.isHidden = true
//        GarmentConformityOthersReasonView.isHidden = true
        
        
            // Set Delegates
//        CartonNumbers.delegate = self
        AssortmentReason.delegate = self
        MainLabelReason.delegate = self
        CareLabelReason.delegate = self
        HangtagReason.delegate = self
        PolybagReason.delegate = self
        StickerReason.delegate = self
        PriceReason.delegate = self
        HangerReason.delegate = self
        CartonDimensionReason.delegate = self
        ShippingMarksReason.delegate = self
        PakagingOthersReason.delegate = self
        ConstructionReason.delegate = self
        AppearanceReason.delegate = self
        HandfeelReason.delegate = self
        ThreadReason.delegate = self
        StitchesReason.delegate = self
        PrintReason.delegate = self
        ColorComboReason.delegate = self
        ComponentsReason.delegate = self
        TrimsReason.delegate = self
        AccessoriesReason.delegate = self
        GarmentConformityOthersReason.delegate = self
        
      print(self.CartonNumbers)
        
    }
    
    override func viewDidLayoutSubviews() {
        outMostScrollView.contentSize.height = outMostStackView.frame.size.height
    }
    
  
    
    // Button Actions
    
    @IBAction func ChangeBtnStatus(_ sender: UISegmentedControl) {
        
//        switch sender {
//        case AssortmentBtn:
//            switch AssortmentBtn.selectedSegmentIndex {
//            case 0:
//                // Hide the Reason text bar
//                AssortmentReasonView.isHidden = true
//            case 1:
//                // show the Reason text bar
//                AssortmentReasonView.isHidden = false
//            case 2:
//                // Hide the Reason text bar
//                AssortmentReasonView.isHidden = true
//            }
//        case MainLabelBtn:
//            break
//        default:
//            <#code#>
//        }
        
        
        
        
        if sender == AssortmentBtn{
            
            switch AssortmentBtn.selectedSegmentIndex {
            case 0:
                AssortmentID = "C"
                // Hide the Reason text bar
                //AssortmentReasonView.isHidden = true
            case 1:
                AssortmentID = "NC"
                // show the Reason text bar
                //AssortmentReasonView.isHidden = false
            case 2:
                AssortmentID = "NA"
                // Hide the Reason text bar
                //AssortmentReasonView.isHidden = true
            default:
                break
            }
        }else if sender == MainLabelBtn{
            
            switch MainLabelBtn.selectedSegmentIndex {
            case 0:
                MainLabelID = "C"
                // Hide the Reason text bar
                //MainLabelReasonView.isHidden = true
            case 1:
                MainLabelID = "NC"
                // show the Reason text bar
                //MainLabelReasonView.isHidden = false
            case 2:
                MainLabelID = "NA"
                // Hide the Reason text bar
                //MainLabelReasonView.isHidden = true
            default:
                break
            }
            
        }else if sender == CareLabelBtn{
            
            switch CareLabelBtn.selectedSegmentIndex {
            case 0:
                CareLabelID = "C"
                // Hide the Reason text bar
                //CareLabelReasonView.isHidden = true
            case 1:
                CareLabelID = "NC"
                // show the Reason text bar
                //CareLabelReasonView.isHidden = false
            case 2:
                CareLabelID = "NA"
                // Hide the Reason text bar
                //CareLabelReasonView.isHidden = true
            default:
                break
            }
            
        }else if sender == HangtagBtn{
            
            switch HangtagBtn.selectedSegmentIndex {
            case 0:
                HangtagID = "C"
                // Hide the Reason text bar
                //HangtagReasonView.isHidden = true
            case 1:
                HangtagID = "NC"
                // show the Reason text bar
                //HangtagReasonView.isHidden = false
            case 2:
                HangtagID = "NA"
                // Hide the Reason text bar
                //HangtagReasonView.isHidden = true
            default:
                break
            }
        }else if sender == PolybagBtn{
            
            switch PolybagBtn.selectedSegmentIndex {
            case 0:
                PolybagID = "C"
                // Hide the Reason text bar
                //PolybagReasonView.isHidden = true
            case 1:
                PolybagID = "NC"
                // show the Reason text bar
                //PolybagReasonView.isHidden = false
            case 2:
                PolybagID = "NA"
                // Hide the Reason text bar
                //PolybagReasonView.isHidden = true
            default:
                break
            }
        }else if sender == StickerBtn{
            
            switch StickerBtn.selectedSegmentIndex {
            case 0:
                StickerID = "C"
                // Hide the Reason text bar
                //StickerReasonView.isHidden = true
            case 1:
                StickerID = "NC"
                // show the Reason text bar
                //StickerReasonView.isHidden = false
            case 2:
                StickerID = "NA"
                // Hide the Reason text bar
                //StickerReasonView.isHidden = true
            default:
                break
            }
        }else if sender == PriceBtn{
            
            switch PriceBtn.selectedSegmentIndex {
            case 0:
                PriceID = "C"
                // Hide the Reason text bar
                //PriceReasonView.isHidden = true
            case 1:
                PriceID = "NC"
                // show the Reason text bar
                //PriceReasonView.isHidden = false
            case 2:
                PriceID = "NA"
                // Hide the Reason text bar
                //PriceReasonView.isHidden = true
            default:
                break
            }
        }else if sender == HangerBtn{
            
            switch HangerBtn.selectedSegmentIndex {
            case 0:
                HangerID = "C"
                // Hide the Reason text bar
                //HangerReasonView.isHidden = true
            case 1:
                HangerID = "NC"
                // show the Reason text bar
                //HangerReasonView.isHidden = false
            case 2:
                HangerID = "NA"
                // Hide the Reason text bar
                //HangerReasonView.isHidden = true
            default:
                break
            }
        }else if sender == CartonDimensionBtn{
            
            switch CartonDimensionBtn.selectedSegmentIndex {
            case 0:
                CartonDimensionID = "C"
                // Hide the Reason text bar
                //CartonDimensionReasonView.isHidden = true
            case 1:
                CartonDimensionID = "NC"
                // show the Reason text bar
                //CartonDimensionReasonView.isHidden = false
            case 2:
                CartonDimensionID = "NA"
                // Hide the Reason text bar
                //CartonDimensionReasonView.isHidden = true
            default:
                break
            }
        }else if sender == ShippingMarksBtn{
            
            switch ShippingMarksBtn.selectedSegmentIndex {
            case 0:
                ShippingMarksID = "C"
                // Hide the Reason text bar
                //ShippingMarksReasonView.isHidden = true
            case 1:
                ShippingMarksID = "NC"
                // show the Reason text bar
                //ShippingMarksReasonView.isHidden = false
            case 2:
                ShippingMarksID = "NA"
                // Hide the Reason text bar
                //ShippingMarksReasonView.isHidden = true
            default:
                break
            }
        }else if sender == PakagingOthersBtn{
            
            switch PakagingOthersBtn.selectedSegmentIndex {
            case 0:
                PakagingOthersID = "C"
                // Hide the Reason text bar
                //PakagingOthersReasonView.isHidden = true
            case 1:
                PakagingOthersID = "NC"
                // show the Reason text bar
                //PakagingOthersReasonView.isHidden = false
            case 2:
                PakagingOthersID = "NA"
                // Hide the Reason text bar
                //PakagingOthersReasonView.isHidden = true
            default:
                break
            }
        }
        
        // Garment conformity
        
        else if sender == ConstructionBtn{
            
            switch ConstructionBtn.selectedSegmentIndex {
            case 0:
                ConstructionID = "C"
                // Hide the Reason text bar
                //ConstructionReasonView.isHidden = true
            case 1:
                ConstructionID = "NC"
                // show the Reason text bar
                //ConstructionReasonView.isHidden = false
            case 2:
                ConstructionID = "NA"
                // Hide the Reason text bar
                //ConstructionReasonView.isHidden = true
            default:
                break
            }
        }else if sender == AppearanceBtn{
            
            switch AppearanceBtn.selectedSegmentIndex {
            case 0:
                AppearanceID = "C"
                // Hide the Reason text bar
                //AppearanceReasonView.isHidden = true
            case 1:
                AppearanceID = "NC"
                // show the Reason text bar
                //AppearanceReasonView.isHidden = false
            case 2:
                AppearanceID = "NA"
                // Hide the Reason text bar
                //AppearanceReasonView.isHidden = true
            default:
                break
            }
        }else if sender == HandfeelBtn{
            
            switch HandfeelBtn.selectedSegmentIndex {
            case 0:
                HandfeelID = "C"
                // Hide the Reason text bar
                //HandfeelReasonView.isHidden = true
            case 1:
                HandfeelID = "NC"
                // show the Reason text bar
                //HandfeelReasonView.isHidden = false
            case 2:
                HandfeelID = "NA"
                // Hide the Reason text bar
                //HandfeelReasonView.isHidden = true
            default:
                break
            }
        }else if sender == ThreadBtn{
            
            switch ThreadBtn.selectedSegmentIndex {
            case 0:
                ThreadID = "C"
                // Hide the Reason text bar
                //ThreadReasonView.isHidden = true
            case 1:
                ThreadID = "NC"
                // show the Reason text bar
                //ThreadReasonView.isHidden = false
            case 2:
                ThreadID = "NA"
                // Hide the Reason text bar
                //ThreadReasonView.isHidden = true
            default:
                break
            }
        }else if sender == StitchesBtn{
            
            switch StitchesBtn.selectedSegmentIndex {
            case 0:
                StitchesID = "C"
                // Hide the Reason text bar
                //StitchesReasonView.isHidden = true
            case 1:
                StitchesID = "NC"
                // show the Reason text bar
                //StitchesReasonView.isHidden = false
            case 2:
                StitchesID = "NA"
                // Hide the Reason text bar
                //StitchesReasonView.isHidden = true
            default:
                break
            }
        }else if sender == PrintBtn{
            
            switch PrintBtn.selectedSegmentIndex {
            case 0:
                PrintID = "C"
                // Hide the Reason text bar
                //PrintReasonView.isHidden = true
            case 1:
                PrintID = "NC"
                // show the Reason text bar
                //PrintReasonView.isHidden = false
            case 2:
                PrintID = "NA"
                // Hide the Reason text bar
                //PrintReasonView.isHidden = true
            default:
                break
            }
        }else if sender == ColorComboBtn{
            
            switch ColorComboBtn.selectedSegmentIndex {
            case 0:
                ColorComboID = "C"
                // Hide the Reason text bar
                //ColorComboReasonView.isHidden = true
            case 1:
                ColorComboID = "NC"
                // show the Reason text bar
                //ColorComboReasonView.isHidden = false
            case 2:
                ColorComboID = "NA"
                // Hide the Reason text bar
                //ColorComboReasonView.isHidden = true
            default:
                break
            }
        }else if sender == ComponentsBtn{
            
            switch ComponentsBtn.selectedSegmentIndex {
            case 0:
                ComponentsID = "C"
                // Hide the Reason text bar
                //ComponentsReasonView.isHidden = true
            case 1:
                ComponentsID = "NC"
                // show the Reason text bar
                //ComponentsReasonView.isHidden = false
            case 2:
                ComponentsID = "NA"
                // Hide the Reason text bar
                //ComponentsReasonView.isHidden = true
            default:
                break
            }
        }else if sender == TrimsBtn{
            
            switch TrimsBtn.selectedSegmentIndex {
            case 0:
                TrimsID = "C"
                // Hide the Reason text bar
                //TrimsReasonView.isHidden = true
            case 1:
                TrimsID = "NC"
                // show the Reason text bar
                //TrimsReasonView.isHidden = false
            case 2:
                TrimsID = "NA"
                // Hide the Reason text bar
                //TrimsReasonView.isHidden = true
            default:
                break
            }
        }else if sender == AccessoriesBtn{
            
            switch AccessoriesBtn.selectedSegmentIndex {
            case 0:
                AccessoriesID = "C"
                // Hide the Reason text bar
                //AccessoriesReasonView.isHidden = true
            case 1:
                AccessoriesID = "NC"
                // show the Reason text bar
                //AccessoriesReasonView.isHidden = false
            case 2:
                AccessoriesID = "NA"
                // Hide the Reason text bar
                //AccessoriesReasonView.isHidden = true
            default:
                break
            }
        }else if sender == GarmentConformityOthersBtn{
            
            switch GarmentConformityOthersBtn.selectedSegmentIndex {
            case 0:
                GarmentConformityOthersID = "C"
                // Hide the Reason text bar
                //GarmentConformityOthersReasonView.isHidden = true
            case 1:
                GarmentConformityOthersID = "NC"
                // show the Reason text bar
                //GarmentConformityOthersReasonView.isHidden = false
            case 2:
                GarmentConformityOthersID = "NA"
                // Hide the Reason text bar
                //GarmentConformityOthersReasonView.isHidden = true
            default:
                break
            }
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    
    
    @IBAction func packagingResultBtn_Pressed(_ sender: Any) {
        
        print("SelectPackagingAuditResult button is cliked.")
        
        let popupVC = UIStoryboard(name: "Main2", bundle: nil).instantiateViewController(withIdentifier: "PopUpVCHybrid") as! HybridPopUpViewControler
        
        popupVC.delegate = self
        
        popupVC.pickerdata = ["PASS","FAIL"]
        popupVC.pickerdataIDs = ["P","F"]
        
        popupVC.WhichButton = "PackagingAuditResult"
        
        popupVC.AllreadySelectedText = self.PackagingAuditResultBtn.titleLabel!.text!
        
        self.addChildViewController(popupVC)
        popupVC.view.frame = self.view.frame
        
        self.view.addSubview(popupVC.view)
        self.didMove(toParentViewController: self)
        
    }
    
    func savePackagingAuditResult(_ strText: String, strID: String) {
        print("Packaging Audit Result =",strText)
        print("Packaging Result ID =",strID)
        self.PackagingAuditResultBtn.setTitle(strText, for: UIControlState())
        self.PackagingAuditResult = strID
    }
    
    @IBAction func garmentConformityResultBtn_Pressed(_ sender: Any) {
        
        print("SelectGarmentConformityAuditResult button is cliked.")
        
        let popupVC = UIStoryboard(name: "Main2", bundle: nil).instantiateViewController(withIdentifier: "PopUpVCHybrid") as! HybridPopUpViewControler
        
        popupVC.delegate = self
        
        popupVC.pickerdata = ["PASS","FAIL"]
        popupVC.pickerdataIDs = ["P","F"]
        
        popupVC.WhichButton = "GarmentAuditResult"
        
        popupVC.AllreadySelectedText = self.GarmentConformityAuditResultBtn.titleLabel!.text!
        
        self.addChildViewController(popupVC)
        popupVC.view.frame = self.view.frame
        
        self.view.addSubview(popupVC.view)
        self.didMove(toParentViewController: self)
        

        
    }
    
    func saveGarmentConformityAuditResult(_ strText: String, strID: String) {
        print("GarmentConformity Audit Result =",strText)
        print("GarmentConformity Result ID =",strID)
        self.GarmentConformityAuditResultBtn.setTitle(strText, for: UIControlState())
        self.GarmentConformityAuditResult = strID
    }

    
    var bodyData = ""
    
    @IBAction func saveAndContinue(_ sender: Any) {
        do{
            let jsonCartonNumbers = try JSONSerialization.data(withJSONObject: self.CartonNumbers!, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            //        let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
            print(jsonCartonNumbers)
            let cartonString = self.CartonNumbers.joined(separator: ",")

            
            bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCodeIn!)&CartonNos=\(cartonString)&MeasurementSampleSize=\(self.MeasurementSampleSizeIn!)&OutOfTolerance=\(self.OutOfToleranceIn!)&Pom=\(self.PomIn!)&MeasurementResult=\(self.MeasurementResultIn!)&Assortment=\(self.AssortmentID!)&AssortmentRemarks=\(self.AssortmentReason.text!)&MainLabel=\(self.MainLabelID!)&MainLabelRemarks=\(self.MainLabelReason.text!)&CareLabel=\(self.CareLabelID!)&CareLabelRemarks=\(self.CareLabelReason.text!)&Hangtag=\(self.HangtagID!)&HangtagRemarks=\(self.HangtagReason.text!)&Polybag=\(self.PolybagID!)&PolybagRemarks=\(self.PolybagReason.text!)&Sticker=\(self.StickerID!)&StickerRemarks=\(self.StickerReason.text!)&Price=\(self.PriceID!)&PriceRemarks=\(self.PriceReason.text!)&Hanger=\(self.HangerID!)&HangerRemarks=\(self.HangerReason.text!)&CartonDimension=\(self.CartonDimensionID!)&CartonDimensionRemarks=\(self.CartonDimensionReason.text!)&ShippingMarks=\(self.ShippingMarksID!)&ShippingMarksRemarks=\(self.ShippingMarksReason.text!)&PackingOthers=\(self.PakagingOthersID!)&PackingOthersRemarks=\(self.PakagingOthersReason.text!)&PackingResult=\(self.PackagingAuditResult!)&Construction=\(self.ConstructionID!)&ConstructionRemarks=\(self.ConstructionReason.text!)&Appearance=\(self.AppearanceID!)&AppearanceRemarks=\(self.AppearanceReason.text!)&Handfeel=\(self.HandfeelID!)&HandfeelRemarks=\(self.HandfeelReason.text!)&Thread=\(self.ThreadID!)&ThreadRemarks=\(self.ThreadReason.text!)&Stitches=\(self.StitchesID!)&StitchesRemarks=\(self.StitchesReason.text!)&Print=\(self.PrintID!)&PrintRemarks=\(self.PrintReason.text!)&ColorCombo=\(self.ColorComboID!)&ColorComboRemarks=\(self.ColorComboReason.text!)&Components=\(self.ComponentsID!)&ComponentsRemarks=\(self.ComponentsReason.text!)&Trims=\(self.TrimsID!)&TrimsRemarks=\(self.TrimsReason.text!)&Accessories=\(self.AccessoriesID!)&AccessoriesRemarks=\(self.AccessoriesReason.text!)&GarmentOthers=\(self.GarmentConformityOthersID!)&GarmentOthersRemarks=\(self.GarmentConformityOthersReason.text!)&GarmentResult=\(self.GarmentConformityAuditResult!)"
            
            print("Packaging_bodyData = ",bodyData)
            
            performLoadDataRequestWithURL(bodyDataIn: bodyData)
            
        }catch{
            print("cannot prepare JsonCarton Numbers")
        }
    }
    
    
    
    var task:URLSessionDataTask?
    
    func performLoadDataRequestWithURL(bodyDataIn:String) -> String? {
        
        //        let jsonData = try! JSONSerialization.data(withJSONObject: TAG_CAPsArray, options: JSONSerialization.WritingOptions())
        //        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as! String
        //        print("jsonString =",jsonString)
        
        
//        let bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCodeIn)"
//        
//        print("bodyData =",bodyData)
        
        let request: NSMutableURLRequest = NSMutableURLRequest(url: self.getUrl())
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = bodyDataIn.data(using: String.Encoding.utf8)
        let session = URLSession.shared
        
        task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            print("response =",response)
            
            if (error != nil) {
                print(error)
                
                
                let alertView = UIAlertController(title: nil , message: "Cannot save this Stage \n try saving again.", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                })
                alertView.addAction(ok_action)
                
                self.present(alertView, animated: true, completion: nil)
                
                
            }
            else if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                
                print(json)
                print("Packaging status is OK")
                
                self.performSegue(withIdentifier: "gotoHybridCommentVC", sender: nil)
                

                //                if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                //                    self.present(resultController, animated: true, completion: nil)
                //                }
                
            }//if data != nil
            else{
                print("return data is nil")
            }
            
        })
        
        task?.resume()
        return ""
    }
    
    
    func getUrl() -> URL {
        let toEscape = "http://app.3-tree.com/quonda/save-hybrid-apparel.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
    
    
    
    
    
//    
//    http://portal.3-tree.com/api/android/quonda/save-hybrid-apparel.php
//    
//    Parameters:
//    User
//    AuditCode
//    CartonNos (Json Array of Cartons Nos only … example .. [1, 2, 3, 4, 5] )
//    
//    MeasurementSampleSize
//    OutOfTolerance
//    Pom
//    MeasurementResult (P o r F)
//
//    Assortment  (C or NC or NA)
//    AssortmentRemarks
//    MainLabel
//    MainLabelRemarks
//    CareLabel
//    CareLabelRemarks
//    Hangtag
//    HangtagRemarks
//    Polybag
//    PolybagRemarks
//    Sticker
//    StickerRemarks
//    Price
//    PriceRemarks
//    Hanger
//    HangerRemarks
//    CartonDimension
//    CartonDimensionRemarks
//    ShippingMarks
//    ShippingMarksRemarks
//    PackingOthers
//    PackingOthersRemarks
//    PackingResult (P or F)
//
//    Construction
//    ConstructionRemarks
//    Appearance
//    AppearanceRemarks
//    Handfeel
//    HandfeelRemarks
//    Thread
//    ThreadRemarks
//    Stitches
//    StitchesRemarks
//    Print
//    PrintRemarks
//    ColorCombo
//    ColorComboRemarks
//    Components
//    ComponentsRemarks
//    Trims
//    TrimsRemarks
//    Accessories
//    AccessoriesRemarks
//    GarmentOthers
//    GarmentOthersRemarks
//    GarmentResult (P or  F)
    
    
//    Commnets screen Additional Fields w.r.t Hybrid Report
//    http://portal.3-tree.com/api/android/quonda/save-comments.php
//    
//    Additional Parameters:
//    ShipQty
//    ShipmentDate (Format: 0000-00-00)
//    AssortmentCartonQty
//    AssortmentSizeQty
//    SolidSizeQty
//    SolidSizeType
//    WorkmanshipResult  (P o r F)
//    
    
    
//    @IBAction func AssortmentBtn_Pressed(_ sender: Any) {
//        
//        print("Select Assortment Result button is cliked.")
//        
//        let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVCHybrid") as! HybridPopUpViewControler
//        
//        popupVC.delegate = self
//        
//        popupVC.pickerdata = ["Conformed","NotConformed","N/A"]
//        popupVC.pickerdataIDs = ["A","B","C"]
//        
//        popupVC.WhichButton = "Assortment"
//        
//        popupVC.AllreadySelectedText = self.AssortmentBtn.titleLabel!.text!
//        
//        self.addChildViewController(popupVC)
//        popupVC.view.frame = self.view.frame
//        
//        self.view.addSubview(popupVC.view)
//        self.didMove(toParentViewController: self)
//
//    }
//    
//    func saveAssortment(_ strText: String, strID: String) {
//        self.AssortmentBtn.titleLabel?.text = strText
//        print("strID =",strID)
//        
//        if strText == "NotConformed"{
//            
//            // show the Reason text bar
//            AssortmentView.isHidden = false
//        }else{
//            // Hide the Reason text bar
//            AssortmentView.isHidden = true
//        }
//    }
    
    
    
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
            defaults.removeObject(forKey: "userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
            //        case 4:
            //            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
            //                //                self.dismissViewControllerAnimated(true, completion: nil)
            //                presentViewController(resultController, animated: true, completion: nil)
            //            }
            //
            //            let selectedImage:UIImage = UIImage(named: "Selected QAReport")!
            //            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            //
        //            break
        case 5:
            DefectsData0.removeAll()
            DefectsRateData0.removeAll()
            DefectsAreaData0.removeAll()
            
            self.dismiss(animated: true, completion: nil)
            //            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("5") as? ManagerModeViewController {
            //                self.dismissViewControllerAnimated(true, completion: nil)
            //                presentViewController(resultController, animated: true, completion: nil)
            //            }
            //            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            //            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        default:
            break
        }
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "gotoHybridCommentVC"{
           
            let destinationNVC = segue.destination as! UINavigationController
            let destinationVC = destinationNVC.viewControllers.first as! HybridCommentViewController

//            let destinationVC = segue.destination as! HybridCommentViewController
            destinationVC.AuditCodein = self.AuditCodeLabel.text!
            destinationVC.AuditStagein = self.AuditStageLabel.text!
            destinationVC.AuditStageColorin = self.AuditStageLabel.backgroundColor!
            destinationVC.ReportID_against_current_audit = "36"
        }
    }
 

}
















