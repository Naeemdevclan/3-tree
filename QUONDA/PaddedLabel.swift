//
//  PaddedLabel.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 29/03/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class PaddedLabel: UILabel {

    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: 0, left: 7, bottom: 0, right: 0)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
