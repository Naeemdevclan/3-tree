//
//  PakingTab.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 13/05/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class PakingTab: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate{

//    @IBOutlet weak var PakingTabItem: UITabBarItem!
    
    @IBOutlet weak var PakingCollectionView: UICollectionView!
    
    var numberOfPicturesInSection = 0
    var sketchs:[String] = []
    
    var dictionary:NSDictionary!
    
    
    var jsonFilePath2:URL!
    var created:Bool!
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

//        PakingCollectionView.dataSource = self
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("keyStats.json")
        
        do {
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath.path, encoding: String.Encoding.utf8.rawValue) as String
           // print(readString)
            self.showDataWhenOffline(readString)
        } catch let error as NSError {
            print(error.description)
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfPicturesInSection //10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PackingTabCell//UICollectionViewCell
        
//        cell.imageView.image = self.sketchs[indexPath.row]
       
        if let url = URL(string: self.sketchs[indexPath.row]){
            
            if let data = try? Data(contentsOf: url) {
                
                cell.imageView.image = UIImage(data: data)
            }
        }
        
        
      //  print("original picture string =\(self.sketchs[indexPath.row])")
        
        let aString: String = self.sketchs[indexPath.row] //"This is picture string without /thumbs/"
        let LPictureString = aString.replacingOccurrences(of: "/thumbs/", with: "/")
       // print("newString = \(LPictureString)")
        
        cell.LargePictureString.text = LPictureString
       // print("LPictureString = \(cell.LargePictureString.text!)")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! PackingTabCell
        
        let vc = storyboard!.instantiateViewController(withIdentifier: "showZoomedPicture") as! PicturePreview
        vc.imageURL = cell.LargePictureString.text
//        vc.photo = cell.imageView.image
//        print("vc.photo = \(cell.imageView.image)")
        self.present(vc, animated: false, completion: nil)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showDataWhenOffline(_ savedData:String?){
        
        if savedData != nil {
            
            self.dictionary = self.parseJSON(savedData!)
           // print("\(self.dictionary)")
            
            let PackingArray = self.dictionary["Packing"]!
//            print("PackingArray=")
//            print(PackingArray)
//            print((PackingArray as AnyObject).count)
            
            if (PackingArray as AnyObject).count > 0{
                
                for i in 0  ..< (PackingArray as AnyObject).count {
                    print(i)
                    print((PackingArray as AnyObject).object(at: i))
                    sketchs.append((PackingArray as AnyObject).object(at: i) as! String)
                }
                
                self.numberOfPicturesInSection = (PackingArray as AnyObject).count
               
                PakingCollectionView.dataSource = self
                PakingCollectionView.delegate = self
            }
            else{
                //If PackingArray.count < 1, Then show nil data found message on the Screen
                
                noDataFound()
            }
            
            print(" - - - - - - - ")
            
        }
    }
    
    func noDataFound(){
        
        let alert:UIAlertController = UIAlertController(title: nil, message: "No image found in Paking!", preferredStyle: UIAlertControllerStyle.alert)
        self.present(alert, animated: true, completion: nil)
        
        let duration:UInt64 = 2; // duration in seconds
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(duration*NSEC_PER_SEC)) / Double(NSEC_PER_SEC)) { () -> Void in
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
                
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
