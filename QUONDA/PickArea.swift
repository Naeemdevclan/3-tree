//
//  PickArea.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 20/04/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
var AreaData = [IndexPath:String]()
var AreaDataID = [IndexPath:String]()

var pTappedAreaButtonIndexPath:IndexPath?
var pAreaButtonTag:Int!


class PickArea: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBAction func backBtnPressed(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var AreaTable: UITableView!
    //    var data = [Int:String]()
    var dictionary:NSDictionary!
    var jsonFilePath2:URL!
    var created:Bool!
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    
    var ReportId: String!
    var DefectID_Received: String!

    var cell:UITableViewCell!
    //    var pressed:Bool = false
    var selectedButton:NSMutableArray! = NSMutableArray()
    //    var pTappedAreaButtonIndexPath:NSIndexPath?
    //    var pAreaButtonTag:Int!
    var items: [String] = []//["We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift"]
    var itemsID: [String] = []
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = "Select Defect Area"
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("login.json")
        jsonLoginFilePath = jsonFilePath
        print(jsonLoginFilePath.path)
        
        do {
            var readString: String
            readString = try NSString(contentsOfFile: jsonLoginFilePath.path , encoding: String.Encoding.utf8.rawValue) as String
            print(readString)
            self.showDataWhenOffline(readString)
        } catch let error as NSError {
            print(error.description)
        }
        //        jsonLoginFilePath
        
        for i in 0 ..< (items.count) //yourTableSize = how many rows u got
        {
            selectedButton.add("NO")
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        //        for (var i = 0; i<items.count; i++) //yourTableSize = how many rows u got
        //        {
        //            selectedButton.addObject("NO")
        //        }
        if !DefectsAreaData0.isEmpty {
            for d2 in DefectsAreaData0{
                selectedButton.replaceObject(at: d2.0.row, with: "YES")
                
                if let alreadyCheckedCell = self.AreaTable.cellForRow(at: d2.0 as IndexPath){
                    AreaData[d2.0 as IndexPath] = alreadyCheckedCell.textLabel!.text
                    AreaDataID[d2.0 as IndexPath] = alreadyCheckedCell.detailTextLabel?.text
                }
            }
//            DefectsAreaData0.removeAll()
            self.AreaTable.reloadData()
        }
    }
    //    func checkBoxTapped(sender: AnyObject) {
    //
    //        let button = sender as! UIButton
    //        let view = button.superview!
    //        self.cell = view.superview as! UITableViewCell
    //        let indexPath = AreaTable.indexPathForCell(cell)
    //
    //        var x = button.tag - 100
    //
    ////        if button.selected == true{
    ////            selectedButton.replaceObjectAtIndex(x, withObject: "NO")
    ////            button.selected = false
    ////            AreaData.removeValueForKey(indexPath!)
    ////
    ////        }
    ////        else if button.selected == false{
    ////            selectedButton.replaceObjectAtIndex(x, withObject: "YES")
    ////            button.selected = true
    ////            AreaData[indexPath!] = cell.checkLabel.text
    ////        }
    //
    //        if pTappedAreaButtonIndexPath == indexPath{
    //            selectedButton.replaceObjectAtIndex(x, withObject: "NO")
    //            button.selected = false
    //            AreaData.removeValueForKey(indexPath!)
    //            pTappedAreaButtonIndexPath = nil
    //            pAreaButtonTag = nil
    //        }
    //        else if pTappedAreaButtonIndexPath != indexPath{
    //            if (pTappedAreaButtonIndexPath != nil && pAreaButtonTag != nil){
    //                let Pcell:UITableViewCell = self.AreaTable.cellForRowAtIndexPath(pTappedAreaButtonIndexPath!)! as! UITableViewCell
    //                selectedButton.replaceObjectAtIndex(pAreaButtonTag, withObject: "NO")
    //               let previousButtonState =  Pcell.contentView.subviews.last//button.selected = false
    //                print((previousButtonState?.tag)!-100)
    //                previousButtonState?.setValue(false, forKey: "selected")
    //                print(previousButtonState?.valueForKey("selected"))
    //                AreaData.removeValueForKey(pTappedAreaButtonIndexPath!)
    //            }
    //            pTappedAreaButtonIndexPath = indexPath
    //            selectedButton.replaceObjectAtIndex(x, withObject: "YES")
    //            button.selected = true
    //            AreaData[indexPath!] = cell.textLabel!.text
    //            pAreaButtonTag = x
    //        }
    //    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //return (self.dictionary["DefectAreas"]! as AnyObject).count
        return items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let Ccell = self.AreaTable.dequeueReusableCell(withIdentifier: "ccell")!
        //        var checkBox:UIButton = UIButton(frame: CGRect(x: Ccell.frame.width, y: Ccell.frame.height/4, width: 30, height: 30))
        Ccell.textLabel!.text = items[indexPath.row]
        Ccell.detailTextLabel!.text = itemsID[indexPath.row]
        Ccell.detailTextLabel?.isHidden = true
        
        Ccell.textLabel!.font = UIFont(name: Ccell.textLabel!.font.fontName, size: 13)
        
        //   Ccell.checkBox.layer.cornerRadius = Ccell.checkBox.frame.height/2
        //        checkBox.setBackgroundImage(UIImage(named: "Radio-OFF"), forState: .Normal)
        //        checkBox.setBackgroundImage(UIImage(named: "Radio-ON"), forState: .Selected)
        
        //        checkBox.tag = indexPath.row+100
        //        checkBox.addTarget(self, action: "checkBoxTapped:", forControlEvents: UIControlEvents.TouchUpInside)
        
        //        if selectedButton.objectAtIndex(indexPath.row).isEqualToString("NO"){
        //            checkBox.selected = false
        //        }else{
        //            checkBox.selected = true
        //        }
        if (selectedButton.object(at: indexPath.row) as AnyObject).isEqual(to: "NO"){
            Ccell.accessoryType = .none
            Ccell.backgroundColor = UIColor.white
        }else {
            Ccell.accessoryType = .checkmark
            Ccell.backgroundColor = UIColor.lightGray
        }
        //        Ccell.contentView.addSubview(checkBox)
        
        return Ccell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("selected Index row path\(indexPath.row)")
        
        //        let button = sender as! UIButton
        //        let view = button.superview!
        //        self.cell = view.superview as! SearchStageCell
        //        let indexPath = searchTable.indexPathForCell(cell)
        
        let x = indexPath.row//button.tag - 100
        let cellPressed = tableView.cellForRow(at: indexPath)! 
        
        // - - - - - - - - -
        //
        //        if cellPressed.accessoryType == .None{
        //            cellPressed.accessoryType = .Checkmark
        //            selectedButton.replaceObjectAtIndex(x, withObject: "YES")
        //            data[indexPath] = cellPressed.textLabel!.text
        //
        //        }else if cellPressed.accessoryType == .Checkmark{
        //            cellPressed.accessoryType = .None
        //            selectedButton.replaceObjectAtIndex(x, withObject: "NO")
        //            data.removeValueForKey(indexPath)
        //        }
        
        
        
        
        print(pTappedAreaButtonIndexPath?.row)
//        if pTappedAreaButtonIndexPath == indexPath{
//            //            selectedButton.replaceObjectAtIndex(x, withObject: "NO")
//            ////            button.selected = false
//            //            cellPressed.accessoryType = .None
//            //            AreaData.removeValueForKey(indexPath)
//            //
//            //            pTappedAreaButtonIndexPath = nil
//            //            pAreaButtonTag = nil
//        }
//        else 
        if pTappedAreaButtonIndexPath != indexPath || pTappedAreaButtonIndexPath == indexPath{
            if (pTappedAreaButtonIndexPath != nil && pAreaButtonTag != nil){
//                if let Pcell = self.AreaTable.cellForRowAtIndexPath(pTappedAreaButtonIndexPath!)! as? UITableViewCell{
                    selectedButton.replaceObject(at: pAreaButtonTag, with: "NO")
                    //                let previousButtonState =  Pcell.contentView.subviews.last//button.selected = false
                    //                print((previousButtonState?.tag)!-100)
                    //                previousButtonState?.setValue(false, forKey: "selected")
                    //                print(previousButtonState?.valueForKey("selected"))
//                    Pcell.accessoryType = .None
                    AreaData.removeValue(forKey: pTappedAreaButtonIndexPath!)
                    AreaDataID.removeValue(forKey: pTappedAreaButtonIndexPath!)
//                }
            }
            pTappedAreaButtonIndexPath = indexPath
            selectedButton.replaceObject(at: x, with: "YES")
            //            button.selected = true
            cellPressed.accessoryType = .checkmark
            AreaData[indexPath] = cellPressed.textLabel!.text
            AreaDataID[indexPath] = cellPressed.detailTextLabel?.text
            
            pAreaButtonTag = x
            
            //            DefectsAreaData0 = AreaData
            //            self.dismissViewControllerAnimated(true, completion: nil)
        }
        
        // - - Dismiss the current view - - - - - - -
        DefectsAreaData0 = AreaData as [IndexPath : String]
        self.dismiss(animated: false, completion: nil)
        self.AreaTable.beginUpdates()
        self.AreaTable.endUpdates()
    }
    
    //    func sortCountries(obj1:[String:AnyObject], obj2:[String:AnyObject]){
    //
    //    var sorted_DATA = unsortedCountries.sortedArrayUsingComparator({obj1, obj2 -> NSComparisonResult in
    //
    //    let rstrnt1 = obj1
    //    let rstrnt2 = obj2
    //    if (rstrnt1 as! String) < (rstrnt2 as! String) {
    //    return NSComparisonResult.OrderedAscending
    //    }
    //    if (rstrnt1 as! String) > (rstrnt2 as! String) {
    //    return NSComparisonResult.OrderedDescending
    //    }
    //    return NSComparisonResult.OrderedSame
    //    })
    //    }
    func before(_ value1: String, value2: String) -> Bool {
        // One string is alphabetically first.
        // ... True means value1 precedes value2.
        return value1 < value2;
    }
    
    func showDataWhenOffline(_ savedData:String?){
        
        if savedData != nil {
            
            self.dictionary = self.parseJSON(savedData!)
            print("\(self.dictionary)")
            
            let All_Countries = self.dictionary["DefectAreas"]!  as! [AnyObject]

           // let All_Countries = self.dictionary["DefectAreas"]!  as! [String:AnyObject]
            print(All_Countries)
            
            //            let d = (self.dictionary as NSDictionary).keysSortedByValueUsingSelector("compare:")
            //            print(d)
            //            //            var sortedCountries:[String:AnyObject]?
            
            //            let unsortedCountries = NSArray(objects: All_Countries)
            //            let unsortedC = (All_Countries as! NSArray).sortedArrayUsingDescriptors([NSSortDescriptor(key: "jjklj", ascending: true)]) as! [String:AnyObject]
            //            var a = unsortedCountries.sort({ (item1, item2) -> Bool in
            //             return true
            //            })
            //            var ij = unsortedCountries.sort({ (a, b) -> Bool in
            //                print(a)
            //                print(b)
            ////                if a < b {
            ////                    return true
            ////                }
            //                return false
            //            })
            //            let animals = ["bird": 0, "zebra": 9, "ant": 1]
            var myArray = [String : AnyObject]()
            //
            for countryObj in All_Countries{
                let reports = countryObj["reports"] as! String
                let reportsArr = reports.components(separatedBy: ",")
                if(reportsArr.contains(ReportId!)){
                    
                
                myArray[countryObj["name"] as! String] = countryObj["id"] as AnyObject?
                }
            }
            print(myArray)
            var mynewDICT:[Int:[String:AnyObject]]! = [:]
            //            var mynewDICT: [String:AnyObject]! = [:]
            // Create a dictionary.
//            let animals = ["bird": 0, "zebra": 9, "ant": 1]
            
            // Get the array from the keys property.
            let copy = myArray.keys
            // Sort from low to high (alphabetical order).
            let p = copy.sorted(by: <)
            print(p)
            
            for pi in p{
                print(pi)
            }
            let s = myArray.keys.sorted()
            print(s)
            // Loop over sorted keys.
            var i = 0
            for pin in p{
                for key in copy {
                    if key == pin{
                        // Get value for this key.
                        if let value = myArray[key] {
                            //                            mynewDICT[value as! String] = key
                            mynewDICT[i] = [value as! String : key as AnyObject]
                            self.items.append(key)
                            self.itemsID.append(value as! String)
                            
                            print("Key = \(key), Value = \(value)")
                            i+=1
                        }
                    }
                }}
            print(mynewDICT)
            //            var ix = 0
            //            for var ix = 0; ix < mynewDICT.count; ix++ {
            //                print(mynewDICT[ix])
            //            }
            //            while(ix < mynewDICT.count){
            //            print(mynewDICT[ix])
            //                ix+=1
            //            }
            
            // Get the array from the keys property.
            //            for countryObj in All_Countries{
            //
            //                var sorted_DATA = unsortedCountries.sortedArrayUsingComparator({countryObj, countryObj -> NSComparisonResult in
            //
            //                    let rstrnt1 = countryObj
            //                    let rstrnt2 = countryObj
            //                    if (rstrnt1 as! String) < (rstrnt2 as! String) {
            //                        return NSComparisonResult.OrderedAscending
            //                    }
            //                    if (rstrnt1 as! String) > (rstrnt2 as! String) {
            //                        return NSComparisonResult.OrderedDescending
            //                    }
            //                    return NSComparisonResult.OrderedSame
            //                })
            //            }
            //            print(unsortedCountries)
            
            //            print(sorted_DATA)
            //            if let unsortedCountries = self.dictionary["Countries"] as? NSArray{
            //                let descriptor = NSSortDescriptor(key: "", ascending: true)//(key: "name", ascending: true, selector: "caseInsensitiveCompare:")
            //                sortedCountries = unsortedCountries.sortedArrayUsingDescriptors([descriptor])
            ////            }
            //
            //            var sortedCountries =  All_Countries.sort({ (b:(String, AnyObject), a: (String, AnyObject)) -> Bool in
            //                print(a)
            //                print(b)
            //
            //
            //            })//sort { (left, right) -> Bool in
            //                let first = left.0//left["rstrnt_name"] as? String
            //                let second = right.0//right["rstrnt_name"] as? String
            
            //                return first < second
            //                if first<second{
            //                    return true
            //                }else{
            //                    return false
            //                }
            //                switch (first, second) {
            //                case  x : return x < y
            //                default: return false
            //                }
            //            }
            //            print(sortedCountries)
            //            print(All_Countries)
            //            var arr = Array(All_Countries)
            //            for (k,v) in (Array(All_Countries).sort {$0.1 as! String < $1.1 }) {
            //                print("\(k):\(v)")
            //            }
            print("Total Countries = \(All_Countries.count)")
            
//            for country in All_Countries{
//                //
//                //                print(country)
//                //                self.items.append(country.1 as! String)
//                //                self.itemsID.append(country.0)
//            }
            //            var sorted = self.items.sort() // it sort by id's
            //            print(sorted)
            //           var ii = self.items.sort({ (a, b) -> Bool in
            //            print(a)
            //            print(b)
            //            if a < b {
            //                return true
            //            }
            //            return false
            //
            //            })
            //            print(ii)
            //            country.sort(){$0 < $1}
        }
    }
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
                
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
}
/*
// MARK: - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
// Get the new view controller using segue.destinationViewController.
// Pass the selected object to the new view controller.
}
*/
}


