//
//  PopUpVendorsViewController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 16/11/2017.
//  Copyright © 2017 Musawar. All rights reserved.
//

import UIKit
import UIKit


protocol SavingVendorViewControllerDelegate
{
    func saveVendorSelected(_ strText : String, strID : String)
    
    
    
}
class PopUpVendorsViewController: UIViewController , UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate {
    @IBOutlet weak var tableViewTpConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var navigationBar: UINavigationBar!
    var pickerdata = [String]()
    var pickerdataIDs = [String]()
    
    var tableCellData = [String]()
    var tableCellDataIDs = [String]()
    
    var WhichButton = String()
    
    @IBOutlet weak var tableView: UITableView!
    
    var delegate : SavingVendorViewControllerDelegate?
    //
    
    var PopUpselectedText = ""
    var PopUpselectedID = ""
    
    var AllreadySelectedText = String()
    var selectedRowText = String()
    
    var selectedButton:NSMutableArray! = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       // tableViewTpConstraint.constant = 16

        if(pickerdata.count < 11){
           tableViewTpConstraint.constant = 16
            txtSearch.isHidden = true

        }else{
            tableViewTpConstraint.constant = 56
            txtSearch.isHidden = false
        }
        // Do any additional setup after loading the view.
        navigationBar.topItem?.title = WhichButton
        if !AllreadySelectedText.isEmpty{
            
            selectedRowText = AllreadySelectedText
        }
        
        
        
        self.title = WhichButton
        
        print(WhichButton)
        print("selectedRowText =",selectedRowText)
        showAnimate()
        
        //        self.view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        
        if !pickerdata.isEmpty {
            tableCellData = pickerdata
            
        }
        
        if !pickerdataIDs.isEmpty{
            tableCellDataIDs = pickerdataIDs
            
        }
        else{
            tableCellDataIDs = tableCellData
        }
        sortcellData = tableCellData
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func CancelPressed(_ sender: AnyObject) {
        
        UIView.animate(withDuration: 0.17, animations: {
            self.view.transform = CGAffineTransform(scaleX: 0.2, y: 0.2) // onstart it will make self.view from Normal to Smaller
            self.view.alpha = 0.0 // This alpha value which is zero, means white color
            
        }, completion: { (finished) in
            if finished {
                self.view.removeFromSuperview()
            }
        })
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableCellData.count  //10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        
        cell.textLabel!.text = tableCellData[indexPath.row]
        cell.detailTextLabel!.text = tableCellDataIDs[indexPath.row]
        
        cell.detailTextLabel?.isHidden = true
        
        // Adding checkmarks to the rows
        
        if selectedRowText == cell.textLabel!.text! {
            cell.accessoryType = .checkmark
            
        }else {
            cell.accessoryType = .none
            
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("cell Tapped.")
        
        PopUpselectedText = tableView.cellForRow(at: indexPath)!.textLabel!.text!
        PopUpselectedID = tableView.cellForRow(at: indexPath)!.detailTextLabel!.text!
        
        
         if WhichButton == "Select Vendors Name"{
            
            if((self.delegate) != nil)
            {
                delegate?.saveVendorSelected(PopUpselectedText, strID: PopUpselectedID) //("Report Here")
            }
            
        }
        
        
        
        // adding Checkmark to the selected row
        
        let cellPressed = tableView.cellForRow(at: indexPath)!
        cellPressed.accessoryType = .checkmark
        
        
        //      // - - Dismiss the current view - - - - - - -
        
        removeAnimate()
        
        
        //        self.view.removeFromSuperview()
    }
    
    func showAnimate() -> Void {
        self.view.transform = CGAffineTransform(scaleX: 0.5, y: 0.5) // onstart it will make self.view  from smaller to a little bigger
        self.view.alpha = 0.0 // This alpha value which is zero, means white color
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0) // Now when animation Ended it will make self.view to normal
        })
    }
    
    func removeAnimate() -> Void {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 0.5, y: 0.5) // onstart it will make self.view a little big than normal
            self.view.alpha = 0.0 // This alpha value which is zero, means white color
            
        }, completion: { (finished) in
            if finished {
                //                    self.dismissViewControllerAnimated(true, completion: nil)
                self.view.removeFromSuperview()
            }
        }) 
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
      return  true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        tableCellData = pickerdata
        tableCellDataIDs = pickerdataIDs
        tableView.reloadData()
        print(textField.text!)

    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
//        cellData.removeAll()
//        cellDataIds.removeAll()
        print(textField.text!)

    }
    var cellData = [String]()
    var cellDataIds = [String]()
    var sortcellData = [String]()
    var selectedString = ""
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        cellData.removeAll()
        cellDataIds.removeAll()
        
//        if(selectedString == ""){
//            selectedString = string
//        }else{
        //selectedString += string
        var txtAfterUpdate:NSString = txtSearch.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
       // }
        print(txtAfterUpdate)
        for item in 0..<sortcellData.count{

        if((sortcellData[item].lowercased()).contains(txtAfterUpdate.lowercased)||txtAfterUpdate == ""){
        cellData.append(sortcellData[item])
        cellDataIds.append(pickerdataIDs[item])
            }
        }
        tableCellData = cellData
        tableCellDataIDs = cellDataIds
        tableView.reloadData()
        return true
    }
}
