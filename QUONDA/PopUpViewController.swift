//
//  PopUpViewController.swift
//  PopUp Selection
//
//  Created by Muhammad Naeem on 18/08/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//



import UIKit


protocol SavingViewControllerDelegate
{
    func saveText(_ strText : String, strID : String)
    
    func saveReportText(_ strText : String, strID : String)
    
    func saveBrandText(_ strText : String, strID : String)
    
    func saveVendorText(_ strText : String, strID : String)
    
    func saveUnitText(_ strText : String, strID : String)
    
    func saveStyleText(_ strText : String, strID : String)
    
    func saveSampleSizeText(_ strText : String, strID : String)
    
    func saveProductionLineText(_ strText : String, strID : String)
    
    func saveAuditTypeText(_ strText : String, strID : String)
    
    
    
    func saveAuditorsText(_ strText : String, strID : String)
    func saveVendorsText(_ strText : String, strID : String)
    
    func saveAuditResultText(_ strText : String, strID : String)
    
    func saveAQLText(_ strText : String, strID : String)
    func saveAQLData(_ strText : String, strID : String)
    func saveSelectedSize(_ strText : String, strID : String)
    func saveSelectedColor(_ strText : String, strID : String)



}



class PopUpViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var navigationBar: UINavigationBar!
    var pickerdata = [String]()
    var pickerdataIDs = [String]()
    
    var tableCellData = [String]()
    var tableCellDataIDs = [String]()
    
    var WhichButton = String()
    
    
    var delegate : SavingViewControllerDelegate?
//    
    
    var PopUpselectedText = ""
    var PopUpselectedID = ""
    
    var AllreadySelectedText = String()
    var selectedRowText = String()
    
    var selectedButton:NSMutableArray! = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationBar.topItem?.title = WhichButton
        if !AllreadySelectedText.isEmpty{
        
            selectedRowText = AllreadySelectedText
        }
        
        
       
            self.title = WhichButton
        
        print(WhichButton)
       print("selectedRowText =",selectedRowText)
        showAnimate()
        
//        self.view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
       
        if !pickerdata.isEmpty {
            tableCellData = pickerdata
            
        }
        
        if !pickerdataIDs.isEmpty{
            tableCellDataIDs = pickerdataIDs
            
        }
        else{
            tableCellDataIDs = tableCellData
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func CancelPressed(_ sender: AnyObject) {
        
        UIView.animate(withDuration: 0.17, animations: {
            self.view.transform = CGAffineTransform(scaleX: 0.2, y: 0.2) // onstart it will make self.view from Normal to Smaller
            self.view.alpha = 0.0 // This alpha value which is zero, means white color
            
        }, completion: { (finished) in
            if finished {
                self.view.removeFromSuperview()
            }
        }) 
 
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableCellData.count  //10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        
        cell.textLabel!.text = tableCellData[indexPath.row]
        cell.detailTextLabel!.text = tableCellDataIDs[indexPath.row]
        
        cell.detailTextLabel?.isHidden = true
       
        // Adding checkmarks to the rows
        
        if selectedRowText == cell.textLabel!.text! {
            cell.accessoryType = .checkmark

        }else {
            cell.accessoryType = .none

        }
        
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("cell Tapped.")
        
        PopUpselectedText = tableView.cellForRow(at: indexPath)!.textLabel!.text!
        PopUpselectedID = tableView.cellForRow(at: indexPath)!.detailTextLabel!.text!
        
       
        if WhichButton == "Audit Types" {
            
    
            if((self.delegate) != nil)
            {
                delegate?.saveAuditTypeText(PopUpselectedText, strID: PopUpselectedID) //("Report Here")
            }
            
        }
        else if WhichButton == "Stage" {
          
            if((self.delegate) != nil)
            {
                delegate?.saveText(PopUpselectedText, strID: PopUpselectedID)  //("stageHere")
            }
            
        }
        else if WhichButton == "ReportType" {
           
            if((self.delegate) != nil)
            {
                delegate?.saveReportText(PopUpselectedText, strID: PopUpselectedID) //("Report Here")
            }
            
            
        }else if WhichButton == "BrandName"{
           
            if((self.delegate) != nil)
            {
                delegate?.saveBrandText(PopUpselectedText, strID: PopUpselectedID) //("Report Here")
            }
            
            
        }else if WhichButton == "VendorName"{
            
            if((self.delegate) != nil)
            {
                delegate?.saveVendorText(PopUpselectedText, strID: PopUpselectedID) //("Report Here")
            }
            
        }else if WhichButton == "UnitName"{
            
            if((self.delegate) != nil)
            {
                delegate?.saveUnitText(PopUpselectedText, strID: PopUpselectedID) //("Report Here")
            }
            
        }else if WhichButton == "StyleName"{
            
            if((self.delegate) != nil)
            {
                delegate?.saveStyleText(PopUpselectedText, strID: PopUpselectedID) //("Report Here")
            }
            
        }else if WhichButton == "SampleSize"{
            
            //print(PopUpselectedText)
            if((self.delegate) != nil)
            {
                delegate?.saveSampleSizeText(PopUpselectedText, strID: PopUpselectedID) //("Report Here")
            }
            
        }else if WhichButton == "ProductionLine"{
            
            if((self.delegate) != nil)
            {
                delegate?.saveProductionLineText(PopUpselectedText, strID: PopUpselectedID) //("Report Here")
            }
            
        }else if WhichButton == "Select Auditor"{
        
        if((self.delegate) != nil)
        {
            delegate?.saveAuditorsText(PopUpselectedText, strID: PopUpselectedID) //("Report Here")
        }
        
        }else if WhichButton == "Select Vendor"{
            
            if((self.delegate) != nil)
            {
                delegate?.saveVendorsText(PopUpselectedText, strID: PopUpselectedID) //("Report Here")
            }
            
        }else if WhichButton == "AuditResult"{
            
            if((self.delegate) != nil)
            {
                delegate?.saveAuditResultText(PopUpselectedText, strID: PopUpselectedID) 
            }
            
        }else if WhichButton == "AQLLevel"{
            
            if((self.delegate) != nil)
            {
                delegate?.saveAQLText(PopUpselectedText, strID: PopUpselectedID)
            }
            
        }else if WhichButton == "AQL"{
            
            if((self.delegate) != nil)
            {
                delegate?.saveAQLData(PopUpselectedText, strID: PopUpselectedID)
            }
            
        }else if WhichButton == "SelectSize"{
            
            if((self.delegate) != nil)
            {
                delegate?.saveSelectedSize(PopUpselectedText, strID: PopUpselectedID)
            }
            
        }else if WhichButton == "SelectColor"{
            
            if((self.delegate) != nil)
            {
                delegate?.saveSelectedColor(PopUpselectedText, strID: PopUpselectedID)
            }
            
        }


        

        // adding Checkmark to the selected row
        
        let cellPressed = tableView.cellForRow(at: indexPath)!
        cellPressed.accessoryType = .checkmark
        

//      // - - Dismiss the current view - - - - - - -
    
        removeAnimate()
        
        
//        self.view.removeFromSuperview()
    }
    
    func showAnimate() -> Void {
        self.view.transform = CGAffineTransform(scaleX: 0.5, y: 0.5) // onstart it will make self.view  from smaller to a little bigger
        self.view.alpha = 0.0 // This alpha value which is zero, means white color
        
        UIView.animate(withDuration: 0.25, animations: { 
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0) // Now when animation Ended it will make self.view to normal
        }) 
    }
    
    func removeAnimate() -> Void {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 0.5, y: 0.5) // onstart it will make self.view a little big than normal
            self.view.alpha = 0.0 // This alpha value which is zero, means white color
            
            }, completion: { (finished) in
                if finished {
//                    self.dismissViewControllerAnimated(true, completion: nil)
                    self.view.removeFromSuperview()
                }
        }) 
    }
    

}
