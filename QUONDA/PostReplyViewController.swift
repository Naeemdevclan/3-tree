//
//  PostReplyViewController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 24/04/2017.
//  Copyright © 2017 Muhammad Naeem. All rights reserved.
//

import UIKit
import FileBrowser
class PostReplyViewController: UIViewController,UITextViewDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var contentViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblsubject: UILabel!
    @IBOutlet weak var txtReplyMessage: UITextView!
    
    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var btnStatus: CheckBox!
    @IBOutlet weak var btnPostReply: UIButton!
    @IBOutlet weak var imgStatus: UIImageView!
    
    @IBOutlet weak var btnFile: UIButton!
    
    @IBOutlet weak var btnGallery: UIButton!
    var subject = ""
    var department = ""
    var ticketID = ""
    var priority = ""
    var isClosed = ""

    var messageFrame = UIView()
    var activityIndicator = UIActivityIndicatorView()
    var strLabel = UILabel()
    
    var files = [Any]()
    var capturePhoto: UIImage? = nil
    var imagePicker: UIImagePickerController!
    var showImageView = UIImageView.init()

    override func viewDidLoad() {
        super.viewDidLoad()
        lblDepartment.text = department
        lblsubject.text = subject
        if(priority == "high"){
            self.imgStatus.backgroundColor = UIColor.red
        }
        else if(priority == "medium"){
            self.imgStatus.backgroundColor = UIColor.orange
        }
        else{
            self.imgStatus.backgroundColor = UIColor.blue
        }
        if(isClosed == "N"){
            btnStatus.isChecked = false
            btnPostReply.isHidden = false
        }
        else{
            btnPostReply.isHidden = true
            btnStatus.isChecked = true
        }

        // Do any additional setup after loading the view.
        showImageView.frame = self.view.frame
        showImageView.isHidden = true
        showImageView.backgroundColor = UIColor.white
        self.view.addSubview(showImageView)
        let tapgestures = UITapGestureRecognizer.init(target: self, action: #selector(hideImage))
        tapgestures.delegate = self
        showImageView.isUserInteractionEnabled = true
        showImageView.addGestureRecognizer(tapgestures)
        let backGroundTap = UITapGestureRecognizer.init(target: self, action: #selector(hideTableView))
        backGroundTap.delegate = self
        backGroundTap.numberOfTapsRequired = 1
        self.contentView.addGestureRecognizer(backGroundTap)
    }
    func hideTableView(){
         self.view.endEditing(true)
    }
    func hideImage(){
        showImageView.isHidden = true
    }
    func sampleImageTapped(){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        
        present(imagePicker, animated: true, completion: nil)
    }
    func samplePhotoLibImageTapped(){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        imagePicker.dismiss(animated: true, completion: nil)
        
        DispatchQueue.main.async { () -> Void in
            self.capturePhoto = info[UIImagePickerControllerOriginalImage] as? UIImage
            let imageData:NSData = UIImageJPEGRepresentation(self.capturePhoto!,0.4)! as NSData
            let ImageString = imageData.base64EncodedString(options: .endLineWithLineFeed)
        let randomString = self.randomStringWithLength(6)

            print(randomString)
            print((randomString as String) + ".jpg")

            let fileItem = ["\(randomString).jpg":ImageString]
            self.files.append(fileItem)
            self.attachements()
            
        }
    }
    func randomStringWithLength(_ len: Int) -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for _ in 0 ..< len {
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString
    }

    @IBAction func btnbackAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func fileAction(_ sender: UIButton) {
        let fileBrowser = FileBrowser()
        present(fileBrowser, animated: true, completion: nil)
        fileBrowser.didSelectFile = { (file: FBFile) -> Void in
            let urlString: String = file.filePath.path
            print(urlString)
            // etc.
            do {
                let data = try NSData(contentsOfFile: urlString, options: NSData.ReadingOptions.mappedIfSafe)
                let fileString = data.base64EncodedString(options: .endLineWithLineFeed)
                let fileItem = [file.displayName :fileString]
                print(fileItem)
                self.files.append(fileItem)
                self.attachements()
            }
            catch let error {
                print(error.localizedDescription)
            }
        }
    }
    @IBAction func galleryAction(_ sender: UIButton) {
        samplePhotoLibImageTapped()
    }
    @IBAction func replyAction(_ sender: UIButton) {
        _ = performLoginRequestWithURL(getUrl())
    }
    //MARK:- TOUCHES BEGAN
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK:- TEXTVIEW DELEGATES
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Write a message here..." || textView.text.replacingOccurrences(of: " ", with: "") == "" {
            txtReplyMessage.text = ""
            txtReplyMessage.textColor = .black
        }

//        txtReplyMessage.text = ""
//        txtReplyMessage.textColor = .black
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "Write a message here..." || textView.text.replacingOccurrences(of: " ", with: "") == "" {
            txtReplyMessage.text = "Write a message here..."
            txtReplyMessage.textColor = .lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" || text == ""{
            textView.resignFirstResponder()
        }
        return true
    }
    // API call
    func getUrl() -> URL {
        let toEscape = "http://support.3-tree.com/app/reply-ticket.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
    var bodyData:String?
    func performLoginRequestWithURL(_ url: URL) -> String? {
        
        if(txtReplyMessage.text! == "" || txtReplyMessage.text! == "Write a message here..."){
            let alertView = UIAlertController(title: "Post Reply" , message: "Please write your message", preferredStyle: .alert)
            let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction)-> Void in
            })
            alertView.addAction(ok_action)
            self.present(alertView, animated: true, completion: nil)
            return ""
        }
        else{
            var str = "Files=\(self.files.count)"
            for item  in 0..<self.files.count{
                let it =  self.files[item] as! [String:String]
                for (key, value) in it {
                    str = str + "&" + "File" + "\(item)" + "Name" + "=" + key
                    str =  str + "&" + "File" + "\(item)" + "Data" + "=" + value
                }
            }
        bodyData = "UserEmail=\(defaults.object(forKey: "userEmail") as! String)&Ticket=\(ticketID)&Message=\(txtReplyMessage.text!)&Close=\(btnStatus.isChecked ? "Y" : "N")&\(str)"
            print(url,bodyData!)
            
            let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
            var json = ""
            request.httpMethod = "POST"
            request.httpBody = bodyData!.data(using: String.Encoding.utf8)
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main){
                response, data, error in
                print("\(response)")
                if data != nil {
                    json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    // print(json)
                    let dictionary = self.parseJSON(json)
                    print(dictionary)
                    //                            print("\(dictionary["countries"]!)")
                    //                            print("\(dictionary["Status"]!)")
                    if("\(dictionary["Status"]!)" == "ERROR")
                    {
                        self.activityIndicator.stopAnimating()
                        self.messageFrame.removeFromSuperview()
                        
                        //show popup on LOGIN FAIL
                        let alert = UIAlertController(title: "Alert", message: "\(dictionary["Message"]!)", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(UIAlertAction) -> Void in
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                    }else if dictionary.count < 1{
                        let alertView = UIAlertController(title: "\(dictionary["Message"]!)" , message: "Press OK", preferredStyle: .alert)
                        let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction)-> Void in
                        })
                        alertView.addAction(ok_action)
                        self.present(alertView, animated: true, completion: nil)
                    }
                    else
                    {
                        for (key, value) in dictionary {
                            print("\(key) -> \(value)")
                        }
                        self.dismiss(animated: true, completion: nil)

                        //                    self.priorities = dictionary["Priorities"] as! [String:String]
                        //                    print(self.priorities)
                        //                    self.subjects = dictionary["Subjects"] as! [String:String]
                        //                    print(self.subjects)
                        //                    self.departments = dictionary["Departments"] as! [String:String]
                        //                    print(self.departments)
                        
                        
                        // print("My_UserType = \(dictionary["UserType"]!)")
                        
                        self.activityIndicator.stopAnimating()
                        self.messageFrame.removeFromSuperview()
                        
                    }
                }// data != nil
                else{
                    let alertView = UIAlertController(title: "No Data Found" , message: "Press OK", preferredStyle: .alert)
                    let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction)-> Void in
                    })
                    alertView.addAction(ok_action)
                    self.present(alertView, animated: true, completion: nil)
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                }
            }
            return json
        }
    }
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    func progressBarDisplayer(_ msg:String, _ indicator:Bool ) {
        print(msg)
        strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 200, height: 50))
        strLabel.text = msg
        strLabel.textColor = UIColor.white
        messageFrame = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25 , width: 180, height: 50))
        messageFrame.layer.cornerRadius = 15
        messageFrame.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        if indicator {
            activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
            activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
            activityIndicator.startAnimating()
            messageFrame.addSubview(activityIndicator)
        }
        messageFrame.addSubview(strLabel)
        view.addSubview(messageFrame)
    }
    
    func attachements(){
        let yAxis = 40
        var  i = 0
        for _ in self.files {
            let dict = self.files[i] as! [String:Any]
            for (key, value) in dict {
                print("\(key) -> \(value)")
                
                
                let imgView = UIView()
                imgView.frame = CGRect(x:20,y:btnGallery.frame.origin.y + btnGallery.frame.height + 10 + CGFloat(yAxis*i),width:contentView.frame.width - 40,height:30)
                imgView.tag = 301 + i
                //            let imageView = UIImageView()
                //            imageView.frame = CGRect(x:0,y:3,width:24, height:24)
                //            imageView.image = UIImage.init(named: "crossMark")
                let btn = UIButton.init(type: .custom)
                btn.setTitleColor(UIColor.gray, for: UIControlState.normal)
                btn.frame = CGRect(x:35,y:0,width:contentView.frame.width - 70,height:30)
                
                btn.setTitle("Attachement #\(i + 1)", for: .normal)
                btn.accessibilityHint = "\(value)"
                btn.tag = 101 + i
                btn.addTarget(self, action: #selector(tapped(sender:)), for: .touchUpInside)
                //
                let crossBtn = UIButton.init(type: .custom)
                crossBtn.setTitleColor(UIColor.gray, for: UIControlState.normal)
                crossBtn.frame = CGRect(x:0,y:3,width:24, height:24)
                crossBtn.setImage(UIImage.init(named: "Red-cross"), for: .normal)
                crossBtn.setTitle("Attachement #\(i + 1)", for: .normal)
                crossBtn.accessibilityHint = "\(value)"
                crossBtn.tag = 201 + i
                crossBtn.addTarget(self, action: #selector(reloadAttchmentsView(sender:)), for: .touchUpInside)
                imgView.addSubview(crossBtn)
                // imgView.addSubview(imageView)
                imgView.addSubview(btn)
                contentView.addSubview(imgView)
                contentViewConstraint.constant = contentViewConstraint.constant + 30
                i = i + 1
            }
        }
    }
    @objc func reloadAttchmentsView(sender: UIButton)
    {
        var i = 0
        print(sender.tag)
        for _ in files{
            let viewd = self.view.viewWithTag(301 + i)
            i = i + 1
            viewd?.removeFromSuperview()
            viewDidLayoutSubviews()
        }
        files.remove(at: sender.tag - 201)
        contentViewConstraint.constant = contentViewConstraint.constant - 30
        self.attachements()
    }
    @objc func tapped(sender: UIButton)
    {
        print(sender.accessibilityHint!)
        
        
        let dict = files[sender.tag - 101] as! [String:Any]
        for (key, value) in dict {
            print("\(key) -> \(value)")
            
            self.showImageView.contentMode = .scaleAspectFit
            self.showImageView.isHidden = false
            
            let dataDecoded : Data = Data(base64Encoded: "\(value)", options: .ignoreUnknownCharacters)!
            let decodedimage = UIImage(data: dataDecoded)
            self.showImageView.image = decodedimage
            
            //self.showImageView.image = UIImage(data: value as! Data)
        }
    }


}
