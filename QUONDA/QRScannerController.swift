//
//  QRScannerController.swift
//  QRCodeReader
//
//  Created by Simon Ng on 13/10/2016.
//  Copyright © 2016 AppCoda. All rights reserved.
//

import UIKit
import AVFoundation
protocol SavingScannedCartonsNumbersDelegate
{
    func saveScannedCartons(cartons:[String])
    
}
class QRScannerController: UIViewController, AVCaptureMetadataOutputObjectsDelegate,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var messageLabel:UILabel!
    @IBOutlet var topbar: UIView!
    var cartonsNumbers = [String]()
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    var delegate : SavingScannedCartonsNumbersDelegate?

    let supportedCodeTypes = [AVMetadataObjectTypeUPCECode,
                        AVMetadataObjectTypeCode39Code,
                        AVMetadataObjectTypeCode39Mod43Code,
                        AVMetadataObjectTypeCode93Code,
                        AVMetadataObjectTypeCode128Code,
                        AVMetadataObjectTypeEAN8Code,
                        AVMetadataObjectTypeEAN13Code,
                        AVMetadataObjectTypeAztecCode,
                        AVMetadataObjectTypePDF417Code,
                        AVMetadataObjectTypeQRCode]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video as the media type parameter.
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            
            // Set the input device on the capture session.
            captureSession?.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = captureMetadataOutput.availableMetadataObjectTypes

           // captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            videoPreviewLayer?.frame = CGRect.init(x: 20, y: 64, width: self.view.bounds.width - 40, height: 200)
            view.layer.addSublayer(videoPreviewLayer!)
            
            // Start video capture.
            captureSession?.startRunning()
            
            // Move the message label and top bar to the front
            view.bringSubview(toFront: messageLabel)
            view.bringSubview(toFront: topbar)
            
            // Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
                qrCodeFrameView.layer.borderWidth = 2
                view.addSubview(qrCodeFrameView)
                view.bringSubview(toFront: qrCodeFrameView)
            }
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - AVCaptureMetadataOutputObjectsDelegate Methods
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        print(metadataObjects)
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            messageLabel.text = "No QR/barcode is detected"
            return
        }
        
        // Get the metadata object.
        if let barcodeData = metadataObjects.first {
            if(barcodeData is AVMetadataMachineReadableCodeObject){
        let metadataObj =  barcodeData as! AVMetadataMachineReadableCodeObject
        if supportedCodeTypes.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            qrCodeFrameView?.frame.origin.y = (qrCodeFrameView?.frame.origin.y)! + 50
            if metadataObj.stringValue != nil {
                messageLabel.text = metadataObj.stringValue
                if(!cartonsNumbers.contains(metadataObj.stringValue)){
                    
                    
                    var testString = messageLabel.text!
                    do {
                        
                        let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                        let range = NSRange(location: 0, length: testString.characters.count)
                        let block = { (result: NSTextCheckingResult?, flags: NSRegularExpression.MatchingFlags, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
                            if let result = result, result.resultType == NSTextCheckingResult.CheckingType.link {
                                print(result.url)
                                testString = ""
                            }
                        }
                        detector.enumerateMatches(in: testString, options: [], range: range, using: block)
                    } catch {
                        print(error)
                    }
                    print(testString)
                    /*
                     prints:
                     Optional(http://www.yahoo.com)
                     Optional(http://google.fr)
                     */
                    
                    
                    
                    
                    if(testString != ""){
                    cartonsNumbers.append(metadataObj.stringValue)
                    print(cartonsNumbers)
                    tableView.reloadData()
                    }else{
                        //showToast(message: "No Alphanumeric String")
                        self.view.makeToast(message: "No Alphanumeric String")
                    }
                    
                }
            }
            }
        }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartonsNumbers.count    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.width, height: 44))
        headerView.backgroundColor = UIColor.lightGray
        
        let headerLabel = UILabel.init(frame: headerView.frame)
        headerLabel.font = UIFont.boldSystemFont(ofSize: 18.0)
        headerLabel.textColor = UIColor.gray
        headerLabel.text = "List of Scanned Carton Numbers"
        headerView.addSubview(headerLabel)
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath as IndexPath)

            if(cartonsNumbers.count > 0){
        let row = indexPath.row
        cell.textLabel?.text = cartonsNumbers[row]
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        
        let row = indexPath.row
        print(cartonsNumbers[row])
    }
    //Edit Options
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // the cells you would like the actions to appear needs to be editable
        return true
    }
    
    //    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    //        // you need to implement this method too or you can't swipe to display the actions
    //
    //    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        var buttonsItems = [UITableViewRowAction]()
        
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            print("delete button tapped")
            self.cartonsNumbers.remove(at: indexPath.row)
            tableView.reloadData()
        }
        delete.backgroundColor = UIColor.red
        buttonsItems.append(delete)
        print(buttonsItems)
        return buttonsItems
        
    }
    @IBAction func btnDoneAction(_ sender: UIButton) {
        if(cartonsNumbers.count > 0){
            delegate?.saveScannedCartons(cartons: cartonsNumbers) //("stageHere")
            self.dismiss(animated: true, completion: nil)

        }
    }

    
    //End Edit Options
    @IBAction func btnCancelAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

}
