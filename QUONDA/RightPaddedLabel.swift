//
//  RightPaddedLabel.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 28/09/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class RightPaddedLabel: UILabel {

    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 8.0)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
