//
//  SaveLevisCommentVC.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 11/09/2017.
//  Copyright © 2017 Muhammad Naeem. All rights reserved.
//

import UIKit
import MapKit

class SaveLevisCommentVC: UIViewController, UITabBarDelegate, UITextViewDelegate, SavingViewControllerDelegate,UITextFieldDelegate,SavingHybridDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIScrollViewDelegate,CLLocationManagerDelegate {
    
    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!
    
    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    
    
    @IBOutlet weak var mgfBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var mgfAuditResultHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mgfAuditResultConstraint: NSLayoutConstraint!
    @IBOutlet weak var mgfHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var AuditCode: UILabel!
    @IBOutlet weak var AuditStage: UILabel!
    
    
    @IBOutlet weak var txtSafety: UITextField!
    @IBOutlet weak var txtCriticalFailure: UITextField!
    @IBOutlet weak var txtSewing: UITextField!
    @IBOutlet weak var txtAppearance: UITextField!
    @IBOutlet weak var txtMeasurements: UITextField!
    @IBOutlet weak var txtSundriesMissing: UITextField!
    @IBOutlet weak var txtSundriesBroken: UITextField!
    @IBOutlet weak var txtAccuracy: UITextField!
    @IBOutlet weak var txtPhysicals: UITextField!
    @IBOutlet weak var txtOthers: UITextField!
    
    //New
    @IBOutlet weak var txtCartonsSampled: UITextField!
    @IBOutlet weak var txtCartonsInError: UITextField!
    @IBOutlet weak var txtUnitSampled: UITextField!
    @IBOutlet weak var txtUnitsInError: UITextField!
    @IBOutlet weak var txtOverage: UITextField!
    @IBOutlet weak var txtShortage: UITextField!
    @IBOutlet weak var txtWrongSize: UITextField!
    
    @IBOutlet weak var txtWrongPC: UITextField!
    @IBOutlet weak var txtIrregulars: UITextField!
    @IBOutlet weak var txtWrongSundries: UITextField!
    
    @IBOutlet weak var txtShipQty: UITextField!
    @IBOutlet weak var txtReScreenQty: UITextField!
    @IBOutlet weak var txtCartonSize: UITextField!
    @IBOutlet weak var txtDyed: UITextField!
    @IBOutlet weak var txtCutting: UITextField!
    
    
    @IBOutlet weak var btnReInspection: CheckBox!
    @IBOutlet weak var btnGarmentTest: CheckBox!
    @IBOutlet weak var btnShadeBand: CheckBox!
    @IBOutlet weak var btnFabric: CheckBox!
    @IBOutlet weak var btnQAFile: CheckBox!
    @IBOutlet weak var btnPPMeeting: CheckBox!
    @IBOutlet weak var btnFitting: CheckBox!
    @IBOutlet weak var btnColorCheck: CheckBox!
    @IBOutlet weak var btnAccessoriesCheck: CheckBox!
    @IBOutlet weak var btnMeasurementCheck: CheckBox!
    @IBOutlet weak var btnApprovedSample: CheckBox!
    @IBOutlet weak var btnShippingMark: CheckBox!
    @IBOutlet weak var btnPackingCheck: CheckBox!
    
    
    
    @IBOutlet weak var ShipQuantity3: UITextField!
    @IBOutlet weak var ShipDate: UIButton!
    @IBOutlet weak var shipmentDateLabel: UILabel!
    @IBOutlet weak var AssortmentCartonQty: UITextField!
    @IBOutlet weak var AssortmentCartonSize: UITextField!
    @IBOutlet weak var SolidSizeQty: UITextField!
    @IBOutlet weak var SolidSizeType: UITextField!
    @IBOutlet weak var WorkmanshipResult: UIButton!
    
    @IBOutlet weak var AuditResultBtn: UIButton!
    @IBOutlet weak var QAComments: UITextView!
    
    @IBOutlet weak var packingBtn: UIButton!
    @IBOutlet weak var LabBtn: UIButton!
    @IBOutlet weak var MiscBtn: UIButton!
    let imgv = UIImageView.init()
    
    var sampleImage = UIImageView()
    var AuditResult: String!
    var jsonFilePath:URL!
    // let libraryDirectoryPathString = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true).first!
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    var created:Bool!
    var paramPath = "/quonda"
    var otherImagesType:String!
    var savedParamPath:String!
    
    var dictionary:NSDictionary!
    
    var MutableDictionary:NSMutableDictionary!
    var sampleChecked : String!
    var AuditCodein: String!
    var AuditStagein: String!
    var AuditStageColorin : UIColor!
    var defectRatevalue : String!
    var ReportID_against_current_audit: String!
    
    @IBOutlet weak var viewImgV: UIImageView!
    @IBOutlet weak var ViewUploadImages: UIView!
    // outmostScrollView
    @IBOutlet weak var outmostScrollView: UIScrollView!
    
    // outmostStackView
    @IBOutlet weak var outmostStackView: UIStackView!
    
    
    var placeholderLabel:UILabel!
    
    var capturePhoto: UIImage? = nil
    var packageImage: UIImage? = nil
    var labImage: UIImage? = nil
    var imagePicker: UIImagePickerController!
    
    // WorkmanshipResult variable
    var workmanshipResultValue:String!
    
    
    
    var bodyData:String!
    
    var arrayOfDefectsLoggedRecieved:[((Color:UIColor, ID:String),(DefectType:String,DefectCode:String))]=[] //[(DefectType:String,DefectCode:String)]?
    
    let imgView = UIView.init()
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        outmostScrollView.contentSize.height = outmostStackView.frame.height
        
    }
    func CheckAuditResult(){
        if let counfd =  defaults.object(forKey:"addedDefectsCounts \(self.AuditCodein)"){
            var defectRate = "0.0"
            let countdc = counfd as! Int
            if(countdc>0){
                let dd = Double(countdc)/Double(Int(sampleChecked)!)
                defectRate = String(format: "%.2f", dd*100) + "%"
            }
            if(defectRate == "0.0")
            {
                saveAuditResultText("PASS", strID: "P")

            }
           else if(defectRate > defectRatevalue)
            {
                saveAuditResultText("FAIL", strID: "F")
                
            }else{
                saveAuditResultText("PASS", strID: "P")
            }
        }else{
            saveAuditResultText("PASS", strID: "P")
        }
    }
    func sampleImageTapped(){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        
        present(imagePicker, animated: true, completion: nil)
    }
    func samplePhotoLibImageTapped(){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    func getImageView(){
        imgView.isHidden = true
        imgView.frame = self.view.frame
        imgView.backgroundColor = UIColor.white
        self.view.addSubview(imgView)
        
        imgv.frame = CGRect(x:20,y:80,width:self.imgView.frame.size.width - 40,height:300)
        //imgv.contentMode = .scaleAspectFill
        imgView.addSubview(imgv)
        
        let cancelBtn = UIButton.init()
        cancelBtn.frame = CGRect(x:self.imgView.frame.size.width - 80,y:imgv.frame.size.height + 90,width:70,height:30)
        cancelBtn.backgroundColor = .clear
        cancelBtn.setTitleColor(.gray, for: .normal)
        cancelBtn.setTitle("Cancel", for: .normal)
        cancelBtn.layer.cornerRadius = 6.0;
        cancelBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17.0)
        cancelBtn.addTarget(self, action: #selector(ImageviewCancelBtn), for: .touchUpInside)
        imgView.addSubview(cancelBtn)
        
        let saveBtn = UIButton.init()
        saveBtn.frame = CGRect(x:self.imgView.frame.size.width - 350,y:imgv.frame.size.height + 90,width:170,height:30)
        saveBtn.backgroundColor = .orange
        saveBtn.setTitleColor(.gray, for: .normal)
        saveBtn.layer.cornerRadius = 6.0;
        saveBtn.setTitle("Save Attachment(s)", for: .normal)
        saveBtn.addTarget(self, action: #selector(ImageviewsaveBtn), for: .touchUpInside)
        imgView.addSubview(saveBtn)
        
    }
    func ImageviewCancelBtn(){
        imgView.isHidden = true
        // imgView.removeFromSuperview()
    }
    @IBAction func cancelClicked(_ sender: UIButton) {
        mgfHeightConstraint.constant = 100
        mgfBottomConstraint.constant = 565
        self.ViewUploadImages.isHidden = true

    }
    @IBAction func saveAttachmentsClicked(_ sender: UIButton) {
        mgfHeightConstraint.constant = 120
        mgfBottomConstraint.constant = 565

        self.viewImgV.image = nil
        
        if self.fileManager.fileExists(atPath: self.jsonFilePath.absoluteString, isDirectory: &self.isDirectory) {
            do{
                let resizedImage = self.sampleImage.image?.resize(0.45)
                let jpgImageData = UIImageJPEGRepresentation(resizedImage!,0.80)
                try jpgImageData?.write(to: URL(fileURLWithPath: self.jsonFilePath.path), options: .noFileProtection)
                print("data written successfully!")
                self.SAVE_pressed()
                
            } catch let error as NSError {
                print("Image couldn't written to file ")
                print(error.description)
                // cannot save image in memory
            }
        }
        
    }
    func ImageviewsaveBtn(){
        if self.fileManager.fileExists(atPath: self.jsonFilePath.absoluteString, isDirectory: &self.isDirectory) {
            do{
                let resizedImage = self.sampleImage.image?.resize(0.45)
                let jpgImageData = UIImageJPEGRepresentation(resizedImage!, 0.8)
                try jpgImageData?.write(to: URL(fileURLWithPath: self.jsonFilePath.path), options: .noFileProtection)
                print("data written successfully!")
                self.SAVE_pressed()
                
            } catch let error as NSError {
                print("Image couldn't written to file ")
                print(error.description)
                // cannot save image in memory
            }
        }
        
        imgView.isHidden = true
        //imgView.removeFromSuperview()
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        imagePicker.dismiss(animated: true, completion: nil)
        
        DispatchQueue.main.async { () -> Void in
            self.capturePhoto = info[UIImagePickerControllerOriginalImage] as? UIImage
            //print(self.capturePhoto)
            self.sampleImage.image = nil
            if let capturePhoto = self.capturePhoto{
                self.sampleImage.image = capturePhoto
                //self.getImageView()
                self.viewImgV.image = capturePhoto
                self.mgfHeightConstraint.constant = 409
                self.mgfBottomConstraint.constant = 565 - 359

                self.ViewUploadImages.isHidden = false

                
                
            }
            
        }
    }
    
    @IBAction func actionSampleAppopved(_ sender: CheckBox) {
        
        print(sender.isChecked)
        print(sender.actionSample(sender: btnApprovedSample))
    }
    var MainStoryboard:UIStoryboard!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        mgfHeightConstraint.constant = 100
        mgfBottomConstraint.constant = 565
        self.ViewUploadImages.isHidden = true

        //409
        if let loadedCart = defaults.array(forKey: "defectsArr\(AuditCodein!)") as? [[String: Any]] {
            for item in loadedCart {
                
                
                let diyValues = (item["Color"] as! String).components(separatedBy: " ")
                print("diyValues \(diyValues)")
                let returnedColor = UIColor(colorLiteralRed: diyValues[1].FloatValue()!, green: diyValues[2].FloatValue()!, blue: diyValues[3].FloatValue()!, alpha: diyValues[4].FloatValue()!)
                print(returnedColor)
                print(item["Color"]  as! String)    // A, B
                print("ID" + (item["ID"] as! String))    // 19.99, 4.99
                print("DefectType" + (item["DefectType"]   as! String))
                print("DefectCode" + (item["DefectCode"]   as! String))
                
                let abc = ((Color:returnedColor , ID:item["ID"] as! String),(DefectType:item["DefectType"] as! String,DefectCode:item["DefectCode"] as! String))
                self.arrayOfDefectsLoggedRecieved.append(abc)
                
            }
            print(arrayOfDefectsLoggedRecieved)
        }
        
        
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        print(btnApprovedSample.isChecked)
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Selected Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)
        
        
        // packingBtn.setImage(#imageLiteral(resourceName: "Selected packing"), for: .normal)
        // packingBtn.setImage(UIImage.init(named: "selectedpacking"), for: .normal)
        // LabBtn.setImage(#imageLiteral(resourceName: "Selected lab"), for: .normal)
        // LabBtn.setImage(UIImage.init(named: "selectedlab"), for: .normal)
        
        //MiscBtn.setImage(#imageLiteral(resourceName: "Selected misc"), for: .normal)
        //MiscBtn.setImage(UIImage.init(named: "selectedmisc"), for: .normal)
        
        
        MainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        //        self.ShipQuantity3.delegate = self
        //        self.AssortmentCartonQty.delegate = self
        //        self.AssortmentCartonSize.delegate = self
        //        SolidSizeQty.delegate = self
        //        SolidSizeType.delegate = self
        
        self.QAComments.layer.borderWidth = 1
        self.QAComments.layer.borderColor = UIColor.gray.cgColor
        
        self.QAComments.delegate = self
        
        
        // add placeholder text in QAComments TextView
        placeholderLabel = UILabel()
        placeholderLabel.text = "QA Comments"
        placeholderLabel.font = UIFont.italicSystemFont(ofSize: QAComments.font!.pointSize)
        placeholderLabel.sizeToFit()
        QAComments.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: QAComments.font!.pointSize / 2)
        placeholderLabel.textColor = UIColor(white: 0, alpha: 0.3)
        placeholderLabel.isHidden = !QAComments.text.isEmpty
        
        let libraryDirectoryPath = URL(string: documentsDirectoryPathString)!
        jsonFilePath = libraryDirectoryPath.appendingPathComponent("myCapturedImage.jpg")
        
        // creating a .jpg file in the Library Directory
        if !fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("File created ")
                
            } else {
                print("Couldn't create file for some reason")
            }
        } else {
            print("File already exists")
        }
        
        self.addDoneButtonOnKeyboard()
        self.CheckAuditResult()
        self.sendAuditorLocation()

        // self.getImageView()
        // print(arrayOfDefectsLoggedRecieved!)
    }
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(ScheduleAuditViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
//        self.txtSafety.inputAccessoryView = doneToolbar
//        self.txtCriticalFailure.inputAccessoryView = doneToolbar
//        self.txtSewing.inputAccessoryView = doneToolbar
//        self.txtAppearance.inputAccessoryView = doneToolbar
//        self.txtMeasurements.inputAccessoryView = doneToolbar
//        self.txtSundriesMissing.inputAccessoryView = doneToolbar
//        self.txtSundriesBroken.inputAccessoryView = doneToolbar
//        self.txtAccuracy.inputAccessoryView = doneToolbar
//        self.txtPhysicals.inputAccessoryView = doneToolbar
//        self.txtOthers.inputAccessoryView = doneToolbar
//        
//        self.txtPhysicals.inputAccessoryView = doneToolbar
//        
//        self.txtCartonsSampled.inputAccessoryView = doneToolbar
//        
//        self.txtCartonsInError.inputAccessoryView = doneToolbar
//        
//        self.txtUnitSampled.inputAccessoryView = doneToolbar
//        
//        self.txtUnitsInError.inputAccessoryView = doneToolbar
//        self.txtOverage.inputAccessoryView = doneToolbar
//        self.txtShortage.inputAccessoryView = doneToolbar
//        self.txtWrongSize.inputAccessoryView = doneToolbar
//        self.txtWrongPC.inputAccessoryView = doneToolbar
//        self.txtIrregulars.inputAccessoryView = doneToolbar
//        self.txtWrongSundries.inputAccessoryView = doneToolbar
//        
        
        self.QAComments.inputAccessoryView = doneToolbar
        
    }
    
    func doneButtonAction()
    {
//        self.txtSafety.resignFirstResponder()
//        self.txtCriticalFailure.resignFirstResponder()
//        self.txtSewing.resignFirstResponder()
//        self.txtAppearance.resignFirstResponder()
//        self.txtMeasurements.resignFirstResponder()
//        self.txtSundriesMissing.resignFirstResponder()
//        self.txtSundriesBroken.resignFirstResponder()
//        self.txtAccuracy.resignFirstResponder()
//        self.txtPhysicals.resignFirstResponder()
//        self.txtOthers.resignFirstResponder()
//        self.txtCartonsSampled.resignFirstResponder()
//        self.txtCartonsInError.resignFirstResponder()
//        
//        self.txtUnitSampled.resignFirstResponder()
//        
//        self.txtUnitsInError.resignFirstResponder()
//        
//        self.txtOverage.resignFirstResponder()
//        self.txtShortage.resignFirstResponder()
//        
//        self.txtWrongSize.resignFirstResponder()
//        self.txtWrongPC.resignFirstResponder()
//        self.txtIrregulars.resignFirstResponder()
//        
//        self.txtWrongSundries.resignFirstResponder()
//        
        self.QAComments.resignFirstResponder()
        
    }
    //MARK: Location update
    let locationManager = CLLocationManager()
    
    func sendAuditorLocation() {
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        print("loaction config called")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        print("coordinate =",locations.first?.coordinate)
        
        let latitudeLabel = String(locations.first!.coordinate.latitude)
        let longitudeLabel = String(locations.first!.coordinate.longitude)
        print("latitudeLabel =",latitudeLabel)
        print("longitudeLabel =",longitudeLabel)
        
        self.saveLatitudeLongitude(latitudeLabel, longitudeLabel: longitudeLabel)
        
        //        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error) -> Void in
        //            if (error != nil) {
        //                print("ERROR:" + error!.localizedDescription)
        //                return
        //            }
        //            print("placemarks!.count = ",placemarks!.count)
        //
        //            if placemarks!.count > 0 {
        //                let pm = placemarks![0]
        //                self.saveLatitudeLongitude(pm)
        //                print("called after saveLatitude")
        //            } else {
        //                print("Problem with the data received from geocoder")
        //            }
        //
        //        })
    }
    var locationBodyData = ""
    func saveLatitudeLongitude(_ latitudeLabel:String, longitudeLabel:String){ //(placemark: CLPlacemark) {
        print("lat =", latitudeLabel)
        print("long =", longitudeLabel)
        locationBodyData = "&Latitude=\(latitudeLabel)&Longitude=\(longitudeLabel)"
        
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error:" + error.localizedDescription)
    }
    

    func textViewDidChange(_ textView: UITextView) {
        
        if textView == QAComments{
            
            placeholderLabel.isHidden = !QAComments.text.isEmpty
        }
    }
    ////  SAVE IMAGE START
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileLibraryDirectory(_ filename: String) -> String {
        
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL.path
        
    }
    func loadImageFromPath(_ path: String) -> UIImage? {
        
        let image = UIImage(contentsOfFile: path)
        
        if image == nil {
            
            print("missing image at: \(path)")
        }
        print("Loading image from path: \(path)") // this is just for you to see the path in case you want to go to the directory, using Finder.
        return image
        
    }
    
    
    func SAVE_pressed() {
        
        savedParamPath = nil
        print("sampleImage=")
        print(self.sampleImage.image)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        if (self.sampleImage.image != UIImage(named: "default.png")){
            print("every thing is ok")
        }
        if self.sampleImage.image != UIImage(named: "default.png"){
            
            //Now save to arrayOfDeffectsLogged
            
            self.sampleImage.isUserInteractionEnabled = false
            //get image from Library Dir
            let imagePath = fileLibraryDirectory("myCapturedImage.jpg")  // this path is in string format
            print(imagePath)
            let CapImage = loadImageFromPath(imagePath)
            var CapUIImage:UIImage?
            
            //        if CapUIImage == nil{
            //            print("saved image is nill")
            //        }else{
            CapUIImage = CapImage
            //print(CapUIImage)
            
            let defaultImage = UIImage(named: "default")
            let jpgImageData = UIImageJPEGRepresentation(defaultImage!, 0.8)
            let CapUIImageData = UIImageJPEGRepresentation(defaultImage!, 0.8)
            
            if jpgImageData == CapUIImageData{
                print("same image as sample image")
            }
            
            if savedParamPath != nil{
                
                //                ImageUpload(NSURL(string: imagePath)!, destinationFileName: savedParamPath)
                
            }
            else{
                let randomString = randomStringWithLength(6)
                print("randomString=")
                print(randomString)
                let url: URL = URL(string: imagePath)!
                
                let Name1 = "\(self.AuditCode.text!)"+"_"+otherImagesType!+"_"+"\(randomString).jpg"
                print(Name1)
                let destImageName = Name1
                //"AUDIT_CODE_DEFECT_CODE_AREA_CODE_RANDOM_NO.jpg"
                //            let path = "/quonda/\(NSUUID().UUIDString).jpg"
                
                //            paramPath = paramPath+"/"+"\(NSUUID().UUIDString).jpg"
                paramPath = paramPath+"/"+destImageName
                print("paramPath =")
                print(paramPath)
                
                savedParamPath = paramPath
                
                
                //Now Check if Internet connection is not available then save images and other data to Documents directory
                //?????
                //????
                //??
                //?
                
                //If Internet is available or Not just save the data offline.
                //First Stop sync Service Then save data
                
                syncObject.endBackgroundTask()
                updateTimer?.invalidate()
                syncObject = nil
                
                saveDataInDocumentsDir(imagePath, destinationFileName: destImageName)
                
                
                //NOW START Sync Service again
                
                syncObject = syncService()
                
                
                
                /*
                 if !Reachability.isConnectedToNetwork(){
                 
                 print("Internet is not available in AddDefect screen.")
                 
                 // imagePath: is a path of saved image.
                 //paramPath: will be the path of server, where image has to save.
                 
                 saveDataInDocumentsDir(imagePath, destinationFileName: destImageName)
                 
                 }else{
                 
                 ImageUpload(url, destinationFileName: paramPath)
                 }
                 */
                
            }
            
        }else{
            
            var SI = ""
            
            if self.sampleImage.image == UIImage(named: "default.png"){
                SI = "SamplePhoto "
            }
            let alertView = UIAlertController(title: "Note!" , message: "\(SI) Cannot remain empty", preferredStyle: .alert)
            let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            })
            alertView.addAction(ok_action)
            
            self.present(alertView, animated: true, completion: nil)
            
        }
    }
    
    func saveDataInDocumentsDir(_ imagePath:String!, destinationFileName: String!){
        
        print("You are in AddDeffect OFFLINE mode.")
        
        // Creating Json file
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("AddDefectImagesFile\(self.AuditCode.text!).json")
        
        
        // creating a AddDefectImagesFile.json file in the Documents folder
        
        if !fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("AddDefectImagesFile.json File created ")
                
                // Now save it to FilesToSync.json file
                
                saveFileNameTo_FilesToSync("AddDefectImagesFile\(self.AuditCode.text!).json")
                
                //(jsonFilePath.absoluteString)
                
                
            } else {
                print("Couldn't create file for some reason")
            }
        }
        else {
            
            print("AddDefectImagesFile.json File already exists")
        }
        
        //file creation ends
        
        
        
        let DisplayedImage = self.sampleImage.image!.resize(0.45)
        
        if let data = UIImageJPEGRepresentation(DisplayedImage, 0.8) {
            let Imagefilename = getDocumentsDirectory().appendingPathComponent(destinationFileName)
            let ImagePathsfile = getDocumentsDirectory().appendingPathComponent("AddDefectImagesFile\(self.AuditCode.text!).json")
            
            if !self.fileManager.fileExists(atPath: Imagefilename, isDirectory: &self.isDirectory) {
                
                try? data.write(to: URL(fileURLWithPath: Imagefilename), options: [.atomic])
                
                // first read out the file to add this image in AddDefectImagesFile\(AuditCode)
                
                do{
                    var readString: String
                    readString = try NSString(contentsOfFile: ImagePathsfile, encoding: String.Encoding.utf8.rawValue) as String
                    
                    print("readAuditProgress = ",readString)
                    
                    /// here send readString to function to update values stored in AddDeffectImagesFile(AuditCode) File
                    
                    self.AddDeffectImagesInAuditCodeFile(readString, imgFileName: destinationFileName, destinationFileName:destinationFileName)
                }
                catch let error as NSError{
                    print("There is an error while reading AuditProgress(AuditCode) File.",error.description)
                }
                
                
                //                AddDeffectOfflineImageData.append(Imagefilename) //updateValue(Imagefilename, forKey: "fds")
                //
                //                print("AddDeffectOfflineImageData =",AddDeffectOfflineImageData)
                //
                //                do{
                //                    try "\(AddDeffectOfflineImageData)".writeToFile(ImagePathsfile, atomically: true, encoding: NSUTF8StringEncoding)
                //
                //                    //Now save text data into the document directory.
                //                    saveOfflineTextData(destinationFileName)
                //
                //                }
                //                catch{
                //                    print("threre may be an error in writing AddDeffectOfflineImageData.")
                //                }
                //
                //
                //                //Reading from JsonDictionary File stored in documents directory
                //                do{
                //                    var readString: String
                //                    readString = try NSString(contentsOfFile: ImagePathsfile, encoding: NSUTF8StringEncoding) as String
                //                    //                    print("readString=\(readString)")
                ////                    let readStringParsed = parseJSON(readString)
                //                    print("readAddDefect_ImageString = ",readString)
                //                }
                //                catch let error as NSError{
                //                    print("There is an error while reading from JsonDictionary File.",error.description)
                //
                //                }
                
            }
        }
        
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        //        //Reading from JsonDictionary File stored in documents directory
        //        do{
        //            var readString: String
        //            readString = try NSString(contentsOfFile: ImagePathsfile, encoding: NSUTF8StringEncoding) as String
        //            //                    print("readString=\(readString)")
        //            let readStringParsed = parseJSON(readString)
        //            print("readAddDefect_StringParsed = ",readStringParsed)
        //        }
        //        catch let error as NSError{
        //            print("There is an error while reading from JsonDictionary File.",error.description)
        //
        //        }
        
    }
    func parseAddDefectImagesFile(_ jsonString: String) -> [String:String] {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:String] {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    
    
    func AddDeffectImagesInAuditCodeFile(_ savedData: String?, imgFileName:String, destinationFileName:String!){
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        let jsonFilePath = getDocumentsDirectory().appendingPathComponent("AddDefectImagesFile\(self.AuditCode.text!).json")
        
        if savedData != nil && savedData != "" {
            
            var AddDeffectOfflineImageData:[String:String] = parseAddDefectImagesFile(savedData!)
            
            AddDeffectOfflineImageData.updateValue(imgFileName, forKey: destinationFileName)
            //            AddDeffectOfflineImageData.append(imgFileName)
            
            // Now write into the AuditProgress(AuditCode) file
            
            if self.fileManager.fileExists(atPath: jsonFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AddDeffectOfflineImageData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: jsonFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    //Now save text data into the document directory.
                    if(destinationFileName.contains("LAB")){
                        saveOfflineTextDataForLab(destinationFileName)
                    }else{
                    saveOfflineTextData(destinationFileName)
                    }
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
            
        }else{
            
            var AddDeffectOfflineImageData = [String:String]()
            
            AddDeffectOfflineImageData.updateValue(imgFileName, forKey: destinationFileName)
            //            AddDeffectOfflineImageData.append(imgFileName)
            
            // Now write into the AuditProgress(AuditCode) file
            
            if self.fileManager.fileExists(atPath: jsonFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AddDeffectOfflineImageData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: jsonFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    //Now save text data into the document directory.
                    //saveOfflineTextData(destinationFileName)
                    if(destinationFileName.contains("LAB")){
                        saveOfflineTextDataForLab(destinationFileName)
                    }else{
                        saveOfflineTextData(destinationFileName)
                    }

                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        //Reading from JsonDictionary File stored in documents directory
        do{
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath, encoding: String.Encoding.utf8.rawValue) as String
            
            let readStringParsed = parseAddDefectImagesFile(readString)
            print("readAddDefectImagesStringParsed = ",readStringParsed)
        }
        catch let error as NSError{
            print("There is an error while reading from JsonDictionary File.",error.description)
            
        }
        
    }
    //LAB Report Json
    
    func saveOfflineTextDataForLab(_ destinationFileName:String!){
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        let jsonFilePath2 = documentsDirectoryPath.appendingPathComponent("AddTextAuditCodeFileForLab\(self.AuditCode.text!).json")
        // creating a AddDeffectTextFile.json file in the Documents folder
        
        if !fileManager.fileExists(atPath: jsonFilePath2.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath2.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("AddTextAuditCodeFile.json File created ")
                
                // Now save it to FilesToSync.json file
                
                saveFileNameTo_FilesToSync("AddTextAuditCodeFileForLab\(self.AuditCode.text!).json")
                //(jsonFilePath2.absoluteString)
                
                
            } else {
                print("Couldn't create file for some reason")
            }
        }
        else {
            
            print("AddTextAuditCodeFile.json File already exists")
        }
        
        
        
        
        // First check before saving audit
        
        //        if self.PICK_DEFECT_TYPE.text != "DEFECT TYPE" && self.PICK_DEFECT_CODE.text != "DEFECT CODE" && self.PICK_DEFECT_CODE.text != "" && self.PICK_DEFECT_AREA.text != "DEFECT AREA" {
        //
        
        //First Read out the AddTextAuditCodeFile
        
        do{
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath2.absoluteString, encoding: String.Encoding.utf8.rawValue) as String
            
            print("readAddDefect_TextDataString = ",readString)
            
            /// here send readString to function to update values stored in AddDeffectText(AuditCode) File
            
            self.AddDeffectTextDataAuditCodeFileForLab(readString, TextDataFilePath: jsonFilePath2.absoluteString, destinationFileName: destinationFileName)
            
        }
        catch let error as NSError{
            print("There is an error while reading AddDeffectText(AuditCode) File.",error.description)
        }
        
        // }
        
    }
    
    
    
    //End Lab Report Json
    func saveOfflineTextData(_ destinationFileName:String!){
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        let jsonFilePath2 = documentsDirectoryPath.appendingPathComponent("AddTextAuditCodeFile\(self.AuditCode.text!).json")
        // creating a AddDeffectTextFile.json file in the Documents folder
        
        if !fileManager.fileExists(atPath: jsonFilePath2.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath2.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("AddTextAuditCodeFile.json File created ")
                
                // Now save it to FilesToSync.json file
                
                saveFileNameTo_FilesToSync("AddTextAuditCodeFile\(self.AuditCode.text!).json")
                //(jsonFilePath2.absoluteString)
                
                
            } else {
                print("Couldn't create file for some reason")
            }
        }
        else {
            
            print("AddTextAuditCodeFile.json File already exists")
        }
        
        
        
        
        // First check before saving audit
        
        //        if self.PICK_DEFECT_TYPE.text != "DEFECT TYPE" && self.PICK_DEFECT_CODE.text != "DEFECT CODE" && self.PICK_DEFECT_CODE.text != "" && self.PICK_DEFECT_AREA.text != "DEFECT AREA" {
        //
        
        //First Read out the AddTextAuditCodeFile
        
        do{
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath2.absoluteString, encoding: String.Encoding.utf8.rawValue) as String
            
            print("readAddDefect_TextDataString = ",readString)
            
            /// here send readString to function to update values stored in AddDeffectText(AuditCode) File
            
            self.AddDeffectTextDataAuditCodeFile(readString, TextDataFilePath: jsonFilePath2.absoluteString, destinationFileName: destinationFileName)
            
        }
        catch let error as NSError{
            print("There is an error while reading AddDeffectText(AuditCode) File.",error.description)
        }
        
        // }
    }
    
    func parseAddDefectTextDataJSON(_ jsonString: String) -> [String:String] {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:String] {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    //SAVE LAB IMAGES
        
    func AddDeffectTextDataAuditCodeFileForLab(_ savedTextData:String?, TextDataFilePath:String, destinationFileName:String!) -> Void {
            print("savedData =")
            print(savedTextData!)
            print("TextDataFilePath =")
            print(TextDataFilePath)
            print("destinationFileName =")
            print(destinationFileName!)
            var imageName = ""
            var imageUrl = ""
            if savedTextData != nil && savedTextData != "" {
                
                //            self.endDate = Date()
                //            self.convertedEndDate = defaults.object(forKey: self.AuditCode.text!)as! String
                //            //= self.endDate.CustomDateTimeformat
                //            print(self.convertedEndDate)
                print(destinationFileName!)
                // self.showalertPath(messsage: destinationFileName!)
                //if(destinationFileName!.contains("LAB")){
                    
                    imageName = "&SpecsSheets=\(destinationFileName!)"
                    imageUrl = "http://app.3-tree.com/quonda/save-specs-sheet.php"
                    self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCode.text!)\(imageName)"
                    
                    print("AddDefect-Offline_bodydata-> \(self.bodyData)")
                    
                    
                    var AddDeffectOfflineTextData:[String:String] = parseAddDefectTextDataJSON(savedTextData!)
                    
                    AddDeffectOfflineTextData.updateValue(bodyData, forKey: "AddDeffect_bodyData\(destinationFileName!)")
                    AddDeffectOfflineTextData.updateValue(imageUrl, forKey: "AddDeffect_url_LAB")
                    if self.fileManager.fileExists(atPath: TextDataFilePath, isDirectory: &self.isDirectory) {
                        do{
                            let jsonDictionary = try JSONSerialization.data(withJSONObject: AddDeffectOfflineTextData, options: JSONSerialization.WritingOptions.prettyPrinted)
                            
                            let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                            
                            try json.write(toFile: TextDataFilePath, atomically: true, encoding: String.Encoding.utf8)
                            
                        } catch let error as NSError {
                            print("JSON data couldn't written to AddTextAuditCodeFile // File not exist")
                            print(error.description)
                        }
                    }
                    
                    
                    
               // }
                
                
                
                
                
                // Reset all and dismiss current view
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.savedParamPath = nil
                
                DefectsData0.removeAll()
                DefectsRateData0.removeAll()
                DefectsAreaData0.removeAll()
                
                // self.dismiss(animated: false, completion: nil)
                
            }else{
                
                
                //            self.endDate = Date()
                //            self.convertedEndDate = defaults.object(forKey: self.AuditCodeLabel.text!)as! String  //= self.endDate.CustomDateTimeformat
                //            print(self.convertedEndDate)
                print(destinationFileName!)
               // if(destinationFileName!.contains("LAB")){
                    imageName = "&SpecsSheets=\(destinationFileName!)"
                    imageUrl = "http://app.3-tree.com/quonda/save-specs-sheet.php"
                    self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCode.text!)\(imageName)"
                    
                    //            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCodeLabel.text!)&ReportId=\(self.ReportId!)&DefectCode=\(self.DefectCodeID)&DefectArea=\(self.DefectedAreaID)&Nature=\(self.MajorORMinor!)&SampleNo=\(self.SampleChecked.text!)&DateTime=\(self.convertedEndDate!)&Remarks=\(self.commentsField.text!)"
                    
                    print("AddDefect-Offline_bodydata-> \(self.bodyData)")
                    
                    
                    var AddDeffectOfflineTextData = [String:String]()
                    
                    AddDeffectOfflineTextData.updateValue(bodyData, forKey: "AddDeffect_bodyData\(destinationFileName!)")
                    AddDeffectOfflineTextData.updateValue(imageUrl, forKey: "AddDeffect_url_LAB")
                    
                    if self.fileManager.fileExists(atPath: TextDataFilePath, isDirectory: &self.isDirectory) {
                        do{
                            let jsonDictionary = try JSONSerialization.data(withJSONObject: AddDeffectOfflineTextData, options: JSONSerialization.WritingOptions.prettyPrinted)
                            
                            let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                            
                            try json.write(toFile: TextDataFilePath, atomically: true, encoding: String.Encoding.utf8)
                            
                        } catch let error as NSError {
                            print("JSON data couldn't written to file // File not exist")
                            print(error.description)
                        }
                    }
                    
                    
              //  }
                
                //            AddDeffectOfflineTextData.updateValue(bodyData, forKey: "AddDeffect_bodyData\(destinationFileName!)")
                //            AddDeffectOfflineTextData.updateValue(imageUrl, forKey: "AddDeffect_url")
                
                
                
                // Reset all and dismiss current view
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                //            self.butotn.isEnabled = true
                //            self.butotn.backgroundColor = UIColor.lightGray
                //
                self.savedParamPath = nil
                
                DefectsData0.removeAll()
                DefectsRateData0.removeAll()
                DefectsAreaData0.removeAll()
                
                // self.dismiss(animated: false, completion: nil)
                
                
            }
            
            //Reading from JsonDictionary File stored in documents directory
            do{
                var readString: String
                readString = try NSString(contentsOfFile: TextDataFilePath, encoding: String.Encoding.utf8.rawValue) as String
                //                    print("readString=\(readString)")
                let readStringParsed = parseAddDefectTextDataJSON(readString)
                print("AddDefectText_readStringParsed = ",readStringParsed)
            }
            catch let error as NSError{
                print("There is an error while reading from AddTextAuditCodeFile.",error.description)
                
            }
            
        }
        

        
        
        //END SAVING LAB IMAGES
    
    func AddDeffectTextDataAuditCodeFile(_ savedTextData:String?, TextDataFilePath:String, destinationFileName:String!) -> Void {
        print("savedData =")
        print(savedTextData!)
        print("TextDataFilePath =")
        print(TextDataFilePath)
        print("destinationFileName =")
        print(destinationFileName!)
        var imageName = ""
        var imageUrl = ""
        if savedTextData != nil && savedTextData != "" {
            
            //            self.endDate = Date()
            //            self.convertedEndDate = defaults.object(forKey: self.AuditCode.text!)as! String
            //            //= self.endDate.CustomDateTimeformat
            //            print(self.convertedEndDate)
            print(destinationFileName!)
           // self.showalertPath(messsage: destinationFileName!)
            if(destinationFileName!.contains("LAB")){
                
                imageName = "&SpecsSheets=\(destinationFileName!)"
                imageUrl = "http://app.3-tree.com/quonda/save-specs-sheet.php"
                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCode.text!)\(imageName)"
                
                print("AddDefect-Offline_bodydata-> \(self.bodyData)")
                
                
                var AddDeffectOfflineTextData:[String:String] = parseAddDefectTextDataJSON(savedTextData!)
                
                AddDeffectOfflineTextData.updateValue(bodyData, forKey: "AddDeffect_bodyData\(destinationFileName!)")
                AddDeffectOfflineTextData.updateValue(imageUrl, forKey: "AddDeffect_url_LAB")
                if self.fileManager.fileExists(atPath: TextDataFilePath, isDirectory: &self.isDirectory) {
                    do{
                        let jsonDictionary = try JSONSerialization.data(withJSONObject: AddDeffectOfflineTextData, options: JSONSerialization.WritingOptions.prettyPrinted)
                        
                        let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                        
                        try json.write(toFile: TextDataFilePath, atomically: true, encoding: String.Encoding.utf8)
                        
                    } catch let error as NSError {
                        print("JSON data couldn't written to AddTextAuditCodeFile // File not exist")
                        print(error.description)
                    }
                }
                
                
                
            }
            else{
                
                imageName = "&Image=\(destinationFileName!)"
                imageUrl = "http://app.3-tree.com/quonda/save-report-image.php"
                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCode.text!)\(imageName)"
                print("AddDefect-Offline_bodydata-> \(self.bodyData)")
                var AddDeffectOfflineTextData:[String:String] = parseAddDefectTextDataJSON(savedTextData!)
                
                AddDeffectOfflineTextData.updateValue(bodyData, forKey: "AddDeffect_bodyData\(destinationFileName!)")
                AddDeffectOfflineTextData.updateValue(imageUrl, forKey: "AddDeffect_url_PACK")
                if self.fileManager.fileExists(atPath: TextDataFilePath, isDirectory: &self.isDirectory) {
                    do{
                        let jsonDictionary = try JSONSerialization.data(withJSONObject: AddDeffectOfflineTextData, options: JSONSerialization.WritingOptions.prettyPrinted)
                        
                        let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                        
                        try json.write(toFile: TextDataFilePath, atomically: true, encoding: String.Encoding.utf8)
                        
                    } catch let error as NSError {
                        print("JSON data couldn't written to AddTextAuditCodeFile // File not exist")
                        print(error.description)
                    }
                }
                
                
                
            }
            
            
            
            
            // Reset all and dismiss current view
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.savedParamPath = nil
            
            DefectsData0.removeAll()
            DefectsRateData0.removeAll()
            DefectsAreaData0.removeAll()
            
            // self.dismiss(animated: false, completion: nil)
            
        }else{
            
            
            //            self.endDate = Date()
            //            self.convertedEndDate = defaults.object(forKey: self.AuditCodeLabel.text!)as! String  //= self.endDate.CustomDateTimeformat
            //            print(self.convertedEndDate)
            print(destinationFileName!)
            if(destinationFileName!.contains("LAB")){
                imageName = "&SpecsSheets=\(destinationFileName!)"
                imageUrl = "http://app.3-tree.com/quonda/save-specs-sheet.php"
                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCode.text!)\(imageName)"
                
                //            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCodeLabel.text!)&ReportId=\(self.ReportId!)&DefectCode=\(self.DefectCodeID)&DefectArea=\(self.DefectedAreaID)&Nature=\(self.MajorORMinor!)&SampleNo=\(self.SampleChecked.text!)&DateTime=\(self.convertedEndDate!)&Remarks=\(self.commentsField.text!)"
                
                print("AddDefect-Offline_bodydata-> \(self.bodyData)")
                
                
                var AddDeffectOfflineTextData = [String:String]()
                
                AddDeffectOfflineTextData.updateValue(bodyData, forKey: "AddDeffect_bodyData\(destinationFileName!)")
                AddDeffectOfflineTextData.updateValue(imageUrl, forKey: "AddDeffect_url_LAB")
                
                if self.fileManager.fileExists(atPath: TextDataFilePath, isDirectory: &self.isDirectory) {
                    do{
                        let jsonDictionary = try JSONSerialization.data(withJSONObject: AddDeffectOfflineTextData, options: JSONSerialization.WritingOptions.prettyPrinted)
                        
                        let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                        
                        try json.write(toFile: TextDataFilePath, atomically: true, encoding: String.Encoding.utf8)
                        
                    } catch let error as NSError {
                        print("JSON data couldn't written to file // File not exist")
                        print(error.description)
                    }
                }
                
                
            }
            else{
                
                imageName = "&Image=\(destinationFileName!)"
                imageUrl = "http://app.3-tree.com/quonda/save-report-image.php"
                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCode.text!)\(imageName)"
                
                //            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCodeLabel.text!)&ReportId=\(self.ReportId!)&DefectCode=\(self.DefectCodeID)&DefectArea=\(self.DefectedAreaID)&Nature=\(self.MajorORMinor!)&SampleNo=\(self.SampleChecked.text!)&DateTime=\(self.convertedEndDate!)&Remarks=\(self.commentsField.text!)"
                
                print("AddDefect-Offline_bodydata-> \(self.bodyData)")
                
                
                var AddDeffectOfflineTextData = [String:String]()
                
                AddDeffectOfflineTextData.updateValue(bodyData, forKey: "AddDeffect_bodyData\(destinationFileName!)")
                AddDeffectOfflineTextData.updateValue(imageUrl, forKey: "AddDeffect_url_PACK")
                if self.fileManager.fileExists(atPath: TextDataFilePath, isDirectory: &self.isDirectory) {
                    do{
                        let jsonDictionary = try JSONSerialization.data(withJSONObject: AddDeffectOfflineTextData, options: JSONSerialization.WritingOptions.prettyPrinted)
                        
                        let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                        
                        try json.write(toFile: TextDataFilePath, atomically: true, encoding: String.Encoding.utf8)
                        
                    } catch let error as NSError {
                        print("JSON data couldn't written to file // File not exist")
                        print(error.description)
                    }
                }
                
                
                
            }
            
            //            AddDeffectOfflineTextData.updateValue(bodyData, forKey: "AddDeffect_bodyData\(destinationFileName!)")
            //            AddDeffectOfflineTextData.updateValue(imageUrl, forKey: "AddDeffect_url")
            
            
            
            // Reset all and dismiss current view
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            //            self.butotn.isEnabled = true
            //            self.butotn.backgroundColor = UIColor.lightGray
            //
            self.savedParamPath = nil
            
            DefectsData0.removeAll()
            DefectsRateData0.removeAll()
            DefectsAreaData0.removeAll()
            
            // self.dismiss(animated: false, completion: nil)
            
            
        }
        
        //Reading from JsonDictionary File stored in documents directory
        do{
            var readString: String
            readString = try NSString(contentsOfFile: TextDataFilePath, encoding: String.Encoding.utf8.rawValue) as String
            //                    print("readString=\(readString)")
            let readStringParsed = parseAddDefectTextDataJSON(readString)
            print("AddDefectText_readStringParsed = ",readStringParsed)
        }
        catch let error as NSError{
            print("There is an error while reading from AddTextAuditCodeFile.",error.description)
            
        }
        
    }
    
    
    /// SAVE IMAGE END
    
    override func viewWillAppear(_ animated: Bool) {
        self.AuditCode.text = AuditCodein
        self.AuditStage.text = AuditStagein
        if(AuditStagein == "Final"){
            shipmentDateLabel.isHidden = true
            ShipDate.isHidden = true
        }
        else{
            shipmentDateLabel.isHidden = true
            ShipDate.isHidden = true
        }
        self.AuditStage.backgroundColor = AuditStageColorin
        //
        print("ReportID_against_current_audit =",ReportID_against_current_audit)
    }
    
    
    @IBAction func selectAttachmentImage(_ sender: AnyObject) {
        
        otherImagesType = ""
        let pressedBtn = sender as! UIButton
        print(pressedBtn == packingBtn)
        print(pressedBtn == MiscBtn)
        print(pressedBtn == LabBtn)
        var message = ""
        if pressedBtn == packingBtn{
            otherImagesType = "PACK"
            message =  "Choose Images For Packing"
            
        }
        else if pressedBtn == MiscBtn{
            
            
            otherImagesType = "MISC"
            message =  "Choose Images For \(otherImagesType!)"
            
        }
        else if pressedBtn == LabBtn{
            
            
            paramPath =  "/specs-sheet"
            isLabImage = true
            otherImagesType = "LAB"
            message =  "Choose Image For LAB Report/Specs Sheet"
            
        }
        print(otherImagesType)
        
        if(otherImagesType == "LAB"){
            
        }
        let alertView = UIAlertController(title: nil , message: message, preferredStyle: .alert)
        let camera_action: UIAlertAction = UIAlertAction(title: "Camera", style: .default) { (UIAlertAction) in
            
            self.sampleImageTapped()
        }
        
        let photos_action: UIAlertAction = UIAlertAction(title: "Photos", style: .default) { (UIAlertAction) in
            
            self.samplePhotoLibImageTapped()
        }
        alertView.addAction(camera_action)
        alertView.addAction(photos_action)
        self.present(alertView, animated: true, completion:{
            alertView.view.superview?.isUserInteractionEnabled = true
            alertView.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
        
    }
    func alertControllerBackgroundTapped()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func textFieldShouldReturn(_ userText: UITextField!) -> Bool {
        
        
        if ReportID_against_current_audit == "38"{
            
            //            ShipQuantity3
            //            AssortmentCartonQty
            //            AssortmentCartonSize
            userText.resignFirstResponder()
            
        }
        return true;
    }
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        //        var startString = ""
        //        if(textField == SolidSizeType){
        //            if (SolidSizeType != nil)
        //            {
        //                startString += textField.text!
        //            }
        //            startString += string
        //            let limitNumber = startString.characters.count
        //            if limitNumber > 1
        //            {
        //                return false
        //            }
        //            else
        //            {
        //                return true;
        //            }
        //        }
        //        else {
        return true
        // }
    }
    //    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    //        if(textField == SolidSizeType){
    //            if(SolidSizeQty)
    //        }
    //    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        // mgfBottomConstraint.constant = 205
        //        mgfAuditResultConstraint.constant = 492
        //        mgfAuditResultHeightConstraint.constant = 1750
        
        return true
    }
    //
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        // mgfBottomConstraint.constant = 5
        //        mgfAuditResultConstraint.constant = 292
        //        mgfAuditResultHeightConstraint.constant = 1550
        
        textView.resignFirstResponder()
        return true
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            
            if ReportID_against_current_audit == "34" || ReportID_against_current_audit == "14"{
                textView.resignFirstResponder()
            }
        }  // end of back slash check
        return true
    }
    
    
    @IBAction func selectShipmentDate(_ sender: UIButton) {
        DatePickerDialog().show("SelectDate", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            
            self.ShipDate.setTitle("\(date.formatted)", for: UIControlState())
        }
        
        
    }
    @IBAction func SelectShipmentDate(_ sender: Any) {
        
    }
    
    
    @IBAction func SelectWorkmanshipResult(_ sender: Any) {
        
        print("SelectWorkmanshipAuditResult button is cliked.")
        
        let popupVC = self.storyboard!.instantiateViewController(withIdentifier: "PopUpVCHybrid") as! HybridPopUpViewControler
        
        popupVC.delegate = self
        
        popupVC.pickerdata = ["PASS","FAIL"]
        popupVC.pickerdataIDs = ["P","F"]
        
        popupVC.WhichButton = "GarmentAuditResult"
        
        popupVC.AllreadySelectedText = self.WorkmanshipResult.titleLabel!.text!
        
        self.addChildViewController(popupVC)
        popupVC.view.frame = self.view.frame
        
        self.view.addSubview(popupVC.view)
        self.didMove(toParentViewController: self)
        
        
    }
    
    
    
    func saveGarmentConformityAuditResult(_ strText: String, strID: String) {
        
        self.WorkmanshipResult.setTitle(strText, for: .normal)
        workmanshipResultValue = strID
    }
    func savePackagingAuditResult(_ strText: String, strID: String) {
        // Do Nothing
    }
    
    func showOfflineAuditTypes(){
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("login.json")
        jsonLoginFilePath = jsonFilePath
        print(jsonLoginFilePath.path)
        
        do {
            var readString: String
            readString = try NSString(contentsOfFile: jsonLoginFilePath.path, encoding: String.Encoding.utf8.rawValue) as String
            print(readString)
            self.loadAuditTypes(readString)
        } catch let error as NSError {
            print(error.description)
        }
        
    }
    var AuditResultArrayIDs = [String]()
    var AuditResultArray = [String]()
    func loadAuditTypes(_ savedData:String?){
        if savedData != nil {
            
            // UIApplication.shared.isNetworkActivityIndicatorVisible = true
            AuditResultArray.removeAll()
            AuditResultArrayIDs.removeAll()
            self.dictionary = self.parseJSON(savedData!)
            print(self.dictionary)
            let AuditType = self.dictionary["Results"]! as! [String:AnyObject]
            for type in AuditType{
                //let aType =  type as! [String:AnyObject]
                AuditResultArray.append(type.1 as! String)
                AuditResultArrayIDs.append(type.0)
            }
        }
    }
    @IBAction func SelectAuditResult(_ sender: AnyObject) {
        
        print("SelectAuditResult button is cliked.")
        
        let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as! PopUpViewController
        
        popupVC.delegate = self
        if let userType = defaults.object(forKey: "UserType"){
            if("\(userType as! String)" == "LEVIS"){
                // audit_Item = "BOOKING"
                //AuditTypeView.isHidden = false
                showOfflineAuditTypes()
                popupVC.pickerdata = AuditResultArray
                popupVC.pickerdataIDs = AuditResultArrayIDs
                
                
            }else{
                if(ReportID_against_current_audit == "14"||ReportID_against_current_audit == "34"){
                    popupVC.pickerdata = ["ACCEPTED","REJECTED","HOLD"]
                    popupVC.pickerdataIDs = ["P","F","H"]
                }else{
                    popupVC.pickerdata = ["PASS","FAIL","HOLD"]
                    popupVC.pickerdataIDs = ["P","F","H"]
                    
                }
            }
        }
        
        popupVC.WhichButton = "AuditResult"
        
        popupVC.AllreadySelectedText = self.AuditResultBtn.titleLabel!.text!
        
        self.addChildViewController(popupVC)
        popupVC.view.frame = self.view.frame
        
        self.view.addSubview(popupVC.view)
        self.didMove(toParentViewController: self)
        
    }
    func saveAuditResultText(_ strText: String, strID: String) {
        print("Audit Result =",strText)
        print("Result ID =",strID)
        self.AuditResultBtn.setTitle(strText, for: UIControlState())
        self.AuditResult = strID
    }
    
    func saveAuditTypeText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveAuditorsText(_ strText: String, strID: String) {
        // DO Nothing....
    }
    func saveVendorsText(_ strText: String, strID: String) {
        // DO Nothing....
    }
    func saveText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveUnitText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveBrandText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveStyleText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveReportText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveVendorText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveSampleSizeText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveProductionLineText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveAQLText(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveAQLData(_ strText: String, strID: String) {
        // Do Nothing
    }
    func saveSelectedSize(_ strText: String, strID: String) {
        print(strText,strID)
        
    }
    func saveSelectedColor(_ strText : String, strID : String)
    {
        
    }
    func showalertPath(messsage:String){
        let alertView = UIAlertController(title: nil , message: messsage, preferredStyle: .alert)
        let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertView.addAction(ok_action)
        self.present(alertView, animated: true, completion: nil)
    }

    func showalert(){
        let alertView = UIAlertController(title: nil , message: "Do write some comments", preferredStyle: .alert)
        let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertView.addAction(ok_action)
        self.present(alertView, animated: true, completion: nil)
    }
    @IBAction func Save_and_Continue(_ sender: AnyObject) {
        
        if self.QAComments.text == nil || self.QAComments.text.isEmpty {
            self.showalert()
        }else if self.AuditResult == nil || self.AuditResult.isEmpty {
            
            
            let alertView = UIAlertController(title: nil , message: "Are you sure Audit Result is Fail?", preferredStyle: .alert)
            let yes_action: UIAlertAction = UIAlertAction(title: "Yes", style: .default, handler: { (UIAlertAction) -> Void in
                
                self.AuditResult = "F"
                //                self.saveContinue_Pressed = true
                self.saveOfflineCommentsData()
            })
            let no_action: UIAlertAction = UIAlertAction(title: "No", style: .default, handler: {(UIAlertAction)-> Void in
                
            })
            
            alertView.addAction(yes_action)
            alertView.addAction(no_action)
            
            self.present(alertView, animated: true, completion: nil)
            
            
        }else{
            //            saveContinue_Pressed = true
            self.saveOfflineCommentsData()
        }
    }
    
    
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.MainStoryboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
            //            defaults.removeObjectForKey("userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            if let resultController = MainStoryboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = MainStoryboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
            //        case 4:
            //            if let resultController = MainStoryboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
            //                //                self.dismissViewControllerAnimated(true, completion: nil)
            //                presentViewController(resultController, animated: true, completion: nil)
            //            }
            //
            //            let selectedImage:UIImage = UIImage(named: "Selected QAReport")!
            //            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            //
        //            break
        case 5:
            if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }else{
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 6:
            if let resultController = MainStoryboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = MainStoryboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if let resultController = MainStoryboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_selected_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        default:
            break
        }
    }
    
    
    
    func saveOfflineCommentsData(){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        print("You are in AuditComments OFFLINE mode.")
        
        // Creating AuditComment Json file
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("AuditCommentFile\(self.AuditCode.text!).json")
        
        // creating a .json file in the Documents folder
        
        if !fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("File created ")
                
                // Now save it to FilesToSync.json file
                
                saveFileNameTo_FilesToSync("AuditCommentFile\(self.AuditCode.text!).json")
                //(jsonFilePath.absoluteString)
                
            } else {
                print("Couldn't create file for some reason")
            }
        } else {
            print("File already exists")
        }
        //file creation ends
        
        
        
        //First Read out the AuditComment AuditCodeFile
        
        do{
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath.absoluteString, encoding: String.Encoding.utf8.rawValue) as String
            
            print("readAuditComment_String = ",readString)
            
            /// here send readString to function to update values stored in AuditCommentFile(AuditCode) File
            
            self.AuditCommentAuditCodeFile(readString, AuditCommentFilePath: jsonFilePath.absoluteString)
            
        }
        catch let error as NSError{
            print("There is an error while reading AddDeffectText(AuditCode) File.",error.description)
        }
        
    }// end of func
    
    
    func AuditCommentAuditCodeFile(_ savedCommentsData:String?, AuditCommentFilePath: String) -> Void {
        
        AuditCommentAuditCodeFile_Hybrid(savedCommentsData, AuditCommentFilePath: AuditCommentFilePath)
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        //Reading from JsonDictionary File stored in documents directory
        do{
            var readString: String
            readString = try NSString(contentsOfFile: AuditCommentFilePath, encoding: String.Encoding.utf8.rawValue) as String
            //                    print("readString=\(readString)")
            let readStringParsed = parseCommentsJSON(readString)
            print("AuditComments_readStringParsed = ",readStringParsed)
        }
        catch let error as NSError{
            print("There is an error while reading from JsonDictionary File.",error.description)
            
        }
        
    }
    
    
    
    func AuditCommentAuditCodeFile_Hybrid(_ savedCommentsData:String?, AuditCommentFilePath: String) -> Void{
        
        // - - - -
        
        if savedCommentsData != nil && savedCommentsData != "" {
            
            
            if self.AuditResult == nil{
                
                self.AuditResult = "F"
            }
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodein!)&AuditResult=\(self.AuditResult!)&Comments=\(self.QAComments.text!)\(locationBodyData)"
            print("AuditCommentsHybrid-bodydata -> \(self.bodyData)")
            
            
            
            var AuditCommentsOfflineData:[String:String] = parseCommentsJSON(savedCommentsData!)
            
            
            let randomString = randomStringWithLength(6)
            
            AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "AuditComments_bodyData\(self.AuditCode.text!)_\(randomString)")
            AuditCommentsOfflineData.updateValue("http://app.3-tree.com/quonda/save-comments.php", forKey: "AuditComment_url")
            
            
            if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    //Note!!
                    
                    if !Reachability.isConnectedToNetwork(){
                        
                        //BEFORE MOVING ONTO THE NEXT SCREEN YOU HAVE TO UPDATE AUDITCOMPLETED VALUE IN AUDITMODE.JSON
                        
                        /*   let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
                         
                         let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("AuditMode.json")
                         do {
                         var readString: String
                         readString = try NSString(contentsOfFile: jsonFilePath.path!, encoding: NSUTF8StringEncoding) as String
                         print("readString_AuditorModeVC=\(readString)")
                         self.updateAuditorModeVCDataWhenOffline(readString)
                         } catch let error as NSError {
                         print(error.description)
                         }
                         */
                    }
                    
                    
                    // now move onto the Next screen
                    //???????
                    //?????
                    //???
                    self.locationManager.stopUpdatingLocation()

                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    defaults.set("Completed", forKey: "OfflineAudit\(AuditCodein!)")

                    //let counfd =  defaults.object(forKey:"addedDefectsCounts \(self.AuditCodein)") as! Int
                    
                    //if(DefectAdded.contains(AuditCodein)){
                    //if self.arrayOfDefectsLoggedRecieved.count>0||counfd>0{
                    //if SavedDefects != nil {
                    
                    // performSegue(withIdentifier: "CAP", sender: nil)
                    
                    // }
                    // }
//                    if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
//                        if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
//                            
//                            present(resultController, animated: true, completion: nil)
//                        }
//                    }else{
                        if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                            
                            present(resultController, animated: true, completion: nil)
                        }
                    //}
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
            
        }else{
            
            if self.AuditResult == nil{
                
                self.AuditResult = "F"
            }
            
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(AuditCodein!)&AuditResult=\(self.AuditResult!)&Comments=\(self.QAComments.text!)\(locationBodyData)"
            print("AuditCommentsHybrid-bodydata -> \(self.bodyData)")
            
            
            
            
            
            var AuditCommentsOfflineData = [String:String]()
            
            
            let randomString = randomStringWithLength(6)
            
            AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "AuditComments_bodyData\(self.AuditCode.text!)_\(randomString)")
            AuditCommentsOfflineData.updateValue("http://app.3-tree.com/quonda/save-comments.php", forKey: "AuditComment_url")
            if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    // first read out the file
                    //                    do{
                    //                        let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
                    //                        let jsonSyncFilePath = documentsDirectoryPath.URLByAppendingPathComponent("FilesToSync.json")
                    //
                    //                        var readString: String?
                    //                        readString = try NSString(contentsOfFile: jsonSyncFilePath.path!, encoding: NSUTF8StringEncoding) as String
                    //                        print("sync file after adding comments =",readString)
                    //                    }catch{
                    //                        print("cannot read")
                    //                    }
                    
                    
                    //Note!!
                    
                    //BEFORE MOVING ONTO THE NEXT SCREEN YOU HAVE TO UPDATE AUDITCOMPLETED VALUE IN AUDITMODE.JSON
                    
                    /*     let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
                     
                     let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("AuditMode.json")
                     do {
                     var readString: String
                     readString = try NSString(contentsOfFile: jsonFilePath.path!, encoding: NSUTF8StringEncoding) as String
                     print("readString_AuditorModeVC=\(readString)")
                     self.updateAuditorModeVCDataWhenOffline(readString)
                     } catch let error as NSError {
                     print(error.description)
                     }
                     */
                    
                    
                    // now move onto the ManagerModeViewController screen
                    //?????
                    //???
                    //??
                    self.locationManager.stopUpdatingLocation()
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    defaults.set("Completed", forKey: "OfflineAudit\(AuditCodein!)")

                    //if(DefectAdded.contains(AuditCodein)){
                    //let counfd =  defaults.object(forKey:"addedDefectsCounts \(self.AuditCodein)") as! Int
                    
                    // if self.arrayOfDefectsLoggedRecieved.count>0||counfd>0 {
                    //if SavedDefects != nil {
                    
                    // performSegue(withIdentifier: "CAP", sender: nil)
                    
                    //}else{
//                    if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
//                        if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
//                            
//                            present(resultController, animated: true, completion: nil)
//                        }
//                    }else{
                        if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                            
                            present(resultController, animated: true, completion: nil)
                        }
                   // }
                    //}
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
        }
        
        // - - -
        
    }
    //Start Audit saving Comments
    
    
    
    
    
    //End Audit Saving Comments
    //Random string function
    
    func randomStringWithLength(_ len: Int) -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for i in 0 ..< (len){
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString
    }
    
    
    
    func updateAuditorModeVCDataWhenOffline(_ savedData:String?) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        print("savedDataString=\(savedData)")
        
        if savedData != nil {
            
            self.MutableDictionary = self.JSON_To_FoundationObj(savedData!)
            
            var planned, completed, pending:Int!
            planned = (self.MutableDictionary["Today"]!as! [String: AnyObject])["Planned"] as! Int
            completed = (self.MutableDictionary["Today"]!as! [String: AnyObject])["Completed"] as! Int
            pending = (self.MutableDictionary["Today"]!as! [String: AnyObject])["Pending"] as! Int
            
            print("Total Audits = \(self.MutableDictionary["Audits"]!)")
            
            //            print("OOOOOOOOoooooooo = \(self.MutableDictionary["Audits"]![0]["AuditCode"]!)")
            //            print((self.MutableDictionary["Audits"]![0] as! [String: AnyObject])["Completed"]!)
            
            
            if var All_Audits = self.MutableDictionary["Audits"]! as? [[String : AnyObject]] {
                
                var i = 0
                for Audit in All_Audits{
                    
                    let AuditCodeName = Audit["AuditCode"]! as! String
                    print("AuditorModeVC_AuditCode= \(AuditCodeName)")
                    
                    let completedAuditt = Audit["Completed"]! as! String
                    print("Before =",completedAuditt)
                    
                    if self.AuditCode.text! == AuditCodeName {
                        print("audit codes are same.")
                        All_Audits[i]["Completed"] = "Y" as AnyObject?
                        self.MutableDictionary["Audits"] = All_Audits
                        
                        //Now its Time to save this update in AuditMode.json file
                        saveOfflineAuditorModeVCData()
                    }
                    
                    i += 1
                }
            }
            print("After changing to Completed = ")
            //            print(self.MutableDictionary["Audits"]![0]["Completed"]!)
            
            //            AuditMode.json
            
        }
    }
    
    
    func saveOfflineAuditorModeVCData(){
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("AuditMode.json")
        
        
        if self.fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &self.isDirectory) {
            do{
                let jsonDictionary = try JSONSerialization.data(withJSONObject: self.MutableDictionary, options: JSONSerialization.WritingOptions.prettyPrinted)
                
                let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                
                try json.write(toFile: jsonFilePath.path, atomically: true, encoding: String.Encoding.utf8)
                
                
            } catch let error as NSError {
                print("AuditMode.json data couldn't be written after update // File not exist")
                print(error.description)
            }
        }
        
        
        //Reading from JsonDictionary File stored in documents directory
        do{
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath.path, encoding: String.Encoding.utf8.rawValue) as String
            //                    print("readString=\(readString)")
            let readStringParsed = parseJSON(readString)
            print("AuditorMode in AuditComments_readStringParsed = ",readStringParsed)
        }
        catch let error as NSError{
            print("There is an error while reading from JsonDictionary File.",error.description)
            
        }
        
        
        
        
        
    }// end of func
    
    
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    
    func JSON_To_FoundationObj(_ jsonString: String) -> NSMutableDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSMutableDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    
    func parseCommentsJSON(_ jsonString: String) -> [String:String] {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:String] {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // CAP
        if segue.identifier == "CAP"{
            
            let destinationVC = segue.destination as! CorrectiveActionPlanViewController
            
            //destinationVC.arrayOfDefectsLoggedRecieved_FromCommentsView = self.arrayOfDefectsLoggedRecieved!
            destinationVC.AuditCodeIn = self.AuditCode.text!
            destinationVC.AuditStageIn = self.AuditStage.text!
            destinationVC.AuditStageColor = self.AuditStage.backgroundColor
            destinationVC.ReportID = self.ReportID_against_current_audit
        }
        
        
        //goToAuditorModeViewC
        
        //goto Final comments
        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
