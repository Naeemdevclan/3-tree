//
//  ScheduleAuditViewController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 15/03/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

var DefectsColorsData0 = [IndexPath:String]()
var DefectsSizesData0 = [IndexPath:String]()


class ScheduleAuditViewController: UIViewController, UITabBarDelegate,UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate, UIScrollViewDelegate, SavingViewControllerDelegate,SavingVendorViewControllerDelegate {
    
    
//    @IBOutlet weak var BackgroundBlackView: UIView!
    @IBOutlet weak var AuditScheduleView: UIStackView!
    
    @IBOutlet weak var posHeightCntsraint: NSLayoutConstraint!
    @IBOutlet weak var tabBar2: UITabBar!
    @IBOutlet weak var tabBar1: UITabBar!
    
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!

    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!

    @IBOutlet weak var SAScrollView: UIScrollView!
    
//    @IBOutlet weak var productionAudit: UIButton!
    @IBOutlet weak var selectReportType: UIButton!
    @IBOutlet weak var selectAuditType: UIButton!
    @IBOutlet weak var selectStage: UIButton!
   
    var stageDone = false

    @IBOutlet weak var selectVendor: UIButton!
    
    @IBOutlet weak var selectUnit: UIButton!
    
    @IBOutlet weak var POsView: UIView!
    @IBOutlet weak var Po_Text: UITextField!
    
    fileprivate var suggestions_TableView:UITableView?
    
    @IBOutlet weak var selectBrand: UIButton!
    
    @IBOutlet weak var selectStyleNo: UIButton!
    
    @IBOutlet weak var selectColor: UIButton!
    
    @IBOutlet weak var selectSize: UIButton!
    
    @IBOutlet weak var selectSampleSize: UIButton!
    
    @IBOutlet weak var selectLine: UIButton!
    
    @IBOutlet weak var selectDate: UIButton!
    
    @IBOutlet weak var selectTime: UIButton!
    @IBOutlet weak var selectEndTime: UIButton!
    
//  IBoutlets for setting data
    
//    @IBOutlet weak var AuditTypeLabel: UILabel!
    
    @IBOutlet weak var ReportType: UILabel!
    @IBOutlet weak var AuditType: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var endTimeLable: UILabel!
    
    @IBOutlet weak var stageLabel: UILabel!

    @IBOutlet weak var brandLabel: UILabel!
    
    @IBOutlet weak var unitLabel: UILabel!
    @IBOutlet weak var vendorLabel: UILabel!
    @IBOutlet weak var styleLabel: UILabel!
    @IBOutlet weak var colorsLabel: UILabel!
    
    @IBOutlet weak var sizesLabel: UILabel!
    @IBOutlet weak var sampleSizeLabel: UILabel!
    
    @IBOutlet weak var AQLLabel: UILabel!
    
    @IBOutlet weak var lblAQL: UILabel!
    @IBOutlet weak var productionLineLabel: UILabel!
    
    
    @IBOutlet weak var ReportTypeView: UIView!
    
    @IBOutlet weak var AuditTypeView: UIView!
    @IBOutlet weak var NewLineView: UIView!
    @IBOutlet weak var newLine: UITextField!
    
    @IBOutlet weak var SampleSizeView: UIView!
    @IBOutlet weak var OfferedQuantityView: UIView!
    @IBOutlet weak var offeredQuantity: UITextField!
    
    @IBOutlet weak var AQLLevelView: UIView!
    
    @IBOutlet weak var AQLView: UIView!
    @IBOutlet weak var VenderUnitView: UIView!
    
    @IBOutlet weak var ProductionLineView: UIView!
    
    //Naeem Added
    @IBOutlet weak var lblstyleNo: UILabel!
    
    @IBOutlet weak var imgStyleDArrow: UIImageView!
    @IBOutlet weak var lblColors: UILabel!
    
    @IBOutlet weak var imgColorDArrow: UIImageView!
    
    @IBOutlet weak var lblProductCode: UILabel!
    
    @IBOutlet weak var btnProductCode: UIButton!
    @IBOutlet weak var productCodeLabel: UILabel!
    
    @IBOutlet weak var imgProductCodeDArrow: UIImageView!
    
    @IBOutlet weak var itemNoView: UIView!
    @IBOutlet weak var btnItemNo: UIButton!
    @IBOutlet weak var itemNoLabel: UILabel!
    //for Levis only
    
    
    var auditCode = ""
    var auditEditMode = false

    var AuditReportType = ""
    var colorsArray = [String:AnyObject]()
    var sizesArray = [String:AnyObject]()

    
    @IBOutlet weak var btnSchedule: UIButton!
    var offlineDataLoaded = false
    
    var rescheduleAudit = [String:AnyObject]()

    var PickerArray:[String] = []
    var PickerArrayIDs:[String] = []
    
    var AuditStageArray:[String] = []
    var AuditStageArrayIDs:[String] = []
    
    var ReportTypeArray:[String] = []
    var ReportTypeArrayIDs:[String] = []
    
    var BrandArray:[String] = []
    var BrandArrayIDs:[String] = []

    var AQLArray:[String] = ["Level I","Level II","Level III","Level S-1","Level S-2","Level S-3","Level S-4"]
    var AQLArrayIDs:[String] = ["1","2","3","4","5","6","7"]
    
    var AQLData:[String] = ["1.5","2.5","4.0","6.5"]
    var AQLDataIDs:[String] = ["1","2","3","4"]

    
    var AuditTypeArray = ["Production Audit","Sampling Audit","Non-Production Audit"]
    var AuditTypeArrayIDs = ["PA","SA","NPA"]
    
    var selectVendorArray:[String]  = []
    var selectVendorArrayIDs:[String]  = []
    
    var selectVendorsKV:[String:String] = [:]
    
    
    var selectUnitArray:[String] = []// = ["SU1", "SU2", "SU3", "SU4", "SU5", "SU6", "SU7"]
    var selectUnitArrayIDs:[String] = []
    
//    var selectBrandVendorsArray:[String] = []
//    var selectBrandVendorsArrayIDs:[String] = []
    
    
    var selectStyleArray:[String] = []
    var selectStyleArrayIDs:[String] = []
    var styleSelectedPos = ""
    let selectSampleSizeArray = ["2", "3", "5", "8", "13", "20", "32", "50", "80", "125", "200", "315", "500", "800", "1250"]
   
    var selectLineArray:[String] = []
    var selectLineArrayIDs:[String] = []
    
    var ListOfPOs:[String:String] = [:]
// variable for calculating Units
    
//    var Units:[String] = []
//    var UnitsIDs:[String] = []
    
    var UnitsKV:[String:String] = [:]
    
    var VendorUnitsKV:[String:String] = [:]
   
    var BrandVendorsKV:[String:String] = [:]
    
//    var VendorUnits:[String] = []
//    var VendorUnitsIDs:[String] = []

    
    // - - - - - - - - Variables to get Saved Data - - - - - - - - - -
    
    var dictionary:NSDictionary!
    var jsonFilePath2:URL!
    var created:Bool!
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    
    var items: [String] = []
    var itemsID: [String] = []
    
    // - - - - - - Selected Values - - - - - - - - - -
    
    var selectedProductionAuditID:String!
    var selectedReportTypeID:String!
    var selectedAuditStage:String!
    var SelectedBrandID:String!
    var SelectedVendorID:String!
    var selectedUnitID:String!
    var selectedPoID:String!
    var selectedStyleID:String!
    var selectedAQLID:String!
    var selectedAQLDataID:String!
    var selectedColorsIDs:[String]!
    var colorsIDsString = ""      // without square brackets and last comma
    
    var selectedSizesIDs:[String]!
    var sizesIDsString = ""      // without square brackets and last comma
    
    // use "self.sampleSizeLabel.text!" for selectedSampleSize

    var selectedLine:String!
    
    // use "self.dateLabel.text" for selectedDate
    // use "self.timeLabel.text" for selectedTime

    // - - - - - -Request bodydata- - - - - - -
    
    var bodyData:String!
    var bodyDataForPOs:String!
    
    var autoCompletePossibilities:[String] = []//["317155", "317067","317168","317155", "wand", "wizard", "test1", "test2", "test3", "test4", "test5", "test6", "test7", "test8"]
//    var autoCompletePossibilitiesKeys:[String] = []
    var autoComplete = [String]()
    var autoCompleteKeys = [String]()

    var PoKeysToSend = ""
    var poSelectedFromSuggesstions = false
    
    
//    func myTargetFunction(textField: UITextField) {
//        // user touch field
//        
//        if SelectedBrandID != nil && SelectedVendorID != nil {
//            Po_Text.userInteractionEnabled = true
//            print("selectBrandID and SelectVendorID is not nil")
//            
//        }else{
//            print("First select Brand and Vendor")
//            Po_Text.userInteractionEnabled = false
//        }
//    }

    
    
    var strLabel = UILabel()
    var messageFrame = UIView()
    var activityIndicator = UIActivityIndicatorView()
 
    var EditAudit = ""
    
    override func viewDidLoad() {
        
        if let userType = defaults.object(forKey: "UserType"){
            if("\(userType as! String)" == "MATRIX" || "\(userType as! String)" == "TRIPLETREE"){
                // audit_Item = "BOOKING"
                AuditTypeView.isHidden = false
                ReportTypeView.isHidden = false
                
                lblProductCode.isHidden = true
                btnProductCode.isHidden = true
                productCodeLabel.isHidden = true
                itemNoView.isHidden = true
                posHeightCntsraint.constant = 192
                VenderUnitView.isHidden = false
                saveAuditTypeText("Production Audit", strID: "PA")


                
            }else if("\(userType as! String)" == "LEVIS"){
                
                ReportTypeView.isHidden = true
                ProductionLineView.isHidden = true
                itemNoView.isHidden = false
                
               // AuditType.text = "select Audit Type"
                ReportTypeView.isHidden = true
                AuditTypeView.isHidden = false
                lblstyleNo.isHidden = true
                selectStyleNo.isHidden = true
                styleLabel.isHidden = true
                imgStyleDArrow.isHidden = true
                
                lblColors.isHidden = true
                selectColor.isHidden = true
                colorsLabel.isHidden = true
                imgColorDArrow.isHidden = true
                
                lblProductCode.isHidden = false
                btnProductCode.isHidden = false
                productCodeLabel.isHidden = false
                VenderUnitView.isHidden = true

                
            }
            else{
                
                saveAuditTypeText("Production Audit", strID: "PA")

                posHeightCntsraint.constant = 192
                VenderUnitView.isHidden = false
                itemNoView.isHidden = true

                ReportTypeView.isHidden = false

                lblProductCode.isHidden = true
                btnProductCode.isHidden = true
                productCodeLabel.isHidden = true
                
                lblstyleNo.isHidden = false
                selectStyleNo.isHidden = false
                styleLabel.isHidden = false
                imgStyleDArrow.isHidden = false
                
                lblColors.isHidden = false
                selectColor.isHidden = false
                colorsLabel.isHidden = false
                imgColorDArrow.isHidden = false
                // audit_Item = "SCHEDULE AUDIT"
                AuditTypeView.isHidden = true
                print(userType)
            }
        }

        super.viewDidLoad()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
//        let screenSize = UIScreen.mainScreen().bounds.size
        suggestions_TableView = UITableView(frame: CGRect(x: self.POsView.frame.origin.x, y: self.POsView.frame.origin.y + self.POsView.frame.height-5, width: self.Po_Text.frame.width, height: 100))
//        suggestions_TableView!.dataSource = self
//        suggestions_TableView!.delegate = self
        suggestions_TableView?.rowHeight = 25
        suggestions_TableView!.isHidden = true
       // AQLLevelView.isHidden = false
        
        
       // AQLLevelView.backgroundColor = UIColor.red
        SAScrollView.addSubview(suggestions_TableView!)
        
      //  Po_Text.addTarget(self, action: "myTargetFunction:", forControlEvents: UIControlEvents.TouchDown)
        
        
//        suggestions_TableView.hidden = true
//        suggestions_TableView!.delegate = self
//        suggestions_TableView!.dataSource = self
        
//       UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        // Here. removes all the data before loading so everything clears again.
        // ??????
        
        
        // disable userInteraction on selectColors and SelectSizes Btns
        
        self.selectColor.isUserInteractionEnabled = false
        self.selectSize.isUserInteractionEnabled = false
        
        // Hide the newline in start
        NewLineView.isHidden = true
//        newLine.hidden = true
        OfferedQuantityView.isHidden = true
        AQLView.isHidden = true
        print("newloadSchedule")
        self.colorsLabel.sizeToFit()
        
//        let scrollSize = CGSizeMake(AuditScheduleView.frame.width, AuditScheduleView.frame.height)
//        SAScrollView.contentSize = scrollSize
        
//        SAScrollView.contentSize.height = 800
//        SAScrollView.contentSize.width = 375
        
        // Do any additional setup after loading the view.
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("login.json")
        jsonLoginFilePath = jsonFilePath
//        print(jsonLoginFilePath.path)
        
//        do {
////            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
//            
//            var readString: String
//            readString = try NSString(contentsOfFile: jsonLoginFilePath.path!, encoding: NSUTF8StringEncoding) as String
//            print(readString)
//            self.showStagesDataWhenOffline(readString)
//            self.showReportTypeDataWhenOffline(readString)
//            self.showBrandsDataWhenOffline(readString)
//            self.showVendorsDataWhenOffline(readString)
//            
//            self.showUnits(readString)
//            self.showVendorUnits(readString)
//            
//            self.showBrandVendors(readString)
//            
//        } catch let error as NSError {
//            print(error.description)
//        }

// - - - - - setting outlets of this class- - - - - - - - -
        
        Po_Text.delegate = self
        //initally disable the userInteraction of Po_Text Field
       // Po_Text.userInteractionEnabled = false
        newLine.delegate = self
        offeredQuantity.delegate = self
        
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Selected Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)

        
        
//        self.applyGradient(productionAudit)
        self.applyGradient(selectReportType)
        self.applyGradient(selectAuditType)
        
        self.applyGradient(selectStage)
        self.applyGradient(btnItemNo)
        self.applyGradient(selectVendor)
        self.applyGradient(selectUnit)
        self.applyGradient(selectBrand)
        self.applyGradient(selectStyleNo)
        self.applyGradient(btnProductCode)
        self.applyGradient(selectColor)
        self.applyGradient(selectSize)
        self.applyGradient(selectSampleSize)
        
        self.applyGradient(selectLine)
        self.applyGradient(selectDate)
        self.applyGradient(selectTime)
        self.applyGradient(selectEndTime)
        
        // - - - - add notifications to current view    keyboardWillShow:
        
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
//        
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
//        
        
        self.addDoneButtonOnKeyboard()

        // - - - -

//        var SavedUserType = "\(defaults.objectForKey("UserType")as! String)"
//        self.DoActionAgainstCurrentUserReportType(SavedUserType)
//        
    }
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(ScheduleAuditViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.offeredQuantity.inputAccessoryView = doneToolbar
        
    }
    
    func doneButtonAction()
    {
        self.offeredQuantity.resignFirstResponder()
    }
    // adjust the screen size when keyboard appears and hides
    
    func keyboardWillHide(_ sender: Notification) {
        let userInfo: [AnyHashable: Any] = sender.userInfo!
        let keyboardSize: CGSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue.size
        self.view.frame.origin.y += keyboardSize.height
    }
    
    func keyboardWillShow(_ sender: Notification) {
        let userInfo: [AnyHashable: Any] = sender.userInfo!
        let keyboardSize: CGSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue.size
        let offset: CGSize = (userInfo[UIKeyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size
        
        if keyboardSize.height == offset.height {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y -= keyboardSize.height
            })
        } else {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y += keyboardSize.height - offset.height
            })
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
        Po_Text.resignFirstResponder()
    }
    
    
    
    // - - Handling Po Start - -
    
/*
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let substring = (Po_Text.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
        
        searchAutocompleteEntriesWithSubstring(substring)
        
        return true
        
    }
*/
    
    func searchAutocompleteEntriesWithSubstring(_ substring: String)
    {
       // autoComplete.removeAll(keepingCapacity: false)
        //autoCompleteKeys.removeAll(keepingCapacity: false)
        self.autoComplete = []
        print("self.autoComplete in before settting values =",self.autoComplete)
        print(autoCompletePossibilities)
        for value in autoCompletePossibilities
        {
            let myString:NSString! = value as NSString
            print(myString)
         /* let substringRange :NSRange! = myString.rangeOfString(substring)  // it gives Location and Length of substring
            print("substringRange = ",substringRange)
            print("substringRange.location = ",substringRange.location)
            if (substringRange.location  == 0)
            {
                autoComplete.append(value)
            }  */
            
            
//            let substringRangeLowerCase :NSRange! = myString.range(of: substring.lowercased())  // it gives Location and Length of substring
//            let substringRangeUpperCase :NSRange! = myString.range(of: substring.uppercased())  // it gives Location and Length of substring
//
//            print("substringRangeLowerCase = ",substringRangeLowerCase)
//            print("substringRangeUpperCase = ",substringRangeUpperCase)
//            print("substringRangeLowerCase.length =",substringRangeLowerCase.length)
//            print("substringRangeUpperCase.length =",substringRangeUpperCase.length)
            if(selectedReportTypeID == "46"){
                let sbStr = substring.replacingOccurrences(of: ",", with: "")
                if ((myString.lowercased).hasPrefix(sbStr)&&sbStr.characters.count>=2)
                {
                    autoComplete.append(value)
                }
            }else{
            let sbStr = substring.replacingOccurrences(of: ",", with: "")
            if ((myString.lowercased).hasPrefix(sbStr)&&sbStr.characters.count>=2)
            {
                autoComplete.append(value)
            }
            }
        }
        
        
        
//        for(NSString *curString in elementArray) {
//            NSRange substringRangeLowerCase = [curString rangeOfString:[substring lowercaseString]];
//            NSRange substringRangeUpperCase = [curString rangeOfString:[substring uppercaseString]];
//            
//            if (substringRangeLowerCase.length != 0 || substringRangeUpperCase.length != 0) {
//                [autoCompleteArray addObject:curString];
//            }
//        }
        print("autoComplete = ",autoComplete)
//        for key in autoCompletePossibilitiesKeys
//        {
//            let myString:NSString! = key as NSString
//            
//            let substringRange :NSRange! = myString.rangeOfString(substring)
//            
//            if (substringRange.location  == 0)
//            {
//                autoCompleteKeys.append(key)
//            }
//        }
//        print("autoCompleteKeys = ",autoCompleteKeys)
        suggestions_TableView!.reloadData()
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "autocompleteCellIdentifier"
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        
        if cell == nil{
            
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
        }
        
        cell?.textLabel?.font = UIFont.systemFont(ofSize: 12)
        cell?.textLabel?.textColor = UIColor.black
        let index = indexPath.row as Int
        cell?.textLabel!.text = autoComplete[index]
        
        cell?.contentView.gestureRecognizers = nil
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return autoComplete.count
        
    }
    
    var recentlySelectedPo:[String] = []//""
    var recentlySelectedPoKey:[String] = []//""
//    var rangeOfrecentlySelectedPo = Range(0.advancedBy(1)..<2)
    var POText:String = ""
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(POText)
        let selectedCell: UITableViewCell = tableView.cellForRow(at: indexPath)!
        
        self.substring.removeAll()
        ArrayOFsubstring.removeAll()
        
        print("substring in didSelectRow = ",recentlySelectedPo)
        if !(recentlySelectedPo.contains((selectedCell.textLabel?.text)!)){
//            x = []
            recentlySelectedPo.append((selectedCell.textLabel?.text)!) //= (selectedCell.textLabel?.text)!
            if(recentlySelectedPo.count<=1){
            POText.append((selectedCell.textLabel?.text)!)
            }
            else{
                POText.append("," + (selectedCell.textLabel?.text)! )
            }
                    //now save its key
            for PO_KV in ListOfPOs{
                if selectedCell.textLabel!.text! == PO_KV.1 {
                    
                    if !(PoKeysToSend.contains(PO_KV.0)){
                        if(recentlySelectedPo.count>1){
                            PoKeysToSend.append(",")
                        }
                        PoKeysToSend.append(PO_KV.0)
                        
                        poSelectedFromSuggesstions = true
                        
                        recentlySelectedPoKey.append(PO_KV.0) //= PO_KV.0
                        print("recentlySelectedPoKey.append(PO_KV.O) =",recentlySelectedPoKey)
                        print("recentlySelectedPoKey.last = ",recentlySelectedPoKey.last)
                    }
                }
            }
//            if(auditEditMode == true){
//                var possss = POText.components(separatedBy: ",")
//                print(possss)
//                var coltext = [String]()
//                var txtF = Po_Text.text!.components(separatedBy: ",")
//                 print(txtF)
//                for item in 0..<possss.count{
//                    if(!txtF.contains(possss[item])){
//                        txtF.append(possss[item])
//                    }
//                }
//                print(possss)
                Po_Text.text =  POText

                //if(!Po_Text.text!.contains(POText)){
                //}
//            }else{
//                Po_Text.text =  POText
// 
//            }
            print("PoKeysToSend =",PoKeysToSend)
            print("Po_Text.text = ",Po_Text.text!)
        }
//        POText.appendContentsOf((selectedCell.textLabel?.text)!)

//        Po_Text.text = POText   //selectedCell.textLabel!.text!
       
        
//        let arrayOfCharactersInPoText = Array(selectedCell.textLabel!.text!.characters)
//        print("arrayOfCharactersInPoText = ",arrayOfCharactersInPoText)
//        x.removeAll()
//        for c in arrayOfCharactersInPoText{
//            x.append("\(c)")
//        }
//        recentlySelectedPo = x
//        print("po text in form of array stored in x = ",x)
        
        
        self.suggestions_TableView?.isHidden = true
        
//        for PO_KV in ListOfPOs{
//            if selectedCell.textLabel!.text! == PO_KV.1 {
//                
//                if !(PoKeysToSend.containsString(PO_KV.0)){
//                    PoKeysToSend.appendContentsOf(PO_KV.0)
//                    PoKeysToSend.appendContentsOf(",")
//                    poSelectedFromSuggesstions = true
//                }
//            }
//        }
//        print("PoKeysToSend =",PoKeysToSend)
        //if autoCompletePossibilitiesKeys.contains(<#T##element: String##String#>
        
        autoComplete.removeAll(keepingCapacity: false)
        
        
    }
// - - Handling Po ENDs -
    
    override func viewWillAppear(_ animated: Bool) {
//        var SavedUserType = "\(defaults.object(forKey: "UserType")as! String)"
//        self.DoActionAgainstCurrentUserReportType("hvh")

        
        print("schedule appeared!")
        // - - populating data on SizesLabel- - - - - - - -
        if !DefectsSizesData0.isEmpty{
            if !SizesData.isEmpty && !SizesDataID.isEmpty{
                
                self.selectedSizesIDs = []
                for id in SizesDataID{
                    print(id.1)
                    print("-------")
                    
                    self.selectedSizesIDs.append(id.1)// = id.1
                }
                
                // now convert selectedColorsIDs to a simple string format
                sizesIDsString = ""
                
                for i in 0 ..< selectedSizesIDs.count{
                    sizesIDsString = sizesIDsString+selectedSizesIDs[i]
                    
                    if i < self.selectedSizesIDs.count - 1 {
                        sizesIDsString = sizesIDsString+","
                    }
                    
                }
                print("sizesIDsString = \(sizesIDsString)")
                sizesLabel.text = ""
                for d in SizesData{
                    print(d.1)
                    print("-------")
                    //                self.PICK_DEFECT_AREA_TEXT = d.1
                    sizesLabel.text = sizesLabel.text!+d.1+","
                }
                SizesDataID.removeAll()
                SizesData.removeAll()
            }
        }
        else{
            sizesLabel.text = "Select Size(s)"
            sizesIDsString = ""
        }
        
        
// - - - - - - - populating data on selectColors- - - - - - - - 
        if !DefectsColorsData0.isEmpty{
            if !ColorsData.isEmpty && !ColorsDataID.isEmpty{
                //            PICK_DEFECT_AREA.text = ""
                //            self.PICK_DEFECT_AREA_TEXT = ""
                
                self.selectedColorsIDs = []
                for id in ColorsDataID{
                    print(id.1)
                    print("-------")
                    
                    self.selectedColorsIDs.append(id.1)// = id.1
                }
                // now convert selectedColorsIDs to a simple string format
                colorsIDsString = ""
                
                for i in 0 ..< selectedColorsIDs.count{
                    colorsIDsString = colorsIDsString+selectedColorsIDs[i]
                    
                    if i < self.selectedColorsIDs.count - 1 {
                        colorsIDsString = colorsIDsString+","
                    }
                    
                }
                print("colorsIDsString = \(colorsIDsString)")
                colorsLabel.text = ""
                for d in ColorsData{
                    print(d.1)
                    print("-------")
                    //                self.PICK_DEFECT_AREA_TEXT = d.1
                    colorsLabel.text = colorsLabel.text!+d.1+","
                }
                ColorsDataID.removeAll()
                ColorsData.removeAll()
                
                let a = Array(self.selectedColorsIDs)
                print("self.selectedColorsIDs = \(a)")
                
            }
        }
        else{
            colorsLabel.text = "Select Color(s)"
            colorsIDsString = ""
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        EditAudit = ""
    }
    override func viewDidAppear(_ animated: Bool) {
        
        
        if(EditAudit == "Edit"){
            self.progressBarDisplayer("Loading. . .", true)
            }else{
            }
        print("schedule appeared2!")
        if offlineDataLoaded == false{
            
            self.dateLabel.text = Date().formatted
            self.timeLabel.text = Date().formattedTime
            self.endTimeLable.text = Date(timeIntervalSinceNow: 120).formattedTime
            do {
               
                var readString: String
                readString = try NSString(contentsOfFile: jsonLoginFilePath.path, encoding: String.Encoding.utf8.rawValue) as! String
                print(readString)
                
                let one = BlockOperation() {
                    NSLog("one")
                   // self.showStagesDataWhenOffline(readString)
                    self.showReportTypeDataWhenOffline(readString)
                }
                one.completionBlock = {
                    NSLog("one completion")
                }
                
                let two = BlockOperation() {
                    NSLog("two")
                    self.showBrandsDataWhenOffline(readString)
                    self.showVendorsDataWhenOffline(readString)
                }
                two.completionBlock = {
                    NSLog("two completion")
                }
                
                two.addDependency(one)
                
                let three = BlockOperation() {
                    NSLog("three")
                    self.showUnits(readString)
                    self.showVendorUnits(readString)
                }
                three.completionBlock = {
                    NSLog("three completion")
                }
                
                three.addDependency(two)
                
                let four = BlockOperation() {
                    NSLog("three")
                    self.showBrandVendors(readString)
                }
                
                four.completionBlock = {
                    NSLog("four completion")
                }
                
                four.addDependency(three)
                
                let queue = OperationQueue()
                queue.addOperations([one, two, three, four], waitUntilFinished: true)
                
            } catch let error as NSError {
                print(error.description)
            }
            
            offlineDataLoaded = true
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        if(EditAudit == "Edit"){
            
            auditEditMode = true
            btnSchedule.setTitle("Update Schedule", for: .normal)
            self.performAuditDetailRequest(self.getUrlForAuditDetail())

//            let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
//            
//            let jsonFilePath = documentsDirectoryPath.appendingPathComponent("AuditMode.json")
//            jsonFilePath2 = jsonFilePath
//            do {
//                var readString: String
//                readString = try NSString(contentsOfFile: jsonFilePath2.path, encoding: String.Encoding.utf8.rawValue) as String
//                print("AuditMode.json = \(readString)")
//                
//                self.showDataWhenOffline(readString)
//                
//            } catch let error as NSError {
//                print(error.description)
//            }
            print(rescheduleAudit)
            //saveReportText
        }
        if let userType = defaults.object(forKey: "UserType"){
            if("\(userType as! String)" == "LEVIS"){
                // audit_Item = "BOOKING"
                //AuditTypeView.isHidden = false
                self.showOfflineAuditTypes()
            }else{
                
            }}


    }
    func showOfflineAuditTypes(){
        saveReportText("Levis Bottoms",strID: "45")
        ReportTypeView.isHidden = true
        self.showStagesDataWhenOffline(ReportType.text!, strID: "45")
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("login.json")
        jsonLoginFilePath = jsonFilePath
        print(jsonLoginFilePath.path)
        
        do {
            var readString: String
            readString = try NSString(contentsOfFile: jsonLoginFilePath.path, encoding: String.Encoding.utf8.rawValue) as String
            print(readString)
            self.loadAuditTypes(readString)
        } catch let error as NSError {
            print(error.description)
        }

    }
    func loadAuditTypes(_ savedData:String?){
        if savedData != nil {
            
           // UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            self.dictionary = self.parseJSON(savedData!)
            print(self.dictionary)
            let AuditType = self.dictionary["AuditTypes"]! as! [AnyObject]
            AuditTypeArray.removeAll()
            AuditTypeArrayIDs.removeAll()
            for type in AuditType{
                let aType =  type as! [String:AnyObject]
                AuditTypeArray.append(aType["type"] as! String)
                AuditTypeArrayIDs.append(aType["id"] as! String)
            }
            }
    }
        
    ///Offline data Show
    var selectedPosTextData = ""
    func showDataWhenOffline(_ savedData:String?){
        
        if savedData != nil {
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            self.dictionary = self.parseJSON(savedData!)
            let Audit = self.dictionary["Schedule"]! as! [String : AnyObject]
                        
            //            self.totalRowsInTable = self.dictionary["Audits"]!.count
            print("All_Audits count = \(Audit.count)")
            if Audit.count > 0{
                        print("Actual Audit===" ,Audit)
                
//Selected ReportType
                        let indexOfA = ReportTypeArray.index(of:showReportType(reportID: Audit["ReportType"] as! String))
                        saveReportText(showReportType(reportID: Audit["ReportType"] as! String),strID: ReportTypeArrayIDs[indexOfA!])
                        
//Selected Stage
                      let stageIndex = AuditStageArrayIDs.index(of: Audit["AuditStage"] as! String)
                        saveText(AuditStageArray[stageIndex!], strID: Audit["AuditStage"] as! String)
                
    //selected Audit type
                if let userType = defaults.object(forKey: "UserType"){
                    if("\(userType as! String)" == "LEVIS"){
                        if let audittype = Audit["AuditType"]{
                            let typeId = audittype as! String
                            
                            let typeIndex = AuditTypeArrayIDs.index(of: typeId)
                            saveAuditTypeText(AuditTypeArray[typeIndex!], strID: typeId)
                        }

                        // audit_Item = "BOOKING"
                        //AuditTypeView.isHidden = false
                        //self.showOfflineAuditTypes()
                    }else{
                        
                    }}

                 // selected Colors
                if(Audit["ColorsList"] is [String:AnyObject]){
                colorsArray = Audit["ColorsList"] as! [String:AnyObject]
                }
                if( Audit["SizesList"] is [String:AnyObject]){
                let sizesList = Audit["SizesList"] as! [String:AnyObject]
                let SText = Audit["Sizes"] as! String
                print(SText)
                //sizesArray
                let Sizes = SText.components(separatedBy: ",")
                //for item in 0..<sizesList.count{
                    for (key, value) in sizesList {
                        for item in  0..<Sizes.count{
                        if(value as! String == Sizes[item]){
                            sizesArray[key] = value as! String as AnyObject?
                            //SizesValues.append(value as! String)
                        }
                        print("\(key) -> \(value)")
                    }
                }
                }
                
                print(sizesArray)

                
     
//Selected Brand


                    let brandIndex = BrandArrayIDs.index(of:Audit["Brand"] as! String)
                        saveBrandText(BrandArray[brandIndex!], strID: Audit["Brand"] as! String)

//Selected Vender
                        
                        
                        let venderIndex = selectVendorArrayIDs.index(of: Audit["Vendor"] as! String)
                        saveVendorText(selectVendorArray[venderIndex!], strID: Audit["Vendor"] as! String)
 //Selected Style
                styleSelectedPos = Audit["Style"] as! String
                let stylelist =  Audit["StylesList"] as! [String:AnyObject]
               // styleLabel.text = styleSelectedPos
               // self.styleLabel.text =
                for (key, value) in stylelist{
                self.saveStyleTextEdit(value as! String, strID:key)
                }

  //Selected Unit
//                print(selectUnitArrayIDs)
//                print(selectUnitArray)
//                let unitIndex = selectUnitArrayIDs.index(of: Audit["Unit"] as! String)
//                saveUnitText(selectUnitArray[unitIndex!], strID:Audit["Unit"] as! String )
                //selected line
                if(Audit["LinesList"] is [String:AnyObject]){
              let  linesArray = Audit["LinesList"] as! [String:AnyObject]
print(linesArray)
                
                if let lineValue = Audit["Line"]{
                    if(lineValue as! String != "0"){
                    saveProductionLineText(linesArray[lineValue as! String] as! String, strID: lineValue as! String)
                    }
                }
                }
//selected product code
                
                if let ProductCode = Audit["ProductCode"]{
                    
                    self.productCodeLabel.text = ProductCode as? String
                }

//selected Pos
                let posText = Audit["Pos"] as! String
                print(posText)
                let actualPos = posText.components(separatedBy: ",")
                print(actualPos)
                var posValues = [String]()
                if(Audit["PosList"] is [String:AnyObject] ){
                let posArray = Audit["PosList"] as! [String:AnyObject]
                    print(posArray)
                for item in 0..<actualPos.count{
                for (key, value) in posArray {
                    if(actualPos[item] != ""){
                    if(key == actualPos[item]){
                        posValues.append(value as! String)
                    }
                    }
                    print(posValues.joined(separator: ","))
                    print("\(key) -> \(value)")
                }
                    
                }
                }
                PoKeysToSend =  posText
                print(PoKeysToSend)
                recentlySelectedPoKey = posText.components(separatedBy: ",")
                recentlySelectedPo = posValues
                POText = posValues.joined(separator: ",")
                 self.Po_Text.text = posValues.joined(separator: ",")


//                    if(posValues.count > 1 ){
//                        self.Po_Text.text = posValues.joined(separator: ",")
//                    }else{
//                        self.Po_Text.text = posValues[0]
//                    }
                   // self.SetUrl_and_GetColorsSizeStyles()

                //})
//
                self.SetUrl_and_GetColorsSizeStyles()

                
//Selected sample size
                            saveSampleSizeText(Audit["SampleSize"] as! String, strID: "")
                        
                        
// selected production line
                        print(AQLDataIDs)
                        if let aqlVlaue = Audit["Aql"]{
                            let AQLArrayIndex = AQLData.index(of: aqlVlaue as! String)
                            saveAQLData(aqlVlaue as! String, strID:"\(AQLArrayIndex)")
                        }
//selected Inspection Level
                //saveProductionLineText
                if let InspectionLevelVlaue = Audit["InspectionLevel"]{
                    let InspectionLevelArrayIndex = AQLArrayIDs.index(of: InspectionLevelVlaue as! String)
                    saveAQLText(AQLArray[InspectionLevelArrayIndex!], strID:InspectionLevelVlaue as! String)
                }
                
                        

//selected Sizes
//                        let sizeArray = Audit["SizesList"] as! [String:AnyObject]
//                        for (key, value) in sizeArray {
//                            let keyValue = Int(key)
//                            print("\(key) -> \(value)")
//                            print(Sizeitems)
//                            for item in 0..<Sizeitems.count{
//                                if(value as! String == Sizeitems[item]){
//                                    let indexPath = IndexPath(row: item, section: 0)
//                                    SizesData[indexPath] = value as? String
//                                    SizesDataID[indexPath] = "\(item)"
//                                  }
//                            }
//                        }
//                        DefectsSizesData0 = SizesData as [IndexPath : String]
//                        sizesLabel.text = Audit["Sizes"] as? String
//                        sizesIDsString = (Audit["Sizes"] as? String)!
//
                //selected sizes
                
                
                let SizesText = Audit["Sizes"] as! String
                print(SizesText)
                let SizesColors = SizesText.components(separatedBy: ",")
                //let colorsArray = Audit["PosList"] as! [String:AnyObject]
                var SizesValues = [String]()
                for item in 0..<SizesColors.count{
                    for (key, value) in sizesArray {
                        if(key == SizesColors[item]){
                            SizesValues.append(value as! String)
                        }
                        print("\(key) -> \(value)")
                    }
                }
                   sizesLabel.text = SizesText
               // sizesIDsString = SizesText
                
                //selected colors
                let colorsText = Audit["Colors"] as! String
                let actualColors = colorsText.components(separatedBy: ",")
                //let colorsArray = Audit["PosList"] as! [String:AnyObject]
                var colorsValues = [String]()
                for item in 0..<actualColors.count{
                    for (key, value) in colorsArray {
                        if(key == actualColors[item]){
                            colorsValues.append(value as! String)
                        }
                        print("\(key) -> \(value)")
                    }
                }
                colorsLabel.text = colorsText
                    colorsIDsString = colorsText
//colorsLabel.text = Audit["Colors"] as? String

                
// Selected Date
                        print(Audit["StartTime"] as! String)
                        self.dateLabel.text = Audit["AuditDate"] as? String
                        self.timeLabel.text = Audit["StartTime"] as? String
                self.endTimeLable.text = Audit["EndTime"] as? String


// Selected Colors

            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
            }else{
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                print("data count is less than 1")
            }
            
        }else{
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            print("data is nil")
        }
        
    }
    //end
    var poStirng = ""
    var ArrayOFsubstring:[String] = []
    var substring = ""
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        if textField == Po_Text{
            
            self.suggestions_TableView?.isHidden = false
            
            //        // for a moment disable the userInteraction of any other button.so that there is no fatal error: Array index out of range
            //        self.productionAudit.userInteractionEnabled = false
                    self.selectReportType.isUserInteractionEnabled = false
            //        self.selectStage.userInteractionEnabled = false
            //        self.selectBrand.userInteractionEnabled = false
            //        self.selectVendor.userInteractionEnabled = false
            //        self.selectUnit.userInteractionEnabled = false
            
            // For handling Po autocompletion Starts
            //        print("x = ",x)
            if !string.isEmpty{
                //            print("string isNotEmpty")
                //            x.append(string)
                print("substring in didchangecharacter = ",self.substring)
                
                substring.append(string)
                ArrayOFsubstring.append(string)
                
                //            print("x array =",x)
                //            var substringX = ""
                //            for xi in x {
                //                substringX.appendContentsOf(xi)
                //            }
                //            print("substringX =",substringX)
                print("range = ",range)
                print("ReplacementString =",string)
                //            substring = (Po_Text.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
                print("substring =",substring)
                searchAutocompleteEntriesWithSubstring(substring)
                //            searchAutocompleteEntriesWithSubstring(substringX)
                
                
            }else if string.isEmpty{
                
                print("string isEmpty")
                //            // clears the suggesstions when substring is empty
                //            if substring == ""{
                //                autoComplete.removeAll()
                //                suggestions_TableView?.hidden = true
                //            }
                
                // as I know substring is only be empty when we select any value from suggesstions
                
                if !ArrayOFsubstring.isEmpty{
                    ArrayOFsubstring.removeLast()
                    substring = ""
                    for g in ArrayOFsubstring{
                        substring.append(g)
                        
                        //After removing, Now show the list of pos in autocomplete table for the remaining substring
                        searchAutocompleteEntriesWithSubstring(substring)
                    }
                    //                substring = ArrayOFsubstring
                    print("do nothing Just remove the text!")
                    print("ArrayOFsubstring = ",ArrayOFsubstring)
                    print("substring = ",substring)
                    
                    if ArrayOFsubstring.isEmpty{
                        
                        // clears the suggesstions when substring is empty
                        autoComplete.removeAll()
                        suggestions_TableView?.reloadData()//.hidden = true
                    }
                    
                }else{
                    // clears the suggesstions when substring is empty
                    autoComplete.removeAll()
                    suggestions_TableView?.reloadData()//.hidden = true
                    print(POText,PoKeysToSend)
                    substring = ""
                    ArrayOFsubstring = []
                    if !recentlySelectedPo.isEmpty && !recentlySelectedPoKey.isEmpty{
                        print(POText)
                        if POText.contains(recentlySelectedPo.last!){
                            let totalCharactersInrecentlySelectedPo = recentlySelectedPo.last!.characters.count
                            print("totalCharactersInrecentlySelectedPo = ",totalCharactersInrecentlySelectedPo)
                            let rangeOfrecentlySelectedPo = POText.characters.index(POText.endIndex, offsetBy: -(totalCharactersInrecentlySelectedPo))..<POText.endIndex
                            print(recentlySelectedPo)
                            print("rangeOfrecentlySelectedPo = ",rangeOfrecentlySelectedPo)
                            print(POText.substring(with: rangeOfrecentlySelectedPo))
                            let strrr = POText.substring(with: rangeOfrecentlySelectedPo).replacingOccurrences(of: ",", with: "")
                            if(recentlySelectedPo.contains(strrr)||recentlySelectedPo.contains("," + POText.substring(with: rangeOfrecentlySelectedPo))){
                                if(recentlySelectedPo.count == 1){
                                    recentlySelectedPo.removeAll()
                                    POText = ""
                                }else{
                                    recentlySelectedPo.removeLast()
                                    POText.removeSubrange(rangeOfrecentlySelectedPo)
                                    POText.remove(at: POText.index(before: POText.endIndex))
                                }
                                                       print("now POText = ",POText)
                            print("POText = ",POText)
                            }
                            let a = POText
                            print("a=",a)
                            // if here we just set POText to self.Po_Text.text it cannot set at the same time text is removed from the textField. so we use dispatch_async to update the Po_Text later.
                            DispatchQueue.main.async(execute: { () -> Void in
                                self.Po_Text.text = a
                                print("self.Po_Text.text = ", self.Po_Text.text!)
                            })
                            
                            //recentlySelectedPo = ""
                            
                            autoComplete.removeAll(keepingCapacity: false)
                            self.suggestions_TableView?.isHidden = false
                        }
                        print("recentlySelectedPoKey.last! when deleting = ",recentlySelectedPoKey.last!)

                        if(recentlySelectedPoKey.last! == ""){
                            recentlySelectedPoKey.removeLast()
                        }
                        print("recentlySelectedPoKey.last! when deleting = ",recentlySelectedPoKey.last!)
                        if PoKeysToSend.contains(recentlySelectedPoKey.last!){
                            let totalCharactersInrecentlySelectedPoKey = recentlySelectedPoKey.last!.characters.count
                            print("totalCharactersInrecentlySelectedPoKey = ",totalCharactersInrecentlySelectedPoKey)
                            let rangeOfrecentlySelectedPoKey = PoKeysToSend.characters.index(PoKeysToSend.endIndex, offsetBy: -(totalCharactersInrecentlySelectedPoKey))..<PoKeysToSend.endIndex
                            
                            print("rangeOfrecentlySelectedPoKey = ",rangeOfrecentlySelectedPoKey)
                            if(recentlySelectedPoKey.count == 1){
                                recentlySelectedPoKey.removeAll()
                                PoKeysToSend = ""
                            }else{
                                recentlySelectedPoKey.removeLast()
                                PoKeysToSend.removeSubrange(rangeOfrecentlySelectedPoKey)
                                PoKeysToSend.remove(at: PoKeysToSend.index(before: PoKeysToSend.endIndex))
                            }
                            
                            print(PoKeysToSend)
                            //PoKeysToSend.removeSubrange(rangeOfrecentlySelectedPoKey)
                           

                            //recentlySelectedPoKey.removeLast()
                            
                        }
                        if(auditEditMode == true){
//                            print(Po_Text.text!)
//                            if(Po_Text.text! == ""){
//                                PoKeysToSend = ""
//                            }
                        }
                        print("PoKeysToSend when removing = ",PoKeysToSend)
                    }
                    
                }  //else part of if !substring.isEmpty
            }
            return true
        }else{
            return true
        }
    }
    
    
    
    func SetUrl_and_GetColorsSizeStyles(){
        
        if !self.SelectedBrandID.isEmpty && !self.SelectedVendorID.isEmpty {  // PoKeysToSend
            if(auditEditMode == true){
                //self.progressBarDisplayer("Loading. . .", true)
            }else{
            self.progressBarDisplayer("Loading. . .", true)
            }
            
            if(selectedPosTextData != ""){
               // self.Po_Text.text = selectedPosTextData

            }
            //if autoCompletePossibilities is not empty
            
            //                            let commaCount = self.Po_Text.text!.characters.filter{$0 == ","}.count
            print("poSelectedFromSuggesstions in SetUrl_and_GetColorsSizeStyles = ", poSelectedFromSuggesstions)
//            if !self.autoCompletePossibilities.isEmpty && poSelectedFromSuggesstions == true && auditEditMode == false {  // also add check if value is selected from didSelectCell
//                poSelectedFromSuggesstions = false
//                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&Pos=\(self.PoKeysToSend)&Brand=\(self.SelectedBrandID!)&Vendor=\(self.SelectedVendorID!)"
//                print("url for POS is selected")
//                
//            } //else if autoCompletePossibilities is empty
             if auditEditMode == false {  // self.autoCompletePossibilities.isEmpty ||
                
                // custom po added
                var posText = Po_Text.text!
                if(posText.characters.last! == "," ){
                    print(posText)
                   posText = posText.substring(to: posText.index(before: posText.endIndex))
                }
                
                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&Pos=0&Po=\(posText)&Brand=\(self.SelectedBrandID!)&Vendor=\(self.SelectedVendorID!)"
                print("url for po is selected")
//                self.Po_Text.userInteractionEnabled = false
                self.suggestions_TableView?.isHidden = true
            }
            else if(auditEditMode == true ){
                
               
                if(PoKeysToSend.hasPrefix(",")){
                    PoKeysToSend.remove(at: PoKeysToSend.startIndex)
                    //str.remove(at: str.startIndex)
                }
               // if(poSelectedFromSuggesstions == false){
                    var posText = Po_Text.text!
                    if(posText.characters.last! == "," ){
                        print(posText)
                        posText = posText.substring(to: posText.index(before: posText.endIndex))
                    }
                    self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&Pos=0&Po=\(posText)&Brand=\(self.SelectedBrandID!)&Vendor=\(self.SelectedVendorID!)&ReportType=\(selectedReportTypeID!)"
                    print("url for po is selected")
                    //                self.Po_Text.userInteractionEnabled = false
                    self.suggestions_TableView?.isHidden = true
//                }else{
//                     poSelectedFromSuggesstions = false
//                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&Pos=\(self.PoKeysToSend)&Brand=\(self.SelectedBrandID!)&Vendor=\(self.SelectedVendorID!)"
//                }
                print("url for POS is selected")
 
            }
//            else if(auditEditMode == true && poSelectedFromSuggesstions == false){
//                var posText = Po_Text.text!
//                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&Pos=0&Po=\(posText)&Brand=\(self.SelectedBrandID!)&Vendor=\(self.SelectedVendorID!)"
//                print("url for po is selected")
//
//            }
            print("bodyDataForRequestingPoInfo = \(self.bodyData!)")
            
            OperationQueue.main.addOperation {
                
                self.RequestForColorStyleSize(self.getPoInfoUrl())
//                self.Po_Text.resignFirstResponder()
            }
            //    remove all the data(colors,sizes,styles) related to po
            SizesData.removeAll()
            SizesDataID.removeAll()
            
            DefectsColorsData0.removeAll()
            DefectsSizesData0.removeAll()
            
            self.colorsLabel.text = "Select Color(s)"
            self.colorsIDsString = ""
            self.selectedColorsIDs = [] //.removeAll()
            
            self.sizesLabel.text = "Select Size(s)"
            self.sizesIDsString = ""
            self.selectedSizesIDs = [] //.removeAll()
            
            self.selectStyleArray.removeAll()
            self.selectStyleArrayIDs.removeAll()
            self.selectedStyleID = ""
            //self.styleLabel.text = "Select Style #"
            
        }
        // Now Enable the userInteraction of any other button.so that there is no fatal error: Array index out of range
        
//        self.productionAudit.userInteractionEnabled = true
        self.selectReportType.isUserInteractionEnabled = true
        
        self.selectStage.isUserInteractionEnabled = true
        self.selectBrand.isUserInteractionEnabled = true
        self.selectVendor.isUserInteractionEnabled = true
        self.selectUnit.isUserInteractionEnabled = true

        
    }
    
    func applyGradient(_ rect:UIButton){
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = rect.bounds
        gradientLayer.colors = [
            cgColorForRed(222.0, green: 222.0, blue: 222.0),
            cgColorForRed(218.0, green: 218.0, blue: 218.0),
            cgColorForRed(214.0, green: 214.0, blue: 214.0),
            cgColorForRed(210.0, green: 210.0, blue: 210.0),
            cgColorForRed(206.0, green: 206.0, blue: 206.0),
            cgColorForRed(202.0, green: 202.0, blue: 202.0),
            
            cgColorForRed(198.0, green: 198.0, blue: 198.0),
            
            cgColorForRed(194.0, green: 194.0, blue: 194.0),
            cgColorForRed(190.0, green: 190.0, blue: 190.0),
            cgColorForRed(186.0, green: 186.0, blue: 186.0),
            cgColorForRed(182.0, green: 182.0, blue: 182.0),
            cgColorForRed(177.0, green: 177.0, blue: 177.0)
        ]
        
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        
        rect.layer.addSublayer(gradientLayer)
        
    }
    
    
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        SAScrollView.contentSize.height = AuditScheduleView.frame.height+60
        
         suggestions_TableView?.frame.origin.x = self.POsView.frame.origin.x + 17
         suggestions_TableView?.frame.origin.y = self.POsView.frame.origin.y + 2*(self.Po_Text.frame.height) - 12

        //= UITableView(frame: CGRectMake(self.POsView.frame.origin.x, self.POsView.frame.origin.y + self.POsView.frame.height-5, self.Po_Text.frame.width, 100))
    }

//    func scrollViewDidScroll(scrollView: UIScrollView) {
//        if scrollView.contentOffset.x > 0{
//            scrollView.contentOffset.x = 0
//        }
//    }
    
    
    func cgColorForRed(_ red: CGFloat, green: CGFloat, blue: CGFloat) -> AnyObject {
        
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: 1.0).cgColor as AnyObject
    }
    
    
//    @IBAction func AuditTypeClicked(sender: AnyObject) {
//  
//        PickerArray = self.AuditTypeArray
//        PickerArrayIDs = AuditTypeArrayIDs
//        print("PickerArray = \(PickerArray)")
//        
//        let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("PopUpVC") as! PopUpViewController
//        
//        popupVC.delegate = self
//        
//        popupVC.pickerdata = PickerArray
//        popupVC.pickerdataIDs = PickerArrayIDs
//        
//        popupVC.WhichButton = "ProductionAuditType"
//        
//        popupVC.AllreadySelectedText = AuditTypeLabel.text!
//        
//        self.addChildViewController(popupVC)
//        popupVC.view.frame = self.view.frame
//        
//        self.view.addSubview(popupVC.view)
//        self.didMoveToParentViewController(self)
//        
//    }
    
    @IBAction func AuditTypeClick(_ sender: Any) {
        
        
                PickerArray = self.AuditTypeArray
                PickerArrayIDs = AuditTypeArrayIDs
                print("PickerArray = \(PickerArray)")
        
                let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as! PopUpViewController
        
                popupVC.delegate = self
        
                popupVC.pickerdata = PickerArray
                popupVC.pickerdataIDs = PickerArrayIDs
        
                popupVC.WhichButton = "Audit Types"
        
                popupVC.AllreadySelectedText = AuditType.text!
        
                self.addChildViewController(popupVC)
                popupVC.view.frame = self.view.frame
                
                self.view.addSubview(popupVC.view)
                self.didMove(toParentViewController: self)
    }
    @IBAction func ReportTypeClicked(_ sender: AnyObject) {
       
        PickerArray = self.ReportTypeArray
        PickerArrayIDs = ReportTypeArrayIDs
        
        print("PickerArray Report= \(PickerArray)")
        
        let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as! PopUpViewController
        
        popupVC.delegate = self
        popupVC.WhichButton = "ReportType"
        
        
        popupVC.pickerdata = PickerArray
        popupVC.pickerdataIDs = PickerArrayIDs
        
        popupVC.AllreadySelectedText = ReportType.text!
        
        self.addChildViewController(popupVC)
        popupVC.view.frame = self.view.frame
        
        self.view.addSubview(popupVC.view)
        self.didMove(toParentViewController: self)
        
 }
    
    @IBAction func selectVendorClicked(_ sender: AnyObject) {
      
        print("selectVendorArrayIn VENDOR CLICKED = \(selectVendorArray)")
      
        if !self.selectVendorArray.isEmpty && !self.selectVendorArrayIDs.isEmpty {
  
            PickerArray = self.selectVendorArray
            PickerArrayIDs = selectVendorArrayIDs
            
            print("PickerArray = \(PickerArray)")
            
            let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVendorsVC") as! PopUpVendorsViewController
            
            popupVC.delegate = self
            popupVC.WhichButton = "Select Vendors Name"
            
            
            popupVC.pickerdata = PickerArray
            popupVC.pickerdataIDs = PickerArrayIDs
            
            popupVC.AllreadySelectedText = vendorLabel.text!
            
            self.addChildViewController(popupVC)
            popupVC.view.frame = self.view.frame
            
            self.view.addSubview(popupVC.view)
            self.didMove(toParentViewController: self)
            
        }
        else{
            popupMessage("There is NO Vendor found against your selected Brand")
        }
    }
    
    
    @IBAction func selectUnitClicked(_ sender: AnyObject) {
        
        if !selectUnitArray.isEmpty && !selectUnitArrayIDs.isEmpty {
            
            PickerArray = self.selectUnitArray
            PickerArrayIDs = selectUnitArrayIDs
            
            print("PickerArray = \(PickerArray)")
            
            
            let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as! PopUpViewController
            
            popupVC.delegate = self
            popupVC.WhichButton = "UnitName"
            
            
            popupVC.pickerdata = PickerArray
            popupVC.pickerdataIDs = PickerArrayIDs
            
            popupVC.AllreadySelectedText = unitLabel.text!
            
            self.addChildViewController(popupVC)
            popupVC.view.frame = self.view.frame
            
            self.view.addSubview(popupVC.view)
            self.didMove(toParentViewController: self)

        }
    }
    
    @IBAction func selectStyleClicked(_ sender: AnyObject) {
        
        if !self.selectStyleArray.isEmpty && !self.selectStyleArrayIDs.isEmpty {
            
            PickerArray = self.selectStyleArray
            PickerArrayIDs = selectStyleArrayIDs
            
            print("PickerArray = \(PickerArray)")
            
            let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as! PopUpViewController
            
            popupVC.delegate = self
            popupVC.WhichButton = "StyleName"
            
            
            popupVC.pickerdata = PickerArray
            popupVC.pickerdataIDs = PickerArrayIDs
            
            popupVC.AllreadySelectedText = styleLabel.text!
            
            self.addChildViewController(popupVC)
            popupVC.view.frame = self.view.frame
            
            self.view.addSubview(popupVC.view)
            self.didMove(toParentViewController: self)
            
        }
    }
    
    @IBAction func selectSampleSizeClicked(_ sender: AnyObject) {
  
        PickerArray = self.selectSampleSizeArray
        print("PickerArray = \(PickerArray)")
        
        
        let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as! PopUpViewController
        
        popupVC.delegate = self
        
        popupVC.pickerdata = PickerArray
//        popupVC.pickerdataIDs = PickerArrayIDs
        
        popupVC.WhichButton = "SampleSize"
        
        popupVC.AllreadySelectedText = sampleSizeLabel.text!
        
        self.addChildViewController(popupVC)
        popupVC.view.frame = self.view.frame
        
        self.view.addSubview(popupVC.view)
        self.didMove(toParentViewController: self)
        
 }
    
    @IBAction func selectLineClicked(_ sender: AnyObject) {
      
        
        newLine.resignFirstResponder()
        Po_Text.resignFirstResponder()

        if !self.selectLineArray.isEmpty && !self.selectLineArrayIDs.isEmpty {
   
            PickerArray = self.selectLineArray
            PickerArrayIDs = selectLineArrayIDs
            
            print("PickerArray = \(PickerArray)")
            
            
            
            
            let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as! PopUpViewController
            
            popupVC.delegate = self
            
            popupVC.pickerdata = PickerArray
            popupVC.pickerdataIDs = PickerArrayIDs
            
            popupVC.WhichButton = "ProductionLine"
            
            popupVC.AllreadySelectedText = productionLineLabel.text!
            
            self.addChildViewController(popupVC)
            popupVC.view.frame = self.view.frame
            
            self.view.addSubview(popupVC.view)
            self.didMove(toParentViewController: self)
            
        }
        else{
            
            self.selectLineArray.append("Enter New Line")
            self.selectLineArrayIDs.append("new")
        /*
            self.selectLine.selected = true
            
            pickerView.delegate = self
        */
            
            PickerArray = self.selectLineArray
            PickerArrayIDs = selectLineArrayIDs
            
            print("PickerArray = \(PickerArray)")
            
            let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as! PopUpViewController
            
            popupVC.delegate = self
            
            popupVC.pickerdata = PickerArray
            popupVC.pickerdataIDs = PickerArrayIDs
            
            popupVC.WhichButton = "ProductionLine"
            
            self.addChildViewController(popupVC)
            popupVC.view.frame = self.view.frame
            
            self.view.addSubview(popupVC.view)
            self.didMove(toParentViewController: self)
            
        }
    }
    
    
    
    
    
    @IBAction func selectStageCliked(_ sender: AnyObject) {
        print("selectStage button is cliked.")
        
        PickerArray = AuditStageArray
        PickerArrayIDs = AuditStageArrayIDs
        
        print("PickerArray = \(PickerArray)")
        
        let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as! PopUpViewController
        
        popupVC.delegate = self
        
        popupVC.pickerdata = PickerArray
        popupVC.pickerdataIDs = PickerArrayIDs
        
        popupVC.WhichButton = "Stage"
        
        popupVC.AllreadySelectedText = stageLabel.text!
                
        self.addChildViewController(popupVC)
        popupVC.view.frame = self.view.frame
        
        self.view.addSubview(popupVC.view)
        self.didMove(toParentViewController: self)
        
    }
    func saveText(_ strText: String, strID: String) {
        
        stageLabel.text = strText
        selectedAuditStage = strID
    }
    func saveSelectedColor(_ strText : String, strID : String)
    {
        
    }
    
    func DoActionAgainstSelectedReportID(){

        print(selectedReportTypeID)
        // Show or Hide Offered Quantity TextField against Controlist
        if selectedReportTypeID == "28" {   // Controlist
            
            
            AQLArray = ["Level I","Level II"]
            AQLArrayIDs = ["1","2"]

            self.AQLLevelView.isHidden = false
            self.VenderUnitView.isHidden = true
            
            self.ProductionLineView.isHidden = true
            
            self.OfferedQuantityView.isHidden = false
            self.SampleSizeView.isHidden = true
            self.AQLView.isHidden = true

            
        }else if selectedReportTypeID == "36"||selectedReportTypeID == "38" {   //Hybrid Aparel
            if(selectedReportTypeID == "38"){
            AQLArray = ["First","Second"]
            AQLArrayIDs = ["1","2"]
            }
            else{
                AQLArray = ["Level I","Level II","Level III"]
                AQLArrayIDs = ["1","2","3"]

            }

            self.AQLLevelView.isHidden = false
            self.VenderUnitView.isHidden = true
            
            self.ProductionLineView.isHidden = true
            
            self.OfferedQuantityView.isHidden = false
            self.SampleSizeView.isHidden = true
            self.AQLView.isHidden = true

            
        }else if (selectedReportTypeID == "46"){
           // AuditTypeView.isHidden = true
            AQLLevelView.isHidden = true
            self.ProductionLineView.isHidden = true
             VenderUnitView.isHidden = true


        }
        else if (selectedReportTypeID == "45" || selectedReportTypeID == "44"){
            // AuditTypeView.isHidden = true
            AQLLevelView.isHidden = true
            self.ProductionLineView.isHidden = true
            VenderUnitView.isHidden = true
            
        }
        else if selectedReportTypeID == "14"||selectedReportTypeID == "34" {   //MGF OR MGF TEST
            
            AQLArray = ["Level I","Level II","Level III","Level S-1","Level S-2","Level S-3","Level S-4"]
            AQLArrayIDs = ["1","2","3","4","5","6","7"]

            self.AQLLevelView.isHidden = false
            self.VenderUnitView.isHidden = true
            
            self.ProductionLineView.isHidden = true
            
            self.OfferedQuantityView.isHidden = true
            self.SampleSizeView.isHidden = false
            self.AQLView.isHidden = false
            
        }else { // default
            self.AQLLevelView.isHidden = true
            self.VenderUnitView.isHidden = false
            
            self.ProductionLineView.isHidden = false
            
            self.OfferedQuantityView.isHidden = true
            self.SampleSizeView.isHidden = false
            self.AQLView.isHidden = true

            
        }
        
    }
    func saveAuditTypeText(_ strText : String, strID : String)
    {
        AuditType.text = strText
        selectedProductionAuditID = strID
        print(selectedProductionAuditID, AuditType.text!)
        // Show or Hide Offered Quantity TextField against Controlist
        
        
    }
    func saveReportText(_ strText: String, strID: String) {
        
        ReportType.text = strText
        selectedReportTypeID = strID
        print(selectedReportTypeID, ReportType.text!)
        // Show or Hide Offered Quantity TextField against Controlist
        self.showStagesDataWhenOffline(strText, strID: selectedReportTypeID)
        self.DoActionAgainstSelectedReportID()
        
    
        // Show or Hide Offered Quantity TextField against Controlist
//        if selectedReportTypeID == "28"{
//            
//            self.AQLLevelView.hidden = false
//            self.VenderUnitView.hidden = true
//            
//            self.ProductionLineView.hidden = true
//            
//            self.OfferedQuantityView.hidden = false
//            self.SampleSizeView.hidden = true
//        
//        }else{
//            self.AQLLevelView.hidden = true
//            self.VenderUnitView.hidden = false
//            
//            self.ProductionLineView.hidden = false
//            
//            self.OfferedQuantityView.hidden = true
//            self.SampleSizeView.hidden = false
//        }
    }
    var vendersDict = [String:String]()
    func saveBrandText(_ strText: String, strID: String) {
        
        //    remove all the data(colors, 
        PoKeysToSend = ""
        self.POText = ""
        self.Po_Text.text?.removeAll()
        
        
        SizesData.removeAll()
        SizesDataID.removeAll()
        
        Coloritems.removeAll()
        ColoritemsID.removeAll()
        
        Sizeitems.removeAll()
        SizeitemsID.removeAll()
        
        selectColor.isUserInteractionEnabled = false
        selectSize.isUserInteractionEnabled = false
        
        DefectsColorsData0.removeAll()
        DefectsSizesData0.removeAll()
        
        self.colorsLabel.text = "Select Color(s)"
        self.colorsIDsString = ""
        self.selectedColorsIDs = []
        
        self.sizesLabel.text = "Select Size(s)"
        self.sizesIDsString = ""
        self.selectedSizesIDs = []
        
        
        self.selectStyleArray.removeAll()
        self.selectStyleArrayIDs.removeAll()
        self.selectedStyleID = ""
        self.styleLabel.text = "Select Style #"
        self.productCodeLabel.text =  "Select product Code"
        
        //- - - - - - ---- - --- - - --
        
        
        SelectedBrandID = strID
        
        // - - - - -
        print("SelectedBrandID in select Brand =",SelectedBrandID)
        print("without filter BrandVendorsKV= \(BrandVendorsKV)")
        print("BrandVendorsKV.keys = in select Brand",BrandVendorsKV.keys)
        // now filter the BrandVendorsKV Pairs where key k == SelectedBrandID
        for kv in BrandVendorsKV{
            if kv.key == SelectedBrandID{
                print("kv.key =",kv.key)
                print("SelectedBrandID =",SelectedBrandID)
            }
        }
        let filtered = BrandVendorsKV.filter({ (k,v) -> Bool in
//            print("k ==",k)
//            print("selectedBrandID ==",SelectedBrandID)
//            print("v ==",v)
//            print("______")
            
            k == SelectedBrandID
        }) // returned value is an array of tuples like { (1:y),(2:w) }
        
        print("filtered BrandVendorsKV= \(filtered)")
        
        var stringOfBrandVendorsIDs = ""
        
        if !filtered.isEmpty{
            
            print("empty filtered")
            
            let (_, value) = filtered.first!
            //            print("value = \(value)")
            stringOfBrandVendorsIDs = value
            
        }else{
            // clears all the data related to vendors, Units and lines
            
            selectVendorArray.removeAll()
            selectVendorArrayIDs.removeAll()
            self.vendorLabel.text = "Select Vendor"
            self.SelectedVendorID = ""
            
            selectUnitArray.removeAll()
            selectUnitArrayIDs.removeAll()
            self.unitLabel.text = "Select Unit"
            self.selectedUnitID = ""
            
            selectLineArray.removeAll()
            selectLineArrayIDs.removeAll()
            self.productionLineLabel.text = "Select Line"
            self.selectedLine = ""
            NewLineView.isHidden = true
//            self.newLine.hidden = true
        }
        
        if !stringOfBrandVendorsIDs.isEmpty {
            
            print("stringOfBrandVendorsIDs =")
            print(stringOfBrandVendorsIDs)
            
            let arrayOfBrandVendorsIds = stringOfBrandVendorsIDs.components(separatedBy: ",")
            for i in arrayOfBrandVendorsIds{
                print(i)
            }
            
            // before initializing selectVendorArray first remove all items in it.
            
            selectVendorArray.removeAll()
            selectVendorArrayIDs.removeAll()
            self.vendorLabel.text = "Select Vendor"
            
            selectUnitArray.removeAll()
            selectUnitArrayIDs.removeAll()
            self.unitLabel.text = "Select Unit"
            self.selectedUnitID = ""
            
            selectLineArray.removeAll()
            selectLineArrayIDs.removeAll()
            self.productionLineLabel.text = "Select Line"
            self.selectedLine = ""
            NewLineView.isHidden = true
//            self.newLine.hidden = true
            
        
            let listOfKeysIn_selectVendorsKV = Array(selectVendorsKV.keys)
            print("listOfKeys in selectVendorsKV = \(listOfKeysIn_selectVendorsKV)")
            vendersDict.removeAll()
            for i in arrayOfBrandVendorsIds{
                print("i = \(i)")
                for j in 0 ..< listOfKeysIn_selectVendorsKV.count {
                    print("j = \(j)")
                    if i == listOfKeysIn_selectVendorsKV[j]{
                        print("i and j equals")
                        
                       vendersDict[selectVendorsKV[i]!] = i as String
                        
//                        selectVendorArray.append(selectVendorsKV[i]!)
//                        selectVendorArrayIDs.append(i)
                    }
                }
            }
            //Sorting Vendors
            
            
            var mynewDICT:[Int:[String:AnyObject]]! = [:]
            
            // Get the array from the keys property.
            let copy = vendersDict.keys
            // Sort from low to high (alphabetical order).
            let p = copy.sorted(by: <)
            print(p)
            
            for pi in p{
                print(pi)
            }
            let s = vendersDict.keys.sorted()
            print(s)
            // Loop over sorted keys.
            var i = 0
            for pin in p{
                for key in copy {
                    if key == pin{
                        // Get value for this key.
                        if let value = vendersDict[key] {
                            //                            mynewDICT[value as! String] = key
                            mynewDICT[i] = [value : key as AnyObject]
                            
                            selectVendorArray.append(key)
                            selectVendorArrayIDs.append(value)
//                            self.items.append(key)
//                            self.itemsID.append(value as! String)
//                            AuditStageArray.append(key)
//                            AuditStageArrayIDs.append(value as! String)
                            print("Key = \(key), Value = \(value)")
                            i+=1
                        }
                    }
                }}
            
            
            
            
            
            //End Sorting
            
            
            
            
            
            
            print("selectVendorArrayIDs = \(selectVendorArrayIDs)")
            print("selectVendorArray = \(selectVendorArray)")
            
            
        }
        else{
            //                for kv in selectVendorsKV{
            //                    selectVendorArray.append(kv.0)
            //                    selectVendorArrayIDs.append(kv.1)
            //                }
        }
        // - - - - -
        
        
        self.brandLabel.text = strText
        
        self.selectBrand.isSelected = false
        
        // Now Enable the userInteraction of any other button.so that there is no fatal error: Array index out of range
        
//        self.productionAudit.userInteractionEnabled = true
        self.selectReportType.isUserInteractionEnabled = true
        
        self.selectStage.isUserInteractionEnabled = true
        //            self.Po_Text.userInteractionEnabled = true
        self.selectVendor.isUserInteractionEnabled = true
        
    }
    func saveVendorSelected(_ strText: String, strID: String) {
        
        print(strText,strID)
        //    remove all the data(colors,sizes,styles) related to po
        self.ListOfPOs = [:]
        self.autoCompletePossibilities = []
        
        recentlySelectedPoKey.removeAll()
        selectedPoID = ""
        PoKeysToSend = ""
        self.POText = ""
        self.Po_Text.text?.removeAll()
        
        
        
        SizesData.removeAll()
        SizesDataID.removeAll()
        
        Coloritems.removeAll()
        ColoritemsID.removeAll()
        
        Sizeitems.removeAll()
        SizeitemsID.removeAll()
        
        selectColor.isUserInteractionEnabled = false
        selectSize.isUserInteractionEnabled = false
        
        DefectsColorsData0.removeAll()
        DefectsSizesData0.removeAll()
        
        self.colorsLabel.text = "Select Color(s)"
        self.colorsIDsString = ""
        self.selectedColorsIDs = []
        
        self.sizesLabel.text = "Select Size(s)"
        self.sizesIDsString = ""
        self.selectedSizesIDs = []
        
        
        self.selectStyleArray.removeAll()
        self.selectStyleArrayIDs.removeAll()
        self.selectedStyleID = ""
        self.styleLabel.text = "Select Style #"
        self.productCodeLabel.text =  "Select product Code"
        
        
        //- - - - - - ---- - --- - - --
        
        
        // reset unitsArray
        
        self.unitLabel.text = "Select Unit"
        selectedUnitID = ""
        selectUnitArray.removeAll()
        selectUnitArrayIDs.removeAll()
        
        // clears all the data related to lineArray because assuming that you want to change vendor
        
        selectLineArray.removeAll()
        selectLineArrayIDs.removeAll()
        self.productionLineLabel.text = "Select Line"
        self.selectedLine = ""
        NewLineView.isHidden = true
        //        self.newLine.hidden = true
        
        
        SelectedVendorID = strID
        
        
        // -POs -POs -     -POs -POs -POs -    -POs -POs -POs -POs -
        //send request for the list of POs to show in PO text field as suggesstions
//        if(selectedReportTypeID == "46"){
//
//        }else{
        bodyDataForPOs = "User=\(defaults.object(forKey: "userid")as! String)&Brand=\(SelectedBrandID!)&Vendor=\(SelectedVendorID!)&ReportType=\(selectedReportTypeID!)"
        //}
        print("bodyDataForPO=",bodyDataForPOs)
        self.performLoadDataRequestForPOs(self.getUrlForPOs())
        
        
        // - - -     - - - -    - - - - -
        //send request for getting line numbers
        
        self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&Vendor=\(SelectedVendorID!)"
        print("bodyData = \(self.bodyData)")
        
        self.performLoadDataRequestWithURL(self.getUrl())
        
        // - - - - -     - - - - -     - - - - -
        
        
        // now filter the vendorUnitsKeyValue Pairs where key k == SelectedVendorID
        
        let fil = VendorUnitsKV.filter({ (k,v) -> Bool in
            k == "\(SelectedVendorID)"
        }) // returned value is an array of tuples
        
        //            print("fil = \(fil)")
        
        var stringOfVendorIDs = ""
        
        if !fil.isEmpty{
            print("empty fil")
            let (_, value) = fil.first!
            //           print("value = \(value)")
            stringOfVendorIDs = value
            
        }else{
            
            selectUnitArray.removeAll()
            selectUnitArrayIDs.removeAll()
            self.unitLabel.text = "Select Unit"
            self.selectedUnitID = ""
        }
        
        //            for result in fil {
        //                newData[result.0] = result.1
        //                print("result.0 = \(result.0)")
        //                print("result.1 = \(result.1)")
        //                stringOfVendorIDs = result.1
        //            }
        
        if !stringOfVendorIDs.isEmpty {
            print("stringOfVendorIDs =")
            print(stringOfVendorIDs)
            
            let arrayOfVendorsIds = stringOfVendorIDs.components(separatedBy: ",")
            for i in arrayOfVendorsIds{
                print(i)
            }
            
            let listOfKeys = Array(UnitsKV.keys)
            print("listOfKeys = \(listOfKeys)")
            
            for i in arrayOfVendorsIds{
                print("i = \(i)")
                for j in 0 ..< listOfKeys.count {
                    print("j = \(j)")
                    if i == listOfKeys[j]{
                        print("i and j equals")
                        selectUnitArray.append(UnitsKV[i]!)
                        selectUnitArrayIDs.append(i)
                    }
                }
            }
            print("selectUnitArrayIDs = \(selectUnitArrayIDs)")
            print("selectUnitArray = \(selectUnitArray)")
            
        }
        else{
            // dlfjlsdglksglkdjgkdfjg
        }
        //            var filteredA = VendorUnitsKV.filter({$0.0 == "\(SelectedVendorID)"})
        //            print("filteredA= \(filteredA)")
        
        
        self.vendorLabel.text = strText
        
        self.selectVendor.isSelected = false
        
        // Now Enable the userInteraction of any other buttons.so that there is no fatal error: Array index out of range
        
        //        self.productionAudit.userInteractionEnabled = true
        self.selectReportType.isUserInteractionEnabled = true
        
        self.selectStage.isUserInteractionEnabled = true
        self.selectBrand.isUserInteractionEnabled = true
        self.Po_Text.isUserInteractionEnabled = true
        
        
        // Now user can write a po
        
        suggestions_TableView!.dataSource = self
        suggestions_TableView!.delegate = self
        
        Po_Text.isUserInteractionEnabled = true
        
        Po_Text.delegate = self
        

    }
    
    func saveVendorText(_ strText: String, strID: String) {
        
        //    remove all the data(colors,sizes,styles) related to po
        self.ListOfPOs = [:]
        self.autoCompletePossibilities = []
        
        recentlySelectedPoKey.removeAll()
        selectedPoID = ""
        PoKeysToSend = ""
        self.POText = ""
        self.Po_Text.text?.removeAll()
        
        
        
        SizesData.removeAll()
        SizesDataID.removeAll()
        
        Coloritems.removeAll()
        ColoritemsID.removeAll()
        
        Sizeitems.removeAll()
        SizeitemsID.removeAll()
        
        selectColor.isUserInteractionEnabled = false
        selectSize.isUserInteractionEnabled = false
        
        DefectsColorsData0.removeAll()
        DefectsSizesData0.removeAll()
        
        self.colorsLabel.text = "Select Color(s)"
        self.colorsIDsString = ""
        self.selectedColorsIDs = []
        
        self.sizesLabel.text = "Select Size(s)"
        self.sizesIDsString = ""
        self.selectedSizesIDs = []
        
        
        self.selectStyleArray.removeAll()
        self.selectStyleArrayIDs.removeAll()
        self.selectedStyleID = ""
        self.styleLabel.text = "Select Style #"
        self.productCodeLabel.text =  "Select product Code"

        
        //- - - - - - ---- - --- - - --
        
        
        // reset unitsArray
        
        self.unitLabel.text = "Select Unit"
        selectedUnitID = ""
        selectUnitArray.removeAll()
        selectUnitArrayIDs.removeAll()
        
        // clears all the data related to lineArray because assuming that you want to change vendor
        
        selectLineArray.removeAll()
        selectLineArrayIDs.removeAll()
        self.productionLineLabel.text = "Select Line"
        self.selectedLine = ""
        NewLineView.isHidden = true
//        self.newLine.hidden = true
        
        
        SelectedVendorID = strID
        
        
        // -POs -POs -     -POs -POs -POs -    -POs -POs -POs -POs -
        //send request for the list of POs to show in PO text field as suggesstions
        
        bodyDataForPOs = "User=\(defaults.object(forKey: "userid")as! String)&Brand=\(SelectedBrandID!)&Vendor=\(SelectedVendorID!)&ReportType=\(selectedReportTypeID!)"
        print("bodyDataForPO=",bodyDataForPOs)
        self.performLoadDataRequestForPOs(self.getUrlForPOs())
        
        
        // - - -     - - - -    - - - - -
        //send request for getting line numbers
        
        self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&Vendor=\(SelectedVendorID!)"
        print("bodyData = \(self.bodyData)")
        
        self.performLoadDataRequestWithURL(self.getUrl())
        
        // - - - - -     - - - - -     - - - - -
        
        
        // now filter the vendorUnitsKeyValue Pairs where key k == SelectedVendorID
        
        let fil = VendorUnitsKV.filter({ (k,v) -> Bool in
            k == "\(SelectedVendorID)"
        }) // returned value is an array of tuples
        
        //            print("fil = \(fil)")
        
        var stringOfVendorIDs = ""
        
        if !fil.isEmpty{
            print("empty fil")
            let (_, value) = fil.first!
            //           print("value = \(value)")
            stringOfVendorIDs = value
            
        }else{
            
            selectUnitArray.removeAll()
            selectUnitArrayIDs.removeAll()
            self.unitLabel.text = "Select Unit"
            self.selectedUnitID = ""
        }
        
        //            for result in fil {
        //                newData[result.0] = result.1
        //                print("result.0 = \(result.0)")
        //                print("result.1 = \(result.1)")
        //                stringOfVendorIDs = result.1
        //            }
        
        if !stringOfVendorIDs.isEmpty {
            print("stringOfVendorIDs =")
            print(stringOfVendorIDs)
            
            let arrayOfVendorsIds = stringOfVendorIDs.components(separatedBy: ",")
            for i in arrayOfVendorsIds{
                print(i)
            }
            
            let listOfKeys = Array(UnitsKV.keys)
            print("listOfKeys = \(listOfKeys)")
            
            for i in arrayOfVendorsIds{
                print("i = \(i)")
                for j in 0 ..< listOfKeys.count {
                    print("j = \(j)")
                    if i == listOfKeys[j]{
                        print("i and j equals")
                        selectUnitArray.append(UnitsKV[i]!)
                        selectUnitArrayIDs.append(i)
                    }
                }
            }
            print("selectUnitArrayIDs = \(selectUnitArrayIDs)")
            print("selectUnitArray = \(selectUnitArray)")
            
        }
        else{
            // dlfjlsdglksglkdjgkdfjg
        }
        //            var filteredA = VendorUnitsKV.filter({$0.0 == "\(SelectedVendorID)"})
        //            print("filteredA= \(filteredA)")
        
        
        self.vendorLabel.text = strText
        
        self.selectVendor.isSelected = false
        
        // Now Enable the userInteraction of any other buttons.so that there is no fatal error: Array index out of range
        
//        self.productionAudit.userInteractionEnabled = true
        self.selectReportType.isUserInteractionEnabled = true
        
        self.selectStage.isUserInteractionEnabled = true
        self.selectBrand.isUserInteractionEnabled = true
        self.Po_Text.isUserInteractionEnabled = true
        
        
        // Now user can write a po
        
        suggestions_TableView!.dataSource = self
        suggestions_TableView!.delegate = self
        
        Po_Text.isUserInteractionEnabled = true
        
        Po_Text.delegate = self
        
    }
    
    
    func saveUnitText(_ strText: String, strID: String) {
        
        
        // clears all the data related to lineArray because assuming that you want to change unit
        
        selectLineArray.removeAll()
        selectLineArrayIDs.removeAll()
        self.productionLineLabel.text = "Select Line"
        self.selectedLine = ""
        
        
//        if newLine.hidden == false && 
        if NewLineView.isHidden == false{
            NewLineView.isHidden = true
//            newLine.hidden = true
        }
        
        
        if selectUnitArray.isEmpty && selectUnitArrayIDs.isEmpty {
            
            self.selectUnit.isSelected = false
            
            //                    self.selectLineArray.removeAll()
            //                    self.selectLineArrayIDs.removeAll()
            //
            //                    self.selectLineArray = []
            //                    self.selectLineArrayIDs = []
            //                    self.selectLineArray.append("newLine")
            //                    self.selectLineArrayIDs.append("new")
            //
            //                selectedUnitID = "\(selectUnitArrayIDs[numbers.first!])"
            
            
        }
        else{
            
            selectedUnitID = strID
            
            //send request for getting line numbers
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&Vendor=\(SelectedVendorID)&Unit=\(selectedUnitID)"
            print("bodyData = \(self.bodyData)")
            
            self.performLoadDataRequestWithURL(self.getUrl())
            
            
            self.unitLabel.text = strText
            
            self.selectUnit.isSelected = false
        }
        
    }
    
    func saveStyleTextEdit(_ strText: String, strID: String) {
        
        selectedStyleID = strID
        print("selectedStyleID = \(selectedStyleID)")
        DispatchQueue.main.async(execute: {
        self.styleLabel.text = strText
       
        self.selectStyleNo.isSelected = false
             })
    }
    func saveStyleText(_ strText: String, strID: String) {
        
        selectedStyleID = strID
        print("selectedStyleID = \(selectedStyleID)")
        
        self.styleLabel.text = strText
        
        self.selectStyleNo.isSelected = false
    
        ///
        DefectsColorsData0.removeAll()
        DefectsSizesData0.removeAll()
        
        self.colorsLabel.text = "Select Color(s)"
        self.colorsIDsString = ""
        self.selectedColorsIDs = []
        let All_Colors = self.dictionary["Colors"]!  as! [String:AnyObject]
        print("All Colors= \(All_Colors)")
        
        let All_Style_Colors = self.dictionary["StyleColors"]!  as! [String:AnyObject]

        var myArray = [String : AnyObject]()
        //
        //for countryObj in All_Colors{
            
            for color in All_Style_Colors{
                if((color.0 as String).contains(selectedStyleID!)){
                    let sp = color.0 as String
                    let dd = sp.components(separatedBy: "-")
                    myArray[color.1 as! String] = dd[1] as AnyObject?
                }
            }
       
        //}
        print(myArray)
        var mynewDICT:[Int:[String:AnyObject]]! = [:]
        
        // Get the array from the keys property.
        let copy = myArray.keys
        // Sort from low to high (alphabetical order).
        let p = copy.sorted(by: <)
        print(p)
        
        for pi in p{
            print(pi)
        }
        let s = myArray.keys.sorted()
        print(s)
        
        // initialize first element to newline and remove all elements in selectLineArray
        
        Coloritems = []
        ColoritemsID = []
        
        // Loop over sorted keys.
        var i = 0
        for pin in p{
            for key in copy {
                if key == pin{
                    // Get value for this key.
                    if let value = myArray[key] {
                        
                        mynewDICT[i] = [value as! String : key as AnyObject]
                        
                        Coloritems.append(key)
                        ColoritemsID.append(value as! String)
                        
                        print("ColorsKey = \(key), ColorsValue = \(value)")
                        i+=1
                    }
                }
            }}
        print("Coloritems = \(Coloritems)")
        
    
    
        ///

    }
    func saveSampleSizeText(_ strText: String, strID: String) {
        
        self.sampleSizeLabel.text = strText
        if(auditEditMode == true){
        }else{
        let TimeIn = Int(sampleSizeLabel.text!)!*120
        self.endTimeLable.text = Date(timeIntervalSinceNow: TimeInterval(Int(TimeIn))).formattedTime
        }
    }
    func saveProductionLineText(_ strText: String, strID: String) {
        self.selectedLine = strID
        
        if strID == "new"{
            print("newline is tapped!")

            NewLineView.isHidden = false
        }else{
            newLine.text = ""
            NewLineView.isHidden = true
            selectedLine = strID
            
        }
        newLine.resignFirstResponder()
        self.productionLineLabel.text = strText
    }
    
    func saveAuditorsText(_ strText: String, strID: String) {
        // DO Nothing....
    }
    func saveVendorsText(_ strText: String, strID: String) {
        // DO Nothing....
    }
    func saveAuditResultText(_ strText: String, strID: String) {
        // Do Nothing....
    }
    func saveAQLText(_ strText: String, strID: String) {
        self.AQLLabel.text = strText
        self.selectedAQLID = strID
    }
    func saveAQLData(_ strText: String, strID: String) {
        self.lblAQL.text = strText
        self.selectedAQLDataID = strID
    }
    
    @IBAction func selectBrandClicked(_ sender: AnyObject) {
        
        print("selectBrand button is  clicked.")
 
        PickerArray = BrandArray
        PickerArrayIDs = BrandArrayIDs
        
        print("PickerArray = \(PickerArray)")
        
        let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as! PopUpViewController
        
        popupVC.delegate = self
        popupVC.WhichButton = "BrandName"
        
        
        popupVC.pickerdata = PickerArray
        popupVC.pickerdataIDs = PickerArrayIDs
        
        popupVC.AllreadySelectedText = brandLabel.text!
            
        self.addChildViewController(popupVC)
        popupVC.view.frame = self.view.frame
        
        self.view.addSubview(popupVC.view)
        self.didMove(toParentViewController: self)
        

    }
    
    @IBAction func AQLClicked(_ sender: UIButton) {
        
        PickerArray = AQLData
        PickerArrayIDs = AQLDataIDs
        
        print("PickerArray = \(PickerArray)")
        
        let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as! PopUpViewController
        
        popupVC.delegate = self
        popupVC.WhichButton = "AQL"
        
        
        popupVC.pickerdata = PickerArray
        popupVC.pickerdataIDs = PickerArrayIDs
        
        popupVC.AllreadySelectedText = lblAQL.text!
        
        self.addChildViewController(popupVC)
        popupVC.view.frame = self.view.frame
        
        self.view.addSubview(popupVC.view)
        self.didMove(toParentViewController: self)
        

    }
    @IBAction func selectAQLClicked(_ sender: AnyObject) {
        
//        AQLArrayIDs
        self.offeredQuantity.resignFirstResponder()
        print("selectAQL button is  clicked.")
        
        PickerArray = AQLArray
        PickerArrayIDs = AQLArrayIDs
        
        print("PickerArray = \(PickerArray)")
        
        let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as! PopUpViewController
        
        popupVC.delegate = self
        popupVC.WhichButton = "AQLLevel"
        
        
        popupVC.pickerdata = PickerArray
        popupVC.pickerdataIDs = PickerArrayIDs
        
        popupVC.AllreadySelectedText = AQLLabel.text!
        
        self.addChildViewController(popupVC)
        popupVC.view.frame = self.view.frame
        
        self.view.addSubview(popupVC.view)
        self.didMove(toParentViewController: self)
        

    }
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        //btnSchedule.isUserInteractionEnabled = false
        if textField == Po_Text{
            if SelectedBrandID != nil && SelectedVendorID != nil {
                Po_Text.isUserInteractionEnabled = true
                print("selectBrandID and SelectVendorID is not nil")
                
            }else if SelectedBrandID != nil || SelectedVendorID != nil{
                print("First select Brand and Vendor")
                Po_Text.resignFirstResponder()
                popupMessage("First Select Brand and Vendor")
//                Po_Text.userInteractionEnabled = false
                return true
            }
            //        }
            // for a moment disable the userInteraction of any other button.so that there is no fatal error: Array index out of range
            
//            self.productionAudit.userInteractionEnabled = false
            self.selectReportType.isUserInteractionEnabled = false
            
            self.selectStage.isUserInteractionEnabled = false
            self.selectBrand.isUserInteractionEnabled = false
            self.selectVendor.isUserInteractionEnabled = false
            self.selectUnit.isUserInteractionEnabled = false
            
            
            
            print("recentlySelectedPoKey when BeginEditing =",recentlySelectedPoKey)
            print("substring when BeginEditing =",substring)
            substring = ""
            searchAutocompleteEntriesWithSubstring(substring)
            
            suggestions_TableView!.isHidden = false
            if(auditEditMode == true){
            for k in recentlySelectedPoKey{
                if(!PoKeysToSend.contains(k)){
                                PoKeysToSend.append(k+",")
                                poSelectedFromSuggesstions = true
                                // >?>?>?>?>?>?>?>?>>?>
                            }
                }
            }else{
//            for k in recentlySelectedPoKey{
//                PoKeysToSend.append(k+",")
//                poSelectedFromSuggesstions = true  // >?>?>?>?>?>?>?>?>>?>
//            }
            }
            //        self.selectedPoID = PoKeysToSend
            //            print("self.selectedPoID when BeginEditing2 =",self.selectedPoID)
            //PoKeysToSend = recentlySelectedPoKey
            //        Po_Text.text = ""
            //        POText = ""
            print(PoKeysToSend)
            return true
        }else if textField == newLine{
            
            newLine.isUserInteractionEnabled = true
            return true
        
        }else if textField == offeredQuantity{

            return true
            
        }else{
            return false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
//        if(textField == newLine){
//            newLine.resignFirstResponder()
//        }
        if textField == newLine{
            btnSchedule.isUserInteractionEnabled = true
            print("newLine is returned.")
            
            if !newLine.text!.isEmpty{
                selectedLine = "0_"+newLine.text!
            }else{
                selectedLine = ""
            }
            print("selectedLine = ", selectedLine)
            newLine.resignFirstResponder()
            
        }
    }
    var shouldReturn = false
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       print("PoKeysToSend when return pressed =",PoKeysToSend)
       
        // Now Enable the userInteraction of any other button.so that there is no fatal error: Array index out of range
        
//        self.productionAudit.userInteractionEnabled = true
        self.selectReportType.isUserInteractionEnabled = true
        
        self.selectStage.isUserInteractionEnabled = true
        self.selectBrand.isUserInteractionEnabled = true
        self.selectVendor.isUserInteractionEnabled = true
        self.selectUnit.isUserInteractionEnabled = true
        SAScrollView.contentSize.height = AuditScheduleView.frame.height+60
        
        if textField == Po_Text{
//            SAScrollView.contentSize.height = AuditScheduleView.frame.height+60
//            
//            suggestions_TableView?.frame.origin.x = self.POsView.frame.origin.x + 17
//            suggestions_TableView?.frame.origin.y = self.POsView.frame.origin.y + 2*(self.Po_Text.frame.height) - 12
            self.Po_Text.resignFirstResponder()
            
            if Po_Text.text!.isEmpty{
               
                print("PoText is Empty")
                //    remove all the data(colors,sizes,styles) related to po
                SizesData.removeAll()
                SizesDataID.removeAll()
                
                Coloritems.removeAll()
                ColoritemsID.removeAll()
                
                Sizeitems.removeAll()
                SizeitemsID.removeAll()
               
                selectColor.isUserInteractionEnabled = false
                selectSize.isUserInteractionEnabled = false
                
                DefectsColorsData0.removeAll()
                DefectsSizesData0.removeAll()
                
                self.colorsLabel.text = "Select Color(s)"
                self.colorsIDsString = ""
                self.selectedColorsIDs = []
                
                self.sizesLabel.text = "Select Size(s)"
                self.sizesIDsString = ""
                self.selectedSizesIDs = []
                
                
                self.selectStyleArray.removeAll()
                self.selectStyleArrayIDs.removeAll()
                self.selectedStyleID = ""
                self.styleLabel.text = "Select Style #"
                self.productCodeLabel.text =  "Select product Code"
                recentlySelectedPoKey.removeAll()
                PoKeysToSend = ""
            }else{
                
                if(auditEditMode == true){
                    shouldReturn = true
                    SizesData.removeAll()
                    SizesDataID.removeAll()
                    
                    Coloritems.removeAll()
                    ColoritemsID.removeAll()
                    
                    Sizeitems.removeAll()
                    SizeitemsID.removeAll()
                    
                    selectColor.isUserInteractionEnabled = false
                    selectSize.isUserInteractionEnabled = false
                    
                    DefectsColorsData0.removeAll()
                    DefectsSizesData0.removeAll()
                    
                    self.colorsLabel.text = "Select Color(s)"
                    self.colorsIDsString = ""
                    self.selectedColorsIDs = []
                    
                    self.sizesLabel.text = "Select Size(s)"
                    self.sizesIDsString = ""
                    self.selectedSizesIDs = []
                    
                    
                    self.selectStyleArray.removeAll()
                    self.selectStyleArrayIDs.removeAll()
                    self.selectedStyleID = ""
                    self.styleLabel.text = "Select Style #"
                    self.productCodeLabel.text =  "Select product Code"

//                    recentlySelectedPoKey.removeAll()
//                    PoKeysToSend = ""
                }
                SetUrl_and_GetColorsSizeStyles()
            }
            
            
            
            // self.autoCompletePossibilities.removeAll(keepCapacity: false)
            // self.autoComplete.removeAll(keepCapacity: false)
//            dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                self.PoKeysToSend = ""
//            })
            
            
            self.suggestions_TableView?.isHidden = true
            
//            Po_Text.userInteractionEnabled = false
            return true
        }
        else if textField == newLine{
             btnSchedule.isUserInteractionEnabled = true
            print("newLine is returned.")
            
            if !newLine.text!.isEmpty{
                selectedLine = "0_"+newLine.text!
            }else{
                selectedLine = ""
            }
            print("selectedLine = ", selectedLine)
            newLine.resignFirstResponder()
            return true
            
        }else if textField == offeredQuantity{
                        SAScrollView.contentSize.height = AuditScheduleView.frame.height+60
            
                        suggestions_TableView?.frame.origin.x = self.POsView.frame.origin.x + 17
                        suggestions_TableView?.frame.origin.y = self.POsView.frame.origin.y + 2*(self.Po_Text.frame.height) - 12
            textField.resignFirstResponder()
            return true
            
        }else{
            return false
        }
    }

    
    
    @IBAction func SelectDatePressed(_ sender: AnyObject) {
        
        DatePickerDialog().show("SelectDate", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            
            self.dateLabel.text = "\(date.formatted)"
        }

    }
    
    @IBAction func selectEndTimePressed(_ sender: Any) {
        DatePickerDialog().show("SelectEndTime", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .time) {
            (date) -> Void in
                self.endTimeLable.text = "\(date.formattedTime)"
        }
    }
    @IBAction func SelectTimePressed(_ sender: AnyObject) {
        
        DatePickerDialog().show("SelectTime", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .time) {
            (date) -> Void in
            
            self.timeLabel.text = "\(date.formattedTime)"
           
            self.endTimeLable.text = Date.init(timeInterval: 120, since: date).formattedTime
            //Date(timeIntervalSinceNow: 120).formattedTime

        }

    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
//            defaults.removeObjectForKey("userid")
          
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
//            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("2") as? ScheduleAuditViewController {
//                //                self.dismissViewControllerAnimated(true, completion: nil)
//                presentViewController(resultController, animated: true, completion: nil)
//            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
//        case 4:
//            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
//                //                self.dismissViewControllerAnimated(true, completion: nil)
//                presentViewController(resultController, animated: true, completion: nil)
//            }
//          
//            let selectedImage:UIImage = UIImage(named: "Selected QAReport")!
//            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
//            
//            break
        case 5:
            print("self.parentViewController = \(self.presentingViewController)")

            if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }else{
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }
            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
    
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_selected_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break

        default:
            break
        }
    }
    
    func showUnits(_ savedData:String?){
        
        if savedData != nil {
            
            self.dictionary = self.parseJSON(savedData!)
            print("\(self.dictionary)")
            
            let All_Countries = self.dictionary["Units"]!  as? [String:AnyObject]
            print("All Units= \(All_Countries)")
            
            if All_Countries != nil{
            
            var myArray = [String : AnyObject]()
            //
            for countryObj in All_Countries!{
            
                myArray[countryObj.1 as! String] = countryObj.0 as AnyObject?
                
            }
            print(myArray)
            var mynewDICT:[Int:[String:AnyObject]]! = [:]
            
            // Get the array from the keys property.
            let copy = myArray.keys
            // Sort from low to high (alphabetical order).
            let p = copy.sorted(by: <)
            print(p)
            
            for pi in p{
                print(pi)
            }
            let s = myArray.keys.sorted()
            print(s)
            // Loop over sorted keys.
            var i = 0
            for pin in p{
                for key in copy {
                    if key == pin{
                        // Get value for this key.
                        if let value = myArray[key] {
                            //                            mynewDICT[value as! String] = key
                            mynewDICT[i] = [value as! String : key as AnyObject]
                            //                            self.items.append(key)
                            //                            self.itemsID.append(value as! String)
//                            Units.append(key)
//                            UnitsIDs.append(value as! String)
//                            var UnitsKV:[String:String] = [:]
                            UnitsKV[value as! String] = key
                            print("Key = \(key), Value = \(value)")
                            i+=1
                        }
                    }
                }}
//            print(mynewDICT)
            print("UnitsKV = \(UnitsKV)")
        }
        }
    }

    func showVendorUnits(_ savedData:String?){
        
        if savedData != nil {
            
            self.dictionary = self.parseJSON(savedData!)
            print("\(self.dictionary)")
            
            let All_Countries = self.dictionary["VendorUnits"]!  as? [String:AnyObject]
            
            print("All stages= \(All_Countries)")
            
            if All_Countries != nil{

            var myArray = [String : AnyObject]()
            //
            for countryObj in All_Countries!{
                
                myArray[countryObj.1 as! String] = countryObj.0 as AnyObject?
                
            }
            print(myArray)
            var mynewDICT:[Int:[String:AnyObject]]! = [:]
            
            // Get the array from the keys property.
            let copy = myArray.keys
            // Sort from low to high (alphabetical order).
            let p = copy.sorted(by: <)
            print(p)
            
            for pi in p{
                print(pi)
            }
            let s = myArray.keys.sorted()
            print(s)
            // Loop over sorted keys.
            var i = 0
            for pin in p{
                for key in copy {
                    if key == pin{
                        // Get value for this key.
                        if let value = myArray[key] {
                            //                            mynewDICT[value as! String] = key
                            mynewDICT[i] = [value as! String : key as AnyObject]
                            //                            self.items.append(key)
                            //                            self.itemsID.append(value as! String)
//                            VendorUnits.append(key)
//                            VendorUnitsIDs.append(value as! String)
//                            var VendorUnitsKV:[String:String] = [:]
                            VendorUnitsKV[value as! String] = key
                            print("Key = \(key), Value = \(value)")
                            i+=1
                        }
                    }
                }}
            print(mynewDICT)
            print("Total Countries = \(All_Countries!.count)")
        }
        }
    }

    
    func showVendorsDataWhenOffline(_ savedData:String?){
        
        if savedData != nil {
            
            self.dictionary = self.parseJSON(savedData!)
            print("\(self.dictionary)")
            
            let All_Countries = self.dictionary["Vendors"]!  as? [String:AnyObject]
            print("All stages= \(All_Countries)")
            
            if All_Countries != nil{

            var myArray = [String : AnyObject]()
            //
            for countryObj in All_Countries!{
                print("VVVVVVVV=== \(countryObj)")
                if(countryObj.1 is String){
                myArray[countryObj.1 as! String] = countryObj.0 as AnyObject?
                
            }
                }
            print(myArray)
            var mynewDICT:[Int:[String:AnyObject]]! = [:]
            
            // Get the array from the keys property.
            let copy = myArray.keys
            // Sort from low to high (alphabetical order).
            let p = copy.sorted(by: <)
            print(p)
            
            for pi in p{
                print(pi)
            }
            let s = myArray.keys.sorted()
            print(s)
            // Loop over sorted keys.
            var i = 0
            for pin in p{
                for key in copy {
                    if key == pin{
                        // Get value for this key.
                        if let value = myArray[key] {
                            //                            mynewDICT[value as! String] = key
                            mynewDICT[i] = [value as! String : key as AnyObject]
                            //                            self.items.append(key)
                            //                            self.itemsID.append(value as! String)
                         
                            // fill the below 2 arrays when brand is selected
                            
                          //  selectVendorArray.append(key)
                          //  selectVendorArrayIDs.append(value as! String)
                            
//                            var selectVendorsKV:[String:String] = [:]
                            selectVendorsKV[value as! String] = key

                            
                            print("Key = \(key), Value = \(value)")
                            i+=1
                        }
                    }
                }}
                print("selectVendorsKV =",selectVendorsKV)
            print(mynewDICT)
            print("Total Countries = \(All_Countries!.count)")
            }
        }
    }

    
    func showBrandsDataWhenOffline(_ savedData:String?){
        
        if savedData != nil {
            
            
            if let userType = defaults.object(forKey: "UserType"){
                if("\(userType as! String)" == "LEVIS"){
                    
                    self.dictionary = self.parseJSON(savedData!)
                    print("\(self.dictionary)")
                    
                    let All_Countries = self.dictionary["LevisBrands"]!  as! [AnyObject]
                    print("All stages= \(All_Countries)")
                    //var myArray = [String : AnyObject]()
                    //
                    for countryObj in All_Countries{
                        BrandArray.append(countryObj["name"] as! String)
                        BrandArrayIDs.append(countryObj["id"] as! String)
                        //myArray[countryObj.1 as! String] = countryObj.0 as AnyObject?
                        
                    }

                    

                }else{
            self.dictionary = self.parseJSON(savedData!)
            print("\(self.dictionary)")
            
            let All_Countries = self.dictionary["Brands"]!  as! [String:AnyObject]
            print("All stages= \(All_Countries)")
            
            
            var myArray = [String : AnyObject]()
            //
            for countryObj in All_Countries{
                
                
                myArray[countryObj.1 as! String] = countryObj.0 as AnyObject?
                
            }
            print(myArray)
            var mynewDICT:[Int:[String:AnyObject]]! = [:]
            
            // Get the array from the keys property.
            let copy = myArray.keys
            // Sort from low to high (alphabetical order).
            let p = copy.sorted(by: <)
            print(p)
            
            for pi in p{
                print(pi)
            }
            let s = myArray.keys.sorted()
            print(s)
            // Loop over sorted keys.
            var i = 0
            for pin in p{
                for key in copy {
                    if key == pin{
                        // Get value for this key.
                        if let value = myArray[key] {
                            //                            mynewDICT[value as! String] = key
                            mynewDICT[i] = [value as! String : key as AnyObject]
                            //                            self.items.append(key)
                            //                            self.itemsID.append(value as! String)
                            BrandArray.append(key)
                            BrandArrayIDs.append(value as! String)
                            print("Key = \(key), Value = \(value)")
                            i+=1
                        }
                    }
                }}
                         print(mynewDICT)
            print("Total Countries = \(All_Countries.count)")
                }
            }

        }
    }

    func showBrandVendors(_ savedData:String?){
        
        if savedData != nil {
            
            self.dictionary = self.parseJSON(savedData!)
            print("parsed saveData in show Brands =\(self.dictionary)")
            
            let All_BrandVendors = self.dictionary["BrandVendors"]!  as! [String:AnyObject]
            print("BrandVendors in show Brands= \(All_BrandVendors)")
            
            
//            var myArray = [String : AnyObject]()
//            //
//            for countryObj in All_BrandVendors{
//                
//                myArray[countryObj.1 as! String] = countryObj.0
//                
//            }
//            print(myArray)
//            var mynewDICT:[Int:[String:AnyObject]]! = [:]
            
//            //Get the array from the keys property.
//            let copy = myArray.keys
//            print("copy in show Brands =",copy)
//            // Sort from low to high (alphabetical order).
//            let p = copy.sort(<)
//            print("sorted BrandVendors in show Brand =",p)
//            
//            for pi in p{
////                print(pi)
//            }
            
            
            
//            let s = myArray.keys.sort()
//            print("sorted key in show Brands",s)
//            // Loop over sorted keys.
//            var i = 0
//            for pin in p{
//                for key in copy {
//                    if key == pin{
//                        // Get value for this key.
//                        if let value = myArray[key] {
//
//                            mynewDICT[i] = [value as! String : key]
//                           
//                            BrandVendorsKV[value as! String] = key
//                            print("Key = \(value), Value = \(key)")
//                            i+=1
//                        }
//                    }
//                }}
            
            // ?? --  ??  --  ?? -- ??
            let copy2 = All_BrandVendors.keys
            let p2 = copy2.sorted()
            let s = All_BrandVendors.keys.sorted()
            print("sorted key in show Brands",s)
            // Loop over sorted keys.
            var i = 0
            for pin in p2{
                for key in copy2 {
                    if key == pin{
                        // Get value for this key.
                        if let value = All_BrandVendors[key] {
                            
                            BrandVendorsKV[key] = value as! String
                            print("BrandVendorsKV-Key = \(key), BrandVendorsKV-Value = \(value)")
                            i+=1
                        }
                    }
                }}
            
            // ??  ?? // ?? ??  ??  ??
            
            print("BrandVendorsKV.keys in showbrands =",BrandVendorsKV.keys)
            print("BrandVendorsKV in showbrands =",BrandVendorsKV)
//            print(mynewDICT)
            print("Total Countries = \(All_BrandVendors.count)")
            
        }
//        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
   var reports = [[String:AnyObject]]()
    func showReportType(reportID:String)->String{
        
                    var reportType = ""
            //
            for countryObj in reports{
                if(reportID == countryObj["id"]! as! String){
                print(countryObj["name"]!)
               reportType = countryObj["name"] as! String
                }
            }
        return reportType
        }
    func showReportTypeDataWhenOffline(_ savedData:String?){
        
        if savedData != nil {
            reports.removeAll()
            self.dictionary = self.parseJSON(savedData!)
            print("\(self.dictionary)")
            
        reports = self.dictionary["Reports"]!  as! [[String:AnyObject]]
            print("All stages= \(reports)")
            
            
            var myArray = [String : AnyObject]()
            //
            for countryObj in reports{
                print(countryObj["name"]!)
                myArray[countryObj["name"] as! String] = countryObj["id"]!
                
            }
            print(myArray)
            var mynewDICT:[Int:[String:AnyObject]]! = [:]
            
            // Get the array from the keys property.
            let copy = myArray.keys
            // Sort from low to high (alphabetical order).
            let p = copy.sorted(by: <)
            print(p)
            
            for pi in p{
                print(pi)
            }
            let s = myArray.keys.sorted()
            print(s)
            // Loop over sorted keys.
            var i = 0
            for pin in p{
                for key in copy {
                    if key == pin{
                        // Get value for this key.
                        if let value = myArray[key] {
                            //                            mynewDICT[value as! String] = key
                            mynewDICT[i] = [value as! String : key as AnyObject]
//                            self.items.append(key)
//                            self.itemsID.append(value as! String)
                            ReportTypeArray.append(key)
                            ReportTypeArrayIDs.append(value as! String)
                            print("Key = \(key), Value = \(value)")
                            i+=1
                        }
                    }
                }}
            print(mynewDICT)
            print("Total Countries = \(reports.count)")
            
            // if  ReportTypeArray.count greater than zero then show Select ReportType Button
            if ReportTypeArray.count>1 && ReportTypeArrayIDs.count>1{
                
                // then Show selectReportType button
                ReportTypeView.isHidden = false
            }else{
                // hide the SelectReportType button
                ReportTypeView.isHidden = true
                for reportTxt  in ReportTypeArray{
                    ReportType.text = reportTxt
                }

                for reportID in ReportTypeArrayIDs{
                    selectedReportTypeID = reportID
                    self.showStagesDataWhenOffline(ReportType.text!, strID: reportID)
                }
             if selectedReportTypeID != nil{
                    self.DoActionAgainstSelectedReportID()
                }

            }
        }
    }

    
    func showStagesDataWhenOffline(_ strText: String, strID: String){
        AuditStageArray.removeAll()
        AuditStageArrayIDs.removeAll()
        do{
        var readString: String
        readString = try NSString(contentsOfFile: jsonLoginFilePath.path, encoding: String.Encoding.utf8.rawValue) as String
        print(readString)
        
        if readString != nil {
            
            self.dictionary = self.parseJSON(readString)
            print("\(self.dictionary)")
            
            let All_CountriesD = self.dictionary["Reports"]!  as! [[String:AnyObject]]
            print("All stages= \(All_CountriesD)")
            
            
            var myArrayD = [String : AnyObject]()
            //
            var stages = ""
            var arr = [String]()
            for countryObj in All_CountriesD{
                print(countryObj["name"]!)
                if(countryObj["name"]! as! String == strText){
                    stages = countryObj["stages"] as! String
                    arr = stages.components(separatedBy: ",")
                    print(arr)
                }
                myArrayD[countryObj["name"] as! String] = countryObj["id"]!
                
            }

            let All_Countries = self.dictionary["Stages"]!  as! [String:AnyObject]
            print("All stages= \(All_Countries)")
            

            var myArray = [String : AnyObject]()
            //i in 0 ..< len
            if(strID == "14"||strID == "34"||strID == "45" || strID == "44" || strID == "32" || strID == "46"){
            for item in 0..<arr.count{
            for countryObj in All_Countries{
                if(countryObj.0 == arr[item] ){
                myArray[countryObj.1 as! String] = countryObj.0 as AnyObject?
                }
            }
            }
            }else{
                for countryObj in All_Countries{
                        myArray[countryObj.1 as! String] = countryObj.0 as AnyObject?
                }
            }
            print(myArray)
            var mynewDICT:[Int:[String:AnyObject]]! = [:]
            
            // Get the array from the keys property.
            let copy = myArray.keys
            // Sort from low to high (alphabetical order).
            let p = copy.sorted(by: <)
            print(p)
            
            for pi in p{
                print(pi)
            }
            let s = myArray.keys.sorted()
            print(s)
            // Loop over sorted keys.
            var i = 0
            for pin in p{
                for key in copy {
                    if key == pin{
                        // Get value for this key.
                        if let value = myArray[key] {
                            //                            mynewDICT[value as! String] = key
                            mynewDICT[i] = [value as! String : key as AnyObject]
                            self.items.append(key)
                            self.itemsID.append(value as! String)
                            AuditStageArray.append(key)
                            AuditStageArrayIDs.append(value as! String)
                            print("Key = \(key), Value = \(value)")
                            i+=1
                        }
                    }
                }}
            print(mynewDICT)
                      print("Total Countries = \(All_Countries.count)")
            
        }
        }catch let error as NSError {
            print(error.description)
        }
    }
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
                
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    //----------- new Call to request audit detail ----------
    func getUrlForAuditDetail() -> URL {
        let toEscape = "http://app.3-tree.com/quonda/audit-schedule.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
    func performAuditDetailRequest(_ url:URL){
        self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(auditCode)"
        
        print("Audit Detail_bodyData= \(self.bodyData)")
        
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = self.bodyData!.data(using: String.Encoding.utf8)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if (error != nil) {
                print(error!)
                let alertView = UIAlertController(title: "Cannot get Audit Detail due to some reason." , message: "\(error!.localizedDescription)", preferredStyle: .alert)
                let Reload_action: UIAlertAction = UIAlertAction(title: "Retry", style: .default, handler: { (UIAlertAction) -> Void in
                        self.activityIndicator.stopAnimating()
                        self.messageFrame.removeFromSuperview()
                    
                self.performAuditDetailRequest(self.getUrlForAuditDetail())
                })
                let no_action: UIAlertAction = UIAlertAction(title: "NO", style: .default, handler:  {(UIAlertAction) -> Void in

                })
                
                
                alertView.addAction(Reload_action)
                alertView.addAction(no_action)
                self.present(alertView, animated: true, completion: nil)
                
            }
            else if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                
                print(json)
                
                
//                self.dictionary = self.parseJSON(json)
                if("\(self.dictionary["Status"]!)" == "ERROR"){
                    
                    DispatchQueue.main.async(execute: {
                        self.activityIndicator.stopAnimating()
                        self.messageFrame.removeFromSuperview()
                    })
                    self.popupMessage("\(self.dictionary["Message"]!)")
                    
                }
                else if("\(self.dictionary["Status"]!)" == "OK"){
                   
                    DispatchQueue.main.async(execute: {
                         self.showDataWhenOffline(json)
                        self.activityIndicator.stopAnimating()
                        self.messageFrame.removeFromSuperview()
                    })
                }
            }//if data != nil
            else{
                print("return data is nil")
                
                let alertView = UIAlertController(title: nil , message: "Press OK to Retry ", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                 self.performAuditDetailRequest(self.getUrlForAuditDetail())
                })
                let Cancel_action: UIAlertAction = UIAlertAction(title: "No", style: .default, handler: { (UIAlertAction) -> Void in
                    
                })
                
                alertView.addAction(Cancel_action)
                alertView.addAction(ok_action)
                self.present(alertView, animated: true, completion: nil)
            }
        }) 
        
        task.resume()
    }
    // - - - - - - new Call to request for line numbers- - - - - - - -
    func getUrl() -> URL {
        let toEscape = "http://app.3-tree.com/quonda/vendor-lines.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
//    var bodyData:String!
    func performLoadDataRequestWithURL(_ url: URL) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
//        let today = NSDate()
//        self.bodyData = "User=\(defaults.objectForKey("userid")as! String)&Vendor="
        
        print("KeyStatsAndImages_bodyData= \(self.bodyData)")
        
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = self.bodyData!.data(using: String.Encoding.utf8)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request as! URLRequest, completionHandler: { (data, response, error) -> Void in
            
//            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
//                print("error")
//                return
//            }
            if (error != nil) {
                print(error)
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                let alertView = UIAlertController(title: "Error in loading data" , message: "\(error!.localizedDescription)", preferredStyle: .alert)
                let Reload_action: UIAlertAction = UIAlertAction(title: "Reload", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    self.performLoadDataRequestWithURL(self.getUrl())
                })
                let no_action: UIAlertAction = UIAlertAction(title: "NO", style: .default, handler: nil)
                
                alertView.addAction(Reload_action)
                alertView.addAction(no_action)
                self.present(alertView, animated: true, completion: nil)
                
            }
            else if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                                
                print(json)
                
                
                
                
                self.dictionary = self.parseJSON(json)
                
                print("\(self.dictionary)")
                
                // some times there may be not line found
                
                if (self.dictionary["Lines"]! as AnyObject).count >= 1 {
                    
                    
                    let All_lines = self.dictionary["Lines"]!  as! [String:AnyObject]
                    print("All stages= \(All_lines)")
                    
                    
                    var myArray = [String : AnyObject]()
                    //
                    for countryObj in All_lines{
                        
                        myArray[countryObj.1 as! String] = countryObj.0 as AnyObject?
                        
                    }
                    print(myArray)
                    var mynewDICT:[Int:[String:AnyObject]]! = [:]
                    
                    // Get the array from the keys property.
                    let copy = myArray.keys
                    // Sort from low to high (alphabetical order).
                    let p = copy.sorted(by: <)
                    print(p)
                    
                    for pi in p{
                        print(pi)
                    }
                    let s = myArray.keys.sorted()
                    print(s)
                    
                    // initialize first element to newline and remove all elements in selectLineArray
                    self.selectLineArray.removeAll()
                    self.selectLineArrayIDs.removeAll()
                    
                    self.selectLineArray.append("Enter New Line")
                    self.selectLineArrayIDs.append("new")
                    
                    // Loop over sorted keys.
                    var i = 0
                    for pin in p{
                        for key in copy {
                            if key == pin{
                                // Get value for this key.
                                if let value = myArray[key] {
                                    
                                    mynewDICT[i] = [value as! String : key as AnyObject]
                                    //                                self.items.append(key)
                                    //                                self.itemsID.append(value as! String)
                                    self.selectLineArray.append(key)
                                    self.selectLineArrayIDs.append(value as! String)
                                    print("UnitKey = \(key), UnitValue = \(value)")
                                    i+=1
                                }
                            }
                        }}
                }else {
                    print("there is no line found")
//                    if !self.selectLineArray.isEmpty && !self.selectLineArrayIDs.isEmpty {
//                    self.selectLineArray.removeAll()
//                    self.selectLineArrayIDs.removeAll()
//                    }
//                    self.selectLineArray = []
//                    self.selectLineArrayIDs = []
//                    self.selectLineArray.append("newLine")
//                    self.selectLineArrayIDs.append("new")
                }
// - - - - - my logic- - - - -  - - - -
//                var lines = self.dictionary["Lines"]! as! [String:AnyObject]
//                lines["new"] = "newline"
////                lines.appendContentsOf("\(self.dictionary["Lines"]!)")
////               lines = "\(self.dictionary["Lines"]!)"
//                print("lines= \(lines)")
//                
//                let l = Array(lines)
//                print("ArrayOfLines = \(l)")

//                var lines, Defective, SampleSize:String!
//                var Sketch,Brand, Vendor, Po:String!
//                
//                lines = self.dictionary["Lines"]! as? String
//                print("lines= \(lines)")

                
             
                
                if("\(self.dictionary["Status"]!)" == "OK"){
                    
                    //                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    //
                    //                            self.AuditCodesTable.reloadData()
                    //                        })
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                //               } //else part of count<1
            }//if data != nil
            else{
                print("return data is nil")
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                let alertView = UIAlertController(title: "No line Found" , message: "Press OK to Reload geting lines", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    self.performLoadDataRequestWithURL(self.getUrl())
                })
                let Cancel_action: UIAlertAction = UIAlertAction(title: "No", style: .default, handler: nil)
                
                alertView.addAction(Cancel_action)
                alertView.addAction(ok_action)
                self.present(alertView, animated: true, completion: nil)
            }
            
//            let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
        }) 
        
        task.resume()
    }
    
    func getUrlForPOs() -> URL {
        let toEscape = "http://app.3-tree.com/quonda/vendor-pos.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }

    func performLoadDataRequestForPOs(_ url: URL) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = self.bodyDataForPOs!.data(using: String.Encoding.utf8)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request as! URLRequest, completionHandler: { (data, response, error) -> Void in
            
//            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
//                print("error")
//                return
//            }
            
            if (error != nil) {
                print(error)
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                let alertView = UIAlertController(title: "Error in loading POs" , message: "\(error!.localizedDescription)", preferredStyle: .alert)
                let ReTry_action: UIAlertAction = UIAlertAction(title: "Retry", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    self.performLoadDataRequestForPOs(self.getUrlForPOs())
                })
                let no_action: UIAlertAction = UIAlertAction(title: "NO", style: .default, handler: nil)
                
                alertView.addAction(ReTry_action)
                alertView.addAction(no_action)
                self.present(alertView, animated: true, completion: nil)
                
            }

            else if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                
                print(json)
                
                
                
                
                self.dictionary = self.parseJSON(json)
                
                print("List of POs Fetched = \(self.dictionary)")
                
                // some times there may be no PO found
                
                if (self.dictionary["Pos"]! as AnyObject).count >= 1 {
                    
                    
                    let All_lines = self.dictionary["Pos"]!  as! [String:AnyObject]
                    print("All stages= \(All_lines)")
                    
                    
                    var myArray = [String : AnyObject]()
                    //
                    for countryObj in All_lines{
                        
                        myArray[countryObj.1 as! String] = countryObj.0 as AnyObject?
                        
                    }
                    print(myArray)
                    var mynewDICT:[Int:[String:AnyObject]]! = [:]
                    
                    // Get the array from the keys property.
                    let copy = myArray.keys
                    // Sort from low to high (alphabetical order).
                    let p = copy.sorted(by: <)
                    print(p)
                    
                    for pi in p{
                        print(pi)
                    }
                    let s = myArray.keys.sorted()
                    print(s)
                    
        
                    // Loop over sorted keys.
                    var i = 0
                    for pin in p{
                        for key in copy {
                            if key == pin{
                                // Get value for this key.
                                if let value = myArray[key] {
                                    
                                    mynewDICT[i] = [value as! String : key as AnyObject]

//                                    self.selectLineArray.append(key)
//                                    self.selectLineArrayIDs.append(value as! String)
                                    self.ListOfPOs[value as! String] = key
                                    print("POsKey = \(key), POsValue = \(value)")
                                    i+=1
                                }
                            }
                        }}
                }else {
                    print("there is no line found")

                }
            
                if("\(self.dictionary["Status"]!)" == "OK"){

//                    var listOfPOValues:[String] = []//= self.ListOfPOs.values
//                    var listOfPOKeys:[String] = []  //= self.ListOfPOs.keys
                    
                    // first reset all the previous autoCompletePossibilities
//                    self.autoCompletePossibilities.removeAll()
                   // DispatchQueue.main.async(execute: { () -> Void in
                        self.autoCompletePossibilities = []
                        print("clearing autoCompletePossibilities")
                   // })
//                    self.autoCompletePossibilities = []
                    print("self.autoCompletePossibilities before initalizing =",self.autoCompletePossibilities)
                   
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.autoComplete = []
                        print("clearing autoComplete")
                    })
                    print("autoComplete before initalizing =",self.autoComplete)
                    
                    for i in self.ListOfPOs{
                        print("PO.key",i.0)
                        print("PO.value",i.1)
//                        self.autoCompletePossibilitiesKeys.append(i.0)
                        self.autoCompletePossibilities.append(i.1)
                    }
                    print("listOfPOValues",self.autoCompletePossibilities)
//                    print("listOfPOKeys",self.autoCompletePossibilitiesKeys)
                    
//                    self.autoCompletePossibilities = listOfPOValues
//                    self.autoCompletePossibilitiesKeys = listOfPOKeys

                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                //               } //else part of count<1
            }//if data != nil
            else{
                print("return data is nil")
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                let alertView = UIAlertController(title: "No POs Found" , message: "Press OK to Reload geting list of POs", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    //self.performLoadDataRequestForPOs(self.getUrlForPOs())
                })
                let Cancel_action: UIAlertAction = UIAlertAction(title: "No", style: .default, handler: nil)
                
                alertView.addAction(Cancel_action)
                alertView.addAction(ok_action)
                self.present(alertView, animated: true, completion: nil)
            }
            
            //            let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
        }) 
        
        task.resume()
    }
    
    
    
    
    func getPoInfoUrl() -> URL {
        let toEscape = "http://app.3-tree.com/quonda/po-styles.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
    
    func RequestForColorStyleSize(_ url: URL) {
        
        if(auditEditMode == true){
            self.progressBarDisplayer("Loading. . .", true)
        }
//        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
//        let today = NSDate()
        //        self.bodyData = "User=\(defaults.objectForKey("userid")as! String)&Vendor="
        print(url)
        print("KeyStatsAndImages_bodyData= \(self.bodyData!)")
        
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = self.bodyData!.data(using: String.Encoding.utf8)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request as! URLRequest, completionHandler: { (data, response, error) -> Void in
            
//            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
//                print("error")
//                return
//            }
            if (error != nil) {
                print(error)
//                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
              
                DispatchQueue.main.async(execute: {
                    
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                })
                
                let alertView = UIAlertController(title: "Cannot load Po information" , message: "\(error!.localizedDescription)", preferredStyle: .alert)
                let Reload_action: UIAlertAction = UIAlertAction(title: "Reload", style: .default, handler: { (UIAlertAction) -> Void in
                    if(self.auditEditMode == true){
                       // self.progressBarDisplayer("Loading. . .", true)

                    }else{
                    self.progressBarDisplayer("Loading. . .", true)
                    }
                    self.RequestForColorStyleSize(self.getPoInfoUrl())  // i have changed it recently check if it is wrong
                })
                let no_action: UIAlertAction = UIAlertAction(title: "NO", style: .default, handler: { (UIAlertAction) -> Void in
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                    })
                
                alertView.addAction(Reload_action)
                alertView.addAction(no_action)
                self.present(alertView, animated: true, completion: nil)
                
            }
            else if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                
                print(json)
                
                
                
                
                self.dictionary = self.parseJSON(json)
                
                print("information against Po = \(self.dictionary)")
                
                if ("\(self.dictionary["Message"]!)".contains("No matching")){
                    
                    // - - - Disable user interactions on selectColorBtn and selectSizeBtn - - - -
                    
                    self.selectColor.isUserInteractionEnabled = false
                    self.selectSize.isUserInteractionEnabled = false
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    print("Message = \(self.dictionary["Message"]!)")
                    self.popupMessage("\(self.dictionary["Message"]!)")
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.activityIndicator.stopAnimating()
                        self.messageFrame.removeFromSuperview()
                    })
                }else if ("\(self.dictionary["Status"]!)" == "OK"){
                    
                    if let ProductCode = self.dictionary["ProductCode"]{
                        
                        DispatchQueue.main.async(execute: {
                        self.productCodeLabel.text = ProductCode as? String
                        })
                        //                        let pos = posList as! [AnyObject]
                        //                        var strrr = [String]()
                        //                        for item in pos{
                        //                            strrr.append(item["id"] as! String)
                        //                            self.selectedPoID = strrr.joined(separator: ",")
                        //                        }
                    }
                    
                    if let ItemNumber = self.dictionary["ItemNumber"]{
                        DispatchQueue.main.async(execute: {
                        self.itemNoLabel.text = ItemNumber as? String
                        })
                    }
                    
                    if (self.dictionary["Styles"]! as AnyObject).count >= 1 {
                        
                        DispatchQueue.main.async(execute: {
                            self.btnSchedule.isUserInteractionEnabled = true
                            self.activityIndicator.stopAnimating()
                            self.messageFrame.removeFromSuperview()
                        })
                        

                        let All_Styles = self.dictionary["Styles"]!  as! [String:AnyObject]
                        print("All Styles= \(All_Styles)")
                        
                        
                        var myArray = [String : AnyObject]()
                        //
                        for countryObj in All_Styles{
                            
                            myArray[countryObj.1 as! String] = countryObj.0 as AnyObject?
                            
                        }
                        print(myArray)
                        var mynewDICT:[Int:[String:AnyObject]]! = [:]
                        
                        // Get the array from the keys property.
                        let copy = myArray.keys
                        // Sort from low to high (alphabetical order).
                        let p = copy.sorted(by: <)
                        print(p)
                        
                        for pi in p{
                            print(pi)
                        }
                        let s = myArray.keys.sorted()
                        print(s)
                        
                        // initialize first element to newline and remove all elements in selectLineArray
                        self.selectStyleArray.removeAll()
                        self.selectStyleArrayIDs.removeAll()
                        
                        // Loop over sorted keys.
                        var i = 0
                        for pin in p{
                            for key in copy {
                                if key == pin{
                                    // Get value for this key.
                                    if let value = myArray[key] {
                                        
                                        mynewDICT[i] = [value as! String : key as AnyObject]
                                        //                                self.items.append(key)
                                        //                                self.itemsID.append(value as! String)
                                        self.selectStyleArray.append(key)
                                        self.selectStyleArrayIDs.append(value as! String)
                                        print("StylesKey = \(key), StylesValue = \(value)")
                                        i+=1
                                    }
                                }
                            }}
                    }
                    
                    if(self.auditEditMode == true){
                        
                        
                        if let styleIndex = self.selectStyleArrayIDs.index(of:self.styleSelectedPos){
                        
                    self.saveStyleTextEdit(self.selectStyleArray[styleIndex], strID:self.styleSelectedPos)
                            
                        }else{
                            self.styleLabel.text = "Select Style #"
                            //self.productCodeLabel.text =  "Select product Code"
                        }
                    }
                    else{
                        print(self.selectStyleArray,self.selectStyleArrayIDs)
                        if(self.selectStyleArray.count == 1){
                            self.saveStyleTextEdit(self.selectStyleArray[0], strID:self.selectStyleArrayIDs[0])
                        }else{
                            DispatchQueue.main.async(execute: {

                            self.styleLabel.text = "Select Style #"
                            })
                        }
                    }

                    // now get list of colors
                    if (self.dictionary["Colors"]! as AnyObject).count >= 1 {
                        
                        DispatchQueue.main.async(execute: {
                            
                            self.activityIndicator.stopAnimating()
                            self.messageFrame.removeFromSuperview()
                        })
                        let All_Colors = self.dictionary["Colors"]!  as! [String:AnyObject]
                        print("All Colors= \(All_Colors)")
                        
                        
                        var myArray = [String : AnyObject]()
                        //
                        for countryObj in All_Colors{
                            
                            myArray[countryObj.1 as! String] = countryObj.0 as AnyObject?
                            
                        }
                        print(myArray)
                        var mynewDICT:[Int:[String:AnyObject]]! = [:]
                        
                        // Get the array from the keys property.
                        let copy = myArray.keys
                        // Sort from low to high (alphabetical order).
                        let p = copy.sorted(by: <)
                        print(p)
                        
                        for pi in p{
                            print(pi)
                        }
                        let s = myArray.keys.sorted()
                        print(s)
                        
                        // initialize first element to newline and remove all elements in selectLineArray
                       
                        Coloritems = []
                        ColoritemsID = []
                        
                        // Loop over sorted keys.
                        var i = 0
                        for pin in p{
                            for key in copy {
                                if key == pin{
                                    // Get value for this key.
                                    if let value = myArray[key] {
                                        
                                        mynewDICT[i] = [value as! String : key as AnyObject]
                                        
                                        Coloritems.append(key)
                                        ColoritemsID.append(value as! String)

                                        print("ColorsKey = \(key), ColorsValue = \(value)")
                                        i+=1
                                    }
                                }
                            }}
                        print("Coloritems = \(Coloritems)")
                    }
                    if(self.auditEditMode == true && self.shouldReturn == false){
                        for (key, value) in self.colorsArray {
                            //let keyValue = Int(key)
                            print("\(key) -> \(value)")
                            for item in 0..<Coloritems.count{
                                if(value as! String == Coloritems[item]){
                                    let indexPath = IndexPath(row: item, section: 0)
                                    ColorsData[indexPath] = value as? String
                                    ColorsDataID[indexPath] = "\(item)"
                                }
                            }
                        }
                        DefectsColorsData0 = ColorsData as [IndexPath : String]
                        if !DefectsColorsData0.isEmpty{
                            if !ColorsData.isEmpty && !ColorsDataID.isEmpty{
                                //            PICK_DEFECT_AREA.text = ""
                                //            self.PICK_DEFECT_AREA_TEXT = ""
                                
                                self.selectedColorsIDs = []
                                for id in ColorsDataID{
                                    print(id.1)
                                    print("-------")
                                    
                                    self.selectedColorsIDs.append(id.1)// = id.1
                                }
                                // now convert selectedColorsIDs to a simple string format
                                self.colorsIDsString = ""
                                
                                for i in 0 ..< self.selectedColorsIDs.count{
                                    self.colorsIDsString = self.colorsIDsString+self.selectedColorsIDs[i]
                                    
                                    if i < self.selectedColorsIDs.count - 1 {
                                        self.colorsIDsString = self.colorsIDsString+","
                                    }
                                    
                                }
                                print("colorsIDsString = \(self.colorsIDsString)")
                                self.colorsLabel.text = ""
                                for d in ColorsData{
                                    print(d.1)
                                    print("-------")
                                    //                self.PICK_DEFECT_AREA_TEXT = d.1
                                    self.colorsLabel.text = self.colorsLabel.text!+d.1+","
                                }
                                ColorsDataID.removeAll()
                                ColorsData.removeAll()
                                
                                let a = Array(self.selectedColorsIDs)
                                print("self.selectedColorsIDs = \(a)")
                                
                            }
                        }
                        else{
                            self.colorsLabel.text = "Select Color(s)"
                            self.colorsIDsString = ""
                        }


                    }
//                    if(self.auditEditMode == true){
//                        var arr = [String]()
//                        print(Coloritems)
//                        print(ColoritemsID)
//                        print(self.colorsArray)

//                        for item in self.colorsArray {
//                            let color = item as! String
//                            if(ColoritemsID.contains(color)){
//                                let colorIndex = ColoritemsID.index(of:color)
//                                let colorID = Coloritems[colorIndex!]
//                                //let keyValue = Int(colorID)
//                                let indexPath = IndexPath(row: colorIndex!, section: 0)
//                                ColorsData[indexPath] = colorID
//                                ColorsDataID[indexPath] = color
//                                arr.append("\(colorID)")
//                                print("\(colorID) -> \(color)")
//                                
//                            }
//                            
//                        }
//                        DefectsColorsData0 = ColorsData as [IndexPath : String]
//
//                    self.colorsLabel.text = arr.joined(separator: ",")
//
//
//                    }
                    // now get list of sizes
                    if (self.dictionary["Sizes"]! as AnyObject).count >= 1 {
                        
                        self.activityIndicator.stopAnimating()
                        self.messageFrame.removeFromSuperview()
                        

                        
                        let All_Sizes = self.dictionary["Sizes"]!  as! [String:AnyObject]
                        print("All Sizes= \(All_Sizes)")
                        
                        
                        var myArray = [String : AnyObject]()
                        //
                        for countryObj in All_Sizes{
                            
                            myArray[countryObj.1 as! String] = countryObj.0 as AnyObject?
                            
                        }
                        print(myArray)
                        var mynewDICT:[Int:[String:AnyObject]]! = [:]
                        
                        // Get the array from the keys property.
                        let copy = myArray.keys
                        // Sort from low to high (alphabetical order).
                        let p = copy.sorted(by: <)
                        print(p)
                        
                        for pi in p{
                            print(pi)
                        }
                        let s = myArray.keys.sorted()
                        print(s)
                        
                        // initialize first element to newline and remove all elements in selectLineArray
                        
                        Sizeitems = []
                        SizeitemsID = []
                        
                        // Loop over sorted keys.
                        var i = 0
                        for pin in p{
                            for key in copy {
                                if key == pin{
                                    // Get value for this key.
                                    if let value = myArray[key] {
                                        
                                        mynewDICT[i] = [value as! String : key as AnyObject]
                                        
                                        Sizeitems.append(key)
                                        SizeitemsID.append(value as! String)
                                        
                                        print("SizesKey = \(key), SizesValue = \(value)")
                                        i+=1
                                    }
                                }
                            }}
                        print("Sizesitems = \(Sizeitems)")
                    }
                    if(self.auditEditMode == true && self.shouldReturn == false){
                        for (key, value) in self.sizesArray {
                            //let keyValue = Int(key)
                            print("\(key) -> \(value)")
                            for item in 0..<Sizeitems.count{
                                if(value as! String == Sizeitems[item]){
                                    let indexPath = IndexPath(row: item, section: 0)
                                    SizesData[indexPath] = value as? String
                                    SizesDataID[indexPath] = "\(key)"
                                    //sizesIDsString = SizesDataID[indexPath].joi

                                }
                            }
                        }
                        DefectsSizesData0 = SizesData as [IndexPath : String]
                        if !DefectsSizesData0.isEmpty{
                            if !SizesData.isEmpty && !SizesDataID.isEmpty{
                                
                                self.selectedSizesIDs = []
                                for id in SizesDataID{
                                    print(id.1)
                                    print("-------")
                                    
                                    self.selectedSizesIDs.append(id.1)// = id.1
                                }
                                
                                // now convert selectedColorsIDs to a simple string format
                                self.sizesIDsString = ""
                                
                                for i in 0 ..< self.selectedSizesIDs.count{
                                    self.sizesIDsString = self.sizesIDsString+self.selectedSizesIDs[i]
                                    
                                    if i < self.selectedSizesIDs.count - 1 {
                                        self.sizesIDsString = self.sizesIDsString+","
                                    }
                                    
                                }
                                print("sizesIDsString = \(self.sizesIDsString)")
                                self.sizesLabel.text = ""
                                for d in SizesData{
                                    print(d.1)
                                    print("-------")
                                    //                self.PICK_DEFECT_AREA_TEXT = d.1
                                    self.sizesLabel.text = self.sizesLabel.text!+d.1+","
                                }
                                SizesDataID.removeAll()
                                SizesData.removeAll()
                            }
                        }
                        else{
                            self.sizesLabel.text = "Select Size(s)"
                            self.sizesIDsString = ""
                        }
                    }


                    // now getting PO id
                    //****** PO id is already has been set above when we select po from suggestions ********
                  print("PoKeysToSend when RequestForColorStyleSize() called =",self.PoKeysToSend)
                    self.selectedPoID = self.PoKeysToSend
                    print("selectedPoID when RequestForColorStyleSize() called =", self.selectedPoID)
                    //self.PoKeysToSend = ""
                    //- - - end getting data - - - - -
                    
                    //******* If a custom Po is written then get Po id ********
                    if let posList = self.dictionary["Pos"]{
                        let pos = posList as! [AnyObject]
                        var strrr = [String]()
                        for item in pos{
                            strrr.append(item["id"] as! String)
                            self.selectedPoID = strrr.joined(separator: ",")
                            //self.autoCompletePossibilities.append(item["id"] as! String)
                        }
                    }
                    
                    
                    print(self.selectedPoID)
                    if self.dictionary["Po"] != nil{
                        if "\(self.dictionary["Po"]!)" != "0" {
                            print("self.dictionary[Po]!   is not nil" )
                            if !"\(self.dictionary["Po"]!)".isEmpty{
                               // self.selectedPoID = "\(self.dictionary["Po"]!)"
                                print("PoKeysToSend when custom Po =",self.selectedPoID)
                                //self.PoKeysToSend = self.selectedPoID
                                //self.//recentlySelectedPoKey.append(selected)
                                // Now disable the userinteraction of po_Text
                                self.Po_Text.isUserInteractionEnabled = true
                            }
                        }
                        
                    }
                    
//                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    DispatchQueue.main.async(execute: {
                        
                        self.activityIndicator.stopAnimating()
                        self.messageFrame.removeFromSuperview()
                    })
                    // - - -Enable the userInteraction of selectColorBtn and selectSizeBtn - - - 
                    
                    self.selectColor.isUserInteractionEnabled = true
                    self.selectSize.isUserInteractionEnabled = true
                    
                }else{
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.activityIndicator.stopAnimating()
                        self.messageFrame.removeFromSuperview()
                    })
                    // - - - Disable user interactions on selectColorBtn and selectSizeBtn - - - - 
                   
                    self.selectColor.isUserInteractionEnabled = false
                    self.selectSize.isUserInteractionEnabled = false
                    
                    self.popupMessage("\(self.dictionary["Message"]!)")
                }
                
            }//if data != nil
            else{
                print("return data is nil")
//                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
                

                let alertView = UIAlertController(title: "No Data found" , message: "Press OK to Retry geting information against Po", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    self.progressBarDisplayer("Loading. . .", true)
                    self.performLoadDataRequestWithURL(self.getUrl())
                })
                let Cancel_action: UIAlertAction = UIAlertAction(title: "No", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                    })

                alertView.addAction(Cancel_action)
                alertView.addAction(ok_action)
                self.present(alertView, animated: true, completion: nil)
            }
            
//            let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
        }) 
//        DispatchQueue.main.async(execute: {
//            
//            self.activityIndicator.stopAnimating()
//            self.messageFrame.removeFromSuperview()
//        })
        task.resume()
    }

    // schedule button
    
    @IBAction func scheduleBtnClicked(_ sender: AnyObject) {
//        Vendor
//        Unit
//        Pos
//        Style
//        Colors
//        SampleSize
//        Sizes
//        Line
//        AuditDate
//        AuditTime
//        Group
//        DeviceId
        var DeviceId = ""
//        if let identifierForVendor = UIDevice.current.identifierForVendor {
//                            DeviceId = identifierForVendor.uuidString
//                        }
        newLine.resignFirstResponder()
        if let uniqueId = KeychainService.loadPassword(){
            print(uniqueId as String)
            DeviceId = uniqueId as String
        }

        print("selectedProductionAuditID \(selectedProductionAuditID)")
        print("selectedAuditStage\(selectedAuditStage)")
        print("timeLabel\(self.timeLabel.text!)")
        print("dateLabel\(self.dateLabel.text!)")
        print("selectedLine\(selectedLine)")
        print("sampleSizeLabel\(self.sampleSizeLabel.text!)")
        print("selectedColorsIDs\(selectedColorsIDs)")
        print("selectedStyleID\(selectedStyleID)")
        print("selectedPoID\(selectedPoID)")
        print("selectedUnitID\(selectedUnitID)")
        print("SelectedVendorID\(SelectedVendorID)")
        print("SelectedBrandID\(SelectedBrandID)")
        print("selectedReportTypeID\(selectedReportTypeID)")
        print("selectedAQLID\(selectedAQLID)")
        print("selectedAQLDataID\(selectedAQLDataID)")

        
        // I have removed "selectedUnitID != nil" check
       
        //&& selectedColorsIDs != nil
        
        if selectedReportTypeID != nil && SelectedBrandID != nil && SelectedVendorID != nil && selectedPoID != nil && selectedStyleID != nil && self.dateLabel.text! != "Select Date" && self.timeLabel.text! != "Select Time" && selectedAuditStage != nil {
            
            // if Hybrid user
            if "\(selectedReportTypeID!)" == "36" && offeredQuantity.text != nil {
                //Hybrid
                
                if selectedAQLID == nil{
                    selectedAQLID = "2"
                }
                if(auditEditMode == true){
                    let lastChar = self.selectedPoID[self.selectedPoID.index(before: self.selectedPoID.endIndex)]
                    // lastChar is "😍"
                    if(lastChar == ","){
                        let endIndex = self.selectedPoID.index(self.selectedPoID.endIndex, offsetBy: -1)
                        self.selectedPoID = self.selectedPoID.substring(to: endIndex)
                    }
                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(auditCode)&ReportType=\(selectedReportTypeID!)&AuditStage=\(selectedAuditStage!)&Brand=\(SelectedBrandID!)&Vendor=\(SelectedVendorID!)&Pos=\(selectedPoID!)&Style=\(selectedStyleID!)&Colors=\(colorsLabel.text!)&Sizes=\(sizesIDsString)&Quantity=\(self.offeredQuantity.text!)&InspectionLevel=\(selectedAQLID!)&AuditDate=\(self.dateLabel.text!)&StartTime=\(self.timeLabel.text!)&EndTime=\(self.endTimeLable.text!)&DeviceId=\(DeviceId)"
                } else{
                    self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&ReportType=\(selectedReportTypeID!)&AuditStage=\(selectedAuditStage!)&Brand=\(SelectedBrandID!)&Vendor=\(SelectedVendorID!)&Pos=\(selectedPoID!)&Style=\(selectedStyleID!)&Colors=\(colorsLabel.text!)&Sizes=\(sizesIDsString)&Quantity=\(self.offeredQuantity.text!)&InspectionLevel=\(selectedAQLID!)&AuditDate=\(self.dateLabel.text!)&AuditTime=\(self.timeLabel.text!)&DeviceId=\(DeviceId)"
                }

//                self.bodyData = "User=\(defaults.objectForKey("userid")as! String)&ReportType=\(selectedReportTypeID)&AuditStage=\(selectedAuditStage)&Brand=\(SelectedBrandID)&Vendor=\(SelectedVendorID)&Pos=\(selectedPoID)&Style=\(selectedStyleID)&Colors=\(colorsIDsString)&Sizes=\(sizesIDsString)&Quantity=\(self.offeredQuantity.text!)&InspectionLevel=\(selectedAQLID)&AuditDate=\(self.dateLabel.text!)&AuditTime=\(self.timeLabel.text!)"
                
                print("body data for scheduleing Hybrid = \(self.bodyData)")
                ScheduleRequest(getScheduleUrl())
                
                
            }else if ("\(selectedReportTypeID!)" == "28"||"\(selectedReportTypeID!)" == "38") && offeredQuantity.text != nil{
                //Controlist
                if selectedAQLID == nil{
                    selectedAQLID = "2"
                }
                if(auditEditMode == true){
                    let lastChar = self.selectedPoID[self.selectedPoID.index(before: self.selectedPoID.endIndex)]
                    // lastChar is "😍"
                    if(lastChar == ","){
                        let endIndex = self.selectedPoID.index(self.selectedPoID.endIndex, offsetBy: -1)
                        self.selectedPoID = self.selectedPoID.substring(to: endIndex)
                    }

                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(auditCode)&ReportType=\(selectedReportTypeID!)&AuditStage=\(selectedAuditStage!)&Brand=\(SelectedBrandID!)&Vendor=\(SelectedVendorID!)&Pos=\(selectedPoID!)&Style=\(selectedStyleID!)&Colors=\(colorsLabel.text!)&Sizes=\(sizesIDsString)&Quantity=\(self.offeredQuantity.text!)&InspectionLevel=\(selectedAQLID!)&Aql=\(lblAQL.text!)&AuditDate=\(self.dateLabel.text!)&StartTime=\(self.timeLabel.text!)&EndTime=\(self.endTimeLable.text!)&DeviceId=\(DeviceId)"
                }
                else{
                    self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&ReportType=\(selectedReportTypeID!)&AuditStage=\(selectedAuditStage!)&Brand=\(SelectedBrandID!)&Vendor=\(SelectedVendorID!)&Pos=\(selectedPoID!)&Style=\(selectedStyleID!)&Colors=\(colorsLabel.text!)&Sizes=\(sizesIDsString)&Quantity=\(self.offeredQuantity.text!)&InspectionLevel=\(selectedAQLID!)&Aql=\(lblAQL.text!)&AuditDate=\(self.dateLabel.text!)&AuditTime=\(self.timeLabel.text!)&DeviceId=\(DeviceId)"
                }
                print("body data for scheduleing Hybrid = \(self.bodyData)")
                ScheduleRequest(getScheduleUrl())

                
            }else if ("\(selectedReportTypeID!)" == "14"||"\(selectedReportTypeID!)" == "34"){
                //Controlist
                if selectedAQLID == nil{
                    selectedAQLID = "2"
                }
                if(auditEditMode == true){
                    let lastChar = self.selectedPoID[self.selectedPoID.index(before: self.selectedPoID.endIndex)]
                    // lastChar is "😍"
                    if(lastChar == ","){
                        let endIndex = self.selectedPoID.index(self.selectedPoID.endIndex, offsetBy: -1)
                        self.selectedPoID = self.selectedPoID.substring(to: endIndex)
                    }

                print("SampleSize=\(self.sampleSizeLabel.text!)")
                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(auditCode)&ReportType=\(selectedReportTypeID!)&AuditStage=\(selectedAuditStage!)&Brand=\(SelectedBrandID!)&Vendor=\(SelectedVendorID!)&Pos=\(selectedPoID!)&Style=\(selectedStyleID!)&Colors=\(colorsLabel.text!)&Sizes=\(sizesIDsString)&Quantity=\(self.offeredQuantity.text!)&SampleSize=\(self.sampleSizeLabel.text!)&InspectionLevel=\(selectedAQLID!)&Aql=\(lblAQL.text!)&AuditDate=\(self.dateLabel.text!)&StartTime=\(self.timeLabel.text!)&EndTime=\(self.endTimeLable.text!)&DeviceId=\(DeviceId)"
                }
                else{
                    
                    self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&ReportType=\(selectedReportTypeID!)&AuditStage=\(selectedAuditStage!)&Brand=\(SelectedBrandID!)&Vendor=\(SelectedVendorID!)&Pos=\(selectedPoID!)&Style=\(selectedStyleID!)&Colors=\(colorsLabel.text!)&Sizes=\(sizesIDsString)&Quantity=\(self.offeredQuantity.text!)&SampleSize=\(self.sampleSizeLabel.text!)&InspectionLevel=\(selectedAQLID!)&Aql=\(lblAQL.text!)&AuditDate=\(self.dateLabel.text!)&AuditTime=\(self.timeLabel.text!)&DeviceId=\(DeviceId)"
                }
                print("body data for scheduleing Hybrid = \(self.bodyData)")
                ScheduleRequest(getScheduleUrl())
                
                
            }else if self.sampleSizeLabel.text! != "Select Sample Size" && selectedLine != nil{
                //AnyOther
                if(auditEditMode == true){
                    let lastChar = self.selectedPoID[self.selectedPoID.index(before: self.selectedPoID.endIndex)]
                    // lastChar is "😍"
                    if(lastChar == ","){
                        let endIndex = self.selectedPoID.index(self.selectedPoID.endIndex, offsetBy: -1)
                        self.selectedPoID = self.selectedPoID.substring(to: endIndex)
                    }

                self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(auditCode)&ReportType=\(selectedReportTypeID!)&AuditStage=\(selectedAuditStage!)&Brand=\(SelectedBrandID!)&Vendor=\(SelectedVendorID!)&Unit=\(selectedUnitID!)&Pos=\(selectedPoID!)&Style=\(selectedStyleID!)&Colors=\(colorsLabel.text!)&Sizes=\(sizesIDsString)&SampleSize=\(self.sampleSizeLabel.text!)&Line=\(selectedLine!)&AuditDate=\(self.dateLabel.text!)&StartTime=\(self.timeLabel.text!)&EndTime=\(self.endTimeLable.text!)&DeviceId=\(DeviceId)&AuditType=\(selectedProductionAuditID!)"
                }
                else{
                   self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&ReportType=\(selectedReportTypeID!)&AuditStage=\(selectedAuditStage!)&Brand=\(SelectedBrandID!)&Vendor=\(SelectedVendorID!)&Unit=\(selectedUnitID!)&Pos=\(selectedPoID!)&Style=\(selectedStyleID!)&Colors=\(colorsLabel.text!)&Sizes=\(sizesIDsString)&SampleSize=\(self.sampleSizeLabel.text!)&Line=\(selectedLine!)&AuditDate=\(self.dateLabel.text!)&AuditTime=\(self.timeLabel.text!)&DeviceId=\(DeviceId)&AuditType=\(selectedProductionAuditID!)"
                }
//                self.bodyData = "User=\(defaults.objectForKey("userid")as! String)&ReportType=\(selectedReportTypeID)&AuditStage=\(selectedAuditStage)&Brand=\(SelectedBrandID)&Vendor=\(SelectedVendorID)&Unit=\(selectedUnitID)&Pos=\(selectedPoID)&Style=\(selectedStyleID)&Colors=\(colorsIDsString)&Sizes=\(sizesIDsString)&SampleSize=\(self.sampleSizeLabel.text!)&Line=\(selectedLine)&AuditDate=\(self.dateLabel.text!)&AuditTime=\(self.timeLabel.text!)"
              
                print("body data for scheduleing = \(self.bodyData)")
                ScheduleRequest(getScheduleUrl())

            }else{
                popupMessage("Audit cannot schedule. \nMake sure all fields are selected.\nThen Try again")
            }
            
        }else{
            popupMessage("Audit cannot schedule. \nMake sure all fields are selected.\nThen Try again")
        }
    }
    
    func getScheduleUrl() -> URL {
        var toEscape = ""
        
        if(auditEditMode == true){
             toEscape = "http://app.3-tree.com/quonda/re-schedule-audit.php"
        }else{
             toEscape = "http://app.3-tree.com/quonda/schedule-audit.php"
                }
//        let toEscape = "http://portal.3-tree.com/api/android/quonda/schedule-audit.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
    
    func ScheduleRequest(_ url: URL) {
        
//        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        self.progressBarDisplayer("Loading. . .", true)
        
//        let today = NSDate()
        //        self.bodyData = "User=\(defaults.objectForKey("userid")as! String)&Vendor="
        
        print("KeyStatsAndImages_bodyData= \(self.bodyData)")
        
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = self.bodyData!.data(using: String.Encoding.utf8)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request as! URLRequest, completionHandler: { (data, response, error) -> Void in
            
//            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
//                print("error")
//                return
//            }
            if (error != nil) {
                print(error)
//                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
                
                let alertView = UIAlertController(title: "Cannot Schedule due to some reason." , message: "\(error!.localizedDescription)", preferredStyle: .alert)
                let Reload_action: UIAlertAction = UIAlertAction(title: "Retry", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    self.progressBarDisplayer("Loading. . .", true)
                    self.performLoadDataRequestWithURL(self.getUrl())
                })
                let no_action: UIAlertAction = UIAlertAction(title: "NO", style: .default, handler:  {(UIAlertAction) -> Void in
                    
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                })
                
                
                alertView.addAction(Reload_action)
                alertView.addAction(no_action)
                self.present(alertView, animated: true, completion: nil)
                
            }
            else if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                
                print(json)
                
                
                self.dictionary = self.parseJSON(json)
                
                print("information when Schedule btn clicked = \(self.dictionary)")
                
                
                if("\(self.dictionary["Status"]!)" == "ERROR"){
                    
                    DispatchQueue.main.async(execute: {
                        self.activityIndicator.stopAnimating()
                        self.messageFrame.removeFromSuperview()
                    })
                    self.popupMessage("\(self.dictionary["Message"]!)")
//                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    
                }
                else if("\(self.dictionary["Status"]!)" == "OK"){
                    DispatchQueue.main.async(execute: {
                        
                        self.activityIndicator.stopAnimating()
                        self.messageFrame.removeFromSuperview()
                    })
                    
                    //                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false    
                    if(self.auditEditMode == true){
                        self.auditEditMode = false
                    self.popupMessage2("Audit Scheduled Update Successfully")
                        var sBody = ""
                        if(self.selectedReportTypeID == "14"||self.selectedReportTypeID == "34"||self.selectedReportTypeID == "28"||self.selectedReportTypeID == "37"||self.selectedReportTypeID == "38"){
                            sBody = "\(self.auditCode) is in \(self.vendorLabel.text!) from \(self.timeLabel.text!) to \(self.endTimeLable.text!) on \(self.dateLabel.text!)";
                            
                        }else{
                            sBody = "\(self.auditCode) is in \(self.vendorLabel.text!) Line \(self.productionLineLabel.text!) from \(self.timeLabel.text!) to \(self.endTimeLable.text!) on \(self.dateLabel.text!)";
                            
                        }
                        self.scheduleLocalNotification(title: "New Audit Job", message: self.dictionary["Notification"] as! String)

                                           }
                    else{
                         var sBody = ""
                        if(self.selectedReportTypeID == "14"||self.selectedReportTypeID == "34"||self.selectedReportTypeID == "28"||self.selectedReportTypeID == "37"||self.selectedReportTypeID == "38"){
                            sBody = "New Audit is in \(self.vendorLabel.text!) from \(self.timeLabel.text!) to \(self.endTimeLable.text!) on \(self.dateLabel.text!)";

                        }else{
                            sBody = "New Audit is in \(self.vendorLabel.text!) Line \(self.productionLineLabel.text!) from \(self.timeLabel.text!) to \(self.endTimeLable.text!) on \(self.dateLabel.text!)";

                        }
self.scheduleLocalNotification(title: "New Audit Job", message: self.dictionary["Notification"] as! String)
                        self.popupMessage2("Audit Scheduled Successfully")
  
                    }
                    
                    // now move on to the Auditors Mode 
                    
//                    if let resultController = self.storyboard!.instantiateViewControllerWithIdentifier("5") as? ManagerModeViewController {
//                        self.presentViewController(resultController, animated: true, completion: nil)
//                    }
                   
                }
                //               } //else part of count<1
            }//if data != nil
            else{
                print("return data is nil")
//                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
               
                
                let alertView = UIAlertController(title: nil , message: "Press OK to Retry ", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    self.progressBarDisplayer("Loading. . .", true)
                    self.performLoadDataRequestWithURL(self.getUrl())
                })
                let Cancel_action: UIAlertAction = UIAlertAction(title: "No", style: .default, handler: { (UIAlertAction) -> Void in
                    
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                })
                
                alertView.addAction(Cancel_action)
                alertView.addAction(ok_action)
                self.present(alertView, animated: true, completion: nil)
            }
            
//            let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
        }) 
        
        task.resume()
    }
    func scheduleLocalNotification(title:String,message:String) {
        let localNotification = UILocalNotification()
        localNotification.fireDate = Date.init(timeIntervalSinceNow: 5)
        localNotification.alertBody = "\(message)"
        localNotification.alertTitle = title
        localNotification.alertAction = "View List"
        localNotification.category = "shoppingListReminderCategory"
        UIApplication.shared.scheduleLocalNotification(localNotification)
        
        // scheduleNotifications()        UIApplication.shared.scheduledLocalNotifications()
        
        
    }
    func popupMessage(_ message:String){
        let alertView = UIAlertController(title: nil , message: message, preferredStyle: .alert)
        let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertView.addAction(ok_action)
        self.present(alertView, animated: true, completion: nil)
    }
    
    func popupMessage2(_ message:String){
        
        let alert:UIAlertController = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.alert)
        self.present(alert, animated: true, completion: nil)
        
        let duration:UInt64 = 3; // duration in seconds
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(duration*NSEC_PER_SEC)) / Double(NSEC_PER_SEC)) { () -> Void in
            alert.dismiss(animated: true, completion: {
              
//                if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
//                    if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
//                        
//                        self.present(resultController, animated: true, completion: nil)
//                    }
//                }else{
                    if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                        
                        self.present(resultController, animated: true, completion: nil)
                    }
                //}

            })
        }
    }
    
    
    func progressBarDisplayer(_ msg:String, _ indicator:Bool ) {
        print(msg)
        self.strLabel = UILabel(frame: CGRect(x: view.frame.midX - 40, y: view.frame.midY - 50, width: 200, height: 50))
        
        self.strLabel.text = msg
        self.strLabel.textColor = UIColor.white
        self.messageFrame = UIView(frame: CGRect(x: 0, y: 0 , width: view.frame.width, height: view.frame.height ))
        
        self.messageFrame.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        if indicator {
            self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
            self.activityIndicator.frame = CGRect(x: view.frame.midX - 80, y: view.frame.midY - 50, width: 50, height: 50)
            self.activityIndicator.startAnimating()
            self.messageFrame.addSubview(activityIndicator)
        }
        self.messageFrame.addSubview(strLabel)
        self.view.addSubview(self.messageFrame)
    }
    
    func saveSelectedSize(_ strText: String, strID: String) {
        print(strText,strID)
        
    }    
    
}
