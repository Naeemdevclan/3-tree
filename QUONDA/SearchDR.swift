//
//  SearchDR.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 29/03/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
var DefectRateData = [IndexPath:String]()
var DefectRateDataID = [IndexPath:String]()


var pDRTappedButtonIndexPath:IndexPath!
var pDRButtonTag:Int!


class SearchDR: UIViewController {

    @IBOutlet weak var DRTable: UITableView!
    
    var dictionary:NSDictionary!
    var jsonFilePath2:URL!
    var created:Bool!
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    
    
    var cell:UITableViewCell!
    //    var pressed:Bool = false
    var selectedButton:NSMutableArray! = NSMutableArray()
//    var pDRTappedButtonIndexPath:NSIndexPath!

    //    var pBrandButtonTag:Int!
    var items: [String] = []//["We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift"]
    var itemsID: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("login.json")
        jsonLoginFilePath = jsonFilePath
        print(jsonLoginFilePath.path)
        
        do {
            var readString: String
            readString = try NSString(contentsOfFile: jsonLoginFilePath.path, encoding: String.Encoding.utf8.rawValue) as String
            print(readString)
            self.showDataWhenOffline(readString)
        } catch let error as NSError {
            print(error.description)
        }
        //        jsonLoginFilePath
        
        for i in 0 ..< (items.count) //yourTableSize = how many rows u got
        {
            selectedButton.add("NO")
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        //        for (var i = 0; i<items.count; i++) //yourTableSize = how many rows u got
        //        {
        //            selectedButton.addObject("NO")
        //        }
        if !data6.isEmpty {
            for d2 in data6{
                selectedButton.replaceObject(at: d2.0.row, with: "YES")
                
                let alreadyCheckedCell:UITableViewCell = self.DRTable.cellForRow(at: d2.0 as IndexPath)!
                //                alreadyCheckedCell.checkBox.selected = true
                
                DefectRateData[d2.0 as IndexPath] = alreadyCheckedCell.textLabel!.text
                DefectRateDataID[d2.0 as IndexPath] = alreadyCheckedCell.detailTextLabel?.text
                //                alreadyCheckedCell.checkBox.setBackgroundImage(UIImage(named: "check.png"), forState: .Selected)
            }
            data6.removeAll()
            self.DRTable.reloadData()
        }
    }
    
    //    func checkBoxTapped(sender: AnyObject) {
    //
    //        let button = sender as! UIButton
    //        let view = button.superview!
    //        self.cell = view.superview as! UITableViewCell
    //        let indexPath = VendorTable.indexPathForCell(cell)
    //
    //        var x = button.tag - 100
    //
    //        if pBrandTappedButtonIndexPath == indexPath{
    //            selectedButton.replaceObjectAtIndex(x, withObject: "NO")
    //            button.selected = false
    //            DefectRateData.removeValueForKey(indexPath!)
    //            pBrandTappedButtonIndexPath = nil
    //            pBrandButtonTag = nil
    //        }
    //        else if pBrandTappedButtonIndexPath != indexPath{
    //            if (pBrandTappedButtonIndexPath != nil && pBrandButtonTag != nil){
    //                let Pcell:UITableViewCell = self.VendorTable.cellForRowAtIndexPath(pBrandTappedButtonIndexPath!)! as! UITableViewCell
    //                selectedButton.replaceObjectAtIndex(pBrandButtonTag, withObject: "NO")
    //                let previousButtonState =  Pcell.contentView.subviews.last//button.selected = false
    //                print((previousButtonState?.tag)!-100)
    //                previousButtonState?.setValue(false, forKey: "selected")
    //                print(previousButtonState?.valueForKey("selected"))
    //                DefectRateData.removeValueForKey(pBrandTappedButtonIndexPath!)
    //            }
    //            pBrandTappedButtonIndexPath = indexPath
    //            selectedButton.replaceObjectAtIndex(x, withObject: "YES")
    //            button.selected = true
    //            DefectRateData[indexPath!] = cell.textLabel!.text
    //            pBrandButtonTag = x
    //        }
    //    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (self.dictionary["DefectRates"]! as AnyObject).count
        //        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        let Ccell = self.DRTable.dequeueReusableCell(withIdentifier: "ccell")!
        //        var checkBox:UIButton = UIButton(frame: CGRect(x: Ccell.frame.width, y: Ccell.frame.height/4, width: 30, height: 30))
        Ccell.detailTextLabel!.text = itemsID[indexPath.row]
        Ccell.detailTextLabel?.isHidden = true
        
        Ccell.textLabel!.text = items[indexPath.row]
        Ccell.textLabel!.font = UIFont(name: Ccell.textLabel!.font.fontName, size: 13)

        //   Ccell.checkBox.layer.cornerRadius = Ccell.checkBox.frame.height/2
        //        checkBox.setBackgroundImage(UIImage(named: "Radio-OFF"), forState: .Normal)
        //        checkBox.setBackgroundImage(UIImage(named: "Radio-ON"), forState: .Selected)
        //
        //        checkBox.tag = indexPath.row+100
        //        checkBox.addTarget(self, action: "checkBoxTapped:", forControlEvents: UIControlEvents.TouchUpInside)
        //
        //        if selectedButton.objectAtIndex(indexPath.row).isEqualToString("NO"){
        //            checkBox.selected = false
        //        }else{
        //            checkBox.selected = true
        //        }
        //        Ccell.contentView.addSubview(checkBox)
        
        if (selectedButton.object(at: indexPath.row) as AnyObject).isEqual(to: "NO"){
            Ccell.accessoryType = .none
        }else {
            Ccell.accessoryType = .checkmark
            Ccell.backgroundColor = UIColor.lightGray
        }
        
        return Ccell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        
        print("selected Index row path\(indexPath.row)")
        
        let x = indexPath.row//button.tag - 100
        let cellPressed = tableView.cellForRow(at: indexPath)!
        
        
        //        if pTappedButtonIndexPath == indexPath{
        //            //            selectedButton.replaceObjectAtIndex(x, withObject: "NO")
        //            ////            button.selected = false
        //            //            cellPressed.accessoryType = .None
        //            //            DefectRateData.removeValueForKey(indexPath)
        //            //
        //            //            pTappedButtonIndexPath = nil
        //            //            pDRButtonTag = nil
        //        }
        //        else
        if pDRTappedButtonIndexPath != indexPath{
            if (pDRTappedButtonIndexPath != nil && pDRButtonTag != nil){
                let Pcell:UITableViewCell = self.DRTable.cellForRow(at: pDRTappedButtonIndexPath!)!
                selectedButton.replaceObject(at: pDRButtonTag, with: "NO")
                Pcell.accessoryType = .none
                DefectRateData.removeValue(forKey: pDRTappedButtonIndexPath!)
                DefectRateDataID.removeValue(forKey: pDRTappedButtonIndexPath!)
            }
            pDRTappedButtonIndexPath = indexPath
            selectedButton.replaceObject(at: x, with: "YES")
            //            button.selected = true
            cellPressed.accessoryType = .checkmark
            DefectRateData[indexPath] = cellPressed.textLabel!.text
            DefectRateDataID[indexPath] = cellPressed.detailTextLabel?.text

            pDRButtonTag = x
            
            //            data3 = DefectRateData
            //            self.dismissViewControllerAnimated(true, completion: nil)
        }
        
        // - - Dismiss the current view - - - - - - -
        data6  = DefectRateData as [IndexPath : String]
        self.dismiss(animated: false, completion: nil)
        self.DRTable.beginUpdates()
        self.DRTable.endUpdates()
    }
    
    func showDataWhenOffline(_ savedData:String?){
        
        if savedData != nil {
            
            self.dictionary = self.parseJSON(savedData!)
            print("\(self.dictionary)")
            
            let All_DefectRates = self.dictionary["DefectRates"]!  as! [String:AnyObject]
            print(All_DefectRates)
            print("Total DefectRates = \(All_DefectRates.count)")
            
            
            var myArray = [String : AnyObject]()
            
            for defect in All_DefectRates{
                
                myArray[defect.1 as! String] = defect.0 as AnyObject?
                
            }
            print(myArray)
            var mynewDICT:[Int:[String:AnyObject]]! = [:]
            // Create a dictionary.
            let animals = ["bird": 0, "zebra": 9, "ant": 1]
            
            // Get the array from the keys property.
            let copy = myArray.keys
            // Sort from low to high (alphabetical order).
            let p = copy.sorted(by: <)
            print(p)
            
            for pi in p{
                print(pi)
            }
            let s = myArray.keys.sorted()
            print(s)
            // Loop over sorted keys.
            var i = 0
            for pin in p{
                for key in copy {
                    if key == pin{
                        // Get value for this key.
                        if let value = myArray[key] {
                            //                            mynewDICT[value as! String] = key
                            mynewDICT[i] = [value as! String : key as AnyObject]
                            self.items.append(key)
                            self.itemsID.append(value as! String)
                            
                            print("Key = \(key), Value = \(value)")
                            i+=1
                        }
                    }
                }}

            
            for DR in All_DefectRates{
//                print(DR)
//                self.items.append(DR.1 as! String)
//                self.itemsID.append(DR.0)
            }
        }
    }
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
