//
//  SearchReportType.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 26/03/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
var ReportTypeData = [IndexPath:String]()
var ReportTypeDataID = [IndexPath:String]()

//var data7 = [NSIndexPath:String]()
var selectedAccessoryButton:NSMutableArray! = NSMutableArray()

class SearchReportType: UIViewController {

    @IBOutlet weak var ReportTypeTable: UITableView!
    //    var data = [Int:String]()
    var dictionary:NSDictionary!
    var jsonFilePath2:URL!
    var created:Bool!
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    
    
    var cell:SearchStageCell!
    //    var pressed:Bool = false
    var previousTappedButtonIndexPath:IndexPath!
//    var selectedAccessoryButton:NSMutableArray! = NSMutableArray()
    
    var items: [String] = []//["We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift"]
    var itemsID: [String] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("login.json")
        jsonLoginFilePath = jsonFilePath
        print(jsonLoginFilePath.path)
        
        do {
            var readString: String
            readString = try NSString(contentsOfFile: jsonLoginFilePath.path, encoding: String.Encoding.utf8.rawValue) as String
            print(readString)
            self.showDataWhenOffline(readString)
        } catch let error as NSError {
            print(error.description)
        }
        //        jsonLoginFilePath
        
        for i in 0 ..< (items.count) //yourTableSize = how many rows u got
        {
            selectedAccessoryButton.add("NO")
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if !data7.isEmpty {
            for d2 in data7{
                selectedAccessoryButton.replaceObject(at: d2.0.row, with: "YES")
                
                let alreadyCheckedCell = self.ReportTypeTable.cellForRow(at: d2.0 as IndexPath)!
                //                alreadyCheckedCell.checkBox.selected = true
                
                ReportTypeData[d2.0 as IndexPath] = alreadyCheckedCell.textLabel!.text
                ReportTypeDataID[d2.0 as IndexPath] = alreadyCheckedCell.detailTextLabel?.text
                //                alreadyCheckedCell.checkBox.setBackgroundImage(UIImage(named: "check.png"), forState: .Selected)
            }
            data7.removeAll()
            self.ReportTypeTable.reloadData()
        }
    }
//    @IBAction func checkBox(sender: AnyObject) {
//        
//        let button = sender as! UIButton
//        let view = button.superview!
//        self.cell = view.superview as! SearchStageCell
//        let indexPath = ReportTypeTable.indexPathForCell(cell)
//        
//        var x = button.tag - 100
//        
//        if button.selected == true{
//            selectedAccessoryButton.replaceObjectAtIndex(x, withObject: "NO")
//            button.selected = false
//            ReportTypeData.removeValueForKey(indexPath!)
//            
//        }
//        else if button.selected == false{
//            selectedAccessoryButton.replaceObjectAtIndex(x, withObject: "YES")
//            button.selected = true
//            ReportTypeData[indexPath!] = cell.checkLabel.text
//        }
//    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (self.dictionary["Reports"]! as AnyObject).count
        //        return items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        let Rcell = self.ReportTypeTable.dequeueReusableCell(withIdentifier: "Rcell")!
//        var checkBox:UIButton = UIButton(type: .Custom)
        
        Rcell.detailTextLabel!.text = itemsID[indexPath.row]
        Rcell.detailTextLabel?.isHidden = true
        
        Rcell.textLabel!.text = items[indexPath.row]
        Rcell.textLabel!.font = UIFont(name: Rcell.textLabel!.font.fontName, size: 13)

//        //   Rcell.checkBox.layer.cornerRadius = Rcell.checkBox.frame.height/2
//        Rcell.checkBox.setBackgroundImage(UIImage(named: "uncheck"), forState: .Normal)
//        Rcell.checkBox.setBackgroundImage(UIImage(named: "check"), forState: .Selected)
        
//        Rcell.checkBox.tag = indexPath.row+100
        
        if (selectedAccessoryButton.object(at: indexPath.row) as AnyObject).isEqual(to: "NO"){
            Rcell.accessoryType = .none
        }else {
            Rcell.accessoryType = .checkmark
        }
//            Rcell.accessoryType = .None
//            Rcell.checkBox.selected = false
//            //            data.removeValueForKey(indexPath)
//        }else{
//            Rcell.accessoryType = .None
//            Rcell.checkBox.selected = true
//            //            data[indexPath] = Rcell.checkLabel.text
//        }
        
        return Rcell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        
        print("selected Index row path\(indexPath.row)")
        
//        let button = sender as! UIButton
//        let view = button.superview!
//        self.cell = view.superview as! SearchStageCell
//        let indexPath = ReportTypeTable.indexPathForCell(cell)
        
        let x = indexPath.row//button.tag - 100
        let cellPressed = tableView.cellForRow(at: indexPath)!
        
        if cellPressed.accessoryType == .none{
            cellPressed.accessoryType = .checkmark
            selectedAccessoryButton.replaceObject(at: x, with: "YES")
            ReportTypeData[indexPath] = cellPressed.textLabel!.text
            ReportTypeDataID[indexPath] = cellPressed.detailTextLabel?.text

        
        }else if cellPressed.accessoryType == .checkmark{
            cellPressed.accessoryType = .none
            selectedAccessoryButton.replaceObject(at: x, with: "NO")
            ReportTypeData.removeValue(forKey: indexPath)
            ReportTypeDataID.removeValue(forKey: indexPath)
        }
            //        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
//        if indexPath.row != lastSelectedIndexPath?.row {
//            if let lastSelectedIndexPath = lastSelectedIndexPath {
//                let oldCell = tableView.cellForRowAtIndexPath(lastSelectedIndexPath)
//                oldCell?.accessoryType = .None
//            }
//            
//            let newCell = tableView.cellForRowAtIndexPath(indexPath)
//            newCell?.accessoryType = .Checkmark
//            
//            lastSelectedIndexPath = indexPath
//        }
    }
    func showDataWhenOffline(_ savedData:String?){
        
        if savedData != nil {
            
            self.dictionary = self.parseJSON(savedData!)
            print("\(self.dictionary)")
            let All_Reports = self.dictionary["Reports"]!  as! [AnyObject]
            print("Total Reports:")
            print((self.dictionary["Reports"]! as AnyObject).count)
            
            //            print("Total Login Data = \(self.dictionary["All_Countries"]!.count)")
            
            //            self.totalRowsInTable = self.dictionary["Audits"]!.count
            
            var myArray = [String : AnyObject]()
            
            for report in All_Reports{
                
                myArray[report["name"] as! String] = report["id"] as AnyObject?
                
            }
            print(myArray)
            var mynewDICT:[Int:[String:AnyObject]]! = [:]
            // Create a dictionary.
            let animals = ["bird": 0, "zebra": 9, "ant": 1]
            
            // Get the array from the keys property.
            let copy = myArray.keys
            // Sort from low to high (alphabetical order).
            let p = copy.sorted(by: <)
            print(p)
            
            for pi in p{
                print(pi)
            }
            let s = myArray.keys.sorted()
            print(s)
            // Loop over sorted keys.
            var i = 0
            for pin in p{
                for key in copy {
                    if key == pin{
                        // Get value for this key.
                        if let value = myArray[key] {
                            //                            mynewDICT[value as! String] = key
                            mynewDICT[i] = [value as! String : key as AnyObject]
                            self.items.append(key)
                            self.itemsID.append(value as! String)
                            
                            print("Key = \(key), Value = \(value)")
                            i+=1
                        }
                    }
                }}

            for report in All_Reports{
//                print(report.1)
//                self.items.append(report.1)
//                self.itemsID.append(report.0)
            }
        }
    }
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    } 
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
