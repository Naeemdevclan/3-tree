//
//  SearchStageCell.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 18/03/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class SearchStageCell: UITableViewCell {

    @IBOutlet weak var checkBox: UIButton!
    @IBOutlet weak var checkLabel: UILabel!
//    var pressed:Bool = false
//    @IBOutlet weak var checkBoxImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        checkBox.setBackgroundImage(UIImage(named: "uncheck.png"), forState: UIControlState.Normal)
        checkBox.isSelected = false
//        checkBox.layer.cornerRadius = checkBox.frame.height/2
//        checkBoxImage.image =  UIImage(named: "uncheck.png")
        

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
