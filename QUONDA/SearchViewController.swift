//
//  SearchViewController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 15/03/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
var data2 = [IndexPath:String]()
var data3 = [IndexPath:String]()
var data4 = [IndexPath:String]()
var data5 = [IndexPath:String]()
var data6 = [IndexPath:String]()
var data7 = [IndexPath:String]()
var data8 = [IndexPath:String]()


class SearchViewController: UIViewController,UITabBarDelegate,UITextFieldDelegate {

    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    weak var tab1vc: UIViewController!
    weak var tab2vc: UIViewController!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!

    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    
    @IBOutlet weak var search: UILabel!
//    var datePicker1:UIDatePicker = UIDatePicker()
    @IBOutlet weak var StartDate: UILabel!
    @IBOutlet weak var EndDate: UILabel!
    @IBOutlet weak var Country: UILabel!
    
    @IBOutlet weak var Vendor: UILabel!
    @IBOutlet weak var Brand: UILabel!
    @IBOutlet weak var DefectRate: UILabel!
    
    @IBOutlet weak var ReportType: UILabel!
    @IBOutlet weak var AuditResult: UILabel!
    @IBOutlet weak var AuditCode: UITextField!
    @IBOutlet weak var PO: UITextField!
    @IBOutlet weak var Style: UITextField!
    
    var vendorID:AnyObject = "" as AnyObject
    var CountryID:AnyObject = "" as AnyObject
    var BrandID:AnyObject = "" as AnyObject
    var DefectRateID:AnyObject = "" as AnyObject
    var StageID:AnyObject = "" as AnyObject
    var ReportID:AnyObject = "" as AnyObject
    var AuditResultID:AnyObject = "" as AnyObject
    
    @IBAction func SearchButton(_ sender: AnyObject) {
        self.Search()
//        let displayResultsController = self.storyboard?.instantiateViewControllerWithIdentifier("displayResults") as! DisplaySearchResult
//        self.navigationController?.pushViewController(displayResultsController, animated: true)
        
//        let Btnback = UIBarButtonItem(title: "back", style: .Done, target: self, action: "Search")
//        self.navigationController?.navigationItem.backBarButtonItem = Btnback
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       
        AuditCode.resignFirstResponder()
        PO.resignFirstResponder()
        Style.resignFirstResponder()
        
        return true
    }
    
    var selectedStages:[String] = []
    
    override func viewWillAppear(_ animated: Bool) {
        print("view will appear")
        
//        if !StageData.isEmpty && !StageDataID.isEmpty{
//            self.search.text = ""
//            self.StageID = ""
//            var myStageIDstring:[String] = []
//            for id in StageDataID{
//                print(id.1)
//                print("-------")
//                myStageIDstring.append(id.1)
//            }
//                self.StageID = myStageIDstring.joinWithSeparator(",")
//                print(self.StageID)
////            }else{
////                self.StageID = ""
////            }
//
////            for d in StageData{
////                print(d.1)
////                print("-------")
////                //            list = list+d.1+","
////                self.search.text = self.search.text!+d.1+","
////            }
////            StageData.removeAll()
//        }else{
//            self.StageID = ""
//            self.search.text = "All Stages"
//        }
        
        
        if !CountriesData.isEmpty && !CountriesDataID.isEmpty{
            self.Country.text = ""
            self.CountryID = "" as AnyObject
//            self.Country.tag = 0000
            for id in CountriesDataID{
                print(id.1)
                print("-------")
                //            list = list+d.1+","
//                self.Country.tag = Int(id.1)!
                self.CountryID = id.1 as AnyObject
            }
            for d in CountriesData{
                print(d.1)
                print("-------")
                //            list = list+d.1+","
                self.Country.text = d.1
            }
            CountriesData.removeAll()
        }

        
        if !VendorsData.isEmpty && !VendorsDataID.isEmpty{
            self.Vendor.text = ""
//            self.Vendor.tag = 00000
            self.vendorID = "" as AnyObject
            for id in VendorsDataID{
                print(id.1)
                self.vendorID = id.1 as AnyObject
//                self.Vendor.tag = Int(id.1)!
                print(Vendor.tag)
            }
            for d in VendorsData{
                print(d.1)
                print("-------")
                //            list = list+d.1+","
                self.Vendor.text = d.1
            }
            VendorsData.removeAll()
        }
//        if !VendorsData.isEmpty{
//            self.Vendor.text = ""
//            for d in VendorsData{
//                print(d.1)
//                print("-------")
//                //            list = list+d.1+","
//                self.Vendor.text = d.1
//            }
//            VendorsData.removeAll()
//        }
        
        if !BrandsData.isEmpty && !BrandsDataID.isEmpty{
            self.Brand.text = ""
            self.BrandID = "" as AnyObject
            for id in BrandsDataID{
                print(id.1)
                self.BrandID = id.1 as AnyObject
            }
            for d in BrandsData{
                print(d.1)
                print("-------")
                //            list = list+d.1+","
                self.Brand.text = d.1
            }
            BrandsData.removeAll()
        }
        
        if !DefectRateData.isEmpty && !DefectRateDataID.isEmpty{
            self.DefectRate.text = ""
            self.DefectRateID = "" as AnyObject
            for id in DefectRateDataID{
                print(id.1)
                print("-------")
                //            list = list+d.1+","
                self.DefectRateID = id.1 as AnyObject
            }

            for d in DefectRateData{
                print(d.1)
                print("-------")
                //            list = list+d.1+","
                self.DefectRate.text = d.1
            }
            DefectRateData.removeAll()
        }
        
//        if !ReportTypeData.isEmpty{
//            self.ReportType.text = ""
//            for d in ReportTypeData{
//                print(d.1)
//                print("-------")
//                //            list = list+d.1+","
//                self.ReportType.text = self.ReportType.text!+d.1+","
//            }
//            ReportTypeData.removeAll()
//        }else{
//            self.ReportType.text = "All Types"
//        }
        
        if !AuditResultData.isEmpty && !AuditResultDataID.isEmpty{
            self.AuditResult.text = ""
            self.AuditResultID = "" as AnyObject
            for id in AuditResultDataID{
                print(id.1)
                print("-------")
                //            list = list+d.1+","
                self.AuditResultID = id.1 as AnyObject
            }
            for d in AuditResultData{
                print(d.1)
                print("-------")
                //            list = list+d.1+","
                self.AuditResult.text = d.1
            }
            AuditResultData.removeAll()
        }
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        print("view did appear")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AuditCode.delegate = self
        PO.delegate = self
        Style.delegate = self
        
        // clears all the data related to navigation views
        
        StageData.removeAll()
        StageDataID.removeAll()
        data2.removeAll()
        selectedStageAccessoryButton.removeAllObjects()
        
        data3.removeAll()
        data4.removeAll()
        data5.removeAll()
        data6.removeAll()
        
        ReportTypeData.removeAll()
        ReportTypeDataID.removeAll()
        data7.removeAll()
        selectedAccessoryButton.removeAllObjects()
        
        data8.removeAll()

        
//        navigationItem.backBarButtonItem.
        // Do any additional setup after loading the view.
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Selected Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)

        
      // - - - - - - - -SEARCH VIEW ITEMS - - - - - - - - -
        // - - Default values for SEARCH ITEMS- - - - -
        
        self.Country.text = "All Countries"
        self.search.text = "All Stages"
        self.StartDate.text = "Any Date"
        self.EndDate.text = "Any Date"
        self.Vendor.text = "All Vendors"
        self.Brand.text = "All Brands"
        self.DefectRate.text = "Any DR"
        self.ReportType.text = "All Types"
        self.AuditResult.text = "All Results"
        
        
        // add gesture to search Label
        var tapGesture = UITapGestureRecognizer(target: self, action: #selector(SearchViewController.handlePopUp(_:)))
        self.search.isUserInteractionEnabled=true
        self.search.addGestureRecognizer(tapGesture)
        
        // add gesture to StartDate Label
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(SearchViewController.onDatePickerValueChanged(_:)))
        self.StartDate.isUserInteractionEnabled=true
        self.StartDate.addGestureRecognizer(tapGesture)
        
        // add gesture to EndDate Label
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(SearchViewController.onDatePickerValueChanged(_:)))
        self.EndDate.isUserInteractionEnabled=true
        self.EndDate.addGestureRecognizer(tapGesture)
        
        // add gesture to country Label
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(SearchViewController.handlePopUp(_:)))
        self.Country.isUserInteractionEnabled=true
        self.Country.addGestureRecognizer(tapGesture)
        
        // add gesture to Vendor Label
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(SearchViewController.handlePopUp(_:)))
        self.Vendor.isUserInteractionEnabled=true
        self.Vendor.addGestureRecognizer(tapGesture)
        
        // add gesture to Brand Label
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(SearchViewController.handlePopUp(_:)))
        self.Brand.isUserInteractionEnabled=true
        self.Brand.addGestureRecognizer(tapGesture)
        
        // add gesture to DefectRate Label
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(SearchViewController.handlePopUp(_:)))
        self.DefectRate.isUserInteractionEnabled=true
        self.DefectRate.addGestureRecognizer(tapGesture)
        
        // add gesture to ReportType Label
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(SearchViewController.handlePopUp(_:)))
        self.ReportType.isUserInteractionEnabled=true
        self.ReportType.addGestureRecognizer(tapGesture)
        
        // add gesture to AuditResult Label
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(SearchViewController.handlePopUp(_:)))
        self.AuditResult.isUserInteractionEnabled=true
        self.AuditResult.addGestureRecognizer(tapGesture)
    }
    
    
    func onDatePickerValueChanged(_ sender:UITapGestureRecognizer){
        
        DatePickerDialog().show("SelectDate", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            print(sender.view?.tag)
            if sender.view?.tag == 2{
                self.EndDate.text = "\(date.formatted)"
            }else if sender.view?.tag == 1{
                self.StartDate.text = "\(date.formatted)"
            }
        }
    }
    
    var popoverContent:AnyObject!
    
    func handlePopUp(_ sender:UITapGestureRecognizer){
       
        self.Country.tag = 3
        self.Vendor.tag = 4

        print("searchHandler is running now..")
        var nav:UINavigationController!
        
        if sender.view?.tag == 0{
            
            popoverContent = storyboard!.instantiateViewController(withIdentifier: "searchStage") as! SearchStage
            nav = UINavigationController(rootViewController: popoverContent as! SearchStage)
            let btnDone = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(SearchViewController.dismissStages))
            nav.topViewController?.navigationItem.rightBarButtonItem = btnDone
            nav.modalPresentationStyle = UIModalPresentationStyle.popover
            let popover = nav.popoverPresentationController
            popover!.sourceView = self.view

        }
        else if sender.view?.tag == 3{
            
//               var searchContryVC
            popoverContent = storyboard!.instantiateViewController(withIdentifier: "searchCountry") as! SearchCountry
//            presentViewController(searchContryVC, animated: true, completion: nil)
            nav = UINavigationController(rootViewController: popoverContent as! SearchCountry)
            let btnClear = UIBarButtonItem(title: "Clear", style: .done, target: self, action: #selector(SearchViewController.dismissCountries))
            nav.topViewController?.navigationItem.rightBarButtonItem = btnClear
            nav.modalPresentationStyle = UIModalPresentationStyle.popover
            let popover = nav.popoverPresentationController
            popover!.sourceView = self.view

        }else if sender.view?.tag == 4{
            
            popoverContent = storyboard!.instantiateViewController(withIdentifier: "SearchVendor") as! SearchVender
            nav = UINavigationController(rootViewController: popoverContent as! SearchVender)
            let btnClear = UIBarButtonItem(title: "Clear", style: .done, target: self, action: #selector(SearchViewController.dismissVendors))
            nav.topViewController?.navigationItem.rightBarButtonItem = btnClear
            nav.modalPresentationStyle = UIModalPresentationStyle.popover
            let popover = nav.popoverPresentationController
            popover!.sourceView = self.view
            
        }else if sender.view?.tag == 5{
            
            popoverContent = storyboard!.instantiateViewController(withIdentifier: "searchBrand") as! SearchBrand
            nav = UINavigationController(rootViewController: popoverContent as! SearchBrand)
            let btnClear = UIBarButtonItem(title: "Clear", style: .done, target: self, action: #selector(SearchViewController.dismissBrand))
            nav.topViewController?.navigationItem.rightBarButtonItem = btnClear
            nav.modalPresentationStyle = UIModalPresentationStyle.popover
            let popover = nav.popoverPresentationController
            popover!.sourceView = self.view
            
        }else if sender.view?.tag == 6{
            
            popoverContent = storyboard!.instantiateViewController(withIdentifier: "searchDR") as! SearchDR
            nav = UINavigationController(rootViewController: popoverContent as! SearchDR)
            let btnClear = UIBarButtonItem(title: "Clear", style: .done, target: self, action: #selector(SearchViewController.dismissDR))
            nav.topViewController?.navigationItem.rightBarButtonItem = btnClear
            nav.modalPresentationStyle = UIModalPresentationStyle.popover
            let popover = nav.popoverPresentationController
            popover!.sourceView = self.view
            
        }else if sender.view?.tag == 7{
            
            popoverContent = storyboard!.instantiateViewController(withIdentifier: "searchReportType") as! SearchReportType
            nav = UINavigationController(rootViewController: popoverContent as! SearchReportType)
            let btnDone = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(SearchViewController.dismissReportType))
            nav.topViewController?.navigationItem.rightBarButtonItem = btnDone
            nav.modalPresentationStyle = UIModalPresentationStyle.popover
            let popover = nav.popoverPresentationController
            popover!.sourceView = self.view
            
        }else if sender.view?.tag == 8{
            
            popoverContent = storyboard!.instantiateViewController(withIdentifier: "searchAR") as! SearchAuditResult
            nav = UINavigationController(rootViewController: popoverContent as! SearchAuditResult)
            let btnClear = UIBarButtonItem(title: "Clear", style: .done, target: self, action: #selector(SearchViewController.dismissAR))
            nav.topViewController?.navigationItem.rightBarButtonItem = btnClear
            nav.modalPresentationStyle = UIModalPresentationStyle.popover
            let popover = nav.popoverPresentationController
            popover!.sourceView = self.view
            
        }
//        nav = UINavigationController(rootViewController: popoverContent)
        
//        let btnDone = UIBarButtonItem(title: "Done", style: .Done, target: self, action: "dismiss")
//        nav.topViewController?.navigationItem.rightBarButtonItem = btnDone
        
//        nav.modalPresentationStyle = UIModalPresentationStyle.Popover
//        var popover = nav.popoverPresentationController
        
       
//        popover!.sourceView = self.view
        if let nav = nav{
            self.present(nav, animated: true, completion: nil)
        }
    }
    
//    override func drawTextInRect(rect: CGRect) {
//        let insets = UIEdgeInsets.init(top: 0, left: 5, bottom: 0, right: 5)
//        super.drawTextInRect(UIEdgeInsetsInsetRect(rect, insets))
//    }
    var displaySearchResult:DisplaySearchResult!
    func Search(){
     print("searchButton is pressed now..")
        displaySearchResult = storyboard!.instantiateViewController(withIdentifier: "displayResults") as! DisplaySearchResult
        let nav = UINavigationController(rootViewController: displaySearchResult)
        
        let btnDone = UIBarButtonItem(title: "back", style: .plain, target: self, action: #selector(SearchViewController.dismiss2))
        nav.topViewController?.navigationItem.leftBarButtonItem = btnDone
        
        nav.modalPresentationStyle = UIModalPresentationStyle.popover
        let popover = nav.popoverPresentationController

//        popoverContent.preferredContentSize = CGSizeMake(500,600)
//        popover!.delegate = self
        popover!.sourceView = self.view
//        popover!.sourceRect = CGRectMake(100,100,0,0)
        
        self.present(nav, animated: true, completion: nil)
    }
    
//    if !StageData.isEmpty && !StageDataID.isEmpty{
//    self.search.text = ""
//    self.StageID = ""
//    var myStageIDstring:[String] = []
//    for id in StageDataID{
//    print(id.1)
//    print("-------")
//    myStageIDstring.append(id.1)
//    }
//    self.StageID = myStageIDstring.joinWithSeparator(",")
//    print(self.StageID)
    
    func dismissStages() {
        if !StageData.isEmpty && !StageDataID.isEmpty{
            self.search.text = ""
            self.StageID = "" as AnyObject
            var myStageIDstring:[String] = []
            
            for id in StageDataID{
                print(id.1)
                print("-------")
                myStageIDstring.append(id.1)
            }
            self.StageID = myStageIDstring.joined(separator: ",") as AnyObject
            print(self.StageID)

//            var list:String
//        print(StageData)
            
        for d in StageData{
            print(d.1)
            print("-------")
            self.search.text = self.search.text!+d.1+","
        }
        data2 = StageData as [IndexPath : String]
        StageData.removeAll()
        popoverContent.dismiss(animated: true, completion: nil)
        }else{
            self.search.text = "All Stages"
            self.StageID = "" as AnyObject
            popoverContent.dismiss(animated: true, completion: nil)
        }
    }
    
    func dismissCountries() {
        self.Country.text = "All Countries"
        CountriesData.removeAll()
        data3.removeAll()
//        var list:String = ""
//        print(CountriesData)
//        for d in CountriesData{
//            print(d.1)
//            print("-------")
//            self.Country.text = d.1
//        }
//        print(list)
//        
//        data3 = CountriesData
//        
        popoverContent.dismiss(animated: true, completion: nil)
        
    }
    
    func dismissVendors() {
        self.Vendor.text = "All Vendors"
        VendorsData.removeAll()
        data4.removeAll()
//        var list:String = ""
//        print(VendorsData)
//        for d in VendorsData{
//            print(d.1)
//            print("-------")
//            self.Vendor.text = d.1
//        }
//        print(list)
//        
//        data4 = VendorsData
        
        popoverContent.dismiss(animated: true, completion: nil)
    }
    
    func dismissBrand() {
        self.Brand.text = "All Brands"
        BrandsData.removeAll()
        data5.removeAll()
        
        popoverContent.dismiss(animated: true, completion: nil)
    }
    func dismissDR() {
        self.DefectRate.text = "Any DR"
        DefectRateData.removeAll()
        data6.removeAll()
        
        popoverContent.dismiss(animated: true, completion: nil)
    }

//    func dismissReportType() {
//        var list:String = ""
//        print(ReportTypeData)
//        for d in ReportTypeData{
//            print(d.1)
//            print("-------")
//            self.ReportType.text = d.1
//        }
//        print(list)
//        
//        data7 = ReportTypeData
//        
//        popoverContent.dismissViewControllerAnimated(true, completion: nil)
//    }
    func dismissReportType() {
        if !ReportTypeData.isEmpty && !ReportTypeDataID.isEmpty{
            self.ReportType.text = ""
            self.ReportID = "" as AnyObject
            var myReportIDstring:[String] = []
            
            for id in ReportTypeDataID{
                print(id.1)
                print("-------")
                myReportIDstring.append(id.1)
            }
            self.ReportID = myReportIDstring.joined(separator: ",") as AnyObject
            print(self.ReportID)
            
//            var list:String
            //        print(StageData)
            
            for d in ReportTypeData{
                print(d.1)
                print("-------")
                self.ReportType.text = self.ReportType.text!+d.1+","
            }
            data7 = ReportTypeData as [IndexPath : String]
            ReportTypeData.removeAll()
            popoverContent.dismiss(animated: true, completion: nil)
        }else{
            self.ReportType.text = "All Types"
            self.ReportID = "" as AnyObject
            popoverContent.dismiss(animated: true, completion: nil)
        }
    }
    
    
    
    
    
    
    func dismissAR() {
        self.AuditResult.text = "All Results"
        AuditResultData.removeAll()
        data8.removeAll()
        
        popoverContent.dismiss(animated: true, completion: nil)
    }
    
    
    func dismiss2(){
        displaySearchResult.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
//            defaults.removeObjectForKey("userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
//        case 4:
//            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
//                //                self.dismissViewControllerAnimated(true, completion: nil)
//                presentViewController(resultController, animated: true, completion: nil)
//            }
//            
//            let selectedImage:UIImage = UIImage(named: "Selected QAReport")!
//            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
//            
//            break
        case 5:
            if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }else{
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }
            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
//            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("7") as? SearchViewController {
//                //                self.dismissViewControllerAnimated(true, completion: nil)
//                presentViewController(resultController, animated: true, completion: nil)
//            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        default:
            break
        }
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
       
        if self.AuditCode.text!.isEmpty{
            self.AuditCode.text = ""
        }
        if self.PO.text!.isEmpty{
            self.PO.text = ""
        }
        if self.Style.text!.isEmpty{
            self.Style.text = ""
        }
        if self.Country.text == "All Countries"{
            self.Country.text = ""
        }
        if self.search.text == "All Stages"{
            self.search.text = ""
        }
        if self.StartDate.text == "Any Date"{
            self.StartDate.text = ""
        }
        if self.EndDate.text == "Any Date"{
            self.EndDate.text = ""
        }
        if self.Vendor.text == "All Vendors"{
            self.Vendor.text = ""
        }
        if self.Brand.text == "All Brands"{
            self.Brand.text = ""
        }
        if self.DefectRate.text == "Any DR"{
            self.DefectRate.text = ""
        }
        if self.ReportType.text == "All Types"{
            self.ReportType.text = ""
        }
        if self.AuditResult.text == "All Results"{
            self.AuditResult.text = ""
        }
        var bodyData: String = ""
//        if(self.AuditCode.text == ""&&self.PO.text == ""&&self.Style.text == ""&&self.Country.text == ""&&self.search.text == ""&&self.StartDate.text == ""&&self.EndDate.text == ""&&self.Vendor.text == ""&&self.Brand.text == ""&&self.DefectRate.text == ""&&self.ReportType.text == ""&&self.AuditResult.text == ""){
//            bodyData = "User=\(defaults.object(forKey: "userid")as! String)&Status=Any"
//        }
//        else{
        // sending data except Page ID
        // page id will be assigned in Dashboard view
       bodyData = "User=\(defaults.object(forKey: "userid")as! String)&Status=Any&AuditStage=\(self.StageID)&FromDate=\(self.StartDate.text!)&ToDate=\(self.EndDate.text!)&Country=\(self.CountryID)&Vendor=\(self.vendorID)&Brand=\(self.BrandID)&DefectRate=\(self.DefectRateID)&AuditResult=\(self.AuditResultID)&ReportType=\(self.ReportID)&AuditCode=\(self.AuditCode.text!)&Po=\(self.PO.text!)&Style=\(self.Style.text!)"
        //}
        let destinationVC = segue.destination as! DashboardViewController
        destinationVC.searchParameters = bodyData
        destinationVC.isSearchRequest = true
        
        StageDataID.removeAll()
        selectedStageAccessoryButton.removeAllObjects()
        data2.removeAll()
        
        pTappedButtonIndexPath = nil
        CountriesDataID.removeAll()
        data3.removeAll()
        
        VendorsDataID.removeAll()
        pVendorTappedButtonIndexPath = nil
        data4.removeAll()
        
        BrandsDataID.removeAll()
        pBrandTappedButtonIndexPath = nil
        data5.removeAll()
        
        DefectRateDataID.removeAll()
        pDRTappedButtonIndexPath = nil
        data6.removeAll()
        
        ReportTypeDataID.removeAll()
        selectedAccessoryButton.removeAllObjects()
        data7.removeAll()
        
        AuditResultDataID.removeAll()
        pARTappedButtonIndexPath = nil
        data8.removeAll()
        
    }

    
}
extension Date {
    var formatted: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"// "dd MMM yyyy"  // HH:mm:ss Z"
        return  formatter.string(from: self)
    }
    var formattedTime: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm" // HH:mm:ss Z"
        return  formatter.string(from: self)
    }
    var formattedDateTime: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy HH:mm" //HH:mm:ss Z"
        return  formatter.string(from: self)
    }
}



