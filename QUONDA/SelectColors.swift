//
//  SelectColors.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 26/05/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
var ColorsData = [IndexPath:String]()
var ColorsDataID = [IndexPath:String]()
 
var pTappedColorButtonIndexPath:IndexPath?
var pColorsButtonTag:Int!

var Coloritems: [String] = []
var ColoritemsID: [String] = []

class SelectColors: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBAction func backButtonPressed(_ sender: AnyObject) {
        
//        var basketTopFrame:CGRect = self.view.frame
//        basketTopFrame.origin.x =  self.view.frame.width - 20 //115
//        UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
//            self.view.frame = basketTopFrame
//        }) { (finished) in
//            if finished {
//                self.dismissViewControllerAnimated(true, completion: nil)
//            }
//        }
//        
//        
////        UIView.animateWithDuration(0.5, animations: {
////            self.view.transform = CGAffineTransformMakeScale(1.0, 0.05)
//////            self.view.alpha = 0.0 // This alpha value which is zero, means white color
////            
////        }) { (finished) in
////            if finished {
////                self.dismissViewControllerAnimated(true, completion: nil)
////            }
////        }
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBOutlet weak var ColorsTable: UITableView!
    //    var data = [Int:String]()
    var dictionary:NSDictionary!
//    var jsonFilePath2:NSURL!
//    var created:Bool!
//    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first!
//    var isDirectory: ObjCBool = false
//    let fileManager = NSFileManager.defaultManager()
    
    
    var cell:UITableViewCell!
    //    var pressed:Bool = false
    var selectedButton:NSMutableArray! = NSMutableArray()
    //    var pTappedColorButtonIndexPath:NSIndexPath?
    //    var pColorsButtonTag:Int!
    
    var items: [String] = []
    var itemsID: [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        items = Coloritems
        itemsID = ColoritemsID
        
//        self.ColorsTable.dataSource = self
//        self.ColorsTable.delegate = self
        
//        let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
//        
//        let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("login.json")
//        jsonLoginFilePath = jsonFilePath
//        print(jsonLoginFilePath.path!)
        
//        do {
//            var readString: String
//            readString = try NSString(contentsOfFile: jsonLoginFilePath.path!, encoding: NSUTF8StringEncoding) as String
//            print(readString)
//            self.showDataWhenOffline()
//        } catch let error as NSError {
//            print(error.description)
//        }
        print(items)
        for i in 0 ..< (items.count) //yourTableSize = how many rows u got
        {
            selectedButton.add("NO")
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        //        for (var i = 0; i<items.count; i++) //yourTableSize = how many rows u got
        //        {
        //            selectedButton.addObject("NO")
        //        }
        print(DefectsColorsData0)
        if !DefectsColorsData0.isEmpty {
            for d2 in DefectsColorsData0{
                selectedButton.replaceObject(at: d2.0.row, with: "YES")
                
                if let alreadyCheckedCell = self.ColorsTable.cellForRow(at: d2.0 as IndexPath){
                    ColorsData[d2.0 as IndexPath] = alreadyCheckedCell.textLabel!.text
                    ColorsDataID[d2.0 as IndexPath] = alreadyCheckedCell.detailTextLabel?.text
                }
            }
            //            DefectsColorsData0.removeAll()
            self.ColorsTable.reloadData()
        }
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        return self.dictionary["DefectAreas"]!.count
        return items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let Ccell = self.ColorsTable.dequeueReusableCell(withIdentifier: "ccell")!
        //        var checkBox:UIButton = UIButton(frame: CGRect(x: Ccell.frame.width, y: Ccell.frame.height/4, width: 30, height: 30))
        Ccell.textLabel!.text = items[indexPath.row]
        Ccell.detailTextLabel!.text = itemsID[indexPath.row]
        Ccell.detailTextLabel?.isHidden = true
        
        Ccell.textLabel!.font = UIFont(name: Ccell.textLabel!.font.fontName, size: 13)
        
        //   Ccell.checkBox.layer.cornerRadius = Ccell.checkBox.frame.height/2
        //        checkBox.setBackgroundImage(UIImage(named: "Radio-OFF"), forState: .Normal)
        //        checkBox.setBackgroundImage(UIImage(named: "Radio-ON"), forState: .Selected)
        
        //        checkBox.tag = indexPath.row+100
        //        checkBox.addTarget(self, action: "checkBoxTapped:", forControlEvents: UIControlEvents.TouchUpInside)
        
        //        if selectedButton.objectAtIndex(indexPath.row).isEqualToString("NO"){
        //            checkBox.selected = false
        //        }else{
        //            checkBox.selected = true
        //        }
        if (selectedButton.object(at: indexPath.row) as AnyObject).isEqual(to: "NO"){
            Ccell.accessoryType = .none
//            Ccell.backgroundColor = UIColor.whiteColor()
        }else {
            Ccell.accessoryType = .checkmark
//            Ccell.backgroundColor = UIColor.lightGrayColor()
        }
        //        Ccell.contentView.addSubview(checkBox)
        
        return Ccell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("selected Index row path\(indexPath.row)")
        
        //        let button = sender as! UIButton
        //        let view = button.superview!
        //        self.cell = view.superview as! SearchStageCell
        //        let indexPath = searchTable.indexPathForCell(cell)
        
        let x = indexPath.row//button.tag - 100
        let cellPressed = tableView.cellForRow(at: indexPath)!
        
        // - - - - - - - - -
        //
        //        if cellPressed.accessoryType == .None{
        //            cellPressed.accessoryType = .Checkmark
        //            selectedButton.replaceObjectAtIndex(x, withObject: "YES")
        //            data[indexPath] = cellPressed.textLabel!.text
        //
        //        }else if cellPressed.accessoryType == .Checkmark{
        //            cellPressed.accessoryType = .None
        //            selectedButton.replaceObjectAtIndex(x, withObject: "NO")
        //            data.removeValueForKey(indexPath)
        //        }
        
        
        
/*
        print(pTappedColorButtonIndexPath?.row)
        if pTappedColorButtonIndexPath == indexPath{
            
            selectedButton.replaceObjectAtIndex(x, withObject: "NO")
            
            ColorsData.removeValueForKey(indexPath)
            ColorsDataID.removeValueForKey(indexPath)
            
            ////            button.selected = false
            //            cellPressed.accessoryType = .None
            //            ColorsData.removeValueForKey(indexPath)
            //
            //            pTappedColorButtonIndexPath = nil
            //            pColorsButtonTag = nil
        }
        else if pTappedColorButtonIndexPath != indexPath{
            if cellPressed.accessoryType == .Checkmark {
                //                if let Pcell = self.ColorsTable.cellForRowAtIndexPath(pTappedColorButtonIndexPath!)! as? UITableViewCell{
                selectedButton.replaceObjectAtIndex(x, withObject: "NO")
                //                let previousButtonState =  Pcell.contentView.subviews.last//button.selected = false
                //                print((previousButtonState?.tag)!-100)
                //                previousButtonState?.setValue(false, forKey: "selected")
                //                print(previousButtonState?.valueForKey("selected"))
                //                    Pcell.accessoryType = .None
                ColorsData.removeValueForKey(indexPath)
                ColorsDataID.removeValueForKey(indexPath)
                //                }
            }
            pTappedColorButtonIndexPath = indexPath
            selectedButton.replaceObjectAtIndex(x, withObject: "YES")
            //            button.selected = true
            cellPressed.accessoryType = .Checkmark
            ColorsData[indexPath] = cellPressed.textLabel!.text
            ColorsDataID[indexPath] = cellPressed.detailTextLabel?.text
            
           
        }
*/
        
        if cellPressed.accessoryType == .none{
            cellPressed.accessoryType = .checkmark
//            cellPressed.backgroundColor = UIColor.lightGrayColor()
            selectedButton.replaceObject(at: x, with: "YES")
            ColorsData[indexPath] = cellPressed.textLabel!.text
            ColorsDataID[indexPath] = cellPressed.detailTextLabel?.text
            
            
        }else if cellPressed.accessoryType == .checkmark{
            cellPressed.accessoryType = .none
//            cellPressed.backgroundColor = UIColor.whiteColor()
            selectedButton.replaceObject(at: x, with: "NO")
            ColorsData.removeValue(forKey: indexPath)
            ColorsDataID.removeValue(forKey: indexPath)
            
        }
        
        // - - Dismiss the current view - - - - - - -
        DefectsColorsData0 = ColorsData as [IndexPath : String]
//        self.dismissViewControllerAnimated(false, completion: nil)
//        self.ColorsTable.beginUpdates()
//        self.ColorsTable.endUpdates()
        
    }
    
    //    func sortCountries(obj1:[String:AnyObject], obj2:[String:AnyObject]){
    //
    //    var sorted_DATA = unsortedCountries.sortedArrayUsingComparator({obj1, obj2 -> NSComparisonResult in
    //
    //    let rstrnt1 = obj1
    //    let rstrnt2 = obj2
    //    if (rstrnt1 as! String) < (rstrnt2 as! String) {
    //    return NSComparisonResult.OrderedAscending
    //    }
    //    if (rstrnt1 as! String) > (rstrnt2 as! String) {
    //    return NSComparisonResult.OrderedDescending
    //    }
    //    return NSComparisonResult.OrderedSame
    //    })
    //    }
    func before(_ value1: String, value2: String) -> Bool {
        // One string is alphabetically first.
        // ... True means value1 precedes value2.
        return value1 < value2;
    }
    
//    func showDataWhenOffline(){
//        
//        if items.count >= 1 {
//            
//            self.dictionary = self.parseJSON(savedData!)
//            print("\(self.dictionary)")
//            
//            var All_Countries = self.dictionary["DefectAreas"]!  as! [String:AnyObject]
//            print(All_Countries)
//            
//            
//            var myArray = [String : AnyObject]()
//            //
//            for countryObj in All_Countries{
//                
//                myArray[countryObj.1 as! String] = countryObj.0
//                
//            }
//            print(myArray)
//            var mynewDICT:[Int:[String:AnyObject]]! = [:]
//            //            var mynewDICT: [String:AnyObject]! = [:]
//            // Create a dictionary.
//            let animals = ["bird": 0, "zebra": 9, "ant": 1]
//            
//            // Get the array from the keys property.
//            var copy = myArray.keys
//            // Sort from low to high (alphabetical order).
//            var p = copy.sort(<)
//            print(p)
//            
//            for pi in p{
//                print(pi)
//            }
//            var s = myArray.keys.sort()
//            print(s)
//            // Loop over sorted keys.
//            var i = 0
//            for pin in p{
//                for key in copy {
//                    if key == pin{
//                        // Get value for this key.
//                        if let value = myArray[key] {
//                            //                            mynewDICT[value as! String] = key
//                            mynewDICT[i] = [value as! String : key]
//                            self.items.append(key)
//                            self.itemsID.append(value as! String)
//                            
//                            print("Key = \(key), Value = \(value)")
//                            i+=1
//                        }
//                    }
//                }}
//            print(mynewDICT)
//            //            var ix = 0
//            //            for var ix = 0; ix < mynewDICT.count; ix++ {
//            //                print(mynewDICT[ix])
//            //            }
//            //            while(ix < mynewDICT.count){
//            //            print(mynewDICT[ix])
//            //                ix+=1
//            //            }
//            
//            // Get the array from the keys property.
//            //            for countryObj in All_Countries{
//            //
//            //                var sorted_DATA = unsortedCountries.sortedArrayUsingComparator({countryObj, countryObj -> NSComparisonResult in
//            //
//            //                    let rstrnt1 = countryObj
//            //                    let rstrnt2 = countryObj
//            //                    if (rstrnt1 as! String) < (rstrnt2 as! String) {
//            //                        return NSComparisonResult.OrderedAscending
//            //                    }
//            //                    if (rstrnt1 as! String) > (rstrnt2 as! String) {
//            //                        return NSComparisonResult.OrderedDescending
//            //                    }
//            //                    return NSComparisonResult.OrderedSame
//            //                })
//            //            }
//            //            print(unsortedCountries)
//            
//            //            print(sorted_DATA)
//            //            if let unsortedCountries = self.dictionary["Countries"] as? NSArray{
//            //                let descriptor = NSSortDescriptor(key: "", ascending: true)//(key: "name", ascending: true, selector: "caseInsensitiveCompare:")
//            //                sortedCountries = unsortedCountries.sortedArrayUsingDescriptors([descriptor])
//            ////            }
//            //
//            //            var sortedCountries =  All_Countries.sort({ (b:(String, AnyObject), a: (String, AnyObject)) -> Bool in
//            //                print(a)
//            //                print(b)
//            //
//            //
//            //            })//sort { (left, right) -> Bool in
//            //                let first = left.0//left["rstrnt_name"] as? String
//            //                let second = right.0//right["rstrnt_name"] as? String
//            
//            //                return first < second
//            //                if first<second{
//            //                    return true
//            //                }else{
//            //                    return false
//            //                }
//            //                switch (first, second) {
//            //                case  x : return x < y
//            //                default: return false
//            //                }
//            //            }
//            //            print(sortedCountries)
//            //            print(All_Countries)
//            //            var arr = Array(All_Countries)
//            //            for (k,v) in (Array(All_Countries).sort {$0.1 as! String < $1.1 }) {
//            //                print("\(k):\(v)")
//            //            }
//            print("Total Countries = \(All_Countries.count)")
//            
//            //            for country in All_Countries{
//            //                //
//            //                //                print(country)
//            //                //                self.items.append(country.1 as! String)
//            //                //                self.itemsID.append(country.0)
//            //            }
//            //            var sorted = self.items.sort() // it sort by id's
//            //            print(sorted)
//            //           var ii = self.items.sort({ (a, b) -> Bool in
//            //            print(a)
//            //            print(b)
//            //            if a < b {
//            //                return true
//            //            }
//            //            return false
//            //
//            //            })
//            //            print(ii)
//            //            country.sort(){$0 < $1}
//        }
//    }
    
//    func parseJSON(jsonString: String) -> NSDictionary {
//        if let data = jsonString.dataUsingEncoding(NSUTF8StringEncoding) {
//            var error: NSError?
//            if let json = try? NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary {
//                return json!
//                
//            } else if let error = error {
//                //Here's where the error comes back.
//                print("JSON Error: \(error)")
//            } else {
//                print("Unknown JSON Error")
//            }
//        }
//        return ["":""]
//    }
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
}


