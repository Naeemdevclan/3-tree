//
//  SpecsMeasurementsViewController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 15/11/2017.
//  Copyright © 2017 Musawar. All rights reserved.
//

import UIKit

class SpecsMeasurementsViewController: UIViewController,UITabBarDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,SavingViewControllerDelegate  {
    
    @IBOutlet weak var btnApprove: UIButton!
    
    @IBOutlet weak var titleView: UIView!
    var PickerArray:[String] = []
    var PickerArrayIDs:[String] = []
    var sizeSpecs = [AnyObject]()
    var sizeSpecsListArray = [AnyObject]()
    var allSizeSpecsListArray = [AnyObject]()
    
    
    
    var AuditCodeSp : String!
    var SampleCheckedSp : String!
    var SampleSizeSp : String!
    var AuditStageSp : String!
    var Report_Id :String!
    var AuditStageColorSp : UIColor!
    
    
    @IBOutlet weak var selectSizeView: UIView!
    @IBOutlet weak var lblSelectSize: UILabel!
    
    @IBOutlet weak var lblSelectColor: UILabel!
    @IBOutlet weak var selectColorView: UIView!
    @IBOutlet weak var specsListView: UITableView!
    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    weak var tab1vc: UIViewController!
    weak var tab2vc: UIViewController!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!
    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    
    //IBOutlets
    @IBOutlet weak var AuditCodeLabel: UILabel!
    @IBOutlet weak var SampleChecked: UILabel!
    @IBOutlet weak var SampleSize: UILabel!
    @IBOutlet weak var AuditStageLabel: UILabel!
    
    //load Offline data variables
    var created:Bool!
    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    var isDirectory: ObjCBool = false
    let fileManager = FileManager.default
    
    
    override func viewDidLoad() {
        specsListView.isHidden = true
        super.viewDidLoad()
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        print(Report_Id)
        if(Report_Id == "44" || Report_Id == "45"){
            selectColorView.isHidden = true
        }else{
            selectColorView.isHidden = false
        }
        //        self.scrollView.contentSize.height = 10000
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Selected Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)
        btnApprove.isHidden = true
        
        print("sampleSpecsChecked\(AuditCodeSp!)")
        if let spChecked = defaults.object(forKey: "sampleSpecsChecked\(AuditCodeSp!)"){
            SampleCheckedSp = spChecked as! String
            print(SampleCheckedSp!)
            
        }else{
            SampleCheckedSp = "1"
        }
        print(SampleCheckedSp!)


        AuditStageLabel.text = AuditStageSp
        AuditCodeLabel.text = AuditCodeSp
        AuditStageLabel.backgroundColor = AuditStageColorSp
        SampleChecked.text = SampleCheckedSp!
        SampleSize.text = SampleSizeSp
        
        // Saving JSON DATA
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("AuditMode.json")
        jsonLoginFilePath = jsonFilePath
        print(jsonLoginFilePath)
        do {
            var readString: String
            readString = try NSString(contentsOfFile: jsonLoginFilePath.path , encoding: String.Encoding.utf8.rawValue) as String
            print(readString)
            self.showDataWhenOffline(readString)
        } catch let error as NSError {
            print(error.description)
        }
        yAxis = specsListView.frame.origin.y
        self.view.bringSubview(toFront: specsListView)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnApprovedActions(_ sender: UIButton) {
        
        
        
        //self.saveOfflineSampleSpecsData()

        //SampleChecked.text = SampleCheckedSp
//        lblSelectSize.text = "Select Size"
//        sizeSpecsListArray.removeAll()
        let spIntVal = Int(SampleCheckedSp!)!
        
        SampleChecked.text = "\(spIntVal + 1)"
        SampleCheckedSp = "\(spIntVal + 1)"
        
        if let spChecked = defaults.object(forKey:"sampleSpecsChecked\(AuditCodeLabel.text!)"){
            SampleCheckedSp = spChecked as! String
        }
        
        if(SampleCheckedSp! == SampleSizeSp){
            btnApprove.setTitle("Approve & Finish", for: .normal)
            self.btnApprove.backgroundColor = UIColor(colorLiteralRed: 96.0/255, green: 147.0/255, blue: 9.0/255, alpha: 1.0)
        }else{
            }
    }
    
    func addDoneButtonOnKeyboard(textField:UITextField)
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(SpecsMeasurementsViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        // Now add toolbar to each of the keyboard in textfield
        textField.inputAccessoryView = doneToolbar
        
        
    }
    
    func doneButtonAction()
    {
        //self.view.endEditing(true)
        self.specsListView.endEditing(true)
        titleView.isHidden = false
        AuditStageLabel.isHidden = false
        specsListView.frame.origin.y = yAxis
        
    }
    
    var dictionary:NSDictionary!
    func showDataWhenOffline(_ savedData:String?){
        
        if savedData != nil {
            self.dictionary = self.parseJSON(savedData!)
            for item in self.dictionary["Audits"] as! [AnyObject]{
                if(item["AuditCode"] as? String == AuditCodeLabel.text!){
                    print(item)
                    print(item["SizeSpecs"] as! [AnyObject])
                    sizeSpecs = item["SizeSpecs"] as! [AnyObject]
                    let currentAudit = item["AuditView"] as! [String:AnyObject]
                    if(currentAudit["Sizes"] is [String:AnyObject]){
                        print(currentAudit["Sizes"] as! [String:AnyObject])
                        let sizes = currentAudit["Sizes"] as! [String:AnyObject]
                        
                        for (key, value) in sizes {
                            SizeArrayIDs.append(key)
                            SizeArray.append(value as! String)
                            print("\(key) -> \(value)")
                        }
                    }
                    let colors = currentAudit["Colors"] as! [AnyObject]
                    for item in 0..<colors.count{
                        ColorArrayIDs.append("\(item)")
                        ColorArray.append(colors[item] as! String)
                    }
                    
                }
            }
        }
    }
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
                
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
            //            defaults.removeObjectForKey("userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
            //        case 4:
            //            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
            //                //                self.dismissViewControllerAnimated(true, completion: nil)
            //                presentViewController(resultController, animated: true, completion: nil)
            //            }
            //
            //            let selectedImage:UIImage = UIImage(named: "Selected QAReport")!
            //            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            //
        //            break
        case 5:
            //self.dismiss(animated: true, completion: nil)
            self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
            
            //            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("5") as? ManagerModeViewController {
            //                //                self.dismissViewControllerAnimated(true, completion: nil)
            //                presentViewController(resultController, animated: true, completion: nil)
            //            }
            //            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            //            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            break
        case 8:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            let selectedImage:UIImage = UIImage(named: "tech_support_selected_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            break
        default:
            break
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sizeSpecsListArray.count + 1;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : SpecsTableViewCell = specsListView.dequeueReusableCell(withIdentifier: "specsCell", for: indexPath) as! SpecsTableViewCell
        
        if(indexPath.row == 0){
            cell.lblMeasurements.font = UIFont.boldSystemFont(ofSize: 20)
            cell.lblMeasurements.text = "Measurement point"
            cell.lblSpecs.text = "Specs"
            cell.lblPointTolerance.text = "Tolerance"
            cell.lblDeviation.text = "Deviation"
            cell.lblFindings.text = "Findings"
            cell.lblFindings.isHidden = false
            cell.txtFindings.isHidden = true
            cell.lblSpecs.font = UIFont.boldSystemFont(ofSize: 14)
            cell.lblFindings.font = UIFont.boldSystemFont(ofSize: 14)
            cell.lblPointTolerance.font = UIFont.boldSystemFont(ofSize: 14)
            cell.lblDeviation.font = UIFont.boldSystemFont(ofSize: 14)
            cell.lblDeviation.textColor = UIColor.darkGray
            
            
        }else{
            cell.lblDeviation.textColor = UIColor(colorLiteralRed: 96.0/255, green: 147.0/255, blue: 9.0/255, alpha: 1.0)
            
            cell.lblMeasurements.font = UIFont.systemFont(ofSize: 18)
            cell.lblSpecs.font = UIFont.systemFont(ofSize: 14)
            cell.lblFindings.font = UIFont.systemFont(ofSize: 14)
            cell.lblPointTolerance.font = UIFont.systemFont(ofSize: 14)
            cell.lblDeviation.font = UIFont.systemFont(ofSize: 13)
            
            cell.lblFindings.isHidden = true
            cell.txtFindings.isHidden = false
            cell.txtFindings.placeholder = "Findings"
            cell.txtFindings.keyboardType = UIKeyboardType.decimalPad
            addDoneButtonOnKeyboard(textField: cell.txtFindings)
            if(sizeSpecsListArray.count>0){
                let specs = sizeSpecsListArray[indexPath.row  - 1] as! [String:AnyObject]
                cell.lblMeasurements.text = specs["Point"] as? String
                cell.lblSpecs.text = specs["Specs"] as? String
                cell.lblPointTolerance.text = specs["Tolerance"] as? String
                cell.txtFindings.tag = Int((specs["PointId"] as? String)!)!
                //            if(pointsValues["Point\((specs["PointId"] as! String))"] != nil){
                //                cell.txtFindings.text = pointsValues["Point\((specs["PointId"] as! String))"]
                //            }else{
                //                cell.txtFindings.text = ""
                //            }
                loadValuesData(cell: cell, indexpath: indexPath.row - 1)
            }
        }
        if(Report_Id == "44"||Report_Id == "45"||Report_Id == "46"){
            cell.lblDeviation.isHidden = false
            // cell.lblDeviation.textColor = UIColor(colorLiteralRed: 96.0/255, green: 147.0/255, blue: 9.0/255, alpha: 1.0)
            
        }else{
            cell.lblDeviation.isHidden = true
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.dismiss(animated: true, completion: nil)
        tableView.deselectRow(at: indexPath, animated: false)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 102.0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let btnView = UIView.init()
        btnView.frame = CGRect(x: 0, y: 0, width: specsListView.bounds.width, height: 200)
        btnView.backgroundColor = .white
        let button = UIButton.init(frame: CGRect(x: 20, y: 20, width: specsListView.bounds.width/2, height: 40))
        button.setTitle("Save & Submit", for: .normal)
        button.addTarget(self, action: #selector(SpecsMeasurementsViewController.submitRequest), for: .touchUpInside)
        button.backgroundColor = .orange
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.layer.cornerRadius = 5
        btnView.addSubview(button)
        
        //Cancel
        
        let buttonCnl = UIButton.init(frame: CGRect(x: 40 + specsListView.bounds.width/2, y: 20, width: specsListView.bounds.width/4, height: 40))
        buttonCnl.setTitle("Cancel", for: .normal)
        buttonCnl.addTarget(self, action: #selector(SpecsMeasurementsViewController.cancelView), for: .touchUpInside)
        buttonCnl.backgroundColor = .orange
        buttonCnl.setTitleColor(UIColor.darkGray, for: .normal)
        buttonCnl.layer.cornerRadius = 5
        btnView.addSubview(buttonCnl)
        
        return btnView
    }
    func cancelView(){
        self.dismiss(animated: true, completion: nil)
        defaults.set("Cancel", forKey: "SpecsBtn")
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 200.0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    func submitRequest(){
        if let cnbtn = defaults.object(forKey: "SpecsBtn"){
            defaults.removeObject(forKey: "SpecsBtn")
        }
       
        self.saveOfflineSampleSpecsData()
        lblSelectSize.text = "Select Size"
        sortArray(specs: [])
       
//        if(sizeSpecsListArray.count > 0){
//            specsListView.isHidden = false
//        } else{
//            specsListView.isHidden = true
//        }
//        specsListView.reloadData()
       // defaults.set("\(SampleCheckedSp)", forKey: "sampleSpecsChecked\(AuditCodeLabel.text!)")
        print("sampleSpecsChecked\(AuditCodeLabel.text!)")

        let spIntVal = Int(SampleCheckedSp!)!
        
        SampleChecked.text = "\(spIntVal + 1)"
        SampleCheckedSp = "\(spIntVal + 1)"
        
        defaults.set("\(SampleCheckedSp!)", forKey: "sampleSpecsChecked\(AuditCodeLabel.text!)")

//        if let spChecked = defaults.object(forKey:"sampleSpecsChecked\(AuditCodeLabel.text!)"){
//            SampleCheckedSp = spChecked as! String
//        }

    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        titleView.isHidden = false
        AuditStageLabel.isHidden = true
        
        
        specsListView.frame.origin.y = yAxis
        //        selectSizeView.frame.origin.y = yAxisSView
        //        selectColorView.frame.origin.y = yAxixCView
        //        selectMeasurementView.frame.origin.y = yAxixMView
        let cell: SpecsTableViewCell = textField.superview!.superview?.superview as! SpecsTableViewCell
        // let table = cell.superview as! UITableView
        if  let textFieldIndexPath = specsListView.indexPath(for: cell){
            print(textFieldIndexPath.row)
            
            loadValuesData(cell: cell, indexpath: (textFieldIndexPath.row - 1))
        }
        return true
    }
    var yAxis:CGFloat = 0.0;
    var yAxisSView:CGFloat = 0.0;
    var yAxixCView:CGFloat = 0.0;
    var yAxixMView:CGFloat = 0.0;
    //var yAxixView:CGFloat = 0.0;
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print(textField.tag)
        
        textField.layer.borderColor = UIColor.orange.cgColor
        textField.layer.borderWidth = 1.0
        
        // if(yAxis != 0.0){
        // specsListView.frame.origin.y = yAxis
        //}
        // yAxis = specsListView.frame.origin.y
        // if(textField.tag > 3){
        specsListView.frame.origin.y = yAxis - 210.0
        AuditStageLabel.isHidden = true
        
        titleView.isHidden = true
        
        //}
        
        //        if(yAxisSView != 0.0){
        //            selectSizeView.frame.origin.y = yAxisSView
        //        }
        //        yAxisSView = selectSizeView.frame.origin.y
        //        if(textField.tag > 3){
        //            selectSizeView.frame.origin.y = yAxisSView - 210.0
        //        }
        //
        //        if(yAxixCView != 0.0){
        //            selectColorView.frame.origin.y = yAxixCView
        //        }
        //        yAxixCView = selectColorView.frame.origin.y
        //        if(textField.tag > 3){
        //            selectColorView.frame.origin.y = yAxixCView - 210.0
        //        }
        //        if(yAxixMView != 0.0){
        //            selectColorView.frame.origin.y = yAxixMView
        //        }
        //        yAxixMView = selectMeasurementView.frame.origin.y
        //        if(textField.tag > 3){
        //            selectMeasurementView.frame.origin.y = yAxixMView - 210.0
        //        }
        return true
    }
    func loadValuesData(cell: SpecsTableViewCell ,indexpath:Int){
        if(sizeSpecsListArray.count>0){
            let specs = sizeSpecsListArray[indexpath] as! [String:AnyObject]
            cell.lblMeasurements.text = specs["Point"] as? String
            cell.lblSpecs.text = specs["Specs"] as? String
            cell.lblPointTolerance.text = specs["Tolerance"] as? String
            cell.txtFindings.tag = Int((specs["PointId"] as? String)!)!
            cell.lblDeviation.textColor = UIColor(colorLiteralRed: 96.0/255, green: 147.0/255, blue: 9.0/255, alpha: 1.0)
            cell.lblDeviation.text = "0.0"
            if(pointsValues["Point\((specs["PointId"] as! String))"] != nil){
                cell.txtFindings.text = pointsValues["Point\((specs["PointId"] as! String))"]
                if(cell.txtFindings.text!.characters.count > 0){
                    //                    var tolTxt = cell.lblPointTolerance.text!.components(separatedBy: "/")
                    let upperLimit = "\(specs["PlusTolerance"] as! NSNumber)"
                    let lowerLimit = "\(specs["MinusTolerance"] as! NSNumber)"
                    
                    
                    
                    print(cell.txtFindings.text!)
                    
                    let upL = returnDoulbleVal(val: upperLimit)
                    let loL = returnDoulbleVal(val: lowerLimit)
                    
                    //                    print(tol)
                    if let tfingd = Double(cell.txtFindings.text!){
                        print(tfingd)

                   // let tfingd = Double(cell.txtFindings.text!)!
                    let specsVal =  returnSpecsDoubleVal(val:(cell.lblSpecs.text!))
                    print(specsVal)
                    cell.lblDeviation.text = "\(Double(round(1000*(tfingd - specsVal))/1000))"
                    if(!(cell.lblDeviation.text!.hasPrefix("-"))){
                        cell.lblDeviation.text = "+" + cell.lblDeviation.text!
                    }
                    if(Report_Id == "46"){
                        //cell.lblDeviation.textColor = colorCodesfordeviationJcrew
                        cell.lblDeviation.textColor = colorCodesfordeviationJcrew(uprLmt: upL, lwrLmt: loL, findings: tfingd, specs: specsVal)
                    }else{
                        cell.lblDeviation.textColor = colorCodesfordeviation(uprLmt: upL, lwrLmt: loL, findings: tfingd, specs: specsVal)
                    }
                    }else{
                        // self.view.makeToast(message: "Invalid Entry")
                        self.view.makeToast(message: "Invalid Entry", duration: 1.0, position:HRToastPositionTop as AnyObject)
                        cell.txtFindings.text = ""
                        //pointsValues["Point\(textField.tag)"] = textField.text
                        pointsValues["Point\((specs["PointId"] as! String))"] = ""

                    }
                }
            }else{
                cell.txtFindings.text = ""
            }
        }
    }
    func returnSpecsDoubleVal(val:String)->Double{
        
        let specss = val.components(separatedBy: " ")
        if(specss.count > 1){
            return returnDoulbleVal(val: specss[0]) + returnDoulbleVal(val: specss[1])
        }else{
            return returnDoulbleVal(val: specss[0])
        }
    }
    func returnDoulbleVal(val:String)->Double{
        if(val.contains("/")){
            let tt = val.components(separatedBy: "/")
            let numenator = Double(tt[0])!
            let denominator = Double(tt[1])!
            return (numenator/denominator)
        }else{
            return Double(val)!
        }
    }
    func colorCodesfordeviationJcrew(uprLmt:Double,lwrLmt:Double,findings:Double,specs:Double)->UIColor{
        
        //        let newupLimint = findings + uprLmt + 0.25
        //        let newloLimint = findings - lwrLmt - 0.25
        //
        
        let upperLimit = findings + uprLmt
        let lowerLimit = findings - lwrLmt
        
        if(specs <= upperLimit&&specs >= lowerLimit&&findings != specs){
            return UIColor.orange
        }
        else if(specs > upperLimit||specs < upperLimit&&findings != specs ){
            return UIColor.red
        }
        else {
            return UIColor(colorLiteralRed: 96.0/255, green: 147.0/255, blue: 9.0/255, alpha: 1.0)
        }
    }
    
    func colorCodesfordeviation(uprLmt:Double,lwrLmt:Double,findings:Double,specs:Double)->UIColor{
        
        let newupLimint = findings + uprLmt + 0.25
        let newloLimint = findings - lwrLmt - 0.25
        
        
        let upperLimit = findings + uprLmt
        let lowerLimit = findings - lwrLmt
        
        if(specs <= upperLimit&&specs >= lowerLimit){
            return UIColor(colorLiteralRed: 96.0/255, green: 147.0/255, blue: 9.0/255, alpha: 1.0)
        }
        else if(specs <= newupLimint&&specs >= newloLimint&&findings != specs){
            return UIColor.orange
        }
        else if(specs > newupLimint||specs < newloLimint&&findings != specs ){
            return UIColor.red
        }
        else {
            return UIColor(colorLiteralRed: 96.0/255, green: 147.0/255, blue: 9.0/255, alpha: 1.0)
            // self.ApprovedButton.backgroundColor = UIColor(colorLiteralRed: 96.0/255, green: 147.0/255, blue: 9.0/255, alpha: 1.0)
        }
    }
    var pointsValues = [String :String]()
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        pointsValues["Point\(textField.tag)"] = textField.text
        
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.layer.borderWidth = 1.0
        
        
        // specsListView.frame.origin.y = yAxis
        //        selectSizeView.frame.origin.y = yAxisSView
        //        selectColorView.frame.origin.y = yAxixCView
        //        selectMeasurementView.frame.origin.y = yAxixMView
        //                titleView.isHidden = false
        
        let cell: SpecsTableViewCell = textField.superview!.superview?.superview as! SpecsTableViewCell
        // let table = cell.superview as! UITableView
        if let textFieldIndexPath = specsListView.indexPath(for: cell){
            print(textFieldIndexPath.row)
            
            loadValuesData(cell: cell, indexpath: (textFieldIndexPath.row - 1))
        }
        print(pointsValues,"/n")
        return true
    }
    var SizeArray = [String]()
    var SizeArrayIDs = [String]()
    
    var ColorArray = [String]()
    var ColorArrayIDs = [String]()
    
    @IBOutlet weak var selectMeasurementView: UIView!
    @IBOutlet weak var lblSelectMeasurment: UILabel!
    @IBAction func btnSelectFullBodyMeasurementsAction(_ sender: UIButton) {
        PickerArray = ["Full Body Measurements","Critical Measurements"]
        PickerArrayIDs = ["1","2"]
        let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as! PopUpViewController
        
        popupVC.delegate = self
        
        popupVC.pickerdata = PickerArray
        popupVC.pickerdataIDs = PickerArrayIDs
        
        popupVC.WhichButton = "UnitName"
        
        popupVC.AllreadySelectedText = lblSelectMeasurment.text!
        
        self.addChildViewController(popupVC)
        popupVC.view.frame = self.view.frame
        
        self.view.addSubview(popupVC.view)
        self.didMove(toParentViewController: self)
        
        
    }
    @IBAction func btnSelectSizeAction(_ sender: UIButton) {
        PickerArray = self.SizeArray
        PickerArrayIDs = self.SizeArrayIDs
        print("PickerArray = \(PickerArray)")
        print("SizeArrayIDs = \(SizeArrayIDs)")
        
        
        let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as! PopUpViewController
        
        popupVC.delegate = self
        
        popupVC.pickerdata = PickerArray
        popupVC.pickerdataIDs = PickerArrayIDs
        
        popupVC.WhichButton = "SelectSize"
        
        popupVC.AllreadySelectedText = lblSelectSize.text!
        
        self.addChildViewController(popupVC)
        popupVC.view.frame = self.view.frame
        
        self.view.addSubview(popupVC.view)
        self.didMove(toParentViewController: self)
        
    }
    @IBAction func btnSelectColorAction(_ sender: UIButton) {
        PickerArray = self.ColorArray
        PickerArrayIDs = self.ColorArrayIDs
        print("PickerArray = \(PickerArray)")
        print("PickerArray = \(ColorArrayIDs)")
        
        
        let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as! PopUpViewController
        
        popupVC.delegate = self
        
        popupVC.pickerdata = PickerArray
        popupVC.pickerdataIDs = PickerArrayIDs
        
        popupVC.WhichButton = "SelectColor"
        
        popupVC.AllreadySelectedText = lblSelectColor.text!
        
        self.addChildViewController(popupVC)
        popupVC.view.frame = self.view.frame
        
        self.view.addSubview(popupVC.view)
        self.didMove(toParentViewController: self)
        
    }
    var selectedSizeId = ""
    func saveSelectedSize(_ strText: String, strID: String) {
        print(strText,strID)
        lblSelectSize.text = strText
        selectedSizeId = strID
       print( sizeSpecs)
        for item in sizeSpecs{
            if(item["Size"] as! String == strID){
                print(item)
                allSizeSpecsListArray = item["Specs"] as! [AnyObject]
                self.sortArray(specs: allSizeSpecsListArray)
                
            }
        }
    }
    func sortArray(specs:[AnyObject]){
        pointsValues.removeAll()
        sizeSpecsListArray.removeAll()
        if(lblSelectMeasurment.text! == "Full Body Measurements"){
            sizeSpecsListArray = specs
        }else{
            for item in allSizeSpecsListArray{
                if(item["Nature"] as! String == "C"){
                    sizeSpecsListArray.append(item)
                }
            }
        }
        if(sizeSpecsListArray.count > 0){
            specsListView.isHidden = false
        } else{
            specsListView.isHidden = true
        }
        specsListView.reloadData()
        print(specs)
        //        MinusTolerance = "0.375";
        //        Nature = S;
        //        PlusTolerance = "0.375";
        //        Point = "Across Back";
        //        PointId = 21630;
        //        Specs = "15.875";
        //        Tolerance = "-0.375/+0.375";
    }
    var selectedColorId = ""
    func saveSelectedColor(_ strText : String, strID : String){
        self.lblSelectColor.text = strText
        selectedColorId = strID
    }
    
    func saveText(_ strText : String, strID : String){
        
    }
    
    func saveReportText(_ strText : String, strID : String){
        
    }
    
    func saveBrandText(_ strText : String, strID : String){
        
    }
    
    func saveVendorText(_ strText : String, strID : String){
        
    }
    
    func saveUnitText(_ strText : String, strID : String){
        lblSelectMeasurment.text = strText
        self.sortArray(specs: allSizeSpecsListArray)
        
    }
    
    func saveStyleText(_ strText : String, strID : String){
        
    }
    
    func saveSampleSizeText(_ strText : String, strID : String){
        
    }
    
    func saveProductionLineText(_ strText : String, strID : String){
        
    }
    
    func saveAuditTypeText(_ strText : String, strID : String){
        
    }
    func saveAuditorsText(_ strText : String, strID : String){
        
    }
    func saveVendorsText(_ strText : String, strID : String){
        
    }
    
    func saveAuditResultText(_ strText : String, strID : String){
        
    }
    
    func saveAQLText(_ strText : String, strID : String){
        
    }
    func saveAQLData(_ strText : String, strID : String){
        
    }
    //Save Data Offline
    
    func saveOfflineSampleSpecsData(){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        print("You are in Sample Specs OFFLINE mode.")
        
        // Creating AuditComment Json file
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("SampleSpecsFile\(self.AuditCodeLabel.text!).json")
        
        // creating a .json file in the Documents folder
        
        if !fileManager.fileExists(atPath: jsonFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("File created ")
                
                // Now save it to FilesToSync.json file
                
                saveFileNameTo_FilesToSync("SampleSpecsFile\(self.AuditCodeLabel.text!).json")
                //(jsonFilePath.absoluteString)
                
            } else {
                print("Couldn't create file for some reason")
            }
        } else {
            print("File already exists")
        }
        //file creation ends
        
        
        
        //First Read out the AuditComment AuditCodeFile
        
        do{
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath.absoluteString, encoding: String.Encoding.utf8.rawValue) as String
            
            print("readAuditComment_String = ",readString)
            
            /// here send readString to function to update values stored in AuditCommentFile(AuditCode) File
            
            self.AuditCommentAuditCodeFile(readString, AuditCommentFilePath: jsonFilePath.absoluteString)
            
        }
        catch let error as NSError{
            print("There is an error while reading AddDeffectText(AuditCode) File.",error.description)
        }
        
    }// end of func
    func parseCommentsJSON(_ jsonString: String) -> [String:String] {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:String] {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    var bodyData:String!
    func AuditCommentAuditCodeFile(_ savedCommentsData:String?, AuditCommentFilePath: String) -> Void {
        AuditCommentAuditCodeFile_Hybrid(savedCommentsData, AuditCommentFilePath: AuditCommentFilePath)
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        //Reading from JsonDictionary File stored in documents directory
        do{
            var readString: String
            readString = try NSString(contentsOfFile: AuditCommentFilePath, encoding: String.Encoding.utf8.rawValue) as String
            //                    print("readString=\(readString)")
            let readStringParsed = parseCommentsJSON(readString)
            print("AuditComments_readStringParsed = ",readStringParsed)
        }
        catch let error as NSError{
            print("There is an error while reading from JsonDictionary File.",error.description)
            
        }
        
    }
    
    func AuditCommentAuditCodeFile_Hybrid(_ savedCommentsData:String?, AuditCommentFilePath: String) -> Void{
        
        // - - - -
        
        if savedCommentsData != nil && savedCommentsData != "" {
            
            //            User
            //            AuditCode
            //            DateTime
            //            Size
            //            Color
            //            Point1
            //            Point2
            //            Point3
            //dd-MMM-yyyy HH:mm
            let todaydate : Date = Date()
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd hh:mm:ss"
            // paymentDate.text = dateFormatterGet.string(from: todaydate)
            var poits = ""
            for (key, value) in pointsValues{
                //for item in pointsValues{
                if(value != ""){
                    poits = "&" + "\(key)=\(value)" + poits
                }
            }
            print(poits)
            if(Report_Id == "44" || Report_Id == "45"){
                self.lblSelectColor.text! = "STANDARD"
            }
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCodeLabel.text!)&Size=\(self.selectedSizeId)&Color=\(self.lblSelectColor.text!)&DateTime=\(dateFormatterGet.string(from: todaydate))&\(poits)&sample_no=\(SampleChecked.text!)"
            print("AuditCommentsHybrid-bodydata -> \(self.bodyData)")
            var AuditCommentsOfflineData:[String:String] = parseCommentsJSON(savedCommentsData!)
            
            
            let randomString = randomStringWithLength(6)
            
            AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "SampleSpecs_bodyData\(self.AuditCodeLabel.text!)_\(SampleChecked.text!)_\(randomString)")
            AuditCommentsOfflineData.updateValue("http://app.3-tree.com/quonda/save-sample-specs.php", forKey: "SampleSpecs_url")
            
            
            if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    //Note!!
                    
                    if !Reachability.isConnectedToNetwork(){
                        
                    }
                    
                    
                    // now move onto the Next screen
                    //???????
                    //?????
                    //???
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    print(SampleCheckedSp!)
                    print("sampleSpecsChecked\(AuditCodeLabel.text!)")


                    if(SampleCheckedSp! == SampleSizeSp){
                        self.dismiss(animated: true, completion: nil)
                    }else{
                        btnApprove.setTitle("NEXT >>", for: .normal)
                        self.btnApprove.backgroundColor = UIColor.gray

                    }

                    
                    
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
            
        }else{
            let todaydate : Date = Date()
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd hh:mm:ss"
            var poits = ""          // self.btnPackingCheck.isChecked ? "Y" : "N"
            for (key, value) in pointsValues{
                //for item in pointsValues{
                if(value != ""){
                    poits = "&" + "\(key)=\(value)" + poits
                }
                //poits = "&" + "\(key)=\(value)" + poits
            }
            if(Report_Id == "44" || Report_Id == "45"){
                self.lblSelectColor.text! = "STANDARD"
            }
            print(poits)
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&AuditCode=\(self.AuditCodeLabel.text!)&Size=\(self.selectedSizeId)&Color=\(self.lblSelectColor.text!)&DateTime=\(dateFormatterGet.string(from: todaydate))\(poits)&sample_no=\(SampleChecked.text!)"
            
            print("AuditCommentsHybrid-bodydata -> \(self.bodyData)")
            
            
            
            
            
            var AuditCommentsOfflineData = [String:String]()
            
            
            let randomString = randomStringWithLength(6)
            
            AuditCommentsOfflineData.updateValue(self.bodyData, forKey: "SampleSpecs_bodyData\(self.AuditCodeLabel.text!)_\(SampleChecked.text!)_\(randomString)")
            AuditCommentsOfflineData.updateValue("http://app.3-tree.com/quonda/save-sample-specs.php", forKey: "SampleSpecs_url")
            if self.fileManager.fileExists(atPath: AuditCommentFilePath, isDirectory: &self.isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: AuditCommentsOfflineData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: AuditCommentFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    print("sampleSpecsChecked\(AuditCodeLabel.text!)")


                    if(SampleCheckedSp! == SampleSizeSp){
                        self.dismiss(animated: true, completion: nil)

                    }else{
                        btnApprove.setTitle("NEXT >>", for: .normal)
                        self.btnApprove.backgroundColor = UIColor.gray

                    }

                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
        }
        
        // - - -
        
    }
    
    //Random string function
    
    func randomStringWithLength(_ len: Int) -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for i in 0 ..< (len){
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString
    }
    
    
    
    
    //End save data offline
    
}
