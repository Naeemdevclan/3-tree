//
//  SpecsTableViewCell.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 07/09/2017.
//  Copyright © 2017 Muhammad Naeem. All rights reserved.
//

import UIKit

class SpecsTableViewCell: UITableViewCell {

    @IBOutlet weak var txtFindings: UITextField!
    @IBOutlet weak var lblSpecs: UILabel!
    @IBOutlet weak var lblPointTolerance: UILabel!
    @IBOutlet weak var lblMeasurements: UILabel!
    @IBOutlet weak var lblDeviation: UILabel!
    
    @IBOutlet weak var lblFindings: UILabel!
    @IBOutlet weak var deviationWidthCons: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    public func configure(text: String?, placeholder: String) {
        txtFindings.text = text
        txtFindings.placeholder = placeholder
        
        txtFindings.accessibilityValue = text
        txtFindings.accessibilityLabel = placeholder
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
