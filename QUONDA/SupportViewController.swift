//
//  SupportViewController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 19/04/2017.
//  Copyright © 2017 Muhammad Naeem. All rights reserved.
//

import UIKit

class SupportViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource,UITabBarDelegate{

    @IBOutlet weak var ticketTabbar: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    @IBOutlet weak var tabBar1: UITabBar!

    @IBOutlet weak var openTicketItem: UITabBarItem!
    @IBOutlet weak var closeTicketItem: UITabBarItem!
    @IBOutlet weak var createTicketItem: UITabBarItem!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    
    @IBOutlet weak var Support_Item: UITabBarItem!
    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!

    @IBOutlet weak var noSupportTicketLable: UILabel!
    @IBOutlet weak var ticketList: UITableView!
    var ticket = "Open Ticket"
    var messageFrame = UIView()
    var activityIndicator = UIActivityIndicatorView()
    var strLabel = UILabel()
    var priorities = [String : String]()
    var subjects = [String : String]()
    var departments = [String : String]()
    var listOpen = [Any]()
    var listClose = [Any]()

    var blinkTimer: Timer?

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ticketTabbar.delegate = self
        noSupportTicketLable.isHidden = true
       //let fff = logout_Item.appearance()
        self.ticketTabbar.selectionIndicatorImage = UIImage().createSelectionIndicator(color: UIColor.darkGray, size: CGSize(width:self.ticketTabbar.frame.width/CGFloat(self.ticketTabbar.items!.count), height:self.ticketTabbar.frame.height), lineWidth: 4.0)
        let appearance = UITabBarItem.appearance()
        let attributes: [String: AnyObject] = [NSFontAttributeName:UIFont(name: "HelveticaNeue-Bold", size: 9)!, NSForegroundColorAttributeName: UIColor.lightGray]
        appearance.setTitleTextAttributes(attributes, for: .normal)
        let attributesSelected: [String: AnyObject] = [NSFontAttributeName:UIFont(name: "HelveticaNeue-Bold", size: 9)!, NSForegroundColorAttributeName: UIColor.lightGray]
        appearance.setTitleTextAttributes(attributesSelected, for: .selected)
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!,UIImage(named: "tech_support_selected_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)

        self.ticketTabbar.selectedItem = self.openTicketItem;

        // Do any additional setup after loading the view.
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
            blinkTimer?.invalidate()
            blinkTimer = nil
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
            blinkTimer?.invalidate()
            blinkTimer = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(true)
        progressBarDisplayer("Loading tickets", true)

        //        Gets url string
        let url = getLogin()
        
        //        Posts url with UITextField data.
        _ = performLoginRequestWithURL(url)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func event(timer: Timer!) {
        let info = timer.userInfo
        print(info!)
        let roww = info as! [String: Any]
       let cell = ticketList.cellForRow(at: roww["Index"] as! IndexPath)
        UIView.animate(withDuration: 0.5,animations: { () -> Void in
            if cell!.accessoryView?.alpha == 1{
               cell!.accessoryView?.alpha = 0
            }
            else{
                cell!.accessoryView?.alpha = 1
            }
//            cell?.accessoryView = UIIsmageView(image: UIImage(named: "dis_btn"))
        },
        completion:nil /*{(wentThrough: Bool) -> Void in
            UIView.animate(withDuration:1,animations: { () -> Void in
//                cell?.accessoryView = UIImageView(image: UIImage(named: "dis_highlight_btn"))
            })
        }*/)
    }
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
            //            defaults.removeObjectForKey("userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
       // case 4:
            //            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
            //                //                self.dismissViewControllerAnimated(true, completion: nil)
            //                presentViewController(resultController, animated: true, completion: nil)
            //            }
            
//            let selectedImage:UIImage = UIImage(named: "QAReport")!
//            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
//            
//            break
        case 5:
            if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }else{
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }
            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "8") as? SwormModeViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
       // case 9:
//            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
//                //                self.dismissViewControllerAnimated(true, completion: nil)
//                present(resultController, animated: true, completion: nil)
//            }
            
//            let selectedImage:UIImage = UIImage(named: "tech_support_selected_icon")!
//            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            

            break;
        case 10:
            ticket = "Open Ticket"
            ticketList.reloadData()
            self.noTicketsCheck()
            break;
        case 11:
            ticket = "Closed Ticket"
            ticketList.reloadData()
            self.noTicketsCheck()
            break;
        case 12:
            let createTicketVC = self.storyboard!.instantiateViewController(withIdentifier: "createTicketVC") as! CreateTicketViewController
            createTicketVC.category = self.departments
            createTicketVC.subcategory = self.subjects
            createTicketVC.priority = self.priorities
            present(createTicketVC, animated: true, completion: nil)

            break;
        default:
            break;
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(ticket == "Open Ticket"){
            return listOpen.count
        }
        else{
            return listClose.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       

        var cell :TicketTableViewCell!
            cell = tableView.dequeueReusableCell(withIdentifier: "TicketCell", for: indexPath) as! TicketTableViewCell
        var dict = [String:Any]()
        if(ticket == "Open Ticket"){
            dict = listOpen[indexPath.row] as! [String : Any]
        }
        else{
            dict = listClose[indexPath.row] as! [String : Any]
        }
        cell.lblTicketId.text = dict["Id"] as? String
        let splittedStringsArray = (dict["Created"] as? String)?.characters.split(separator: " ", maxSplits: 1).map(String.init)
        cell.lblTicketDate.text = splittedStringsArray?[0]
        cell.lblTicketTime.text = splittedStringsArray?[1]
        cell.lblTicketSubject.text = dict["Subject"] as? String
        let accessoryView = UIView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        let accessoryViewImage = UIImageView(image: UIImage(named: "dis_btn"))
        accessoryViewImage.center = CGPoint(x: 12, y: 12)
        accessoryView.addSubview(accessoryViewImage)
        
        cell.accessoryView = accessoryView
        let waiting = dict["Waiting"] as? String
        if(waiting == "Y"){
        blinkTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self,
                                     selector: #selector(event(timer:)), userInfo: ["Index":indexPath], repeats: true)
        }
        cell.lblTicketDepartment.text = dict["Department"] as? String
        if(dict["Priority"] as? String == "high"){
        cell.imgTicketColor.backgroundColor = UIColor.red
        }
        else if(dict["Priority"] as? String == "medium"){
            cell.imgTicketColor.backgroundColor = UIColor.orange
        }
        else{
            cell.imgTicketColor.backgroundColor = UIColor.blue
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var dict = [String:Any]()
        if(ticket == "Open Ticket"){
            dict = listOpen[indexPath.row] as! [String : Any]
        }
        else{
            dict = listClose[indexPath.row] as! [String : Any]
        }

        let createTicketVC = self.storyboard!.instantiateViewController(withIdentifier: "ticketdetailVC") as! TicketDetailViewController
        createTicketVC.ticketID = (dict["Id"] as? String)!
        present(createTicketVC, animated: true, completion: nil)

    }
    func noTicketsCheck(){
        if(self.ticket == "Open Ticket"){
            if(self.listOpen.count <= 0){
                self.noSupportTicketLable.isHidden = false
                self.ticketList.isHidden = true
            }
            else{
                self.noSupportTicketLable.isHidden = true
                self.ticketList.isHidden = false
            }
        }
        else{
            if(self.listClose.count <= 0){
                self.noSupportTicketLable.isHidden = false
                self.ticketList.isHidden = true
            }
            else{
                self.noSupportTicketLable.isHidden = true
                self.ticketList.isHidden = false
            }
        }
    }
    // API call
    func getLogin() -> URL {
        let toEscape = "http://support.3-tree.com/app/app.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
    var bodyData:String?
    func performLoginRequestWithURL(_ url: URL) -> String? {
        
        bodyData = "UserEmail=\(defaults.object(forKey: "userEmail") as! String)"
        print(url,bodyData)
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = bodyData!.data(using: String.Encoding.utf8)
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main){
            response, data, error in
            print("\(response)")
            if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                
                // print(json)
                let dictionary = self.parseJSON(json)
                print(dictionary)
//                            print("\(dictionary["countries"]!)")
//                            print("\(dictionary["Status"]!)")
                if("\(dictionary["Status"]!)" == "ERROR")
                {
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                    
                    //show popup on LOGIN FAIL
                    let alert = UIAlertController(title: "Alert", message: "\(dictionary["Message"]!)", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(UIAlertAction) -> Void in
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }else if dictionary.count < 1{
                    let alertView = UIAlertController(title: "\(dictionary["Message"]!)" , message: "Press OK", preferredStyle: .alert)
                    let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction)-> Void in
                        })
                    alertView.addAction(ok_action)
                    self.present(alertView, animated: true, completion: nil)
                }
                else
                {
                    for (key, value) in dictionary {
                        print("\(key) -> \(value)")
                    }
                    self.priorities = dictionary["Priorities"] as! [String:String]
                      print(self.priorities)
                    self.subjects = dictionary["Subjects"] as! [String:String]
                      print(self.subjects)
                    self.departments = dictionary["Departments"] as! [String:String]
                    print(self.departments)
                    self.listOpen = dictionary["OpenTickets"] as! [Any]
                    self.listClose = dictionary["ClosedTickets"] as! [Any]
                    self.ticketList.reloadData()
                    self.noTicketsCheck()
                    print(self.listOpen.count)
                    
                   // print("My_UserType = \(dictionary["UserType"]!)")
                    
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                    
                }
            }// data != nil
            else{
                let alertView = UIAlertController(title: "No Data Found" , message: "OK", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction)-> Void in
                })
                alertView.addAction(ok_action)
                self.present(alertView, animated: true, completion: nil)
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
            }
        }
        return json
    }
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    func progressBarDisplayer(_ msg:String, _ indicator:Bool ) {
        print(msg)
        strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 200, height: 50))
        strLabel.text = msg
        strLabel.textColor = UIColor.white
        messageFrame = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25 , width: 180, height: 50))
        messageFrame.layer.cornerRadius = 15
        messageFrame.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        if indicator {
            activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
            activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
            activityIndicator.startAnimating()
            messageFrame.addSubview(activityIndicator)
        }
        messageFrame.addSubview(strLabel)
        view.addSubview(messageFrame)
    }
    

}
extension UIImage {
    func createSelectionIndicator(color: UIColor, size: CGSize, lineWidth: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(x:0, y:size.height - lineWidth, width:size.width, height:lineWidth))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
