//
//  SwormModeViewController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 15/03/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
import MapKit

class SwormModeViewController: UIViewController, UITabBarDelegate {

    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    weak var tab1vc: UIViewController!
    weak var tab2vc: UIViewController!
    
    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    
    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    @IBOutlet weak var Support_Item: UITabBarItem!

    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var Btn_All_Audits: UIButton!
    @IBOutlet weak var Btn_On_Going_Audits: UIButton!
    @IBOutlet weak var Btn_Failed_Audits: UIButton!
    @IBOutlet weak var Btn_Final_Audits: UIButton!
    

    @IBOutlet weak var labels_BackgroundView: UIView!
    
    var regionRadius: CLLocationDistance!
    var gradientLayer: CAGradientLayer!
    
    var activityIndicator = UIActivityIndicatorView()
    var messageFrame = UIView()
    var strLabel = UILabel()
    var dictionary:NSDictionary!
    
    var annotationsArray_Defaults_Vendors = [default_Artwork]()
    var annotationsArray_Default_Auditors = [default_Artwork_Auditor]()
    var annotationsArray_Btn_Selected = [Artwork]()
    var annotationsArray_AllAudits = [Artwork]()
    var annotationsArray_On_Going_Audits = [Artwork]()
    var annotationsArray_Failed_Audits = [Artwork]()
    var annotationsArray_Final_Audits = [Artwork]()

var bodyData:String?
    
    @IBAction func All_Audits_Pressed(_ sender: AnyObject) {
        Btn_All_Audits.isSelected = true
        Btn_Failed_Audits.isSelected = false
        Btn_On_Going_Audits.isSelected = false
        Btn_Final_Audits.isSelected = false
        
        let url = getUrl("http://app.3-tree.com/quonda/audit-locations.php")
        self.performLoadDataRequestWithURL(url)

        mapView.removeAnnotations(annotationsArray_Default_Auditors)
        mapView.removeAnnotations(annotationsArray_Defaults_Vendors)
        mapView.removeAnnotations(annotationsArray_On_Going_Audits)
        mapView.removeAnnotations(annotationsArray_Failed_Audits)
        mapView.removeAnnotations(annotationsArray_Final_Audits)

    }
    @IBAction func On_Going_Audits_Pressed(_ sender: AnyObject) {
        Btn_On_Going_Audits.isSelected = true
        Btn_All_Audits.isSelected = false
        Btn_Failed_Audits.isSelected = false
        Btn_Final_Audits.isSelected = false
        
        let url = getUrl("http://app.3-tree.com/quonda/audit-locations.php")
        self.performLoadDataRequestWithURL(url)
        
        mapView.removeAnnotations(annotationsArray_Default_Auditors)
        mapView.removeAnnotations(annotationsArray_Defaults_Vendors)
        mapView.removeAnnotations(annotationsArray_AllAudits)
        mapView.removeAnnotations(annotationsArray_Failed_Audits)
        mapView.removeAnnotations(annotationsArray_Final_Audits)

    }
    
    @IBAction func Failed_Audits_Pressed(_ sender: AnyObject) {
        Btn_Failed_Audits.isSelected = true
        print(Btn_Failed_Audits.isSelected)
        Btn_All_Audits.isSelected = false
        Btn_On_Going_Audits.isSelected = false
        Btn_Final_Audits.isSelected = false
        let url = getUrl("http://app.3-tree.com/quonda/audit-locations.php")
        self.performLoadDataRequestWithURL(url)
        
        mapView.removeAnnotations(annotationsArray_Default_Auditors)
        mapView.removeAnnotations(annotationsArray_Defaults_Vendors)
        mapView.removeAnnotations(annotationsArray_On_Going_Audits)
        mapView.removeAnnotations(annotationsArray_AllAudits)
        mapView.removeAnnotations(annotationsArray_Final_Audits)

    }
    @IBAction func Final_Audits_Pressed(_ sender: AnyObject) {
        Btn_Final_Audits.isSelected = true
        Btn_On_Going_Audits.isSelected = false
        Btn_All_Audits.isSelected = false
        Btn_Failed_Audits.isSelected = false
        
        let url = getUrl("http://app.3-tree.com/quonda/audit-locations.php")
        self.performLoadDataRequestWithURL(url)
        
        mapView.removeAnnotations(annotationsArray_Default_Auditors)
        mapView.removeAnnotations(annotationsArray_Defaults_Vendors)
        mapView.removeAnnotations(annotationsArray_On_Going_Audits)
        mapView.removeAnnotations(annotationsArray_Failed_Audits)
        mapView.removeAnnotations(annotationsArray_AllAudits)

    }
    var onloadFirstAttemp:Bool!
    override func viewDidLoad() {
        
        onloadFirstAttemp = true
        self.presentingViewController?.dismiss(animated: false, completion: nil)

        super.viewDidLoad()
               let url = getUrl("http://app.3-tree.com/locations.php")
        self.performLoadDataRequestWithURL(url)

        
        // Do any additional setup after loading the view.
        regionRadius = 10000

        
        let initialLocation = CLLocation(latitude: 31.4103926, longitude: 74.2286463)
        centerMapOnLocation(initialLocation)
        
        self.tabBar2.delegate = self
        self.tabBar1.delegate = self
        
        var TabBarItemImages: [UIImage] = []
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "QAReport")!, UIImage(named: "Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Selected Swarm Mode")!,UIImage(named: "tech_support_icon")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        QAreport_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
        Support_Item.image = TabBarItemImages[8].withRenderingMode(.alwaysOriginal)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func getUrl(_ urlString:String) -> URL {
        let toEscape = urlString//"http://portal.3-tree.com/api/android/locations.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
//    var bodyData:String?
    func performLoadDataRequestWithURL(_ url: URL) -> String? {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        self.Btn_Final_Audits.isEnabled = false
        self.Btn_On_Going_Audits.isEnabled = false
        self.Btn_All_Audits.isEnabled = false
        self.Btn_Failed_Audits.isEnabled = false
        
        if Btn_All_Audits.isSelected == true{
            print("AllAudits Btn selected")
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&Status=All"
        }
        else if Btn_Failed_Audits.isSelected == true{
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&Status=Fail"
        }
        else if Btn_Final_Audits.isSelected == true{
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&Status=Final"
        }
        else if Btn_On_Going_Audits.isSelected == true{
            self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)&Status=Current"
        }
        else{
           self.bodyData = "User=\(defaults.object(forKey: "userid")as! String)"
        }
        
        var request: NSMutableURLRequest = NSMutableURLRequest(url: url)
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = self.bodyData!.data(using: String.Encoding.utf8)
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main){
            response, data, error in
            print("\(response)")
            if (error != nil) {
                print(error?.localizedDescription)
            }
            
            if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                print(json)
                self.dictionary = self.parseJSON(json)
                
                print(json)
                print("\(self.dictionary)")
                var All_Audits = self.dictionary["Auditors"]! as! [[String : AnyObject]]
                var All_Vendors = self.dictionary["Vendors"]! as! [[String : AnyObject]]
                
                print("All_Audits = ",All_Audits)
                if All_Vendors.count < 1{
                    print("There is no Vendor")
                }

                if All_Audits.count >= 1 && All_Vendors.count < 1{
                    print("There is no Auditor")
                
                if self.Btn_All_Audits.isSelected == true {
                     print("All Audits Dictionary =\(self.dictionary)")
                    for Audit in All_Audits{
                        let AuditorName = Audit["Name"]! as! String
                        print(AuditorName)
                        let AuditorLatitude = Audit["Latitude"]! as! String
                        print(AuditorLatitude)
                        let AuditorLongitude = Audit["Longitude"]! as! String
                        print(AuditorLongitude)
                        var AuditorAddress = ""
                        if(Audit["Address"] is NSNull){
                            
                        } else{
                        AuditorAddress = Audit["Address"]! as! String
                        }
                        let VendorAudits = Audit["Audits"]!

                        self.annotationsArray_AllAudits.append(Artwork(title: AuditorName, locationName: AuditorAddress, discipline: VendorAudits, coordinate: CLLocationCoordinate2D(latitude: (AuditorLatitude as NSString).doubleValue, longitude: (AuditorLongitude as NSString).doubleValue), imageName: "marker_all.png"))
                        
                        print("annotationsArray_AllAudits =",self.annotationsArray_AllAudits)
                    }
                    DispatchQueue.main.async(execute: { () -> Void in
//                        self.mapView.viewForAnnotation(self.annotationsArray_AllAudits.first!)
                        self.mapView.addAnnotations(self.annotationsArray_AllAudits)
                    })
                    
                }
                else if self.Btn_Failed_Audits.isSelected == true{
                    var AuditorAddress = ""
                    for Audit in All_Audits{
                        let AuditorName = Audit["Name"]! as! String
                        print(AuditorName)
                        let AuditorLatitude = Audit["Latitude"]! as! String
                        print(AuditorLatitude)
                        let AuditorLongitude = Audit["Longitude"]! as! String
                        print(AuditorLongitude)
                        if let auditorddd = Audit["Address"] {
                            if(auditorddd is NSNull){
                                
                            }
                            else {
                        AuditorAddress = auditorddd as! String
                            }
                        }
                        let VendorAudits = Audit["Audits"]!
                        
                        self.annotationsArray_Failed_Audits.append(Artwork(title: AuditorName, locationName: AuditorAddress, discipline: VendorAudits, coordinate: CLLocationCoordinate2D(latitude: (AuditorLatitude as NSString).doubleValue, longitude: (AuditorLongitude as NSString).doubleValue), imageName: "marker_fail.png"))
                    }
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.mapView.addAnnotations(self.annotationsArray_Failed_Audits)
                    })
                }
                else if self.Btn_Final_Audits.isSelected == true{
                    
                    for Audit in All_Audits{
                        let AuditorName = Audit["Name"]! as! String
                        print(AuditorName)
                        let AuditorLatitude = Audit["Latitude"]! as! String
                        print(AuditorLatitude)
                        let AuditorLongitude = Audit["Longitude"]! as! String
                        print(AuditorLongitude)
                        var AuditorAddress = ""

                        if(Audit["Address"] is NSNull){
                            
                        } else{
                            AuditorAddress = Audit["Address"]! as! String
                        }
                        let VendorAudits = Audit["Audits"]!
                        print(VendorAudits)
                        print(VendorAudits.object(at: 0))
//                        print(VendorAudits[0]["Auditor"])
                        
                        self.annotationsArray_Final_Audits.append(Artwork(title: AuditorName, locationName: AuditorAddress, discipline: VendorAudits, coordinate: CLLocationCoordinate2D(latitude: (AuditorLatitude as NSString).doubleValue, longitude: (AuditorLongitude as NSString).doubleValue), imageName: "marker_final.png"))
                    }
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.mapView.addAnnotations(self.annotationsArray_Final_Audits)
                    })
                }
                else if self.Btn_On_Going_Audits.isSelected == true{
                    
                    for Audit in All_Audits{
                        let AuditorName = Audit["Name"]! as! String
                        print(AuditorName)
                        let AuditorLatitude = Audit["Latitude"]! as! String
                        print(AuditorLatitude)
                        let AuditorLongitude = Audit["Longitude"]! as! String
                        print(AuditorLongitude)
                        var AuditorAddress = ""
                        if(Audit["Address"] is NSNull){
                            
                        } else{
                            AuditorAddress = Audit["Address"]! as! String
                        }
                        let VendorAudits = Audit["Audits"]!
                        
                        self.annotationsArray_On_Going_Audits.append(Artwork(title: AuditorName, locationName: AuditorAddress, discipline: VendorAudits, coordinate: CLLocationCoordinate2D(latitude: (AuditorLatitude as NSString).doubleValue, longitude: (AuditorLongitude as NSString).doubleValue), imageName: "marker_ongoing.png"))
                    }
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.mapView.addAnnotations(self.annotationsArray_On_Going_Audits)
                    })

                    }
                }else if self.onloadFirstAttemp == true{
               
                    self.onloadFirstAttemp = false
                    
                    for Vendor in All_Vendors{
                        
                        let VendorName = Vendor["Name"]! as! String
                        print(VendorName)
                        let VendorLatitude = Vendor["Latitude"]! as! String
                        print(VendorLatitude)
                        let VendorLongitude = Vendor["Longitude"]! as! String
                        print(VendorLongitude)
                        let VendorAddress = Vendor["Address"]! as! String
                        
                        self.annotationsArray_Defaults_Vendors.append(default_Artwork(title: VendorName, coordinate: CLLocationCoordinate2D(latitude: (VendorLatitude as NSString).doubleValue, longitude: (VendorLongitude as NSString).doubleValue), address: VendorAddress, imageName: "marker_all.png"))
                        
                    }
                    
                    for Audit in All_Audits{
                        
                        let AuditorName = Audit["Name"]! as! String
                        print(AuditorName)
                        let AuditorLatitude = Audit["Latitude"]! as! String
                        print(AuditorLatitude)
                        let AuditorLongitude = Audit["Longitude"]! as! String
                        print(AuditorLongitude)
                        let AuditorDateTime = Audit["DateTime"]! as! String
                        print("AuditorDateTime =",AuditorDateTime)
                        let AuditorAddress = Audit["Address"]! as! String
                        
                        //                        self.tableCellData.append(items(A: AuditorName, B: AuditorLatitude, C: AuditorLongitude, D: AuditorDateTime, E: AuditorAddress))
                        
                        self.annotationsArray_Default_Auditors.append(default_Artwork_Auditor(title: AuditorName, locationName: AuditorAddress, discipline: AuditorDateTime, coordinate: CLLocationCoordinate2D(latitude: (AuditorLatitude as NSString).doubleValue, longitude: (AuditorLongitude as NSString).doubleValue), imageName: "marker.png",dateTime: AuditorDateTime))
                    }
                
                    print(self.annotationsArray_Default_Auditors.count)
                print(self.annotationsArray_Default_Auditors)
                
                DispatchQueue.main.async(execute: { () -> Void in
                    self.mapView.addAnnotations(self.annotationsArray_Default_Auditors)
                    self.mapView.addAnnotations(self.annotationsArray_Defaults_Vendors)
                })
           
            }// else part of All_Audits >=1
            
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.Btn_Final_Audits.isEnabled = true
                self.Btn_On_Going_Audits.isEnabled = true
                self.Btn_All_Audits.isEnabled = true
                self.Btn_Failed_Audits.isEnabled = true
            }//if data != nil
            else{
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                // show alert
                let alertView = UIAlertController(title: "No Data Found" , message: "\(error) \n Press OK", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                    
                alertView.addAction(ok_action)
                self.present(alertView, animated: true, completion: nil)

                //- - -  - - - -
                self.Btn_Final_Audits.isEnabled = true
                self.Btn_On_Going_Audits.isEnabled = true
                self.Btn_All_Audits.isEnabled = true
                self.Btn_Failed_Audits.isEnabled = true
                
                return print("return data is nil")
            }
        }
        return json
    }
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    func progressBarDisplayer(_ msg:String, _ indicator:Bool ) {
        print(msg)
        strLabel = UILabel(frame: CGRect(x: view.frame.midX - 40, y: view.frame.midY - 50, width: 200, height: 50))  // x: 50, y: 0, width: 200, height: 50
        strLabel.text = msg
        strLabel.textColor = UIColor.white
        messageFrame = UIView(frame: CGRect(x: 0, y: 69 , width: view.frame.width, height: view.frame.height - 118))  //x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50
        //        messageFrame.layer.cornerRadius = 15
        messageFrame.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        if indicator {
            activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
            activityIndicator.frame = CGRect(x: view.frame.midX - 80, y: view.frame.midY - 50, width: 50, height: 50)  // x: 0, y: 0, width: 50, height: 50
            activityIndicator.startAnimating()
            messageFrame.addSubview(activityIndicator)
        }
        messageFrame.addSubview(strLabel)
        view.addSubview(messageFrame)
    }

    
    func centerMapOnLocation(_ location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
            regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            let loginVC = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController") as! LoginView
            
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "password")
//            defaults.removeObjectForKey("userid")
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            present(loginVC, animated: true, completion: nil)
            
            break
        case 2:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "2") as? ScheduleAuditViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "3") as? DashboardViewController {
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
//        case 4:
//            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("4") as? QAReportViewController {
//                //                self.dismissViewControllerAnimated(true, completion: nil)
//                presentViewController(resultController, animated: true, completion: nil)
//            }
//            
//            let selectedImage:UIImage = UIImage(named: "Selected QAReport")!
//            item.selectedImage = selectedImage.imageWithRenderingMode(.AlwaysOriginal)
//            
//            break
        case 5:
            if(defaults.object(forKey: "AuditsManager")as! String == "Y"){
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "5") as? ManagerModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }else{
                if let resultController = storyboard!.instantiateViewController(withIdentifier: "auditorModeViewController") as? AuditorModeViewController {
                    
                    present(resultController, animated: true, completion: nil)
                }
            }

            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 6:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "6") as? KPIViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "7") as? SearchViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
//            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("8") as? SwormModeViewController {
//                //                self.dismissViewControllerAnimated(true, completion: nil)
//                presentViewController(resultController, animated: true, completion: nil)
//            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 9:
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "supportVC") as? SupportViewController {
                //                self.dismissViewControllerAnimated(true, completion: nil)
                present(resultController, animated: true, completion: nil)
            }
            
            let selectedImage:UIImage = UIImage(named: "tech_support_icon")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        default:
            break
        }
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}



