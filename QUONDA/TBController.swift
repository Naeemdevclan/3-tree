//
//  TBController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 12/05/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class TBController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupSwipeGestureRecognizers(true)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UITabBarController {
    func setupSwipeGestureRecognizers(_ cycleThroughTabs: Bool = false) {
        let swipeLeftGestureRecognizer = UISwipeGestureRecognizer(target: self, action: cycleThroughTabs ? #selector(UITabBarController.handleSwipeLeftAllowingCyclingThroughTabs(_:))
            : #selector(UITabBarController.handleSwipeLeft(_:)))
        swipeLeftGestureRecognizer.direction = .left
        self.view.addGestureRecognizer(swipeLeftGestureRecognizer)
//        self.tabBar.addGestureRecognizer(swipeLeftGestureRecognizer)
        
        let swipeRightGestureRecognizer = UISwipeGestureRecognizer(target: self, action: cycleThroughTabs ? #selector(UITabBarController.handleSwipeRightAllowingCyclingThroughTabs(_:))
            : #selector(UITabBarController.handleSwipeRight(_:)))
        swipeRightGestureRecognizer.direction = .right
        self.view.addGestureRecognizer(swipeRightGestureRecognizer)
//        self.tabBar.addGestureRecognizer(swipeRightGestureRecognizer)
    }
    
    func handleSwipeLeft(_ swipe: UISwipeGestureRecognizer) {
        self.selectedIndex -= 1
    }
    
    func handleSwipeRight(_ swipe: UISwipeGestureRecognizer) {
        self.selectedIndex += 1
    }
    
    func handleSwipeLeftAllowingCyclingThroughTabs(_ swipe: UISwipeGestureRecognizer) {
        let maxIndex = (self.viewControllers?.count ?? 0)
        let nextIndex = self.selectedIndex - 1
        self.selectedIndex = nextIndex >= 0 ? nextIndex : maxIndex - 1
        
    }
    
    func handleSwipeRightAllowingCyclingThroughTabs(_ swipe: UISwipeGestureRecognizer) {
        let maxIndex = (self.viewControllers?.count ?? 0)
        let nextIndex = self.selectedIndex + 1
        self.selectedIndex = nextIndex < maxIndex ? nextIndex : 0
    }
}

