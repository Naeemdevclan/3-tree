//
//  TicketDetailViewController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 20/04/2017.
//  Copyright © 2017 Muhammad Naeem. All rights reserved.
//

import UIKit

class TicketDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate {

    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var lblsubject: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var messageFrame = UIView()
    var activityIndicator = UIActivityIndicatorView()
    var strLabel = UILabel()
    var ticketID = ""
    var listData = [Any]()
    var message = ""
    var dateTime = ""
    var status = ""
    var priority = ""
    var attachments = [String:Any]()
    var showImageView = UIImageView.init()
    let viewTag     = 1000
    let imgViewTag  = 2000
    let btnTag      = 3000
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showImageView.frame = self.view.frame
        showImageView.isHidden = true
        showImageView.backgroundColor = UIColor.white
        self.view.addSubview(showImageView)
        let tapgestures = UITapGestureRecognizer.init(target: self, action: #selector(hideImage))
        tapgestures.delegate = self
        showImageView.isUserInteractionEnabled = true
        showImageView.addGestureRecognizer(tapgestures)
    }
    func hideImage(){
        showImageView.isHidden = true
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        viewDidLayoutSubviews()
        tableView.reloadData()
        progressBarDisplayer("Loading ticket details", true)
        _ = performLoginRequestWithURL(getUrl())

    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
            }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnbackAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData.count + 1;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell :TicketTableViewCell!
        if(indexPath.row == 0){
        cell = tableView.dequeueReusableCell(withIdentifier: "ticketCell", for: indexPath) as! TicketTableViewCell
            cell.lblTicketId.text = ticketID
            cell.lblDateAndTime.text = dateTime
            cell.message.text = message
            cell.lbStatus.text = status == "N" ? "Open" : "Close"
  
                    //let listd = attachments
                    let yAxis = 40
                    var  i = 0
                    for (key, value) in attachments {
                        print("\(key) -> \(value)")
                        
                        var imgView     = cell.contentView.viewWithTag(viewTag+i)
                        var imageView   = cell.contentView.viewWithTag(imgViewTag+i) as? UIImageView
                        var btn         = cell.contentView.viewWithTag(btnTag+i) as? UIButton
                        
                        if imgView == nil {
                            imgView = UIView()
                            cell.contentView.addSubview(imgView!)
                        }
                        
                        if imageView == nil {
                            imageView = UIImageView()
                            imgView!.addSubview(imageView!)
                        }
                        
                        if btn == nil {
                            btn = UIButton.init(type: .custom)
                            imgView!.addSubview(btn!)
                        }
                        
                        imgView?.tag    = viewTag+i
                        imageView?.tag  = imgViewTag+i
                        btn?.tag        = btnTag+i
                        
                        imgView!.frame = CGRect(x:20,y:cell.message.frame.origin.y + cell.message.frame.height + 30 + CGFloat(yAxis*i),width:cell.frame.width - 40,height:30)
                        
                        imageView!.frame = CGRect(x:0,y:3,width:24, height:24)
                        imageView!.image = UIImage.init(named: "file_manager")
                    
                        btn!.accessibilityHint   = "\(value)"
                        btn!.frame               = CGRect(x:35,y:0,width:cell.frame.width - 70,height:30)
                        btn!.setTitleColor(UIColor.gray, for: UIControlState.normal)
                        btn!.titleLabel?.font =  UIFont.systemFont(ofSize: 13);
                        btn!.setTitle("\(value)", for: .normal)
                        btn!.addTarget(self, action: #selector(tapped(sender:)), for: .touchUpInside)
                        
                        i = i + 1
                    }
        }
        else {
//            var cell :TicketTableViewCell!
            cell = tableView.dequeueReusableCell(withIdentifier: "replyCell", for: indexPath) as! TicketTableViewCell
            if self.listData.count > 0{
                let dict = self.listData[indexPath.row - 1] as! [String: Any]
                cell.lblDataTimeReply.text = dict["DateTime"] as? String
                cell.lblReplyBy.text = dict["User"] as? String
                cell.lblReplyMsg.text = dict["Message"] as? String
               
                if let lis = dict["Attachments"]{
                    
                    print(lis)
                if(lis is [String:Any]){
                let listd = lis as! [String:Any]
                let yAxis = 40
                var  i = 0
                for (key, value) in listd {
                    print("\(key) -> \(value)")
                    
                    let imgView = UIView()
        imgView.frame = CGRect(x:20,y:cell.lblReplyMsg.frame.origin.y + cell.lblReplyMsg.frame.height + 10 + CGFloat(yAxis*i),width:cell.frame.width - 40,height:30)
                    let imageView = UIImageView()
                    imageView.frame = CGRect(x:0,y:3,width:24, height:24)
                    imageView.image = UIImage.init(named: "file_manager")
                    let btn                     = UIButton.init(type: .custom)
                    btn.setTitleColor(UIColor.gray, for: UIControlState.normal)
                    btn.frame                   = CGRect(x:35,y:0,width:cell.frame.width - 70,height:30)
                    btn.titleLabel?.font =  UIFont.systemFont(ofSize: 13);
                    btn.setTitle("\(value)", for: .normal)
                    btn.accessibilityHint = "\(value)"
                    btn.addTarget(self, action: #selector(tapped(sender:)), for: .touchUpInside)
                    
                    
                    imgView.addSubview(imageView)
                    imgView.addSubview(btn)
                    cell.contentView.addSubview(imgView)
                    i = i + 1
                }
                }
                
            }
            }
         }
        cell.selectionStyle = .none
        return cell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
            let footerView = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:80))
            footerView.backgroundColor = UIColor.groupTableViewBackground
            let saveBtn = UIButton.init()
            saveBtn.backgroundColor = UIColor.orange
            saveBtn.layer.cornerRadius = 5.0
            saveBtn.setTitle("POST A REPLY", for: .normal)
            saveBtn.addTarget(self, action: #selector(Measurement_Stage_TMClothing.saveBtnPressed), for: .touchUpInside)
            saveBtn.frame = CGRect(x:10, y:10, width:tableView.frame.size.width - 20, height:40)
            // saveBtn.center = footerView.center
            footerView.addSubview(saveBtn)
            return footerView
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
         return 80.0;
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0{
            let height = heightForView(text:"/n" + message + "/n", font: UIFont.systemFont(ofSize: 17), width: ScreenSize.SCREEN_WIDTH-16)
            return 88 + height + CGFloat(attachments.count*40)
            
        }
        
        let dict = self.listData[indexPath.row - 1] as! [String: Any]
        let height = heightForView(text: (dict["Message"] as? String)!, font: UIFont.systemFont(ofSize: 17), width: ScreenSize.SCREEN_WIDTH-40)
        var attachmentViewHeight = 0
        if let listd = dict["Attachments"]{
        
        attachmentViewHeight = (listd as AnyObject).count*40
        }

        
        return 57 + height + CGFloat(attachmentViewHeight)
    }
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//      //  return UITableViewAutomaticDimension
//    }


    @objc func tapped(sender: UIButton)
    {
        print(sender.accessibilityHint!)
        
        if let checkedUrl = URL(string:"http://support.3-tree.com/content/attachments/" + sender.accessibilityHint!) {
            showImageView.contentMode = .scaleAspectFit
            downloadImage(url: checkedUrl)
        }
    }
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    func downloadImage(url: URL) {
        print("Download Started")
        getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() { () -> Void in
                self.showImageView.isHidden = false
                self.showImageView.image = UIImage(data: data)
            }
        }
    }
    func saveBtnPressed(sender : UIButton){
        let createTicketVC = self.storyboard!.instantiateViewController(withIdentifier: "postReplyVC") as! PostReplyViewController
        createTicketVC.ticketID = ticketID
        createTicketVC.department = lblDepartment.text!
        createTicketVC.subject = lblsubject.text!
        createTicketVC.priority = priority
        createTicketVC.isClosed = status
        attachments.removeAll()
        tableView.reloadData()
        present(createTicketVC, animated: true, completion: nil)

        print("POST HERE")
    }
    // API call
    func getUrl() -> URL {
        let toEscape = "http://support.3-tree.com/app/ticket-details.php"
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
    var bodyData:String?
    func performLoginRequestWithURL(_ url: URL) -> String? {
        
        bodyData = "UserEmail=\(defaults.object(forKey: "userEmail") as! String)&Ticket=\(ticketID)"
        print(url,bodyData!)
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
        var json = ""
        request.httpMethod = "POST"
        request.httpBody = bodyData!.data(using: String.Encoding.utf8)
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main){
            response, data, error in
            print("\(response)")
            if data != nil {
                json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                
                // print(json)
                let dictionary = self.parseJSON(json)
                print(dictionary)
                //                            print("\(dictionary["countries"]!)")
                //                            print("\(dictionary["Status"]!)")
                if("\(dictionary["Status"]!)" == "ERROR")
                {
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                    
                    //show popup on LOGIN FAIL
                    let alert = UIAlertController(title: "Alert", message:"\(dictionary["Message"]!)", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(UIAlertAction) -> Void in
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }else if dictionary.count < 1{
                    let alertView = UIAlertController(title: "\(dictionary["Message"]!)" , message: "Press OK", preferredStyle: .alert)
                    let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction)-> Void in
                    })
                    alertView.addAction(ok_action)
                    self.present(alertView, animated: true, completion: nil)
                }
                else
                {
                    for (key, value) in dictionary {
                        print("\(key) -> \(value)")
                    }
                    self.listData = dictionary["History"] as! [Any]
                    print(self.listData)
                    self.message = dictionary["Message"] as! String
                    self.dateTime = dictionary["Created"] as! String
                    self.status = dictionary["Closed"] as! String
                    self.attachments.removeAll()
                    print(self.attachments)
                    if let attach = dictionary["Attachments"]{
                        if(attach is [String:Any]){
                            self.attachments = attach as! [String:Any]
                        }
                    }
                    // print("My_UserType = \(dictionary["UserType"]!)")
                    self.lblsubject.text = dictionary["Subject"] as? String
                    self.lblDepartment.text = dictionary["Department"] as? String
                    self.priority = (dictionary["Priority"] as? String)!
                    if(dictionary["Priority"] as? String == "high"){
                        self.imgStatus.backgroundColor = UIColor.red
                    }
                    else if(dictionary["Priority"] as? String == "medium"){
                        self.imgStatus.backgroundColor = UIColor.orange
                    }
                    else{
                        self.imgStatus.backgroundColor = UIColor.blue
                    }

                    self.tableView.reloadData()
                    self.activityIndicator.stopAnimating()
                    self.messageFrame.removeFromSuperview()
                    
                }
            }// data != nil
            else{
                let alertView = UIAlertController(title: "No Data Found" , message: "Press OK", preferredStyle: .alert)
                let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction)-> Void in
                })
                alertView.addAction(ok_action)
                self.present(alertView, animated: true, completion: nil)
                self.activityIndicator.stopAnimating()
                self.messageFrame.removeFromSuperview()
            }
        }
        return json
    }
    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    func progressBarDisplayer(_ msg:String, _ indicator:Bool ) {
        print(msg)
        strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 200, height: 50))
        strLabel.text = msg
        strLabel.textColor = UIColor.white
        messageFrame = UIView(frame: CGRect(x: view.frame.midX - 80, y: view.frame.midY - 25 , width: 230, height: 50))
        messageFrame.layer.cornerRadius = 15
        messageFrame.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        if indicator {
            activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
            activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
            activityIndicator.startAnimating()
            messageFrame.addSubview(activityIndicator)
        }
        messageFrame.addSubview(strLabel)
        view.addSubview(messageFrame)
    }

}



