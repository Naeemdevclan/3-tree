//
//  TicketTableViewCell.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 19/04/2017.
//  Copyright © 2017 Muhammad Naeem. All rights reserved.
//
var imagesList = [Any]()
import UIKit
class TicketTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblTicketId: UILabel!
    @IBOutlet weak var imgTicketColor: UIImageView!
    @IBOutlet weak var lblTicketDate: UILabel!
    @IBOutlet weak var lblTicketTime: UILabel!
    @IBOutlet weak var lblTicketSubject: UILabel!
    @IBOutlet weak var lblTicketDepartment: UILabel!
    @IBOutlet weak var lblDateAndTime: UILabel!
    @IBOutlet weak var lbStatus: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var lblReplyBy: UILabel!
    @IBOutlet weak var lblDataTimeReply: UILabel!
    @IBOutlet weak var lblReplyMsg: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
