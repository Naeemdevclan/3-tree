//
//  VCMapView.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 08/04/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import Foundation
import MapKit


extension SwormModeViewController: MKMapViewDelegate{//, UITableViewDataSource,UITableViewDelegate,UIPopoverPresentationControllerDelegate {
    
    // 1
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        if !(annotation is Artwork) {
//            return nil
//        }
        
        if annotation.isKind(of: default_Artwork_Auditor.self){
            let reuseId = "AuditorAnno"
            var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
            if anView == nil {
                anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
                anView!.canShowCallout = true
            }
            else {
                anView!.annotation = annotation
            }
            //Set annotation-specific properties **AFTER**
            //the view is dequeued or created...
            
            let cpa = annotation as! default_Artwork_Auditor
            print("cpa.locationName =",cpa.locationName)
            anView!.image = UIImage(named:cpa.imageName)
            print("auditor clicked.")
            
            
            let subtitleView = UILabel()
            subtitleView.font = subtitleView.font.withSize(12)
            subtitleView.numberOfLines = 0
            subtitleView.text = annotation.subtitle!
            anView!.detailCalloutAccessoryView = subtitleView
            
            return anView
        }

        if annotation.isKind(of: default_Artwork.self){
            let reuseId = "myCustomAnnotationView"
            var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
            if anView == nil {
                anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
                anView!.canShowCallout = true
            }
            else {
                anView!.annotation = annotation
            }
            //Set annotation-specific properties **AFTER**
            //the view is dequeued or created...
            
            let cpa = annotation as! default_Artwork
            anView!.image = UIImage(named:cpa.imageName)
            
            return anView
        }
        else{
            
            let reuseId = "test"
            
            var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
            if anView == nil {
                anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
                anView!.canShowCallout = true
            }
            else {
                anView!.annotation = annotation
            }
            //Set annotation-specific properties **AFTER**
            //the view is dequeued or created...
            
            let cpa = annotation as! Artwork
            anView!.image = UIImage(named:cpa.imageName)
            
            
            
            if Btn_All_Audits.isSelected == true{
                print("Btn_All_Audits selected now")
            }
            
            if Btn_All_Audits.isSelected == true{
                anView!.rightCalloutAccessoryView = UIButton(type: .detailDisclosure) as UIView
            }
            if Btn_Failed_Audits.isSelected == true{
                anView!.rightCalloutAccessoryView = UIButton(type: .detailDisclosure) as UIView
            }
            if Btn_Final_Audits.isSelected == true{
                anView!.rightCalloutAccessoryView = UIButton(type: .detailDisclosure) as UIView
            }
            if Btn_On_Going_Audits.isSelected == true{
                anView!.rightCalloutAccessoryView = UIButton(type: .detailDisclosure) as UIView
            }
            
            return anView
            //        if let annotation = annotation as? Artwork {
            //            let identifier = "pin"
            //            var view: MKPinAnnotationView
            //            if let dequeuedView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier) as? MKPinAnnotationView { // 2
            //                    dequeuedView.annotation = annotation
            //                    view = dequeuedView
            //            } else {
            //                // 3
            //                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            //                view.canShowCallout = true
            //                view.calloutOffset = CGPoint(x: -5, y: 5)
            //                view.rightCalloutAccessoryView = UIButton(type: .DetailDisclosure) as UIView
            //                
            //            }
            //            
            //            let cpa = annotation as Artwork
            //            view.image = UIImage(named:cpa.imageName)
            //            return view
            //        }
            //        return nil
        }
    }
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view.annotation!.isKind(of: default_Artwork.self){
            print("default_Artwork is selected")
        }

        
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
       let location =  view.annotation as! Artwork
        print("location.title =",location.title)
        print("location.discipline =",location.discipline)
        
       performSegue(withIdentifier: "showPOPUP", sender: view)
//        contentController.modalPresentationStyle = UIModalPresentationStyle.Popover
//        let popOverPC: UIPopoverPresentationController = contentController.popoverPresentationController!
////        popOverPC.barButtonItem = areaListBtn
//        popOverPC.permittedArrowDirections = UIPopoverArrowDirection.Any
//        popOverPC.delegate = self
//        presentViewController(contentController, animated: true, completion: nil)

    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let tableArray =  (sender! as AnyObject).annotation as! Artwork
        print(tableArray.title)
        print(tableArray.discipline)

        if segue.identifier == "showPOPUP" {
            print("transitioning to detail")
            let AnnotationPopupNavigationController = segue.destination as! UINavigationController
            let destinationVC = AnnotationPopupNavigationController.topViewController as! AnnotationPopup
            destinationVC.title = tableArray.title
            destinationVC.popupArray = tableArray.discipline
        }
    }
}


