//
//  default_Artwork_Auditor.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 12/04/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
import MapKit

class default_Artwork_Auditor: NSObject, MKAnnotation {
    
    let title: String?
    let locationName: String
    let discipline: String
    let coordinate: CLLocationCoordinate2D
    var imageName: String!
    
    
    init(title: String, locationName: String, discipline: String, coordinate: CLLocationCoordinate2D, imageName: String!, dateTime: String?) {
        self.title = title //+" "+discipline
        
        if dateTime != nil{
            self.locationName = locationName+"\n"+dateTime!
        }else{
            self.locationName = locationName
        }
        
        self.discipline = discipline
        self.coordinate = coordinate
        self.imageName = imageName
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
    

    
}
