//
//  FirstViewController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 25/02/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
var isUpdateAvaialble = false
class mainView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    func appUpdateAvailable() -> Bool
    {
        let storeInfoURL: String = "http://itunes.apple.com/lookup?bundleId=com.3-tree.QUONDA"
        var upgradeAvailable = false
        // Get the main bundle of the app so that we can determine the app's version number
        let bundle = Bundle.main
        if let infoDictionary = bundle.infoDictionary {
            // The URL for this app on the iTunes store uses the Apple ID for the  This never changes, so it is a constant
            let urlOnAppStore = NSURL(string: storeInfoURL)
            if let dataInJSON = NSData(contentsOf: urlOnAppStore! as URL) {
                // Try to deserialize the JSON that we got
                if let dict: NSDictionary = try! JSONSerialization.jsonObject(with: dataInJSON as Data, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject] as NSDictionary? {
                    if let results:NSArray = dict["results"] as? NSArray {
                        print(results[0] as! [String : Any])
                        if let version = ((results[0]) as AnyObject)["version"] as? String {
                            // Get the version number of the current version installed on device
                            if let currentVersion = infoDictionary["CFBundleShortVersionString"] as? String {
                                // Check if they are the same. If not, an upgrade is available.
                                print("\(version)")
                                
                                print("\(currentVersion)")
                                if version != currentVersion {
                                    //self.popupMessage("\(currentVersion) Update is Avaialable")
                                    // AppVersion.text = "Application Version:" + currentVersion
                                    //  AppVersion.textColor = UIColor.red
                                    upgradeAvailable = true
                                }else{
                                    // AppVersion.text = "Application Version:" + version
                                    // AppVersion.textColor = UIColor.green
                                }
                            }
                        }
                    }
                }
            }
        }
        return upgradeAvailable
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        
        if defaults.object(forKey: "username") != nil && defaults.object(forKey: "password") != nil{
           // print("defaults.objectForKey(userid) = ",defaults.object(forKey: "userid"))
//            let usrName:String = defaults.object(forKey: "username")as! String
//            let pas:String = defaults.object(forKey: "password")as! String
            //print("userName = ",usrName)
            //print("password",pas)
            isUpdateAvaialble = self.appUpdateAvailable()

            self.view.resignFirstResponder()
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "3")
            
            self.present(vc, animated: true, completion: nil)
        }
        else{
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "loginViewController")
            self.present(vc, animated: true, completion: nil)
        }
    }


}

