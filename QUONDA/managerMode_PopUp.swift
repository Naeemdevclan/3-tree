//
//  managerMode_PopUp.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 24/10/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class managerMode_PopUp: UIViewController {

    @IBOutlet weak var backView: UIView!
    
    var jsonFilePath:URL!
    var auditCode_Tapped = ""
    var demoText = ""
    
    
    
    
    var dictionary:NSDictionary!
    
    
    //IBOutlets
    @IBOutlet weak var pictureImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var auditCodeLabel: UILabel!
    @IBOutlet weak var auditorLabel: UILabel!
    @IBOutlet weak var vendorLabel: UILabel!
    @IBOutlet weak var brandLabel: UILabel!
    @IBOutlet weak var poLabel: UILabel!
    @IBOutlet weak var styleLabel: UILabel!
    @IBOutlet weak var seasonLabel: UILabel!
    @IBOutlet weak var EtdRequiredLabel: UILabel!
    @IBOutlet weak var reportTypeLabel: UILabel!
    @IBOutlet weak var auditStageLabel: UILabel!
    @IBOutlet weak var auditTimeLabel: UILabel!
    @IBOutlet weak var sampleSizeLabel: UILabel!
    @IBOutlet weak var lineLabel: UILabel!
    @IBOutlet weak var defectsLabel: UILabel!
    @IBOutlet weak var auditResultLabel: UILabel!
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        print("auditCode_Tapped =",auditCode_Tapped)
        showAnimate()
        
        
        // adding tap gesture to dismiss the view
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissCurrentView))
        self.backView.isUserInteractionEnabled=true
        self.backView.addGestureRecognizer(tapGesture)
        
    }

    
    
    override func viewDidAppear(_ animated: Bool) {
        
        do {
            var readString: String
            readString = try NSString(contentsOfFile: jsonFilePath.path, encoding: String.Encoding.utf8.rawValue) as String
            print("readString=\(readString)")
            self.activityIndicator.startAnimating()
            self.ShowPopUpData(readString)
            
        } catch let error as NSError {
            print(error.description)
        }
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissCurrentView() -> Void {
        print("Going to dismiss ManagerMode PopUp")
        print("demoText =",demoText)
        
        UIView.animate(withDuration: 0.17, animations: {
            self.view.transform = CGAffineTransform(scaleX: 0.2, y: 0.2) // onstart it will make self.view from Normal to Smaller
            self.view.alpha = 0.0 // This alpha value which is zero, means white color
            
        }, completion: { (finished) in
            if finished {
                self.view.removeFromSuperview()
            }
        }) 
        
    }
    
    
    
    func ShowPopUpData(_ savedData:String?) {
        
        print("savedDataString=\(savedData)")
        
        if savedData != nil && savedData != "" {
            
            self.dictionary = self.parseJSON(savedData!)
            
            let All_Audits = self.dictionary["Audits"]! as! [[String : AnyObject]]
            print("Total Audits = \((self.dictionary["Audits"]! as AnyObject).count)")
            print("All_Audits =",All_Audits)
            
            for Audit in All_Audits{
                if auditCode_Tapped == Audit["AuditCode"]! as! String{
                    
                    let AuditCode,Auditor,Vendor,Brand,Po,Style,Season,ETDrequired,ReportType,AuditStage: String
                    let AuditTime,SampleSize,Line,Defects,AuditResult,Picture : String?
                    
                    AuditCode = Audit["AuditCode"]! as! String
                    Auditor = Audit["Auditor"]! as! String
                    Vendor = Audit["Vendor"]! as! String
                    Brand = Audit["Brand"]! as! String
                    Po = Audit["Po"]! as! String
                    Style = Audit["Style"]! as! String
                    Season = Audit["Season"]! as! String
                    ETDrequired = Audit["EtdRequired"]! as! String
                    ReportType = Audit["ReportType"]! as! String
                    AuditStage = Audit["AuditStage"]! as! String
                    AuditTime = Audit["AuditTime"]! as? String
                    SampleSize = Audit["SampleSize"]! as? String
                    Line = Audit["Line"] as? String
                    Defects = Audit["Defects"]! as? String
                    AuditResult = Audit["AuditResult"]! as? String
                    Picture = Audit["Picture"]! as? String
                    
                    print("data against selected audit =",AuditCode,Auditor,Vendor,Brand,Picture!,ETDrequired,AuditTime!)
                    
                    //setting data to IBOutlets
                    self.auditCodeLabel.text = AuditCode
                    auditorLabel.text = Auditor
                    vendorLabel.text = Vendor
                    brandLabel.text = Brand
                    poLabel.text = Po
                    styleLabel.text = Style
                    seasonLabel.text = Season
                    EtdRequiredLabel.text = ETDrequired
                    reportTypeLabel.text = ReportType
                    auditStageLabel.text = AuditStage
                    auditTimeLabel.text = AuditTime
                    sampleSizeLabel.text = SampleSize
                   
                    if let line = Line{
                        lineLabel.text = Line
                    }else{
                        lineLabel.text = "-"
                    }
                    
                    defectsLabel.text = Defects
                    auditResultLabel.text = AuditResult
                    
                    // set default image
                    
                    DispatchQueue.main.async(execute: {
                        self.pictureImageView.image = UIImage(named: "default.jpg")
                    })
                    
                    // now get image from server\\
                    print("Pictures == ")
                    print(Picture! == "" ? "default.jpg" : Picture!)
                    let request: URLRequest = URLRequest.init(url: URL.init(string: Picture == "" ? "default.jpg" : Picture!)!)
                    
                
                    let session = URLSession.shared
                    
                    let ImageDownloadTask = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                        
                        if error == nil {
                            
                            // Convert the downloaded data in to a UIImage object
                            let image = UIImage(data: data!)
                            print("image =",image)
                            
                            DispatchQueue.main.async(execute: {
                                
                                self.pictureImageView.image = image
                                self.activityIndicator.stopAnimating()
                            })
                            
                        }
                        else {
                            print("Error: \(error!.localizedDescription)")
                            
                            DispatchQueue.main.async(execute: {
                            
                                self.activityIndicator.stopAnimating()
                            })

                        }

                    }) 
                    ImageDownloadTask.resume()

                }
            }
                
        }
    }
    

    
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }

    
    
    
    func showAnimate() -> Void {
        self.view.transform = CGAffineTransform(scaleX: 0.5, y: 0.5) // onstart it will make self.view  from smaller to a little bigger
        self.view.alpha = 0.0 // This alpha value which is zero, means white color
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0) // Now when animation Ended it will make self.view to normal
        }) 
    }
    
    
    
    
    
    
    func removeAnimate() -> Void {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 0.5, y: 0.5) // onstart it will make self.view a little big than normal
            self.view.alpha = 0.0 // This alpha value which is zero, means white color
            
        }, completion: { (finished) in
            if finished {
                //                    self.dismissViewControllerAnimated(true, completion: nil)
                self.view.removeFromSuperview()
            }
        }) 
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
