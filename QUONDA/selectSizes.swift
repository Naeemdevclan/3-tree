//
//  selectSizes.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 27/05/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
var SizesData = [IndexPath:String]()
var SizesDataID = [IndexPath:String]()

var pTappedSizeButtonIndexPath:IndexPath?
var pSizeButtonTag:Int!

var Sizeitems: [String] = []
var SizeitemsID: [String] = []

class selectSizes: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBAction func backButtonPressed(_ sender: AnyObject) {
        self.dismiss(animated: false, completion: nil)
    }


    @IBOutlet weak var sizesTable: UITableView!
    //    var data = [Int:String]()
    var dictionary:NSDictionary!
//    var jsonFilePath2:NSURL!
//    var created:Bool!
//    let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first!
//    var isDirectory: ObjCBool = false
//    let fileManager = NSFileManager.defaultManager()
    
    
    var cell:UITableViewCell!
    //    var pressed:Bool = false
    var selectedButton:NSMutableArray! = NSMutableArray()

    var items: [String] = []
    var itemsID: [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       
        items = Sizeitems
        itemsID = SizeitemsID
        items.append("Select All")
        itemsID.append("A")
        
//        let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
//        
//        let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("login.json")
//        jsonLoginFilePath = jsonFilePath
//        print(jsonLoginFilePath.path!)
//        
//        do {
//            var readString: String
//            readString = try NSString(contentsOfFile: jsonLoginFilePath.path!, encoding: NSUTF8StringEncoding) as String
//            print(readString)
//            self.showDataWhenOffline(readString)
//        } catch let error as NSError {
//            print(error.description)
//        }
        
        for i in 0 ..< (items.count) //yourTableSize = how many rows u got
        {
            selectedButton.add("NO")
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        //        for (var i = 0; i<items.count; i++) //yourTableSize = how many rows u got
        //        {
        //            selectedButton.addObject("NO")
        //        }
        print(DefectsSizesData0)
        if !DefectsSizesData0.isEmpty {
            for d2 in DefectsSizesData0{
                print(d2)
                print(selectedButton)
                selectedButton.replaceObject(at: d2.0.row, with: "YES")
                
                if let alreadyCheckedCell = self.sizesTable.cellForRow(at: d2.0 as IndexPath){
                    SizesData[d2.0 as IndexPath] = alreadyCheckedCell.textLabel!.text
                    SizesDataID[d2.0 as IndexPath] = alreadyCheckedCell.detailTextLabel?.text
                }
            }
            //            DefectsSizesData0.removeAll()
            self.sizesTable.reloadData()
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        return self.dictionary["DefectAreas"]!.count
        return items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let Ccell = self.sizesTable.dequeueReusableCell(withIdentifier: "ccell")!
        //        var checkBox:UIButton = UIButton(frame: CGRect(x: Ccell.frame.width, y: Ccell.frame.height/4, width: 30, height: 30))
        Ccell.textLabel!.text = items[indexPath.row]
        Ccell.detailTextLabel!.text = itemsID[indexPath.row]
        Ccell.detailTextLabel?.isHidden = true
        
        Ccell.textLabel!.font = UIFont(name: Ccell.textLabel!.font.fontName, size: 13)
        
        //   Ccell.checkBox.layer.cornerRadius = Ccell.checkBox.frame.height/2
        //        checkBox.setBackgroundImage(UIImage(named: "Radio-OFF"), forState: .Normal)
        //        checkBox.setBackgroundImage(UIImage(named: "Radio-ON"), forState: .Selected)
        
        //        checkBox.tag = indexPath.row+100
        //        checkBox.addTarget(self, action: "checkBoxTapped:", forControlEvents: UIControlEvents.TouchUpInside)
        
        //        if selectedButton.objectAtIndex(indexPath.row).isEqualToString("NO"){
        //            checkBox.selected = false
        //        }else{
        //            checkBox.selected = true
        //        }
        if (selectedButton.object(at: indexPath.row) as AnyObject).isEqual(to: "NO"){
            Ccell.accessoryType = .none
//            Ccell.backgroundColor = UIColor.whiteColor()
        }else {
            Ccell.accessoryType = .checkmark
//            Ccell.backgroundColor = UIColor.lightGrayColor()
        }
        //        Ccell.contentView.addSubview(checkBox)
        
        return Ccell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("selected Index row path\(indexPath.row)")
       
        let x = indexPath.row//button.tag - 100
        let cellPressed = tableView.cellForRow(at: indexPath)!
        
        if(cellPressed.textLabel!.text == "Select All"){
          if cellPressed.accessoryType == .none{
            for item in 0..<items.count{
                let indexPath = IndexPath(row: item, section: 0)
                //if cellPressed.accessoryType == .none{
                    cellPressed.accessoryType = .checkmark
                    //            cellPressed.backgroundColor = UIColor.lightGrayColor()
                    selectedButton.replaceObject(at: item, with: "YES")
                    SizesData[indexPath] = items[item] as String
                    SizesDataID[indexPath] = itemsID[item] as String
                    
                    
               
                DefectsSizesData0 = SizesData as [IndexPath : String]
                
                }
              }else if cellPressed.accessoryType == .checkmark{
            for item in 0..<items.count{
                let indexPath = IndexPath(row: item, section: 0)
                cellPressed.accessoryType = .none
                //            cellPressed.backgroundColor = UIColor.whiteColor()
                selectedButton.replaceObject(at: item, with: "NO")
                SizesData.removeValue(forKey: indexPath)
                SizesDataID.removeValue(forKey: indexPath)
            }
            DefectsSizesData0 = SizesData as [IndexPath : String]
                
            }
tableView.reloadData()
        }else{
        
        if cellPressed.accessoryType == .none{
            cellPressed.accessoryType = .checkmark
//            cellPressed.backgroundColor = UIColor.lightGrayColor()
            selectedButton.replaceObject(at: x, with: "YES")
            SizesData[indexPath] = cellPressed.textLabel!.text
            SizesDataID[indexPath] = cellPressed.detailTextLabel?.text
            
            
        }else if cellPressed.accessoryType == .checkmark{
            cellPressed.accessoryType = .none
//            cellPressed.backgroundColor = UIColor.whiteColor()
            selectedButton.replaceObject(at: x, with: "NO")
            SizesData.removeValue(forKey: indexPath)
            SizesDataID.removeValue(forKey: indexPath)
            
        }
        
        
        // - - Dismiss the current view - - - - - - -
        DefectsSizesData0 = SizesData as [IndexPath : String]
        }
    }
    
    func before(_ value1: String, value2: String) -> Bool {
        // One string is alphabetically first.
        // ... True means value1 precedes value2.
        return value1 < value2;
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
}


