//
//  syncService.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 01/08/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
import RebekkaTouch

class syncService {
   
    // Rebekka Touch FTP Session
    var session: Session!
    var dirArray:[AnyObject]!
    var monArray:[AnyObject]!
    var dayArray:[AnyObject]!
    
    var resource: ResourceItem!
    var monthResource: String!
    var dayResource: String!
    
    var paramPath = "/quonda"
    
    // create FTP Session configuration
    var configuration = SessionConfiguration()
    
    var serverIMGPath:String?
    
    //-let locationManager = CLLocationManager()
    
    
    init(){
        print("sync init() called")
        
        createSyncFile()
        
        
        configuration.host = "192.163.245.220"
        configuration.username = "ali"
        configuration.password = "AAutpksa1546"
        
        self.session = Session(configuration: configuration)
    
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reinstateBackgroundTask), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        updateTimer = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(self.syncAudit), userInfo: nil, repeats: true)
        
        registerBackgroundTask()
        
        // send Auditor Location to the server
        //-NSTimer.scheduledTimerWithTimeInterval(1800.0, target: self, selector: #selector(self.sendAuditorLocation), userInfo: nil, repeats: true)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    
//    func sendAuditorLocation() {
//        
//        self.locationManager.delegate = self
//        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
//        self.locationManager.requestWhenInUseAuthorization()
//        self.locationManager.startUpdatingLocation()
//        
//        print("loaction config called")
//    }
//    
//    
//    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        
//        print("coordinate =",locations.first?.coordinate)
//        
//        var latitudeLabel = String(locations.first!.coordinate.latitude)
//        var longitudeLabel = String(locations.first!.coordinate.longitude)
//        
//        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error) -> Void in
//            if (error != nil) {
//                print("ERROR:" + error!.localizedDescription)
//                return
//            }
//            print("placemarks!.count = ",placemarks!.count)
//            
//            if placemarks!.count > 0 {
//                let pm = placemarks![0]
//                self.saveLatitudeLongitude(pm)
//                print("called after saveLatitude")
//            } else {
//                print("Problem with the data received from geocoder")
//            }
//            
//        })
//    }
//    
//    
//    
//    func saveLatitudeLongitude(placemark: CLPlacemark) {
//
//        var latitudeLabel:String = String(placemark.location!.coordinate.latitude)
//        var longitudeLabel:String = String(placemark.location!.coordinate.longitude)
////        let lat = String(placemark.location!.coordinate.latitude)
//        print("lat =", latitudeLabel)
//       
//        var bodyData = "User=\(defaults.objectForKey("userid")as! String)&Latitude=\(latitudeLabel)&Longitude=\(longitudeLabel)"
//        print("bodydata for location =",bodyData)
//        
//        let request: NSMutableURLRequest = NSMutableURLRequest(URL: NSURL(string: "http://portal.3-tree.com/api/android/location.php")!)
//        var json = ""
//        request.HTTPMethod = "POST"
//        request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding)
//        
//        let session = NSURLSession.sharedSession()
//        
//        let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
//            
//            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
//                print("error")
//                return
//            }
//            if (error != nil) {
//                print(error)
//                
//            }
//            else if data != nil {
//                json = NSString(data: data!, encoding: NSUTF8StringEncoding) as! String
//                
//                print(json)
//                
//                
//                var dictionary:NSDictionary = self.parseJSON(json)
//                
//                print("information when Schedule btn clicked = \(dictionary)")
//                
//                if("\(dictionary["Status"]!)" == "OK"){
//                    
//                   print("Successfuly sent Coordinates \(dictionary["Message"]!)")
//                    
//                }else{
//                    print("There is an error while sending Coordinates to the server.")
//                }
//                
//            }//if data != nil
//            else{
//                print("return data is nil")
//            }
//            
//        }
//        
//        task.resume()
//    
//
//        self.locationManager.stopUpdatingLocation()
//    }
//    
//    
//    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
//        print("Error:" + error.localizedDescription)
//    }
//    
    
    
    func createSyncFile(){
        
// //  //  // Now Creating FilesToSync.json file to save List of FileNames that will be created in whole audit process
 
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        
        let jsonSyncFilePath = documentsDirectoryPath.appendingPathComponent("FilesToSync.json")
        
        // creating a FilesToSync.json file in the Documents folder
        let created:Bool?
        if !fileManager.fileExists(atPath: jsonSyncFilePath.absoluteString, isDirectory: &isDirectory) {
            created = fileManager.createFile(atPath: jsonSyncFilePath.absoluteString, contents: nil, attributes: nil)
            if (created != nil) {
                print("FilesToSync.json File created ")
                
            } else {
                print("Couldn't create FilesToSync.json File for some reason")
            }
        } else {
            print("FilesToSync.json File already exists")
        }
        //file creation ends
        
    }
    
    
    
    func removeDuplicates(_ array: [String]) -> [String] {
        var encountered = Set<String>()
        var result: [String] = []
        for value in array {
            if encountered.contains(value) {
                // Do not add a duplicate element.
            }
            else {
                // Add value to the set.
                encountered.insert(value)
                // ... Append the value.
                result.append(value)
            }
        }
        return result
    }
    
    func scheduleLocalNotification(title:String,message:String) {
        let localNotification = UILocalNotification()
        localNotification.fireDate = Date.init(timeIntervalSinceNow: 5)
        localNotification.alertBody = "\(message)"
        localNotification.alertTitle = title
        localNotification.alertAction = "View List"
        localNotification.category = "shoppingListReminderCategory"
        UIApplication.shared.scheduleLocalNotification(localNotification)
        
        // scheduleNotifications()        UIApplication.shared.scheduledLocalNotifications()

        
    }
    @objc func syncAudit(){
       
        print("< Syncronizing Start >")
        
        // first read out the file
        do{
            let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
            let jsonSyncFilePath = documentsDirectoryPath.appendingPathComponent("FilesToSync.json")
            
            var readString: String?
            readString = try NSString(contentsOfFile: jsonSyncFilePath.path, encoding: String.Encoding.utf8.rawValue) as String
            print("Reading_syncFile_when_service_called =",readString)
              //scheduleLocalNotification()
            if readString != nil && readString != "" {
                
                var FilesIn_SyncFile:[String] = parseSyncFile(readString!)
                
               let ff = self.removeDuplicates(FilesIn_SyncFile)
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: ff, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: jsonSyncFilePath.path, atomically: true, encoding: String.Encoding.utf8)
                }catch{
                    print("cannot write after removing duplicates")
                }
                
                readString = try NSString(contentsOfFile: jsonSyncFilePath.path, encoding: String.Encoding.utf8.rawValue) as String
                FilesIn_SyncFile = parseSyncFile(readString!)
                print("Files_in_SyncFile_while_syncronizing =",FilesIn_SyncFile)
                
                SyncFilesWithPaths(FilesIn_SyncFile)
                
                // Now Read all the files in sync file if any of the file is empty then delete this file from document dir and also delete this file Name from sync file.
                
            }
          
        }
        catch let error as NSError{
            print("There is an error while reading AuditProgress(AuditCode) File.",error.description)
        }
      
    }
    
    
    func SyncFilesWithPaths(_ ArrayOfFilesPaths:[String]){
        
        //- - - - - - - - -  - -
        //First Read out the sync file to find if count of (AuditProgress + AddDefectText + AddDefectImage) is == 0 Then send request for comments file.
        
        var totalJsonFiles_OtherThan_CommentFile:Int!
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        let jsonSyncFilePath = documentsDirectoryPath.appendingPathComponent("FilesToSync.json")
        
        var fileDataString: String!
        do{
            fileDataString = try NSString(contentsOfFile: jsonSyncFilePath.absoluteString, encoding: String.Encoding.utf8.rawValue) as String
        }catch{
            print("couldn't read file to Count Audit keys")
        }
        if fileDataString != nil{
            var DataInAuditProgressFile:[String] = parseSyncFile(fileDataString)
            
            
            print("All FilePaths in sync File =",DataInAuditProgressFile)
            
            DataInAuditProgressFile = DataInAuditProgressFile.filter({ (PathInSyncFile) -> Bool in
                !PathInSyncFile.contains("Comment")
            })
            print("All FilePaths except Comment files =",DataInAuditProgressFile)
            totalJsonFiles_OtherThan_CommentFile = DataInAuditProgressFile.count
            
//            if totalJsonFiles_OtherThan_CommentFile == 0{
//                // send comment file to server
//            }else{
//                // send all other files to server
//            }
        }
        
        // - - - - - - - - - - - - -
        
        

        
        for filePath in ArrayOfFilesPaths{
            
            print("filePath =",filePath)
            
            if filePath.contains("Comment"){
                
                if totalJsonFiles_OtherThan_CommentFile != nil && totalJsonFiles_OtherThan_CommentFile == 0{
                    // send comment file to server
                    
                    let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
                    let filePath = documentsDirectoryPath.appendingPathComponent(filePath).absoluteString
                    if fileManager.fileExists(atPath: filePath, isDirectory: &isDirectory ){
                        print("sending.. comment file")
                        serialQueue.async(execute: {
                            
                            print("you are in serial queue")
                            
                            self.callServer(filePath)
                            
                        })
                    }
                }
            }
            else{
                // send all other files to server
                
                let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
                let filePath = documentsDirectoryPath.appendingPathComponent(filePath).absoluteString
                if fileManager.fileExists(atPath: filePath, isDirectory: &isDirectory ){
                     print("sending.. other than comment file")
                    serialQueue.async(execute: {
                        
                        print("you are in serial queue")
                        
                        self.callServer(filePath)
                        
                    })
                }
            }
            
//            
//            let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!
//            let filePath = documentsDirectoryPath.URLByAppendingPathComponent(filePath).absoluteString
//         
//            
//            if fileManager.fileExistsAtPath(filePath, isDirectory: &isDirectory ){
//            
//                // Do sync process for all the filePath
////                sleep(4)
//                dispatch_async(serialQueue, {
//                    
//                    print("you are in serial queue")
//                    
//                    self.callServer(filePath)
//                    
//                })
//            }
        }
        
    }
    
    
    
    func callServer(_ filePath:String){
        
            if !filePath.contains("ImagesFile"){
                
                self.sendFIFO(filePath)
                
            }else{
                
                
                // First get auditCode from this imagePath
                //Then send server request to upload Defect Images
                print(filePath)
                let auditCode = self.ExtractAuditCodeFromImageFilePath(filePath)
                 print(auditCode)
                let ScheduleDate = defaults.object(forKey: auditCode) as? String
                
                
                if ScheduleDate != nil{
                    
                    paramPath = "/quonda"
                    serverIMGPath = ""
                    serverIMGPath = ListDir_And_UploadImage(ScheduleDate!,filePath: filePath)
                }
                
                //                print("serverIMGPath =",serverIMGPath)
                print("paramPath =",paramPath)
                
                
                //                let delay = 5.0 * Double(NSEC_PER_SEC)
                //                let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                //                sleep(7)
                //                dispatch_after(time, serialQueue2) {
                
                //                    self.sendFIFO_IMG(filePath)
                // Here do call to serverImage Request
                //                }
                
                
        }
    }
    
    func ExtractAuditCodeFromImageFilePath(_ filePath:String) ->String {
        
//        let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("AddDefectImagesFile\(self.AuditCodeLabel.text!).json")
//        print("jsonFilePath =",jsonFilePath)
       print("filePath before extract AuditCode =",filePath)
        var NewPath = filePath  //jsonFilePath.absoluteString
       
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        let documentsDirectoryPath_AbsouluteString = documentsDirectoryPath.absoluteString
        let totalCharactersInDocDir = documentsDirectoryPath_AbsouluteString.characters.count
        
        let jsonExt = ".json"
        let ImageName = "/AddDefectImagesFile"
        
        let rangeOfFilePath = filePath.characters.index(filePath.startIndex, offsetBy: totalCharactersInDocDir)..<filePath.characters.index(filePath.endIndex, offsetBy: -jsonExt.characters.count)
        print("rangeOfFilePath = ",rangeOfFilePath)
        
        
        let rangeOfDocDir = NewPath.range(of: documentsDirectoryPath_AbsouluteString)
        NewPath.removeSubrange(rangeOfDocDir!)
        print("rangeOfDocDir = ",rangeOfDocDir)
        print("NewPath = ",NewPath)
        
        
        let rangeOfImageName = NewPath.range(of: ImageName)
        NewPath.removeSubrange(rangeOfImageName!)
        print("rangeOfImageName in NewPath =",rangeOfImageName)
        print("NewPath = ",NewPath)
        
        
        let rangeOfFileExt = NewPath.range(of: jsonExt)
        NewPath.removeSubrange(rangeOfFileExt!)
        print("rangeOfFileExtension in NewPath =",rangeOfFileExt)
        print("AuditCode in NewPath = ",NewPath)
        
        return NewPath
    }
    
    //documentsDirectoryPath.URLByAppendingPathComponent("AuditCommentFile\(myCell!.textLabel!.text!).json")
  
    func ExtractAuditCodeFromCommentFilePath(_ filePath:String) ->String {
        
        //        let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("AddDefectImagesFile\(self.AuditCodeLabel.text!).json")
        //        print("jsonFilePath =",jsonFilePath)
        print("filePath before extract AuditCode =",filePath)
        var NewPath = filePath  //jsonFilePath.absoluteString
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        let documentsDirectoryPath_AbsouluteString = documentsDirectoryPath.absoluteString
        let totalCharactersInDocDir = documentsDirectoryPath_AbsouluteString.characters.count
        
        let jsonExt = ".json"
        let commentFileName = "/AuditCommentFile"
        
        let rangeOfFilePath = filePath.characters.index(filePath.startIndex, offsetBy: totalCharactersInDocDir)..<filePath.characters.index(filePath.endIndex, offsetBy: -jsonExt.characters.count)
        print("rangeOfFilePath = ",rangeOfFilePath)
        
        
        let rangeOfDocDir = NewPath.range(of: documentsDirectoryPath_AbsouluteString)
        NewPath.removeSubrange(rangeOfDocDir!)
        print("rangeOfDocDir = ",rangeOfDocDir)
        print("NewPath = ",NewPath)
        
        
        let rangeOfcommentFileName = NewPath.range(of: commentFileName)
        NewPath.removeSubrange(rangeOfcommentFileName!)
        print("rangeOfcommentFileName in NewPath =",rangeOfcommentFileName)
        print("NewPath = ",NewPath)
        
        
        let rangeOfFileExt = NewPath.range(of: jsonExt)
        NewPath.removeSubrange(rangeOfFileExt!)
        print("rangeOfFileExtension in NewPath =",rangeOfFileExt)
        print("AuditCode in NewPath = ",NewPath)
        
        return NewPath
    }
    
    
    
    func sendFIFO(_ filePath:String){
        
        if fileManager.fileExists(atPath: filePath, isDirectory: &isDirectory) {
            
            var filePathWithinFunction = filePath
            
            // first read out the file
            do{
                var readString: String
                readString = try NSString(contentsOfFile: filePathWithinFunction, encoding: String.Encoding.utf8.rawValue) as String
                
                print("reading saved file =",readString)
               // let str = readString.
//                let range: Range<String.Index> = readString.range(of: "O")!
//                let index: Int = readString.distance(from: readString.startIndex, to: range.lowerBound)
//                let endIndex = readString.index(readString.startIndex, offsetBy: 11)
//                let acb = index as! String.Index
//               let abc = readString[index...endIndex]
//                print(abc)
//
//                print(index)
                let pass = "P";
                print(pass)
                readString = readString.replacingOccurrences(of:"Optional(P)", with: "P")

print(readString)
                var readAuditProgressParsed:NSDictionary = self.parseJSON(readString)
                print(readString)

                
                let values = readAuditProgressParsed.allValues as! [String]
                let keys = readAuditProgressParsed.allKeys as! [String]
                
                print("| | | | | | | | |\n")
                print("file Keys =",keys)
                print("file Keys =",values)
                
                // var bodyValues:[String]!    //this array cannot append it should be assigned an empty array
                
                var bodyValues:[String] = []
                var bodyUrl:String!
                
                var bodyKeys:[String] = []
                var bodyKeys_int:[Int] = []
                
                for k:String in keys{
                    
                    if !k.contains("url") && k != ""{
                        
                        if filePath.contains("AuditProgress"){
                            let kk = Int(k)!
                            bodyKeys_int.append(kk)
                        }else{
                            bodyKeys.append(k)
                        }
                    }
                }
//                var ij = unsortedCountries.sort({ (a, b) -> Bool in
                    //                print(a)
                    //                print(b)
                    ////                if a < b {
                    ////                    return true
                    ////                }
                    //                return false
                    //            })
                if !bodyKeys_int.isEmpty && bodyKeys.isEmpty{
                    bodyKeys_int = bodyKeys_int.sorted{$0 < $1}
                    for k in bodyKeys_int{
                        let sk = String(k)
                        bodyKeys.append(sk)
                    }
                }
                
                
                var sorted_bodyKeys:[String] = []
                
                if filePath.contains("AuditProgress"){
                
                    sorted_bodyKeys = bodyKeys
                // Apply sorting here to Body Keys
//                    sorted_bodyKeys = bodyKeys.sort{$0 < $1}
                    
                }else{
                    
                    sorted_bodyKeys = bodyKeys.sorted()
                }

                print("sorted_bodyKeys =",sorted_bodyKeys)
                
                for v:String in values{
                    
                    if v.contains("http:"){
                        bodyUrl = v
                        
                    }else if v != ""{
                        
                        bodyValues.append(v)
                        
                    }
                }
                
                //                    print("bodyValues =",bodyValues)
                //                    print("bodyKeys =",bodyKeys)
                //                    print("bodyUrl =",bodyUrl)
                
                for key in sorted_bodyKeys{
                    print("Key in Sorted_bodyKeys =",key)
                    print("sorted_bodyKeys =",sorted_bodyKeys)
                    var body = readAuditProgressParsed.value(forKey: key) as! String

                    let url = self.serverUrl(bodyUrl)
                    print("url=",url)
                    print("body= ",body)
                    body = body.replacingOccurrences(of:"Optional(", with: "")
                    body = body.replacingOccurrences(of:")", with: "")
                   // body = body.replacingOccurrences(of:"\"", with: "")
                    //body = body.replacingOccurrences(of:"\"", with: "")



                    
                    print(body)

                    
                    var dictionary:NSDictionary!
                    
                    var status:Bool = false
                    
                    let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
                    var json = ""
                    request.httpMethod = "POST"
                    request.httpBody = body.data(using: String.Encoding.utf8)
                    let session = URLSession.shared
                   
                    session.configuration.timeoutIntervalForRequest = TimeInterval(150)
                    
                    let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
                        print("\(response)")
                        
                        if (error != nil) {
                            
                            status = false
                            print(error)
                        }
                        
                        if data != nil {
                            json = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
                            
                            dictionary = self.parseJSON(json)
                            
                            print("\(dictionary)")
                            print("\(dictionary["Result"])")
                            
                            if ("\(dictionary["Status"]!)" == "OK") {
                                // remove the keyValue pair saved in fileName(AuditCode)file
                                
                                status = true
                                print("Request sent!")
                                //UIApplication.shared.scheduledLocalNotifications
                                if filePath.contains("AuditProgress"){

                               
                                }

                                // removes the keyValue Pair in fileName(AuditCode)File
                                
                                print("start removing KEYVALUE pair \n")
                                
                                var fileDataString: String!
                                do{
                                    fileDataString = try NSString(contentsOfFile: filePathWithinFunction, encoding: String.Encoding.utf8.rawValue) as String
                                }catch{
                                    print("couldn't read file to Remove KV pair")
                                }
                                if fileDataString != nil{
                                    var DataInAuditProgressFile = self.parseToRemove(fileDataString)
                                    
                                    print("Before Removing KEYVALUE pair in AuditProgress =",DataInAuditProgressFile.values)
                                    
                                    DataInAuditProgressFile.removeValue(forKey: key)
                                    
                                    print("\n After Removing KEYVALUE pair in AuditProgress =",DataInAuditProgressFile.values)
                                    
                                    // Now write it back into the file.
                                    
                                    if fileManager.fileExists(atPath: filePathWithinFunction, isDirectory: &isDirectory) {
                                        do{
                                            let jsonDictionary = try JSONSerialization.data(withJSONObject: DataInAuditProgressFile, options: JSONSerialization.WritingOptions.prettyPrinted)
                                            
                                            let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                                            
                                            try json.write(toFile: filePathWithinFunction, atomically: true, encoding: String.Encoding.utf8)
                                            print("Now updated AuditProgress File after Removing KEYVALUE pair. \n")
                                            print("Updated AuditProgress File =",json)
                                            
                                            
                                        } catch let error as NSError {
                                            print("JSON data couldn't be written to file // File not exist")
                                            print(error.description)
                                            
                                        }
                                    } // end of writing to FileName\(AuditCode) file after removing keyValue pair
                                    
                                }
                            
                                
                                // if it is the comment file then delete it.
                                
                                print("filePathWithinFunction = ",filePathWithinFunction)
                                
                               //  Before delete count if in this file bodyKey.count = 0 if yes then delete.
                                   
                               // first read out the file
                                var fileDataString2: String!
                                do{
                                    fileDataString2 = try NSString(contentsOfFile: filePathWithinFunction, encoding: String.Encoding.utf8.rawValue) as String
                                }catch{
                                    print("couldn't read file to Delete")
                                }
                                if fileDataString2 != nil{
                                    
                                    var readAuditProgressParsed:NSDictionary = self.parseJSON(fileDataString2)
                                    
                                    
                                    let keys = readAuditProgressParsed.allKeys as! [String]
                                    
                                    var bodyKeys:[String] = []
                                    
                                    for k:String in keys{
                                        
                                        if !k.contains("url") && k != ""{
                                            bodyKeys.append(k)
                                        }
                                    }
                                    
                                    
                                    if bodyKeys.count == 0{
                                        
                                        //Now delete the file.
                                        do{
                                            try fileManager.removeItem(atPath: filePathWithinFunction)
                                            print("file deleted successfully =",filePathWithinFunction)
                                            print("\n\n")
                                            
                                            //Now also Remove this filePath from syncFile.json
                                            // First read syncFile.json
                                            // Then update syncFile.json
                                            // Then write it back to doc dir. 
                                            let fileName = self.ExtractFileName(filePathWithinFunction)
                                            self.RemoveFileNameFrom_FilesToSync(fileName)
                                            
                                            
                                            //?????
                                            //????
                                            //???
                                            //??
                                            //?
                                        }
                                        catch{
                                            print("Could not delete file: \(error)")
                                        }
                                        
                                    }
                                }//end of fileDataString2 != nil
                                
                                // Set the Status of commentFile that is it fully synced or not. and use this status in AuditorModeViewController.
                                
                                if filePathWithinFunction.contains("Comment"){
                                   
                                    let auditcodeOFcommentFile = self.ExtractAuditCodeFromCommentFilePath(filePathWithinFunction)
                                    
                                    TempDefaults.set(true, forKey: "commentStatus\(auditcodeOFcommentFile)")
                                    self.scheduleLocalNotification(title:"\(auditcodeOFcommentFile) Report Synced",message:"Audit Report Synced")
                                }
                                //End of setting CommentFile Status
                            }
                            
                        }//if data != nil
                        else{
                            status = false
                        }
                    }) 
                    task.resume()
                    
                }
            }
            catch let error as NSError{
                print("There is an error while reading AuditProgress(AuditCode) File.",error.description)
            }
        }
        //        })
        //        }
        //        AuditProgressQueue.addOperation(RequestOperation)
        
    }
    
    func ExtractFileName(_ filePath:String) ->String {
        
        print("filePath before extract FileName =",filePath)
        var FileName = filePath
        
        let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
        let documentsDirectoryPath_AbsouluteString = documentsDirectoryPath.absoluteString
        
        let ForwordSlash = "/"
        
        
        let rangeOfDocDir = FileName.range(of: documentsDirectoryPath_AbsouluteString)
        FileName.removeSubrange(rangeOfDocDir!)
        print("rangeOfDocDir = ",rangeOfDocDir)
        print("NewPath = ",FileName)
        
        
        let rangeOfForwordSlash = FileName.range(of: ForwordSlash)
        FileName.removeSubrange(rangeOfForwordSlash!)
        print("rangeOfForwordSlash in NewPath =",rangeOfForwordSlash)
        print("NewPath = ",FileName)
        
        
        return FileName
    }

    
    func RemoveFileNameFrom_FilesToSync(_ FileName:String) -> Void {
        
        
        // first read out the file
        do{
            let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
            let jsonSyncFilePath = documentsDirectoryPath.appendingPathComponent("FilesToSync.json")
            
            var readString: String
            readString = try NSString(contentsOfFile: jsonSyncFilePath.path, encoding: String.Encoding.utf8.rawValue) as String
            
            let readStringParsed = parseSyncFile(readString)
            print("Files Before Removing the fileName from SyncFile =",readStringParsed)
            /// here send readString to function to update FileNames stored in FilesToSync.json File
            
            removeFile(readString, FileName: FileName, jsonSyncFilePath: jsonSyncFilePath.absoluteString)
            
        }
        catch let error as NSError{
            print("There is an error while reading AuditProgress(AuditCode) File.",error.description)
        }
    }
    
    func removeFile(_ savedData: String?, FileName:String, jsonSyncFilePath:String){
        
        
        if savedData != nil && savedData != "" {
            
            var FilesIn_SyncFile:[String] = parseSyncFile(savedData!)
            
            //remove this 
            
            // "F" is the file in FilesIn_SyncFile and the below filter function will loop over and over through the files in FilesIn_SyncFile.
            
            FilesIn_SyncFile = FilesIn_SyncFile.filter({ (F) -> Bool in
                F != FileName
            })
            //filter{$0 != "Hello"}
            
//            // Append File Name if it already doesn't exist
//            
//            FilesIn_SyncFile.append(FileName)
//            
            
            // Now write into the FilesToSync.json file
            
            if fileManager.fileExists(atPath: jsonSyncFilePath, isDirectory: &isDirectory) {
                do{
                    let jsonDictionary = try JSONSerialization.data(withJSONObject: FilesIn_SyncFile, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                    
                    try json.write(toFile: jsonSyncFilePath, atomically: true, encoding: String.Encoding.utf8)
                    
                    
                } catch let error as NSError {
                    print("JSON data couldn't written to file // File not exist")
                    print(error.description)
                }
            }
            
            
        }
//        else{
//            
//            var FilesIn_SyncFile:[String] = []
//            
//            //remove this
//            
//            // "F" is the file in FilesIn_SyncFile and the below filter function will loop over and over through the files in FilesIn_SyncFile.
//            
//            FilesIn_SyncFile = FilesIn_SyncFile.filter({ (F) -> Bool in
//                F != FileName
//            })
//
////            FilesIn_SyncFile.append(FileName)
//            
//            // Now write into the FilesToSync.json file
//            
//            if fileManager.fileExistsAtPath(jsonSyncFilePath, isDirectory: &isDirectory) {
//                do{
//                    let jsonDictionary = try NSJSONSerialization.dataWithJSONObject(FilesIn_SyncFile, options: NSJSONWritingOptions.PrettyPrinted)
//                    
//                    let json = NSString(data: jsonDictionary, encoding: NSUTF8StringEncoding) as! String
//                    
//                    try json.writeToFile(jsonSyncFilePath, atomically: true, encoding: NSUTF8StringEncoding)
//                    
//                } catch let error as NSError {
//                    print("JSON data couldn't written to file // File not exist")
//                    print(error.description)
//                }
//            }
//        }
        
        //Reading from JsonDictionary File stored in documents directory
        do{
            var readString: String
            readString = try NSString(contentsOfFile: jsonSyncFilePath, encoding: String.Encoding.utf8.rawValue) as String
            
            let readStringParsed = parseSyncFile(readString)
            print("Files after Removing the fileName from SyncFile =",readStringParsed)
        }
        catch let error as NSError{
            print("There is an error while reading from JsonDictionary File.",error.description)
            
        }
        
    }
    
    

    
    func sendFIFO_IMG(_ filePath:String){
        
        if fileManager.fileExists(atPath: filePath, isDirectory: &isDirectory) {
            
            var filePathWithinFunction = filePath
            
            // first read out the file
            do{
                var readString: String
                readString = try NSString(contentsOfFile: filePathWithinFunction, encoding: String.Encoding.utf8.rawValue) as String
                
                print("reading saved IMG.JSON file =",readString)
                
                var readAuditProgressParsed:NSDictionary = self.parseJSON(readString)
                
                
                let values = readAuditProgressParsed.allValues as! [String]
                let keys = readAuditProgressParsed.allKeys as! [String]
                
                print("| | | | | | | | |\n")
                print("IMG file Keys =",keys)
                
                // var bodyValues:[String]!    //this array cannot append it should be assigned an empty array
                
                var bodyValues:[String] = []
                
                var bodyKeys:[String] = []
                
                for k:String in keys{
                    
                    if k != ""{
                        bodyKeys.append(k)
                    }
                }
               
                // Apply sorting here to Body Keys
                var Sorted_ImagebodyKeys = bodyKeys.sorted()
                
                print("Sorted_ImagebodyKeys =",Sorted_ImagebodyKeys)
                
                for v:String in values{
                    
                    if v != ""{
                        bodyValues.append(v)
                    }
                }
                
                //                    print("bodyValues =",bodyValues)
                //                    print("bodyKeys =",bodyKeys)
                //                    print("bodyUrl =",bodyUrl)
                
                for key in Sorted_ImagebodyKeys{
                    print("ImageKey in Sorted_ImagebodyKeys =",key)
                    print("sorted_ImagebodyKeys =",Sorted_ImagebodyKeys)
                    var Imagebody = readAuditProgressParsed.value(forKey: key) as! String
                    
//                    let url = self.serverUrl(bodyUrl)
//                    print("url=",url)
                    
                    print("ImageKey=",key)  // this is destination file name
                    print("Imagebody= ",Imagebody) // this is docs path of image
                    
                    let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
                    
//                    Imagebody
                       let NewImagebody = documentsDirectoryPath.appendingPathComponent(Imagebody).absoluteString
                    

//                    Imagebody = 
                    
                    if fileManager.fileExists(atPath: NewImagebody, isDirectory: &isDirectory){
                        
                        print("YES Image file present at ",NewImagebody)
                        let url: URL = URL(string: NewImagebody)!
//                        let url_to_string = url.absoluteString
                        
                        var destinationFileUrl = self.paramPath+"/"+key
                        if(destinationFileUrl.contains("LAB")){
                            destinationFileUrl = destinationFileUrl.replacingOccurrences(of: "/quonda", with: "/specs-sheet")
                        }

                        print("DST = ")
                        print(destinationFileUrl)
                        self.ImageUpload(url, destinationFileName: destinationFileUrl, filePathWithinFunction: filePathWithinFunction, ImageKey: key)
                        
                    }else{
                        print("Image file NOT exist at ",NewImagebody)
                    }
                }
            }
            catch let error as NSError{
                print("There is an error while reading AuditProgress(AuditCode) File.",error.description)
            }
        }
        //        })
        //        }
        //        AuditProgressQueue.addOperation(RequestOperation)
        
    }
    
    
    func serverUrl(_ toEscape:String) -> URL {
        let urlString = toEscape.addingPercentEscapes(using: String.Encoding.utf8)!
        let url = URL(string: urlString)
        return url!
    }
    
    
  
    func parseJSON(_ jsonString: String) -> NSDictionary {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                print(json!)
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else { 
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    
    func parseToRemove(_ jsonString: String) -> Dictionary<String,String> {
        if let data = jsonString.data(using: String.Encoding.utf8) {
            let error: NSError? = nil
            if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String,String> {
                return json!
            } else if let error = error {
                //Here's where the error comes back.
                print("JSON Error: \(error)")
            } else {
                print("Unknown JSON Error")
            }
        }
        return ["":""]
    }
    
    
    func ListDir_And_UploadImage(_ AuditScheduleDate:String, filePath:String) -> String {
        var pathString = ""

        if(filePath.contains("LAB")){
            self.paramPath = "/specs-sheet"
            pathString = "/specs-sheet"

        }
        else{
            self.paramPath = "/quonda"
            pathString = "/quonda"
        }
        print(pathString)
//        if isLabImage{
//            self.paramPath = "/specs-sheet"
//            pathString = "/specs-sheet"
//            isLabImage = false
//        }else{
//            self.paramPath = "/quonda"
//            pathString = "/quonda"
//
//            
//        }
//        
        
        print("AuditScheduleDate =")
        print(AuditScheduleDate)
        let scheduleDateString = AuditScheduleDate //"2016-04-20"
        print(scheduleDateString)
        var parts = scheduleDateString.components(separatedBy: "-")
        print(parts[0]) // year
        print(parts[1]) // month
        print(parts[2]) // day
        self.session.list(pathString) {
            (resources, error) -> Void in
            if resources == nil{
                print("resources = nil")
                
                self.ListDir_And_UploadImage(AuditScheduleDate,filePath: filePath)
                //                print("Cannot Determine Server Image Path ")
            }
            else{
                
                self.dirArray = resources!
                
                for item in self.dirArray{
                    self.resource = item as! ResourceItem
                    
                    if self.resource.type == .Directory && self.resource.name == parts[0]{
                        if self.paramPath != "\(pathString)/\(parts[0])"{
                            
                            self.paramPath = self.paramPath+"/"+"\(parts[0])"
                        }
                        print("paramPath in lisDir year =",self.paramPath)
                        self.session.list("\(pathString)/"+"\(parts[0])", completionHandler: { (resources, error) -> Void in
                            if resources == nil{
                                print("Cannot Determine Server Image Path ")
                            }
                            else{
                                
                                self.monArray = resources!
                                
                                for item in self.monArray{
                                    self.resource = item as! ResourceItem
                                    
                                    self.monthResource = nil
                                    
                                    if self.resource.type == .Directory && self.resource.name == parts[1]{
                                        self.monthResource = self.resource.name
                                        // - - - -
                                        if self.paramPath != "\(pathString)/\(parts[0])/\(parts[1])"{
                                           
                                            self.paramPath = "\(pathString)/\(parts[0])/\(parts[1])"
                                        }
                                        print("paramPath in lisDir mon=",self.paramPath)
//                                        self.paramPath = self.paramPath+"/"+"\(parts[1])"
                                        break
                                    }
                                }
                                if self.monthResource == nil{
                                    self.session.createDirectory("\(pathString)/"+"\(parts[0])"+"/"+"\(parts[1])") {
                                        (result, error) -> Void in
                                        print("Create directory with result:\n\(result), error: \(error)")
                                        if result == true{
                                            self.monthResource = parts[1]
                                            // -  - - -
                                            if self.paramPath != "\(pathString)/\(parts[0])/\(parts[1])"{
                                                
                                                self.paramPath = "\(pathString)/\(parts[0])/\(parts[1])"
                                            }
                                            print("paramPath in lisDir mon=",self.paramPath)
                                            
//                                            self.paramPath = self.paramPath+"/"+"\(parts[1])"
                                        }//still resolve error handling in case of error
                                    }
                                }
                                if self.monthResource == parts[1]{
                                    
                                    self.session.list("\(pathString)/"+"\(parts[0])"+"/"+"\(parts[1])", completionHandler: { (resources, error) -> Void in
                                        if resources == nil{
                                            print("Cannot Determine Server Image Path ")
                                        }
                                        else{
                                            
                                            self.dayArray = resources!   // Array of days in a month
                                            
                                            for item in self.dayArray{
                                                self.resource = item as! ResourceItem
                                                
                                                self.dayResource = nil
                                                
                                                if self.resource.type == .Directory && self.resource.name == parts[2]{
                                                    print("day name is here..")
                                                    print(self.resource.name)
                                                    self.dayResource = parts[2]
                                                    // - - - - - -
                                                    if self.paramPath != "\(pathString)/\(parts[0])/\(parts[1])/\(parts[2])"{
                                                        
                                                        self.paramPath = "\(pathString)/\(parts[0])/\(parts[1])/\(parts[2])"
                                                    }
                                                    print("paramPath in lisDir day=",self.paramPath)
                                                    
                                                    print("filePath Before upload =",filePath)
                                                    self.sendFIFO_IMG(filePath)
                                                    
                                                    self.dirArray = nil
                                                    self.monArray = nil
                                                    self.dayArray = nil
                                                    self.monthResource = nil
                                                    self.dayResource = nil

//                                                    self.paramPath = self.paramPath+"/"+"\(parts[2])"
                                                    break
                                                }
                                            }
                                            if self.dayResource == nil{
                                                self.session.createDirectory("\(pathString)/"+"\(parts[0])"+"/"+"\(parts[1])"+"/"+"\(parts[2])") {
                                                    (result, error) -> Void in
                                                    print("day result=\(result)")
                                                    print("day error=\(error)")
                                                    
                                                    if result == true{
                                                        
                                                        self.dayResource = parts[2]
                                                        // - - - - - -
                                                        if self.paramPath != "\(pathString)/\(parts[0])/\(parts[1])/\(parts[2])"{
                                                            
                                                            self.paramPath = "\(pathString)/\(parts[0])/\(parts[1])/\(parts[2])"
                                                        }
                                                        print("paramPath in lisDir day=",self.paramPath)

//                                                        self.paramPath = self.paramPath+"/"+"\(parts[2])"
                                                       
                                                        //Send Server image request
                                                        print("filePath Before upload =",filePath)
                                                        self.sendFIFO_IMG(filePath)
                                                        
                                                        self.dirArray = nil
                                                        self.monArray = nil
                                                        self.dayArray = nil
                                                        self.monthResource = nil
                                                        self.dayResource = nil
                                                    }
                                                    else{ // case of error if day dir cannot create
                                                        self.session.createDirectory("\(pathString)/"+"\(parts[0])"+"/"+"\(parts[1])"+"/"+"\(parts[2])") {
                                                            (result, error) -> Void in
                                                            print("day result=\(result)")
                                                            print("day error=\(error)")
                                                            
                                                            if result == true{
                                                                
                                                                self.dayResource = parts[2]
                                                                // - - - - -
                                                                if self.paramPath != "\(pathString)/\(parts[0])/\(parts[1])/\(parts[2])"{
                                                                    
                                                                    self.paramPath = "\(pathString)/\(parts[0])/\(parts[1])/\(parts[2])"
                                                                }
                                                                print("paramPath in lisDir day=",self.paramPath)
   
//                                                                self.paramPath = self.paramPath+"/"+"\(parts[2])"
                                                                
                                                                //Send Server image request
                                                                print("filePath Before upload =",filePath)
                                                                self.sendFIFO_IMG(filePath)
                                                                
                                                                self.dirArray = nil
                                                                self.monArray = nil
                                                                self.dayArray = nil
                                                                self.monthResource = nil
                                                                self.dayResource = nil
                                                            }else{
                                                                //if second time day result also = false Then
                                                                self.paramPath = "\(pathString)"
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else if self.dayResource == parts[2]{
                                                print(self.dayResource)
                                                // here we've got a full dayPath for saving our captured imageo onto the FTP Server.
                                                // which is paramPath string.
                                                //Send Server image request
                                               print("paramPath in lisDir day=",self.paramPath)
                                                
                                                if self.paramPath != "\(pathString)/\(parts[0])/\(parts[1])/\(parts[2])"{
                                                    
                                                    print("filePath Before upload =",filePath)
                                                    self.sendFIFO_IMG(filePath)
                                                    self.dirArray = nil
                                                    self.monArray = nil
                                                    self.dayArray = nil
                                                    self.monthResource = nil
                                                    self.dayResource = nil
                                                }

                                            }
                                            
                                            print("end")
                                        } // (else) if quonda/parts[0]/parts[1] != nil
                                    }) // end od month dir listing
                                }
                            } // (else) if quonda/parts[0] != nil
                        }) // end of year dir listing
                        break
                    }
                    print("- - - -")
                }
            } //(else) if resources in quonda dir != nil
        } // end of quonda dir
        
        return paramPath
    }
    
    
    
    var success = ""
    //upload function for FTP image upload
    func ImageUpload(_ url:URL!, destinationFileName:String!, filePathWithinFunction:String, ImageKey:String) {
        
        //if Reachability.isConnectedToNetwork() == true {
            print("Internet connection is OK")
            
            self.session.upload(url, path: destinationFileName) {
                (result, error) -> Void in
                print("Upload file with result:\n\(result), error: \(error)\n\n")
                
                print(result)
                self.success = "\(result)"
                if self.success == "true"{
                    
                    //CALL Function To Upload TEXT DATA
                    
//                    self.uploadTextData()
                   // UIApplication.shared.scheduledLocalNotifications
                   // self.scheduleLocalNotification(title:"Audit",message:"Audit Syned with Images.")

                    // deletes the saved image
                    
                    let url_to_string = url.absoluteString
                    
                    
                    
//                    let libraryDirectoryPath = NSURL(string: self.libraryDirectoryPathString)!
//                    self.jsonFilePath = libraryDirectoryPath.URLByAppendingPathComponent("myCapturedImage.jpg")
//                    
                    // creating a .jpg file in the Library Directory
                    
                    
                    if fileManager.fileExists(atPath: url_to_string, isDirectory: &isDirectory) {
                       
                        //Now delete the file.
                        do{
                            try fileManager.removeItem(atPath: url_to_string)
                            print("Image jpg file deleted successfully =",url_to_string)
                            print("\n\n")
                            
//                            let fileName = self.ExtractFileName(url_to_string)
//                            self.RemoveFileNameFrom_FilesToSync(fileName)
                            
                            // - - - - - - - - - - - - - - -
                            
                                // remove the keyValue pair saved in fileName(AuditCode)file
                                
                                print("Image Request sent!")
                                
                                // removes the keyValue Pair in ImageFileName(AuditCode)File
                                
                                print("start removing KEYVALUE pair in imageFile \n")
                            
                            //Reading image json file
                            
                                var fileDataString: String!
                                do{
                                    fileDataString = try NSString(contentsOfFile: filePathWithinFunction, encoding: String.Encoding.utf8.rawValue) as String
                                }catch{
                                    print("couldn't read file to Remove KV pair")
                                }
                                if fileDataString != nil{
                                    var DataInAuditProgressFile = self.parseToRemove(fileDataString)
                                    
                                    print("Before Removing KEYVALUE pair in AddDefectImg=",DataInAuditProgressFile.values)
                                    
                                    DataInAuditProgressFile.removeValue(forKey: ImageKey)
                                    
                                    print("\nAfter Removing KEYVALUE pair in AddDefectImg=",DataInAuditProgressFile.values)
                                    
                                    // Now write it back into the file.
                                    
                                    if fileManager.fileExists(atPath: filePathWithinFunction, isDirectory: &isDirectory) {
                                        do{
                                            let jsonDictionary = try JSONSerialization.data(withJSONObject: DataInAuditProgressFile, options: JSONSerialization.WritingOptions.prettyPrinted)
                                            
                                            let json = NSString(data: jsonDictionary, encoding: String.Encoding.utf8.rawValue) as! String
                                            
                                            try json.write(toFile: filePathWithinFunction, atomically: true, encoding: String.Encoding.utf8)
                                            print("Now updated AddDefectImg Json File after Removing KEYVALUE pair. \n")
                                            print("Updated AddDefectImg Json File =",json)
                                            
                                            
                                        } catch let error as NSError {
                                            print("JSON data couldn't be written to file // File not exist")
                                            print(error.description)
                                            
                                        }
                                    } // end of writing to AddDefectImg\(AuditCode).json file after removing keyValue pair
                                    
                                }
                            
                            
                             // Now going to delete AddDefectImg Json file from doc dir
                            
                            
                                //  Before delete count if in this file bodyKey.count = 0 if yes then delete.
                                
                                // first read out the file
                                var fileDataString2: String!
                                do{
                                    fileDataString2 = try NSString(contentsOfFile: filePathWithinFunction, encoding: String.Encoding.utf8.rawValue) as String
                                }catch{
                                    print("couldn't read file to Delete AddDefectImg.Json")
                                }
                                if fileDataString2 != nil{
                                    
                                    let readAuditProgressParsed:NSDictionary = self.parseJSON(fileDataString2)
                                    
                                    
                                    let keys = readAuditProgressParsed.allKeys as! [String]
                                    
                                    var bodyKeys:[String] = []
                                    
                                    for k:String in keys{
                                        
                                        if k != ""{
                                            bodyKeys.append(k)
                                        }
                                    }
                                    
                                    
                                    if bodyKeys.count == 0{
                                        
                                        //Now delete the file.
                                        do{
                                            try fileManager.removeItem(atPath: filePathWithinFunction)
                                            print("Img Json file deleted successfully =",filePathWithinFunction)
                                            print("\n\n")
                                            
                                            //Now also Remove this filePath from syncFile.json
                                            // First read syncFile.json
                                            // Then update syncFile.json
                                            // Then write it back to doc dir.
                                            let fileName = self.ExtractFileName(filePathWithinFunction)
                                            self.RemoveFileNameFrom_FilesToSync(fileName)
                                            
                                            
                                            //?????
                                            //????
                                            //???
                                            //??
                                            //?
                                        }
                                        catch{
                                            print("Could not delete file: \(error)")
                                        }
                                        
                                    }
                                }//end of fileDataString2 != nil
                            
                            // - - - - - - - - - - - - - - -

                        }
                        catch{
                            print("Could not delete file: \(error)")
                        }
                        
                    } else {
                        print("File NOT exists")
                    }
                }else{
//                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
//                    self.butotn.enabled = true
//                    self.butotn.backgroundColor = UIColor.lightGrayColor()
//                    
//                    let alertView = UIAlertController(title: "Retry" , message: "\(error!.description)Audit Not Saved", preferredStyle: .Alert)
//                    let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .Default, handler: { (UIAlertAction) -> Void in
//                        
//                        self.sampleImage.userInteractionEnabled = true
//                        self.BtnDefectType.userInteractionEnabled = true
//                        self.BtnDefectCode.userInteractionEnabled = true
//                        self.BtnDefectArea.userInteractionEnabled = true
//                        self.commentsField.userInteractionEnabled = true
//                        self.savedParamPath = destinationFileName
//                    })
//                    
//                    alertView.addAction(ok_action)
//                    self.presentViewController(alertView, animated: true, completion: nil)
                }
            }
       // } //if internet connection is OK
//        else{
//            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
//            self.butotn.enabled = true
//            self.butotn.backgroundColor = UIColor.lightGrayColor()
//            
//            let alertView = UIAlertController(title: "Audit cannot save" , message: "You may not connected to Internet.\nTry again", preferredStyle: .Alert)
//            
//            let ok_action: UIAlertAction = UIAlertAction(title: "OK", style: .Default, handler: { (UIAlertAction) -> Void in
//                
//                self.sampleImage.userInteractionEnabled = true
//                self.BtnDefectType.userInteractionEnabled = true
//                self.BtnDefectCode.userInteractionEnabled = true
//                self.BtnDefectArea.userInteractionEnabled = true
//                self.commentsField.userInteractionEnabled = true
//                
//            })
//            
//            alertView.addAction(ok_action)
//            self.presentViewController(alertView, animated: true, completion: nil)
//        } // Network failed else part
    }
    

    
@objc func reinstateBackgroundTask() {
        if updateTimer != nil && (backgroundTask == UIBackgroundTaskInvalid) {
            registerBackgroundTask()
        }
    }
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask (expirationHandler: {
            [unowned self] in
            self.endBackgroundTask()
        })
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTask() {
        NSLog("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
    }
    
}







