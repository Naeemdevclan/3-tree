//
//  tabedController.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 25/02/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class tabedController: UIViewController,UITabBarDelegate {

    @IBOutlet weak var tabBar1: UITabBar!
    @IBOutlet weak var tabBar2: UITabBar!
    
    weak var tab1vc: UIViewController!
    weak var tab2vc: UIViewController!
    weak var tab3vc: UIViewController!
    weak var tab4vc: UIViewController!
    weak var tab5vc: UIViewController!
    weak var tab6vc: UIViewController!
    weak var tab7vc: UIViewController!
    weak var tab8vc: UIViewController!
    
    var currentViewController: UIViewController!

    @IBOutlet weak var logout_Item: UITabBarItem!
    @IBOutlet weak var schedule_Audit_Item: UITabBarItem!
    @IBOutlet weak var dashboard_Item: UITabBarItem!
    @IBOutlet weak var QAreport_Item: UITabBarItem!
    
    @IBOutlet weak var auditor_Mode_Item: UITabBarItem!
    @IBOutlet weak var kpi_Mode_Item: UITabBarItem!
    @IBOutlet weak var search_Item: UITabBarItem!
    @IBOutlet weak var swarm_Mode_Item: UITabBarItem!
    
 
//    override func prefersStatusBarHidden() -> Bool {
//        return true
//    }
    var sampleData:String?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        tabBar1.backgroundColor = 
        
        print(sampleData)
        
        var TabBarItemImages: [UIImage] = []  // Dashboard
        TabBarItemImages += [UIImage(named: "Logout")!, UIImage(named: "Schedule Audit")!, UIImage(named: "Dashboard")!, UIImage(named: "Dashboard")!, UIImage(named: "Auditor Mode")!, UIImage(named: "Kpi Mode")!, UIImage(named: "Search")!, UIImage(named: "Swarm Mode")!]
        
        logout_Item.image = TabBarItemImages[0].withRenderingMode(.alwaysOriginal)
        schedule_Audit_Item.image = TabBarItemImages[1].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[2].withRenderingMode(.alwaysOriginal)
        dashboard_Item.image = TabBarItemImages[3].withRenderingMode(.alwaysOriginal)
        
        auditor_Mode_Item.image = TabBarItemImages[4].withRenderingMode(.alwaysOriginal)
        kpi_Mode_Item.image = TabBarItemImages[5].withRenderingMode(.alwaysOriginal)
        search_Item.image = TabBarItemImages[6].withRenderingMode(.alwaysOriginal)
        swarm_Mode_Item.image = TabBarItemImages[7].withRenderingMode(.alwaysOriginal)
    }
    
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("selected tabItem: \(item.tag)")
        switch (item.tag) {
        case 1:
            /*
            let loginVC = self.storyboard!.instantiateViewControllerWithIdentifier("loginViewController") as! LoginView
            
            loginVC.defaults.removeObjectForKey("username")
            loginVC.defaults.removeObjectForKey("password")
            
            presentViewController(loginVC, animated: true, completion: nil)
            */
            
            if tab1vc == nil{
                self.tab1vc = self.storyboard!.instantiateViewController(withIdentifier: "1")
                      }
            self.view.insertSubview(tab1vc.view, belowSubview: self.tabBar1)
            self.view.insertSubview(tab1vc.view, belowSubview: self.tabBar2)
            
            if currentViewController != nil{
                currentViewController.view.removeFromSuperview()
                currentViewController = tab1vc
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected-Logout")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 2:
            if tab2vc == nil{
                self.tab2vc = self.storyboard!.instantiateViewController(withIdentifier: "2")
            }
            self.view.insertSubview(tab2vc.view, belowSubview: self.tabBar1)
            self.view.insertSubview(tab2vc.view, belowSubview: self.tabBar2)
            
            if currentViewController != nil{
                currentViewController.view.removeFromSuperview()
                currentViewController = tab2vc
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Schedule Audit")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 3:
            
//            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("3") as? DashboardViewController {
//                presentViewController(resultController, animated: true, completion: nil)
//            }
            if tab3vc == nil{
                self.tab3vc = self.storyboard!.instantiateViewController(withIdentifier: "3")
                present(self.tab3vc, animated: true, completion: nil)
            }
            self.tab3vc = nil
            
            self.view.insertSubview(tab3vc.view, belowSubview: self.tabBar1)
            self.view.insertSubview(tab3vc.view, belowSubview: self.tabBar2)
            
            if currentViewController != nil{
                currentViewController.view.removeFromSuperview()
                currentViewController = tab3vc
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 4:
            if tab4vc == nil{
                self.tab4vc = self.storyboard!.instantiateViewController(withIdentifier: "4")
            }
            self.view.insertSubview(tab4vc.view, belowSubview: self.tabBar1)
            self.view.insertSubview(tab4vc.view, belowSubview: self.tabBar2)
            
            if currentViewController != nil{
                currentViewController.view.removeFromSuperview()
                currentViewController = tab4vc
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Dashboard")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 5:
            if tab5vc == nil{
                self.tab5vc = self.storyboard!.instantiateViewController(withIdentifier: "5")
            }
            self.view.insertSubview(tab5vc.view, belowSubview: self.tabBar1)
            self.view.insertSubview(tab5vc.view, belowSubview: self.tabBar2)
            
            if currentViewController != nil{
                currentViewController.view.removeFromSuperview()
                currentViewController = tab5vc
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Auditor Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 6:
            if tab6vc == nil{
                self.tab6vc = self.storyboard!.instantiateViewController(withIdentifier: "6")
            }
            self.view.insertSubview(tab6vc.view, belowSubview: self.tabBar1)
            self.view.insertSubview(tab6vc.view, belowSubview: self.tabBar2)
            
            if currentViewController != nil{
                currentViewController.view.removeFromSuperview()
                currentViewController = tab6vc
            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Kpi Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 7:
            if tab7vc == nil{
                self.tab7vc = self.storyboard!.instantiateViewController(withIdentifier: "7")
            }
            self.view.insertSubview(tab7vc.view, belowSubview: self.tabBar1)
            self.view.insertSubview(tab7vc.view, belowSubview: self.tabBar2)
            
//            if currentViewController != nil{
//                currentViewController.view.removeFromSuperview()
//                currentViewController = tab7vc
//            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Search")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        case 8:
            if tab8vc == nil{
                self.tab8vc = self.storyboard!.instantiateViewController(withIdentifier: "8")
            }
            self.view.insertSubview(tab8vc.view, belowSubview: self.tabBar1)
            self.view.insertSubview(tab8vc.view, belowSubview: self.tabBar2)
            
//            if currentViewController != nil{
//                currentViewController.view.removeFromSuperview()
//                currentViewController = tab8vc
//            }
            
            let selectedImage:UIImage = UIImage(named: "Selected Swarm Mode")!
            item.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
            
            break
        default:
            break
        }
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
