//
//  testTF.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 07/11/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit

class testTF: UITextField {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    var keyboardShowDate = Date()
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        
        self.initialisation()
    }
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        
        self.initialisation()
    }
    
    func initialisation()
    {
        self.addTarget(self, action: #selector(testTF.editingDidBegin(_:)), for: UIControlEvents.editingDidBegin)
        self.addTarget(self, action: #selector(testTF.editingDidEnd(_:)), for: UIControlEvents.editingDidEnd)
    }
    
    // MARK: Delegates
    
    func editingDidBegin(_ sender: AnyObject)
    {
        self.becomeFirstResponder()
        self.keyboardShowDate = Date()
    }
    
    func editingDidEnd(_ sender: AnyObject)
    {
        if(fabs(self.keyboardShowDate.timeIntervalSinceNow) <= 0.5)
        {
            self.becomeFirstResponder()
            let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            
            DispatchQueue.main.asyncAfter(deadline: delayTime, execute: { () -> Void in
                self.becomeFirstResponder()
            })
            
            
        }
    }

}


typealias dispatch_cancelable_block_t = ((_ cancel: Bool) -> Void)

func dispatch_async(_ block: @escaping ()->(), background: Bool = true) -> dispatch_cancelable_block_t?
{
    return dispatch_after(0, block: block, background: background)
}

func dispatch_after(_ delay: TimeInterval = 0, block: @escaping ()->(), background: Bool = false) -> dispatch_cancelable_block_t?
{
    var cancelableBlock : dispatch_cancelable_block_t?
    
    let delayBlock : dispatch_cancelable_block_t = { (cancel) -> Void in
        if(cancel == false)
        {
            if(background)
            {
                block()
            }
            else
            {
                DispatchQueue.main.async(execute: block)
            }
        }
        
        cancelableBlock = nil
    };
    
    cancelableBlock = delayBlock
    
    DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay * TimeInterval(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
        if let cb = cancelableBlock
        {
            cb(false)
        }
    })
    
    return cancelableBlock
}

func cancel_block(_ block: dispatch_cancelable_block_t)
{
    block(true)
}
