
//  userScriptClickEvents.js
//  QUONDA
//
//  Created by Muhammad Naeem on 11/05/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.



//This method 'window.webkit.messageHandlers.buttonClicked.postMessage script message' posts the "buttonClicked" event notifications from JS document

// "buttonClicked" is assigned as a ScriptMessageHandler in swift file


// Here ScriptMessageHandler name is "buttonClicked" which is current used in window.webkit.messageHandlers.buttonClicked.postMessage


var Brandbutton = document.getElementById("Brand");

var Vendorbutton = document.getElementById("Vendor");

var DateRangebutton = document.getElementById("DateRange");

var InspectionType;
var InlineBtn = document.getElementById("Inline");
var FinalBtn = document.getElementById("Final");

var KeyStatbutton = document.getElementById("KeyStats");

var AuditProgressionBtn = document.getElementById("AuditProgression");

var BrandProgressionBtn = document.getElementById("BrandProgression");



//KeyStats&Images
KeyStatbutton.addEventListener("click", function() {
                             
                               if (InlineBtn.className == "selected"){
                               InspectionType = "I"
                               }
                               else if (FinalBtn.className == "selected"){
                               InspectionType = "F"
                               }

                        var messgeToPost = {'KeyStatsBtnId':DateRangebutton.value,'brand':Brandbutton.value, 'vendor':Vendorbutton.value,'InspectionType':InspectionType};
                        window.webkit.messageHandlers.buttonClicked.postMessage(messgeToPost);
                        },false);




//DateRange
DateRangebutton.addEventListener("click", function() {
                        var messgeToPost = {'DateRangeBtnId':DateRangebutton.value};
                        window.webkit.messageHandlers.buttonClicked.postMessage(messgeToPost);
                        },false);

//SelectBrand
Brandbutton.addEventListener("click", function() {
                             var selectedValue;
                             if (InlineBtn.className == "selected"){
                                selectedValue = "I"
                             }
                             else if (FinalBtn.className == "selected"){
                             selectedValue = "F"
                             }
                             var messgeToPost = {'BrandBtnId':'hello', 'key2':selectedValue, 'key3':"value3"} //{'BrandBtnId':Brandbutton.value};
                                 window.webkit.messageHandlers.buttonClicked.postMessage(messgeToPost);
                                 },false);

//SelectVendor
Vendorbutton.addEventListener("click", function() {
                             var messgeToPost = {'VendorBtnId':Vendorbutton.value};
                             window.webkit.messageHandlers.buttonClicked.postMessage(messgeToPost);
                             },false);

//AuditProgression
AuditProgressionBtn.addEventListener("click", function() {
                                     
                                     if (InlineBtn.className == "selected"){
                                     InspectionType = "I"
                                     }
                                     else if (FinalBtn.className == "selected"){
                                     InspectionType = "F"
                                     }
                                     
                                     var messgeToPost = {'AuditProgression':DateRangebutton.value,'brand':Brandbutton.value, 'vendor':Vendorbutton.value,'InspectionType':InspectionType};
                                     
                                     
                              //var messgeToPost = {'AuditProgression':DateRangebutton.value,'brand':Brandbutton.value, 'vendor':Vendorbutton.value};
                              window.webkit.messageHandlers.buttonClicked.postMessage(messgeToPost);
                              },false);

//BrandProgression
BrandProgressionBtn.addEventListener("click", function() {
                                     
                                     if (InlineBtn.className == "selected"){
                                     InspectionType = "I"
                                     }
                                     else if (FinalBtn.className == "selected"){
                                     InspectionType = "F"
                                     }
                                     
                                     var messgeToPost = {'BrandProgression':DateRangebutton.value,'brand':Brandbutton.value, 'vendor':Vendorbutton.value,'InspectionType':InspectionType};
                                     
//                                     var messgeToPost = {'BrandProgression':DateRangebutton.value};
                                     window.webkit.messageHandlers.buttonClicked.postMessage(messgeToPost);
                                     },false);



