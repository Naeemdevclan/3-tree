//
//  default_Artwork.swift
//  QUONDA
//
//  Created by Muhammad Naeem on 12/04/2016.
//  Copyright © 2016 Muhammad Naeem. All rights reserved.
//

import UIKit
import MapKit

class default_Artwork: NSObject, MKAnnotation {
    let title: String?
    let coordinate: CLLocationCoordinate2D
    let address: String
    var imageName: String!
    
    init(title: String, coordinate: CLLocationCoordinate2D, address:String, imageName: String!) {
        self.title = title
        self.coordinate = coordinate
        self.address = address
        self.imageName = imageName
        super.init()
    }
    var subtitle: String? {
        return address
    }

    }
